primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarLeadsCadastrosSite();

function listarLeadsCadastrosSite(){
    
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarLeadsCadastrosSite",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);

        dadosGlobais = dados;
        
        $('#tabelaLeadsCadastrosSite').html('');
        
        if(dados.length>0){
            
          for(var i = 0; i < dados.length; i++) {
            
            $('#tabelaLeadsCadastrosSite').append(

                '<tr>' +
                    '<td>'+dados[i].id_leads+'</td>' +
                    '<td>'+dados[i].nome_leads+'</td>' +
                    '<td>'+dados[i].telefone_leads+' <a href="https://api.whatsapp.com/send?phone='+dados[i].telefone_leads+'&text=Olá *'+dados[i].nome_leads+'*, somos da LêkalêKids, tudo bem?" target="_blank"><button type="button" class="float-lg-right btn btn-xs btn-success waves-effect waves-light"><i class="mdi mdi-whatsapp"></i></button></a></td>' +
                    '<td>'+dados[i].email_leads+'</td>' +
                    '<td><button type="button" onclick="javascript:AbrirModalExcluir(' + i + ');" class="btn btn-block btn-xs btn-danger waves-effect waves-light"><span class="mr-2"><i class="far fa-trash-alt"></i></span> Excluir</button></td>' +
                '</tr>' 
                  
            );
          }

        } else {
            
            $('#tabelaLeadsCadastrosSite').append(
              '<td colspan="5">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );

        }
      }
    }
  );
}

function AbrirModalExcluir(index){showModalExcluir(index);}

function showModalExcluir(index){

  $('#modalExcluir').modal('show');
  $("#idExcluirLead").val(dadosGlobais[index].id_leads);
  $("#idExcluir").html(dadosGlobais[index].id_leads);
  $('#fecharModalExcluir').trigger('click');
  
}

$('#formExcluir').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = excluirLeads(self);
});

function excluirLeads(dados){
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/site/excluir/excluirLeads",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroExcluir').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarLeadsCadastrosSite();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#modalExcluir').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarLeadsCadastrosSite();
          
        }
      }
    }
  );
}

$('#formExcluirTudo').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = excluirTudo(self);
});

function excluirTudo(dados){
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/site/excluir/excluirTudo",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroExcluirTudo').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarLeadsCadastrosSite();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#excluirTudo').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarLeadsCadastrosSite();
          
        }
      }
    }
  );
}
