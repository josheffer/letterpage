
<style>
.fundo_card-body
{
    background-color: #373e48;
}

.custom-file-input:disabled~.custom-file-label {
    background-color: #4b5460;
}

</style>

<div class="content-page">

    <div class="content">
                    
        <div class="container-fluid">
        
            <div class="row mt-3">
            
                <div class="col-12">

                    <?php
                        $this->load->view('botoesLandingPages');
                    ?>
                           
                    <div class="row mt-3">

                        <div class="col-lg-12">

                            <div id="erroMsgEscolherTemplate"></div>
                            
                            <div class="card">

                                <div class="card-body">
                                    
                                    <h5 class="card-title mb-3">Escolha o template para a landing page</h5>
                                    
                                        <div class="row">

                                            <div class="col-xl-12">
                                                
                                                <div class="card-body">
                                                    
                                                <div class="row">

                                                    <?php 

                                                        foreach ($templates as $templatesRow) { 

                                                            if($landingpage[0]->template_landingPage ==  $templatesRow->id_templates)
                                                            {
                                                                
                                                                $templateAtual =  '<button type="submit" class="btn btn-xs btn-success btn-block waves-effect waves-light" disabled="disabled">
                                                                                        <i class="mdi mdi-check-all"></i> Atual
                                                                                    </button>';
                                                            } else {

                                                                $templateAtual = '<button type="submit" class="btn btn-primary btn-xs waves-effect waves-light">Escolher</button>';

                                                            }
                                                        
                                                        ?>
                                                    
                                                        <div class="col-lg-6 col-xl-2">
                                                            <div class="card text-center">
                                                                <img class="card-img-top img-fluid imagem-cover-top" src="/assets/images/templates/<?=$templatesRow->imagemTemplate_templates?>" alt="Card image cap">
                                                                <div class="card-body">

                                                                    <h5 class="card-title"><?=$templatesRow->nomeTemplate_templates?></h5>

                                                                    <form id="escolherTemplate<?=$templatesRow->id_templates?>">

                                                                        <input type="hidden" name="idLandingPage" id="idLandingPage" value="<?=$landingpage[0]->id_landingPage?>">
                                                                        <input type="hidden" name="idTemplate" id="idTemplate" value="<?=$templatesRow->id_templates?>">

                                                                        <?=$templateAtual?>

                                                                    </form>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    
                                                    <?php } ?>
                                                
                                                </div>

                                                </div> 

                                            </div>

                                        </div> 

                                    </div>
                                    
                                </div>

                             </div>

                        </div>

                    </div>

                </div>

            </div> 

        </div>

    </div> 
    
</div> 

