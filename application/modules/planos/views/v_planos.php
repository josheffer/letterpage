

<div class="content-page">
    
    <div class="content">
        
        <div class="container-fluid">
            
            <div class="row mt-4">
                
                <div class="col-12">
                    
                    <div class="row">

                        <div class="col-lg-12">

                            <div id="msgErro"></div>
                            
                            <?php if($permissaoLinks[35]->permissao_niveisPaginas==='1'):?>
                                <div class="card">

                                    <div class="card-body">
                                        
                                        <h5 class="card-title mb-3">Cadastrar plano</h5>
                                        
                                        <div class="row">
                                            
                                            <div class="col-lg-12 col-xl-12">
                                                
                                                <div class="card-box">
                                                    
                                                    <form id="formCadastrarNovoPlano">
                                                        
                                                        <div class="row">
                                                            
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Nome</label>
                                                                    <input type="text" class="form-control" id="nomePlano" name="nomePlano" autocomplete="off">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Valor</label>
                                                                    <input type="text" class="form-control" id="valorPlano" name="valorPlano" autocomplete="off"
                                                                    data-toggle="input-mask" data-mask-format="00.000,00" data-reverse="true">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label>Qtd de páginas</label>
                                                                    <input type="number" class="form-control" id="qtdPages_Plano" name="qtdPages_Plano" autocomplete="off">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label>Meses</label>
                                                                    <select class="form-control" id="mesesPlano" name="mesesPlano">
                                                                        <option value="">Selecione</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-1">
                                                                <div class="form-group">
                                                                    <label>Destaque?</label>
                                                                        <div>
                                                                            <input type="checkbox" class="switch_1" id="destaque_Plano" name="destaque_Plano">
                                                                        </div>
                                                                        
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="firstname">Descrição</label>
                                                                    <textarea type="text" class="form-control" id="descricaoPlano" name="descricaoPlano" autocomplete="off"></textarea>
                                                                </div>
                                                            </div>
                                                            
                                                        </div> 
                                                        
                                                        <div class="text-center">
                                                            <button type="submit" id="botaoCadastrarPagina" class="btn btn-block btn-success waves-effect waves-light mt-2">
                                                                <i class="mdi mdi-content-save"></i> Criar
                                                            </button>
                                                        </div>

                                                    </form>

                                                </div>

                                            </div> 
                                        </div>
                                        
                                    </div>
                                </div>
                            <?php endif;?>

                            <div id="msgErroPlanos"></div>
                            
                            <div class="row">
                                <div class="col-xl-12">
                                    <div id="accordion" class="mb-3">
                                        <div class="card mb-1">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="m-0">
                                                    <a class="text-dark" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                        Planos cadastrados
                                                    </a>
                                                </h5>
                                            </div>
                                
                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                                <div class="card-body">

                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Nome</th>
                                                                <th>Valor</th>
                                                                <th>Meses</th>
                                                                <?php if($permissaoLinks[36]->permissao_niveisPaginas==='1'):?><th>Status</th><?php endif;?>
                                                                <?php if($permissaoLinks[37]->permissao_niveisPaginas==='1'):?><th>Ações</th><?php endif;?>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tabelaListarPlanos">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
</div>

<?php if($permissaoLinks[36]->permissao_niveisPaginas==='1'):?>
    <div id="modalAtualizarStatus" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mySmallModalLabel">Atualizar status</h4>
                    <button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <form id="formAtualizaStatusPlanos">
                    <div class="modal-body">
                        
                        <div id="msgErroAtualizarStatus"></div>

                        Tem certeza que deseja atualizar o status desse plano??
                        <input type="hidden" name="idPlanoAtualizar" id="idPlanoAtualizar">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">(Sim) Atualizar</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif;?>

<?php if($permissaoLinks[37]->permissao_niveisPaginas==='1'):?>
    <div id="modalDeletarPlano" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mySmallModalLabel">Deletar plano</h4>
                    <button type="button" id="fecharModalDeletarPlano" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <form id="formDeletarPlano">
                    <input type="hidden" id="idDeletarPlano" name="idDeletarPlano">
                    <div class="modal-body">
                        
                        <div id="msgErroDeletarPlano"></div>

                        Tem certeza que deseja deletar esse plano?

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">(Sim) DELETAR</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif;?>

<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>