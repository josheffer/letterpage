dadosMinhaConta();

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

function dadosMinhaConta(){
    
  $.ajax(
    {
        
      url: "/dashboard/usuarios/dadosMinhaConta",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $("#idUsuariologado").val(dados.id_usuario);
        $("#loginUsuarioLogado").val(dados.login_usuario);
        $("#nomeUsuarioLogado").val(dados.nome_usuario);
        $("#nomeUsuarioLogadoBarra").html(dados.nome_usuario);
        $("#emailUsuarioLogado").val(dados.email_usuario);

        $('#imagemUsuarioPreview').attr("src", "/assets/painel/images/usuarios/"+dados.imagem_usuario);
        $('#imagemPerfilUsuarioBarra').attr("src", "/assets/painel/images/usuarios/"+dados.imagem_usuario);
      }
    }
  );
}


function readURL(input) 
{
    if (input.files && input.files[0]) 
    {
        var reader = new FileReader();

        reader.onload = function (e) 
        {
            $('#imagemUsuarioPreview')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$('#formMinhaContaUsuario').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarMinhaContaUsuario(self);

});

function atualizarMinhaContaUsuario(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/usuarios/atualizarMinhaContaUsuario",
      dataType: 'json',
      data: new FormData($('#formMinhaContaUsuario')[0]),
      cache: false,
      contentType: false,
      processData: false,

      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $("#botaoCadastrarTemplate").text("Tentar novamente...").prop("disabled", false);
          $("#nomeTemplate").prop("disabled", false);
          $("#imagemTemplate").prop("disabled", false);

          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          dadosMinhaConta();
          
        }
      }
    }
  );
}