listarPlanos();

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

function listarPlanos(){
    
  $.ajax(
    {
        
      url: "/dashboard/planos/listarPlanos",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#tabelaListarPlanos').html('');
        
        if(dados.dados.length>0){
          
          dadosGlobais = dados.dados;
          
          for(var i = 0; i < dados.dados.length; i++) {
            
            $('#escolherPlano'+ dados.dados[i].id_planos).submit(function(e){
              e.preventDefault();
              var self = $(this);
              var retorno = atualizarPlano(self);
              
            });

            if (dados.dados[i].status_planos === '0') 
            {
              if(dados.links[36].permissao_niveisPaginas === '1'){
                var botaoStatus = '<button type="button" onclick="javascript:AbrirModalAtualizarStatus(' + i + ');" class="btn btn-xs btn-dark waves-effect waves-light mr-1"><i class="la la-thumbs-down"></i></button>';
                var botaoStatusInicio = '<td>';
                var botaoStatusFim = '</td>';
              } else {
                var botaoStatus = '';
                var botaoStatusInicio = '';
                var botaoStatusFim = '';
              }
              
              
            } else {

              if(dados.links[36].permissao_niveisPaginas === '1'){
                var botaoStatus = '<button type="button" onclick="javascript:AbrirModalAtualizarStatus(' + i + ');" class="btn btn-xs btn-purple waves-effect waves-light mr-1"><i class="la la-thumbs-up"></i></button>';
                var botaoStatusInicio = '<td>';
                var botaoStatusFim = '</td>';
              } else {
                var botaoStatus = '';
                var botaoStatusInicio = '';
                var botaoStatusFim = '';
              }

            }

            if(dados.links[37].permissao_niveisPaginas === '1'){
              var botaoDeletar = '<button type="button" onclick="javascript:AbrirModalDeletarPlanos(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light">Excluir</button>';
              var botaoDeletarInicio = '<td>';
              var botaoDeletarFim = '</td>';
            } else {
              var botaoDeletar = '';
              var botaoDeletarInicio = '';
              var botaoDeletarFim = '';
            }

            

            $('#tabelaListarPlanos').append(

              '<tr>' +

                '<th>'+ dados.dados[i].id_planos +'</th>' +
                '<td>'+ dados.dados[i].nome_planos +'</td>' +
                '<td>'+ dados.dados[i].valor_planos +'</td>' +
                
                '<td>'+ dados.dados[i].meses_planos +'</td>' +

                botaoStatusInicio + 
                  botaoStatus + 
                botaoStatusFim +

                botaoDeletarInicio +
                  botaoDeletar +
                botaoDeletarFim +
                
             '</tr>'
                  
            );
          }

        } else {
            
            $('#tabelaListarPlanos').append(
              '<td colspan="5">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhum plano criado!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );

        }
      }
    }
  );
}

CKEDITOR.replace( 'descricaoPlano' );

$('#formCadastrarNovoPlano').submit(function(e){
  
  e.preventDefault();
  var self = $(this);
  
  for ( instance in CKEDITOR.instances )
  CKEDITOR.instances[instance].updateElement();
  // alert(self.serialize());
  var retorno = criarPlano(self);

});

function criarPlano(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/planos/cadastrar",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErro').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarPlanos();
          
        } else {
          
          $('#msgErro').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          $('#formCadastrarNovoPlano').each (function(){
            this.reset();
          });

          CKEDITOR.instances.descricaoPlano.setData("");

          listarPlanos();
          
        }
      }
    }
  );
}

function atualizarPlano(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/planos/atualizarPlano",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgEscolherPlano').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
          
        } else {
          
          $('#erroMsgEscolherPlano').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(5000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          setTimeout(function() {
            window.location.reload(1);
          }, 6000);
          
        }
      }
    }
  );
}

function AbrirModalAtualizarStatus(index) {

  showModalAtualizarStatus(index);
  
}

function showModalAtualizarStatus(index){

  $('#modalAtualizarStatus').modal('show');
  
  $("#idPlanoAtualizar").val(dadosGlobais[index].id_planos);
  
}

$('#formAtualizaStatusPlanos').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarStatusPlanos(self);

});

function atualizarStatusPlanos(dados){
  
$.ajax(
  {
    type: "POST",
    data: dados.serialize(),
    url: "/dashboard/planos/atualizarStatusPlano",
    dataType: 'json',
    
    success: function(retorno){
        
      if(retorno.ret == false) {

        $('#msgErroAtualizarStatus').html(
            
          '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
              '<h5 class="text-danger">Erro!</h5>' +
                retorno.msg +
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
              '<span aria-hidden="true">&times;</span>' +
              '</button>' +
          '</div>'
        );
        
        $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
        
        listarPlanos();
        
      } else {
        
        $('#msgErroPlanos').html(
            
          '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
            '<h5 class="text-success">Tudo certo!</h5>' +
              retorno.msg +
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
              '<span aria-hidden="true">&times;</span>' +
              '</button>' +
          '</div>'
        );

        $('#modalAtualizarStatus').modal('hide');
        
        $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

        listarPlanos();
        
      }
    }
  }
);
}

function AbrirModalDeletarPlanos(index) {

  showModalDeletarPlanos(index);
  
}

function showModalDeletarPlanos(index){

  $('#modalDeletarPlano').modal('show');
  
  $("#idDeletarPlano").val(dadosGlobais[index].id_planos);
  
}

$('#formDeletarPlano').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = deletarPlanos(self);

});


function deletarPlanos(dados){
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/planos/deletarPlanos",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {
  
          $('#msgErroDeletarPlano').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
          $('html,body').scrollTop(0);
  
          listarPlanos();
          
        } else {
          
          $('#erroMsgAtualizarStatus').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
  
          $('#modalDeletarPlano').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
          $('html,body').scrollTop(0);
  
          listarPlanos();
  
  
        }
      }
    }
  );
  }