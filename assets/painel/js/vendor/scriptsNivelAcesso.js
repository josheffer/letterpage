primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listaNiveisAcesso();

function listaNiveisAcesso(){
    
  $.ajax(
    {
        
      url: "/dashboard/permissoes/listarNiveisAcesso",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#tabelaListarNiveisAcesso').html('');
        
        if(dados.listar.length>0){
          
          dadosGlobais = dados.listar;
          
          for(var i = 0; i < dados.listar.length; i++) {

            if(dados.links[16].permissao_niveisPaginas === '1')
            {
              var botaoAcessarNivel = '<a href="/dashboard/permissoes/permissaoNiveisAcesso/'+ dados.listar[i].id_nivelAcesso +'"><button type="button" class="btn btn-info waves-effect waves-light"><i class="fas fa-gavel"></i></button></a>';
              var inicioBotaoAcessarNivel = '<td>';
              var FimBotaoAcessarNivel = '</td>';
            } else {
              
              var botaoAcessarNivel = '';
              var inicioBotaoAcessarNivel = '';
              var FimBotaoAcessarNivel = '';
            }

            if(dados.links[30].permissao_niveisPaginas === '1')
            {
              var botaoEditarNivel = '<button type="button" onclick="javascript:AbrirModalEditarNivelAcesso(' + i + ');" class="btn btn-primary btn-xs waves-effect waves-light mr-2">Modificar</button>';
              
            } else {
              
              var botaoEditarNivel = '';
            }

            if(dados.links[31].permissao_niveisPaginas === '1')
            {
              var botalExcluirNivel = '<button type="button" onclick="javascript:AbrirModalDeletarNivelAcesso(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light">Excluir</button>';
              
            } else {
              
              var botalExcluirNivel = '';
            }

            if(dados.links[30].permissao_niveisPaginas === '1' || dados.links[31].permissao_niveisPaginas === '1')
            {
              var inicioTd = '<td>';
              var fimTd = '</td>';
              
            } else {
              
              var inicioTd = '';
              var fimTd = '';
            }

            $('#tabelaListarNiveisAcesso').append(

              '<tr>' +

                '<th>' + dados.listar[i].id_nivelAcesso +'</th>' +

                '<td><h5><span class="badge" style="background-color:'+ dados.listar[i].cor_nivelAcesso +';color:'+ dados.listar[i].corTexto_nivelAcesso +';">'+ dados.listar[i].nome_nivelAcesso +'</span></h5></td>' +
                
                inicioBotaoAcessarNivel +
                  botaoAcessarNivel +
                FimBotaoAcessarNivel +
                
                inicioTd +
                  botaoEditarNivel +
                  botalExcluirNivel +
                fimTd +
                
             '</tr>'
                  
            );
          }

        } else {
            
            $('#tabelaListarNiveisAcesso').append(
              '<td colspan="4">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhum nível de acesso criado!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );

        }
      }
    }
  );
}

$('#formCadastrarNivelAcesso').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = criarNovaNivelAcesso(self);
});

function criarNovaNivelAcesso(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/permissoes/criarNivelAcesso",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErro').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaNiveisAcesso();
          
        } else {
          
          $('#msgErro').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
          
          $('#formCadastrarNivelAcesso').each (function(){
            this.reset();
          });

          listaNiveisAcesso();

        }
      }
    }
  );
}

function AbrirModalEditarNivelAcesso(editarNivelAcesso) {

  showModalEditarNivelAcesso(editarNivelAcesso);

}

function showModalEditarNivelAcesso(editarNivelAcesso){

  $('#modalAtualizarNivelAcesso').modal('show');
  
  $("#idNivelAcessotualizar").html(dadosGlobais[editarNivelAcesso].id_nivelAcesso);
  $("#atualizarIdNivelAcesso").val(dadosGlobais[editarNivelAcesso].id_nivelAcesso);
  $("#atualizarNomeNivelAcessoTitulo").html(dadosGlobais[editarNivelAcesso].nome_nivelAcesso);
  $("#atualizarNomeNivelAcesso").val(dadosGlobais[editarNivelAcesso].nome_nivelAcesso);
  $("#atualizarCorTextoNivelAcesso").val(dadosGlobais[editarNivelAcesso].corTexto_nivelAcesso);
  $("#atualizarCorNivelAcesso").val(dadosGlobais[editarNivelAcesso].cor_nivelAcesso);
  
  $('#fecharModalAtualizarNivelAcesso').trigger('click');

}

$('#formAtualziarNivelAcesso').submit(function(e){
  
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarNivelAcesso(self);

});

function atualizarNivelAcesso(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/permissoes/atualizarNivelAcesso",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroAtualizarNivelAcesso').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaNiveisAcesso();
          
        } else {
          
          $('#msgErro').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#modalAtualizarNivelAcesso').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaNiveisAcesso();
        }
      }
    }
  );
}

function AbrirModalDeletarNivelAcesso(deletarNivelAcesso) {

  showModalDeletarNivelAcesso(deletarNivelAcesso);

}

function showModalDeletarNivelAcesso(deletarNivelAcesso){

  $('#modalDeletarNivelAcesso').modal('show');
  
  $("#idNivelAcessoDeletar").val(dadosGlobais[deletarNivelAcesso].id_nivelAcesso);
  $("#nomeNivelAcessoDeletar").html(dadosGlobais[deletarNivelAcesso].nome_nivelAcesso);
  
  $('#fecharModalAtualizarNivelAcesso').trigger('click');

}

$('#formDeletarNivelAcesso').submit(function(e){
  
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = deletarNivelAcesso(self);

});

function deletarNivelAcesso(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/permissoes/deletarNivelAcesso",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroDeletarNivelAcesso').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaNiveisAcesso();
          
        } else {
          
          $('#msgErro').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#modalDeletarNivelAcesso').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaNiveisAcesso();
        }
      }
    }
  );
}