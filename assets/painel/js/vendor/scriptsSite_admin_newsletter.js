primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listaGrupos();
listarNewsletter();

function listarNewsletter(){
    
    $.ajax(
      {
          
        url: "/dashboard/listagens/listarNewsletter",
        
        ajax: 'dados.json',
        
        success: function(dados){
            
          var dados = JSON.parse(dados);
          
          if(dados.ativo_news === '1'){$('#ativo_newsletter').val(1).attr("checked", "checked");} else {$('#ativo_newsletter').val(1);}
          $('#email_newsletter').val(dados.email_news);
          
        }
      }
    );
};

$('#formAtualizarNewsletter').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarNewsletter(self);
});

function atualizarNewsletter(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarNewsletter",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarNewsletter();

          $('#formAtualizarNewsletter').each (function(){
            this.reset();
          });
        }
      }
    }
  );
}

function listaGrupos(index){
    
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarGrupo",
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#listargruposEmailIndividual').html('');

        if(dados[0].cd_grp_emails !== '0'){

          $('#listaEmailsLandingPage').append('<option value="">Selecione...</option>');
          
          for(var i = 0; i < dados.length; i++) {
            
            $('#listaEmailsLandingPage').append('<option value="'+dados[i].cd_grp_emails+'">'+dados[i].nm_grp_emails+'</option>');

          }

        } else {

          $('#listaEmailsLandingPage').append('<option value="">Nenhum grupo cadastrado</option>');
          
        }
        
      }
    }
  );
}