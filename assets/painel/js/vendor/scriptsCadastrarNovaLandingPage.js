listaGrupos();

function listaGrupos() {

    $.ajax({

        url: "/listagens/listarGrupo",

        success: function(data) {

            var dados = JSON.parse(data);

            $('#listargruposEmailIndividual').html('');

            if (dados[0].id_grupos !== '0') {

                $('#listaEmailsLandingPage').append('<option value="">Selecione...</option>');

                for (var i = 0; i < dados.length; i++) {

                    $('#listaEmailsLandingPage').append('<option value="' + dados[i].id_grupos + '">' + dados[i].nome_grupos + '</option>');

                }

            } else {

                $('#listaEmailsLandingPage').append('<option value="">Nenhum grupo cadastrado</option>');

            }

        }
    });
}