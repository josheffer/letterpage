<div class="content-page">

<div class="content">
    
    <div class="container-fluid mt-2">
        
        <div class="row">
            <div class="col-12">

                <?php
                    $this->load->view('botoesAll');
                ?>

                <div class="page-title-box">
                    
                    <div class="row">
                        
                        <div class="col-md-12">

                            <div id="mensagemErroEmails" class="mb-2 mt-2"></div>

                                <div class="card">

                                    <h6 class="card-header">Cadastrar E-mail individual</h6>
                                    
                                    <div class="card-body">
                                        
                                        <form id="formCadastrarNovoEmailIndividual">
                                            
                                            <div class="form-row align-items-center">
                                                
                                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-6">
                                                    <h6 class="font-13">Nome</h6>
                                                    <input type="text" class="form-control mb-2" id="nomeDestinatarioEmail" name="nomeDestinatarioEmail" autocomplete="off" autofocus>
                                                </div>

                                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-6">
                                                    <h6 class="font-13">E-mail</h6>
                                                    <input type="text" class="form-control mb-2" id="emailDestinatario" name="emailDestinatario" autocomplete="off">
                                                </div>

                                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-6">
                                                    <h6 class="font-13">Grupo</h6>
                                                    <select class="selectpicker mb-2"  data-style="btn-light" id="listargruposEmailIndividual" name="codigoGrupo"></select>
                                                </div>

                                                <div class="col-xl-4"></div>
                                                
                                                <div class="col-xl-4 mt-3">
                                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                                        <div id="botaoCadastrarGrupo">
                                                            <button type="submit" class="btn btn-success waves-effect waves-light btn-block">Cadastrar</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-4"></div> 

                                            </div>

                                        </form>

                                    </div>

                                </div> 
                            </div>
                        
                            <div class="col-md-12">

                                <div id="mensagemErroImportarLista"></div>

                                <div class="card">
                                    <h6 class="card-header">Importar lista <small class="text-warning"><strong>(Arquivos do tipo .CSV)</strong></small></h6>
                                    
                                    <div class="card-body">
                                        
                                        <form id="formImportarCSV">
                                            
                                            <div class="form-row align-items-center" id="divSumir">
                                                
                                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4">
                                                    <h6 class="font-13">Arquivo <code>(Máx 3mb)</code></h6>

                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-success file-upload-btn" id="botaoSelecionarArquivo">
                                                                    Procurar..
                                                                    <input type="file" class="file-upload" name="listaCSV" id="listaCSV" accept=".csv"/>
                                                                </button>
                                                            </span>

                                                            <input type="text" class="form-control file-upload-text" disabled placeholder="Escolha um arquivo" accept=".csv"/>
                                                            
                                                        </div>
                                                        
                                                    
                                                </div>

                                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4">
                                                    <h6 class="font-13">Grupo</h6>
                                                    <select class="selectpicker" data-style="btn-light" id="listargruposImportar" name="codigoGrupoImportar"></select>
                                                </div>

                                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4 mt-4">
                                                    <button type="submit" id="botaoEnviarArqCSV" class="btn btn-primary waves-effect waves-light btn-block">Importar</button>
                                                </div>

                                            </div>
                                        </form>

                                    </div>

                                </div> 
                            </div>
                    </div>

                    <div id="mensagemSUCESSOExclusaoListaEmails"></div>

                    <div class="row">
                        <div class="col-xl-12">
                            <div id="accordion" class="mb-3">
                                <div class="card mb-1">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="m-0">
                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                                Listas cadastradas
                                            </a>
                                        </h5>
                                    </div>
                        
                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                        <div class="card-body">

                                            <div class="table-responsive">
                                                <table class="table mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th>Nome</th>
                                                        <th>Quantidade</th>
                                                        <th>Ações</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tabelaEmails"></tbody>
                                                </table>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                    
                </div>

            </div>

        </div>     
            
    </div>

</div> 
    
</div>

<!-- MODAL DELETAR GRUPO -->
<!-- MODAL DELETAR GRUPO -->
<div id="ModalExcluirEmailsLista" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Excluir e-mails da lista: <i><span id="tituloListaExcluir"></span></i></h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div id="mensagemERROExcluirEmailsLista" class="col-md-12 mt-2"></div>
        
        <form id="formExcluirLista">

            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="idGrupoExcluirLista" name="idGrupoExcluirLista">
                            <input type="hidden" class="form-control" id="nomeGrupoExcluirLista" name="nomeGrupoExcluirLista">
                            <p>Deseja realmente excluir os e-mails dentro da lista: 
                                <strong><span id="excluirNomeLista"></span></strong>?
                                Após a exclusão não será possível reverter essa ação! Tem certeza?
                            </p>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
                <button type="submit" id="botaoExcluirEmailsLista" class="btn btn-danger waves-effect waves-light">Excluir</button>
            </div>
        </form>

    </div>
</div>
</div>
<!-- MODAL DELETAR GRUPO -->
<!-- MODAL DELETAR GRUPO -->