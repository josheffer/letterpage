primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarTabelaGrupos();

$('#formCadastrarNovoGrupo').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self. serialize());
    var retorno = cadastrarNovoGrupo(self);

});

$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

function cadastrarNovoGrupo(dados) {

    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/cadastros/cadastrarGrupo",
        dataType: 'json',
        beforeSend: function() {

            $("#nomeGrupoCadastro").prop("disabled", true);
            $("#botaoCadastrarGrupo").prop("disabled", true).html(
                '<button class="btn btn-success waves-effect waves-light mb-2 btn-block" type="button" disabled="">' +
                '<span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>' +
                'Aguarde..' +
                '</button>'
            );

        },
        success: function(retorno) {

            if (retorno.ret === false) {


                $("#nomeGrupoCadastro").prop("disabled", false);
                $("#botaoCadastrarGrupo").prop("disabled", false).html(
                    '<button class="btn btn-success waves-effect waves-light mb-2 btn-block" type="submit">' +
                    'Cadastrar' +
                    '</button>'
                );

                $('#mensagemCadastroGrupo').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<strong>Erro!</strong>' + retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                document.getElementById('nomeGrupoCadastro').focus();

                listarTabelaGrupos();

            } else {

                $("#nomeGrupoCadastro").prop("disabled", false);
                $("#botaoCadastrarGrupo").prop("disabled", false).html(
                    '<button class="btn btn-success waves-effect waves-light mb-2 btn-block" type="submit">' +
                    'Cadastrar' +
                    '</button>'
                );

                $('#mensagemCadastroGrupo').html(

                    '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                    '<strong>Tudo certo!</strong>' + retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '</div>'

                );

                $('html,body').scrollTop(0);

                $('#formCadastrarNovoGrupo').each(function() {
                    this.reset();
                });

                document.getElementById('nomeGrupoCadastro').focus();

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                listarTabelaGrupos();

            }
        }
    });
}

function listarTabelaGrupos() {

    $.ajax({

        url: "/listagens/listarGrupo",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            // console.log(dados);

            $('#tabelaListarGrupos').html('');

            if (dados.length > 0) {

                //ATRIBUINDO DADOS NA VARIÁVEL GLOBAL
                dadosGlobais = dados;
                //NESSA PARTE COMEÇA A CONSTRUÇÃO DOS DADOS NA TABELA
                for (var i = 0; i < dados.length; i++) {
                    //   '+ dados[i].foto_botaoDownload +'

                    $('#tabelaListarGrupos').append(

                        '<tr>' +
                        '<td>' + dados[i].id_grupos + '</td>' +
                        '<td>' + dados[i].nome_grupos + '</td>' +
                        '<td>' +
                        '<button type="button" onclick="javascript:AbrirModalEditarGrupo(' + i + ');" class="btn btn-primary btn-xs waves-effect waves-light mr-2" title="Código: ' + dados[i].id_grupos + '">Modificar</button>' +
                        '<button type="button" onclick="javascript:AbrirModalExcluirGrupo(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light" title="Código: ' + dados[i].id_grupos + '">Excluir</button>' +
                        '</td>' +
                        '</tr>'

                    );
                }

            } else {

                $('#tabelaListarGrupos').append(
                    '<tr>' +
                    '<td scope="row" colspan="6" class="text-center col-md-1">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Sem grupos criados!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>' +
                    '</tr>'
                );

            }
        }
    });
}


function AbrirModalEditarGrupo(index) {

    $('#ModalModificarGrupo').modal('show');

    showModal(index);

    $('#ModalModificarGrupo').on('shown.bs.modal', function() {
        $("#alterarNomeGrupo").focus();
    });

}

function showModal(index) {

    $("#idGrupo").val(dadosGlobais[index].id_grupos);

    $("#alterarNomeGrupo").val(dadosGlobais[index].nome_grupos);

    $("#tittuloGrupo").html(dadosGlobais[index].nome_grupos);

}

$('#formEditarGrupo').submit(function(e) {

    e.preventDefault();

    var self = $(this);

    // alert(self.serialize());

    var retorno = alterarFormularioGrupo(self);

});

function alterarFormularioGrupo(dados) {

    $('#fecharMsg').trigger('click');

    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/alteracoes/alterarGrupo",

        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#mensagemERROAlterargrupo').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Erro! </strong><br>' + retorno.msg +
                    '</div>'
                );

                listarTabelaGrupos();

            } else {

                retorno.ret === true;

                $('#mensagemSUCESSOgrupo').html(

                    '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Tudo certo! </strong>' + retorno.msg +
                    '</div>'
                );

                $('#ModalModificarGrupo').modal('hide');

                listarTabelaGrupos();
            }
        }
    });
}


function AbrirModalExcluirGrupo(index) {

    $('#ModalExcluirGrupo').modal('show');

    showModalExcluir(index);

}

function showModalExcluir(index) {

    $('#fecharMsgExcluir').trigger('click');

    $("#idGrupoExcluir").val(dadosGlobais[index].id_grupos);

    $("#excluirNomeGrupo").html(dadosGlobais[index].nome_grupos);

    $("#tituloGrupoExcluir").html(dadosGlobais[index].nome_grupos);

}

$('#formExcluirGrupo').submit(function(e) {

    e.preventDefault();

    var self = $(this);

    // alert(self.serialize());

    var retorno = excluirFormularioGrupo(self);

});

function excluirFormularioGrupo(dados) {

    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/exclusoes/excluirGrupo",

        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#mensagemERROAExcluirgrupo').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" id="fecharMsgExcluir">×</span>' +
                    '</button>' +
                    '<strong>Erro! </strong><br>' + retorno.msg +
                    '</div>'
                );

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                listarTabelaGrupos();

            } else {

                retorno.ret === true;

                $('#mensagemSUCESSOgrupo').html(

                    '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Tudo certo! </strong>' + retorno.msg +
                    '</div>'
                );

                $('#ModalExcluirGrupo').modal('hide');

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                listarTabelaGrupos();
            }
        }
    });
}