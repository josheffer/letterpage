$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

//VARIAVEL GLOBAL VISTA POR TODOS AS FUNÇÕES
var dadosGlobais;

listaLandingPages();
listaGrupos();

$('#formCadastrarNovaLandingPage').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cadastrarNovaLandingPage(self);

});

function cadastrarNovaLandingPage(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/cadastro/cadastrarNovaLandingPage",
        dataType: 'json',
        data: new FormData($('#formCadastrarNovaLandingPage')[0]),
        cache: false,
        contentType: false,
        processData: false,

        beforeSend: function() {

            $("#botaoCriarNovaLandingPage").text("Cadastrando...").prop("disabled", true);
            $("#nomeLandingPage").prop("disabled", true);
            $("#tituloLandingPage").prop("disabled", true);
            $("#imagemLogotipo").prop("disabled", true);
            $("#imagemFavicon").prop("disabled", true);

        },

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#erroMsg').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $("#botaoCriarNovaLandingPage").text("Tentar novamente!").prop("disabled", false);
                $("#nomeLandingPage").prop("disabled", false);
                $("#tituloLandingPage").prop("disabled", false);
                $("#imagemLogotipo").prop("disabled", false);
                $("#imagemFavicon").prop("disabled", false);

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

            } else {

                $('#erroMsg').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#formCadastrarNovaLandingPage').each(function() {
                    this.reset();
                });

                $('#previewFavicon').attr('src', '');
                $('#previewLogo').attr('src', '');

                $("#botaoCriarNovaLandingPage").text("Cadastrar nova LandingPage!").prop("disabled", false);
                $("#nomeLandingPage").prop("disabled", false);
                $("#tituloLandingPage").prop("disabled", false);
                $("#imagemLogotipo").prop("disabled", false);
                $("#imagemFavicon").prop("disabled", false);

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);


            }
        }
    });
}

function doTruncarStr(str, size) {
    if (str == undefined || str == 'undefined' || str == '' || size == undefined || size == 'undefined' || size == '') {
        return str;
    }

    var shortText = str;
    if (str.length >= size + 3) {
        shortText = str.substring(0, size).concat('...');
    }
    return shortText;
}

function listaLandingPages() {

    $.ajax({

        url: "/dashboard/listagens/listarTodasLandingPages",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            var dadosGerais = dados;

            $('#tabelaListarLandingPages ').html('');

            if (dados.links[12].permissao_niveisPaginas === '1' && dados.links[13].permissao_niveisPaginas === '1') {

                var inicioSelecTemp = '<td>';
                var fimSelecTemp = '</td>';
                var classBotaoSelecTemp = '';
            } else {
                var permissaoBotaoSelecionarTemp = '';
                var inicioSelecTemp = '';
                var fimSelecTemp = '';
                var classBotaoSelecTemp = '';
            }

            if (dados.links[12].permissao_niveisPaginas === '0' && dados.links[13].permissao_niveisPaginas === '1') {

                var inicioSelecTemp = '<td>';
                var fimSelecTemp = '</td>';
                var classBotaoSelecTemp = 'btn-block';
            }

            if (dados.links[12].permissao_niveisPaginas === '1' && dados.links[13].permissao_niveisPaginas === '0') {

                var inicioSelecTemp = '<td>';
                var fimSelecTemp = '</td>';
                var classBotaoSelecTemp = 'btn-block';
            }

            if (dados.links[18].permissao_niveisPaginas === '1') {
                var permissaoBotaoAttStatus = '';
            }

            if (dados.dados.length > 0) {

                dadosGlobais = dados.dados;

                for (var i = 0; i < dados.dados.length; i++) {

                    if (dados.links[11].permissao_niveisPaginas === '1') {
                        var botaoVerLeads = '<td><a href="/dashboard/landingPages/leads/' + dados.dados[i].id_landingPage + '"><button type="button"  class="btn btn-xs btn-warning" style="background-color: #ffffff; border-color: #ffffff; color: #3b434e;"><i class="la la-bar-chart-o"></i></button></a></td>';
                    }

                    if (dados.links[12].permissao_niveisPaginas === '1') {
                        var botaoSelecionarTemp = '<a href="/dashboard/templates/selecionarTemplate/' + dados.dados[i].id_landingPage + '"><button type="button" class="btn ' + classBotaoSelecTemp + ' btn-xs btn-info mr-2"><i class="fe-layout"></i></button></a>';

                    } else {

                        var botaoSelecionarTemp = '';
                    }

                    if (dados.links[13].permissao_niveisPaginas === '1') {
                        var botaoEditarTemp = '<a href="/dashboard/templates/configurarTemplate/' + dados.dados[i].id_landingPage + '"><button type="button" class="btn ' + classBotaoSelecTemp + ' btn-xs btn-light mr-2 waves-effect waves-light"><i class="mdi mdi-pencil"></i></button></a>';

                    } else {

                        var botaoEditarTemp = '';
                    }

                    if (dados.links[18].permissao_niveisPaginas === '1') {

                        if (dados.dados[i].status_landingPage == 0) {
                            var botaoStatus = '<button type="button" onclick="javascript:AbrirModalAtualizarStatus(' + i + ');" class="btn btn-block btn-xs btn-dark waves-effect waves-light"><i class="fas fa-thumbs-down"></i></button>';
                            var inicioStatus = '<td>';
                            var fimStatus = '</td>';
                        } else {
                            var botaoStatus = '<button type="button" onclick="javascript:AbrirModalAtualizarStatus(' + i + ');" class="btn btn-block btn-xs btn-purple waves-effect waves-light"><i class="fas fa-thumbs-up"></i></button>';
                            var inicioStatus = '<td>';
                            var fimStatus = '</td>';
                        }

                    } else {
                        var botaoStatus = '';
                    }

                    if (dados.dados[i].status_landingPage == 1) {

                        var botaoAcessar =
                            '<a href="' + dados.dados[i].link_landingPage + '" target="_blank">' +
                            '<button type="button" class="btn btn-xs btn-primary waves-effect waves-light">' +
                            '<span class="btn-label"><i class="mdi mdi-cursor-default"></i></span>Acessar' +
                            '</button>' +
                            '</a>';

                    } else {

                        var botaoAcessar =
                            '<button type="button" class="btn btn-xs btn-primary waves-effect waves-light" disabled="disabled">' +
                            '<span class="btn-label"><i class="mdi mdi-cursor-default"></i></span>Acessar' +
                            '</button>';

                    }

                    if (dados.links[19].permissao_niveisPaginas === '1') {
                        botaoEditarLandingPage =
                            '<button type="button" onclick="javascript:AbrirModalEditarLandingPage(' + i + ');" class="btn btn-xs btn-success waves-effect waves-light">' +
                            '<span class="btn-label"><i class="la la-pencil-square"></i></span>Editar' +
                            '</button>'
                    } else {

                        botaoEditarLandingPage = "";
                    }

                    if (dados.links[20].permissao_niveisPaginas === '1') {
                        botaoExcluirLandingPage =
                            '<button type="button" onclick="javascript:AbrirModalExcluirLandingPages(' + i + ');" class="btn btn-xs btn-danger waves-effect waves-light">' +
                            '<span class="btn-label"><i class="far fa-trash-alt"></i></span>Excluir' +
                            '</button>'
                    } else {

                        botaoExcluirLandingPage = "";
                    }

                    $('#tabelaListarLandingPages ').append(

                        '<tr>' +

                        '<th scope="row">' + dados.dados[i].id_landingPage + '</th>' +

                        '<td>' + doTruncarStr(dados.dados[i].nome_landingPage, 50) + '</td>' +

                        botaoVerLeads +

                        inicioStatus +
                        botaoStatus +
                        fimStatus +

                        inicioSelecTemp +
                        botaoSelecionarTemp +
                        botaoEditarTemp +
                        fimSelecTemp +

                        '<td>' +
                        ' <div class="button-list">' +

                        botaoAcessar +

                        botaoEditarLandingPage +

                        botaoExcluirLandingPage +

                        '</div>' +

                        '</td>' +

                        '</tr>'

                    );
                }

            } else {

                $('#tabelaListarLandingPages').append(
                    '<td colspan="6">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma landing page criada!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }
        }
    });
}


function AbrirModalAtualizarStatus(attStatus) {

    showModalAtualizarStatus(attStatus);

}

function showModalAtualizarStatus(attStatus) {

    $('#modalAtualizarStatus').modal('show');

    $("#idLandingPage").val(dadosGlobais[attStatus].id_landingPage);

    $('#fecharModalAtualizarStatus').trigger('click');

}

$('#formAtualizaStatus').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarStatusLandingPage(self);

});

function atualizarStatusLandingPage(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/landingpages/atualizar/atualizarStatusLandingPage",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroAtualizarStatus').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                // listaLandingPages();

            } else {

                $('#msgSucesso').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#modalAtualizarStatus').modal('hide');

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaLandingPages();

            }
        }
    });
}

function AbrirModalExcluirLandingPages(index) {

    showModalExcluirLandingPages(index);

}

function showModalExcluirLandingPages(index) {

    $('#modalExcluirLandingPage').modal('show');

    $("#idLandingPageExcluir").val(dadosGlobais[index].id_landingPage);
    $("#idLanding").html(dadosGlobais[index].id_landingPage);

    $('#fecharModalAtualizarStatus').trigger('click');

}

$('#formExcluirLandingPage').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = excluirLandingPages(self);

});

function excluirLandingPages(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/landingpages/excluir/excluirLandingPages",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroExcluirLandingPage').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaLandingPages();

            } else {

                $('#msgSucesso').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#modalExcluirLandingPage').modal('hide');

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaLandingPages();

            }
        }
    });
}

//INICIO MODAL EDITAR LANDING PAGE
function showModalEditarTemplate(index) {

    $('#modalEditarTemplateLandingPage').modal('show');

    $("#nomeCampanhaVisualizarTemplate").html(dadosGlobais[index].ds_nome);
    $("#corpoMensagemVisualizar").html(dadosGlobais[index].ds_mensagem);


}

function AbrirModalEditarLandingPage(index) {

    showModalEditarLandingPage(index);

}

function showModalEditarLandingPage(index) {

    $('#modalEditarLandingPage').modal('show');

    $("#nomeLandingPageEditar").val(dadosGlobais[index].nome_landingPage);
    $("#tituloLandingPageEditar").val(dadosGlobais[index].titulo_landingPage);
    $("#idLandingPageEditar").val(dadosGlobais[index].id_landingPage);
    $("#tituloNomeLandingPageEditar").html(dadosGlobais[index].nome_landingPage);

}

$('#formEditarLandingPage').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = editarLandingPages(self);

});

function editarLandingPages(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/landingpages/atualizar/editarLandingPage",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#erroMsgEditarLandingPage').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaLandingPages();

            } else {

                $('#msgSucesso').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#modalEditarLandingPage').modal('hide');

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaLandingPages();

            }
        }
    });
}
//FIM MODAL EDITAR LANDING PAGE

$(document).on('show.bs.modal', '.modal', function(event) {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

function AbrirConfirmacaoTemplate(index) {

    showModalConfirmacaoTemplate(index);

}

function showModalConfirmacaoTemplate(index) {

    $('#modalConfirmarSelecaoModeloTemplate').modal('show');

    $("#idTemplateModeloSelecionar").val(dadosGlobais2[index].id_templates);

    $("#idLandingPageModeloSelecionar").val(dadosGlobais[index].id_landingPage);

}

function listaGrupos(index) {

    $.ajax({

        url: "/dashboard/listagens/listarGrupo",

        success: function(data) {

            var dados = JSON.parse(data);

            $('#listargruposEmailIndividual').html('');

            if (dados[0].id_grupos !== '0') {

                $('#listaEmailsLandingPage').append('<option value="">Selecione...</option>');

                for (var i = 0; i < dados.length; i++) {

                    $('#listaEmailsLandingPage').append('<option value="' + dados[i].id_grupos + '">' + dados[i].nome_grupos + '</option>');

                }

            } else {

                $('#listaEmailsLandingPage').append('<option value="">Nenhum grupo cadastrado</option>');

            }

        }
    });
}