$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarCampanhasTemplates();
listarModelosTemplateEmails();

function listarCampanhasTemplates() {

    $.ajax({

        url: "/listagens/listarCampanhasTemplates",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#tabelaListarCampanhas_x_templateEmail').html('');

            dadosGlobais = dados;

            if (dados.campanhas.length > 0) {


                for (var i = 0; i < dados.campanhas.length; i++) {

                    $('#tabelaListarCampanhas_x_templateEmail').append(

                        '<tr>' +
                        '<td>' + dados.campanhas[i].id_campanha + '</td>' +
                        '<td>' + dados.campanhas[i].nome_campanha + '</td>' +

                        '<td>' +
                        '<a href="javascript: void(0);" onclick="javascript:AbrirModalVerTemplate(' + i + ');" class="btn btn-xs btn-info mr-1" data-toggle="tooltip" data-placement="top" title="Visualizar template" data-original-title="Visualizar template">' +
                        '<i class="mdi mdi-eye"></i>' +
                        '</a>' +
                        '</td>' +

                        '<td>' +
                        '<div class="custom-control custom-radio">' +
                        '<input type="radio" id="selecionarCampanha' + dados.campanhas[i].id_campanha + '" name="selecionarCampanha" value="' + dados.campanhas[i].id_campanha + '" class="custom-control-input">' +
                        '<label class="custom-control-label" for="selecionarCampanha' + dados.campanhas[i].id_campanha + '"></label>' +
                        '</div>' +
                        '</td>' +
                        '</tr>'

                    );
                }

            } else {

                $('#tabelaListarCampanhas_x_templateEmail').append(
                    '<tr>' +
                    '<td scope="row" colspan="4" class="text-center col-md-1">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Sem campanhas criadas!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>' +
                    '</tr>'
                );

            }
        }
    });
}

function listarModelosTemplateEmails() {

    $.ajax({

        url: "/listagens/listarModelosTemplateEmailsTabela",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#tabelaListarModelosTemplate').html('');

            dadosTemplateEmail = dados;

            if (dados.infosTemp.length > 0) {

                for (var i = 0; i < dados.infosTemp.length; i++) {

                    $('#tabelaListarModelosTemplate').append(

                        '<tr>' +
                        '<td>' + dados.infosTemp[i].id_templateEmail + '</td>' +

                        '<td>' + dados.infosTemp[i].nome_templateEmail + '</td>' +

                        '<td>' +
                        '<a href="javascript: void(0);" onclick="javascript:AbrirModalVerTemplateEmail(' + i + ');" class="btn btn-xs btn-info mr-1" data-toggle="tooltip" data-placement="top" title="Visualizar template" data-original-title="Visualizar template">' +
                        '<i class="mdi mdi-eye"></i>' +
                        '</a>' +
                        '</td>' +

                        '<td>' +
                        '<div class="custom-control custom-radio">' +
                        '<input type="radio" id="selecionarTemplate' + dados.infosTemp[i].id_templateEmail + '" name="selecionarTemplate" value="' + dados.infosTemp[i].id_templateEmail + '" class="custom-control-input">' +
                        '<label class="custom-control-label" for="selecionarTemplate' + dados.infosTemp[i].id_templateEmail + '"></label>' +
                        '</div>' +
                        '</td>' +
                        '</tr>'

                    );
                }

            } else {

                $('#tabelaListarModelosTemplate').append(
                    '<tr>' +
                    '<td scope="row" colspan="4" class="text-center col-md-1">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Sem modelos criados!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>' +
                    '</tr>'
                );

            }
        }
    });
}

function AbrirModalVerTemplate(verTemplate) {

    showModalVerTemplate(verTemplate);

}

function showModalVerTemplate(verTemplate) {

    $('#modalVerTemplateCampanha').modal('show');

    $("#nomeModeloTemplateVerCampanha").html("#" + dadosGlobais.campanhas[verTemplate].id_templateEmail + " - " + dadosGlobais.campanhas[verTemplate].nome_templateEmail);
    $("#ModeloTemplate").html(dadosGlobais.campanhas[verTemplate].html_templateEmail);

    $('#linkImagemLogotipo').attr('src', dadosGlobais.dadosTemplate[0].linkLogotipo_configs_template);
    $('#linkImagemLogotipo2').attr('src', dadosGlobais.dadosTemplate[0].linkLogotipo_configs_template);
    $('#linkImagem').attr('href', dadosGlobais.dadosTemplate[0].linkImagem_configs_template);
    $('#linkImagem2').attr('href', dadosGlobais.dadosTemplate[0].linkImagem_configs_template);
    $('#linkWhatsApp').attr('href', dadosGlobais.dadosTemplate[0].linkWhatsApp_configs_template);
    $('#linkInstagram').attr('href', dadosGlobais.dadosTemplate[0].linkInstagram_configs_template);
    $('#linkFacebook').attr('href', dadosGlobais.dadosTemplate[0].linkFacebook_configs_template);

    $('#linkWhatsApp2').attr('href', dadosGlobais.dadosTemplate[0].linkWhatsApp_configs_template);
    $('#linkInstagram2').attr('href', dadosGlobais.dadosTemplate[0].linkInstagram_configs_template);
    $('#linkFacebook2').attr('href', dadosGlobais.dadosTemplate[0].linkFacebook_configs_template);

    $("#empresaNome").html(dadosGlobais.dadosTemplate[0].nomeEmpresa_configs_template);
    $("#endereco").html(dadosGlobais.dadosTemplate[0].endereco_configs_template);

    $('#linkDominioRed').attr('href', dadosGlobais.dadosTemplate[0].linkDominio_configs_template);
    $("#dominio").html(dadosGlobais.dadosTemplate[0].dominio_configs_template);

}

function AbrirModalVerTemplateEmail(verTemplateEmail) {

    showModalVerTemplateEmail(verTemplateEmail);

}

function showModalVerTemplateEmail(verTemplateEmail) {

    $('#modalVerTemplateEmail').modal('show');

    $("#nomeModeloTemplateEmail").html("#" + dadosTemplateEmail.infosTemp[verTemplateEmail].id_templateEmail + " - " + dadosTemplateEmail.infosTemp[verTemplateEmail].nome_templateEmail);
    $("#ModeloTemplateEmail").html(dadosTemplateEmail.infosTemp[verTemplateEmail].html_templateEmail);

    $('#linkImagemLogotipo').attr('src', dadosTemplateEmail.dadosConfigsTemp[0].linkLogotipo_configs_template);
    $('#linkImagemLogotipo2').attr('src', dadosTemplateEmail.dadosConfigsTemp[0].linkLogotipo_configs_template);
    $('#linkImagem').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkImagem_configs_template);
    $('#linkImagem2').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkImagem_configs_template);
    $('#linkWhatsApp').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkWhatsApp_configs_template);
    $('#linkInstagram').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkInstagram_configs_template);
    $('#linkFacebook').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkFacebook_configs_template);

    $('#linkWhatsApp2').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkWhatsApp_configs_template);
    $('#linkInstagram2').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkInstagram_configs_template);
    $('#linkFacebook2').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkFacebook_configs_template);

    $("#empresaNome").html(dadosTemplateEmail.dadosConfigsTemp[0].nomeEmpresa_configs_template);
    $("#endereco").html(dadosTemplateEmail.dadosConfigsTemp[0].endereco_configs_template);

    $('#linkDominioRed').attr('href', dadosTemplateEmail.dadosConfigsTemp[0].linkDominio_configs_template);
    $("#dominio").html(dadosTemplateEmail.dadosConfigsTemp[0].dominio_configs_template);

}

$('#formAtualizarTemplateCampanha').submit(function(e) {

    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarTemplateEmailCampanha(self);

});

function atualizarTemplateEmailCampanha(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/alteracoes/atualizarTemplateEmailCampanha",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                var inputs = document.querySelectorAll('input[type="radio"]');
                for (var i = 0, l = inputs.length; i < l; i++) {
                    inputs[i].checked = false;
                }

                listarCampanhasTemplates();
                listarModelosTemplateEmails();
            }
        }
    });
}