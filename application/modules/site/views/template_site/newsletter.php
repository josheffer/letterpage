            <?php if($newsletter === '1'):?>
                <section class="newsletter padding-top-text-60 padding-bottom-60 wow fadeIn">
                    <div class="container mt-4">
                        <h3 class="title_h3  text-capitalize home_title_h3 text-center wow fadeInUp">Receba novidades</h3>
                        <form class="form-inline justify-content-center" id="formNewsletter">
                        
                            <input type="hidden" name="id_listaNewsletter" name="id_listaNewsletter" value="<?=$id_listaNewsletter?>">

                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="nome_newsletter" name="nome_newsletter" placeholder="Seu nome" autocomplete="off">
                            </div>
                            
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="telefone_newsletter" name="telefone_newsletter" placeholder="Seu telefone (Whatsapp)" autocomplete="off"
                                        data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                            </div>

                            <div class="form-group col-md-3">
                                <input type="email" class="form-control" id="email_newsletter" name="email_newsletter" placeholder="Seu e-mail" autocomplete="off">
                            </div>
                            
                            <button type="submit" class="btn border-btn text-uppercase wow fadeInRight" name="subscribe">Assinar
                                <i class="flaticon-arrows"></i>
                            </button>

                        </form>

                        <div class="container mt-4">
                            <div class="row justify-content-center">
                                <div class="col-10" id="msgErro"></div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif;?>