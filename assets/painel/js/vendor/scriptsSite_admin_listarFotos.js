primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarFotos();

function listarFotos(){
    
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarFotos",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);

        dadosGlobais = dados;
        
        $('#tabelaFotos').html('');
        
        if(dados.length>0){
            
          for(var i = 0; i < dados.length; i++) {

            if(dados[i].ativo_fotos === '1')
            {
              
              var botaoStatus = '<button type="submit" onclick="javascript:atualizarStatusFotos(' + i + ');" class="btn btn-xs btn-block btn-purple waves-effect waves-light">Bloquear</button>';
            
            } else {

              var botaoStatus = '<button type="submit" onclick="javascript:atualizarStatusFotos(' + i + ');" class="btn btn-xs btn-block btn-danger waves-effect waves-light">Desbloquear</button>';

            }
            
            
            $('#tabelaFotos').append(

              '<tr>' +
                '<th>'+dados[i].id_fotos+'</th>' +
                '<td>'+dados[i].nome_fotos+'</td>' +
                '<td>'+botaoStatus+'</td>' +
                '<td>' +
                    '<button type="button" onclick="javascript:editarFoto(' + i + ');" class="btn btn-xs btn-primary waves-effect waves-light mr-2"><i class="mdi mdi-pencil-outline"></i></button>' +
                    '<button type="button" onclick="javascript:excluirFoto(' + i + ');" class="btn btn-xs bg-soft-red waves-effect waves-light mr-1"><i class="mdi mdi-trash-can"></i></i></button>' +
                '</td>' +
              '</tr>' 
                  
            );
          }

        } else {
            
            $('#tabelaFotos').append(
              '<td colspan="4">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhuma foto cadastrada!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );

        }
      }
    }
  );
}

function atualizarStatusFotos(dados){
  
  $.ajax(
    {
      type: "POST",
      
      data: { 
        idAtualizar: dadosGlobais[dados].id_fotos
      },

      url: "/dashboard/atualizar/atualizarStatusFotos",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
          }, 1000, 'linear');

          listarFotos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

         $('html,body').animate({
            scrollTop: 0
          }, 1000, 'linear');

          listarFotos();
          
        }
      }
    }
  );
}


function editarFoto(attFoto)
{
  
  $('#formEditarFoto').each (function(){
    this.reset();
  });

  showModalEditar(attFoto);
}

function showModalEditar(attFoto){

  $('#modalEditarFoto').modal('show');
  $("#idEditarFoto").val(dadosGlobais[attFoto].id_fotos);
  $("#nomeFotoEditar").val(dadosGlobais[attFoto].nome_fotos);
  $("#tituloNomeFoto").html(dadosGlobais[attFoto].nome_fotos);

  $("#seoFotoEditar").val(dadosGlobais[attFoto].seo_fotos);
  
  $("#previewEditar").html('<img width="100%" src="/assets/site/images/fotos/'+dadosGlobais[attFoto].imagem_fotos+'">');
  
}

$('#formEditarFoto').submit(function(e){
  e.preventDefault();
  var self = $(this);
  
  // alert(self.serialize());
  var retorno = atualizarFotos(self);
});

function atualizarFotos(dados){

  var form = $('#formEditarFoto')[0];

  var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarFotos",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgEditarFoto').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
            }, 1000, 'linear');

          listarFotos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
            }, 1000, 'linear');

            $('#formEditarFoto').each (function(){
              this.reset();
            });

          $('#modalEditarFoto').modal('hide');

          listarFotos();
          
        }
      }
    }
  );
}

function excluirFoto(excluirFoto)
{
  showModalExcluir(excluirFoto);
}

function showModalExcluir(excluirFoto){

  $('#modalExcluir').modal('show');
  $("#idExcluir").html(dadosGlobais[excluirFoto].id_fotos);
  $("#idfotos").val(dadosGlobais[excluirFoto].id_fotos);
}

$('#formExcluirFoto').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = deletarFotos(self);
});

function deletarFotos(dados){
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/site/excluir/deletarFotos",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroExcluirTudo').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
            }, 1000, 'linear');

          listarFotos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#modalExcluir').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
            }, 1000, 'linear');

          listarFotos();
          
        }
      }
    }
  );
}

$('#formCadastrarNovaFoto').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = cadastrarNovaFoto(self);
});

function cadastrarNovaFoto(dados){

  var form = $('#formCadastrarNovaFoto')[0];

  var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/cadastrar/cadastrarNovaFoto",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
          }, 1000, 'linear');


          listarFotos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
          }, 1000, 'linear');

          $('#formCadastrarNovaFoto').each (function(){
            this.reset();
          });
            

          listarFotos();
          
        }
      }
    }
  );
}