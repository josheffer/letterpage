listarAcessos();
listarRelatoriosLeads();
listarRelatorioSemana();
listarRelatorioMensal();
listarRelatorioAnual();

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

function listarAcessos(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatorioAcessos/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
                
                var dados = JSON.parse(data);
                
                dadosGlobais = dados;

                $('#acessosDiario').html(dados.diario[0].totalAcessosDiario);
                $('#acessosSemanais').html(dados.semanal[0].totalAcessosSemanal);
                $('#acessosMensal').html(dados.mensal[0].totalAcessosMensal);
                $('#acessosAnual').html(dados.anual[0].totalAcessosAnual);

                // DADOS DE ACESSO MOBILE
                $('#acessosMobile').html(dados.dispositivo_mobile[0].totalAcessosMobile);
                $('#acessosMobileIphone').html(dados.dispositivo_mobileIphone[0].totalAcessosMobileIphone);
                $('#acessosMobileAndroid').html(dados.dispositivo_mobileAndroid[0].totalAcessosMobileAndroid);
                $('#acessosMobileOpera').html(dados.dispositivo_mobileOpera[0].totalAcessosMobileOpera);
                $('#acessosMobileFirefox').html(dados.dispositivo_mobileFirefox[0].totalAcessosMobileFirefox);
                $('#acessosMobileMiBrowser').html(dados.dispositivo_mobileMiBrowser[0].totalAcessosMobileMiBrowser);
                $('#acessosMobileSafari').html(dados.dispositivo_mobileSafari[0].totalAcessosMobileSafari);
                $('#acessosMobileChrome').html(dados.dispositivo_mobileChrome[0].totalAcessosMobileChrome);

                // DADOS DE ACESSO DESKTOP
                $('#acessosDesktop').html(dados.dispositivo_desktop[0].totalAcessosDesktop);
                $('#acessosPCChrome').html(dados.dispositivo_PCChrome[0].totalAcessosPCChrome);
                $('#acessosPCFirefox').html(dados.dispositivo_PCFirefox[0].totalAcessosPCFirefox);
                $('#acessosPCExplorer').html(dados.dispositivo_PCExplorer[0].totalAcessosPCExplorer);
                $('#acessosPCOpera').html(dados.dispositivo_PCOpera[0].totalAcessosPCOpera);
                $('#acessosPCSafari').html(dados.dispositivo_PCSafari[0].totalAcessosPCSafari);
                $('#acessosPCWindows').html(dados.dispositivo_PCWindows[0].totalAcessosPCWindows);
                $('#acessosPCLinux').html(dados.dispositivo_PCLinux[0].totalAcessosPCLinux);
                $('#acessosPCMac').html(dados.dispositivo_PCMac[0].totalAcessosPCMac);
            }
        }
    );
}

function transforma_magicamente(s){
              
	function duas_casas(numero){
		if (numero <= 9){
			numero = "0"+numero;
        }
		return numero;
	}

    hora = duas_casas(Math.trunc(s/3600));
    minuto = duas_casas(Math.trunc((s%3600)/60));
    segundo = duas_casas((s%3600)%60);
              
    formatado = hora+":"+minuto+":"+segundo;
              
    return formatado;
 }

function listarRelatoriosLeads(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatoriosLeads/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;
                  
                  for(var i = 0; i < dados.length; i++) {
                   
                    moment.locale('pt-br');

                    if(dados[i].tempo_contador === null)
                    {
                        var tempoOnline = '<span class="text-warning">Usuário Online</span>';

                    } else {

                        var tempoOnline = transforma_magicamente(dados[i].tempo_contador);

                    }
                    
                    if(dados[i].dispositivo_contador === '0')
                    {
                        var dispositivo = '<i class="la la-desktop avatar-title font-22 text-secondary"></i>';

                    } else {

                        var dispositivo = '<i class="mdi mdi-cellphone-android avatar-title font-22 text-secondary"></i>';
                    }
                    
                    $('#tabelaLeadsDiario').append(
        
                        '<tr>' +
                        '<td>'+dados[i].ip_contador+'</td>' +
                        '<td>'+dados[i].cidade_contador+'</td>' +
                        '<td>'+dados[i].estado_contador+'</td>' +
                        '<td>'+dispositivo+'</td>' +
                        '<td>'+dados[i].navegador_contador+'</td>' +
                        '<td>'+dados[i].SO_contador+'</td>' +
                        '<td>'+moment(dados[i].data_contador).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_contador, "H:m").format("H:m")+'</td>' +
                        '<td>'+tempoOnline+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaLeadsDiario').append(
                      '<td colspan="9">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum Gerado hoje'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioDiario(relatorioDiario) {
  
    showModalRelatorioDiario(relatorioDiario);
    
}

function showModalRelatorioDiario(relatorioDiario){
  
    $('#leadsDiario').modal('show');    
    
}

function listarRelatorioSemana(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatorioSemana/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;
                  
                  for(var i = 0; i < dados.length; i++) {
                   
                    moment.locale('pt-br');

                    if(dados[i].tempo_contador === null)
                    {
                        var tempoOnline = '<span class="text-warning">Usuário Online</span>';

                    } else {

                        var tempoOnline = transforma_magicamente(dados[i].tempo_contador);

                    }
                    
                    if(dados[i].dispositivo_contador === '0')
                    {
                        var dispositivo = '<i class="la la-desktop avatar-title font-22 text-secondary"></i>';

                    } else {

                        var dispositivo = '<i class="mdi mdi-cellphone-android avatar-title font-22 text-secondary"></i>';
                    }
                    
                    $('#tabelaLeadsSemana').append(
        
                        '<tr>' +
                        '<td>'+dados[i].ip_contador+'</td>' +
                        '<td>'+dados[i].cidade_contador+'</td>' +
                        '<td>'+dados[i].estado_contador+'</td>' +
                        '<td>'+dispositivo+'</td>' +
                        '<td>'+dados[i].navegador_contador+'</td>' +
                        '<td>'+dados[i].SO_contador+'</td>' +
                        '<td>'+moment(dados[i].data_contador).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_contador, "H:m").format("H:m")+'</td>' +
                        '<td>'+tempoOnline+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaLeadsSemana').append(
                      '<td colspan="9">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum acesso gerado na semana' +
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioSemana(relatorioSemana) {
  
    showModalRelatorioSemana(relatorioSemana);
    
}

function showModalRelatorioSemana(relatorioSemana){
  
    $('#leadsSemana').modal('show');    
    
}

function listarRelatorioMensal(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatorioMensal/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;
                  
                  for(var i = 0; i < dados.length; i++) {
                   
                    moment.locale('pt-br');

                    if(dados[i].tempo_contador === null)
                    {
                        var tempoOnline = '<span class="text-warning">Usuário Online</span>';

                    } else {

                        var tempoOnline = transforma_magicamente(dados[i].tempo_contador);

                    }
                    
                    if(dados[i].dispositivo_contador === '0')
                    {
                        var dispositivo = '<i class="la la-desktop avatar-title font-22 text-secondary"></i>';

                    } else {

                        var dispositivo = '<i class="mdi mdi-cellphone-android avatar-title font-22 text-secondary"></i>';
                    }
                    
                    $('#tabelaLeadsMensal').append(
        
                        '<tr>' +
                        '<td>'+dados[i].ip_contador+'</td>' +
                        '<td>'+dados[i].cidade_contador+'</td>' +
                        '<td>'+dados[i].estado_contador+'</td>' +
                        '<td>'+dispositivo+'</td>' +
                        '<td>'+dados[i].navegador_contador+'</td>' +
                        '<td>'+dados[i].SO_contador+'</td>' +
                        '<td>'+moment(dados[i].data_contador).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_contador, "H:m").format("H:m")+'</td>' +
                        '<td>'+tempoOnline+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {

                    var mesAtual = new Date;
                    var monName = new Array ("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro")
              
                    $('#tabelaLeadsMensal').append(
                      '<td colspan="9">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum acesso gerado no mês de <strong>' +monName[mesAtual.getMonth()]+ '</strong>'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioMensal(relatorioMensal) {
  
    showModalRelatorioMensal(relatorioMensal);
    
}

function showModalRelatorioMensal(relatorioMensal){
  
    $('#leadsMensal').modal('show');    
    
}

function listarRelatorioAnual(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatorioAnual/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;
                  
                  for(var i = 0; i < dados.length; i++) {
                   
                    moment.locale('pt-br');

                    if(dados[i].tempo_contador === null)
                    {
                        var tempoOnline = '<span class="text-warning">Usuário Online</span>';

                    } else {

                        var tempoOnline = transforma_magicamente(dados[i].tempo_contador);

                    }
                    
                    if(dados[i].dispositivo_contador === '0')
                    {
                        var dispositivo = '<i class="la la-desktop avatar-title font-22 text-secondary"></i>';

                    } else {

                        var dispositivo = '<i class="mdi mdi-cellphone-android avatar-title font-22 text-secondary"></i>';
                    }
                    
                    $('#tabelaLeadsAnual').append(
        
                        '<tr>' +
                        '<td>'+dados[i].ip_contador+'</td>' +
                        '<td>'+dados[i].cidade_contador+'</td>' +
                        '<td>'+dados[i].estado_contador+'</td>' +
                        '<td>'+dispositivo+'</td>' +
                        '<td>'+dados[i].navegador_contador+'</td>' +
                        '<td>'+dados[i].SO_contador+'</td>' +
                        '<td>'+moment(dados[i].data_contador).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_contador, "H:m").format("H:m")+'</td>' +
                        '<td>'+tempoOnline+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {

                    var anoAtual = new Date;

                    $('#tabelaLeadsAnual').append(
                      '<td colspan="9">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum acesso gerado no ano de <strong>' +anoAtual.getFullYear()+ '</strong>'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioAnual(relatorioAnual) {
  
    showModalRelatorioAnual(relatorioAnual);
    
}

function showModalRelatorioAnual(relatorioAnual){
  
    $('#leadsAnual').modal('show');    
    
}