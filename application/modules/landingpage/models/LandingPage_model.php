<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class LandingPage_model extends CI_Model {

    public function m_consultaDadosSessao($id)
    {           
        $result =  $this->db->query("SELECT * FROM usuarios WHERE id_usuario = " . $id);

        foreach ($result->result_array() as $dados)
        {
            return $dados;    
        }
    }
    
    public function m_gerarCodigoCriarLandingPage() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_landingPage) as codigo FROM landing_pages");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    public function m_CriarLandingPage($dados)
    {

        $this->db->query("INSERT INTO lp_modeloconfig_head                      (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modeloconfig_logomenuslide             (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico1                 (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico2                 (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico3                 (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico4                 (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico5                 (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico6                 (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico7                 (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico8                 (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_forms                    (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_personalizacao           (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_redessociais             (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico_maps             (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        $this->db->query("INSERT INTO lp_modelosconfig_servico_paginasucesso    (id_modelo, id_landingPage) VALUES (".$dados['template_landingPage'].", ".$dados['id_landingPage'].")");
        
        return $this->db->insert('landing_pages', $dados);
    }

    public function m_listarTodasLandingPages()
    {
        $this->db->select("*");
        $this->db->order_by('id_landingPage', 'DESC');
        $resultado = $this->db->get("landing_pages")->result();

        return $resultado;
    }

    public function buscarLandingPage($id)
    {
        $resultado = $this->db->query("SELECT * FROM landing_pages WHERE id_landingPage =". $id)->result();
        
        return $resultado;
    }

    public function m_listarGrupos()
    {
        $resultado =  $this->db->query("SELECT * FROM newsletter_grupos")->result();

        return $resultado;    

    }

    public function buscarLandingPageToken($token, $id)
    {
        
        $resultado = $this->db->query("SELECT * FROM lp_landing_pages_contador 
                                        WHERE token_contador = '$token' 
                                        AND landingPage_contador = '$id'
                                        LIMIT 1")->result();
        
        return $resultado;
    }
    
    public function buscarConfisgs($id)
    {
        $resultado = $this->db->query("SELECT * FROM configs_landing_pages WHERE id_configs =". $id)->result();
        
        return $resultado;
    }

    public function alterarDadosConfigs($dados = NULL, $id = NULL)
    {  
        
        if ($dados != NULL && $id != NULL){
            return $this->db->update('configs_landing_pages', $dados, array('id_configs'=>$id));
        }
    }

    public function m_atualizarStatusLaingPage($dados)
    {
        
        $resultado = $this->db->query("UPDATE landing_pages SET status_landingPage = '".$dados['status_landingPage']."' WHERE id_landingPage = '".$dados['id_landingPage']."';");
        
        return $resultado;
    }

    public function m_tempoOnline($dados)
    {
        
        $resultado = $this->db->query("UPDATE lp_landing_pages_contador 
                                        SET tempo_contador = '".$dados['tempo_contador']."' 
                                        WHERE token_contador = '".$dados['token_contador']."'
                                        AND landingPage_contador = ".$dados['landingPage_contador']);
        
        return $resultado;
    
    }

    public function m_excluirLadingPage($id)
    {
        $this->db->query("DELETE FROM lp_modeloconfig_head                      WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modeloconfig_logomenuslide             WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico1                 WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico2                 WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico3                 WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico4                 WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico5                 WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico6                 WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico7                 WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico8                 WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_forms                    WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_personalizacao           WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_redessociais             WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico_maps             WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_modelosconfig_servico_paginasucesso    WHERE id_landingPage = '".$id."';");
        $this->db->query("DELETE FROM lp_landing_pages_contador                 WHERE landingPage_contador = '".$id."';");
        
        $resultado = $this->db->query("DELETE FROM landing_pages WHERE id_landingPage = '".$id."';");
        
        return $resultado;
        
    }

    public function m_atualizarDadosLanding($dados)
    {
        
        $resultado = $this->db->query("UPDATE landing_pages SET 
                                                            nome_landingPage        = '".$dados['nome_landingPage']."',
                                                            titulo_landingPage      = '".$dados['titulo_landingPage']."',
                                                            link_landingPage        = '".$dados['link_landingPage']."',
                                                            grupoLista_landingPage  = '".$dados['grupoLista_landingPage']."' 
                                                            
                                                            WHERE id_landingPage    = '".$dados['id_landingPage']."';");
        
        return $resultado;
    }
    
    public function m_contadorDeVisitas($dados)
    {
        return $this->db->insert('lp_landing_pages_contador', $dados);
    }

    public function m_buscarContadorToken($tokenContador, $landingPageID)
    {
        
        $resultado = $this->db->query("SELECT * FROM    lp_landing_pages_contador 
                                                WHERE   token_contador = '".$tokenContador."' 
                                                AND     landingPage_contador = '".$landingPageID."';")->result();

        return $resultado;
    }

    public function m_listarRelatorioAcessos($id)
    {

        $dataInicial = date('Ymd');
        $dateFinal = date('Ymd');
        
        $resultado['diario'] = $this->db->query("SELECT count(id_contador) AS totalAcessosDiario 
                                                    FROM lp_landing_pages_contador 
                                                    WHERE data_contador >= ".$dataInicial." 
                                                    AND data_contador <= ".$dateFinal."
                                                    AND landingPage_contador = ".$id)->result();

        $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $resultado['semanal'] = $this->db->query("SELECT count(id_contador) AS totalAcessosSemanal
                                                    FROM lp_landing_pages_contador  
                                                    WHERE data_contador >= ".$primeiroDiaDaSemana." 
                                                    AND data_contador <= ".$ultimoDiaDaSemana."
                                                    AND landingPage_contador = ".$id)->result();

        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));
        
        $primeiroDiaDoMes = date('Ymd',$data_incio);
        $ultimoDiaDoMes = date('Ymd',$data_fim);
        
        $resultado['mensal'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMensal
                                                    FROM lp_landing_pages_contador 
                                                    WHERE data_contador >= ".$primeiroDiaDoMes." 
                                                    AND data_contador <= ".$ultimoDiaDoMes." 
                                                    AND landingPage_contador <= ".$id)->result();

        $primeiroDiaDoAno = date('Y').'0101';
        $ultimoDiaDoAno = date('Y').'1231';

        $resultado['anual'] = $this->db->query("SELECT count(id_contador) AS totalAcessosAnual
                                                FROM lp_landing_pages_contador  
                                                WHERE data_contador >= ".$primeiroDiaDoAno." 
                                                AND data_contador <= ".$ultimoDiaDoAno."
                                                AND landingPage_contador <= ".$id)->result();

        $resultado['dispositivo_desktop'] = $this->db->query("SELECT count(id_contador) AS totalAcessosDesktop
                                                                FROM lp_landing_pages_contador  
                                                                WHERE dispositivo_contador = '0'
                                                                AND landingPage_contador = ".$id)->result();

        $resultado['dispositivo_mobile'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMobile
                                                                FROM lp_landing_pages_contador  
                                                                WHERE dispositivo_contador = '1'
                                                                AND landingPage_contador = ".$id)->result();
                                                                
        $resultado['dispositivo_mobileSafari'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMobileSafari
                                                                    FROM lp_landing_pages_contador  
                                                                    WHERE dispositivo_contador = '1'
                                                                    AND navegador_contador = 'Safari'
                                                                    AND landingPage_contador = ".$id)->result();
                                                                    
        $resultado['dispositivo_mobileChrome'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMobileChrome
                                                                    FROM lp_landing_pages_contador  
                                                                    WHERE dispositivo_contador = '1'
                                                                    AND navegador_contador = 'Chrome'
                                                                    AND landingPage_contador = ".$id)->result();
                                                                    
        $resultado['dispositivo_mobileMiBrowser'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMobileMiBrowser
                                                                        FROM lp_landing_pages_contador  
                                                                        WHERE dispositivo_contador = '1'
                                                                        AND navegador_contador = 'MiBrowser'
                                                                        AND landingPage_contador =  '$id'")->result();
                                                                    
        $resultado['dispositivo_mobileFirefox'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMobileFirefox
                                                                    FROM lp_landing_pages_contador  
                                                                    WHERE dispositivo_contador = '1'
                                                                    AND navegador_contador = 'Firefox'
                                                                    AND landingPage_contador = ".$id)->result();
                                                                    
        $resultado['dispositivo_mobileOpera'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMobileOpera
                                                                    FROM lp_landing_pages_contador  
                                                                    WHERE dispositivo_contador = '1'
                                                                    AND navegador_contador = 'Opera'
                                                                    AND landingPage_contador = ".$id)->result();

        $resultado['dispositivo_mobileAndroid'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMobileAndroid
                                                                    FROM lp_landing_pages_contador  
                                                                    WHERE dispositivo_contador = '1'
                                                                    AND SO_contador = 'Android'
                                                                    AND landingPage_contador = ".$id)->result();

        $resultado['dispositivo_mobileIphone'] = $this->db->query("SELECT count(id_contador) AS totalAcessosMobileIphone
                                                                    FROM lp_landing_pages_contador  
                                                                    WHERE dispositivo_contador = '1'
                                                                    AND SO_contador = 'Iphone'
                                                                    AND landingPage_contador = ".$id)->result();    

        $resultado['dispositivo_PCOpera'] = $this->db->query("SELECT count(id_contador) AS totalAcessosPCOpera
                                                                FROM lp_landing_pages_contador  
                                                                WHERE dispositivo_contador = '0'
                                                                AND navegador_contador = 'Opera'
                                                                AND landingPage_contador = ".$id)->result();   

        $resultado['dispositivo_PCExplorer'] = $this->db->query("SELECT count(id_contador) AS totalAcessosPCExplorer
                                                                FROM lp_landing_pages_contador  
                                                                WHERE navegador_contador in ('Edge', 'Explorer')
                                                                AND  dispositivo_contador = '0'
                                                                AND landingPage_contador = ".$id)->result();
                                                                
        $resultado['dispositivo_PCChrome'] = $this->db->query("SELECT count(id_contador) AS totalAcessosPCChrome
                                                                FROM lp_landing_pages_contador  
                                                                WHERE dispositivo_contador = '0'
                                                                AND navegador_contador = 'Chrome'
                                                                AND landingPage_contador = ".$id)->result();
        
        $resultado['dispositivo_PCFirefox'] = $this->db->query("SELECT count(id_contador) AS totalAcessosPCFirefox
                                                                FROM lp_landing_pages_contador  
                                                                WHERE dispositivo_contador = '0'
                                                                AND navegador_contador = 'Firefox'
                                                                AND landingPage_contador = ".$id)->result();    

        $resultado['dispositivo_PCSafari'] = $this->db->query("SELECT count(id_contador) AS totalAcessosPCSafari
                                                            FROM lp_landing_pages_contador  
                                                            WHERE dispositivo_contador = '0'
                                                            AND SO_contador = 'Mac'
                                                            AND navegador_contador = 'Safari'
                                                            AND landingPage_contador = ".$id)->result();

        $resultado['dispositivo_PCWindows'] = $this->db->query("SELECT count(id_contador) AS totalAcessosPCWindows
                                                                FROM lp_landing_pages_contador  
                                                                WHERE dispositivo_contador = '0'
                                                                AND SO_contador = 'Windows'
                                                                AND landingPage_contador = ".$id)->result(); 
                                                                
        $resultado['dispositivo_PCLinux'] = $this->db->query("SELECT count(id_contador) AS totalAcessosPCLinux
                                                                FROM lp_landing_pages_contador  
                                                                WHERE dispositivo_contador = '0'
                                                                AND SO_contador = 'Linux'
                                                                AND landingPage_contador = ".$id)->result();                                                         

        $resultado['dispositivo_PCMac'] = $this->db->query("SELECT count(id_contador) AS totalAcessosPCMac
                                                                FROM lp_landing_pages_contador  
                                                                WHERE dispositivo_contador = '0'
                                                                AND SO_contador = 'Mac'
                                                                AND landingPage_contador = ".$id)->result(); 

        return $resultado;

    }
    
    public function m_listarDadosLandingPages()
    {
        $dataInicial = date('Ymd');
        $dateFinal = date('Ymd');
        
        $resultado['acessosDoDia'] = $this->db->query("SELECT count(id_contador) AS totalAcessosdoDia 
                                                        FROM lp_landing_pages_contador 
                                                        WHERE data_contador >= ".$dataInicial." 
                                                        AND data_contador <= ".$dateFinal)->result();

        $resultado['Visitas'] = $this->db->query("SELECT count(id_contador) AS totalVisitas
                                                                FROM lp_landing_pages_contador;")->result(); 

        $resultado['totalVisitasMobile'] = $this->db->query("SELECT count(id_contador) AS totalVisitasMobile
                                                FROM lp_landing_pages_contador
                                                WHERE dispositivo_contador = '1';")->result();    

        $resultado['totalVisitasDesktop'] = $this->db->query("SELECT count(id_contador) AS totalVisitasDesktop
                                                    FROM lp_landing_pages_contador
                                                    WHERE dispositivo_contador = '0';")->result();

        $resultado['totalPaginasAtivas'] = $this->db->query("SELECT count(id_landingPage) as toalPaginasAtivas
                                                                FROM landing_pages
                                                                WHERE status_landingPage ='1';")->result();
        return $resultado;
    
    }

    public function m_listarRelatoriosLeads($id)
    {
        
        $dataInicial = date('Ymd');
        $dateFinal = date('Ymd');
        
        $resultado = $this->db->query("SELECT *
                                        FROM lp_landing_pages_contador 
                                        WHERE data_contador >= '".$dataInicial."' 
                                        AND data_contador <= '".$dateFinal."'
                                        AND landingPage_contador = '$id'")->result();
                                        
        return $resultado;
    }

    public function m_listarRelatorioSemana($id)
    {

        $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $resultado = $this->db->query("SELECT *
                                        FROM lp_landing_pages_contador  
                                        WHERE data_contador >= ".$primeiroDiaDaSemana." 
                                        AND data_contador <= ".$ultimoDiaDaSemana."
                                        AND landingPage_contador = ".$id)->result();
                                        
        return $resultado;
    }

    public function m_listarRelatorioMensal($id)
    {
        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $primeiroDiaDoMes = date('Ymd',$data_incio);
        $ultimoDiaDoMes = date('Ymd',$data_fim);
        
        $resultado = $this->db->query("SELECT *
                                        FROM lp_landing_pages_contador 
                                        WHERE data_contador >= ".$primeiroDiaDoMes." 
                                        AND data_contador <= ".$ultimoDiaDoMes." 
                                        AND landingPage_contador = ".$id)->result();
                                                    
        return $resultado;
    }

    public function m_listarRelatorioAnual($id)
    {
        $primeiroDiaDoAno = date('Y').'0101';
        $ultimoDiaDoAno = date('Y').'1231';

        $resultado = $this->db->query("SELECT *
                                                FROM lp_landing_pages_contador  
                                                WHERE data_contador >= ".$primeiroDiaDoAno." 
                                                AND data_contador <= ".$ultimoDiaDoAno."
                                                AND landingPage_contador = ".$id)->result();
                                                    
        return $resultado;
    }
    
    public function m_listarRelatoriosCadastros($id)
    {
        $data_cadastros = date('Ymd');
        $data_cadastros = date('Ymd');
        
        $resultado['relatorioCadastroDiario'] = $this->db->query("SELECT count(id_cadastros) AS totalCadadastrosDiario
                                                                    FROM leads_cadastros
                                                                    WHERE landingPage_cadastros = '".$id."'
                                                                    AND data_cadastros >= '".$data_cadastros."'
                                                                    AND data_cadastros <= '".$data_cadastros."';")->result();
        
        $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $resultado['relatorioCadastroSemanal'] = $this->db->query("SELECT count(id_cadastros) AS totalCadadastrosSemanal
                                        FROM leads_cadastros  
                                        WHERE data_cadastros >= ".$primeiroDiaDaSemana." 
                                        AND data_cadastros <= ".$ultimoDiaDaSemana."
                                        AND landingPage_cadastros = ".$id)->result();

        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $primeiroDiaDoMes = date('Ymd',$data_incio);
        $ultimoDiaDoMes = date('Ymd',$data_fim);
        
        $resultado['relatorioCadastroMensal'] = $this->db->query("SELECT count(id_cadastros) AS totalCadadastrosMensal
                                                                                FROM leads_cadastros 
                                                                                WHERE data_cadastros >= ".$primeiroDiaDoMes." 
                                                                                AND data_cadastros <= ".$ultimoDiaDoMes." 
                                                                                AND landingPage_cadastros = ".$id)->result();

        $primeiroDiaDoAno = date('Y').'0101';
        $ultimoDiaDoAno = date('Y').'1231';
                                                                        
        $resultado['relatorioCadastroAnual'] = $this->db->query("SELECT count(id_cadastros) AS totalCadadastrosAnual
                                    FROM leads_cadastros  
                                    WHERE data_cadastros >= ".$primeiroDiaDoAno." 
                                    AND data_cadastros <= ".$ultimoDiaDoAno."
                                    AND landingPage_cadastros = ".$id)->result();
                                                    
        return $resultado;
    }

    public function m_listarRelatorioCadastrosDiario($id)
    {
        $dataInicial = date('Ymd');
        $dateFinal = date('Ymd');
        
        $resultado = $this->db->query(
                                    "SELECT   a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                                a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                                a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                                b.id_landingPage, b.nome_landingPage,
                                                c.id_grupos, c.nome_grupos
                                    FROM leads_cadastros as a
                                    LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                    LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                    WHERE data_cadastros >= '".$dataInicial."' 
                                    AND data_cadastros <= '".$dateFinal."'
                                    AND landingPage_cadastros = '".$id."'")->result();                              
        return $resultado;
    }
    
    public function m_listarRelatorioCadastrosSemanal($id)
    {
        $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $resultado = $this->db->query(
                                    "SELECT   a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                                a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                                a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                                b.id_landingPage, b.nome_landingPage,
                                                c.id_grupos, c.nome_grupos
                                    FROM leads_cadastros as a
                                    LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                    LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                    WHERE data_cadastros >= '".$primeiroDiaDaSemana."' 
                                    AND data_cadastros <= '".$ultimoDiaDaSemana."'
                                    AND landingPage_cadastros = '".$id."'")->result();                              
        return $resultado;
    }

    public function m_listarRelatorioCadastrosMensal($id)
    {

        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $primeiroDiaDoMes = date('Ymd',$data_incio);
        $ultimoDiaDoMes = date('Ymd',$data_fim);
        
        $resultado = $this->db->query(
                                        "SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                            a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                            a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                            b.id_landingPage, b.nome_landingPage,
                                            c.id_grupos, c.nome_grupos
                                            FROM leads_cadastros as a
                                            LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                            LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                            WHERE data_cadastros >= ".$primeiroDiaDoMes." 
                                            AND data_cadastros <= ".$ultimoDiaDoMes." 
                                            AND landingPage_cadastros = ".$id)->result();                             
        return $resultado;
    
    }

    public function m_listarRelatorioCadastrosAnual($id)
    {

        $primeiroDiaDoAno = date('Y').'0101';
        $ultimoDiaDoAno = date('Y').'1231';
                                                                        
        $resultado = $this->db->query(
                                        "SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE data_cadastros >= ".$primeiroDiaDoAno." 
                                        AND data_cadastros <= ".$ultimoDiaDoAno."
                                        AND landingPage_cadastros = ".$id)->result();
        
        return $resultado;
    }
    
    public function m_listarRelatorioCadastrosFacebook($id)
    {
                                                                  
        $resultado = $this->db->query("SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE ondeNosConheceu_cadastros = 'Facebook'")->result();
        
        return $resultado;
    }

    public function m_listarRelatorioCadastrosInstagram($id)
    {
                                                                  
        $resultado = $this->db->query("SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE ondeNosConheceu_cadastros = 'Instagram'")->result();
        
        return $resultado;
    }

    public function m_listarRelatorioCadastrosWhatsApp($id)
    {
                                                                  
        $resultado = $this->db->query("SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE ondeNosConheceu_cadastros = 'WhatsApp'")->result();
        
        return $resultado;
    }

    public function m_listarRelatorioCadastrosGoogle($id)
    {
                                                                  
        $resultado = $this->db->query("SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE ondeNosConheceu_cadastros = 'Google'")->result();
        
        return $resultado;
    }

    public function m_listarRelatorioCadastrosYouTube($id)
    {
                                                                  
        $resultado = $this->db->query("SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE ondeNosConheceu_cadastros = 'YouTube'")->result();
        
        return $resultado;
    }

    public function m_listarRelatorioCadastrosMessenger($id)
    {
                                                                  
        $resultado = $this->db->query("SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE ondeNosConheceu_cadastros = 'Messenger'")->result();
        
        return $resultado;
    }

    public function m_listarRelatorioCadastrosAmigos($id)
    {
                                                                  
        $resultado = $this->db->query("SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE ondeNosConheceu_cadastros = 'Amigos'")->result();
        
        return $resultado;
    }
    
    public function m_listarRelatorioCadastrosOutros($id)
    {
                                                                  
        $resultado = $this->db->query("SELECT a.id_cadastros, a.nome_cadastros, a.telefone_cadastros,
                                        a.email_cadastros, a.ondeNosConheceu_cadastros, a.cidade_cadastros,
                                        a.estado_cadastros, a.data_cadastros, a.hora_cadastros,
                                        b.id_landingPage, b.nome_landingPage,
                                        c.id_grupos, c.nome_grupos
                                        FROM leads_cadastros as a
                                        LEFT JOIN landing_pages as b on a.landingPage_cadastros = b.id_landingPage
                                        LEFT JOIN newsletter_grupos as c on c.id_grupos = b.id_landingPage
                                        WHERE ondeNosConheceu_cadastros = 'Outros'")->result();
        
        return $resultado;
    }
    
    public function m_listarRelatorioCadastrosTotais($id)
    {
                                                                  
        $resultado['totalCadastrosFacebook'] = $this->db->query(
            "SELECT count(id_cadastros) AS totalFacebook 
                FROM leads_cadastros 
                WHERE ondeNosConheceu_cadastros = 'Facebook'
                AND landingPage_cadastros = ".$id)->result();
    
        $resultado['totalCadastrosInstagram'] = $this->db->query(
            "SELECT count(id_cadastros) AS totalInstagram 
                FROM leads_cadastros 
                WHERE ondeNosConheceu_cadastros = 'Instagram'
                AND landingPage_cadastros = ".$id)->result();

        $resultado['totalCadastrosWhatsApp'] = $this->db->query(
            "SELECT count(id_cadastros) AS totalWhatsApp 
                FROM leads_cadastros 
                WHERE ondeNosConheceu_cadastros = 'WhatsApp'
                AND landingPage_cadastros = ".$id)->result();

        $resultado['totalCadastrosGoogle'] = $this->db->query(
            "SELECT count(id_cadastros) AS totalGoogle
                FROM leads_cadastros 
                WHERE ondeNosConheceu_cadastros = 'Google'
                AND landingPage_cadastros = ".$id)->result();
        
        $resultado['totalCadastrosYouTube'] = $this->db->query(
            "SELECT count(id_cadastros) AS totalYouTube
                FROM leads_cadastros 
                WHERE ondeNosConheceu_cadastros = 'YouTube'
                AND landingPage_cadastros = ".$id)->result();

        $resultado['totalCadastrosMessenger'] = $this->db->query(
            "SELECT count(id_cadastros) AS totalMessenger
                FROM leads_cadastros 
                WHERE ondeNosConheceu_cadastros = 'Messenger'
                AND landingPage_cadastros = ".$id)->result();

        $resultado['totalCadastrosAmigos'] = $this->db->query(
            "SELECT count(id_cadastros) AS totalAmigos
                FROM leads_cadastros 
                WHERE ondeNosConheceu_cadastros = 'Amigos'
                AND landingPage_cadastros = ".$id)->result();

        $resultado['totalCadastrosOutros'] = $this->db->query(
            "SELECT count(id_cadastros) AS totalOutros
                FROM leads_cadastros 
                WHERE ondeNosConheceu_cadastros = 'Outros'
                AND landingPage_cadastros = ".$id)->result();
                
        return $resultado;
    }
    
    public function m_gerarCodigoLeadsCadastros() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_cadastros) as codigo FROM leads_cadastros");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    public function m_gerarCodigoEmail() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_emails) as codigo FROM newsletter_emails");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;

        }
        
    }

    public function m_leadsCadastroForm($dados)
    {
        $data['id_emails'] = $this->m_gerarCodigoEmail();
        $data['news_nome'] = $dados['nome_cadastros'];
        $data['news_emails'] = $dados['email_cadastros'];
        $data['news_status'] =  "1";
        $data['news_grupoLista'] = $dados['grupoLista_cadastros'];
        
        if($this->db->insert('newsletter_emails', $data))
        {
            $GrupoEmailxGrupo['id_grupos'] = $data['news_grupoLista'];
            $GrupoEmailxGrupo['id_emails'] = $data['id_emails'];

            $this->db->insert('newsletter_grupos_x_emails', $GrupoEmailxGrupo);
    
            return $this->db->insert('leads_cadastros', $dados);
        }
        
    }

    public function m_gerarCodigoClicks() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_clicks) as codigo FROM lp_landing_pages_clicks");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    public function buscarLandingPageClicksToken($token, $id, $nomeBotao)
    {
        
        $resultado = $this->db->query("SELECT * FROM lp_landing_pages_clicks 
                                        WHERE token_clicks = '$token' 
                                        AND idLandingPage_clicks = '$id'
                                        AND nomeBotao_clicks = '$nomeBotao'
                                        LIMIT 1")->result();
        return $resultado;
    }

    public function m_clicks($dados)
    {
        
        return $this->db->insert('lp_landing_pages_clicks', $dados);
        
        return $resultado;

    }

    public function m_listarRelatoriosClicks($id)
    {

        $dataInicial = date('Ymd');
        $dateFinal = date('Ymd');
        
        $resultado['clicks_diario'] = $this->db->query("SELECT count(id_clicks) AS totalClicksDiario 
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE dataClick__clicks >= ".$dataInicial." 
                                                    AND dataClick__clicks <= ".$dateFinal."
                                                    AND idLandingPage_clicks = ".$id)->result();  
        
        $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $resultado['clicks_semanal'] = $this->db->query("SELECT count(id_clicks) AS totalClicksSemanal
                                                    FROM lp_landing_pages_clicks  
                                                    WHERE dataClick__clicks >= ".$primeiroDiaDaSemana." 
                                                    AND dataClick__clicks <= ".$ultimoDiaDaSemana."
                                                    AND idLandingPage_clicks = ".$id)->result();    
                                                    
        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));
        
        $primeiroDiaDoMes = date('Ymd',$data_incio);
        $ultimoDiaDoMes = date('Ymd',$data_fim);
        
        $resultado['clicks_mensal'] = $this->db->query("SELECT count(id_clicks) AS totalClicksMensal
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE dataClick__clicks >= ".$primeiroDiaDoMes." 
                                                    AND dataClick__clicks <= ".$ultimoDiaDoMes." 
                                                    AND idLandingPage_clicks <= ".$id)->result();

        $primeiroDiaDoAno = date('Y').'0101';
        $ultimoDiaDoAno = date('Y').'1231';

        $resultado['clicks_anual'] = $this->db->query("SELECT count(id_clicks) AS totalClicksAnual
                                                FROM lp_landing_pages_clicks  
                                                WHERE dataClick__clicks >= ".$primeiroDiaDoAno." 
                                                AND dataClick__clicks <= ".$ultimoDiaDoAno."
                                                AND idLandingPage_clicks <= ".$id)->result(); 
                                                
        return $resultado;
    
    }
    
    public function m_listarRelatorioClicksDiario($id)
    {

        $dataInicial = date('Ymd');
        $dateFinal = date('Ymd');
        
        $resultado = $this->db->query(
                                "SELECT 
                                    idLandingPage_clicks,  token_clicks, nomeBotao_clicks, 
                                    linkBotao_clicks, dataClick__clicks, horaClick__clicks,
                                    nome_landingPage
                                FROM lp_landing_pages_clicks as a
                                LEFT JOIN landing_pages as b on a.idLandingPage_clicks = b.id_landingPage
                                WHERE dataClick__clicks >= '".$dataInicial."'
                                AND dataClick__clicks <= '".$dateFinal."'
                                AND idLandingPage_clicks = '".$id."'")->result(); 
                                

        return $resultado;
    
    }

    public function m_listarRelatorioClicksSemanal($id)
    {

        $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $resultado = $this->db->query(
                            "SELECT 
                                idLandingPage_clicks,  token_clicks, nomeBotao_clicks, 
                                linkBotao_clicks, dataClick__clicks, horaClick__clicks,
                                nome_landingPage
                            FROM lp_landing_pages_clicks as a
                            LEFT JOIN landing_pages as b on a.idLandingPage_clicks = b.id_landingPage
                            WHERE dataClick__clicks >= '".$primeiroDiaDaSemana."'
                            AND dataClick__clicks <= '".$ultimoDiaDaSemana."'
                            AND idLandingPage_clicks = '".$id."'")->result();                           
        return $resultado;
    
    }

    public function m_listarRelatorioClicksMensal($id)
    {
        
        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $primeiroDiaDoMes = date('Ymd',$data_incio);
        $ultimoDiaDoMes = date('Ymd',$data_fim);
        
        $resultado = $this->db->query("SELECT 
                                            idLandingPage_clicks,  token_clicks, nomeBotao_clicks, 
                                            linkBotao_clicks, dataClick__clicks, horaClick__clicks,
                                            nome_landingPage
                                            FROM lp_landing_pages_clicks as a
                                        LEFT JOIN landing_pages as b on a.idLandingPage_clicks = b.id_landingPage
                                        WHERE dataClick__clicks >= ".$primeiroDiaDoMes." 
                                        AND dataClick__clicks <= ".$ultimoDiaDoMes." 
                                        AND idLandingPage_clicks = ".$id)->result();
                                                    
        return $resultado;

    }

    public function m_listarRelatorioClicksAnual($id)
    {

        $primeiroDiaDoAno = date('Y').'0101';
        $ultimoDiaDoAno = date('Y').'1231';

        $resultado = $this->db->query("SELECT 
                                            idLandingPage_clicks,  token_clicks, nomeBotao_clicks, 
                                            linkBotao_clicks, dataClick__clicks, horaClick__clicks,
                                            nome_landingPage
                                            FROM lp_landing_pages_clicks as a
                                            LEFT JOIN landing_pages as b on a.idLandingPage_clicks = b.id_landingPage 
                                            WHERE dataClick__clicks >= ".$primeiroDiaDoAno." 
                                            AND dataClick__clicks <= ".$ultimoDiaDoAno."
                                            AND idLandingPage_clicks = ".$id)->result();
                                                    
        return $resultado;
    
    }

    public function m_listarRelatorioClicksBotoes($id)
    {
        
        $resultado['clicks_whatsApp1Lateral'] = $this->db->query(
                                                "SELECT count(id_clicks) AS totalClicksWhatsApp1Lateral 
                                                FROM lp_landing_pages_clicks 
                                                WHERE idLandingPage_clicks = '".$id."'
                                                AND nomeBotao_clicks = 'botaoLateralWhatsApp1'")->result();
        
        $resultado['clicks_whatsApp2Lateral'] = $this->db->query(
                                                "SELECT count(id_clicks) AS totalClicksWhatsApp2Lateral 
                                                FROM lp_landing_pages_clicks 
                                                WHERE idLandingPage_clicks = '".$id."'
                                                AND nomeBotao_clicks = 'botaoLateralWhatsApp2'")->result();
                                                
        $resultado['clicks_whatsApp1Rodape'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksWhatsApp1Rodape 
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoRodapeWhatsApp1'")->result();

        $resultado['clicks_whatsApp2Rodape'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksWhatsApp2Rodape 
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoRodapeWhatsApp2'")->result();
        
        $resultado['clicks_botaoRodapeFacebook'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoRodapeFacebook 
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoRodapeFacebook'")->result();
        
        $resultado['clicks_botaoRodapeInstagram'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoRodapeInstagram 
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoRodapeInstagram'")->result();
                                                    
        $resultado['clicks_botaoRodapeTelefone1'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoRodapeTelefone1 
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoRodapeTelefone1'")->result();
                                                    
        $resultado['clicks_botaoRodapeTelefone2'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoRodapeTelefone2 
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoRodapeTelefone2'")->result();
    
        $resultado['clicks_botaoLogo'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoLogo
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'BotaoLogo'")->result();
                                                    
        $resultado['clicks_botaoSlide'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoSlide
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'BotaoSlide'")->result();

        $resultado['clicks_linkRodapeSite'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksLinkRodapeSite
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'linkRodapeSite'")->result();
                                                    
        $resultado['clicks_botaoRedesSociais1'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoRedesSociais1
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'BotaoRedesSociais1'")->result();

        $resultado['clicks_botaoRedesSociais2'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoRedesSociais2
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'BotaoRedesSociais2'")->result();

        $resultado['clicks_botaoRedesSociais3'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoRedesSociais3
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'BotaoRedesSociais3'")->result();
                                                    
        $resultado['clicks_botaoServico2'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico2
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico2'")->result();
                                                    
        $resultado['clicks_botaoServico4'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico4
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico4'")->result();

        $resultado['clicks_botaoServico5opc1'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico5opc1
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico5opc1'")->result();

        $resultado['clicks_botaoServico5opc2'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico5opc2
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico5opc2'")->result();

        $resultado['clicks_botaoServico5opc3'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico5opc3
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico5opc3'")->result();

        $resultado['clicks_botaoServico5opc4'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico5opc4
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico5opc4'")->result();

        $resultado['clicks_botaoServico5opc5'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico5opc5
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico5opc5'")->result();

        $resultado['clicks_botaoServico5opc6'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico5opc6
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico5opc6'")->result();

        $resultado['clicks_botaoServico5opc7'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico5opc7
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico5opc7'")->result();

        $resultado['clicks_botaoServico5opc8'] = $this->db->query(
                                                    "SELECT count(id_clicks) AS totalClicksBotaoServico5opc8
                                                    FROM lp_landing_pages_clicks 
                                                    WHERE idLandingPage_clicks = '".$id."'
                                                    AND nomeBotao_clicks = 'botaoServico5opc8'")->result();
                                                    
        return $resultado;
        
    }

}