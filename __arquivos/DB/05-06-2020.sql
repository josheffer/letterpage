-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.10.10    Database: letterpage
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configs_landing_pages`
--

DROP TABLE IF EXISTS `configs_landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configs_landing_pages` (
  `id_configs` int(11) NOT NULL,
  `fone1_landingPage` varchar(45) DEFAULT NULL,
  `fone2_landingPage` varchar(45) DEFAULT NULL,
  `fone1TextoWhats_landingPage` varchar(256) DEFAULT NULL,
  `fone2TextoWhats_landingPage` varchar(256) DEFAULT NULL,
  `verificaWhatsapp1_landingPage` enum('0','1') DEFAULT '1',
  `verificaWhatsapp2_landingPage` enum('0','1') DEFAULT '1',
  `linkFacebook_landingPage` varchar(60) DEFAULT NULL,
  `linkInstagram_landingPage` varchar(60) DEFAULT NULL,
  `linkSite_landingPage` varchar(60) DEFAULT NULL,
  `linkAlternativo1_landingPage` varchar(60) DEFAULT NULL,
  `linkAlternativo2_landingPage` varchar(60) DEFAULT NULL,
  `email_landingPage` varchar(45) DEFAULT NULL,
  `endereco_landingPage` varchar(145) DEFAULT NULL,
  `logo_landingPage` varchar(45) DEFAULT NULL,
  `favicon_landingPage` varchar(45) DEFAULT NULL,
  `googleMaps_landingPage` text,
  PRIMARY KEY (`id_configs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configs_landing_pages`
--

LOCK TABLES `configs_landing_pages` WRITE;
/*!40000 ALTER TABLE `configs_landing_pages` DISABLE KEYS */;
INSERT INTO `configs_landing_pages` VALUES (1,'62993998725','62993998725','Olá, você está falando com o Vendedor 1','Olá, você está falando com o Vendedor 2','1','1','teste','teste','teste.com','','','teste@teste.com','endereco','img_logo_1824337042.jpg','img_favicon_33273439.jpg','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61152.03025844469!2d-49.34097843224888!3d-16.676788807968148!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x16d0acbabdf231e5!2sSupermercado%20Hora%20Extra!5e0!3m2!1spt-BR!2sbr!4v1586979740914!5m2!1spt-BR!2sbr\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>');
/*!40000 ALTER TABLE `configs_landing_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL,
  `razaoSocial_empresa` varchar(145) DEFAULT NULL,
  `nomeFantasia_empresa` varchar(145) DEFAULT NULL,
  `cnpj_empresa` varchar(20) DEFAULT NULL,
  `email_empresa` varchar(120) DEFAULT NULL,
  `telefone_empresa` varchar(15) DEFAULT NULL,
  `nomeResponsavel_empresa` varchar(45) DEFAULT NULL,
  `telefoneResponsavel_empresa` varchar(15) DEFAULT NULL,
  `emailResponsavel_empresa` varchar(120) DEFAULT NULL,
  `cpfResponsavel_empresa` varchar(20) DEFAULT NULL,
  `imagem_empresa` varchar(145) DEFAULT NULL,
  `status_empresa` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`id_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'Conselho regional de farmácia','Conselho Regional de Farmácia','99999999999999','teste@teste.com','99999999999','teste nome responsavel','99999999999','teste2@teste.com','99999999999','img_logo_empresa_242261945.png','1');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landing_pages`
--

DROP TABLE IF EXISTS `landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `landing_pages` (
  `id_landingPage` int(11) NOT NULL AUTO_INCREMENT,
  `nome_landingPage` varchar(145) NOT NULL COMMENT 'Nome para identificação da landing page',
  `titulo_landingPage` varchar(512) NOT NULL COMMENT 'Texto que aparecerá na aba do browser ao abrir a página',
  `status_landingPage` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'Status da Landing Page, 0 = desativada, 1 ativada',
  `template_landingPage` int(11) NOT NULL DEFAULT '1' COMMENT 'Template da landing page, por padrão iniciará com o template 1 (template padrão)',
  `link_landingPage` varchar(512) NOT NULL,
  `grupoLista_landingPage` varchar(45) NOT NULL,
  PRIMARY KEY (`id_landingPage`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landing_pages`
--

LOCK TABLES `landing_pages` WRITE;
/*!40000 ALTER TABLE `landing_pages` DISABLE KEYS */;
INSERT INTO `landing_pages` VALUES (1,'Campanha 1','campanha numero 1','1',1,'http://letterpage.net.br/landingpages/1/campanha-numero-1','1'),(2,'asasd','asdsad','1',1,'http://letterpage.net.br/landingpages/2/asdsad','5'),(3,'asdasdas','dasdasd','1',1,'http://letterpage.net.br/landingpages/3/dasdasd','1');
/*!40000 ALTER TABLE `landing_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_cadastros`
--

DROP TABLE IF EXISTS `leads_cadastros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_cadastros` (
  `id_cadastros` int(11) NOT NULL,
  `nome_cadastros` varchar(145) DEFAULT NULL,
  `telefone_cadastros` varchar(45) DEFAULT NULL,
  `email_cadastros` varchar(90) DEFAULT NULL,
  `ondeNosConheceu_cadastros` varchar(45) DEFAULT NULL,
  `landingPage_cadastros` int(11) DEFAULT NULL,
  `grupoLista_cadastros` int(11) DEFAULT NULL,
  `cidade_cadastros` varchar(45) DEFAULT NULL,
  `estado_cadastros` varchar(45) DEFAULT NULL,
  `data_cadastros` varchar(9) DEFAULT NULL,
  `hora_cadastros` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_cadastros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_cadastros`
--

LOCK TABLES `leads_cadastros` WRITE;
/*!40000 ALTER TABLE `leads_cadastros` DISABLE KEYS */;
INSERT INTO `leads_cadastros` VALUES (1,'dsdfsd','+5542342342342','dfsdfsds@gmail.com','Facebook',1,1,'Aparecida de Goiania','Goias','20200415','1722'),(2,'Josheffer Robert','+5562993998725','josheffer@gmail.com','Facebook',1,1,'Aparecida de Goiania','Goias','20200415','1732'),(3,'asdas','+5512331231231','asdasd@gmail.com','Instagram',1,1,'Aparecida de Goiania','Goias','20200429','1718'),(4,'Kreme','+5562993998725','kremegay@gmail.com','Outros',1,1,'Aparecida de Goiania','Goias','20200514','1122'),(5,'dasfs','+5512341231231','2asdas@gmail.com','Amigos',1,1,'Aparecida de Goiania','Goias','20200514','1659'),(6,'uoeiuweiou','+5512312312312','qwaskdljaslkj@gmail.com','Instagram',1,1,'Aparecida de Goiania','Goias','20200528','1448'),(7,'rtterter','+5523423423423','rdsfsdf@gmail.com','WhatsApp',1,1,'Aparecida de Goiania','Goias','20200528','1449'),(8,'aaaaaaaaa','+5522222222222','aaaaaa@gmail.com','Instagram',1,1,'Aparecida de Goiania','Goias','20200528','1459'),(9,'wwww','+5512123123123','3123123@gmail.com','WhatsApp',1,1,'Goiania','Goias','20200528','1513'),(10,'sssssss','+552222','ssss@gmail.com','Google',1,1,'Aparecida de Goiania','Goias','20200528','1514');
/*!40000 ALTER TABLE `leads_cadastros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_landing_pages_clicks`
--

DROP TABLE IF EXISTS `lp_landing_pages_clicks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_landing_pages_clicks` (
  `id_clicks` int(11) NOT NULL,
  `idLandingPage_clicks` int(11) DEFAULT NULL,
  `token_clicks` varchar(90) DEFAULT NULL,
  `nomeBotao_clicks` varchar(45) DEFAULT NULL,
  `linkBotao_clicks` varchar(145) DEFAULT NULL,
  `dataClick__clicks` varchar(45) DEFAULT NULL,
  `horaClick__clicks` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_clicks`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_landing_pages_clicks`
--

LOCK TABLES `lp_landing_pages_clicks` WRITE;
/*!40000 ALTER TABLE `lp_landing_pages_clicks` DISABLE KEYS */;
INSERT INTO `lp_landing_pages_clicks` VALUES (1,1,'cggjl03gb5t6ng9npj15bjqp99q8pqv4','botaoLateralWhatsApp1','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%202','20200415','1735'),(2,1,'cggjl03gb5t6ng9npj15bjqp99q8pqv4','botaoLateralWhatsApp2','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%201','20200415','1736'),(3,1,'cggjl03gb5t6ng9npj15bjqp99q8pqv4','botaoRodapeWhatsApp1','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%201','20200415','1737'),(4,1,'0i71ph9vkgm4d0vfsibj9g0qmrrl4for','botaoRodapeWhatsApp1','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%201','20200514','1145'),(5,1,'0i71ph9vkgm4d0vfsibj9g0qmrrl4for','botaoRodapeWhatsApp2','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%202','20200514','1146'),(6,1,'5padk5kmbaeor7pn51dm6k7nkrvcgrkt','botaoRodapeFacebook','https://www.facebook.com/teste','20200528','1548');
/*!40000 ALTER TABLE `lp_landing_pages_clicks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_landing_pages_contador`
--

DROP TABLE IF EXISTS `lp_landing_pages_contador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_landing_pages_contador` (
  `id_contador` int(11) NOT NULL AUTO_INCREMENT,
  `ip_contador` varchar(45) DEFAULT NULL,
  `cidade_contador` varchar(65) DEFAULT NULL,
  `estado_contador` varchar(45) DEFAULT NULL,
  `pais_contador` varchar(45) DEFAULT NULL,
  `dispositivo_contador` varchar(45) DEFAULT NULL,
  `navegador_contador` varchar(45) DEFAULT NULL,
  `SO_contador` varchar(45) DEFAULT NULL,
  `landingPage_contador` int(11) DEFAULT NULL,
  `data_contador` varchar(45) DEFAULT NULL,
  `hora_contador` varchar(45) DEFAULT NULL,
  `tempo_contador` varchar(45) DEFAULT NULL,
  `token_contador` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id_contador`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_landing_pages_contador`
--

LOCK TABLES `lp_landing_pages_contador` WRITE;
/*!40000 ALTER TABLE `lp_landing_pages_contador` DISABLE KEYS */;
INSERT INTO `lp_landing_pages_contador` VALUES (47,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1647','590','2rbq28cj769js7ikkkl0s5na3oud02e1'),(48,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1657','170','6qk9rq4kvfsths254gaqt7p20nl34otu'),(49,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1700','302','pu0tv75j4k0uqc4e8ongtnpk09l0a9cs'),(50,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1705','406','62lr0soe23vo0fusj7obuugo8kq11f2r'),(51,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1712','329','pdgp1c2hrvfmpchf6o0tv5kecfn2mc6g'),(52,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1718','257','10u7g0tc6r2m5qdsjme4don3o63lvcqo'),(53,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1727',NULL,'8io4noajk9hcf91o03aoc7dd9ltiuaoi'),(54,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1731','373','cggjl03gb5t6ng9npj15bjqp99q8pqv4'),(55,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1738','239','r7uvn760rhvb89mo5st5n6e6cla04hqu'),(56,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1742','208','iv40onrq9f5tuq079u762eam6kjl70eb'),(57,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1746',NULL,'utflb06iulk2upd0ai4l9p2p1bfkivl4'),(58,'177.30.111.114','Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200429','1707','638','5iuih44ddrs7ptr4qij172v0oq4vd587'),(59,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200429','1718','7','0r787dbrs9pca0u02tbk26c2740vr9j1'),(60,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200429','1724',NULL,'gaqejsu3u5alaak8lhc2ikmp4g8q2lo3'),(61,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200514','1119','115','gn2boakv9hac5vd7bjj3b3mj84gemumb'),(62,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200514','1145','178','0i71ph9vkgm4d0vfsibj9g0qmrrl4for'),(63,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200514','1148','42','dh3ov9e0e00ie2gft6nemn677k43a1ia'),(64,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200514','1659','9','bgsci59jivv2hml4lp8h9e5v9r13t73i'),(65,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200515','1505',NULL,'bvjq71sej6sq7o8mvads4jsipqg0gpsb'),(66,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200522','1618','2','mk684ssql3bqtjcl3odll3b85puhmvda'),(67,'177.30.111.114','Goiania','Goias','Brazil','0','Opera','Windows',2,'20200526','1334','6','u25f4ck084bdo6f8cu0l6pli675mao9u'),(68,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200527','1100','4','h6f3gbg3pjfrbn88v6ta7o8e4sekb90u'),(69,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',3,'20200527','1105','3','3ceu504nfkml4m19hm0h5crsgu0cp5ja'),(70,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200527','1115',NULL,'rk1p4tq48vfof6avm890cmpb87s3vqg9'),(71,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',3,'20200527','1115','6','rk1p4tq48vfof6avm890cmpb87s3vqg9'),(72,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',2,'20200527','1115','3','rk1p4tq48vfof6avm890cmpb87s3vqg9'),(73,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200528','1447','668','9s3a4lmkqqud0t6gvnalqgtg6djiuk1m'),(74,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200528','1513','48','mjmps2l2aie6nqs37ta10e2lqvlmudds'),(75,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200528','1548','6321','5padk5kmbaeor7pn51dm6k7nkrvcgrkt'),(76,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200529','2155',NULL,'edghf5q9frratg1c40t68d4t8m8365bp'),(77,'177.30.111.114','Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200602','2047',NULL,'uj5h83gd53fhcr7sueujpme8fvpoi2lc'),(78,'177.30.111.114','Goiania','Goias','Brazil','0','Chrome','Windows',3,'20200603','2352',NULL,'iud8i211sg2ruddm9idhd75qlboojob8'),(79,'177.30.111.114','Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200604','2053',NULL,'jhfsoa6bi249n3tq7j4h9spd39odcqb7');
/*!40000 ALTER TABLE `lp_landing_pages_contador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modeloconfig_head`
--

DROP TABLE IF EXISTS `lp_modeloconfig_head`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modeloconfig_head` (
  `id_modelo` int(11) DEFAULT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `topo_head_css` text,
  `topo_head_js` text,
  `topo_head_SEO_keywords` text,
  `topo_head_SEO_description` text,
  `topo_head_SEO_description2` text,
  `topo_head_SEO_description3` text,
  `topo_head_SEO_imagem` varchar(145) DEFAULT NULL,
  `topo_head_coordenadasMaps` varchar(80) DEFAULT NULL,
  `topo_head_cidade` varchar(80) DEFAULT NULL,
  `topo_head_estado` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modeloconfig_head`
--

LOCK TABLES `lp_modeloconfig_head` WRITE;
/*!40000 ALTER TABLE `lp_modeloconfig_head` DISABLE KEYS */;
INSERT INTO `lp_modeloconfig_head` VALUES (1,1,'','','teste','teste de landingpage','teste de landingpage','teste de landingpage','img_SEO_1190214192.jpg','-49.34097843224888!3d-16.676788807968148','Goiânia, Goiás','GO-BR'),(1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `lp_modeloconfig_head` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modeloconfig_logomenuslide`
--

DROP TABLE IF EXISTS `lp_modeloconfig_logomenuslide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modeloconfig_logomenuslide` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_logo1` enum('0','1') DEFAULT '1',
  `body_logo2` enum('0','1') DEFAULT '1',
  `body_imagemPrincipalSlide` enum('0','1') DEFAULT '1',
  `body_imagemFundoSlide` enum('0','1') DEFAULT '1',
  `body_menu_topo` enum('0','1') DEFAULT '1',
  `body_slide1` enum('0','1') DEFAULT '1',
  `body_slide` enum('0','1') DEFAULT '1',
  `body_slide_titulo1` varchar(145) DEFAULT 'Titulo',
  `body_slide_titulo2` varchar(145) DEFAULT 'Titulo',
  `body_slide_titulo3` varchar(145) DEFAULT 'Titulo',
  `body_slide_descricao1` varchar(256) DEFAULT 'Descrição',
  `body_slide_descricao2` varchar(256) DEFAULT 'Descrição',
  `body_slide_descricao3` varchar(256) DEFAULT 'Descrição',
  `body_slide_imagemFundoSlide` varchar(256) DEFAULT 'banner-bg.png',
  `body_slide_imagem1` varchar(256) DEFAULT 'hero-banner.png',
  `body_slide_imagem2` varchar(256) DEFAULT 'hero-banner.png',
  `body_slide_imagem3` varchar(256) DEFAULT 'hero-banner.png',
  `body_slide_imagem1_SEO` varchar(256) DEFAULT NULL,
  `body_slide_imagem2_SEO` varchar(256) DEFAULT NULL,
  `body_slide_imagem3_SEO` varchar(256) DEFAULT NULL,
  `body_slide_botao1` enum('0','1') DEFAULT '1',
  `body_slide_botao2` enum('0','1') DEFAULT '1',
  `body_slide_botao3` enum('0','1') DEFAULT '1',
  `body_slide_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoLink1` varchar(145) DEFAULT 'www.google.com.br',
  `body_slide_botaoLink2` varchar(145) DEFAULT 'www.google.com.br',
  `body_slide_botaoLink3` varchar(145) DEFAULT 'www.google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modeloconfig_logomenuslide`
--

LOCK TABLES `lp_modeloconfig_logomenuslide` WRITE;
/*!40000 ALTER TABLE `lp_modeloconfig_logomenuslide` DISABLE KEYS */;
INSERT INTO `lp_modeloconfig_logomenuslide` VALUES (1,1,'1','1','1','1','1','1','1','Teste banner 2','Titulo','Titulo','teste banner 2','Descrição','Descrição','banner-bg.png','hero-banner.png','hero-banner.png','hero-banner.png',NULL,'',NULL,'','1','1','Ver +','Ver +','Ver +','www.google.com.br','www.google.com.br','www.google.com.br'),(1,2,'1','1','1','1','1','1','1','Titulo','Titulo','Titulo','Descrição','Descrição','Descrição','banner-bg.png','hero-banner.png','hero-banner.png','hero-banner.png',NULL,NULL,NULL,'1','1','1','Ver +','Ver +','Ver +','www.google.com.br','www.google.com.br','www.google.com.br'),(1,3,'1','1','1','1','1','1','1','Titulo','Titulo','Titulo','Descrição','Descrição','Descrição','banner-bg.png','hero-banner.png','hero-banner.png','hero-banner.png',NULL,NULL,NULL,'1','1','1','Ver +','Ver +','Ver +','www.google.com.br','www.google.com.br','www.google.com.br');
/*!40000 ALTER TABLE `lp_modeloconfig_logomenuslide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_forms`
--

DROP TABLE IF EXISTS `lp_modelosconfig_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_forms` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico1_form1` enum('0','1') DEFAULT '1',
  `body_servico1_form1_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico1_form1_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico1_form1_corFundoForm` varchar(10) DEFAULT '#000000',
  `body_servico1_form1_corTextBotao` varchar(10) DEFAULT '#000000',
  `body_servico1_form2` enum('0','1') DEFAULT '1',
  `body_servico1_form2_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico1_form2_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico1_form2_corFundoForm` varchar(10) DEFAULT '#000000',
  `body_servico1_form2_corTextBotao` varchar(10) DEFAULT '#000000',
  `body_servico2_form3` enum('0','1') DEFAULT '1',
  `body_servico2_form3_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico2_form3_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico2_form3_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico2_form3_corTextBotao` varchar(10) DEFAULT '#000000',
  `body_servico3_form4` enum('0','1') DEFAULT '1',
  `body_servico3_form4_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico3_form4_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico3_form4_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico3_form4_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico4_form5` enum('0','1') DEFAULT '1',
  `body_servico4_form5_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico4_form5_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico4_form5_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico4_form5_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico5_form6` enum('0','1') DEFAULT '1',
  `body_servico5_form6_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico5_form6_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico5_form6_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico5_form6_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico6_form7` enum('0','1') DEFAULT '1',
  `body_servico6_form7_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico6_form7_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico6_form7_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico6_form7_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico7_form8` enum('0','1') DEFAULT '1',
  `body_servico7_form8_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico7_form8_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico7_form8_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico7_form8_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico_RedesSociais_form` enum('0','1') DEFAULT '1',
  `body_servico_RedesSociais_form_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico_RedesSociais_form_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico_RedesSociais_form_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico_RedesSociais_form_corTextBotao` varchar(45) DEFAULT '#000000',
  `nosConheceuOpcao1` varchar(45) DEFAULT 'Facebook',
  `nosConheceuOpcao2` varchar(45) DEFAULT 'Instagram',
  `nosConheceuOpcao3` varchar(45) DEFAULT 'WhatsApp',
  `nosConheceuOpcao4` varchar(45) DEFAULT 'Google',
  `nosConheceuOpcao5` varchar(45) DEFAULT 'YouTube',
  `nosConheceuOpcao6` varchar(45) DEFAULT 'Messenger',
  `nosConheceuOpcao7` varchar(45) DEFAULT 'Amigos',
  `nosConheceuOpcao8` varchar(45) DEFAULT 'Outros',
  `nosConheceuOpcao1_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao2_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao3_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao4_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao5_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao6_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao7_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao8_visivel` enum('0','1') DEFAULT '1',
  `body_servicoForm_tituloForm1` varchar(128) DEFAULT 'Titulo 1',
  `body_servicoForm_descricaoForm1` varchar(512) DEFAULT 'Descrição 1',
  `body_servicoForm_tituloForm2` varchar(128) DEFAULT 'Titulo 2',
  `body_servicoForm_descricaoForm2` varchar(512) DEFAULT 'Descrição 2',
  `body_servicoForm_tituloForm3` varchar(128) DEFAULT 'Titulo 3',
  `body_servicoForm_descricaoForm3` varchar(512) DEFAULT 'Descrição 3',
  `body_servicoForm_tituloForm4` varchar(128) DEFAULT 'Titulo 4',
  `body_servicoForm_descricaoForm4` varchar(512) DEFAULT 'Descrição 4',
  `body_servicoForm_tituloForm5` varchar(128) DEFAULT 'Titulo 5',
  `body_servicoForm_descricaoForm5` varchar(512) DEFAULT 'Descrição 5',
  `body_servicoForm_tituloForm6` varchar(128) DEFAULT 'Titulo 6',
  `body_servicoForm_descricaoForm6` varchar(512) DEFAULT 'Descrição 6',
  `body_servicoForm_tituloForm7` varchar(128) DEFAULT 'Titulo 7',
  `body_servicoForm_descricaoForm7` varchar(512) DEFAULT 'Descrição 7',
  `body_servicoForm_tituloForm8` varchar(128) DEFAULT 'Titulo 8',
  `body_servicoForm_descricaoForm8` varchar(512) DEFAULT 'Descrição 8',
  `body_servicoForm_tituloFormRS` varchar(128) DEFAULT 'Titulo RS',
  `body_servicoForm_descricaoFormRS` varchar(512) DEFAULT 'Descrição RS',
  `body_servico_form1_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form1_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form2_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form2_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form3_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form3_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form4_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form4_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form5_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form5_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form6_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form6_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form7_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form7_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form8_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form8_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_formRS_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_formRS_corDescricao` varchar(10) DEFAULT '#ffffff'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_forms`
--

LOCK TABLES `lp_modelosconfig_forms` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_forms` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_forms` VALUES (1,1,'1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','Facebook','Instagram','WhatsApp','Google','YouTube','Messenger','Amigos','Outros','1','1','1','1','1','1','1','1','Titulo 1','Descrição 1','Titulo 2','Descrição 2','Titulo 3','Descrição 3','Titulo 4','Descrição 4','Titulo 5','Descrição 5','Titulo 6','Descrição 6','Titulo 7','Descrição 7','Titulo 8','Descrição 8','Titulo RS','Descrição RS','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000'),(1,2,'1','#ffffff','Cadastrar','#000000','#000000','1','#ffffff','Cadastrar','#000000','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','Facebook','Instagram','WhatsApp','Google','YouTube','Messenger','Amigos','Outros','1','1','1','1','1','1','1','1','Titulo 1','Descrição 1','Titulo 2','Descrição 2','Titulo 3','Descrição 3','Titulo 4','Descrição 4','Titulo 5','Descrição 5','Titulo 6','Descrição 6','Titulo 7','Descrição 7','Titulo 8','Descrição 8','Titulo RS','Descrição RS','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'),(1,3,'1','#ffffff','Cadastrar','#000000','#000000','1','#ffffff','Cadastrar','#000000','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','Facebook','Instagram','WhatsApp','Google','YouTube','Messenger','Amigos','Outros','1','1','1','1','1','1','1','1','Titulo 1','Descrição 1','Titulo 2','Descrição 2','Titulo 3','Descrição 3','Titulo 4','Descrição 4','Titulo 5','Descrição 5','Titulo 6','Descrição 6','Titulo 7','Descrição 7','Titulo 8','Descrição 8','Titulo RS','Descrição RS','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff');
/*!40000 ALTER TABLE `lp_modelosconfig_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_personalizacao`
--

DROP TABLE IF EXISTS `lp_modelosconfig_personalizacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_personalizacao` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `corFundoMenu` varchar(10) DEFAULT '#000000',
  `corFundoSlide1` varchar(10) DEFAULT '#000000',
  `corFundoSlide` varchar(10) DEFAULT '#000000',
  `corTextoTituloSlide` varchar(10) DEFAULT '#ffffff',
  `corTextoDescricaoSlide` varchar(10) DEFAULT '#ffffff',
  `corTextoBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `corFundoBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `corFundoHoverBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `corTextoHoverBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `servico1_corTituloGenerico1` varchar(10) DEFAULT '#000000',
  `servico1_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico1_corBg` varchar(10) DEFAULT '#ffffff',
  `servico1_corBoxImage` varchar(10) DEFAULT '#ffffff',
  `servico1_corTexto` varchar(10) DEFAULT '#000000',
  `servico2_corTituloGenerico1` varchar(10) DEFAULT '#000000',
  `servico2_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico2_corBg` varchar(10) DEFAULT '#000000',
  `servico2_corTextoBotao` varchar(10) DEFAULT '#ffffff',
  `servico2_corTextoHoverBotao` varchar(10) DEFAULT '#000000',
  `servico2_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico2_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico3_corTituloGenerico1` varchar(10) DEFAULT '#000000',
  `servico3_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico3_corBg` varchar(10) DEFAULT '#000000',
  `servico3_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico3_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico4_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico4_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico4_corBg` varchar(10) DEFAULT '#000000',
  `servico4_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico4_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico4_corTextoBotao` varchar(10) DEFAULT '#ffffff',
  `servico4_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico4_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico4_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico5_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico5_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico5_corBg` varchar(10) DEFAULT '#000000',
  `servico5_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico5_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico5_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico5_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico5_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico5_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico6_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico6_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico6_corBg` varchar(10) DEFAULT '#000000',
  `servico6_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico6_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico6_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico6_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico6_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico6_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico7_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico7_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico7_corBg` varchar(10) DEFAULT '#000000',
  `servico7_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico7_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico7_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico7_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico7_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico7_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico_redesSociais_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico_redesSociais_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corBg` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico_redesSociais_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico_redesSociais_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico_redesSociais_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico_maps_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico_maps_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico_maps_corBg` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoHoverBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoTitulo` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoDescricao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corFundoHoverBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corBG` varchar(10) DEFAULT '#000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_personalizacao`
--

LOCK TABLES `lp_modelosconfig_personalizacao` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_personalizacao` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_personalizacao` VALUES (1,1,'#68ec06','#f8b981','#000000','#ffff00','#ffff00','#000000','#ffffff','#3c3c3c','#a6a6a6','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#aeaeae','#ffffff','#3a3a3a','#ffffff','#ffffff','#000000','#202020','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#999999','#ffffff','#2c2c2c','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#b5b5b5','#ffffff','#2d2d2d','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#ffffff','#585858','#d7151b'),(1,2,'#000000','#000000','#000000','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000'),(1,3,'#000000','#000000','#000000','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000');
/*!40000 ALTER TABLE `lp_modelosconfig_personalizacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_redessociais`
--

DROP TABLE IF EXISTS `lp_modelosconfig_redessociais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_redessociais` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_redesSociais` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico_redesSociais` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo_redesSociais` varchar(45) DEFAULT 'Titulo serviço Redes Sociais',
  `body_textoGenericoDescricao_redesSociais` varchar(512) DEFAULT 'Descrição serviço Redes Sociais',
  `body_redesSociais_opcao1` enum('0','1') DEFAULT '1',
  `body_redesSociais_opcao2` enum('0','1') DEFAULT '1',
  `body_redesSociais_opcao3` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico_redesSociais_Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico_redesSociais_Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico_redesSociais_Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico_redesSociais_Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico_redesSociais_Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico_redesSociais_Descricao1` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico_redesSociais_Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico_redesSociais_Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico_redesSociais_ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico_redesSociais_ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico_redesSociais_ImagemSEO3` varchar(90) DEFAULT 'info SEO3',
  `body_servico_redesSociais_botao1` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botao2` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botao3` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botaoTexto1` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoTexto2` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoTexto3` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico_redesSociais_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico_redesSociais_botaoLink3` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_redessociais`
--

LOCK TABLES `lp_modelosconfig_redessociais` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_redessociais` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_redessociais` VALUES (1,1,'1','1','Titulo serviço Redes Sociais','Descrição serviço Redes Sociais','1','1','1','img_redeSocial_182195700.jpg','img_redeSocial_364774887.jpg','img_redeSocial_686029054.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','descrição serviço 2','descrição serviço 2','descrição serviço 3','info SEO 1','info SEO 2','info SEO3','1','1','1','ver +','ver +','ver +','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço Redes Sociais','Descrição serviço Redes Sociais','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','descrição serviço 2','descrição serviço 2','descrição serviço 3','info SEO 1','info SEO 2','info SEO3','1','1','1','ver +','ver +','ver +','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço Redes Sociais','Descrição serviço Redes Sociais','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','descrição serviço 2','descrição serviço 2','descrição serviço 3','info SEO 1','info SEO 2','info SEO3','1','1','1','ver +','ver +','ver +','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_redessociais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico1`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico1` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico1` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico1` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo1` varchar(45) DEFAULT 'Titulo serviço 1',
  `body_textoGenericoDescricao1` varchar(512) DEFAULT 'Descrição serviço 1',
  `body_servico1_opcao1` enum('0','1') DEFAULT '1',
  `body_servico1_opcao2` enum('0','1') DEFAULT '1',
  `body_servico1_opcao3` enum('0','1') DEFAULT '1',
  `body_servico1_opcao4` enum('0','1') DEFAULT '1',
  `body_servico1_opcao5` enum('0','1') DEFAULT '1',
  `body_servico1_opcao6` enum('0','1') DEFAULT '1',
  `body_servico1_opcao7` enum('0','1') DEFAULT '1',
  `body_servico1_opcao8` enum('0','1') DEFAULT '1',
  `body_servico1Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico1Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico1Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico1Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico1Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico1Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico1Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico1Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico1Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico1Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico1Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico1Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico1Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico1Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico1Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico1Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico1ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico1ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico1ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico1ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico1ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico1ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico1ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico1ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico1_botao1` enum('0','1') DEFAULT '1',
  `body_servico1_botao2` enum('0','1') DEFAULT '1',
  `body_servico1_botao3` enum('0','1') DEFAULT '1',
  `body_servico1_botao4` enum('0','1') DEFAULT '1',
  `body_servico1_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela onde será armazenado os dados da seção de serviços 1';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico1`
--

LOCK TABLES `lp_modelosconfig_servico1` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico1` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico1` VALUES (1,1,'1','1','Titulo serviço 1','Descrição serviço 1','1','1','1','1','1','1','1','1','img_servicos_712412573.jpg','img_servicos_376450579.jpg','img_servicos_1180822558.jpg','img_servicos_1446300572.jpg','img_servicos_977435545.jpg','img_servicos_1704069091.jpg','img_servicos_949986457.jpg','img_servicos_1526723470.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 1','Descrição serviço 1','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 1','Descrição serviço 1','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico2`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico2` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico2` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico2` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo2` varchar(45) DEFAULT 'Titulo serviço 2',
  `body_textoGenericoDescricao2` varchar(512) DEFAULT 'Descrição serviço 2',
  `body_servico2Imagem1` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem2` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem3` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem4` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem5` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem6` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem7` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem8` varchar(145) DEFAULT 'about.png',
  `body_servico2Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico2Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico2Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico2Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico2Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico2Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico2Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico2Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico2Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico2Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico2Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico2Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico2Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico2Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico2Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico2Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico2ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico2ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico2ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico2ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico2ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico2ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico2ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico2ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico2_botao1` enum('0','1') DEFAULT '1',
  `body_servico2_botao2` enum('0','1') DEFAULT '1',
  `body_servico2_botao3` enum('0','1') DEFAULT '1',
  `body_servico2_botao4` enum('0','1') DEFAULT '1',
  `body_servico2_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico2`
--

LOCK TABLES `lp_modelosconfig_servico2` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico2` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico2` VALUES (1,1,'1','1','Titulo serviço 2','Descrição serviço 2','img_servicos_1910013780.jpg','about.png','about.png','about.png','about.png','about.png','about.png','about.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 2','Descrição serviço 2','about.png','about.png','about.png','about.png','about.png','about.png','about.png','about.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 2','Descrição serviço 2','about.png','about.png','about.png','about.png','about.png','about.png','about.png','about.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico3`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico3` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico3` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico3` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo3` varchar(45) DEFAULT 'Titulo serviço 3',
  `body_textoGenericoDescricao3` varchar(512) DEFAULT 'Descrição serviço 3',
  `body_servico3_TodasOpcoes` enum('0','1') DEFAULT '1',
  `body_servico3_opcao1` enum('0','1') DEFAULT '1',
  `body_servico3_opcao2` enum('0','1') DEFAULT '1',
  `body_servico3_opcao3` enum('0','1') DEFAULT '1',
  `body_servico3_opcao4` enum('0','1') DEFAULT '1',
  `body_servico3_opcao5` enum('0','1') DEFAULT '1',
  `body_servico3_opcao6` enum('0','1') DEFAULT '1',
  `body_servico3_opcao7` enum('0','1') DEFAULT '1',
  `body_servico3_opcao8` enum('0','1') DEFAULT '1',
  `body_servico3Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico3Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico3Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico3Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico3Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico3Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico3Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico3Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico3Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico3Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico3Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico3Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico3Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico3Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico3Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico3Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico3ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico3ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico3ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico3ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico3ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico3ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico3ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico3ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico3_botao1` enum('0','1') DEFAULT '1',
  `body_servico3_botao2` enum('0','1') DEFAULT '1',
  `body_servico3_botao3` enum('0','1') DEFAULT '1',
  `body_servico3_botao4` enum('0','1') DEFAULT '1',
  `body_servico3_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico3`
--

LOCK TABLES `lp_modelosconfig_servico3` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico3` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico3` VALUES (1,1,'1','1','Titulo serviço 3','Descrição serviço 3','1','1','1','1','1','1','1','1','1','img_servicos_1941908849.jpg','img_servicos_1846368818.jpg','img_servicos_1073224146.jpg','img_servicos_2143148693.jpg','img_servicos_1144940828.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 3','Descrição serviço 3','1','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 3','Descrição serviço 3','1','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico4`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico4` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico4` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico4` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo4` varchar(45) DEFAULT 'Titulo serviço 4',
  `body_textoGenericoDescricao4` varchar(512) DEFAULT 'Descrição serviço 4',
  `body_servico4Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico4Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico4Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico4Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico4Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico4Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico4Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico4Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico4Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico4Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico4Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico4Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico4Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico4Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico4Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico4Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico4ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico4ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico4ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico4ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico4ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico4ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico4ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico4ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico4_botao1` enum('0','1') DEFAULT '1',
  `body_servico4_botao2` enum('0','1') DEFAULT '1',
  `body_servico4_botao3` enum('0','1') DEFAULT '1',
  `body_servico4_botao4` enum('0','1') DEFAULT '1',
  `body_servico4_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico4`
--

LOCK TABLES `lp_modelosconfig_servico4` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico4` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico4` VALUES (1,1,'1','1','Titulo serviço 4','Descrição serviço 4','img_servicos_24925061.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 4','Descrição serviço 4','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 4','Descrição serviço 4','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico5`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico5` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico5` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico5` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo5` varchar(45) DEFAULT 'Titulo serviço 5',
  `body_textoGenericoDescricao5` varchar(512) DEFAULT 'Descrição serviço 5',
  `body_servico5_opcao1` enum('0','1') DEFAULT '1',
  `body_servico5_opcao2` enum('0','1') DEFAULT '1',
  `body_servico5_opcao3` enum('0','1') DEFAULT '1',
  `body_servico5_opcao4` enum('0','1') DEFAULT '1',
  `body_servico5_opcao5` enum('0','1') DEFAULT '1',
  `body_servico5_opcao6` enum('0','1') DEFAULT '1',
  `body_servico5_opcao7` enum('0','1') DEFAULT '1',
  `body_servico5_opcao8` enum('0','1') DEFAULT '1',
  `body_servico5Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico5Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico5Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico5Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico5Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico5Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico5Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico5Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico5Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico5Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico5Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico5Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico5Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico5Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico5Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico5Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico5ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico5ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico5ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico5ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico5ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico5ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico5ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico5ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico5_botao1` enum('0','1') DEFAULT '1',
  `body_servico5_botao2` enum('0','1') DEFAULT '1',
  `body_servico5_botao3` enum('0','1') DEFAULT '1',
  `body_servico5_botao4` enum('0','1') DEFAULT '1',
  `body_servico5_botao5` enum('0','1') DEFAULT '1',
  `body_servico5_botao6` enum('0','1') DEFAULT '1',
  `body_servico5_botao7` enum('0','1') DEFAULT '1',
  `body_servico5_botao8` enum('0','1') DEFAULT '1',
  `body_servico5_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto5` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto6` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto7` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto8` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink4` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink5` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink6` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink7` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink8` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_preco1_opcao1` enum('0','1') DEFAULT '1',
  `body_servico5_preco2_opcao2` enum('0','1') DEFAULT '1',
  `body_servico5_preco3_opcao3` enum('0','1') DEFAULT '1',
  `body_servico5_preco4_opcao4` enum('0','1') DEFAULT '1',
  `body_servico5_preco5_opcao5` enum('0','1') DEFAULT '1',
  `body_servico5_preco6_opcao6` enum('0','1') DEFAULT '1',
  `body_servico5_preco7_opcao7` enum('0','1') DEFAULT '1',
  `body_servico5_preco8_opcao8` enum('0','1') DEFAULT '1',
  `body_servico5_preco1` varchar(45) DEFAULT '0.00',
  `body_servico5_preco2` varchar(45) DEFAULT '0.00',
  `body_servico5_preco3` varchar(45) DEFAULT '0.00',
  `body_servico5_preco4` varchar(45) DEFAULT '0.00',
  `body_servico5_preco5` varchar(45) DEFAULT '0.00',
  `body_servico5_preco6` varchar(45) DEFAULT '0.00',
  `body_servico5_preco7` varchar(45) DEFAULT '0.00',
  `body_servico5_preco8` varchar(45) DEFAULT '0.00',
  `body_servico5_opcao1Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao1Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao1Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao1Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao1Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao1Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao1Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao1Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao1Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao1Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao2Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao2Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao2Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao2Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao2Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao2Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao2Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao2Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao2Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao2Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao3Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao3Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao3Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao3Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao3Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao3Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao3Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao3Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao3Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao3Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao4Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao4Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao4Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao4Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao4Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao4Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao4Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao4Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao4Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao4Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao5Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao5Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao5Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao5Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao5Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao5Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao5Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao5Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao5Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao5Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao6Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao6Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao6Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao6Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao6Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao6Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao6Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao6Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao6Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao6Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao7Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao7Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao7Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao7Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao7Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao7Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao7Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao7Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao7Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao7Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao8Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao8Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao8Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao8Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao8Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao8Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao8Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao8Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao8Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao8Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao9Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao9Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao9Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao9Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao9Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao9Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao9Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao9Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao9Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao9Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao10Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao10Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao10Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao10Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao10Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao10Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao10Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao10Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao10Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao10Descricao10` varchar(45) DEFAULT 'Descrição 10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico5`
--

LOCK TABLES `lp_modelosconfig_servico5` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico5` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico5` VALUES (1,1,'1','1','Titulo serviço 5','Descrição serviço 5','1','1','1','1','1','1','1','1','img_servicos_868824182.jpg','img_servicos_1614353600.jpg','img_servicos_214148070.jpg','img_servicos_1264671382.jpg','img_servicos_70487885.jpg','img_servicos_156864761.jpg','img_servicos_165478150.jpg','img_servicos_201846313.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','1','1','1','1','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','1','1','1','1','1','1','1','1','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10'),(1,2,'1','1','Titulo serviço 5','Descrição serviço 5','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','1','1','1','1','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','1','1','1','1','1','1','1','1','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10'),(1,3,'1','1','Titulo serviço 5','Descrição serviço 5','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','1','1','1','1','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','1','1','1','1','1','1','1','1','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10');
/*!40000 ALTER TABLE `lp_modelosconfig_servico5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico6`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico6` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico6` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico6` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo6` varchar(45) DEFAULT 'Titulo serviço 6',
  `body_textoGenericoDescricao6` varchar(512) DEFAULT 'Descrição serviço 6',
  `body_servico6_opcao1` enum('0','1') DEFAULT '1',
  `body_servico6_opcao2` enum('0','1') DEFAULT '1',
  `body_servico6_opcao3` enum('0','1') DEFAULT '1',
  `body_servico6_opcao4` enum('0','1') DEFAULT '1',
  `body_servico6_opcao5` enum('0','1') DEFAULT '1',
  `body_servico6_opcao6` enum('0','1') DEFAULT '1',
  `body_servico6_opcao7` enum('0','1') DEFAULT '1',
  `body_servico6_opcao8` enum('0','1') DEFAULT '1',
  `body_servico6Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico6Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico6Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico6Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico6Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico6Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico6Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico6Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico6Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico6Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico6Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico6Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico6Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico6Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico6Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico6Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico6ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico6ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico6ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico6ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico6ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico6ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico6ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico6ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico6_botao1` enum('0','1') DEFAULT '1',
  `body_servico6_botao2` enum('0','1') DEFAULT '1',
  `body_servico6_botao3` enum('0','1') DEFAULT '1',
  `body_servico6_botao4` enum('0','1') DEFAULT '1',
  `body_servico6_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico6`
--

LOCK TABLES `lp_modelosconfig_servico6` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico6` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico6` VALUES (1,1,'1','1','Titulo serviço 6','Descrição serviço 6','1','1','1','1','1','1','1','1','img_servicos_1911543749.jpg','img_servicos_1764444234.jpg','img_servicos_864422360.jpg','img_servicos_335848619.jpg','img_servicos_2114683196.jpg','img_servicos_1877245552.jpg','img_servicos_566385506.jpg','img_servicos_1850458837.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 6','Descrição serviço 6','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 6','Descrição serviço 6','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico7`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico7` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico7` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico7` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo7` varchar(45) DEFAULT 'Titulo serviço 7',
  `body_textoGenericoDescricao7` varchar(512) DEFAULT 'Descrição serviço 7',
  `body_servico7_opcao1` enum('0','1') DEFAULT '1',
  `body_servico7_opcao2` enum('0','1') DEFAULT '1',
  `body_servico7_opcao3` enum('0','1') DEFAULT '1',
  `body_servico7_opcao4` enum('0','1') DEFAULT '1',
  `body_servico7_opcao5` enum('0','1') DEFAULT '1',
  `body_servico7_opcao6` enum('0','1') DEFAULT '1',
  `body_servico7_opcao7` enum('0','1') DEFAULT '1',
  `body_servico7_opcao8` enum('0','1') DEFAULT '1',
  `body_servico7Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico7Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico7Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico7Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico7Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico7Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico7Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico7Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico7Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico7Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico7Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico7Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico7Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico7Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico7Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico7Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico7ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico7ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico7ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico7ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico7ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico7ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico7ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico7ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico7_botao1` enum('0','1') DEFAULT '1',
  `body_servico7_botao2` enum('0','1') DEFAULT '1',
  `body_servico7_botao3` enum('0','1') DEFAULT '1',
  `body_servico7_botao4` enum('0','1') DEFAULT '1',
  `body_servico7_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico7`
--

LOCK TABLES `lp_modelosconfig_servico7` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico7` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico7` VALUES (1,1,'1','1','Titulo serviço 7','Descrição serviço 7','1','1','1','1','1','1','1','1','img_servicos_793904790.jpg','img_servicos_889542648.jpg','img_servicos_1795373101.jpg','img_servicos_302488441.jpg','img_servicos_999830559.jpg','img_servicos_1559030168.jpg','img_servicos_65712972.jpg','img_servicos_723085572.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 7','Descrição serviço 7','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 7','Descrição serviço 7','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico8`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico8` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico8` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico8` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo8` varchar(45) DEFAULT 'Titulo serviço 8',
  `body_textoGenericoDescricao8` varchar(512) DEFAULT 'Descrição serviço 8',
  `body_servico8Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico8Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico8Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico8Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico8Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico8Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico8Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico8Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico8Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico8Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico8Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico8Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico8Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico8Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico8Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico8Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico8ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico8ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico8ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico8ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico8ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico8ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico8ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico8ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico8_botao1` enum('0','1') DEFAULT '1',
  `body_servico8_botao2` enum('0','1') DEFAULT '1',
  `body_servico8_botao3` enum('0','1') DEFAULT '1',
  `body_servico8_botao4` enum('0','1') DEFAULT '1',
  `body_servico8_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico8`
--

LOCK TABLES `lp_modelosconfig_servico8` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico8` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico8` VALUES (1,1,'1','1','Titulo serviço 8','Descrição serviço 8','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 8','Descrição serviço 8','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 8','Descrição serviço 8','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico_maps`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico_maps` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_maps` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico_maps` enum('0','1') DEFAULT '1',
  `body_maps_textoGenericoTitulo1` varchar(45) DEFAULT 'Titulo serviço MAPS',
  `body_maps_textoGenericoDescricao1` varchar(512) DEFAULT 'Descrição serviço MAPS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico_maps`
--

LOCK TABLES `lp_modelosconfig_servico_maps` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico_maps` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico_maps` VALUES (1,1,'1','1','Titulo serviço MAPS','Descrição serviço MAPS'),(1,2,'1','1','Titulo serviço MAPS','Descrição serviço MAPS'),(1,3,'1','1','Titulo serviço MAPS','Descrição serviço MAPS');
/*!40000 ALTER TABLE `lp_modelosconfig_servico_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico_paginasucesso`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico_paginasucesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_modelosconfig_servico_paginasucesso` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_textoGenerico_paginaSucesso` varchar(512) DEFAULT NULL,
  `body_textoGenericoDescricao_paginaSucesso` longtext,
  `body_icone_paginaSucesso` varchar(100) DEFAULT NULL,
  `body_iconeBotao_paginaSucesso` varchar(100) DEFAULT NULL,
  `body_textoBotao_paginaSucesso` varchar(45) DEFAULT NULL,
  `body_linkBotao_paginaSucesso` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico_paginasucesso`
--

LOCK TABLES `lp_modelosconfig_servico_paginasucesso` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico_paginasucesso` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico_paginasucesso` VALUES (1,1,'Obrigado por se cadastrar!!','<p>Fique atento ao seu e-mail, em breve enviaremos novidade.</p>\r\n',' mdi mdi-near-me',' mdi mdi-screw-flat-top','Voltar!','//decoradoces.com.br'),(1,2,NULL,NULL,NULL,NULL,NULL,NULL),(1,3,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `lp_modelosconfig_servico_paginasucesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_planos`
--

DROP TABLE IF EXISTS `lp_planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lp_planos` (
  `id_planos` int(11) NOT NULL,
  `nome_planos` varchar(145) DEFAULT NULL,
  `valor_planos` varchar(12) DEFAULT NULL,
  `meses_planos` int(11) DEFAULT NULL,
  `qtdPages_planos` int(11) DEFAULT NULL,
  `destaque_planos` enum('0','1') DEFAULT NULL,
  `descricao_planos` longtext,
  `status_planos` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_planos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_planos`
--

LOCK TABLES `lp_planos` WRITE;
/*!40000 ALTER TABLE `lp_planos` DISABLE KEYS */;
INSERT INTO `lp_planos` VALUES (1,'LP 1','1.500,00',2,10,'0','<p>asdasd</p>\r\n\r\n<p>asd</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>asda</p>\r\n\r\n<p>sdas</p>\r\n\r\n<p>dasda</p>\r\n','1'),(2,'LP 2','29,90',1,10,'1','<p>asdasd</p>\r\n\r\n<p>asd</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>asda</p>\r\n\r\n<p>sdas</p>\r\n\r\n<p>dasda</p>\r\n','1'),(3,'LP 3','850,00',4,5,'0','<p>asdasd</p>\r\n\r\n<p>asd</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>asda</p>\r\n\r\n<p>sdas</p>\r\n\r\n<p>dasda</p>\r\n','1');
/*!40000 ALTER TABLE `lp_planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_aberturas_email`
--

DROP TABLE IF EXISTS `newsletter_aberturas_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_aberturas_email` (
  `id_aberturas` bigint(14) unsigned NOT NULL,
  `nome_aberturas` varchar(145) DEFAULT NULL,
  `email_aberturas` varchar(256) DEFAULT NULL,
  `aberto_aberturas` int(11) DEFAULT '0',
  `campanha_aberturas` varchar(512) DEFAULT NULL,
  `data_aberturas` varchar(15) DEFAULT NULL,
  `hora_aberturas` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_aberturas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_aberturas_email`
--

LOCK TABLES `newsletter_aberturas_email` WRITE;
/*!40000 ALTER TABLE `newsletter_aberturas_email` DISABLE KEYS */;
INSERT INTO `newsletter_aberturas_email` VALUES (1,'asdasdasd','asdas@gmail.com',1,'asdasdasd','20200527','10:25:40');
/*!40000 ALTER TABLE `newsletter_aberturas_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_campanhas`
--

DROP TABLE IF EXISTS `newsletter_campanhas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_campanhas` (
  `id_campanha` bigint(15) NOT NULL,
  `nome_campanha` varchar(90) NOT NULL,
  `titulo_campanha` varchar(90) NOT NULL,
  `assunto_campanha` varchar(150) NOT NULL,
  `destino_campanha` text NOT NULL,
  `mensagem_campanha` longblob NOT NULL,
  `statusBotao_campanha` enum('0','1','2') NOT NULL COMMENT '0 = pendente\n1 = em andamento\n2 = enviada',
  `id_templateEmail` int(11) NOT NULL,
  PRIMARY KEY (`id_campanha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_campanhas`
--

LOCK TABLES `newsletter_campanhas` WRITE;
/*!40000 ALTER TABLE `newsletter_campanhas` DISABLE KEYS */;
INSERT INTO `newsletter_campanhas` VALUES (1,'teste','teste','teste','5','<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n','2',1),(2,'asdasd','asdasdasd','asdasd','5','<p>asdasdasd</p>\r\n','2',1),(3,'teste 3','teste de envio 3','Esse é um e-mail de teste nº 3','5','<table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' style=\'width:100%\'>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n','2',1),(4,'asdasdasd','asdasd','asdasdasd','5','<p>asdasdasd</p>\r\n','2',1),(5,'qweqweqwe','qweqweq','weqweqwe','5','<p>asdasdqweqwe</p>\r\n','2',1),(6,'asdasdas','dasdasdasdasdq','weqweqw','5','<p>asdasdas</p>\r\n','2',1),(7,'qqqqqqqqqqq','qqqqqqqqqqqqqqqq','qqqqqqqqqqqqqqq','5','<p>qqqqqqqqqqqqqqqqq</p>\r\n','2',1);
/*!40000 ALTER TABLE `newsletter_campanhas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_configs_envio`
--

DROP TABLE IF EXISTS `newsletter_configs_envio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_configs_envio` (
  `id_configsEnvios` int(11) NOT NULL AUTO_INCREMENT,
  `host_configsEnvios` varchar(145) DEFAULT NULL,
  `usuario_configsEnvios` varchar(145) DEFAULT NULL,
  `senha_configsEnvios` varchar(145) DEFAULT NULL,
  `porta_configsEnvios` int(5) DEFAULT NULL,
  `debug_configsEnvios` enum('0','1','2') DEFAULT NULL,
  `SMTPSecure_configsEnvios` varchar(15) DEFAULT NULL,
  `SMTPAuth_configsEnvios` varchar(145) DEFAULT NULL,
  `CharSet_configsEnvios` varchar(145) DEFAULT NULL,
  `Encoding_configsEnvios` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_configsEnvios`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Tabela que contém as configurações da conta que irá enviar os e-mails.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_configs_envio`
--

LOCK TABLES `newsletter_configs_envio` WRITE;
/*!40000 ALTER TABLE `newsletter_configs_envio` DISABLE KEYS */;
INSERT INTO `newsletter_configs_envio` VALUES (1,'mail.solidsystem.net.br','newsletter@solidsystem.net.br','123@mudar!@!@',465,'2','ssl','true','UTF-8','base64');
/*!40000 ALTER TABLE `newsletter_configs_envio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_configs_template_email`
--

DROP TABLE IF EXISTS `newsletter_configs_template_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_configs_template_email` (
  `id_configs_template` int(11) NOT NULL AUTO_INCREMENT,
  `linkImagem_configs_template` varchar(512) DEFAULT NULL,
  `linkLogotipo_configs_template` varchar(512) DEFAULT NULL,
  `nomeEmpresa_configs_template` varchar(145) DEFAULT NULL,
  `endereco_configs_template` varchar(145) DEFAULT NULL,
  `dominio_configs_template` varchar(145) DEFAULT NULL,
  `linkDominio_configs_template` varchar(512) DEFAULT NULL,
  `linkFacebook_configs_template` varchar(512) DEFAULT NULL,
  `linkInstagram_configs_template` varchar(512) DEFAULT NULL,
  `linkWhatsApp_configs_template` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_configs_template`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_configs_template_email`
--

LOCK TABLES `newsletter_configs_template_email` WRITE;
/*!40000 ALTER TABLE `newsletter_configs_template_email` DISABLE KEYS */;
INSERT INTO `newsletter_configs_template_email` VALUES (1,'http://www.crfgo.org.br','https://i.imgur.com/SNLpEMJ.png','CRF-GO | Conselho Regional de Farmácia do Estado de Goiás','Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110','www.crfgo.org.br','http://www.crfgo.org.br','www','www','www');
/*!40000 ALTER TABLE `newsletter_configs_template_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_emails`
--

DROP TABLE IF EXISTS `newsletter_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_emails` (
  `id_emails` int(11) NOT NULL,
  `news_nome` varchar(65) DEFAULT NULL,
  `news_emails` varchar(120) DEFAULT NULL,
  `news_status` enum('0','1') DEFAULT NULL,
  `news_grupoLista` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_emails`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_emails`
--

LOCK TABLES `newsletter_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_emails` DISABLE KEYS */;
INSERT INTO `newsletter_emails` VALUES (1,'josheffer','josheffer@gmail.com','1',5);
/*!40000 ALTER TABLE `newsletter_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_envios`
--

DROP TABLE IF EXISTS `newsletter_envios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_envios` (
  `id_envios` bigint(14) unsigned NOT NULL,
  `id_campanha` int(11) DEFAULT NULL,
  `nome_campanha_envios` varchar(120) DEFAULT NULL,
  `nomePessoa_envios` varchar(90) DEFAULT NULL,
  `mensagem_envios` longblob,
  `assunto_envios` varchar(120) DEFAULT NULL,
  `destino_envios` text,
  `emails_envios` varchar(120) DEFAULT NULL,
  `statusEmails_envios` enum('0','1','2') DEFAULT NULL,
  PRIMARY KEY (`id_envios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_envios`
--

LOCK TABLES `newsletter_envios` WRITE;
/*!40000 ALTER TABLE `newsletter_envios` DISABLE KEYS */;
INSERT INTO `newsletter_envios` VALUES (20200527000001,2,'asdasdasd','asdasdasd','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">asdasdasd</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdasd</p>\r\n</p>\r\n\r\n<img src=\'http://letterpage.net.br/newsletter/confirmarEmail?nome=asdasdasd&email=asdas@gmail.com&campanha=asdasdasd\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=asdas@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','asdasd','5','asdas@gmail.com','1'),(20200527000002,2,'asdasdasd','asdasdasdasdasd','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">asdasdasdasdasd</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdasd</p>\r\n</p>\r\n\r\n<img src=\'http://letterpage.net.br/newsletter/confirmarEmail?nome=asdasdasdasdasd&email=asdasdasdasd12321@gmail.com&campanha=asdasdasd\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=asdasdasdasd12321@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','asdasd','5','asdasdasdasd12321@gmail.com','1'),(20200527000003,1,'teste','Josheffer Robert','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">Josheffer Robert</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n</p>\r\n\r\n<img src=\'http://letterpage.net.br/newsletter/confirmarEmail?nome=Josheffer Robert&email=josheffer@gmail.com&campanha=teste\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=josheffer@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','teste','5','josheffer@gmail.com','1'),(20200527000004,3,'teste de envio 3','Josheffer Robert','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        <span id=\"titulo2\">teste de envio 3</span>\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">Josheffer Robert</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' style=\'width:100%\'>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</p>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=josheffer@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','Esse é um e-mail de teste nº 3','5','josheffer@gmail.com','1'),(20200603000001,4,'asdasd','josheffer','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        <span id=\"titulo2\">asdasd</span>\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">josheffer</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdasd</p>\r\n</p>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=josheffer@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','asdasdasd','5','josheffer@gmail.com','1'),(20200603000002,5,'qweqweq','josheffer','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        <span id=\"titulo2\">qweqweq</span>\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">josheffer</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdqweqwe</p>\r\n</p>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=josheffer@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','weqweqwe','5','josheffer@gmail.com','1'),(20200603000003,6,'dasdasdasdasdq','josheffer','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        <span id=\"titulo2\">dasdasdasdasdq</span>\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">josheffer</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdas</p>\r\n</p>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=josheffer@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','weqweqw','5','josheffer@gmail.com','2'),(20200603000004,7,'qqqqqqqqqqqqqqqq','josheffer','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        <span id=\"titulo2\">qqqqqqqqqqqqqqqq</span>\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">josheffer</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>qqqqqqqqqqqqqqqqq</p>\r\n</p>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=josheffer@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','qqqqqqqqqqqqqqq','5','josheffer@gmail.com','1');
/*!40000 ALTER TABLE `newsletter_envios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_grupos`
--

DROP TABLE IF EXISTS `newsletter_grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_grupos` (
  `id_grupos` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `nome_grupos` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_grupos`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_grupos`
--

LOCK TABLES `newsletter_grupos` WRITE;
/*!40000 ALTER TABLE `newsletter_grupos` DISABLE KEYS */;
INSERT INTO `newsletter_grupos` VALUES (5,'jozin');
/*!40000 ALTER TABLE `newsletter_grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_grupos_x_emails`
--

DROP TABLE IF EXISTS `newsletter_grupos_x_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_grupos_x_emails` (
  `id_grupos` tinyint(2) unsigned NOT NULL,
  `id_emails` bigint(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_grupos_x_emails`
--

LOCK TABLES `newsletter_grupos_x_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_grupos_x_emails` DISABLE KEYS */;
INSERT INTO `newsletter_grupos_x_emails` VALUES (5,1);
/*!40000 ALTER TABLE `newsletter_grupos_x_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_template_email`
--

DROP TABLE IF EXISTS `newsletter_template_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_template_email` (
  `id_templateEmail` int(11) NOT NULL AUTO_INCREMENT,
  `nome_templateEmail` varchar(145) DEFAULT NULL,
  `html_templateEmail` longblob,
  `status_templateEmail` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_templateEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_template_email`
--

LOCK TABLES `newsletter_template_email` WRITE;
/*!40000 ALTER TABLE `newsletter_template_email` DISABLE KEYS */;
INSERT INTO `newsletter_template_email` VALUES (1,'Padrão','<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        <span id=\"titulo2\">{{titulo2}}</span>\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"{{linkImagem}}\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"{{linkImagemLogotipo}}\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">{{nomePessoa}}</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\">{{mensagem}}</p>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">{{endereco}}</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"{{cancelarInscricao}}\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"{{linkDominio}}\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">{{dominio}}</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','1');
/*!40000 ALTER TABLE `newsletter_template_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pu_niveis_paginas`
--

DROP TABLE IF EXISTS `pu_niveis_paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pu_niveis_paginas` (
  `id_niveisPaginas` int(11) NOT NULL AUTO_INCREMENT,
  `id_nivelAcesso` varchar(45) DEFAULT NULL,
  `id_pagina` varchar(45) DEFAULT NULL,
  `permissao_niveisPaginas` enum('0','1') DEFAULT NULL,
  `menu_niveisPaginas` enum('0','1') DEFAULT NULL,
  `add_niveisPaginas` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_niveisPaginas`)
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pu_niveis_paginas`
--

LOCK TABLES `pu_niveis_paginas` WRITE;
/*!40000 ALTER TABLE `pu_niveis_paginas` DISABLE KEYS */;
INSERT INTO `pu_niveis_paginas` VALUES (6,'5','1','1','1','1'),(7,'4','1','1','0','1'),(8,'3','1','0','0','0'),(9,'2','1','1','1','1'),(10,'1','1','1','1','1'),(11,'5','2','1','1','1'),(12,'4','2','1','0','1'),(13,'3','2','0','0','0'),(14,'2','2','0','1','1'),(15,'1','2','1','1','1'),(16,'5','3','0','0','1'),(17,'4','3','1','1','1'),(18,'3','3','0','0','0'),(19,'2','3','0','0','1'),(20,'1','3','1','1','1'),(26,'5','5','0','0','1'),(27,'4','5','1','1','1'),(28,'3','5','0','0','0'),(29,'2','5','0','0','1'),(30,'1','5','1','1','1'),(36,'5','7','1','0','1'),(37,'4','7','1','1','1'),(38,'3','7','0','0','0'),(39,'2','7','0','0','1'),(40,'1','7','1','0','1'),(41,'5','8','1','0','1'),(42,'4','8','1','1','1'),(43,'3','8','0','0','0'),(44,'2','8','0','0','1'),(45,'1','8','1','0','1'),(46,'5','9','1','0','1'),(47,'4','9','1','1','1'),(48,'3','9','0','0','0'),(49,'2','9','0','0','1'),(50,'1','9','1','0','1'),(51,'5','10','1','0','1'),(52,'4','10','1','1','1'),(53,'3','10','0','0','0'),(54,'2','10','0','0','1'),(55,'1','10','1','0','1'),(56,'5','11','0','0','1'),(57,'4','11','1','1','1'),(58,'3','11','0','0','0'),(59,'2','11','0','0','1'),(60,'1','11','1','0','1'),(61,'5','12','1','0','1'),(62,'4','12','1','1','1'),(63,'3','12','0','0','0'),(64,'2','12','0','0','1'),(65,'1','12','1','0','1'),(66,'5','13','1','0','1'),(67,'4','13','1','1','1'),(68,'3','13','0','0','0'),(69,'2','13','0','0','1'),(70,'1','13','1','0','1'),(71,'5','14','1','0','1'),(72,'4','14','1','1','1'),(73,'3','14','0','0','0'),(74,'2','14','0','0','1'),(75,'1','14','1','0','1'),(76,'5','15','0','0','1'),(77,'4','15','1','1','1'),(78,'3','15','0','0','0'),(79,'2','15','0','0','1'),(80,'1','15','1','0','1'),(81,'5','16','0','0','1'),(82,'4','16','1','1','1'),(83,'3','16','0','0','0'),(84,'2','16','0','0','1'),(85,'1','16','1','0','1'),(86,'5','17','0','0','1'),(87,'4','17','1','1','1'),(88,'3','17','0','0','0'),(89,'2','17','0','0','1'),(90,'1','17','1','0','1'),(91,'5','18','1','0','1'),(92,'4','18','1','1','1'),(93,'3','18','0','0','0'),(94,'2','18','1','0','1'),(95,'1','18','1','0','1'),(96,'5','19','1','0','1'),(97,'4','19','0','0','0'),(98,'3','19','0','0','0'),(99,'2','19','0','0','1'),(100,'1','19','1','0','1'),(101,'5','20','1','0','1'),(102,'4','20','0','0','0'),(103,'3','20','0','0','0'),(104,'2','20','0','0','1'),(105,'1','20','1','0','1'),(106,'5','21','1','0','1'),(107,'4','21','0','0','0'),(108,'3','21','0','0','0'),(109,'2','21','0','0','1'),(110,'1','21','1','0','1'),(111,'5','22','1','0','1'),(112,'4','22','0','0','0'),(113,'3','22','0','0','0'),(114,'2','22','0','0','1'),(115,'1','22','1','0','1'),(116,'5','23','1','0','1'),(117,'4','23','0','0','0'),(118,'3','23','0','0','0'),(119,'2','23','0','0','1'),(120,'1','23','1','0','1'),(121,'5','24','0','0','1'),(122,'4','24','0','0','0'),(123,'3','24','0','0','0'),(124,'2','24','0','0','1'),(125,'1','24','1','0','1'),(126,'5','25','0','0','1'),(127,'4','25','0','0','0'),(128,'3','25','0','0','0'),(129,'2','25','0','0','1'),(130,'1','25','1','0','1'),(131,'5','26','1','0','1'),(132,'4','26','0','0','0'),(133,'3','26','0','0','0'),(134,'2','26','0','0','1'),(135,'1','26','1','0','1'),(136,'5','27','1','0','1'),(137,'4','27','0','0','0'),(138,'3','27','0','0','0'),(139,'2','27','0','0','1'),(140,'1','27','1','0','1'),(141,'5','28','1','0','1'),(142,'4','28','0','0','0'),(143,'3','28','0','0','0'),(144,'2','28','0','0','1'),(145,'1','28','1','0','1'),(146,'5','29','1','0','1'),(147,'4','29','0','0','0'),(148,'3','29','0','0','0'),(149,'2','29','0','0','1'),(150,'1','29','1','0','1'),(151,'5','30','0','0','1'),(152,'4','30','0','0','0'),(153,'3','30','0','0','0'),(154,'2','30','0','0','1'),(155,'1','30','1','0','1'),(156,'5','31','0','0','1'),(157,'4','31','0','0','0'),(158,'3','31','0','0','0'),(159,'2','31','0','0','1'),(160,'1','31','1','0','1'),(161,'5','32','0','0','1'),(162,'4','32','0','0','0'),(163,'3','32','0','0','0'),(164,'2','32','0','0','1'),(165,'1','32','1','0','1'),(166,'5','33','0','0','1'),(167,'4','33','1','0','0'),(168,'3','33','1','0','0'),(169,'2','33','0','0','1'),(170,'1','33','1','0','1'),(171,'5','34','0','0','1'),(172,'4','34','0','0','0'),(173,'3','34','0','0','0'),(174,'2','34','0','0','1'),(175,'1','34','1','0','1'),(176,'5','35','0','0','1'),(177,'4','35','0','0','0'),(178,'3','35','0','0','0'),(179,'2','35','0','0','1'),(180,'1','35','1','0','1'),(231,'5','46','0','0','1'),(232,'4','46','0','0','0'),(233,'3','46','0','0','0'),(234,'2','46','0','0','1'),(235,'1','46','1','1','1'),(236,'5','47','0','0','1'),(237,'4','47','0','0','0'),(238,'3','47','0','0','0'),(239,'2','47','0','0','1'),(240,'1','47','1','0','1'),(241,'5','48','0','0','1'),(242,'4','48','0','0','0'),(243,'3','48','0','0','0'),(244,'2','48','0','0','1'),(245,'1','48','1','0','1'),(246,'5','49','0','0','1'),(247,'4','49','0','0','0'),(248,'3','49','0','0','0'),(249,'2','49','0','0','1'),(250,'1','49','1','0','1'),(251,'5','50','0','0','1'),(252,'4','50','0','0','0'),(253,'3','50','0','0','0'),(254,'2','50','0','0','1'),(255,'1','50','1','0','1'),(256,'5','51','0','0','1'),(257,'4','51','0','0','0'),(258,'3','51','0','0','0'),(259,'2','51','0','0','1'),(260,'1','51','1','0','1'),(261,'5','52','0','0','0'),(262,'4','52','0','0','0'),(263,'3','52','0','0','0'),(264,'2','52','0','0','0'),(265,'1','52','1','0','1'),(266,'5','53','0','0','0'),(267,'4','53','0','0','0'),(268,'3','53','0','0','0'),(269,'2','53','0','0','0'),(270,'1','53','1','0','1'),(271,'5','54','0','0','0'),(272,'4','54','0','0','0'),(273,'3','54','0','0','0'),(274,'2','54','0','0','0'),(275,'1','54','1','0','1'),(276,'5','55','0','0','0'),(277,'4','55','0','0','0'),(278,'3','55','0','0','0'),(279,'2','55','0','0','0'),(280,'1','55','1','0','1'),(281,'5','56','0','0','0'),(282,'4','56','0','0','0'),(283,'3','56','0','0','0'),(284,'2','56','0','0','0'),(285,'1','56','1','0','1'),(296,'5','59','0','0','0'),(297,'4','59','0','0','0'),(298,'3','59','0','0','0'),(299,'2','59','0','0','0'),(300,'1','59','1','0','1'),(301,'5','60','0','0','0'),(302,'4','60','0','0','0'),(303,'3','60','0','0','0'),(304,'2','60','0','0','0'),(305,'1','60','1','0','1'),(306,'5','61','0','0','0'),(307,'4','61','0','0','0'),(308,'3','61','0','0','0'),(309,'2','61','0','0','0'),(310,'1','61','0','0','1');
/*!40000 ALTER TABLE `pu_niveis_paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pu_nivelacesso`
--

DROP TABLE IF EXISTS `pu_nivelacesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pu_nivelacesso` (
  `id_nivelAcesso` int(11) NOT NULL,
  `nome_nivelAcesso` varchar(45) DEFAULT NULL,
  `cor_nivelAcesso` varchar(10) DEFAULT NULL,
  `corTexto_nivelAcesso` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pu_nivelacesso`
--

LOCK TABLES `pu_nivelacesso` WRITE;
/*!40000 ALTER TABLE `pu_nivelacesso` DISABLE KEYS */;
INSERT INTO `pu_nivelacesso` VALUES (1,'Administração','#000000','#ffffff'),(2,'Marketing','#45309c','#ffffff'),(3,'Juridico','#c02929','#ffffff'),(4,'Contabilidade','#82c655','#000000'),(5,'Cliente','#652dc4','#ffffff');
/*!40000 ALTER TABLE `pu_nivelacesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pu_paginas`
--

DROP TABLE IF EXISTS `pu_paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pu_paginas` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(45) DEFAULT NULL,
  `endereco_pagina` varchar(256) DEFAULT NULL,
  `observacao_pagina` varchar(128) DEFAULT NULL,
  `icone_pagina` varchar(128) DEFAULT NULL,
  `controller_pagina` varchar(145) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela que armazena todas as páginas que o usuário pode acessar.\n\nusar as rotas para armazenar novas páginas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pu_paginas`
--

LOCK TABLES `pu_paginas` WRITE;
/*!40000 ALTER TABLE `pu_paginas` DISABLE KEYS */;
INSERT INTO `pu_paginas` VALUES (1,'Home','/dashboard','Página inicial',' mdi mdi-home',NULL),(2,'Landing Pages','/dashboard/landingPages','Página de gerenciamento das landing pages','fe-file-text',NULL),(3,'Usuários','/dashboard/usuarios/usuarios','Página de gerenciamento de usuários','mdi mdi-account-multiple-outline',NULL),(5,'Permissões','/dashboard/permissoes/niveisAcesso','Página de permissões','fas fa-gavel',NULL),(7,'Grupos','/dashboard/grupos','Página de Grupos','',NULL),(8,'Cadastrar nova landingPage','/dashboard/cadastrarNovaLandingPage','','',NULL),(9,'Configs LandingPages','/dashboard/configuracoes','Página de configurações das landingpages','',NULL),(10,'Templates LandingPages','/dashboard/templates','Página de configurações dos templates das landingPages','',NULL),(11,'Páginas','/dashboard/permissoes/paginas','Página de gerenciamento das páginas do sistema','',NULL),(12,'Leads','/dashboard/landingPages/leads/','Página de leads das landings pages','',NULL),(13,'Selecionar Template','/dashboard/templates/selecionarTemplate/','Página para selecionar o template da landing page','',NULL),(14,'Configurar template','/dashboard/templates/configurarTemplate/','Página de configuração do template da landing page','',NULL),(15,'Metodo cadastrar usuário','/dashboard/usuarios/cadastrarUsuario','Metodo cadastro de usuario','',NULL),(16,'Metodo listar usuários','/dashboard/usuarios/listarUsuarios','Metodo para listar todos os usuários na página de usuários','',NULL),(17,'Permissão do nível de acesso','/dashboard/permissoes/permissaoNiveisAcesso/','página de permissão do nível de acesso','',NULL),(18,'Minha conta','/dashboard/usuarios/minhaconta','Página para gerenciar a conta do usuário logado','',NULL),(19,'Metodo atualizar status Landing Page','/landingpages/atualizar/atualizarStatusLandingPage','','',NULL),(20,'Metodo Editar Landing Page','/landingpages/atualizar/editarLandingPage','Metodo usado para subir o modal e editar os dados da landing page','',NULL),(21,'metodo excluir Landing Page','/landingpages/excluir/excluirLandingPages','Metodo usado para subir o modal e excluir a landing page','',NULL),(22,'Metodo atualizar status do Usuário','/dashboard/usuarios/atualizarStatusUsuario','Metodo usado para subir o modal e atualizar o status do usuário','',NULL),(23,'Metodo atualizar dados do Usuário','/dashboard/usuarios/atualizarDadosUsuario','Metodo usado para subir o modal e atualizar os dados do usuário','',NULL),(24,'Metodo excluir  Usuário','/dashboard/usuarios/deletarDadosUsuario','Metodo usado para subir o modal e excluir usuário	','',NULL),(25,'Metodo atualizar dados de configurações da la','/landingpages/atualizar/configuracoes','','',NULL),(26,'Metodo cadastrar template (Agência)','/dashboard/cadastro/cadastrarNovoTemplateLandingPage','','',NULL),(27,'Metodo atualizar status do template (Agência)','/dashboard/atualizar/atualizarStatusTemplates','','',NULL),(28,'Metodo que atualiza os dados da template (Age','/dashboard/atualizar/atualizarDadosTemplate','','',NULL),(29,'Metodo excluir template (agencia)','/dashboard/excluir/excluirTemplate','','',NULL),(30,'Metodo cadastrar novo nível de acesso','/dashboard/permissoes/criarNivelAcesso','','',NULL),(31,'Metodo editar nivel de acesso','/dashboard/permissoes/atualizarNivelAcesso','Botão que sobe o modal para editar o nivel','',NULL),(32,'Metodo excluir nivel de acesso','/dashboard/permissoes/deletarNivelAcesso','Botão que sobe o modal para excluir o nivel','',NULL),(33,'Botão associar página ao nivel de acesso','/dashboard/permissoes/associarRemoverPagina','','',NULL),(34,'Permissão MENU','/dashboard/permissoes/liberarBloquearMenu','','',NULL),(35,'Botão permissão','/dashboard/permissoes/liberarBloquearPermissao','','',NULL),(46,'Newsletter','/dashboard/newsletter','',' mdi mdi-email-mark-as-unread','site/site_admin/v_newsletter'),(47,'Grupos Newsletter','/dashboard/newsletter/grupos','','',''),(48,'emails newsletter','/dashboard/newsletter/emails','','',''),(49,'Templates newsletter','/dashboard/newsletter/templateEmail','','',''),(50,'Gerenciar template newsletter','/dashboard/newsletter/gerenciar_template','','',''),(51,'Disparos','/dashboard/newsletter/disparos','','',''),(52,'Grupos landingpage','/dashboard/gruposLandingPage','','',''),(53,'Dashboard','/','','',''),(54,'Painel','/painel','','',''),(55,'Newsletter (modulo)','/newsletter','','',''),(56,'Usuários (Modulo)','/usuarios','','',''),(59,'Confis disparo (SMTP)','/dashboard/newsletter/configuracoes_disparo','','',''),(60,'Dados empresa','/dashboard/empresa/minhaEmpresa','','',''),(61,'Config. do sistema','/dashboard/configs','',' fas fa-cog','');
/*!40000 ALTER TABLE `pu_paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relatorios`
--

DROP TABLE IF EXISTS `relatorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relatorios` (
  `id` int(11) NOT NULL,
  `nomePessoa` varchar(75) NOT NULL,
  `emailPessoa` varchar(45) NOT NULL,
  `nomeNewsletter` varchar(145) NOT NULL,
  `ipPessoa` varchar(45) DEFAULT NULL,
  `dispositivoPessoa` varchar(145) DEFAULT NULL,
  `navegadorPessoa` varchar(145) DEFAULT NULL,
  `SOPessoa` varchar(145) DEFAULT NULL,
  `modeloDispositivo` varchar(145) DEFAULT NULL,
  `dataHora` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relatorios`
--

LOCK TABLES `relatorios` WRITE;
/*!40000 ALTER TABLE `relatorios` DISABLE KEYS */;
/*!40000 ALTER TABLE `relatorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates_landing_pages`
--

DROP TABLE IF EXISTS `templates_landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates_landing_pages` (
  `id_templates` int(11) NOT NULL AUTO_INCREMENT,
  `nomeTemplate_templates` varchar(145) NOT NULL,
  `imagemTemplate_templates` varchar(145) NOT NULL,
  `statusTemplate_templates` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_templates`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates_landing_pages`
--

LOCK TABLES `templates_landing_pages` WRITE;
/*!40000 ALTER TABLE `templates_landing_pages` DISABLE KEYS */;
INSERT INTO `templates_landing_pages` VALUES (1,'Padrão','img_templates_1117283970.jpg','1');
/*!40000 ALTER TABLE `templates_landing_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `login_usuario` varchar(45) NOT NULL,
  `senha_usuario` varchar(145) NOT NULL,
  `nome_usuario` varchar(45) NOT NULL,
  `email_usuario` varchar(90) NOT NULL,
  `status_usuario` enum('0','1') NOT NULL,
  `nivelAcesso_usuario` int(11) DEFAULT NULL,
  `imagem_usuario` varchar(145) NOT NULL DEFAULT 'default.png',
  `token_usuario` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'josheffer','$2y$10$4g/W.I2AnCwxV3aS5W8xbOyx.qpH/t8yB4iYjLyoDo.2uqXCaNKva','Josheffer Robert','josheffer@gmail.com','1',1,'img_usuario_855726029.jpg','2simbdb9rh9rl8uos20f9codcjvjcop3');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-05  1:23:53
