

<div class="content-page">
    
    <div class="content">
        
        <div class="container-fluid">
            
            <div class="row mt-3">
                
                <div class="col-12">

                    <?php
                        $this->load->view('botoesNiveisAcesso');
                    ?>
                    
                    <div class="row">

                        <div class="col-lg-12">

                            <div id="msgErro"></div>
                            
                            <div class="card">
                            
                                <?php if ($permissaoLinks[29]->permissao_niveisPaginas === '1') : ?>
                                    <div class="card-body">
                                        
                                        <h5 class="card-title mb-3">Criar novo nível de acesso</h5>
                                        
                                        <div class="row">
                                            
                                            <div class="col-lg-12 col-xl-12">
                                                
                                                <div class="card-box">
                                                    
                                                    <form id="formCadastrarNivelAcesso">
                                                        
                                                        <div class="row">
                                                            
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="firstname">Nome</label>
                                                                    <input type="text" class="form-control" id="nomeNivelAcesso" name="nomeNivelAcesso" autocomplete="off">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="lastname">Cor de fundo</label>
                                                                    <input type="color" class="form-control" id="corNivelAcesso" name="corNivelAcesso">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="lastname">Cor do texto</label>
                                                                    <input type="color" class="form-control" id="corTextoNivelAcesso" name="corTextoNivelAcesso">
                                                                </div>
                                                            </div>
                                                            
                                                        </div> 
                                                        
                                                        <div class="text-center">
                                                            <button type="submit" id="botaoCadastrarNivelAcesso" class="btn btn-block btn-success waves-effect waves-light mt-2">
                                                                <i class="mdi mdi-content-save"></i> Cadastrar
                                                            </button>
                                                        </div>

                                                    </form>

                                                </div>

                                            </div> 
                                        </div>
                                        
                                    </div>

                                <?php endif; ?>
                            </div>

                            <div class="row">
                                <div class="col-xl-12">
                                    <div id="accordion" class="mb-3">
                                        <div class="card mb-1">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="m-0">
                                                    <a class="text-dark" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                        Níveis de acessos do sistema
                                                    </a>
                                                </h5>
                                            </div>
                                
                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                                <div class="card-body">
                                                
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Nome</th>
                                                                <?php if($permissaoLinks[16]->permissao_niveisPaginas === '1'): ?><th>Permissões</th><?php endif;?>
                                                                    <?php if($permissaoLinks[30]->permissao_niveisPaginas === '1' || $permissaoLinks[31]->permissao_niveisPaginas === '1'): ?><th>Ações</th><?php endif;?>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tabelaListarNiveisAcesso">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    

                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
</div>

<div id="modalAtualizarNivelAcesso" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

    <div class="modal-dialog modal-full">

        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Alterar usuário: <u>#<span id="idNivelAcessotualizar"></span> - <span class="text-warning" id="atualizarNomeNivelAcessoTitulo"></span></u></h4>
                <button type="button" id="fecharModalAtualizarNivelAcesso" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <form id="formAtualziarNivelAcesso">

                <div class="modal-body p-4">

                    <div id="msgErroAtualizarNivelAcesso"></div>
                    
                    <input type="hidden" id="atualizarIdNivelAcesso" name="atualizarIdNivelAcesso">
                        
                    <div class="row">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Nome</label>
                                <input type="text" class="form-control" id="atualizarNomeNivelAcesso" name="atualizarNomeNivelAcesso" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-2" class="control-label">Cor de fundo</label>
                                <input type="color" class="form-control" id="atualizarCorNivelAcesso" name="atualizarCorNivelAcesso" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="field-3" class="control-label">Cor do texto</label>
                                <input type="color" class="form-control" id="atualizarCorTextoNivelAcesso" name="atualizarCorTextoNivelAcesso" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Atualizar</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div id="modalDeletarNivelAcesso" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mySmallModalLabel">Deletar nivel de acesso: <u><strong class="text-warning" id="nomeNivelAcessoDeletar"></strong></u></h4>
                <button type="button" id="fecharModalDeletarNivelAcesso" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <form id="formDeletarNivelAcesso">
                <input type="hidden" id="idNivelAcessoDeletar" name="idNivelAcessoDeletar">
                <div class="modal-body">
                    
                    <div id="msgErroDeletarNivelAcesso"></div>

                    Tem certeza que deseja deletar esse usuário??

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger waves-effect">(Sim) DELETAR</button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>