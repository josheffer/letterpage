<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        date_default_timezone_set('America/Sao_Paulo');

        $this->load->model("LandingPage_model", "landingpage");
        $this->load->model("TemplateLandingPage_model", "template");
        $this->load->model("Modelos_model", "modelos");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->library('permitiracesso');

        $this->load->library('relatorioleads');

        $this->load->model("empresa/Empresa_model", "empresa");
        
    }
    
	public function v_leads()
	{
        $this->permitiracesso->verifica_sessao();

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $dadosSessao['idLead'] = $id;

        $carregarLandingPage = $this->landingpage->buscarLandingPage($id);

        $dadosSessao['dados'] = $this->session->userdata;

        
        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;
        
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $infos['id_nivelAcesso'] = $nivelAcesso_usuario;
        $infos['view'] = "v_leads";

        $acessoPermitido = $this->permitiracesso->verificaAcessoPaginaParametro($infos);
        
        $scripts = array(

            "titulo" => "Leads | Landing page (".$carregarLandingPage[0]->id_landingPage.")",

            "scriptsJS" => '<script src="/assets/painel/js/vendor/scriptsRelatoriosAcessos.js"></script>
                            <script src="/assets/painel/js/vendor/scriptsRelatoriosCadastros.js"></script>    
                            <script src="/assets/painel/js/vendor/scriptsRelatoriosClicks.js"></script>    
                            <script src="/assets/libs/moment.js"></script>',
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
        $this->load->view('painel/Template/t_menuLateral');
        if($acessoPermitido->permissao_niveisPaginas === '1' && $acessoPermitido->add_niveisPaginas === '1')
        {
            $this->load->view('modelos/modelo'.$carregarLandingPage[0]->template_landingPage.'/'.$acessoPermitido->view, $dadosSessao);

        } else {

            $this->load->view('modelos/modelo'.$carregarLandingPage[0]->template_landingPage.'/errors/html/semPermissao', $dadosSessao);
        }
        $this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }
    
    public function c_listarRelatorioAcessos()
    {
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
    
        $resultado = $this->landingpage->m_listarRelatorioAcessos($id);

        echo json_encode($resultado);

    }

    public function c_listarDadosLandingPages()
    {
        
        $resultado = $this->landingpage->m_listarDadosLandingPages();

        echo json_encode($resultado);

    }

    public function c_listarRelatoriosLeads()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatoriosLeads($id);

        echo json_encode($resultado);
    }

    public function c_downListarRelatoriosAcessosDia()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatoriosLeads($id);

        $this->relatorioleads->gerarRelatorioAcessoDia($resultado);
    }

    public function c_downListarRelatoriosAcessosSemana()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioSemana($id);
        
        $this->relatorioleads->gerarRelatorioAcessoSemana($resultado);
    }

    public function c_downListarRelatoriosAcessosMes()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioMensal($id);
        
        $this->relatorioleads->gerarRelatorioAcessoMensal($resultado);
    }
    
    public function c_downListarRelatoriosAcessosAno()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioAnual($id);
        
        $this->relatorioleads->gerarRelatorioAcessoAnual($resultado);
    }
    
    public function c_downListarRelatoriosCadastroDia()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosDiario($id);
        
        $this->relatorioleads->gerarRelatorioCadastroDia($resultado);
    }
    
    public function c_downListarRelatoriosCadastroSemana()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosSemanal($id);
        
        $this->relatorioleads->gerarRelatorioCadastroSemanal($resultado);
    }

    public function c_downListarRelatoriosCadastroMes()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosMensal($id);
        
        $this->relatorioleads->gerarRelatorioCadastroMensal($resultado);
    }

    public function c_downListarRelatoriosCadastroAno()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosAnual($id);
        
        $this->relatorioleads->gerarRelatorioCadastroAnual($resultado);
    }
    
    public function c_downListarRelatoriosClicksBotoesDia()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksDiario($id);
        
        $this->relatorioleads->gerarRelatorioClicksBotoesDia($resultado);
    }
    
    public function c_downListarRelatoriosClicksBotoesSemana()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksSemanal($id);
        
        $this->relatorioleads->gerarRelatorioClicksBotoesSemanal($resultado);
    }
    
    public function c_downListarRelatoriosClicksBotoesMes()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksMensal($id);
        
        $this->relatorioleads->gerarRelatorioClicksBotoesMensal($resultado);
    }
    
    public function c_downListarRelatoriosClicksBotoesAno()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksAnual($id);
        
        $this->relatorioleads->gerarRelatorioClicksBotoesAnual($resultado);
    }

    public function c_listarRelatorioSemana()
    {
    
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioSemana($id);

        echo json_encode($resultado);

    }

    public function c_listarRelatorioMensal()
    {
    
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioMensal($id);

        echo json_encode($resultado);

    }

    public function c_listarRelatorioAnual()
    {
    
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioAnual($id);

        echo json_encode($resultado);

    }

    public function c_listarRelatoriosCadastros()
    {
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatoriosCadastros($id);

        echo json_encode($resultado);

    }
    
    public function c_listarRelatorioCadastrosDiario()
    {
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosDiario($id);

        echo json_encode($resultado);

    }
    
    public function c_listarRelatorioCadastrosSemanal()
    {
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosSemanal($id);

        echo json_encode($resultado);

    }
    
    public function c_listarRelatorioCadastrosMensal()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosMensal($id);

        echo json_encode($resultado);
    }

    public function c_listarRelatorioCadastrosAnual($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosAnual($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosFacebook($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosFacebook($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosInstagram($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosInstagram($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosWhatsApp($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosWhatsApp($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosGoogle($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosGoogle($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosYouTube($id)
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosYouTube($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosMessenger($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosMessenger($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosAmigos($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosAmigos($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosOutros($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosOutros($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioCadastrosTotais($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioCadastrosTotais($id);
        
        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatoriosClicks($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatoriosClicks($id);
        
        echo json_encode($resultado);
    
    }

    public function c_listarRelatorioClicksDiario()
    {
    
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksDiario($id);

        echo json_encode($resultado);

    }
    
    public function c_listarRelatorioClicksSemanal($id)
    {
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksSemanal($id);

        echo json_encode($resultado);

    }
    
    public function c_listarRelatorioClicksMensal($id)
    {
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksMensal($id);

        echo json_encode($resultado);

    }

    public function c_listarRelatorioClicksAnual($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksAnual($id);

        echo json_encode($resultado);
    
    }
    
    public function c_listarRelatorioClicksBotoes($id)
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->m_listarRelatorioClicksBotoes($id);

        echo json_encode($resultado);
    
    }
    
}
