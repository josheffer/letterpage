primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarHeadSEO();
listarLogoMenuSlide();
listarForms();
listarRedesSociais();
listarServicos();

$(document).ready(function() {
  $('#botaoMobileAbrirEFechar').trigger('click');
});

function listarRedesSociais(){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");

  $.ajax(
    {
        
      url: "/dashboard/listagens/listarRedesSociais/"+numModelo[6],
      
      ajax: 'dados.json',
      
      success: function(dados){
          
        var dados = JSON.parse(dados);

        dadosGlobais = dados;

        if(dados.body_redesSociais === '1'){$('#servicoRedesSociais').val(1).attr("checked", "checked");} else {$('#servicoRedesSociais').val(1);}
        
        $('#corFundoRedesSociais').val(dados['configs_personalizadas'][0].servico_redesSociais_corBg);

      }
    }
  );
}

function listarHeadSEO(){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");

  $.ajax(
    {
        
      url: "/dashboard/listagens/listarHeadSEO/"+numModelo[6],
      
      ajax: 'dados.json',
      
      success: function(dados){
          
        var dados = JSON.parse(dados);

        dadosGlobais = dados;
        
        $('#idModelo_headSEO').val(dados.id_modelo); 
        $('#idLandingPage_headSEO').val(dados.id_landingPage); 
        $('#cssHeadTOPO').html(dados.topo_head_css); 
        $('#jsHead').html(dados.topo_head_js); 
        $('#SEOKeywordsHead').html(dados.topo_head_SEO_keywords); 
        $('#SEODescriptionsHead').html(dados.topo_head_SEO_description); 
        $('#SEODescriptionsHead2').html(dados.topo_head_SEO_description2); 
        $('#SEODescriptionsHead3').html(dados.topo_head_SEO_description3); 
        $('#SEOCoordGMaps').val(dados.topo_head_coordenadasMaps); 
        $('#SEOCidade').val(dados.topo_head_cidade); 
        $('#SEOEstado').val(dados.topo_head_estado); 
        $("#verImagemDoSEO").attr("src", "/assets/images/SEO/"+dados.topo_head_SEO_imagem);
        
      }
    }
  );
}

function listarLogoMenuSlide(){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");

  $.ajax(
    {
        
      url: "/dashboard/listagens/listarLogoMenuSlide/"+numModelo[6],
      
      ajax: 'dados.json',
      
      success: function(dados){
          
        var dados = JSON.parse(dados);
        
        $('#cssHead').html('');

        dadosGlobais = dados;
        
        if(dados['logoMenuSlide'][0].body_logo1 === '1'){$('#logotipoMenu').val(1).attr("checked", "checked");} else {$('#logotipoMenu').val(1);}
        if(dados['logoMenuSlide'][0].body_slide === '1'){$('#ativaDesativaSlideLP').val(1).attr("checked", "checked");} else {$('#ativaDesativaSlideLP').val(1);}
        if(dados['logoMenuSlide'][0].body_slide_botao1 === '1'){$('#botaoSlideAtivaDesativa').val(1).attr("checked", "checked");} else {$('#botaoSlideAtivaDesativa').val(1);}
        
        if(dados['logoMenuSlide'][0].body_imagemPrincipalSlide === '1'){$('#imagemPrincipalAtivadoDesativado').val(1).attr("checked", "checked");} else {$('#imagemPrincipalAtivadoDesativado').val(1);}
        if(dados['logoMenuSlide'][0].body_imagemFundoSlide === '1'){$('#imagemFundoSlideAtivadoDesativado').val(1).attr("checked", "checked");} else {$('#imagemFundoSlideAtivadoDesativado').val(1);}
        
        if(dados['logoMenuSlide'][0].body_slide1 === '1'){$('#ativoBannerSlide1').val(1).attr("checked", "checked");} else {$('#ativoBannerSlide1').val(1);}

        $('#idModeloLogoMenuSlide').val(dados['logoMenuSlide'][0].id_modelo);
        $('#idLandingPage_LogoMenuSlide').val(dados['logoMenuSlide'][0].id_landingPage); 
        $('#tituloBanner').val(dados['logoMenuSlide'][0].body_slide_titulo1);
        $('#descricaoBanner').val(dados['logoMenuSlide'][0].body_slide_descricao1);
        $('#corTextoTituloBanner').val(dados['configs_personalizadas'][0].corTextoTituloSlide);
        $('#corTextoDescricaoBanner').val(dados['configs_personalizadas'][0].corTextoDescricaoSlide);
        $('#corFundoBannerSlide').val(dados['configs_personalizadas'][0].corFundoSlide);
        $('#textoBotaoSlideBanner').val(dados['logoMenuSlide'][0].body_slide_botaoTexto1);
        $('#linkBotaoSlideBanner').val(dados['logoMenuSlide'][0].body_slide_botaoLink1);
        $('#corTextoBotaoSlideBanner').val(dados['configs_personalizadas'][0].corTextoBotaoSlide);
        $('#corTextoHoverBotaoSlideBanner').val(dados['configs_personalizadas'][0].corTextoHoverBotaoSlide);
        $('#corfundoBotaoSlideBanner').val(dados['configs_personalizadas'][0].corFundoBotaoSlide);
        $('#corFundoHoverBotaoSlideBanner').val(dados['configs_personalizadas'][0].corFundoHoverBotaoSlide);
        $('#corFundoMenu').val(dados['configs_personalizadas'][0].corFundoMenu);
        $('#atualizarCorFundoSlide1').val(dados['configs_personalizadas'][0].corFundoSlide1);
        $("#verImagemPrincipal").attr("src", "/assets/images/slide/"+dados['logoMenuSlide'][0].body_slide_imagem1);
        $("#verImagemDeFundo").attr("src", "/assets/images/slide/"+dados['logoMenuSlide'][0].body_slide_imagemFundoSlide);
        $("#verImagemBannerSlide1").attr("src", "/assets/images/slide/"+dados['logoMenuSlide'][0].body_slide_imagem2);
        $('#seoBannerSlide2').val(dados['logoMenuSlide'][0].body_slide_imagem2_SEO);
        
      }
    }
  );
}

function listarForms(){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");

  $.ajax(
    {
        
      url: "/dashboard/listagens/listarForms/"+numModelo[6],
      
      ajax: 'dados.json',
      
      success: function(dados){
          
        var dados = JSON.parse(dados);
        
        $('#cssHead').html('');

        dadosGlobais = dados;
        
        $('#idModeloFormularios').val(dados.id_modelo);
        $('#idLandingPageFormularios').val(dados.id_landingPage);
        $('#nosConheceuOpcao1').val(dados.nosConheceuOpcao1);
        $('#nosConheceuOpcao2').val(dados.nosConheceuOpcao2);
        $('#nosConheceuOpcao3').val(dados.nosConheceuOpcao3);
        $('#nosConheceuOpcao4').val(dados.nosConheceuOpcao4);
        $('#nosConheceuOpcao5').val(dados.nosConheceuOpcao5);
        $('#nosConheceuOpcao6').val(dados.nosConheceuOpcao6);
        $('#nosConheceuOpcao7').val(dados.nosConheceuOpcao7);
        $('#nosConheceuOpcao8').val(dados.nosConheceuOpcao8);

        if(dados.nosConheceuOpcao1_visivel === '1'){$('#nosConheceuOpcao1_visivel').val(1).attr("checked", "checked");} else {$('#nosConheceuOpcao1_visivel').val(1);}
        if(dados.nosConheceuOpcao2_visivel === '1'){$('#nosConheceuOpcao2_visivel').val(1).attr("checked", "checked");} else {$('#nosConheceuOpcao2_visivel').val(1);}
        if(dados.nosConheceuOpcao3_visivel === '1'){$('#nosConheceuOpcao3_visivel').val(1).attr("checked", "checked");} else {$('#nosConheceuOpcao3_visivel').val(1);}
        if(dados.nosConheceuOpcao4_visivel === '1'){$('#nosConheceuOpcao4_visivel').val(1).attr("checked", "checked");} else {$('#nosConheceuOpcao4_visivel').val(1);}
        if(dados.nosConheceuOpcao5_visivel === '1'){$('#nosConheceuOpcao5_visivel').val(1).attr("checked", "checked");} else {$('#nosConheceuOpcao5_visivel').val(1);}
        if(dados.nosConheceuOpcao6_visivel === '1'){$('#nosConheceuOpcao6_visivel').val(1).attr("checked", "checked");} else {$('#nosConheceuOpcao6_visivel').val(1);}
        if(dados.nosConheceuOpcao7_visivel === '1'){$('#nosConheceuOpcao7_visivel').val(1).attr("checked", "checked");} else {$('#nosConheceuOpcao7_visivel').val(1);}
        if(dados.nosConheceuOpcao8_visivel === '1'){$('#nosConheceuOpcao8_visivel').val(1).attr("checked", "checked");} else {$('#nosConheceuOpcao8_visivel').val(1);}

        if(dados.body_servico1_form1 === '1'){$('#formFormulario1').val(1).attr("checked", "checked");} else {$('#formFormulario1').val(1);}
        $('#textoBotaoForm1').val(dados.body_servico1_form1_textBotao);
        $('#corTextoBotaoForm1').val(dados.body_servico1_form1_corTextBotao);
        $('#corFundoBotaoForm1').val(dados.body_servico1_form1_corFundoBotao);
        $('#corFundoFormularioForm1').val(dados.body_servico1_form1_corFundoForm);
        $('#tituloForm1').val(dados.body_servicoForm_tituloForm1);
        $('#descricaoForm1').val(dados.body_servicoForm_descricaoForm1);
        $('#corTituloForm1').val(dados.body_servico_form1_corTitulo);
        $('#corDescricaoForm1').val(dados.body_servico_form1_corDescricao);

        if(dados.body_servico_RedesSociais_form === '1'){$('#formFormularioRedesSociais').val(1).attr("checked", "checked");} else {$('#formFormularioRedesSociais').val(1);}
        $('#textoBotaoFormRedesSociais').val(dados.body_servico_RedesSociais_form_textBotao);
        $('#corTextoBotaoFormRedesSociais').val(dados.body_servico_RedesSociais_form_corTextBotao);
        $('#corFundoBotaoFormRedesSociais').val(dados.body_servico_RedesSociais_form_corFundoBotao);
        $('#corFundoFormularioFormRedesSociais').val(dados.body_servico_RedesSociais_form_corFundoForm);
        $('#tituloFormRS').val(dados.body_servicoForm_tituloFormRS);
        $('#descricaoFormRS').val(dados.body_servicoForm_descricaoFormRS);
        $('#corTituloFormRS').val(dados.body_servico_formRS_corTitulo);
        $('#corDescricaoFormRS').val(dados.body_servico_formRS_corDescricao);
        
        if(dados.body_servico1_form2 === '1'){$('#formFormulario2').val(1).attr("checked", "checked");} else {$('#formFormulario2').val(1);}
        $('#textoBotaoForm2').val(dados.body_servico1_form2_textBotao);
        $('#corTextoBotaoForm2').val(dados.body_servico1_form2_corTextBotao);
        $('#corFundoBotaoForm2').val(dados.body_servico1_form2_corFundoBotao);
        $('#corFundoFormularioForm2').val(dados.body_servico1_form2_corFundoForm);
        $('#tituloForm2').val(dados.body_servicoForm_tituloForm2);
        $('#descricaoForm2').val(dados.body_servicoForm_descricaoForm2);
        $('#corTituloForm2').val(dados.body_servico_form2_corTitulo);
        $('#corDescricaoForm2').val(dados.body_servico_form2_corDescricao);

        if(dados.body_servico2_form3 === '1'){$('#formFormulario3').val(1).attr("checked", "checked");} else {$('#formFormulario3').val(1);}
        $('#textoBotaoForm3').val(dados.body_servico2_form3_textBotao);
        $('#corTextoBotaoForm3').val(dados.body_servico2_form3_corTextBotao);
        $('#corFundoBotaoForm3').val(dados.body_servico2_form3_corFundoBotao);
        $('#corFundoFormularioForm3').val(dados.body_servico2_form3_corFundoForm);
        $('#tituloForm3').val(dados.body_servicoForm_tituloForm3);
        $('#descricaoForm3').val(dados.body_servicoForm_descricaoForm3);
        $('#corTituloForm3').val(dados.body_servico_form3_corTitulo);
        $('#corDescricaoForm3').val(dados.body_servico_form3_corDescricao);

        if(dados.body_servico3_form4 === '1'){$('#formFormulario4').val(1).attr("checked", "checked");} else {$('#formFormulario4').val(1);}
        $('#textoBotaoForm4').val(dados.body_servico3_form4_textBotao);
        $('#corTextoBotaoForm4').val(dados.body_servico3_form4_corTextBotao);
        $('#corFundoBotaoForm4').val(dados.body_servico3_form4_corFundoBotao);
        $('#corFundoFormularioForm4').val(dados.body_servico3_form4_corFundoForm);
        $('#tituloForm4').val(dados.body_servicoForm_tituloForm4);
        $('#descricaoForm4').val(dados.body_servicoForm_descricaoForm4);
        $('#corTituloForm4').val(dados.body_servico_form4_corTitulo);
        $('#corDescricaoForm4').val(dados.body_servico_form4_corDescricao);

        if(dados.body_servico4_form5 === '1'){$('#formFormulario5').val(1).attr("checked", "checked");} else {$('#formFormulario5').val(1);}
        $('#textoBotaoForm5').val(dados.body_servico4_form5_textBotao);
        $('#corTextoBotaoForm5').val(dados.body_servico4_form5_corTextBotao);
        $('#corFundoBotaoForm5').val(dados.body_servico4_form5_corFundoBotao);
        $('#corFundoFormularioForm5').val(dados.body_servico4_form5_corFundoForm);
        $('#tituloForm5').val(dados.body_servicoForm_tituloForm5);
        $('#descricaoForm5').val(dados.body_servicoForm_descricaoForm5);
        $('#corTituloForm5').val(dados.body_servico_form5_corTitulo);
        $('#corDescricaoForm5').val(dados.body_servico_form5_corDescricao);

        if(dados.body_servico5_form6 === '1'){$('#formFormulario6').val(1).attr("checked", "checked");} else {$('#formFormulario6').val(1);}
        $('#textoBotaoForm6').val(dados.body_servico5_form6_textBotao);
        $('#corTextoBotaoForm6').val(dados.body_servico4_form5_corTextBotao);
        $('#corFundoBotaoForm6').val(dados.body_servico5_form6_corFundoBotao);
        $('#corFundoFormularioForm6').val(dados.body_servico5_form6_corFundoForm);
        $('#tituloForm6').val(dados.body_servicoForm_tituloForm6);
        $('#descricaoForm6').val(dados.body_servicoForm_descricaoForm6);
        $('#corTituloForm6').val(dados.body_servico_form6_corTitulo);
        $('#corDescricaoForm6').val(dados.body_servico_form6_corDescricao);

        if(dados.body_servico6_form7 === '1'){$('#formFormulario7').val(1).attr("checked", "checked");} else {$('#formFormulario7').val(1);}
        $('#textoBotaoForm7').val(dados.body_servico6_form7_textBotao);
        $('#corTextoBotaoForm7').val(dados.body_servico6_form7_corTextBotao);
        $('#corFundoBotaoForm7').val(dados.body_servico6_form7_corFundoBotao);
        $('#corFundoFormularioForm7').val(dados.body_servico6_form7_corFundoForm);
        $('#tituloForm7').val(dados.body_servicoForm_tituloForm7);
        $('#descricaoForm7').val(dados.body_servicoForm_descricaoForm7);
        $('#corTituloForm7').val(dados.body_servico_form7_corTitulo);
        $('#corDescricaoForm7').val(dados.body_servico_form7_corDescricao);

        if(dados.body_servico7_form8 === '1'){$('#formFormulario8').val(1).attr("checked", "checked");} else {$('#formFormulario8').val(1);}
        $('#textoBotaoForm8').val(dados.body_servico7_form8_textBotao);
        $('#corTextoBotaoForm8').val(dados.body_servico7_form8_corTextBotao);
        $('#corFundoBotaoForm8').val(dados.body_servico7_form8_corFundoBotao);
        $('#corFundoFormularioForm8').val(dados.body_servico7_form8_corFundoForm);
        $('#tituloForm8').val(dados.body_servicoForm_tituloForm8);
        $('#descricaoForm8').val(dados.body_servicoForm_descricaoForm8);
        $('#corTituloForm8').val(dados.body_servico_form8_corTitulo);
        $('#corDescricaoForm8').val(dados.body_servico_form8_corDescricao);
        
      }
    }
  );
}

function listarRedesSociais(){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");

  $.ajax(
    {
        
      url: "/dashboard/listagens/listarRedesSociais/"+numModelo[6],
      
      ajax: 'dados.json',
      
      success: function(dados){
          
        var dados = JSON.parse(dados);

        dadosGlobais = dados;
        
        if(dados['redesSociais'][0].body_redesSociais === '1'){$('#servicoRedesSociais').val(1).attr("checked", "checked");} else {$('#servicoRedesSociais').val(1);}
        if(dados['redesSociais'][0].body_conteudoGenerico_redesSociais === '1'){$('#servicoRedesSociaisConteudoGenerico').val(1).attr("checked", "checked");} else {$('#servicoRedesSociaisConteudoGenerico').val(1);}
        if(dados['redesSociais'][0].body_redesSociais_opcao1 === '1'){$('#opcao1RedeSocial').val(1).attr("checked", "checked");} else {$('#opcao1RedeSocial').val(1);}
        if(dados['redesSociais'][0].body_redesSociais_opcao2 === '1'){$('#opcao2RedeSocial').val(1).attr("checked", "checked");} else {$('#opcao2RedeSocial').val(1);}
        if(dados['redesSociais'][0].body_redesSociais_opcao3 === '1'){$('#opcao3RedeSocial').val(1).attr("checked", "checked");} else {$('#opcao3RedeSocial').val(1);}

        if(dados['redesSociais'][0].body_servico_redesSociais_botao1 === '1'){$('#botaoRedeSocial1').val(1).attr("checked", "checked");} else {$('#botaoRedeSocial1').val(1);}
        if(dados['redesSociais'][0].body_servico_redesSociais_botao2 === '1'){$('#botaoRedeSocial2').val(1).attr("checked", "checked");} else {$('#botaoRedeSocial2').val(1);}
        if(dados['redesSociais'][0].body_servico_redesSociais_botao3 === '1'){$('#botaoRedeSocial3').val(1).attr("checked", "checked");} else {$('#botaoRedeSocial3').val(1);}
        
        $('#idConfigsRedesSociais').val(dados['redesSociais'][0].id_modelo);
        $('#idLandingPageRedesSociais').val(dados['redesSociais'][0].id_landingPage);
        $('#corFundoRedesSociais').val(dados['configs_personalizadas'][0].servico_redesSociais_corBg);
        $('#tituloRedesSociais').val(dados['redesSociais'][0].body_textoGenericoTitulo_redesSociais);
        $('#descricaoRedesSociais').val(dados['redesSociais'][0].body_textoGenericoDescricao_redesSociais);
        $('#corTituloRedesSociais').val(dados['configs_personalizadas'][0].servico_redesSociais_corTituloGenerico1);
        $('#corTextoDescricaoRedesSociais').val(dados['configs_personalizadas'][0].servico_redesSociais_corDescricaoGenerico1);
        $('#corTituloeDescricaoRedeSocial').val(dados['configs_personalizadas'][0].servico_redesSociais_corTexto);
        $('#corFundoRedeSocial').val(dados['configs_personalizadas'][0].servico_redesSociais_corBoxImage);
        
        $('#tituloRedeSocial1').val(dados['redesSociais'][0].body_servico_redesSociais_Titulo1);
        $('#tituloRedeSocial2').val(dados['redesSociais'][0].body_servico_redesSociais_Titulo2);
        $('#tituloRedeSocial3').val(dados['redesSociais'][0].body_servico_redesSociais_Titulo3);
        
        $('#descricaoRedeSocial1').val(dados['redesSociais'][0].body_servico_redesSociais_Descricao1); 
        $('#descricaoRedeSocial2').val(dados['redesSociais'][0].body_servico_redesSociais_Descricao2);
        $('#descricaoRedeSocial3').val(dados['redesSociais'][0].body_servico_redesSociais_Descricao3);
        
        $('#textoBotao_RedeSocial_Opcao1').val(dados['redesSociais'][0].body_servico_redesSociais_botaoTexto1);
        $('#textoBotao_RedeSocial_Opcao2').val(dados['redesSociais'][0].body_servico_redesSociais_botaoTexto2);
        $('#textoBotao_RedeSocial_Opcao3').val(dados['redesSociais'][0].body_servico_redesSociais_botaoTexto3);

        $('#linkBotao_RedeSocial_Opcao1').val(dados['redesSociais'][0].body_servico_redesSociais_botaoLink1);
        $('#linkBotao_RedeSocial_Opcao2').val(dados['redesSociais'][0].body_servico_redesSociais_botaoLink2);
        $('#linkBotao_RedeSocial_Opcao3').val(dados['redesSociais'][0].body_servico_redesSociais_botaoLink3);

        $('#imagemSEO_RedeSocial_Opcao1').val(dados['redesSociais'][0].body_servico_redesSociais_ImagemSEO1);
        $('#imagemSEO_RedeSocial_Opcao2').val(dados['redesSociais'][0].body_servico_redesSociais_ImagemSEO2);
        $('#imagemSEO_RedeSocial_Opcao3').val(dados['redesSociais'][0].body_servico_redesSociais_ImagemSEO3);
        
        $('#corTextoRedeSocial').val(dados['configs_personalizadas'][0].servico_redesSociais_corTextoBotao);
        $('#corTextoHoverBotaoRedeSocial').val(dados['configs_personalizadas'][0].servico_redesSociais_corTextoHoverBotao);
        $('#corfundoBotaoRedeSocial').val(dados['configs_personalizadas'][0].servico_redesSociais_corFundoBotao);
        $('#corFundoHoverBotaoRedeSocial').val(dados['configs_personalizadas'][0].servico_redesSociais_corFundoHoverBotao);

        $("#verImagemRedeSocial1").attr("src", "/assets/images/social/"+dados['redesSociais'][0].body_servico_redesSociais_Imagem1);
        $("#verImagemRedeSocial2").attr("src", "/assets/images/social/"+dados['redesSociais'][0].body_servico_redesSociais_Imagem2);
        $("#verImagemRedeSocial3").attr("src", "/assets/images/social/"+dados['redesSociais'][0].body_servico_redesSociais_Imagem3);

        
      }
    }
  );
}

function listarServicos(){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");

  $.ajax(
    {
        
      url: "/dashboard/listagens/listarServicos/"+numModelo[6],
      
      ajax: 'dados.json',
      
      success: function(dados){
          
        var dados = JSON.parse(dados);

        dadosGlobais = dados;
        
        if(dados['servico1'][0].body_servico1 === '1'){$('#servico1_ativaDesativa').val(1).attr("checked", "checked");}else{$('#servico1_ativaDesativa').val(1);}
        if(dados['servico1'][0].body_conteudoGenerico1 === '1'){$('#servico1_ConteudoGenerico').val(1).attr("checked", "checked");}else{$('#servico1_ConteudoGenerico').val(1);}
        if(dados['servico1'][0].body_servico1_opcao1 === '1'){$('#servico1_opcao1').val(1).attr("checked", "checked");}else{$('#servico1_opcao1').val(1);}
        if(dados['servico1'][0].body_servico1_opcao2 === '1'){$('#servico1_opcao2').val(1).attr("checked", "checked");}else{$('#servico1_opcao2').val(1);}
        if(dados['servico1'][0].body_servico1_opcao3 === '1'){$('#servico1_opcao3').val(1).attr("checked", "checked");}else{$('#servico1_opcao3').val(1);}
        if(dados['servico1'][0].body_servico1_opcao4 === '1'){$('#servico1_opcao4').val(1).attr("checked", "checked");}else{$('#servico1_opcao4').val(1);}
        if(dados['servico1'][0].body_servico1_opcao5 === '1'){$('#servico1_opcao5').val(1).attr("checked", "checked");}else{$('#servico1_opcao5').val(1);}
        if(dados['servico1'][0].body_servico1_opcao6 === '1'){$('#servico1_opcao6').val(1).attr("checked", "checked");}else{$('#servico1_opcao6').val(1);}
        if(dados['servico1'][0].body_servico1_opcao7 === '1'){$('#servico1_opcao7').val(1).attr("checked", "checked");}else{$('#servico1_opcao7').val(1);}
        if(dados['servico1'][0].body_servico1_opcao8 === '1'){$('#servico1_opcao8').val(1).attr("checked", "checked");}else{$('#servico1_opcao8').val(1);}
         
        $('#idModelo_servico1').val(dados['servico1'][0].id_modelo);
        $('#idLandingPage_servico1').val(dados['servico1'][0].id_landingPage);
        $('#idLandingPage_servico2').val(dados['servico2'][0].id_landingPage);
        $('#idLandingPage_servico3').val(dados['servico3'][0].id_landingPage);
        $('#idLandingPage_servico4').val(dados['servico4'][0].id_landingPage);
        $('#idLandingPage_servico5').val(dados['servico5'][0].id_landingPage);
        $('#idLandingPage_servico6').val(dados['servico6'][0].id_landingPage);
        $('#idLandingPage_servico7').val(dados['servico7'][0].id_landingPage);
        $('#idLandingPage_servico8').val(dados['servico8'][0].id_landingPage);

        $('#servico1_titulo').val(dados['servico1'][0].body_textoGenericoTitulo1);
        $('#servico1_descricao').val(dados['servico1'][0].body_textoGenericoDescricao1);
        $('#servico1_corTitulo').val(dados['servicos_personalizado'][0].servico1_corTituloGenerico1);
        $('#servico1_corDescricao').val(dados['servicos_personalizado'][0].servico1_corDescricaoGenerico1);
        $('#servico1_corFundoBG').val(dados['servicos_personalizado'][0].servico1_corBg);
        $('#servico1_corTextosProdsServs').val(dados['servicos_personalizado'][0].servico1_corTexto);
        $('#servico1_corFundoProdutosServicos').val(dados['servicos_personalizado'][0].servico1_corBoxImage);
        
        $("#servico1_imagem1").attr("src", "/assets/images/servicos/"+dados['servico1'][0].body_servico1Imagem1);
        $('#servico1_titulo_opcao1').val(dados['servico1'][0].body_servico1Titulo1);
        $('#servico1_descricao_opcao1').val(dados['servico1'][0].body_servico1Descricao1);
        $('#servico1_SEO_opcao1').val(dados['servico1'][0].body_servico1ImagemSEO1);

        $("#servico1_imagem2").attr("src", "/assets/images/servicos/"+dados['servico1'][0].body_servico1Imagem2);
        $('#servico1_titulo_opcao2').val(dados['servico1'][0].body_servico1Titulo2);
        $('#servico1_descricao_opcao2').val(dados['servico1'][0].body_servico1Descricao2);
        $('#servico1_SEO_opcao2').val(dados['servico1'][0].body_servico1ImagemSEO2);

        $("#servico1_imagem3").attr("src", "/assets/images/servicos/"+dados['servico1'][0].body_servico1Imagem3);
        $('#servico1_titulo_opcao3').val(dados['servico1'][0].body_servico1Titulo3);
        $('#servico1_descricao_opcao3').val(dados['servico1'][0].body_servico1Descricao3);
        $('#servico1_SEO_opcao3').val(dados['servico1'][0].body_servico1ImagemSEO3);

        $("#servico1_imagem4").attr("src", "/assets/images/servicos/"+dados['servico1'][0].body_servico1Imagem4);
        $('#servico1_titulo_opcao4').val(dados['servico1'][0].body_servico1Titulo4);
        $('#servico1_descricao_opcao4').val(dados['servico1'][0].body_servico1Descricao4);
        $('#servico1_SEO_opcao4').val(dados['servico1'][0].body_servico1ImagemSEO4);
        
        $("#servico1_imagem5").attr("src", "/assets/images/servicos/"+dados['servico1'][0].body_servico1Imagem5);
        $('#servico1_titulo_opcao5').val(dados['servico1'][0].body_servico1Titulo5);
        $('#servico1_descricao_opcao5').val(dados['servico1'][0].body_servico1Descricao5);
        $('#servico1_SEO_opcao5').val(dados['servico1'][0].body_servico1ImagemSEO5);

        $("#servico1_imagem6").attr("src", "/assets/images/servicos/"+dados['servico1'][0].body_servico1Imagem6);
        $('#servico1_titulo_opcao6').val(dados['servico1'][0].body_servico1Titulo6);
        $('#servico1_descricao_opcao6').val(dados['servico1'][0].body_servico1Descricao6);
        $('#servico1_SEO_opcao6').val(dados['servico1'][0].body_servico1ImagemSEO6);

        $("#servico1_imagem7").attr("src", "/assets/images/servicos/"+dados['servico1'][0].body_servico1Imagem7);
        $('#servico1_titulo_opcao7').val(dados['servico1'][0].body_servico1Titulo7);
        $('#servico1_descricao_opcao7').val(dados['servico1'][0].body_servico1Descricao7);
        $('#servico1_SEO_opcao7').val(dados['servico1'][0].body_servico1ImagemSEO7);

        $("#servico1_imagem8").attr("src", "/assets/images/servicos/"+dados['servico1'][0].body_servico1Imagem8);
        $('#servico1_titulo_opcao8').val(dados['servico1'][0].body_servico1Titulo8);
        $('#servico1_descricao_opcao8').val(dados['servico1'][0].body_servico1Descricao8);
        $('#servico1_SEO_opcao8').val(dados['servico1'][0].body_servico1ImagemSEO8);

        if(dados['servico2'][0].body_servico2 === '1'){$('#servico2_ativaDesativa').val(1).attr("checked", "checked");}else{$('#servico2_ativaDesativa').val(1);}
        if(dados['servico2'][0].body_conteudoGenerico2 === '1'){$('#servico2_ConteudoGenerico').val(1).attr("checked", "checked");}else{$('#servico2_ConteudoGenerico').val(1);}
        if(dados['servico2'][0].body_servico2_botao1 === '1'){$('#servico2_botao').val(1).attr("checked", "checked");}else{$('#servico2_botao').val(1);}
        
        $('#idModelo_servico2').val(dados['servico2'][0].id_modelo);

        $('#servico2_titulo').val(dados['servico2'][0].body_textoGenericoTitulo2);
        $('#servico2_descricao').val(dados['servico2'][0].body_textoGenericoDescricao2);
        $('#servico2_corTitulo').val(dados['servicos_personalizado'][0].servico2_corTituloGenerico1);
        $('#servico2_corDescricao').val(dados['servicos_personalizado'][0].servico2_corDescricaoGenerico1);
        $('#servico2_corFundoBG').val(dados['servicos_personalizado'][0].servico2_corBg);
        $("#verImagemServico2").attr("src", "/assets/images/servicos/"+dados['servico2'][0].body_servico2Imagem1);
        $('#servico2_textoBotao').val(dados['servico2'][0].body_servico2_botaoTexto1);
        $('#servico2_linkBotao').val(dados['servico2'][0].body_servico2_botaoLink1);
        $('#servico2_corTextoBotao').val(dados['servicos_personalizado'][0].servico2_corTextoBotao);
        $('#servico2_corTextoHoverBotao').val(dados['servicos_personalizado'][0].servico2_corTextoHoverBotao);
        $('#servico2_corFundoBotao').val(dados['servicos_personalizado'][0].servico2_corFundoBotao);
        $('#servico2_corFundoHoverBotao').val(dados['servicos_personalizado'][0].servico2_corFundoHoverBotao);
        $('#servico2_imagemSEO').val(dados['servico2'][0].body_servico2ImagemSEO1);

        $('#idModelo_servico3').val(dados['servico3'][0].id_modelo);

        if(dados['servico3'][0].body_servico3 === '1'){$('#servico3_ativaDesativa').val(1).attr("checked", "checked");}else{$('#servico3_ativaDesativa').val(1);}
        if(dados['servico3'][0].body_conteudoGenerico3 === '1'){$('#servico3_ConteudoGenerico').val(1).attr("checked", "checked");}else{$('#servico3_ConteudoGenerico').val(1);}
        if(dados['servico3'][0].body_servico3_TodasOpcoes === '1'){$('#servico3_todasOpcoes').val(1).attr("checked", "checked");}else{$('#servico3_todasOpcoes').val(1);}
        if(dados['servico3'][0].body_servico3_opcao1 === '1'){$('#servico3_opcao1').val(1).attr("checked", "checked");}else{$('#servico3_opcao1').val(1);}
        if(dados['servico3'][0].body_servico3_opcao2 === '1'){$('#servico3_opcao2').val(1).attr("checked", "checked");}else{$('#servico3_opcao2').val(1);}
        if(dados['servico3'][0].body_servico3_opcao3 === '1'){$('#servico3_opcao3').val(1).attr("checked", "checked");}else{$('#servico3_opcao3').val(1);}
        if(dados['servico3'][0].body_servico3_opcao4 === '1'){$('#servico3_opcao4').val(1).attr("checked", "checked");}else{$('#servico3_opcao4').val(1);}
        $('#servico3_titulo').val(dados['servico3'][0].body_textoGenericoTitulo3);
        $('#servico3_descricao').val(dados['servico3'][0].body_textoGenericoDescricao3);
        $('#servico3_corTitulo').val(dados['servicos_personalizado'][0].servico3_corTituloGenerico1);
        $('#servico3_corDescricao').val(dados['servicos_personalizado'][0].servico3_corDescricaoGenerico1);
        $('#servico3_corFundoBG').val(dados['servicos_personalizado'][0].servico3_corBg);
        $('#servico3_corTextos').val(dados['servicos_personalizado'][0].servico3_corTexto);
        $('#servico3_corFundoImagens').val(dados['servicos_personalizado'][0].servico3_corBoxImage);
        $("#verImagemServico3").attr("src", "/assets/images/servicos/"+dados['servico3'][0].body_servico3Imagem5);
        $('#servico3_imagemSEO5').val(dados['servico3'][0].body_servico3ImagemSEO5);
        
        $("#servico3_imagem1").attr("src", "/assets/images/servicos/"+dados['servico3'][0].body_servico3Imagem1);
        $('#servico3_titulo_opcao1').val(dados['servico3'][0].body_servico3Titulo1);
        $('#servico3_descricao_opcao1').val(dados['servico3'][0].body_servico3Descricao1);
        $('#servico3_SEO_opcao1').val(dados['servico3'][0].body_servico3ImagemSEO1);

        $("#servico3_imagem2").attr("src", "/assets/images/servicos/"+dados['servico3'][0].body_servico3Imagem2);
        $('#servico3_titulo_opcao2').val(dados['servico3'][0].body_servico3Titulo2);
        $('#servico3_descricao_opcao2').val(dados['servico3'][0].body_servico3Descricao2);
        $('#servico3_SEO_opcao2').val(dados['servico3'][0].body_servico3ImagemSEO2);

        $("#servico3_imagem3").attr("src", "/assets/images/servicos/"+dados['servico3'][0].body_servico3Imagem3);
        $('#servico3_titulo_opcao3').val(dados['servico3'][0].body_servico3Titulo3);
        $('#servico3_descricao_opcao3').val(dados['servico3'][0].body_servico3Descricao3);
        $('#servico3_SEO_opcao3').val(dados['servico3'][0].body_servico3ImagemSEO3);

        $("#servico3_imagem4").attr("src", "/assets/images/servicos/"+dados['servico3'][0].body_servico3Imagem4);
        $('#servico3_titulo_opcao4').val(dados['servico3'][0].body_servico3Titulo4);
        $('#servico3_descricao_opcao4').val(dados['servico3'][0].body_servico3Descricao4);
        $('#servico3_SEO_opcao4').val(dados['servico3'][0].body_servico3ImagemSEO4);

        if(dados['servico4'][0].body_servico4 === '1'){$('#servico4_ativaDesativa').val(1).attr("checked", "checked");}else{$('#servico4_ativaDesativa').val(1);}
        if(dados['servico4'][0].body_conteudoGenerico4 === '1'){$('#servico4_ConteudoGenerico').val(1).attr("checked", "checked");}else{$('#servico4_ConteudoGenerico').val(1);}
        if(dados['servico4'][0].body_servico4_botao1 === '1'){$('#servico4_botao').val(1).attr("checked", "checked");}else{$('#servico4_botao').val(1);}
        
        $('#idModelo_servico4').val(dados['servico2'][0].id_modelo);

        $('#servico4_titulo').val(dados['servico4'][0].body_textoGenericoTitulo4);
        $('#servico4_descricao').val(dados['servico4'][0].body_textoGenericoDescricao4);
        $('#servico4_corTitulo').val(dados['servicos_personalizado'][0].servico4_corTituloGenerico1);
        $('#servico4_corDescricao').val(dados['servicos_personalizado'][0].servico4_corDescricaoGenerico1);
        $('#servico4_corFundoBG').val(dados['servicos_personalizado'][0].servico4_corBg);
        $("#verImagemServico4").attr("src", "/assets/images/servicos/"+dados['servico4'][0].body_servico4Imagem1);
        $('#servico4_textoBotao').val(dados['servico4'][0].body_servico4_botaoTexto1);
        $('#servico4_linkBotao').val(dados['servico4'][0].body_servico4_botaoLink1);
        
        $('#servico4_corTextoBotao').val(dados['servicos_personalizado'][0].servico4_corTextoBotao);
        $('#servico4_corTextoHoverBotao').val(dados['servicos_personalizado'][0].servico4_corTextoHoverBotao);
        $('#servico4_corFundoBotao').val(dados['servicos_personalizado'][0].servico4_corFundoBotao);
        $('#servico4_corFundoHoverBotao').val(dados['servicos_personalizado'][0].servico4_corFundoHoverBotao);
        $('#servico4_imagemSEO').val(dados['servico4'][0].body_servico4ImagemSEO1);
        

        // INICIO SERVIÇO 5
        $('#idModelo_servico5').val(dados['servico5'][0].id_modelo);

        if(dados['servico5'][0].body_servico5 === '1'){$('#servico5_ativaDesativa').val(1).attr("checked", "checked");}else{$('#servico5_ativaDesativa').val(1);}
        if(dados['servico5'][0].body_conteudoGenerico5 === '1'){$('#servico5_ConteudoGenerico').val(1).attr("checked", "checked");}else{$('#servico5_ConteudoGenerico').val(1);}
        
        $('#servico5_titulo').val(dados['servico5'][0].body_textoGenericoTitulo5);     
        $('#servico5_descricao').val(dados['servico5'][0].body_textoGenericoDescricao5);     
        $('#servico5_corTitulo').val(dados['servicos_personalizado'][0].servico5_corTituloGenerico1);     
        $('#servico5_corDescricao').val(dados['servicos_personalizado'][0].servico5_corDescricaoGenerico1);     
        $('#servico5_corFundoBG').val(dados['servicos_personalizado'][0].servico5_corBg);     
        $('#servico5_corTextos').val(dados['servicos_personalizado'][0].servico5_corTexto);
        
        if(dados['servico5'][0].body_servico5_opcao1 === '1'){$('#servico5_opcao1').val(1).attr("checked", "checked");}else{$('#servico5_opcao1').val(1);}
        if(dados['servico5'][0].body_servico5_opcao2 === '1'){$('#servico5_opcao2').val(1).attr("checked", "checked");}else{$('#servico5_opcao2').val(1);}
        if(dados['servico5'][0].body_servico5_opcao3 === '1'){$('#servico5_opcao3').val(1).attr("checked", "checked");}else{$('#servico5_opcao3').val(1);}
        if(dados['servico5'][0].body_servico5_opcao4 === '1'){$('#servico5_opcao4').val(1).attr("checked", "checked");}else{$('#servico5_opcao4').val(1);}
        if(dados['servico5'][0].body_servico5_opcao5 === '1'){$('#servico5_opcao5').val(1).attr("checked", "checked");}else{$('#servico5_opcao5').val(1);}
        if(dados['servico5'][0].body_servico5_opcao6 === '1'){$('#servico5_opcao6').val(1).attr("checked", "checked");}else{$('#servico5_opcao6').val(1);}
        if(dados['servico5'][0].body_servico5_opcao7 === '1'){$('#servico5_opcao7').val(1).attr("checked", "checked");}else{$('#servico5_opcao7').val(1);}
        if(dados['servico5'][0].body_servico5_opcao8 === '1'){$('#servico5_opcao8').val(1).attr("checked", "checked");}else{$('#servico5_opcao8').val(1);}
        
        if(dados['servico5'][0].body_servico5_preco1_opcao1 === '1'){$('#servico5_preco1_botao1').val(1).attr("checked", "checked");}else{$('#servico5_preco1_botao1').val(1);}
        if(dados['servico5'][0].body_servico5_preco2_opcao2 === '1'){$('#servico5_preco2_botao2').val(1).attr("checked", "checked");}else{$('#servico5_preco2_botao2').val(1);}
        if(dados['servico5'][0].body_servico5_preco3_opcao3 === '1'){$('#servico5_preco3_botao3').val(1).attr("checked", "checked");}else{$('#servico5_preco3_botao3').val(1);}
        if(dados['servico5'][0].body_servico5_preco4_opcao4 === '1'){$('#servico5_preco4_botao4').val(1).attr("checked", "checked");}else{$('#servico5_preco4_botao4').val(1);}
        if(dados['servico5'][0].body_servico5_preco5_opcao5 === '1'){$('#servico5_preco5_botao5').val(1).attr("checked", "checked");}else{$('#servico5_preco5_botao5').val(1);}
        if(dados['servico5'][0].body_servico5_preco6_opcao6 === '1'){$('#servico5_preco6_botao6').val(1).attr("checked", "checked");}else{$('#servico5_preco6_botao6').val(1);}
        if(dados['servico5'][0].body_servico5_preco7_opcao7 === '1'){$('#servico5_preco7_botao7').val(1).attr("checked", "checked");}else{$('#servico5_preco7_botao7').val(1);}
        if(dados['servico5'][0].body_servico5_preco8_opcao8 === '1'){$('#servico5_preco8_botao8').val(1).attr("checked", "checked");}else{$('#servico5_preco8_botao8').val(1);}

        $("#servico5_imagem1").attr("src", "/assets/images/servicos/"+dados['servico5'][0].body_servico5Imagem1);
        $("#servico5_imagem2").attr("src", "/assets/images/servicos/"+dados['servico5'][0].body_servico5Imagem2);
        $("#servico5_imagem3").attr("src", "/assets/images/servicos/"+dados['servico5'][0].body_servico5Imagem3);
        $("#servico5_imagem4").attr("src", "/assets/images/servicos/"+dados['servico5'][0].body_servico5Imagem4);
        $("#servico5_imagem5").attr("src", "/assets/images/servicos/"+dados['servico5'][0].body_servico5Imagem5);
        $("#servico5_imagem6").attr("src", "/assets/images/servicos/"+dados['servico5'][0].body_servico5Imagem6);
        $("#servico5_imagem7").attr("src", "/assets/images/servicos/"+dados['servico5'][0].body_servico5Imagem7);
        $("#servico5_imagem8").attr("src", "/assets/images/servicos/"+dados['servico5'][0].body_servico5Imagem8);

        
        $('#servico5_titulo_opcao1').val(dados['servico5'][0].body_servico5Titulo1);
        $('#servico5_descricao_opcao1').val(dados['servico5'][0].body_servico5Descricao1);
        $('#servico5_SEO_opcao1').val(dados['servico5'][0].body_servico5ImagemSEO1);
        $('#servico5_preco_opcao1').val(dados['servico5'][0].body_servico5_preco1);
        $('#servico5_descricaoRapida_opcao1_opcao1').val(dados['servico5'][0].body_servico5_opcao1Descricao1);
        $('#servico5_descricaoRapida_opcao1_opcao2').val(dados['servico5'][0].body_servico5_opcao1Descricao2);
        $('#servico5_descricaoRapida_opcao1_opcao3').val(dados['servico5'][0].body_servico5_opcao1Descricao3);
        $('#servico5_descricaoRapida_opcao1_opcao4').val(dados['servico5'][0].body_servico5_opcao1Descricao4);
        $('#servico5_descricaoRapida_opcao1_opcao5').val(dados['servico5'][0].body_servico5_opcao1Descricao5);
        $('#servico5_descricaoRapida_opcao1_opcao6').val(dados['servico5'][0].body_servico5_opcao1Descricao6);
        $('#servico5_descricaoRapida_opcao1_opcao7').val(dados['servico5'][0].body_servico5_opcao1Descricao7);
        $('#servico5_descricaoRapida_opcao1_opcao8').val(dados['servico5'][0].body_servico5_opcao1Descricao8);
        $('#servico5_descricaoRapida_opcao1_opcao9').val(dados['servico5'][0].body_servico5_opcao1Descricao9);
        $('#servico5_descricaoRapida_opcao1_opcao10').val(dados['servico5'][0].body_servico5_opcao1Descricao10);

        $('#servico5_titulo_opcao2').val(dados['servico5'][0].body_servico5Titulo2);
        $('#servico5_descricao_opcao2').val(dados['servico5'][0].body_servico5Descricao2);
        $('#servico5_SEO_opcao2').val(dados['servico5'][0].body_servico5ImagemSEO2);
        $('#servico5_preco_opcao2').val(dados['servico5'][0].body_servico5_preco2);
        $('#servico5_descricaoRapida_opcao2_opcao1').val(dados['servico5'][0].body_servico5_opcao2Descricao1);
        $('#servico5_descricaoRapida_opcao2_opcao2').val(dados['servico5'][0].body_servico5_opcao2Descricao2);
        $('#servico5_descricaoRapida_opcao2_opcao3').val(dados['servico5'][0].body_servico5_opcao2Descricao3);
        $('#servico5_descricaoRapida_opcao2_opcao4').val(dados['servico5'][0].body_servico5_opcao2Descricao4);
        $('#servico5_descricaoRapida_opcao2_opcao5').val(dados['servico5'][0].body_servico5_opcao2Descricao5);
        $('#servico5_descricaoRapida_opcao2_opcao6').val(dados['servico5'][0].body_servico5_opcao2Descricao6);
        $('#servico5_descricaoRapida_opcao2_opcao7').val(dados['servico5'][0].body_servico5_opcao2Descricao7);
        $('#servico5_descricaoRapida_opcao2_opcao8').val(dados['servico5'][0].body_servico5_opcao2Descricao8);
        $('#servico5_descricaoRapida_opcao2_opcao9').val(dados['servico5'][0].body_servico5_opcao2Descricao9);
        $('#servico5_descricaoRapida_opcao2_opcao10').val(dados['servico5'][0].body_servico5_opcao2Descricao10);

        $('#servico5_titulo_opcao3').val(dados['servico5'][0].body_servico5Titulo3);
        $('#servico5_descricao_opcao3').val(dados['servico5'][0].body_servico5Descricao3);
        $('#servico5_SEO_opcao3').val(dados['servico5'][0].body_servico5ImagemSEO3);
        $('#servico5_preco_opcao3').val(dados['servico5'][0].body_servico5_preco3);
        $('#servico5_descricaoRapida_opcao3_opcao1').val(dados['servico5'][0].body_servico5_opcao3Descricao1);
        $('#servico5_descricaoRapida_opcao3_opcao2').val(dados['servico5'][0].body_servico5_opcao3Descricao2);
        $('#servico5_descricaoRapida_opcao3_opcao3').val(dados['servico5'][0].body_servico5_opcao3Descricao3);
        $('#servico5_descricaoRapida_opcao3_opcao4').val(dados['servico5'][0].body_servico5_opcao3Descricao4);
        $('#servico5_descricaoRapida_opcao3_opcao5').val(dados['servico5'][0].body_servico5_opcao3Descricao5);
        $('#servico5_descricaoRapida_opcao3_opcao6').val(dados['servico5'][0].body_servico5_opcao3Descricao6);
        $('#servico5_descricaoRapida_opcao3_opcao7').val(dados['servico5'][0].body_servico5_opcao3Descricao7);
        $('#servico5_descricaoRapida_opcao3_opcao8').val(dados['servico5'][0].body_servico5_opcao3Descricao8);
        $('#servico5_descricaoRapida_opcao3_opcao9').val(dados['servico5'][0].body_servico5_opcao3Descricao9);
        $('#servico5_descricaoRapida_opcao3_opcao10').val(dados['servico5'][0].body_servico5_opcao3Descricao10);

        $('#servico5_titulo_opcao4').val(dados['servico5'][0].body_servico5Titulo4);
        $('#servico5_descricao_opcao4').val(dados['servico5'][0].body_servico5Descricao4);
        $('#servico5_SEO_opcao4').val(dados['servico5'][0].body_servico5ImagemSEO4);
        $('#servico5_preco_opcao4').val(dados['servico5'][0].body_servico5_preco4);
        $('#servico5_descricaoRapida_opcao4_opcao1').val(dados['servico5'][0].body_servico5_opcao4Descricao1);
        $('#servico5_descricaoRapida_opcao4_opcao2').val(dados['servico5'][0].body_servico5_opcao4Descricao2);
        $('#servico5_descricaoRapida_opcao4_opcao3').val(dados['servico5'][0].body_servico5_opcao4Descricao3);
        $('#servico5_descricaoRapida_opcao4_opcao4').val(dados['servico5'][0].body_servico5_opcao4Descricao4);
        $('#servico5_descricaoRapida_opcao4_opcao5').val(dados['servico5'][0].body_servico5_opcao4Descricao5);
        $('#servico5_descricaoRapida_opcao4_opcao6').val(dados['servico5'][0].body_servico5_opcao4Descricao6);
        $('#servico5_descricaoRapida_opcao4_opcao7').val(dados['servico5'][0].body_servico5_opcao4Descricao7);
        $('#servico5_descricaoRapida_opcao4_opcao8').val(dados['servico5'][0].body_servico5_opcao4Descricao8);
        $('#servico5_descricaoRapida_opcao4_opcao9').val(dados['servico5'][0].body_servico5_opcao4Descricao9);
        $('#servico5_descricaoRapida_opcao4_opcao10').val(dados['servico5'][0].body_servico5_opcao4Descricao10);

        $('#servico5_titulo_opcao5').val(dados['servico5'][0].body_servico5Titulo5);
        $('#servico5_descricao_opcao5').val(dados['servico5'][0].body_servico5Descricao5);
        $('#servico5_SEO_opcao5').val(dados['servico5'][0].body_servico5ImagemSEO5);
        $('#servico5_preco_opcao5').val(dados['servico5'][0].body_servico5_preco5);
        $('#servico5_descricaoRapida_opcao5_opcao1').val(dados['servico5'][0].body_servico5_opcao5Descricao1);
        $('#servico5_descricaoRapida_opcao5_opcao2').val(dados['servico5'][0].body_servico5_opcao5Descricao2);
        $('#servico5_descricaoRapida_opcao5_opcao3').val(dados['servico5'][0].body_servico5_opcao5Descricao3);
        $('#servico5_descricaoRapida_opcao5_opcao4').val(dados['servico5'][0].body_servico5_opcao5Descricao4);
        $('#servico5_descricaoRapida_opcao5_opcao5').val(dados['servico5'][0].body_servico5_opcao5Descricao5);
        $('#servico5_descricaoRapida_opcao5_opcao6').val(dados['servico5'][0].body_servico5_opcao5Descricao6);
        $('#servico5_descricaoRapida_opcao5_opcao7').val(dados['servico5'][0].body_servico5_opcao5Descricao7);
        $('#servico5_descricaoRapida_opcao5_opcao8').val(dados['servico5'][0].body_servico5_opcao5Descricao8);
        $('#servico5_descricaoRapida_opcao5_opcao9').val(dados['servico5'][0].body_servico5_opcao5Descricao9);
        $('#servico5_descricaoRapida_opcao5_opcao10').val(dados['servico5'][0].body_servico5_opcao5Descricao10);

        $('#servico5_titulo_opcao6').val(dados['servico5'][0].body_servico5Titulo6);
        $('#servico5_descricao_opcao6').val(dados['servico5'][0].body_servico5Descricao6);
        $('#servico5_SEO_opcao6').val(dados['servico5'][0].body_servico5ImagemSEO6);
        $('#servico5_preco_opcao6').val(dados['servico5'][0].body_servico5_preco6);
        $('#servico5_descricaoRapida_opcao6_opcao1').val(dados['servico5'][0].body_servico5_opcao6Descricao1);
        $('#servico5_descricaoRapida_opcao6_opcao2').val(dados['servico5'][0].body_servico5_opcao6Descricao2);
        $('#servico5_descricaoRapida_opcao6_opcao3').val(dados['servico5'][0].body_servico5_opcao6Descricao3);
        $('#servico5_descricaoRapida_opcao6_opcao4').val(dados['servico5'][0].body_servico5_opcao6Descricao4);
        $('#servico5_descricaoRapida_opcao6_opcao5').val(dados['servico5'][0].body_servico5_opcao6Descricao5);
        $('#servico5_descricaoRapida_opcao6_opcao6').val(dados['servico5'][0].body_servico5_opcao6Descricao6);
        $('#servico5_descricaoRapida_opcao6_opcao7').val(dados['servico5'][0].body_servico5_opcao6Descricao7);
        $('#servico5_descricaoRapida_opcao6_opcao8').val(dados['servico5'][0].body_servico5_opcao6Descricao8);
        $('#servico5_descricaoRapida_opcao6_opcao9').val(dados['servico5'][0].body_servico5_opcao6Descricao9);
        $('#servico5_descricaoRapida_opcao6_opcao10').val(dados['servico5'][0].body_servico5_opcao6Descricao10);

        $('#servico5_titulo_opcao7').val(dados['servico5'][0].body_servico5Titulo7);
        $('#servico5_descricao_opcao7').val(dados['servico5'][0].body_servico5Descricao7);
        $('#servico5_SEO_opcao7').val(dados['servico5'][0].body_servico5ImagemSEO7);
        $('#servico5_preco_opcao7').val(dados['servico5'][0].body_servico5_preco7);
        $('#servico5_descricaoRapida_opcao7_opcao1').val(dados['servico5'][0].body_servico5_opcao7Descricao1);
        $('#servico5_descricaoRapida_opcao7_opcao2').val(dados['servico5'][0].body_servico5_opcao7Descricao2);
        $('#servico5_descricaoRapida_opcao7_opcao3').val(dados['servico5'][0].body_servico5_opcao7Descricao3);
        $('#servico5_descricaoRapida_opcao7_opcao4').val(dados['servico5'][0].body_servico5_opcao7Descricao4);
        $('#servico5_descricaoRapida_opcao7_opcao5').val(dados['servico5'][0].body_servico5_opcao7Descricao5);
        $('#servico5_descricaoRapida_opcao7_opcao6').val(dados['servico5'][0].body_servico5_opcao7Descricao6);
        $('#servico5_descricaoRapida_opcao7_opcao7').val(dados['servico5'][0].body_servico5_opcao7Descricao7);
        $('#servico5_descricaoRapida_opcao7_opcao8').val(dados['servico5'][0].body_servico5_opcao7Descricao8);
        $('#servico5_descricaoRapida_opcao7_opcao9').val(dados['servico5'][0].body_servico5_opcao7Descricao9);
        $('#servico5_descricaoRapida_opcao7_opcao10').val(dados['servico5'][0].body_servico5_opcao7Descricao10);

        $('#servico5_titulo_opcao8').val(dados['servico5'][0].body_servico5Titulo8);
        $('#servico5_descricao_opcao8').val(dados['servico5'][0].body_servico5Descricao8);
        $('#servico5_SEO_opcao8').val(dados['servico5'][0].body_servico5ImagemSEO8);
        $('#servico5_preco_opcao8').val(dados['servico5'][0].body_servico5_preco8);
        $('#servico5_descricaoRapida_opcao8_opcao1').val(dados['servico5'][0].body_servico5_opcao8Descricao1);
        $('#servico5_descricaoRapida_opcao8_opcao2').val(dados['servico5'][0].body_servico5_opcao8Descricao2);
        $('#servico5_descricaoRapida_opcao8_opcao3').val(dados['servico5'][0].body_servico5_opcao8Descricao3);
        $('#servico5_descricaoRapida_opcao8_opcao4').val(dados['servico5'][0].body_servico5_opcao8Descricao4);
        $('#servico5_descricaoRapida_opcao8_opcao5').val(dados['servico5'][0].body_servico5_opcao8Descricao5);
        $('#servico5_descricaoRapida_opcao8_opcao6').val(dados['servico5'][0].body_servico5_opcao8Descricao6);
        $('#servico5_descricaoRapida_opcao8_opcao7').val(dados['servico5'][0].body_servico5_opcao8Descricao7);
        $('#servico5_descricaoRapida_opcao8_opcao8').val(dados['servico5'][0].body_servico5_opcao8Descricao8);
        $('#servico5_descricaoRapida_opcao8_opcao9').val(dados['servico5'][0].body_servico5_opcao8Descricao9);
        $('#servico5_descricaoRapida_opcao8_opcao10').val(dados['servico5'][0].body_servico5_opcao8Descricao10);

        $('#servico5_titulo_opcao9').val(dados['servico5'][0].body_servico5Titulo9);
        $('#servico5_descricao_opcao9').val(dados['servico5'][0].body_servico5Descricao9);
        $('#servico5_SEO_opcao9').val(dados['servico5'][0].body_servico5ImagemSEO9);
        $('#servico5_preco_opcao9').val(dados['servico5'][0].body_servico5_preco9);
        $('#servico5_descricaoRapida_opcao9_opcao1').val(dados['servico5'][0].body_servico5_opcao9Descricao1);
        $('#servico5_descricaoRapida_opcao9_opcao2').val(dados['servico5'][0].body_servico5_opcao9Descricao2);
        $('#servico5_descricaoRapida_opcao9_opcao3').val(dados['servico5'][0].body_servico5_opcao9Descricao3);
        $('#servico5_descricaoRapida_opcao9_opcao4').val(dados['servico5'][0].body_servico5_opcao9Descricao4);
        $('#servico5_descricaoRapida_opcao9_opcao5').val(dados['servico5'][0].body_servico5_opcao9Descricao5);
        $('#servico5_descricaoRapida_opcao9_opcao6').val(dados['servico5'][0].body_servico5_opcao9Descricao6);
        $('#servico5_descricaoRapida_opcao9_opcao7').val(dados['servico5'][0].body_servico5_opcao9Descricao7);
        $('#servico5_descricaoRapida_opcao9_opcao8').val(dados['servico5'][0].body_servico5_opcao9Descricao8);
        $('#servico5_descricaoRapida_opcao9_opcao9').val(dados['servico5'][0].body_servico5_opcao9Descricao9);
        $('#servico5_descricaoRapida_opcao9_opcao10').val(dados['servico5'][0].body_servico5_opcao9Descricao10);

        $('#servico5_titulo_opcao10').val(dados['servico5'][0].body_servico5Titulo10);
        $('#servico5_descricao_opcao10').val(dados['servico5'][0].body_servico5Descricao10);
        $('#servico5_SEO_opcao10').val(dados['servico5'][0].body_servico5ImagemSEO10);
        $('#servico5_preco_opcao10').val(dados['servico5'][0].body_servico5_preco10);
        $('#servico5_descricaoRapida_opcao10_opcao1').val(dados['servico5'][0].body_servico5_opcao10Descricao1);
        $('#servico5_descricaoRapida_opcao10_opcao2').val(dados['servico5'][0].body_servico5_opcao10Descricao2);
        $('#servico5_descricaoRapida_opcao10_opcao3').val(dados['servico5'][0].body_servico5_opcao10Descricao3);
        $('#servico5_descricaoRapida_opcao10_opcao4').val(dados['servico5'][0].body_servico5_opcao10Descricao4);
        $('#servico5_descricaoRapida_opcao10_opcao5').val(dados['servico5'][0].body_servico5_opcao10Descricao5);
        $('#servico5_descricaoRapida_opcao10_opcao6').val(dados['servico5'][0].body_servico5_opcao10Descricao6);
        $('#servico5_descricaoRapida_opcao10_opcao7').val(dados['servico5'][0].body_servico5_opcao10Descricao7);
        $('#servico5_descricaoRapida_opcao10_opcao8').val(dados['servico5'][0].body_servico5_opcao10Descricao8);
        $('#servico5_descricaoRapida_opcao10_opcao9').val(dados['servico5'][0].body_servico5_opcao10Descricao9);
        $('#servico5_descricaoRapida_opcao10_opcao10').val(dados['servico5'][0].body_servico5_opcao10Descricao10);
        
        $('#corTextoBotao_servico5').val(dados['servicos_personalizado'][0].servico5_corTextoBotao);
        $('#corTextoHoverBotao_servico5').val(dados['servicos_personalizado'][0].servico5_corTextoHoverBotao);
        $('#corfundoBotao_servico5').val(dados['servicos_personalizado'][0].servico5_corFundoBotao);
        $('#corFundoHoverBotao_servico5').val(dados['servicos_personalizado'][0].servico5_corFundoHoverBotao);
        $('#servico5_corFundoBOXImagem').val(dados['servicos_personalizado'][0].servico5_corBoxImage);
        
        $('#servico5_texoBotao_opcao1').val(dados['servico5'][0].body_servico5_botaoTexto1);
        $('#servico5_texoBotao_opcao2').val(dados['servico5'][0].body_servico5_botaoTexto2);
        $('#servico5_texoBotao_opcao3').val(dados['servico5'][0].body_servico5_botaoTexto3);
        $('#servico5_texoBotao_opcao4').val(dados['servico5'][0].body_servico5_botaoTexto4);
        $('#servico5_texoBotao_opcao5').val(dados['servico5'][0].body_servico5_botaoTexto5);
        $('#servico5_texoBotao_opcao6').val(dados['servico5'][0].body_servico5_botaoTexto6);
        $('#servico5_texoBotao_opcao7').val(dados['servico5'][0].body_servico5_botaoTexto7);
        $('#servico5_texoBotao_opcao8').val(dados['servico5'][0].body_servico5_botaoTexto8);

        $('#servico5_linkBotao_opcao1').val(dados['servico5'][0].body_servico5_botaoLink1);
        $('#servico5_linkBotao_opcao2').val(dados['servico5'][0].body_servico5_botaoLink2);
        $('#servico5_linkBotao_opcao3').val(dados['servico5'][0].body_servico5_botaoLink3);
        $('#servico5_linkBotao_opcao4').val(dados['servico5'][0].body_servico5_botaoLink4);
        $('#servico5_linkBotao_opcao5').val(dados['servico5'][0].body_servico5_botaoLink5);
        $('#servico5_linkBotao_opcao6').val(dados['servico5'][0].body_servico5_botaoLink6);
        $('#servico5_linkBotao_opcao7').val(dados['servico5'][0].body_servico5_botaoLink7);
        $('#servico5_linkBotao_opcao8').val(dados['servico5'][0].body_servico5_botaoLink8);
        
        if(dados['servico5'][0].body_servico5_botao1 === '1'){$('#servico5_botao1').val(1).attr("checked", "checked");}else{$('#servico5_botao1').val(1);}
        if(dados['servico5'][0].body_servico5_botao2 === '1'){$('#servico5_botao2').val(1).attr("checked", "checked");}else{$('#servico5_botao2').val(1);}
        if(dados['servico5'][0].body_servico5_botao3 === '1'){$('#servico5_botao3').val(1).attr("checked", "checked");}else{$('#servico5_botao3').val(1);}
        if(dados['servico5'][0].body_servico5_botao4 === '1'){$('#servico5_botao4').val(1).attr("checked", "checked");}else{$('#servico5_botao4').val(1);}
        if(dados['servico5'][0].body_servico5_botao5 === '1'){$('#servico5_botao5').val(1).attr("checked", "checked");}else{$('#servico5_botao5').val(1);}
        if(dados['servico5'][0].body_servico5_botao6 === '1'){$('#servico5_botao6').val(1).attr("checked", "checked");}else{$('#servico5_botao6').val(1);}
        if(dados['servico5'][0].body_servico5_botao7 === '1'){$('#servico5_botao7').val(1).attr("checked", "checked");}else{$('#servico5_botao7').val(1);}
        if(dados['servico5'][0].body_servico5_botao8 === '1'){$('#servico5_botao8').val(1).attr("checked", "checked");}else{$('#servico5_botao8').val(1);}
        
        // FIM SERVIÇO 5

        $('#idModelo_servico6').val(dados['servico6'][0].id_modelo);
        
        if(dados['servico6'][0].body_servico6 === '1'){$('#servico6_ativaDesativa').val(1).attr("checked", "checked");}else{$('#servico6_ativaDesativa').val(1);}
        if(dados['servico6'][0].body_conteudoGenerico6 === '1'){$('#servico6_ConteudoGenerico').val(1).attr("checked", "checked");}else{$('#servico6_ConteudoGenerico').val(1);}
        if(dados['servico6'][0].body_servico6_opcao1 === '1'){$('#servico6_opcao1').val(1).attr("checked", "checked");}else{$('#servico6_opcao1').val(1);}
        if(dados['servico6'][0].body_servico6_opcao2 === '1'){$('#servico6_opcao2').val(1).attr("checked", "checked");}else{$('#servico6_opcao2').val(1);}
        if(dados['servico6'][0].body_servico6_opcao3 === '1'){$('#servico6_opcao3').val(1).attr("checked", "checked");}else{$('#servico6_opcao3').val(1);}
        if(dados['servico6'][0].body_servico6_opcao4 === '1'){$('#servico6_opcao4').val(1).attr("checked", "checked");}else{$('#servico6_opcao4').val(1);}
        if(dados['servico6'][0].body_servico6_opcao5 === '1'){$('#servico6_opcao5').val(1).attr("checked", "checked");}else{$('#servico6_opcao5').val(1);}
        if(dados['servico6'][0].body_servico6_opcao6 === '1'){$('#servico6_opcao6').val(1).attr("checked", "checked");}else{$('#servico6_opcao6').val(1);}
        if(dados['servico6'][0].body_servico6_opcao7 === '1'){$('#servico6_opcao7').val(1).attr("checked", "checked");}else{$('#servico6_opcao7').val(1);}
        if(dados['servico6'][0].body_servico6_opcao8 === '1'){$('#servico6_opcao8').val(1).attr("checked", "checked");}else{$('#servico6_opcao8').val(1);}
    
        $('#servico6_titulo').val(dados['servico6'][0].body_textoGenericoTitulo6);
        $('#servico6_descricao').val(dados['servico6'][0].body_textoGenericoDescricao6);
        
        $('#servico6_corTitulo').val(dados['servicos_personalizado'][0].servico6_corTituloGenerico1);
        $('#servico6_corDescricao').val(dados['servicos_personalizado'][0].servico6_corDescricaoGenerico1);
        $('#servico6_corFundoBG').val(dados['servicos_personalizado'][0].servico6_corBg);
        $('#servico6_corTextos').val(dados['servicos_personalizado'][0].servico6_corTexto);
        
        $("#servico6_imagem1").attr("src", "/assets/images/servicos/"+dados['servico6'][0].body_servico6Imagem1);
        $('#servico6_SEO_opcao1').val(dados['servico6'][0].body_servico6ImagemSEO1);
        $('#servico6_titulo_opcao1').val(dados['servico6'][0].body_servico6Titulo1);
        $('#servico6_descricao_opcao1').val(dados['servico6'][0].body_servico6Descricao1);

        $("#servico6_imagem2").attr("src", "/assets/images/servicos/"+dados['servico6'][0].body_servico6Imagem2);
        $('#servico6_SEO_opcao2').val(dados['servico6'][0].body_servico6ImagemSEO2);
        $('#servico6_titulo_opcao2').val(dados['servico6'][0].body_servico6Titulo2);
        $('#servico6_descricao_opcao2').val(dados['servico6'][0].body_servico6Descricao2);
        
        $("#servico6_imagem3").attr("src", "/assets/images/servicos/"+dados['servico6'][0].body_servico6Imagem3);
        $('#servico6_SEO_opcao3').val(dados['servico6'][0].body_servico6ImagemSEO3);
        $('#servico6_titulo_opcao3').val(dados['servico6'][0].body_servico6Titulo3);
        $('#servico6_descricao_opcao3').val(dados['servico6'][0].body_servico6Descricao3);

        $("#servico6_imagem4").attr("src", "/assets/images/servicos/"+dados['servico6'][0].body_servico6Imagem4);
        $('#servico6_SEO_opcao4').val(dados['servico6'][0].body_servico6ImagemSEO4);
        $('#servico6_titulo_opcao4').val(dados['servico6'][0].body_servico6Titulo4);
        $('#servico6_descricao_opcao4').val(dados['servico6'][0].body_servico6Descricao4);
        
        $("#servico6_imagem5").attr("src", "/assets/images/servicos/"+dados['servico6'][0].body_servico6Imagem5);
        $('#servico6_SEO_opcao5').val(dados['servico6'][0].body_servico6ImagemSEO5);
        $('#servico6_titulo_opcao5').val(dados['servico6'][0].body_servico6Titulo5);
        $('#servico6_descricao_opcao5').val(dados['servico6'][0].body_servico6Descricao5);

        $("#servico6_imagem6").attr("src", "/assets/images/servicos/"+dados['servico6'][0].body_servico6Imagem6);
        $('#servico6_SEO_opcao6').val(dados['servico6'][0].body_servico6ImagemSEO6);
        $('#servico6_titulo_opcao6').val(dados['servico6'][0].body_servico6Titulo6);
        $('#servico6_descricao_opcao6').val(dados['servico6'][0].body_servico6Descricao6);

        $("#servico6_imagem7").attr("src", "/assets/images/servicos/"+dados['servico6'][0].body_servico6Imagem7);
        $('#servico6_SEO_opcao7').val(dados['servico6'][0].body_servico6ImagemSEO7);
        $('#servico6_titulo_opcao7').val(dados['servico6'][0].body_servico6Titulo7);
        $('#servico6_descricao_opcao7').val(dados['servico6'][0].body_servico6Descricao7);

        $("#servico6_imagem8").attr("src", "/assets/images/servicos/"+dados['servico6'][0].body_servico6Imagem8);
        $('#servico6_SEO_opcao8').val(dados['servico6'][0].body_servico6ImagemSEO8);
        $('#servico6_titulo_opcao8').val(dados['servico6'][0].body_servico6Titulo8);
        $('#servico6_descricao_opcao8').val(dados['servico6'][0].body_servico6Descricao8);
        
        $('#idModelo_servico7').val(dados['servico7'][0].id_modelo);
        if(dados['servico7'][0].body_servico7 === '1'){$('#servico7_ativaDesativa').val(1).attr("checked", "checked");}else{$('#servico7_ativaDesativa').val(1);}
        if(dados['servico7'][0].body_conteudoGenerico7 === '1'){$('#servico7_ConteudoGenerico').val(1).attr("checked", "checked");}else{$('#servico7_ConteudoGenerico').val(1);}
        $('#servico7_titulo').val(dados['servico7'][0].body_textoGenericoTitulo7);
        $('#servico7_descricao').val(dados['servico7'][0].body_textoGenericoDescricao7);
        $('#servico7_corTitulo').val(dados['servicos_personalizado'][0].servico7_corTituloGenerico1);
        $('#servico7_corDescricao').val(dados['servicos_personalizado'][0].servico7_corDescricaoGenerico1);
        $('#servico7_corFundoBG').val(dados['servicos_personalizado'][0].servico7_corBg);
        
        if(dados['servico7'][0].body_servico7_opcao1 === '1'){$('#servico7_opcao1').val(1).attr("checked", "checked");}else{$('#servico7_opcao1').val(1);}
        $("#servico7_imagem1").attr("src", "/assets/images/servicos/"+dados['servico7'][0].body_servico7Imagem1);
        $('#servico7_SEO_opcao1').val(dados['servico7'][0].body_servico7ImagemSEO1);
        
        if(dados['servico7'][0].body_servico7_opcao2 === '1'){$('#servico7_opcao2').val(1).attr("checked", "checked");}else{$('#servico7_opcao2').val(1);}
        $("#servico7_imagem2").attr("src", "/assets/images/servicos/"+dados['servico7'][0].body_servico7Imagem2);
        $('#servico7_SEO_opcao2').val(dados['servico7'][0].body_servico7ImagemSEO2);

        if(dados['servico7'][0].body_servico7_opcao3 === '1'){$('#servico7_opcao3').val(1).attr("checked", "checked");}else{$('#servico7_opcao3').val(1);}
        $("#servico7_imagem3").attr("src", "/assets/images/servicos/"+dados['servico7'][0].body_servico7Imagem3);
        $('#servico7_SEO_opcao3').val(dados['servico7'][0].body_servico7ImagemSEO3);
        
        if(dados['servico7'][0].body_servico7_opcao4 === '1'){$('#servico7_opcao4').val(1).attr("checked", "checked");}else{$('#servico7_opcao4').val(1);}
        $("#servico7_imagem4").attr("src", "/assets/images/servicos/"+dados['servico7'][0].body_servico7Imagem4);
        $('#servico7_SEO_opcao4').val(dados['servico7'][0].body_servico7ImagemSEO4);

        if(dados['servico7'][0].body_servico7_opcao5 === '1'){$('#servico7_opcao5').val(1).attr("checked", "checked");}else{$('#servico7_opcao5').val(1);}
        $("#servico7_imagem5").attr("src", "/assets/images/servicos/"+dados['servico7'][0].body_servico7Imagem5);
        $('#servico7_SEO_opcao5').val(dados['servico7'][0].body_servico7ImagemSEO5);
        
        if(dados['servico7'][0].body_servico7_opcao6 === '1'){$('#servico7_opcao6').val(1).attr("checked", "checked");}else{$('#servico7_opcao6').val(1);}
        $("#servico7_imagem6").attr("src", "/assets/images/servicos/"+dados['servico7'][0].body_servico7Imagem6);
        $('#servico7_SEO_opcao6').val(dados['servico7'][0].body_servico7ImagemSEO6);
        
        if(dados['servico7'][0].body_servico7_opcao7 === '1'){$('#servico7_opcao7').val(1).attr("checked", "checked");}else{$('#servico7_opcao7').val(1);}
        $("#servico7_imagem7").attr("src", "/assets/images/servicos/"+dados['servico7'][0].body_servico7Imagem7);
        $('#servico7_SEO_opcao7').val(dados['servico7'][0].body_servico7ImagemSEO7);
        
        if(dados['servico7'][0].body_servico7_opcao8 === '1'){$('#servico7_opcao8').val(1).attr("checked", "checked");}else{$('#servico7_opcao8').val(1);}
        $("#servico7_imagem8").attr("src", "/assets/images/servicos/"+dados['servico7'][0].body_servico7Imagem8);
        $('#servico7_SEO_opcao8').val(dados['servico7'][0].body_servico7ImagemSEO8);

        
        $('#idModelo_servico8').val(dados['servico8'][0].id_modelo);
        if(dados['servico8'][0].body_maps === '1'){$('#servico8_ativaDesativa').val(1).attr("checked", "checked");}else{$('#servico8_ativaDesativa').val(1);}
        if(dados['servico8'][0].body_conteudoGenerico_maps === '1'){$('#servico8_ConteudoGenerico').val(1).attr("checked", "checked");}else{$('#servico8_ConteudoGenerico').val(1);}

        $('#servico8_titulo').val(dados['servico8'][0].body_maps_textoGenericoTitulo1);
        $('#servico8_descricao').val(dados['servico8'][0].body_maps_textoGenericoDescricao1);
        $('#servico8_corTitulo').val(dados['servicos_personalizado'][0].servico_maps_corTituloGenerico1);
        $('#servico8_corDescricao').val(dados['servicos_personalizado'][0].servico_maps_corDescricaoGenerico1);
        $('#servico8_corFundoBG').val(dados['servicos_personalizado'][0].servico_maps_corBg);
        
        $('#idModelo_PaginaSucesso').val(dados['paginaSucesso'][0].id_modelo);
        $('#idLandingPage_PaginaSucesso').val(dados['paginaSucesso'][0].id_landingPage);
        $('#tituloPaginaSucesso').val(dados['paginaSucesso'][0].body_textoGenerico_paginaSucesso);
        $('#descricaoPaginaSucesso').val(dados['paginaSucesso'][0].body_textoGenericoDescricao_paginaSucesso);
        $('#tituloBotaoPaginaSucesso').val(dados['paginaSucesso'][0].body_textoBotao_paginaSucesso);
        $('#iconePrincipalPaginaSucesso').val(dados['paginaSucesso'][0].body_icone_paginaSucesso);
        $('#iconeBotaoPaginaSucesso').val(dados['paginaSucesso'][0].body_iconeBotao_paginaSucesso);
        $('#linkBotaoPaginaSucesso').val(dados['paginaSucesso'][0].body_linkBotao_paginaSucesso);
        
        $('#corTextoTituloPaginaSucesso').val(dados['servicos_personalizado'][0].servico_paginaSucesso_corTextoTitulo);
        $('#corTextoDescricaoPaginaSucesso').val(dados['servicos_personalizado'][0].servico_paginaSucesso_corTextoDescricao);
        $('#corTextoBotaoPaginaSucesso').val(dados['servicos_personalizado'][0].servico_paginaSucesso_corTextoBotao);
        $('#corTextoBotaoHoverPaginaSucesso').val(dados['servicos_personalizado'][0].servico_paginaSucesso_corTextoHoverBotao);
        $('#corFundoBotaoPaginaSucesso').val(dados['servicos_personalizado'][0].servico_paginaSucesso_corFundoBotao);
        $('#corFundoBotaoHoverPaginaSucesso').val(dados['servicos_personalizado'][0].servico_paginaSucesso_corFundoHoverBotao);
        $('#corFundoPaginaSucesso').val(dados['servicos_personalizado'][0].servico_paginaSucesso_corBG);

      }
    }
  );
}

$('#formServico1').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarServico1(self);
});

function atualizarServico1(dados){

  var form = $('#formServico1')[0];

      var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico1",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}

$('#formHeadSEO').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarHeadSEO(self);
});

function atualizarHeadSEO(dados){

  var form = $('#formHeadSEO')[0];

  var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarHeadSEO",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarHeadSEO();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarHeadSEO();
          
        }
      }
    }
  );
}

$('#formLogoMenuSlide').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarformLogoMenuSlide(self);
});

function atualizarformLogoMenuSlide(dados){

  var form = $('#formLogoMenuSlide')[0];

  var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarformLogoMenuSlide",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarLogoMenuSlide();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarLogoMenuSlide();
          
        }
      }
    }
  );
}

$('#formFormularios').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarFormularios(self);
});

function atualizarFormularios(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarFormularios",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarForms();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarForms();
          
        }
      }
    }
  );
}

$('#formConfuracoesRedesSociais').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarRedesSociais(self);
});

function atualizarRedesSociais(dados){

  var form = $('#formConfuracoesRedesSociais')[0];

      var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarRedesSociais",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarRedesSociais();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarRedesSociais();
          
        }
      }
    }
  );
}

$('#formServico2').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarServico2(self);
});

function atualizarServico2(dados){

  var form = $('#formServico2')[0];

      var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico2",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}

$('#formServico3').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarServico3(self);
});

function atualizarServico3(dados){

  var form = $('#formServico3')[0];

      var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico3",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}

$('#formServico4').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarServico4(self);
});

function atualizarServico4(dados){

  var form = $('#formServico4')[0];

      var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico4",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}

$('#formServico5').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarServico5(self);
});

function atualizarServico5(dados){

  var form = $('#formServico5')[0];

      var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico5",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}

$('#formServico6').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarServico6(self);
});

function atualizarServico6(dados){

  var form = $('#formServico6')[0];

      var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico6",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}

$('#formServico7').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarServico7(self);
});

function atualizarServico7(dados){

  var form = $('#formServico7')[0];

      var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico7",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,

      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}

$('#formServico8').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarServico8(self);
});

function atualizarServico8(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico8",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}

$('#formPaginaSucesso').submit(function(e){
  e.preventDefault();
  var self = $(this);
  
  for ( instance in CKEDITOR.instances )
  CKEDITOR.instances[instance].updateElement();
  // alert(self.serialize());
  var retorno = atualizarServico_paginaSucesso(self);
});

function atualizarServico_paginaSucesso(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarServico_paginaSucesso",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgConfigsModelo1').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        } else {
          
          $('#erroMsgConfigsModelo1').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarServicos();
          
        }
      }
    }
  );
}


CKEDITOR.replace( 'descricaoPaginaSucesso' );

document.getElementById('caminhoFakeServico1_opcao1').addEventListener('click', function() {
	document.getElementById('servico1_imagem_opcao1').click();
});

document.getElementById('servico1_imagem_opcao1').addEventListener('change', function() {
  document.getElementById('servico1nomeFake_opcao1').value = this.value;
  document.getElementById('servico1BotaoFake_opcao1').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico1_opcao2').addEventListener('click', function() {
	document.getElementById('servico1_imagem_opcao2').click();
});

document.getElementById('servico1_imagem_opcao2').addEventListener('change', function() {
  document.getElementById('servico1nomeFake_opcao2').value = this.value;
  document.getElementById('servico1BotaoFake_opcao2').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico1_opcao3').addEventListener('click', function() {
	document.getElementById('servico1_imagem_opcao3').click();
});

document.getElementById('servico1_imagem_opcao3').addEventListener('change', function() {
  document.getElementById('servico1nomeFake_opcao3').value = this.value;
  document.getElementById('servico1BotaoFake_opcao3').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico1_opcao4').addEventListener('click', function() {
	document.getElementById('servico1_imagem_opcao4').click();
});

document.getElementById('servico1_imagem_opcao4').addEventListener('change', function() {
  document.getElementById('servico1nomeFake_opcao4').value = this.value;
  document.getElementById('servico1BotaoFake_opcao4').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico1_opcao5').addEventListener('click', function() {
	document.getElementById('servico1_imagem_opcao5').click();
});

document.getElementById('servico1_imagem_opcao5').addEventListener('change', function() {
  document.getElementById('servico1nomeFake_opcao5').value = this.value;
  document.getElementById('servico1BotaoFake_opcao5').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico1_opcao6').addEventListener('click', function() {
	document.getElementById('servico1_imagem_opcao6').click();
});

document.getElementById('servico1_imagem_opcao6').addEventListener('change', function() {
  document.getElementById('servico1nomeFake_opcao6').value = this.value;
  document.getElementById('servico1BotaoFake_opcao6').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico1_opcao7').addEventListener('click', function() {
	document.getElementById('servico1_imagem_opcao7').click();
});

document.getElementById('servico1_imagem_opcao7').addEventListener('change', function() {
  document.getElementById('servico1nomeFake_opcao7').value = this.value;
  document.getElementById('servico1BotaoFake_opcao7').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico1_opcao8').addEventListener('click', function() {
	document.getElementById('servico1_imagem_opcao8').click();
});

document.getElementById('servico1_imagem_opcao8').addEventListener('change', function() {
  document.getElementById('servico1nomeFake_opcao8').value = this.value;
  document.getElementById('servico1BotaoFake_opcao8').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico3_opcao1').addEventListener('click', function() {
	document.getElementById('servico3_imagem_opcao1').click();
});

document.getElementById('servico3_imagem_opcao1').addEventListener('change', function() {
  document.getElementById('servico3nomeFake_opcao1').value = this.value;
  document.getElementById('servico3BotaoFake_opcao1').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico3_opcao2').addEventListener('click', function() {
	document.getElementById('servico3_imagem_opcao2').click();
});

document.getElementById('servico3_imagem_opcao2').addEventListener('change', function() {
  document.getElementById('servico3nomeFake_opcao2').value = this.value;
  document.getElementById('servico3BotaoFake_opcao2').removeAttribute('disabled');
});


document.getElementById('caminhoFakeServico3_opcao3').addEventListener('click', function() {
	document.getElementById('servico3_imagem_opcao3').click();
});

document.getElementById('servico3_imagem_opcao3').addEventListener('change', function() {
  document.getElementById('servico3nomeFake_opcao3').value = this.value;
  document.getElementById('servico3BotaoFake_opcao3').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico3_opcao4').addEventListener('click', function() {
	document.getElementById('servico3_imagem_opcao4').click();
});

document.getElementById('servico3_imagem_opcao4').addEventListener('change', function() {
  document.getElementById('servico3nomeFake_opcao4').value = this.value;
  document.getElementById('servico3BotaoFake_opcao4').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico6_opcao1').addEventListener('click', function() {
	document.getElementById('servico6_imagem_opcao1').click();
});

document.getElementById('servico6_imagem_opcao1').addEventListener('change', function() {
  document.getElementById('servico6nomeFake_opcao1').value = this.value;
  document.getElementById('servico6BotaoFake_opcao1').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico6_opcao2').addEventListener('click', function() {
	document.getElementById('servico6_imagem_opcao2').click();
});

document.getElementById('servico6_imagem_opcao2').addEventListener('change', function() {
  document.getElementById('servico6nomeFake_opcao2').value = this.value;
  document.getElementById('servico6BotaoFake_opcao2').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico6_opcao3').addEventListener('click', function() {
	document.getElementById('servico6_imagem_opcao3').click();
});

document.getElementById('servico6_imagem_opcao3').addEventListener('change', function() {
  document.getElementById('servico6nomeFake_opcao3').value = this.value;
  document.getElementById('servico6BotaoFake_opcao3').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico6_opcao4').addEventListener('click', function() {
	document.getElementById('servico6_imagem_opcao4').click();
});

document.getElementById('servico6_imagem_opcao4').addEventListener('change', function() {
  document.getElementById('servico6nomeFake_opcao4').value = this.value;
  document.getElementById('servico6BotaoFake_opcao4').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico6_opcao5').addEventListener('click', function() {
	document.getElementById('servico6_imagem_opcao5').click();
});

document.getElementById('servico6_imagem_opcao5').addEventListener('change', function() {
  document.getElementById('servico6nomeFake_opcao5').value = this.value;
  document.getElementById('servico6BotaoFake_opcao5').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico6_opcao6').addEventListener('click', function() {
	document.getElementById('servico6_imagem_opcao6').click();
});

document.getElementById('servico6_imagem_opcao6').addEventListener('change', function() {
  document.getElementById('servico6nomeFake_opcao6').value = this.value;
  document.getElementById('servico6BotaoFake_opcao6').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico6_opcao7').addEventListener('click', function() {
	document.getElementById('servico6_imagem_opcao7').click();
});

document.getElementById('servico6_imagem_opcao7').addEventListener('change', function() {
  document.getElementById('servico6nomeFake_opcao7').value = this.value;
  document.getElementById('servico6BotaoFake_opcao7').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico6_opcao8').addEventListener('click', function() {
	document.getElementById('servico6_imagem_opcao8').click();
});

document.getElementById('servico6_imagem_opcao8').addEventListener('change', function() {
  document.getElementById('servico6nomeFake_opcao8').value = this.value;
  document.getElementById('servico6BotaoFake_opcao8').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico7_opcao1').addEventListener('click', function() {
	document.getElementById('servico7_imagem_opcao1').click();
});

document.getElementById('servico7_imagem_opcao1').addEventListener('change', function() {
  document.getElementById('servico7nomeFake_opcao1').value = this.value;
  document.getElementById('servico7BotaoFake_opcao1').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico7_opcao2').addEventListener('click', function() {
	document.getElementById('servico7_imagem_opcao2').click();
});

document.getElementById('servico7_imagem_opcao2').addEventListener('change', function() {
  document.getElementById('servico7nomeFake_opcao2').value = this.value;
  document.getElementById('servico7BotaoFake_opcao2').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico7_opcao3').addEventListener('click', function() {
	document.getElementById('servico7_imagem_opcao3').click();
});

document.getElementById('servico7_imagem_opcao3').addEventListener('change', function() {
  document.getElementById('servico7nomeFake_opcao3').value = this.value;
  document.getElementById('servico7BotaoFake_opcao3').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico7_opcao4').addEventListener('click', function() {
	document.getElementById('servico7_imagem_opcao4').click();
});

document.getElementById('servico7_imagem_opcao4').addEventListener('change', function() {
  document.getElementById('servico7nomeFake_opcao4').value = this.value;
  document.getElementById('servico7BotaoFake_opcao4').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico7_opcao5').addEventListener('click', function() {
	document.getElementById('servico7_imagem_opcao5').click();
});

document.getElementById('servico7_imagem_opcao5').addEventListener('change', function() {
  document.getElementById('servico7nomeFake_opcao5').value = this.value;
  document.getElementById('servico7BotaoFake_opcao5').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico7_opcao6').addEventListener('click', function() {
	document.getElementById('servico7_imagem_opcao6').click();
});

document.getElementById('servico7_imagem_opcao6').addEventListener('change', function() {
  document.getElementById('servico7nomeFake_opcao6').value = this.value;
  document.getElementById('servico7BotaoFake_opcao6').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico7_opcao7').addEventListener('click', function() {
	document.getElementById('servico7_imagem_opcao7').click();
});

document.getElementById('servico7_imagem_opcao7').addEventListener('change', function() {
  document.getElementById('servico7nomeFake_opcao7').value = this.value;
  document.getElementById('servico7BotaoFake_opcao7').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico7_opcao8').addEventListener('click', function() {
	document.getElementById('servico7_imagem_opcao8').click();
});

document.getElementById('servico7_imagem_opcao8').addEventListener('change', function() {
  document.getElementById('servico7nomeFake_opcao8').value = this.value;
  document.getElementById('servico7BotaoFake_opcao8').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico5_opcao1').addEventListener('click', function() {
	document.getElementById('servico5_imagem_opcao1').click();
});

document.getElementById('servico5_imagem_opcao1').addEventListener('change', function() {
  document.getElementById('servico5nomeFake_opcao1').value = this.value;
  document.getElementById('servico5BotaoFake_opcao1').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico5_opcao2').addEventListener('click', function() {
	document.getElementById('servico5_imagem_opcao2').click();
});

document.getElementById('servico5_imagem_opcao2').addEventListener('change', function() {
  document.getElementById('servico5nomeFake_opcao2').value = this.value;
  document.getElementById('servico5BotaoFake_opcao2').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico5_opcao3').addEventListener('click', function() {
	document.getElementById('servico5_imagem_opcao3').click();
});

document.getElementById('servico5_imagem_opcao3').addEventListener('change', function() {
  document.getElementById('servico5nomeFake_opcao3').value = this.value;
  document.getElementById('servico5BotaoFake_opcao3').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico5_opcao4').addEventListener('click', function() {
	document.getElementById('servico5_imagem_opcao4').click();
});

document.getElementById('servico5_imagem_opcao4').addEventListener('change', function() {
  document.getElementById('servico5nomeFake_opcao4').value = this.value;
  document.getElementById('servico5BotaoFake_opcao4').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico5_opcao5').addEventListener('click', function() {
	document.getElementById('servico5_imagem_opcao5').click();
});

document.getElementById('servico5_imagem_opcao5').addEventListener('change', function() {
  document.getElementById('servico5nomeFake_opcao5').value = this.value;
  document.getElementById('servico5BotaoFake_opcao5').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico5_opcao6').addEventListener('click', function() {
	document.getElementById('servico5_imagem_opcao6').click();
});

document.getElementById('servico5_imagem_opcao6').addEventListener('change', function() {
  document.getElementById('servico5nomeFake_opcao6').value = this.value;
  document.getElementById('servico5BotaoFake_opcao6').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico5_opcao7').addEventListener('click', function() {
	document.getElementById('servico5_imagem_opcao7').click();
});

document.getElementById('servico5_imagem_opcao7').addEventListener('change', function() {
  document.getElementById('servico5nomeFake_opcao7').value = this.value;
  document.getElementById('servico5BotaoFake_opcao7').removeAttribute('disabled');
});

document.getElementById('caminhoFakeServico5_opcao8').addEventListener('click', function() {
	document.getElementById('servico5_imagem_opcao8').click();
});

document.getElementById('servico5_imagem_opcao8').addEventListener('change', function() {
  document.getElementById('servico5nomeFake_opcao8').value = this.value;
  document.getElementById('servico5BotaoFake_opcao8').removeAttribute('disabled');
});