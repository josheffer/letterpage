$('#formLogin').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = autenticarLogin(self);
  
  });
  
  function autenticarLogin(dados){
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "autenticarlogin",
        dataType: 'json',
        
        beforeSend: function() { 
          
          $("#usuario").prop("disabled", true);
          $("#senha").prop("disabled", true);
          $("#botaoAcessarLogin").prop("disabled", true).html('Aguarde...');
  
        },
  
        success: function(retorno){
            
          if(retorno.ret === false){
  
            $("#usuario").prop("disabled", false);
            $("#senha").prop("disabled", false);
            $("#botaoAcessarLogin").prop("disabled", false).html('Tentar login novamente..');
  
            $('#msgErroLogin').html( 

               ' <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Erro!</strong>' + retorno.msg +
                '</div>'
            );
  
          } else {
  
            $("#usuario").prop("disabled", false);
            $("#senha").prop("disabled", false);
            $("#botaoAcessarLogin").prop("disabled", false).html('Acessando..');
            
            window.location = "dashboard/";
                  
            $('#formLogin').each (function(){
            this.reset();
            });
            
          }
        }
      }
    );
  }