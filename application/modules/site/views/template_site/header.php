<!DOCTYPE html>

<html class="no-js" lang="pt-br">
    
    <head>

        <title><?=$seo->site_nomeSite?></title>
        
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1"/>
        <link rel="canonical" href="<?=base_url()?>"/>
        <meta name="description" content="<?=$seo->site_description?>">
        <meta name="keywords" content="<?=$seo->site_keywords?>">
        <meta name="Abstract" content="<?=$seo->site_description?>" />
        <meta name="robots" content="index, follow">
        <meta name="revisit-after" content="1 day">
        <meta name="language" content="Portuguese">
        <meta name="generator" content="N/A">
        <meta name="country" content="BRA" />
        <meta name="email" content="contato@lekalekids.com">
        <meta name="currency" content="R$" />   
        <base href="<?=base_url()?>">
        <link href="" rel="publisher" />
        <meta name="google-site-verification" content="<?=$seo->site_googleVerificacao?>">
        <meta property="og:locale" content="pt_BR"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="<?=$seo->site_nomeSite?>"/>
        <meta property="og:description" content="<?=$seo->site_description?>"/>
        <meta property="og:url" content="<?=base_url()?>"/>
        <meta property="og:site_name" content="<?=$seo->site_nomeSite?>"/>
        <meta property="og:image" content="<?=base_url('/assets/site/images/SEO/').$seo->site_SEO_imagem?>"/>
        <meta property="og:image:secure_url" content="<?=base_url('/assets/site/images/SEO/').$seo->site_SEO_imagem?>"/>
        <meta property="og:image:alt" content="<?=$seo->site_nomeSite?>"/>
        <meta property="og:image:type" content="image/jpeg">
        <meta property="og:image:width" content="850">
        <meta property="og:image:height" content="400">
        <meta itemprop="name" content="<?=$seo->site_nomeSite?>" />
        <meta itemprop="url" content="<?=base_url()?>" />
        <meta name="author" content="Josheffer Robert">
        <meta name="application-name" content="<?=$seo->site_nomeSite?>">
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:description" content="<?=$seo->site_description2?>"/>
        <meta name="twitter:title" content="<?=$seo->site_nomeSite?>"/>
        <meta name="twitter:image" content="<?=base_url('/assets/site/images/SEO/').$seo->site_SEO_imagem?>"/>
        <meta content="<?=$seo->site_corTema?>" name=theme-color>
        <meta name="geo.position" content="<?=$seo->site_coordenadasMaps?>" />
        <meta name="geo.placename" content="<?=$seo->site_cidade?>" />
        <meta name="geo.region" content="<?=$seo->site_estado?>" />
        <meta name="ICBM" content="<?=$seo->site_coordenadasMaps?>" />
        <meta name="author" content="Josheffer Robert" />
        <meta name="copyright" content="Josheffer Robert" />
        <meta name="DC.title" content="<?=$seo->site_nomeSite?>" />
        <meta name="distribution" content="'Global" />
        <meta name="rating" content="'General" />
        <meta name="doc-class" content="'Completed" />
        
        <link rel="icon" href="<?=base_url('assets/images/')?>favicon.png" type="image/png">
        
        <?=$scriptsCSS?>

        <?=$seo->site_css?>
        
        <?=$scriptsJSacima?>

        <script>
            <?=$seo->site_js?>
        </script>

    <Style>
        .titulo_slide1 {color: <?=$slide->corTituloDescricao_slide1;?> !important;}
        .descricao_slide1 {color: <?=$slide->corTituloDescricao_slide1;?> !important;}
        .botao_slide1 .border-btn{color: <?=$slide->corTexto_botaoSlide1;?> !important; border: 1px solid <?=$slide->corTexto_botaoSlide1;?> !important; background-color: <?=$slide->corFundo_botaoSlide1?> !important;}
        .botao_slide1 .border-btn:hover{color: <?=$slide->corTextoHover_botaoSlide1;?> !important; border: 1px solid <?=$slide->corTextoHover_botaoSlide1;?> !important; background-color: <?=$slide->corFundoHover_botaoSlide1?> !important;}

        .titulo_slide2 {color: <?=$slide->corTituloDescricao_slide2;?> !important;}
        .descricao_slide2 {color: <?=$slide->corTituloDescricao_slide2;?> !important;}
        .botao_slide2 .border-btn{color: <?=$slide->corTexto_botaoSlide2;?> !important; border: 1px solid <?=$slide->corTexto_botaoSlide2;?> !important; background-color: <?=$slide->corFundo_botaoSlide2?> !important}
        .botao_slide2 .border-btn:hover{color: <?=$slide->corTextoHover_botaoSlide2;?> !important; border: 1px solid <?=$slide->corTextoHover_botaoSlide2;?> !important; background-color: <?=$slide->corFundoHover_botaoSlide2?> !important;}

        .titulo_slide3 {color: <?=$slide->corTituloDescricao_slide3;?> !important;}
        .descricao_slide3 {color: <?=$slide->corTituloDescricao_slide3;?> !important;}
        .botao_slide3 .border-btn{color: <?=$slide->corTexto_botaoSlide3;?> !important; border: 1px solid <?=$slide->corTexto_botaoSlide3;?> !important; background-color: <?=$slide->corFundo_botaoSlide3?> !important}
        .botao_slide3 .border-btn:hover{color: <?=$slide->corTextoHover_botaoSlide3;?> !important; border: 1px solid <?=$slide->corTextoHover_botaoSlide3;?> !important; background-color: <?=$slide->corFundoHover_botaoSlide3?> !important;}

        .titulo_slide4 {color: <?=$slide->corTituloDescricao_slide4;?> !important;}
        .descricao_slide4 {color: <?=$slide->corTituloDescricao_slide4;?> !important;}
        .botao_slide4 .border-btn{color: <?=$slide->corTexto_botaoSlide4;?> !important; border: 1px solid <?=$slide->corTexto_botaoSlide4;?> !important; background-color: <?=$slide->corFundo_botaoSlide4?> !important}
        .botao_slide4 .border-btn:hover{color: <?=$slide->corTextoHover_botaoSlide4;?> !important; border: 1px solid <?=$slide->corTextoHover_botaoSlide4;?> !important; background-color: <?=$slide->corFundoHover_botaoSlide4?> !important;}

        .summer_collection_section {
            background: url(/assets/site/colecao/<?=$colecao->imagem_colecao;?>) no-repeat !important;
        }

        .btn:focus, .btn:active, button:focus, button:active {
            outline: none !important;
            box-shadow: none !important;
        }

        #image-gallery .modal-footer{
            display: block !important;
        }

        .thumb{
            margin-top: 15px !important;
            margin-bottom: 15px !important;
        }
    </Style>
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    </head>