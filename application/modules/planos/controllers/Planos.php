<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class Planos extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->model("landingpage/LandingPage_model", "landingpage");
        $this->load->model("landingpage/TemplateLandingPage_model", "template");
        $this->load->model("landingpage/Modelos_model", "modelos");
        $this->load->model("usuarios/Usuarios_model", "usuarios");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->model("planos/Planos_model", "planos");
        $this->load->model("login/Meuplano_model", "plano");
        $this->load->library('permitiracesso');

        $this->load->model("empresa/Empresa_model", "empresa");
    }
    
	public function index()
	{
        $this->permitiracesso->verifica_sessao();
        
        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_planos";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Criar plano | Solid System",

            "scriptsJS" => "<script src='/assets/libs/ckeditor/ckeditor.js'></script>
                            <script src='/assets/painel/js/vendor/scriptsPlanos.js'></script>
                            <script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                            <script src='/assets/libs/form-masks.init.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",
            
            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }
    
    public function c_cadastrar()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/planos/cadastrar";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;

                $dados['id_planos']         = $this->planos->m_gerarCodigoCriarPlano();
                $dados['nome_planos']       = $this->input->post('nomePlano');
                $dados['valor_planos']      = $this->input->post('valorPlano');
                $dados['meses_planos']      = $this->input->post('mesesPlano');
                $dados['qtdPages_planos']   = $this->input->post('qtdPages_Plano');
                
                if(empty($this->input->post('destaque_Plano')))
                {
                    $dados['destaque_planos'] = '0';

                } else{

                    $dados['destaque_planos'] = '1';
                }

                $dados['descricao_planos']  = $this->input->post('descricaoPlano');
                
                if(empty($dados['nome_planos']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($dados['valor_planos']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>VALOR</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($dados['meses_planos']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'Escolha a quantidade de <strong>MESES</strong> para o plano!<br>';
                    $sinal = true;
                    
                }

                if(empty($dados['descricao_planos']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'A <strong>DESCRIÇÃO</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                $dados['status_planos']   = '1';
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }
                
            
                $result = $this->planos->m_criarNovoPlano($dados);
                
                if($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Plano criado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível criar o plano';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_listarPlanos()
    {
        $dadosSessao['dados'] = $this->session->userdata;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $resultado['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        $resultado['dados'] = $this->planos->m_listarPlanos();

        echo json_encode($resultado);
    }
    
    public function v_planos()
    {
    
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_planosUsuario";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $meuPlano = $this->plano->buscarMeuPlano();
        
        $dadosSessao['planos'] = $this->planos->m_listarPlanosUsuarios();
        $dadosSessao['meuPlano'] = $meuPlano['id_meuPlano'];
        
        $scripts = array(
            
            "titulo" => "Atualizar plano | Solid System",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsPlanos.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",
            
            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
        $this->load->view('painel/Template/t_menuLateral', $dadosSessao);
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }

    public function c_atualizarPlano()
    {
        $meses = $this->input->post('mesPlano');

        $dados['id_meuPlano']           = $this->input->post('idPlanos');
        $dados['planoAtual']           = $this->input->post('idPlanoAtual');

        $dados['dataInicio_meuPlano']   = date('Ymd');
        
        $dados['dataFim_meuPlano'] = date('Ymd', strtotime("+".$meses." month", strtotime(date('Ymd')))); 
        
        $dados['dataAviso1_meuPlano']       = date('Ymd', strtotime("+ 3 days",     strtotime($dados['dataFim_meuPlano']))); 
        $dados['dataAviso2_meuPlano']       = date('Ymd', strtotime("+ 6 days",     strtotime($dados['dataFim_meuPlano'])));  
        $dados['dataAviso3_meuPlano']       = date('Ymd', strtotime("+ 9 days",     strtotime($dados['dataFim_meuPlano'])));  
        $dados['dataFimServico_meuPlano']   = date('Ymd', strtotime("+ 10 days",    strtotime($dados['dataFim_meuPlano'])));  
        $result = $this->planos->m_atualizarPlano($dados);
        
        if($result)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = 'Seu plano foi atualizado com sucesso! (Atualizando dados em 5 segundos..)';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Desculpa, não foi possível atualizar seu plano';
            echo json_encode($retorno);
        }
        
    }

    public function c_atualizarStatusPlano()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/planos/atualizarStatusPlano";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id = $this->input->post('idPlanoAtualizar');

                $dados['id_planos'] = $id;
                
                $resultado = $this->planos->buscarMeuPlano($id);
                
                $statusAtual = $resultado[0]->status_planos;
                
                if($statusAtual === '1')
                {

                    $dados['status_planos'] = '0';

                } else {

                    $dados['status_planos'] = '1';

                }

                $result = $this->planos->m_atualizarStatusPlanos($dados);

                if ($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Status alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar o status!';
                    echo json_encode($retorno);
                }
    
            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_deletarPlanos()
    {

        $retorno['msg'] = "";
        
        $id = $this->input->post('idDeletarPlano');
        
        $result = $this->planos->m_deletarPlanos($id);

        if ($result)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = 'Plano deletado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Desculpa, não foi possível deletar o plano!';
            echo json_encode($retorno);
        }
    
    }
    
}