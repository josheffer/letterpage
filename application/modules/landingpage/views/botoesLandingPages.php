<div class="row no-gutters">

    <?php 

        if($permissaoLinks[8]->permissao_niveisPaginas === '1' && $permissaoLinks[9]->permissao_niveisPaginas === '1')
        {$tamanho1 = '4'; $tamanho2 = '4'; $tamanho3 = '4';} 

        if($permissaoLinks[8]->permissao_niveisPaginas === '0' && $permissaoLinks[9]->permissao_niveisPaginas === '0')
        {$tamanho1 = '12'; $tamanho2 = '4'; $tamanho3 = '4';} 

        if($permissaoLinks[8]->permissao_niveisPaginas === '1' && $permissaoLinks[9]->permissao_niveisPaginas === '0')
        {$tamanho1 = '6'; $tamanho2 = '6'; $tamanho3 = '6';} 

        if($permissaoLinks[8]->permissao_niveisPaginas === '0' && $permissaoLinks[9]->permissao_niveisPaginas === '1')
        {$tamanho1 = '6'; $tamanho2 = '6'; $tamanho3 = '6';} 
        
        
    ?>
    
    <div class="col-md-6 col-xl-<?=$tamanho1?>">
        <a href="/dashboard/landingPages">
            <div class="bg-soft-primary rounded-0 card-box mb-0">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark"><span>Landing Pages</span></h3>
                            <p class="text-warning mb-1 text-truncate">Todas as landing pages</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <?php if($permissaoLinks[8]->permissao_niveisPaginas === '1'):?>
        <div class="col-md-6 col-xl-<?=$tamanho2?>">
            <a href="/dashboard/configuracoes">
                <div class="bg-soft-danger rounded-0 card-box mb-0">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <h3 class="text-dark"><span>Configurações gerais</span></h3>
                                <p class="text-warning mb-1 text-truncate">Aplica-se a todas as landingpages</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <?php endif;?>
    
    <?php if($permissaoLinks[9]->permissao_niveisPaginas === '1'):?>
        <div class="col-md-6 col-xl-<?=$tamanho3?>">
            <a href="/dashboard/templates">
                <div class="bg-soft-info rounded-0 card-box mb-0">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <h3 class="text-dark"><span>Template</span></h3>
                                <p class="text-warning mb-1 text-truncate">Configurações de templates</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <?php endif;?>
    
</div>

<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>