<style>
.fundo_card-body
{
    background-color: #373e48;
}
.custom-file-input:disabled~.custom-file-label {
    background-color: #4b5460;
}
</style>

            <div class="content-page">
                <div class="content">
                    
                    <div class="container-fluid">
                        
                        <div class="row mt-3">
                            
                            <div class="col-12">

                                <?php
                                    $this->load->view('botoesLandingPages');
                                ?>
                                
                                <div class="row mt-3 mb-2">
                                    <div class="col-lg-12">
                                    <div id="erroMsgAtualizarStatus"></div>
                                        <!-- Portlet card -->
                                        <div class="card">
                                            <div class="card-body">
                                                
                                                <h5 class="card-title mb-3">Criação de Templates</h5>
                                                
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <div id="erroMsg"></div>
                                                            
                                                                <div id="accordion" class="mb-3">
                                                                    
                                                                    <?php if($permissaoLinks[25]->permissao_niveisPaginas === '1'):?>
                                                                        <div class="card mb-1">
                                                                            <div class="card-header" id="headingOne">
                                                                                <h5 class="m-0">
                                                                                    <a class="text-dark" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                                                        <i class="fe-layers mr-1 text-primary"></i> 
                                                                                        Criar template
                                                                                    </a>
                                                                                </h5>
                                                                            </div>
                                                                
                                                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                                                                
                                                                                <div class="card-body">
                                                                                    <form id="formTemplates" enctype="multipart/form-data">
                                                                                        <div class="form-row">
                                                                                        
                                                                                            <div class="form-group col-md-12">
                                                                                                <label class="col-form-label">Nome <small>(20 caracteres)</small></label>
                                                                                                <input type="text" class="form-control" id="nomeTemplate" name="nomeTemplate" maxlength="20" autocomplete="off">
                                                                                            </div>
                                                                                        </div>
                                                                                        <h4 class="header-title">Imagem</h4>
                                                                                        
                                                                                        <div class="form-row">
                                                                                            <div class="col-md-6">
                                                                                                <label class="col-form-label">Imagem
                                                                                                    <small class="text-warning"> (Minímo 500px)</small>
                                                                                                    <i class="mdi mdi-help-circle" data-toggle="tooltip" data-placement="bottom" title="" 
                                                                                                        data-original-title="Utilizar uma imagem com minímo 500px">
                                                                                                    </i>
                                                                                                </label>
                                                                                                <div class="custom-file">
                                                                                                    <input class="custom-file-input" type='file' name="imagemTemplate" id="imagemTemplate" onchange="readURL(this);" >
                                                                                                    <label class="custom-file-label">Procurar imagem para prevew..</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <div class="form-row">
                                                                                                    <div class="form-group col-md-12">
                                                                                                        <label class="col-form-label">Preview</label>
                                                                                                    
                                                                                                        <div class="card">
                                                                                                            <div data-spy="scroll" data-target="#navbar-example2" data-offset="0" class="scrollspy-example">
                                                                                                                <img class="card-img-top img-fluid" id="previewTemplate">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                
                                                                                                </div>
                                                                                            </div>
                                                            
                                                                                            <button type="submit" id="botaoCadastrarTemplate" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Criar Template</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div> 
                                                                            </div>
                                                                        </div>

                                                                    <?php endif;?>

                                                                </div>
                                                                
                                                                <div class="card mb-1">
                                                                    <div class="card-header" id="headingTwo">
                                                                        <h5 class="m-0">
                                                                            <a class="text-dark" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                                                                <i class="fe-layout mr-1 text-primary"></i> 
                                                                                Templates criados
                                                                            </a>
                                                                        </h5>
                                                                    </div>
                                                                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                                                                        <div class="card-body">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered mb-0">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>#</th>
                                                                                        <th>Nome</th>
                                                                                        <?php if($permissaoLinks[26]->permissao_niveisPaginas === '1'): ?><th>Status</th><?php endif;?>
                                                                                        <th>Ações</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody id="tabelaListarTemplates"></tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div> 
            </div>

            <?php if($permissaoLinks[26]->permissao_niveisPaginas === '1'): ?>
                <div id="modalAtualizarStatus" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="mySmallModalLabel">Atualizar status</h4>
                                <button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            
                            <form id="formAtualizaStatusTemplates">
                                <div class="modal-body">
                                    
                                    <div id="msgErroAtualizarStatus"></div>
                                    Tem certeza que deseja atualizar o status do template <span class="text-warning" id="nomeTemplateAtualizarStatus"></span>?
                                    <input type="hidden" name="idTemplate" id="idTemplate">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary waves-effect">(Sim) Atualizar</button>
                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <?php endif;?>

            <div class="modal fade" id="modalVerTemplate" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-full modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalScrollableTitle">Preview do template: <span class="text-warning" id="nomeVerTemplate"></span></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img id="previewTemplateView">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>

            <?php if($permissaoLinks[27]->permissao_niveisPaginas === '1'): ?>
                <div class="modal fade" id="modalEditarTemplate" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                    <div class="modal-dialog modal-full modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalScrollableTitle">Preview do template: <span class="text-warning" id="nomeAlterarTemplate"></span></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div id="msgErroAtualizarTemplate"></div>
                                
                                <form id="formAtualizarTemplates" enctype="multipart/form-data">
                                    <div class="form-row">
                                        <input type="hidden" name="idAtualizarTemplate" id="idAtualizarTemplate">
                                    
                                        <div class="form-group col-md-12">
                                            <label class="col-form-label">Nome <small>(20 caracteres)</small></label>
                                            <input type="text" class="form-control" id="nomeAtualizarTemplate" name="nomeAtualizarTemplate" maxlength="20" autocomplete="off">
                                        </div>
                                    </div>
                                    <h4 class="header-title">Imagem</h4>
                                    
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <label class="col-form-label">Imagem
                                                <small class="text-warning"> (Minímo 500px)</small>
                                                <i class="mdi mdi-help-circle" data-toggle="tooltip" data-placement="bottom" title="" 
                                                    data-original-title="Utilizar uma imagem com minímo 500px">
                                                </i>
                                            </label>
                                            <div class="custom-file">
                                                <input class="custom-file-input" type='file' name="imagemAtualizarTemplate" id="imagemAtualizarTemplate" onchange="readURL(this);" >
                                                <label class="custom-file-label">Procurar imagem para prevew..</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label class="col-form-label">Preview</label>
                                                
                                                    <div class="card">
                                                        <div data-spy="scroll" data-target="#navbar-example2" data-offset="0" class="scrollspy-example">
                                                            <img class="card-img-top img-fluid" id="previewAtualizarTemplate">
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <button type="submit" id="botaoCadastrarTemplate" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Atualizar dados</button>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>

            <?php if($permissaoLinks[28]->permissao_niveisPaginas === '1'): ?>
                <div id="modalExcluirTemplate" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="mySmallModalLabel">Excluir Template (<span id="idTemplateExcluirNum"></span>)</h4>
                                <button type="button" id="fecharModalExcluirTemplate" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            
                            <form id="formExcluirTemplate">
                                <div class="modal-body">
                                    
                                    <div id="msgErroExcluirTemplate"></div>
                                    Tem certeza que deseja excluir esse Template?
                                    <input type="hidden" name="idTemplateExcluir" id="idTemplateExcluir">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger waves-effect">(Sim) Excluir</button>
                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <?php endif;?>
            