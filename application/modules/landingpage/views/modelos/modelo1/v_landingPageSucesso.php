<!DOCTYPE html>
<html lang="pt-br">
    
    <head>
        <meta charset="utf-8" />
        <title><?=$titulo?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <link rel="icon" href="<?=base_url('assets/images/logofavicon/').$configs->favicon_landingPage?>" type="image/png">
        
        <link rel="stylesheet" href="<?=base_url('assets/modelos/modelo1/css/')?>bootstrap.min.css" />
        <link rel="stylesheet" href="<?=base_url('assets/modelos/modelo1/css/')?>icons.min.css" />
        <link rel="stylesheet" href="<?=base_url('assets/modelos/modelo1/css/')?>app.min.css" />

        <style>

            body {background-color: <?=$dadosLandingPage->servico_paginaSucesso_corBG?> !important;}
            .corIconePrincipal{color: <?=$dadosLandingPage->servico_paginaSucesso_corTextoTitulo?> !important;}
            .corTextoTitulo{color: <?=$dadosLandingPage->servico_paginaSucesso_corTextoTitulo?> !important;}
            .corTextoDescricao{color: <?=$dadosLandingPage->servico_paginaSucesso_corTextoDescricao?> !important;}
            
            .botaoVoltar {
                background-color: <?=$dadosLandingPage->servico_paginaSucesso_corFundoBotao?> !important;
                color: <?=$dadosLandingPage->servico_paginaSucesso_corTextoBotao?> !important;
            }

            .botaoVoltar:hover {
                background-color: <?=$dadosLandingPage->servico_paginaSucesso_corFundoHoverBotao?> !important;
                color: <?=$dadosLandingPage->servico_paginaSucesso_corTextoHoverBotao?> !important;
            }
        </style>

    </head>

    <body class="authentication-bg">

        <div class="mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12">

                        <div class="text-center">
                            
                            <div class="col-lg-12 mt-5">
                                <h1><i class="<?=$dadosLandingPage->body_icone_paginaSucesso?> corIconePrincipal" style="font-size: 3em;"></i> </h1>
                                <h2 class="corTextoTitulo"><?=$dadosLandingPage->body_textoGenerico_paginaSucesso?></h2>
                            </div>

                            
                            
                            <span class="corTextoDescricao">
                                <?=$dadosLandingPage->body_textoGenericoDescricao_paginaSucesso?>
                            </span>
                        </div>

                        <div class="text-center mt-3">
                            <a href="<?=$dadosLandingPage->body_linkBotao_paginaSucesso?>">
                                <button type="button" class="btn btn-success btn-rounded waves-effect waves-light botaoVoltar">
                                    <span class="btn-label"><i class="<?=$dadosLandingPage->body_iconeBotao_paginaSucesso?>"></i></span>
                                    <?=$dadosLandingPage->body_textoBotao_paginaSucesso?>
                                </button>
                            </a>
                        </div>

                    </div> 
                </div>
            </div>
        </div>

        <footer class="footer footer-alt">
        &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> <span class="corTextoTitulo"> Todos os direitos reservados | visite o nosso site - <a class="corTextoTitulo" href="<?=$configs->linkSite_landingPage?>" target="_blank">www.<?=$url_atual?></a></span>
        </footer>
        
        <script src="<?=base_url('assets/modelos/modelo1/js/')?>vendor.min.js"></script>
        <script src="<?=base_url('assets/modelos/modelo1/js/')?>app.min.js"></script>
        
    </body>
</html>