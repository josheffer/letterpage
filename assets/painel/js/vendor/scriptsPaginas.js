primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listaPaginas();

function listaPaginas() {

    $.ajax({

        url: "/dashboard/permissoes/listarPaginas",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#tabelaListarPaginas').html('');

            if (dados.length > 0) {

                dadosGlobais = dados;

                for (var i = 0; i < dados.length; i++) {

                    $('#tabelaListarPaginas').append(

                        '<tr class="d-flex">' +

                        '<th class="col-1">' + dados[i].id_pagina + '</th>' +
                        '<td class="col-2">' + dados[i].nome_pagina + '</td>' +
                        '<td class="col-3">' + dados[i].endereco_pagina + '</td>' +
                        '<td class="col-3">' + dados[i].controller_pagina + '</td>' +
                        '<td">' +
                        '<i class="fa fa-info-circle text-muted float-right" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="More Info"></i>' +
                        // + dados[i].observacao_pagina +
                        '</td">' +

                        '<td class="col-1">' +
                        '<i class="' + dados[i].icone_pagina + ' font-22"></i></td>' +
                        '<td class="col-1">' +
                        '<button type="button" onclick="javascript:AbrirModalEditarPagina(' + i + ');" class="btn btn-primary btn-xs waves-effect waves-light mr-1"><i class="mdi mdi-lead-pencil"></i></button>' +
                        '<button type="button" onclick="javascript:AbrirModalDeletarPagina(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light"><i class="mdi mdi-trash-can-outline"></i></button>' +
                        '</td>' +

                        '</tr>'

                    );
                }

            } else {

                $('#tabelaListarPaginas').append(
                    '<td colspan="6">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma página criada!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }
        }
    });
}

$('#formCadastrarNovaPagina').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = criarNovaPagina(self);

});

function criarNovaPagina(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/permissoes/criarPagina",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                $('#formCadastrarNovaPagina').each(function() {
                    this.reset();
                });

                listaPaginas();

            }
        }
    });
}

function AbrirModalEditarPagina(attPagina) {

    showModalEditarPagina(attPagina);

}

function showModalEditarPagina(attPagina) {

    $('#modalEditarPagina').modal('show');

    $("#atualizarIdPagina").val(dadosGlobais[attPagina].id_pagina);
    $("#idPaginaAtualizar").html(dadosGlobais[attPagina].id_pagina);
    $("#atualizarNomePaginaTitulo").html(dadosGlobais[attPagina].nome_pagina);
    $("#atualizarNomePagina").val(dadosGlobais[attPagina].nome_pagina);
    $("#atualizarEnderecoPagina").val(dadosGlobais[attPagina].endereco_pagina);
    $("#atualizarControllerPagina").val(dadosGlobais[attPagina].controller_pagina);
    $("#atualizarObservacaoPagina").val(dadosGlobais[attPagina].observacao_pagina);
    $("#atualizarIconePagina").val(dadosGlobais[attPagina].icone_pagina);

    $('#msgErroAtualizarPagina').trigger('click');

}

$('#formAtualziarPagina').submit(function(e) {

    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarDadosPaginas(self);

});

function atualizarDadosPaginas(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/permissoes/atualizarDadosPaginas",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroAtualizarPagina').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaPaginas();

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#modalEditarPagina').modal('hide');

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaPaginas();
            }
        }
    });
}

function AbrirModalDeletarPagina(deletarPagina) {

    showModalDeletarPagina(deletarPagina);

}

function showModalDeletarPagina(deletarPagina) {

    $('#modalDeletarPagina').modal('show');

    $("#idDeletarPagina").val(dadosGlobais[deletarPagina].id_pagina);

    $('#fecharModalDeletarPagina').trigger('click');

}

$('#formDeletarPagina').submit(function(e) {

    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = deletarDadosPagina(self);

});

function deletarDadosPagina(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/permissoes/deletarDadosPagina",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroDeletarPagina').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaPaginas();

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#modalDeletarPagina').modal('hide');

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listaPaginas();
            }
        }
    });
}