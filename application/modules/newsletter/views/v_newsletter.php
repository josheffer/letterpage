<div class="content-page">

    <div class="content">
        
        <div class="container-fluid">
            
            <div class="row mt-3">
                
                <div class="col-12">
                    
                    <?php
                        $this->load->view('botoesAll');
                    ?>

                    <div class="mt-2" id="mensagemErroNovaCampanha"></div>

                    <div class="row mt-2">
                        
                        <div class="col-lg-12">
                        
                            <div id="accordion">
                                    
                                <div class="card mb-1">
                                    
                                    <div class="card-header" id="headingOne">
                                        
                                        <h5 class="m-0">
                                            
                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                                Configuração de envio
                                            </a>

                                        </h5>

                                    </div>
                        
                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                        
                                        <div class="card-body">

                                        <form id="formCadastrarNovaCampanha">
                                                        
                                            <div class="form-row align-items-center">
                                                
                                                <div class="col-md-6 col-sm-6 col-xl-6 col-lg-6">
                                                    <h6 class="font-13">Nome Newsletter</h6>
                                                    <input type="text" class="form-control mb-2" id="nomeEmailEnviar" name="nomeEmailEnviar" autocomplete="off" autofocus>
                                                </div>
                                                
                                                <div class="col-md-6 col-sm-6 col-xl-6 col-lg-6">
                                                    <h6 class="font-13">Titulo do e-mail</h6>
                                                    <input type="text" class="form-control mb-2" id="tituloEmailEnviar" name="tituloEmailEnviar" autocomplete="off" autofocus>
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xl-6 col-lg-6">
                                                    <h6 class="font-13">Assunto do e-mail</h6>
                                                    <input type="text" class="form-control mb-2" id="assuntoEmailEnviar" name="assuntoEmailEnviar" autocomplete="off">
                                                </div>

                                                <div class="col-md-6 col-sm-6 col-xl-6 col-lg-6">
                                                    <h6 class="font-13">Listas</h6>
                                                    <select class="selectpicker mb-2" data-style="btn-light" id="codigoGrupoEnviar" name="codigoGrupoEnviar"></select>
                                                </div>

                                                <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                                    <h6 class="font-13">Mensagem</h6>
                                                    <textarea name="MensagemEnvio" id="MensagemEnvio" rows="10" cols="80"></textarea>
                                                </div>

                                                <div class="col-xl-4"></div>
                                                
                                                <div class="col-xl-4 mt-3">
                                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                                        <div id="botaoCadastrarGrupo">
                                                            <button type="submit" id="botaoCriarNovaCampanha" class="btn btn-success waves-effect waves-light btn-block">Cadastrar</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-4"></div> 

                                            </div>

                                        </form>
                                            
                                        </div>

                                    </div>

                                </div>
                                
                            </div>

                        </div>

                        <div class="col-xl-12">

                            <div id="mensagemSUCESSOPrepararLista" class="col-md-12 mt-2"></div>

                            <div class="card-box">
                                <h4 class="header-title mb-3">Campanhas pendentes</h4>

                                <div class="table-responsive">
                                    
                                    <table class="table table-bordered table-hover table-centered  table-nowrap m-0">

                                        <thead class="thead-light">
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Titulo</th>
                                                <th>Assunto</th>
                                                <th>Grupo</th>
                                                <th>Envio</th>
                                                <th>Ações</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody id="tabelaListarCampanhas"></tbody>

                                    </table>

                                </div> 

                            </div> 

                        </div>

                        <div class="col-12">
                                            
                            <div id="newsEnviadas" class="mb-3">
                                
                                <div class="card mb-1">
                                    
                                    <div class="card-header" id="newsEnviadasok">
                                        
                                        <h5 class="m-0">
                                            
                                            <a class="text-dark collapsed" data-toggle="collapse" href="#enviadas" aria-expanded="false">
                                                Campanhas enviadas
                                            </a>

                                        </h5>

                                    </div>
                        
                                    <div id="enviadas" class="collapse show" aria-labelledby="newsEnviadasok" data-parent="#newsEnviadas" style="">
                                        
                                        <div class="card-body">

                                            <div class="table-responsive">
                                        
                                                <table class="table table-bordered table-hover table-centered  table-nowrap m-0">

                                                    <thead class="thead-light">
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nome</th>
                                                            <th>Titulo</th>
                                                            <th>Assunto</th>
                                                            <th>Grupo</th>
                                                            <th>Envio</th>
                                                            <th>Template</th>
                                                        </tr>
                                                    </thead>
                                                    
                                                    <tbody id="tabelaCampanhasEnviadas"></tbody>

                                                </table>

                                            </div> 
                                            
                                        </div>

                                    </div>

                                </div>
                                
                            </div>

                        </div>
                        
                    </div>

                </div>

            </div> 

        </div>

    </div> 

</div>

<div id="prepararCampanha" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Disparar campanha: <u><i><span class="text-warning" id="tituloListaPreparar"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>

            <div id="mensagemERROPrepararLista" class="col-md-12 mt-2"></div>
            
            <form id="formPrepararListaEnviar">

                <div class="modal-body">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                
                                <input type="hidden" class="form-control" id="prepararCodigoCampanha" name="prepararCodigoCampanha">
                                <input type="hidden" class="form-control" id="nomeNewsletterPreparar" name="nomeNewsletterPreparar">
                                <input type="hidden" class="form-control" id="assuntoNewsletterPreparar" name="assuntoNewsletterPreparar">
                                <input type="hidden" class="form-control" id="mensagemNewsletterPreparar" name="mensagemNewsletterPreparar">
                                <input type="hidden" class="form-control" id="grupoNewsletterPreparar" name="grupoNewsletterPreparar">
                                <input type="hidden" class="form-control" id="botaoPrepararCampanha" name="botaoPrepararCampanha">

                                <p>Deseja realmente enviar a campanha: <b><u><i><span class="text-warning" id="nomeListaPreparar"></span></i></u></b>?</p>
                                <p>Todos os e-mails no grupo: <u><i><strong><span class="text-warning" id="nomeGrupoPreparar"></span></strong></i></u> receberá o conteúdo da campanha!</p>

                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <button type="submit" id="botaoPrepararLista" class="btn btn-dark waves-effect waves-light">Enviar</button>
                </div>
            </form>

        </div>
    </div>
</div>

<div id="verTemplateCampanha" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ver template da campanha: <u><i><span id="nomeCampanhaVisualizarTemplate"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div id="htmlMensagem"></div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="alterarInformacoesCampanha" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações da campanha: <u><i><span id="alterarNomeCamp"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroEditarCampanha" class="col-md-12"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formAlterarCampanha">

                            <input type="hidden" name="idCampanhaAlterar" id="idCampanhaAlterar">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4">
                                    <h6 class="font-13">Nome da Campanha</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomeCampanha" name="alterarNomeCampanha" autocomplete="off" autofocus>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4">
                                    <h6 class="font-13">Titulo do e-mail</h6>
                                    <input type="text" class="form-control mb-2" id="alterarTituloCampanha" name="alterarTituloCampanha" autocomplete="off" autofocus>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xl-4 col-lg-4">
                                    <h6 class="font-13">Assunto</h6>
                                    <input type="text" class="form-control mb-2" id="alterarAssuntoCampanha" name="alterarAssuntoCampanha" autocomplete="off">
                                </div>

                                <div class="col-md-6 col-sm-6 col-xl-6 col-lg-6">
                                    <h6 class="font-13">Lista atual</h6>
                                    <input type="text" class="form-control mb-2" id="nomeListaAtual" name="nomeListaAtual" readonly>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xl-6 col-lg-6">
                                    <h6 class="font-13">Alterar lista (Opcional)</h6>
                                    <select class="selectpicker mb-2" data-style="btn-light" id="codigoGrupoEditar" name="codigoGrupoEditar"></select>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                    <h6 class="font-13">Mensagem</h6>
                                    <textarea name="MensagemEditar" id="MensagemEditar" rows="10" cols="80"></textarea>
                                </div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-4 mt-3">
                                    <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                        <button type="submit" id="botaoCriarNovaCampanha" class="btn btn-success btn-block waves-effect waves-light">Editar</button>
                                    </div>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="deletarNewsletter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-danger text-center">ATENÇÃO - TENHA CUIDADO AO EXCLUIR, A AÇÃO NÃO PODERÁ SER DESFEITA!!!</h4>
            
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                
            </div>
            
            <div class="modal-body">

            <div id="msgExcluirNewsletter" class="col-md-12"></div>
                
                <form id="formExcluirCampanha">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                
                                <input type="hidden" name="idCampanhaExcluir" id="idCampanhaExcluir">

                                <h4>Tem certeza que deseja excluir a campanha <i><strong class="text-uppercase text-warning"><span id="nomeCampanhaExcluir"></span></strong></i> ?</h4>

                                <p class="text-warning mt-4">Ao solicitar a exclusão da campanha, a ação não poderá ser revertida!!! Tenha cuidado.</p>

                            </div>
                        </div>
                    </div>
                
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Sim, quero excluir!</button>
                <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
            </div>

            </form>

        </div>
    </div>
</div>

<div id="verTemplateCampanhaEnviadas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ver template da campanha: <u><i><span id="nomeCampanhaEnviadaVisualizarTemplate"></span></i></u></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div id="htmlMensagemEnviada"></div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>