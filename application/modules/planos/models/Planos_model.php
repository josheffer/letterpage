<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class Planos_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        
    }
    
    public function m_gerarCodigoCriarPlano() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_planos) as codigo FROM lp_planos");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    public function m_criarNovoPlano($dados)
    {
        return $this->db->insert('lp_planos', $dados);
    }

    public function m_listarPlanos()
    {
        $this->db->select("*");
        $this->db->order_by('id_planos', 'DESC');
        $resultado = $this->db->get("lp_planos")->result();

        return $resultado;
    }

    public function buscarMeuPlano($id)
    {
        $resultado = $this->db->query("SELECT * FROM lp_planos WHERE id_planos =". $id)->result();
        
        return $resultado;
    }

    public function m_atualizarStatusPlanos($dados)
    {

        $resultado = $this->db->query("UPDATE lp_planos SET status_planos = '".$dados['status_planos']."' WHERE id_planos = '".$dados['id_planos']."';");
        
        return $resultado;
    
    }

    public function m_deletarPlanos($id)
    {
        $resultado = $this->db->query("DELETE FROM lp_planos WHERE id_planos = '".$id."';");
        
        return $resultado;
    }

    public function m_listarPlanosUsuarios()
    {
        $this->db->select("*");
        $this->db->order_by('id_planos');
        $resultado = $this->db->get("lp_planos")->result();

        return $resultado;
    }

    public function m_atualizarPlano($dados)
    {
        
        $resultado = $this->db->query("UPDATE meuplano  
                                        SET 
                                        id_meuPlano             = '".$dados['id_meuPlano']."',
                                        dataInicio_meuPlano     = '".$dados['dataInicio_meuPlano']."', 
                                        dataFim_meuPlano        = '".$dados['dataFim_meuPlano']."', 
                                        dataAviso1_meuPlano     = '".$dados['dataAviso1_meuPlano']."', 
                                        dataAviso2_meuPlano     = '".$dados['dataAviso2_meuPlano']."', 
                                        dataAviso3_meuPlano     = '".$dados['dataAviso3_meuPlano']."', 
                                        dataFimServico_meuPlano = '".$dados['dataFimServico_meuPlano']."'
                                           
                                        WHERE (id_meuPlano      = '".$dados['planoAtual']."');");
        
        return $resultado;
    }

    public function buscarPlano()
    {
        $resultado = $this->db->query("SELECT * FROM meuplano")->result();
        
        return $resultado;
    }
    
}