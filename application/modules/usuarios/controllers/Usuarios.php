<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->model("landingpage/LandingPage_model", "landingpage");
        $this->load->model("landingpage/TemplateLandingPage_model", "template");
        $this->load->model("landingpage/Modelos_model", "modelos");
        $this->load->model("usuarios/Usuarios_model", "usuarios");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->library('permitiracesso');

        $this->load->model("empresa/Empresa_model", "empresa");
    }
    
	public function index()
	{
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $this->load->library('permitiracesso');
        
        $view = "v_usuarios";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);

        $metodo = "/dashboard/usuarios/cadastrarUsuario";
        $dadosSessao['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);

        $metodoListar = "/dashboard/usuarios/listarUsuarios";
        $dadosSessao['permitirMetodoListar'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodoListar);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Usuários | Solid System",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsUsuarios.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral', $dadosSessao);
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }
    
    public function c_cadastrarUsuario()
    {   
        $retorno['msg'] = "";
        $sinal = false;
        
        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/usuarios/cadastrarUsuario";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '1')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO PARA CADASTRAR NOVOS USUÁRIOS!</strong></span><br>';
            $sinal = true;
            
        } else {
            
            $retorno['msg'] = "";
            $sinal = false;

            $dados['id_usuario']            = $this->usuarios->m_gerarCodigoCriarUsuarios();
            $dados['login_usuario']         = $this->input->post('loginUsuario');
            $dados['senha_usuario']         = password_hash("123456", PASSWORD_DEFAULT);
            $dados['nome_usuario']          = $this->input->post('nomeUsuario');
            $dados['email_usuario']         = $this->input->post('emailUsuario');
            $dados['status_usuario']        = '1';
            $dados['nivelAcesso_usuario']   = $this->input->post('nivelAcessoUsuario');
            $dados['imagem_usuario']        = 'default.png';
            $dados['token_usuario']         = NULL;
            
            if(empty($dados['login_usuario']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>LOGIN</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }

            $verificaUsuario = $this->input->post('loginUsuario');
            $verificaEmail = $this->input->post('emailUsuario');

            $usuario = $this->usuarios->m_verificaUsuarioExistente($verificaUsuario);
            $email = $this->usuarios->m_verificaEmailExistente($verificaEmail);

            if($usuario)
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>USUÁRIO</strong> já existe, use outro!<br>';
                $sinal = true;
                
            }

            if($email)
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>E-MAIL</strong> já está sendo utilziado por outro usuário, use outro!<br>';
                $sinal = true;
                
            }

            if(empty($dados['nome_usuario']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            if(empty($dados['email_usuario']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>E-MAIL</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }

            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }

            $resultado = $this->usuarios->m_CriarUsuario($dados);

            if($resultado)
            {
                $retorno['ret'] = true;
                $retorno['msg'] = ' Usuário <strong>'. $dados['nome_usuario'].'</strong> criado com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = ' Não foi possível criar o usuário '. $dados['nome_usuario'].', tente novamente mais tarde!';
                echo json_encode($retorno);
            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_listarUsuarios()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/usuarios/listarUsuarios";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			echo json_encode($permitirAcessoPagina);
            
        } else {

            $resultado['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
            $resultado['dados'] = $this->usuarios->m_listarUsuarios();

            echo json_encode($resultado);

        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }

    public function c_atualizarStatusUsuario()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/usuarios/atualizarStatusUsuario";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id = $this->input->post('idUsuario');

                $dados['id_usuario'] = $id;
                
                $resultado = $this->usuarios->m_verificaUsuarioID($id);
                
                $statusAtual = $resultado->status_usuario;

                if($statusAtual === '1')
                {

                    $dados['status_usuario'] = '0';

                } else {

                    $dados['status_usuario'] = '1';

                }

                $result = $this->usuarios->m_atualizarStatusUsuario($dados);

                if ($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Status alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar o status!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    
    }

    public function c_atualizarDadosUsuario()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/usuarios/atualizarDadosUsuario";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;
                
                $dados['id_usuario']            = $this->input->post('atualizarIdUsuario');

                $result = $this->usuarios->m_dadosUsuarioLogado($dados['id_usuario']);
                
                $dados['login_usuario']         = $this->input->post('atualizarLoginUsuario');
                $dados['senha_usuario']         = $result->senha_usuario;
                $dados['nome_usuario']          = $this->input->post('atualizarNomeUsuario');
                $dados['email_usuario']         = $this->input->post('atualizarEmailUsuario');
                $dados['status_usuario']        = $result->status_usuario;
                $dados['nivelAcesso_usuario']   = $this->input->post('atualizarNivelAcessoUsuario');
                $dados['imagem_usuario']        = $result->imagem_usuario;
                $dados['token_usuario']         = $result->token_usuario;
                
                if(empty($dados['nivelAcesso_usuario']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'Escolha um  <strong>NÍVEL DE ACESSO</strong>!<br>';
                    $sinal = true;
                    
                }

                if(empty($dados['login_usuario']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>LOGIN</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($dados['nome_usuario']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }
                
                if(empty($dados['email_usuario']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>E-MAIL</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                $resultado = $this->usuarios->m_atualizarDadosUsuario($dados);

                if($resultado)
                {
                    $retorno['ret'] = true;
                    $retorno['msg'] = ' Dados do usuário <strong>'. $dados['nome_usuario'].'</strong> atualizados com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = ' Não foi possível atualizar os dados do usuário '. $dados['nome_usuario'].', tente novamente mais tarde!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_deletarDadosUsuario()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/usuarios/deletarDadosUsuario";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $id = $this->input->post('idDeletarusuario');

                $resultado = $this->usuarios->m_deletarDadosUsuario($id);

                if($resultado)
                {
                    $retorno['ret'] = true;
                    $retorno['msg'] = ' Usuário deletado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = ' Não foi possível deletar o usuário, tente novamente mais tarde!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    
    }

    public function v_minhaconta()
    {
        $this->permitiracesso->verifica_sessao();
        
        $dadosSessao['dados'] = $this->session->userdata;
        
        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_minhaconta";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);

        
        $scripts = array(

            "titulo" => "Minha conta | Solid System",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsUsuariosMinhaConta.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function c_dadosMinhaConta()
    {

        $dadosSessao['dados'] = $this->session->userdata;
        
        $id = $dadosSessao['dados']['id'];

        $resultado = $this->usuarios->m_dadosUsuarioLogado($id);
        
        echo json_encode($resultado);
    
    }

    public function c_atualizarMinhaContaUsuario()
    {
    
        $retorno['msg'] = "";
        $sinal = false;
        
        $senha = $this->input->post('SenhaUsuarioLogado');
        
        $dados['id_usuario'] = $this->input->post('idUsuariologado');
        $dados['login_usuario'] = $this->input->post('loginUsuarioLogado');

        $resultado = $this->usuarios->m_dadosUsuarioLogado($dados['id_usuario']);
        
        $dados['nivelAcesso_usuario'] = $resultado->nivelAcesso_usuario;
        
        if(empty($senha))
		{
            $dados['senha_usuario'] = $resultado->senha_usuario;
            
        } else {

            $dados['senha_usuario'] = password_hash($senha, PASSWORD_DEFAULT);
        }

        $dados['nome_usuario'] = $this->input->post('nomeUsuarioLogado');
        $dados['email_usuario'] = $this->input->post('emailUsuarioLogado');
        $dados['imagem_usuario'] = $this->input->post('imagemPerfilUsuario');
        
        if(empty($dados['login_usuario']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>LOGIN</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['nome_usuario']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['email_usuario']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>E-MAIL</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if($_FILES['imagemPerfilUsuario']['size'] === 0 )
        {
            $dados['imagem_usuario'] = $resultado->imagem_usuario;
        }

        if(!empty($_FILES['imagemPerfilUsuario']['size']))
        {

            if($resultado->imagem_usuario === 'default.png')
            {
                $config['upload_path']      = './assets/painel/images/usuarios';
                $config['file_name']        = "img_usuario_".rand().".jpg";
                $config['allowed_types']    = '*' ;

                $dados['imagem_usuario'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if (!$this->upload->do_upload('imagemPerfilUsuario'))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'Imagem maior que 500px x 500px<br>';
                    $sinal = true;

                } 

            } else {

                $imagemUsuarioExiste = 'assets/painel/images/usuarios/'.$resultado->imagem_usuario;
            
                if (file_exists($imagemUsuarioExiste)) {
                    
                    $config['upload_path']      = './assets/painel/images/usuarios';
                    $config['file_name']        = "img_usuario_".rand().".jpg";
                    $config['allowed_types']    = '*' ;

                    $dados['imagem_usuario'] = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                if (!$this->upload->do_upload('imagemPerfilUsuario'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem maior que 500px x 500px<br>';
                        $sinal = true;

                    } else {

                        unlink('assets/painel/images/usuarios/'.$resultado->imagem_usuario);
                    }

                } else {

                    $config['upload_path']      = './assets/painel/images/usuarios';
                    $config['file_name']        = "img_usuario_".rand().".jpg";
                    $config['allowed_types']    = '*' ;

                    $dados['imagem_usuario'] = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('imagemPerfilUsuario'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem maior que 500px x 500px<br>';
                        $sinal = true;

                    } 

                }

            }
            
            $dados['status_usuario'] = $resultado->imagem_usuario;
            $dados['token_usuario'] = $resultado->token_usuario;

        }

        $dados['status_usuario'] = $resultado->status_usuario;
        $dados['token_usuario'] = $resultado->token_usuario;
        
        $this->session->set_userdata($dados);

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }

        $resultado = $this->usuarios->m_atualizarDadosUsuario($dados);

        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do usuário <strong>'. $dados['nome_usuario'].'</strong> atualizados com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar os dados do usuário '. $dados['nome_usuario'].', tente novamente mais tarde!';
			echo json_encode($retorno);
        }

    }
}
