<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['modules_locations'] = array(
    APPPATH.'modules/' => '../modules/',
);

$route['default_controller'] = 'site';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// ------------------- ROTA DO PAINEL E LOGIN E LOGOUT DO PAINEL -------------------
$route['dashboard']                             = 'painel';
$route['dashboard/autenticarlogin']['post']     = 'painel/c_autenticarLogin';
$route['dashboard/login']                       = 'painel/login';
$route['dashboard/logout']                      = 'painel/logout';

// ------------------- ROTAS CONFIGS DO  SISTEMA -------------------
$route['dashboard/configs']   = 'configuracoes/layoutSite';

// ------------------- ROTAS DO SITE -------------------
// $route['dashboard/configs']   = 'configuracoes/layoutSite';

// ------------------- ROTAS BACKUP DB SISTEMA -------------------
$route['system_backup/backup_db']  = 'painel/c_backupDB';

// ------------------- ROTAS EMPRESA -------------------

$route['dashboard/empresa/minhaEmpresa']                        = 'empresa';
$route['dashboard/listagens/listarInformacoesEmpresa']          = 'empresa/c_listarInformacoesEmpresa';
$route['dashboard/atualizar/atualizarDadosEmpresa']['post']     = 'empresa/c_atualizarDadosEmpresa';
$route['listagens/primeiroAcesso/checarEmpresa']                = 'empresa/c_checarEmpresa';

// ------------------- ROTAS LANDINGS PAGE -------------------
$route['dashboard/landingPages']                                                = 'landingpage';
$route['dashboard/configuracoes']                                               = 'landingpage/v_configuracoes';
$route['dashboard/cadastrarNovaLandingPage']                                    = 'landingpage/v_cadastrarNovaLandingPage';
$route['dashboard/cadastro/cadastrarNovaLandingPage']['post']                   = 'landingpage/c_cadastrarNovaLandingPage';
$route['dashboard/listagens/listarTodasLandingPages']                           = 'landingpage/c_listarTodasLandingPages';
$route['dashboard/listagens/listarDadosConfigs']                                = 'landingpage/c_listarDadosConfigs';
$route['dashboard/listagens/listarGrupo']                                       = 'landingpage/c_listarGrupos';
$route['dashboard/gruposLandingPage']                                           = 'landingpage/v_gruposLandingPage';
$route['landingpages/atualizar/configuracoes']['post']                          = 'landingpage/atualizarConfigsLandingPage';
$route['landingpages/atualizar/atualizarStatusLandingPage']['post']             = 'landingpage/c_atualizarStatusLandingPage';
$route['landingpages/excluir/excluirLandingPages']['post']                      = 'landingpage/c_excluirLandingPages';
$route['landingpages/atualizar/editarLandingPage']['post']                      = 'landingpage/c_editarLandingPage';
$route['landingpages/(:any)/(:any)']                                            = 'landingpage/v_landingPages/$1/$1';
$route['dashboard/templates']                                                   = 'landingpage/v_templateLandingPages';
$route['dashboard/cadastro/cadastrarNovoTemplateLandingPage']['post']           = 'landingpage/c_cadastrarNovoTemplateLandingPage';
$route['dashboard/listagens/listarTodosTemplates']                              = 'landingpage/c_listarTodosTemplates';
$route['dashboard/listagens/listarTodosTemplatesLandingPages']                  = 'landingpage/c_listarTodosTemplatesLandingPages';
$route['dashboard/atualizar/atualizarStatusTemplates']                          = 'landingpage/c_atualizarStatusTemplates';
$route['dashboard/atualizar/atualizarDadosTemplate']                            = 'landingpage/c_atualizarDadosTemplate';
$route['dashboard/excluir/excluirTemplate']                                     = 'landingpage/c_excluirTemplate';
$route['dashboard/atualizar/atualizarTemplateSelecionadoLandingPage']['post']   = 'landingpage/c_atualizarTemplateSelecionadoLandingPage';
$route['dashboard/templates/selecionarTemplate/(:any)']                         = 'landingpage/v_selecionarTemplate/(:any)';
$route['dashboard/templates/configurarTemplate/(:any)']                         = 'landingpage/v_configurarTemplate/(:any)';

// ------------------- ROTAS RELATÓRIOS LEADS -------------------

$route['dashboard/landingPages/leads/(:any)']                           = 'landingpage/Leads/v_leads/(:any)';
$route['dashboard/listagens/listarRelatorioAcessos/(:any)']             = 'landingpage/Leads/c_listarRelatorioAcessos/$1';
$route['dashboard/listagens/listarRelatoriosLeads/(:any)']              = 'landingpage/Leads/c_listarRelatoriosLeads/$1';
$route['dashboard/listagens/listarRelatorioSemana/(:any)']              = 'landingpage/Leads/c_listarRelatorioSemana/$1';
$route['dashboard/listagens/listarRelatorioMensal/(:any)']              = 'landingpage/Leads/c_listarRelatorioMensal/$1';
$route['dashboard/listagens/listarRelatorioAnual/(:any)']               = 'landingpage/Leads/c_listarRelatorioAnual/$1';
$route['dashboard/listagens/listarRelatoriosCadastros/(:any)']          = 'landingpage/Leads/c_listarRelatoriosCadastros/$1';
$route['dashboard/listagens/listarRelatorioCadastrosDiario/(:any)']     = 'landingpage/Leads/c_listarRelatorioCadastrosDiario/$1';
$route['dashboard/listagens/listarRelatorioCadastrosSemanal/(:any)']    = 'landingpage/Leads/c_listarRelatorioCadastrosSemanal/$1';
$route['dashboard/listagens/listarRelatorioCadastrosMensal/(:any)']     = 'landingpage/Leads/c_listarRelatorioCadastrosMensal/$1';
$route['dashboard/listagens/listarRelatorioCadastrosAnual/(:any)']      = 'landingpage/Leads/c_listarRelatorioCadastrosAnual/$1';
$route['dashboard/listagens/listarRelatorioCadastrosFacebook/(:any)']   = 'landingpage/Leads/c_listarRelatorioCadastrosFacebook/$1';
$route['dashboard/listagens/listarRelatorioCadastrosInstagram/(:any)']  = 'landingpage/Leads/c_listarRelatorioCadastrosInstagram/$1';
$route['dashboard/listagens/listarRelatorioCadastrosWhatsApp/(:any)']   = 'landingpage/Leads/c_listarRelatorioCadastrosWhatsApp/$1';
$route['dashboard/listagens/listarRelatorioCadastrosGoogle/(:any)']     = 'landingpage/Leads/c_listarRelatorioCadastrosGoogle/$1';
$route['dashboard/listagens/listarRelatorioCadastrosYouTube/(:any)']    = 'landingpage/Leads/c_listarRelatorioCadastrosYouTube/$1';
$route['dashboard/listagens/listarRelatorioCadastrosMessenger/(:any)']  = 'landingpage/Leads/c_listarRelatorioCadastrosMessenger/$1';
$route['dashboard/listagens/listarRelatorioCadastrosAmigos/(:any)']     = 'landingpage/Leads/c_listarRelatorioCadastrosAmigos/$1';
$route['dashboard/listagens/listarRelatorioCadastrosOutros/(:any)']     = 'landingpage/Leads/c_listarRelatorioCadastrosOutros/$1';
$route['dashboard/listagens/listarRelatorioCadastrosTotais/(:any)']     = 'landingpage/Leads/c_listarRelatorioCadastrosTotais/$1';
$route['dashboard/listagens/listarRelatoriosClicks/(:any)']             = 'landingpage/Leads/c_listarRelatoriosClicks/$1';
$route['dashboard/listagens/listarRelatorioClicksDiario/(:any)']        = 'landingpage/Leads/c_listarRelatorioClicksDiario/$1';
$route['dashboard/listagens/listarRelatorioClicksSemanal/(:any)']       = 'landingpage/Leads/c_listarRelatorioClicksSemanal/$1';
$route['dashboard/listagens/listarRelatorioClicksMensal/(:any)']        = 'landingpage/Leads/c_listarRelatorioClicksMensal/$1';
$route['dashboard/listagens/listarRelatorioClicksAnual/(:any)']         = 'landingpage/Leads/c_listarRelatorioClicksAnual/$1';
$route['dashboard/listagens/listarRelatorioClicksBotoes/(:any)']        = 'landingpage/Leads/c_listarRelatorioClicksBotoes/$1';

// ------------------- ROTAS RELATÓRIOS DOWNLOADS DE LEADS ACESSOS -------------------

$route['dashboard/relatorios/downListarRelatoriosAcessosDia/(:any)']        = 'landingpage/Leads/c_downListarRelatoriosAcessosDia/$1';
$route['dashboard/relatorios/downListarRelatoriosAcessosSemana/(:any)']     = 'landingpage/Leads/c_downListarRelatoriosAcessosSemana/$1';
$route['dashboard/relatorios/downListarRelatoriosAcessosMes/(:any)']        = 'landingpage/Leads/c_downListarRelatoriosAcessosMes/$1';
$route['dashboard/relatorios/downListarRelatoriosAcessosAno/(:any)']        = 'landingpage/Leads/c_downListarRelatoriosAcessosAno/$1';

// ------------------- ROTAS RELATÓRIOS DOWNLOADS DE LEADS CADASTROS -------------------

$route['dashboard/relatorios/downListarRelatoriosCadastroDia/(:any)']       = 'landingpage/Leads/c_downListarRelatoriosCadastroDia/$1';
$route['dashboard/relatorios/downListarRelatoriosCadastroSemana/(:any)']    = 'landingpage/Leads/c_downListarRelatoriosCadastroSemana/$1';
$route['dashboard/relatorios/downListarRelatoriosCadastroMes/(:any)']       = 'landingpage/Leads/c_downListarRelatoriosCadastroMes/$1';
$route['dashboard/relatorios/downListarRelatoriosCadastroAno/(:any)']       = 'landingpage/Leads/c_downListarRelatoriosCadastroAno/$1';

// ------------------- ROTAS RELATÓRIOS DOWNLOADS DE LEADS CLICKS NOS BOTÕES DA LANDINGPAGE -------------------

$route['dashboard/relatorios/downListarRelatoriosClicksBotoesDia/(:any)']       = 'landingpage/Leads/c_downListarRelatoriosClicksBotoesDia/$1';
$route['dashboard/relatorios/downListarRelatoriosClicksBotoesSemana/(:any)']    = 'landingpage/Leads/c_downListarRelatoriosClicksBotoesSemana/$1';
$route['dashboard/relatorios/downListarRelatoriosClicksBotoesMes/(:any)']       = 'landingpage/Leads/c_downListarRelatoriosClicksBotoesMes/$1';
$route['dashboard/relatorios/downListarRelatoriosClicksBotoesAno/(:any)']       = 'landingpage/Leads/c_downListarRelatoriosClicksBotoesAno/$1';
$route['dashboard/listagens/listarHeadSEO/(:any)']                              = 'landingpage/Modelos/c_listarHeadSEO/(:any)';
$route['dashboard/atualizar/atualizarHeadSEO']['post']                          = 'landingpage/Modelos/c_atualizarHeadSEO';
$route['dashboard/listagens/listarLogoMenuSlide/(:any)']                        = 'landingpage/Modelos/c_listarLogoMenuSlide/(:any)';
$route['dashboard/atualizar/atualizarformLogoMenuSlide']['post']                = 'landingpage/Modelos/c_atualizarformLogoMenuSlide';
$route['dashboard/listagens/listarForms/(:any)']                                = 'landingpage/Modelos/c_listarForms/(:any)';
$route['dashboard/atualizar/atualizarFormularios']['post']                      = 'landingpage/Modelos/c_atualizarFormularios';
$route['dashboard/listagens/listarRedesSociais/(:any)']                         = 'landingpage/Modelos/c_listarRedesSociais/(:any)';
$route['dashboard/atualizar/atualizarRedesSociais']['post']                     = 'landingpage/Modelos/c_atualizarRedesSociais';
$route['dashboard/listagens/listarServicos/(:any)']                             = 'landingpage/Modelos/c_listarServicos/(:any)';
$route['dashboard/atualizar/atualizarServico1']['post']                         = 'landingpage/Modelos/c_atualizarServico1';
$route['dashboard/atualizar/atualizarServico2']['post']                         = 'landingpage/Modelos/c_atualizarServico2';
$route['dashboard/atualizar/atualizarServico3']['post']                         = 'landingpage/Modelos/c_atualizarServico3';
$route['dashboard/atualizar/atualizarServico4']['post']                         = 'landingpage/Modelos/c_atualizarServico4';
$route['dashboard/atualizar/atualizarServico5']['post']                         = 'landingpage/Modelos/c_atualizarServico5';
$route['dashboard/atualizar/atualizarServico6']['post']                         = 'landingpage/Modelos/c_atualizarServico6';
$route['dashboard/atualizar/atualizarServico7']['post']                         = 'landingpage/Modelos/c_atualizarServico7';
$route['dashboard/atualizar/atualizarServico8']['post']                         = 'landingpage/Modelos/c_atualizarServico8';
$route['dashboard/atualizar/atualizarServico_paginaSucesso']['post']            = 'landingpage/Modelos/c_atualizarServico_paginaSucesso';
$route['dashboard/contador']['post']                                            = 'landingpage/c_contador';
$route['dashboard/contador/tempoOnline/(:any)']['post']                         = 'landingpage/c_tempoOnline/(:any)';
$route['dashboard/contador/clicks/(:any)']['post']                              = 'landingpage/c_clicks/(:any)';
$route['listagens/listarOpcoesOndeNosConheceu']['post']                         = 'landingpage/c_listarOpcoesOndeNosConheceu';
$route['dashboard/cadastro/leadsCadastroForm1/(:any)']['post']                  = 'landingpage/c_leadsCadastroForm1/(:any)';

$route['landingpage/(:any)/sucesso'] = 'landingpage/v_cadastroLeadsSucesso/$1';

// ------------------- ROTAS USUARIOS -------------------

$route['dashboard/usuarios/usuarios']                           = 'usuarios';
$route['dashboard/usuarios/listarUsuarios']                     = 'usuarios/c_listarUsuarios';
$route['dashboard/usuarios/cadastrarUsuario']['post']           = 'usuarios/c_cadastrarUsuario';
$route['dashboard/usuarios/atualizarDadosUsuario']['post']      = 'usuarios/c_atualizarDadosUsuario';
$route['dashboard/usuarios/atualizarStatusUsuario']['post']     = 'usuarios/c_atualizarStatusUsuario';
$route['dashboard/usuarios/deletarDadosUsuario']['post']        = 'usuarios/c_deletarDadosUsuario';
$route['dashboard/usuarios/minhaconta']                         = 'usuarios/v_minhaconta';
$route['dashboard/usuarios/dadosMinhaConta']                    = 'usuarios/c_dadosMinhaConta';
$route['dashboard/usuarios/atualizarMinhaContaUsuario']         = 'usuarios/c_atualizarMinhaContaUsuario';

$route['dashboard/usuarios/listaNivelAcessoUsuarios']           = 'painel/c_listaNivelAcessoUsuarios';

// ------------------- ROTAS NIVEIS DE ACESSO E PERMISSÕES -------------------

$route['dashboard/permissoes/niveisAcesso']                     = 'permissoes/v_niveisAcesso';
$route['dashboard/permissoes/paginas']                          = 'permissoes/v_paginas';
$route['dashboard/permissoes/criarPagina']['post']              = 'permissoes/c_criarPagina';
$route['dashboard/permissoes/listarPaginas']                    = 'permissoes/c_listarPaginas';
$route['dashboard/permissoes/atualizarDadosPaginas']['post']    = 'permissoes/c_atualizarDadosPaginas';
$route['dashboard/permissoes/deletarDadosPagina']['post']       = 'permissoes/c_deletarDadosPagina';
$route['dashboard/permissoes/criarNivelAcesso']['post']         = 'permissoes/c_criarNivelAcesso';
$route['dashboard/permissoes/listarNiveisAcesso']               = 'permissoes/c_listarNiveisAcesso';
$route['dashboard/permissoes/atualizarNivelAcesso']['post']     = 'permissoes/c_atualizarNivelAcesso';
$route['dashboard/permissoes/deletarNivelAcesso']['post']       = 'permissoes/c_deletarNivelAcesso';

$route['dashboard/permissoes/permissaoNiveisAcesso/(:any)']             = 'permissoes/v_permissaoNiveisAcesso/(:any)';
$route['dashboard/permissoes/listaNiveisPaginasDisponiveis/(:any)']     = 'permissoes/v_listaNiveisPaginasDisponiveis/$1';
$route['dashboard/permissoes/listaNiveisPaginasADD/(:any)']             = 'permissoes/v_listaNiveisPaginasADD/$1';
$route['dashboard/permissoes/associarRemoverPagina']['post']            = 'permissoes/c_associarRemoverPagina';
$route['dashboard/permissoes/bloquearPermissao']['post']                = 'permissoes/c_bloquearPermissao';
$route['dashboard/permissoes/liberarBloquearPermissao']['post']         = 'permissoes/c_liberarBloquearPermissao';
$route['dashboard/permissoes/liberarBloquearMenu']['post']              = 'permissoes/c_liberarBloquearMenu';

// ------------------- ROTAS NEWSLETTER -------------------

$route['dashboard/newsletter']                          = 'newsletter';
$route['dashboard/newsletter/grupos']                   = 'newsletter/v_grupos';
$route['dashboard/newsletter/emails']                   = 'newsletter/v_emails';
$route['dashboard/newsletter/templateEmail']            = 'newsletter/v_templateEmail';
$route['dashboard/newsletter/gerenciar_template']       = 'newsletter/v_gerenciar_template';
$route['dashboard/newsletter/configuracoes_disparo']    = 'newsletter/v_configuracoes_disparo';

$route['dashboard/listagens/listarConfigs_disparo']             = 'newsletter/c_listarConfigs_disparo';
$route['dashboard/atualizar/atualizarConfigsDisparo']['post']   = 'newsletter/c_atualizarConfigsDisparo';

//ROTAS DOS GRUPOS
$route['dashboard/grupos']                  = 'newsletter/v_grupos';
$route['cadastros/cadastrarGrupo']['post']  = 'newsletter/c_cadastrarGrupo';
$route['listagens/listarGrupo']             = 'newsletter/c_listarGrupos';
$route['alteracoes/alterarGrupo']['post']   = 'newsletter/c_alterarGrupos';
$route['exclusoes/excluirGrupo']['post']    = 'newsletter/c_excluirGrupo';

//ROTAS DOS E-MAILS
$route['cadastros/cadastrarEmailIndividual']['post']    = 'newsletter/c_cadastrarEmailIndividual';
$route['listagens/listarListasEmails']                  = 'newsletter/c_listarListasEmails';
$route['cadastros/cadastrarListaEmailCSV']['post']      = 'newsletter/c_cadastrarListaEmailCSV';
$route['exclusoes/excluirEmailsLista']['post']          = 'newsletter/c_excluirEmailsLista';

//ROTA DE PREPARA E-MAILS PARA ENVIO
$route['cadastros/cadastrarNovaCampanha']['post']       = 'newsletter/c_cadastrarNovaCampanha';
$route['alteracoes/alterarCampanha']['post']            = 'newsletter/c_alterarCampanha';
$route['listagens/listarCampanhas']                     = 'newsletter/c_listarCampanhas';
$route['listagens/listarCampanhasEnviadas']             = 'newsletter/c_listarCampanhasEnviadas';
$route['exclusoes/excluirCampanha']['post']             = 'newsletter/c_excluirCampanha';
$route['cadastros/prepararCampanhaEnvio']['post']       = 'newsletter/c_prepararCampanhaEnvio';

//ROTA MODELOS DE TEMPLATE DE E-MAILS
$route['cadastros/cadastrarNovoModeloTemplateEmail']['post']            = 'newsletter/c_cadastrarNovoModeloTemplateEmail';
$route['listagens/listarModelosTemplateEmails']                         = 'newsletter/c_listarModelosTemplateEmails';
$route['dashboard/atualizar/atualizarStatusModeloTemplate']['post']     = 'newsletter/c_atualizarStatusModeloTemplate';
$route['dashboard/atualizar/atualizarInfosModeloTemplate']['post']      = 'newsletter/c_atualizarInfosModeloTemplate';
$route['dashboard/exclusoes/excluirModeloTemplateEmail']['post']        = 'newsletter/c_excluirModeloTemplateEmail';
$route['listagens/listarConfigsTemplateEmails']                         = 'newsletter/c_listarConfigsTemplateEmails';
$route['dashboard/atualizar/atualizarDadosConfigsTempEmail']['post']    = 'newsletter/c_atualizarDadosConfigsTempEmail';
$route['listagens/listarCampanhasTemplates']                            = 'newsletter/c_listarCampanhasTemplates';
$route['listagens/listarModelosTemplateEmailsTabela']                   = 'newsletter/c_listarModelosTemplateEmailsTabela';
$route['alteracoes/atualizarTemplateEmailCampanha']['post']             = 'newsletter/c_atualizarTemplateEmailCampanha';

//ROTA CANCELAMENTO DE INSCRIÇÃO DE NEWSLETTER
$route['newsletter/cancelar_inscricao']         = 'newsletter/v_cancelar_inscricao';
$route['newsletter/cancelar_inscricaoNews']['post'] = 'newsletter/c_cancelar_inscricaoNews';

//ROTAS ACOMPANHAR ENVIOS CAMPANHAS
$route['dashboard/newsletter/disparos']     = 'newsletter/v_disparos';
$route['listagens/listarEnviosAndamento']   = 'newsletter/c_listarEnviosAndamento';
$route['listagens/listarEnviosEnviadosOK']  = 'newsletter/c_listarEnviosEnviadosOK';

//ROTA ENVIAR NEWSLETTER
$route['newsletter/disparo_newsletter']  = 'newsletter/c_disparo_newsletter';

//ROTA CONFIRMAR ABERTURA DE E-MAIL DO USUÁRIO
$route['newsletter/confirmarEmail']  = 'newsletter/c_confirmarEmail';

//DOWNLOD RELATORIO DE ENVIOS DAS CAMPANHAS
$route['dashboard/listagens/relatorioEnvios/(:any)'] = 'newsletter/c_relatorioEnvios/$1';
