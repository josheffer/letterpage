<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TemplateLandingPage_model extends CI_Model {

    
    
    public function m_gerarCodigoCriarTemplate() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_templates) as codigo FROM templates_landing_pages");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    public function m_criarNovoTemplate($dados)
    {
        $resultado = $this->db->insert('templates_landing_pages', $dados);
        return $resultado;
    }

    public function m_listarTodosTemplates()
    {
        $this->db->select("*");
        $this->db->order_by('id_templates', 'DESC');
        $resultado = $this->db->get("templates_landing_pages")->result();

        return $resultado;
    }
    
    public function m_listarTodosTemplatesLandingPages()
    {
        $resultado = $this->db->query("SELECT * FROM templates_landing_pages WHERE statusTemplate_templates = '1' GROUP BY id_templates DESC;")->result();

        return $resultado;
    }

    public function buscarTemplate($id)
    {
        $resultado = $this->db->query("SELECT * FROM templates_landing_pages WHERE id_templates =". $id)->result();
        
        return $resultado;
    }

    public function m_atualizarStatusTemplate($dados)
    {
        
        $resultado = $this->db->query("UPDATE templates_landing_pages SET statusTemplate_templates = '".$dados['statusTemplate_templates']."' WHERE id_templates = '".$dados['id_templates']."';");
        
        return $resultado;
    }

    public function m_atualizarDadosTemplate($dados)
    {

        $this->db->where('id_templates', $dados['id_templates']);

        return $this->db->update('templates_landing_pages', $dados);

    }

    public function m_excluirTemplate($id)
    {
        $resultado = $this->db->query("DELETE FROM templates_landing_pages WHERE id_templates = '".$id."';");
        
        return $resultado;
        
    }

    public function m_atualizarTemplateSelecionadoLandingPage($dados)
    {
        
        $resultado = $this->db->query("UPDATE landing_pages SET template_landingPage = '".$dados['template_landingPage']."' WHERE id_landingPage = '".$dados['id_landingPage']."';");
        
        return $resultado;

    }
    
    public function buscarConfigsHead($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modeloconfig_head
                                        WHERE id_landingPage = '".$id."' 
                                        LIMIT 1")->result();

        foreach ($resultado as $resultRow) {

            return $resultRow;

        }
    }

    public function buscarConfigsLogoMenuSlide($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modeloconfig_logomenuslide
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsRedesSociais($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_redessociais
                                        WHERE id_landingPage = '".$id."'  
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsServico1($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico1
                                        WHERE id_landingPage = '".$id."' 
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsServico2($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico2
                                        WHERE id_landingPage = '".$id."' 
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsServico3($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico3
                                        WHERE id_landingPage = '".$id."' 
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsServico4($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico4
                                        WHERE id_landingPage = '".$id."' 
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsServico5($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico5
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsServico6($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico6
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsServico7($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico7
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsServico8($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico8
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsMaps($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico_maps
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsForms($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_forms
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsPersonalizacao($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_personalizacao
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }

    public function buscarConfigsPaginaSucesso($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_servico_paginasucesso
                                        WHERE id_landingPage = '".$id."'
                                        LIMIT 1")->result();
    
        foreach ($resultado as $resultRow) {
                
            return $resultRow;

        }
    }
    
}