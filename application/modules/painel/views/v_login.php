<!DOCTYPE html>

<html lang="pt-br">

    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title><?=$titulo?></title>
    <meta content="SolidSystem Softwares Inteligentes - Captura de leads com páginas personalizadas, disparo de e-mails em massa, acompanhamento em tempo real dos disparos.." name="description">
    <meta content="Josheffer Robert" name="author">
    <link rel="shortcut icon" href="/assets/painel/images/favicon.png">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    
    <div class="wrapper-page">
        <div class="card">
            <div class="card-body">
                
                <h3 class="text-center">
                    <a href="https://solidsystem.net.br" target="_blank" class="logo logo-admin">
                        <img src="/assets/painel/images/logologin.png" height="40" alt="logo">
                    </a>
                </h3>

                <h6 class="text-center font-12">Sistema desenvolvido em parceria com</h6>

                <hr>

                <h3 class="text-center mt-3">
                    <a href="javascript:void(0);" class="logo logo-admin">
                        <img src="<?=base_url('/assets/painel/images/empresa/logotipo/'.$logoEmpresa)?>" height="80" alt="logo">
                    </a>
                </h3>


                <div class="p-3">
                    <h4 class="text-white font-18 m-b-5 text-center">Acessar painel!</h4>
                    <p class="text-white-50 text-center">Seja bem vindo ao painel!</p>
                    <form id="formLogin">
                        <div class="form-group">
                            <label for="username">Usuário</label>
                            <input type="text"class="form-control" id="usuario" name="usuario" autocomplete="off" autofocus>
                        </div>
                        <div class="form-group">
                            <label for="userpassword">Senha</label>
                            <input type="password" class="form-control" id="senha" name="senha" autocomplete="off">
                        </div>
                        <div class="form-group row m-t-20">
                            <div class="col-12 text-right">
                                <button class="btn btn-block btn-primary w-md waves-effect waves-light" id="botaoAcessarLogin" type="submit">Acessar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="msgErro"></div>
            </div>
        </div>
        <div class="m-t-40 text-center">
            <p>2019 - <?=date('Y')?> © Sistema desenvolvido por <a href="https://solidsystem.net.br" target="_blank" class="text-warning" rel="noopener noreferrer">solidsystem.net.br</a></p>
        </div>
    </div>

    <script src="/assets/js/vendor.min.js"></script>
    <script src="/assets/js/jquery-3.5.1.min.js"></script>
    <script src="/assets/js/vendor/login/autenticarlogin.js"></script>
    <script src="/assets/js/app.min.js"></script>
    
</body>
</html>