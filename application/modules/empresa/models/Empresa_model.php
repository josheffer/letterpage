<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model {
    
    public function m_listarInformacoesEmpresa()
    {
        $resultado = $this->db->query("SELECT * FROM empresa ")->result_array();
                                                    
        foreach ($resultado as $dados)
        {
            return $dados;    
        }
    }

    public function m_atualizarDadosEmpresa($dados)
    {
        $resultado = $this->db->query("UPDATE empresa SET 
                                            
                                            razaoSocial_empresa             = '".$dados['razaoSocial_empresa']."', 
                                            nomeFantasia_empresa            = '".$dados['nomeFantasia_empresa']."', 
                                            email_empresa                   = '".$dados['email_empresa']."', 
                                            telefone_empresa                = '".$dados['telefone_empresa']."', 
                                            nomeResponsavel_empresa         = '".$dados['nomeResponsavel_empresa']."', 
                                            telefoneResponsavel_empresa     = '".$dados['telefoneResponsavel_empresa']."',
                                            emailResponsavel_empresa        = '".$dados['emailResponsavel_empresa']."',
                                            imagem_empresa                  = '".$dados['imagem_empresa']."',
                                            status_empresa                  = '".$dados['status_empresa']."',
                                            cpfResponsavel_empresa          = '".$dados['cpfResponsavel_empresa']."'

                                        WHERE id_empresa            = '1';");

        return $resultado;  
    } 
    
    public function m_checarEmpresa()
    {
        $resultado = $this->db->query("SELECT status_empresa FROM empresa")->result_array();
                                                    
        foreach ($resultado as $dados)
        {
            return $dados;    
        }
    }
    
}