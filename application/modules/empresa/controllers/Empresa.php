<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        $this->load->model("Empresa_model", "empresa");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->library('permitiracesso');
        $this->load->model("usuarios/Usuarios_model", "usuarios");

    }
    
	public function index()
	{
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $view = "v_empresa";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $scripts = array(

            "titulo" => "Dados da minha empresa | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsEmpresa.js'></script>",
            "scriptsCSS" => "", 
            "scriptsCSSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
        $this->load->view('painel/Template/t_menuLateral', $dadosSessao);
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function c_listarInformacoesEmpresa()
    {
        $resultado = $this->empresa->m_listarInformacoesEmpresa();

        echo json_encode($resultado);
    }

    public function c_atualizarDadosEmpresa()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $resultado = (object)$this->empresa->m_listarInformacoesEmpresa();
        
        $dados['razaoSocial_empresa']           = $this->input->post('razaoSocialEmpresa');
        $dados['nomeFantasia_empresa']          = $this->input->post('nomeFantasiaEmpresa');
        $dados['cnpj_empresa']                  = $this->input->post('cnpjEmpresa');
        $dados['email_empresa']                 = $this->input->post('emailEmpresa');
        $dados['telefone_empresa']              = $this->input->post('telefoneEmpresa');
        $dados['nomeResponsavel_empresa']       = $this->input->post('nomeResponsavelEmpresa');
        $dados['telefoneResponsavel_empresa']   = $this->input->post('telefoneResponsavelEmpresa');
        $dados['emailResponsavel_empresa']      = $this->input->post('emailResponsavelEmpresa');
        $dados['cpfResponsavel_empresa']        = $this->input->post('cpfResponsavelEmpresa');

        if(empty($dados['razaoSocial_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'A <strong>RAZÃO SOCIAL</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['nomeFantasia_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>NOME FANTASIA</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['cnpj_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>CNPJ</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['email_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>E-MAIL</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['telefone_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>TELEFONE</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['nomeResponsavel_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>NOME DO RESPONSÁVEL</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['telefoneResponsavel_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>TELEFONE DO RESPONSÁVEL</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['emailResponsavel_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>E-MAIL DO RESPONSÁVEL</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['cpfResponsavel_empresa']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>C.P.F DO RESPONSÁVEL</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        if($_FILES['arquivoLogotipoEmpresa']['size'] === 0 )
        {
            $dados['imagem_empresa'] = $resultado->imagem_empresa;
        }

        if(!empty($_FILES['arquivoLogotipoEmpresa']['size']))
        {

            if($resultado->imagem_empresa === 'default.png')
            {
                $config['upload_path']      = './assets/painel/images/empresa/logotipo';
                $config['file_name']        = "img_logo_empresa_".rand().".png";
                $config['max_width']        = 1080;
                $config['max_height']       = 1080;
                $config['allowed_types']    = '*' ;

                $dados['imagem_empresa'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if (!$this->upload->do_upload('arquivoLogotipoEmpresa'))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'Imagem maior que 1080px x 1080px<br>';
                    $sinal = true;

                } 
                
            } else {

                $logotipoExiste = 'assets/painel/images/empresa/logotipo/'.$resultado->imagem_empresa;
            
                if (file_exists($logotipoExiste)) {
                    
                    $config['upload_path']      = './assets/painel/images/empresa/logotipo';
                    $config['file_name']        = "img_logo_empresa_".rand().".png";
                    $config['max_width']        = 1080;
                    $config['max_height']       = 1080;
                    $config['allowed_types']    = '*' ;

                    $dados['imagem_empresa'] = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('arquivoLogotipoEmpresa'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem maior que 1080px x 1080px<br>';
                        $sinal = true;

                    } else {

                        unlink('assets/painel/images/empresa/logotipo/'.$resultado->imagem_empresa);
                    }
                    
                } else {

                    $config['upload_path']      = './assets/painel/images/empresa/logotipo';
                    $config['file_name']        = "img_logo_empresa_".rand().".png";
                    $config['max_width']        = 1080;
                    $config['max_height']       = 1080;
                    $config['allowed_types']    = '*' ;

                    $dados['imagem_empresa'] = $config['file_name'];

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if (!$this->upload->do_upload('arquivoLogotipoEmpresa'))
                    {
                        $retorno['ret'] = false;
                        $retorno['msg'].= 'Imagem maior que 1080px x 1080px<br>';
                        $sinal = true;

                    }
                    

                }
                
            }
            
        }
        
        $dados['status_empresa'] = 1;
        
        $this->session->set_userdata($dados);
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $resultado = $this->empresa->m_atualizarDadosEmpresa($dados);

        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados da empresa atualizados com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível atualizar os dados, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
            
    }

    public function c_checarEmpresa()
    {
        $resultado = $this->empresa->m_checarEmpresa();

        echo json_encode($resultado);
    }
    
}
