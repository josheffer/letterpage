
<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">

        <li class="dropdown notification-list">
            
            <a class="nav-link dropdown-toggle mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                
                <button type="button" class="btn btn-xs btn-light waves-effect">
                    <img src="<?=base_url('assets/painel/images/empresa/logotipo/').$dados['imagem_empresa'];?>" id="imagemEmpresaBarra" alt="Logotipo empresa" style="height: 30px;">
                </button>    
            
                <span class="pro-user-name ml-1" >
                <span id="nomeEmpresaBarra" ><?=$dados['nomeEmpresa']?></span> <i class="mdi mdi-chevron-down"></i> 
                </span>
            </a>
            
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                
                <a href="/dashboard/empresa/minhaEmpresa" class="dropdown-item notify-item">
                    <i class="mdi mdi-bank"></i>
                    <span>Dados da empresa</span>
                </a>
                
            </div>
        </li>
        
        <li class="dropdown notification-list">
            
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="<?=base_url('assets/painel/images/usuarios/').$dados['imagem_usuario'];?>" id="imagemPerfilUsuarioBarra" alt="Usuário Clik Ofertas" class="rounded-circle">
                <span class="pro-user-name ml-1">
                    <span id="nomeUsuarioLogadoBarra"><?=$dados['nome']?></span> <i class="mdi mdi-chevron-down"></i> 
                </span>
            </a>
            
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                
                <div class="dropdown-item noti-title">
                    <h5 class="m-0 text-white">
                        Bem vindo!
                    </h5>
                </div>
                
                <a href="/dashboard/usuarios/minhaconta" class="dropdown-item notify-item">
                    <i class="fe-user"></i>
                    <span>Minha conta</span>
                </a>

                <div class="dropdown-divider"></div>
                
                <a href="/dashboard/logout" class="dropdown-item notify-item">
                    <i class="fe-log-out"></i>
                    <span>Sair</span>
                </a>

            </div>
        </li>
        
    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a href="/dashboard" class="logo text-center">
            <span class="logo-lg" style="background-color: #2e333c;">
                <img src="<?=base_url('assets/painel/images/logo-dashboard.png')?>" class="mt-1 mb-1" alt="" height="50">
                <!-- <span class="logo-lg-text-light">Olho No Clik</span> -->
            </span>
            <span class="logo-sm" style="background-color: #2e333c;">
                <!-- <span class="logo-sm-text-dark">X</span> -->
                <img src="<?=base_url('assets/painel/images/logo-sm.png')?>" alt="" height="40">
            </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile waves-effect waves-light" id="botaoMobileAbrirEFechar">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </li>
        
    </ul>

</div>