primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarSlides();
listarParceiros();

function listarSlides() {

    $.ajax({

        url: "/dashboard/listagens/listarSlides",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            if (dados.ativo_slideGeral === '1') { $('#ativo_slideGeral').val(1).attr("checked", "checked"); } else { $('#ativo_slideGeral').val(1); }

            if (dados.ativo_slide1 === '1') { $('#ativo_slide1').val(1).attr("checked", "checked"); } else { $('#ativo_slide1').val(1); }
            if (dados.ativo_botaoSlide1 === '1') { $('#ativo_Botaoslide1').val(1).attr("checked", "checked"); } else { $('#ativo_Botaoslide1').val(1); }
            $('#titulo_slide1').val(dados.titulo_slide1);
            $('#descricao_slide1').val(dados.descricao_slide1);
            $('#descricao_slide1').val(dados.descricao_slide1);
            $('#textoBotao_slide1').val(dados.texto_botaoSlide1);
            $('#corTextoBotao_slide1').val(dados.corTexto_botaoSlide1);
            $('#corTextoHoverBotao_slide1').val(dados.corTextoHover_botaoSlide1);
            $('#corFundoBotao_slide1').val(dados.corFundo_botaoSlide1);
            $('#CorFundoHoverBotao_slide1').val(dados.corFundoHover_botaoSlide1);
            $('#corTituloDescricao_slide1').val(dados.corTituloDescricao_slide1);

            if (dados.ativo_slide2 === '1') { $('#ativo_slide2').val(1).attr("checked", "checked"); } else { $('#ativo_slide2').val(1); }
            if (dados.ativo_botaoSlide2 === '1') { $('#ativo_Botaoslide2').val(1).attr("checked", "checked"); } else { $('#ativo_Botaoslide2').val(1); }
            $('#titulo_slide2').val(dados.titulo_slide2);
            $('#descricao_slide2').val(dados.descricao_slide2);
            $('#descricao_slide2').val(dados.descricao_slide2);
            $('#textoBotao_slide2').val(dados.texto_botaoSlide2);
            $('#corTextoBotao_slide2').val(dados.corTexto_botaoSlide2);
            $('#corTextoHoverBotao_slide1').val(dados.corTextoHover_botaoSlide2);
            $('#corFundoBotao_slide2').val(dados.corFundo_botaoSlide2);
            $('#CorFundoHoverBotao_slide2').val(dados.corFundoHover_botaoSlide2);
            $('#corTituloDescricao_slide2').val(dados.corTituloDescricao_slide2);

            if (dados.ativo_slide3 === '1') { $('#ativo_slide3').val(1).attr("checked", "checked"); } else { $('#ativo_slide3').val(1); }
            if (dados.ativo_botaoSlide3 === '1') { $('#ativo_Botaoslide3').val(1).attr("checked", "checked"); } else { $('#ativo_Botaoslide3').val(1); }
            $('#titulo_slide3').val(dados.titulo_slide3);
            $('#descricao_slide3').val(dados.descricao_slide3);
            $('#descricao_slide3').val(dados.descricao_slide3);
            $('#textoBotao_slide3').val(dados.texto_botaoSlide3);
            $('#corTextoBotao_slide3').val(dados.corTexto_botaoSlide3);
            $('#corTextoHoverBotao_slide3').val(dados.corTextoHover_botaoSlide3);
            $('#corFundoBotao_slide3').val(dados.corFundo_botaoSlide3);
            $('#CorFundoHoverBotao_slide3').val(dados.corFundoHover_botaoSlide3);
            $('#corTituloDescricao_slide3').val(dados.corTituloDescricao_slide3);

            if (dados.ativo_slide4 === '1') { $('#ativo_slide4').val(1).attr("checked", "checked"); } else { $('#ativo_slide4').val(1); }
            if (dados.ativo_botaoSlide4 === '1') { $('#ativo_Botaoslide4').val(1).attr("checked", "checked"); } else { $('#ativo_Botaoslide4').val(1); }
            $('#titulo_slide4').val(dados.titulo_slide4);
            $('#descricao_slide4').val(dados.descricao_slide4);
            $('#descricao_slide4').val(dados.descricao_slide4);
            $('#textoBotao_slide4').val(dados.texto_botaoSlide4);
            $('#corTextoBotao_slide4').val(dados.corTexto_botaoSlide4);
            $('#corTextoHoverBotao_slide4').val(dados.corTextoHover_botaoSlide4);
            $('#corFundoBotao_slide4').val(dados.corFundo_botaoSlide4);
            $('#CorFundoHoverBotao_slide4').val(dados.corFundoHover_botaoSlide4);
            $('#corTituloDescricao_slide4').val(dados.corTituloDescricao_slide4);

            $('#imagemSlide1').attr("src", "/assets/site/images/banners/" + dados.imagem_slide1);
            $('#imagemSlide2').attr("src", "/assets/site/images/banners/" + dados.imagem_slide2);
            $('#imagemSlide3').attr("src", "/assets/site/images/banners/" + dados.imagem_slide3);
            $('#imagemSlide4').attr("src", "/assets/site/images/banners/" + dados.imagem_slide4);

            $('#linkBotao_slide1').val(dados.link_botaoSlide1);
            $('#linkBotao_slide2').val(dados.link_botaoSlide2);
            $('#linkBotao_slide3').val(dados.link_botaoSlide3);
            $('#linkBotao_slide4').val(dados.link_botaoSlide4);

            $('#seo_slide1').val(dados.seo_slide1);
            $('#seo_slide2').val(dados.seo_slide2);
            $('#seo_slide3').val(dados.seo_slide3);
            $('#seo_slide4').val(dados.seo_slide4);

        }
    });
}

$('#formAtualizarSlides').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarSlides(self);
});

function atualizarSlides(dados) {

    var form = $('#formAtualizarSlides')[0];

    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarSlides",
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 400000,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#erroMsg').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarHeadSEO();

            } else {

                $('#erroMsg').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarHeadSEO();

            }
        }
    });
}

function listarParceiros() {

    $.ajax({

        url: "/dashboard/listagens/listarParceiros",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#seo_parceiros1').val(dados.seo_parceiros1);
            $('#seo_parceiros2').val(dados.seo_parceiros2);
            $('#seo_parceiros3').val(dados.seo_parceiros3);
            $('#seo_parceiros4').val(dados.seo_parceiros4);
            $('#seo_parceiros5').val(dados.seo_parceiros5);
            $('#seo_parceiros6').val(dados.seo_parceiros6);
            $('#seo_parceiros7').val(dados.seo_parceiros7);
            $('#seo_parceiros8').val(dados.seo_parceiros8);
            $('#seo_parceiros9').val(dados.seo_parceiros9);
            $('#seo_parceiros10').val(dados.seo_parceiros10);
            $('#seo_parceiros11').val(dados.seo_parceiros11);
            $('#seo_parceiros12').val(dados.seo_parceiros12);

            if (dados.ativoGeral_parceiros === '1') { $('#ativoGeral_parceiro').val(1).attr("checked", "checked"); } else { $('#ativoGeral_parceiro').val(1); }

            if (dados.ativo1_parceiros === '1') { $('#ativo_parceiro1').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro1').val(1); }
            $('#imagemParceiro1').attr("src", "/assets/site/parceiros/" + dados.imagem1_parceiros);

            if (dados.ativo2_parceiros === '1') { $('#ativo_parceiro2').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro2').val(1); }
            $('#imagemParceiro2').attr("src", "/assets/site/parceiros/" + dados.imagem2_parceiros);

            if (dados.ativo3_parceiros === '1') { $('#ativo_parceiro3').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro3').val(1); }
            $('#imagemParceiro3').attr("src", "/assets/site/parceiros/" + dados.imagem3_parceiros);

            if (dados.ativo4_parceiros === '1') { $('#ativo_parceiro4').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro4').val(1); }
            $('#imagemParceiro4').attr("src", "/assets/site/parceiros/" + dados.imagem4_parceiros);

            if (dados.ativo5_parceiros === '1') { $('#ativo_parceiro5').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro5').val(1); }
            $('#imagemParceiro5').attr("src", "/assets/site/parceiros/" + dados.imagem5_parceiros);

            if (dados.ativo6_parceiros === '1') { $('#ativo_parceiro6').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro6').val(1); }
            $('#imagemParceiro6').attr("src", "/assets/site/parceiros/" + dados.imagem6_parceiros);

            if (dados.ativo7_parceiros === '1') { $('#ativo_parceiro7').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro7').val(1); }
            $('#imagemParceiro7').attr("src", "/assets/site/parceiros/" + dados.imagem7_parceiros);

            if (dados.ativo8_parceiros === '1') { $('#ativo_parceiro8').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro8').val(1); }
            $('#imagemParceiro8').attr("src", "/assets/site/parceiros/" + dados.imagem8_parceiros);

            if (dados.ativo9_parceiros === '1') { $('#ativo_parceiro9').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro9').val(1); }
            $('#imagemParceiro9').attr("src", "/assets/site/parceiros/" + dados.imagem9_parceiros);

            if (dados.ativo10_parceiros === '1') { $('#ativo_parceiro10').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro10').val(1); }
            $('#imagemParceiro10').attr("src", "/assets/site/parceiros/" + dados.imagem10_parceiros);

            if (dados.ativo11_parceiros === '1') { $('#ativo_parceiro11').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro11').val(1); }
            $('#imagemParceiro11').attr("src", "/assets/site/parceiros/" + dados.imagem11_parceiros);

            if (dados.ativo12_parceiros === '1') { $('#ativo_parceiro12').val(1).attr("checked", "checked"); } else { $('#ativo_parceiro12').val(1); }
            $('#imagemParceiro12').attr("src", "/assets/site/parceiros/" + dados.imagem12_parceiros);

        }
    });
}

$('#formAtualizarParceiros').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarParceiros(self);
});

function atualizarParceiros(dados) {

    var form = $('#formAtualizarParceiros')[0];

    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarParceiros",
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 400000,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#erroMsg').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarParceiros();

            } else {

                $('#erroMsg').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                $(document).ready(function() { $('imagem_parceiro1').val(""); });
                $(document).ready(function() { $('imagem_parceiro2').val(""); });
                $(document).ready(function() { $('imagem_parceiro3').val(""); });
                $(document).ready(function() { $('imagem_parceiro4').val(""); });


                listarParceiros();

            }
        }
    });
}