$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarModelosTemplateEmails();
listarConfigsTemplateEmails();

$('#cadastrarNovoModeloTemplateEmail').submit(function(e) {

    e.preventDefault();

    var self = $(this);

    // alert(self.serialize());

    var retorno = cadastrarNovoModelo(self);

});

function cadastrarNovoModelo(dados) {

    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/cadastros/cadastrarNovoModeloTemplateEmail",

        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarModelosTemplateEmails()

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                $('#cadastrarNovoModeloTemplateEmail').each(function() {
                    this.reset();
                });

                listarModelosTemplateEmails()

            }
        }
    });
}

function listarModelosTemplateEmails() {

    $.ajax({

        url: "/listagens/listarModelosTemplateEmails",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#tabelaListarModelosTemplateEmails').html('');

            if (dados.temp.length > 0) {

                dadosGlobais = dados;

                for (var i = 0; i < dados.temp.length; i++) {

                    if (dados.temp[i].status_templateEmail === '1') {

                        var botaoStatus = '<button type="submit" onclick="javascript:atualizarStatusModeloTemplate(' + i + ');" class="btn btn-xs btn-block btn-purple waves-effect waves-light">Bloquear</button>';

                    } else {

                        var botaoStatus = '<button type="submit" onclick="javascript:atualizarStatusModeloTemplate(' + i + ');" class="btn btn-xs btn-block btn-danger waves-effect waves-light">Desbloquear</button>';

                    }


                    $('#tabelaListarModelosTemplateEmails').append(

                        '<tr>' +
                        '<th>' + dados.temp[i].id_templateEmail + '</th>' +
                        '<td>' + dados.temp[i].nome_templateEmail + '</td>' +

                        '<td>' + botaoStatus + '</td>' +

                        '<td>' +
                        '<button type="button" onclick="javascript:AbrirModalVisualizarModelo(' + i + ');" class="btn btn-xs btn-block btn-warning waves-effect waves-light mr-2">' +
                        '<i class="fe-file-text"></i>' +
                        '</button>' +
                        '</td>' +


                        '<td>' +

                        '<button type="button" onclick="javascript:AbrirModalEditarModeloTemplateEmail(' + i + ');" class="btn btn-xs btn-primary waves-effect waves-light mr-2">' +
                        '<i class="mdi mdi-pencil-outline"></i>' +
                        '</button>' +

                        '<button type="button" onclick="javascript:AbrirModalExcluirModeloTemplateEmail(' + i + ');" class="btn btn-xs bg-soft-red waves-effect waves-light mr-1">' +
                        '<i class="mdi mdi-trash-can"></i>' +
                        '</button>' +
                        '</td>' +

                        '</tr>'

                    );
                }

            } else {

                $('#tabelaListarModelosTemplateEmails').append(
                    '<tr>' +
                    '<td scope="row" colspan="4" class="text-center col-md-1">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Sem modelos criados!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>' +
                    '</tr>'
                );

            }
        }
    });
}

function atualizarStatusModeloTemplate(dados) {

    $.ajax({
        type: "POST",

        data: {
            idAtualizar: dadosGlobais.temp[dados].id_templateEmail
        },

        url: "/dashboard/atualizar/atualizarStatusModeloTemplate",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarModelosTemplateEmails();

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarModelosTemplateEmails();

            }
        }
    });
}

function AbrirModalVisualizarModelo(visualizarModelo) {

    showModalVisualizarModelo(visualizarModelo);

}

function showModalVisualizarModelo(visualizarModelo) {

    $('#modalVerModeloTemplate').modal('show');

    $("#nomeModeloTemplateVisualizar").html("#" + dadosGlobais.temp[visualizarModelo].id_templateEmail + " - " + dadosGlobais.temp[visualizarModelo].nome_templateEmail);
    $("#htmlTemplate").html(dadosGlobais.temp[visualizarModelo].html_templateEmail);

    $('#linkImagemLogotipo').attr('src', dadosGlobais.dadosTemp[0].linkLogotipo_configs_template);
    $('#linkImagemLogotipo2').attr('src', dadosGlobais.dadosTemp[0].linkLogotipo_configs_template);
    $('#linkImagem').attr('href', dadosGlobais.dadosTemp[0].linkImagem_configs_template);
    $('#linkImagem2').attr('href', dadosGlobais.dadosTemp[0].linkImagem_configs_template);
    $('#linkWhatsApp').attr('href', dadosGlobais.dadosTemp[0].linkWhatsApp_configs_template);
    $('#linkInstagram').attr('href', dadosGlobais.dadosTemp[0].linkInstagram_configs_template);
    $('#linkFacebook').attr('href', dadosGlobais.dadosTemp[0].linkFacebook_configs_template);

    $('#linkWhatsApp2').attr('href', dadosGlobais.dadosTemp[0].linkWhatsApp_configs_template);
    $('#linkInstagram2').attr('href', dadosGlobais.dadosTemp[0].linkInstagram_configs_template);
    $('#linkFacebook2').attr('href', dadosGlobais.dadosTemp[0].linkFacebook_configs_template);

    $("#empresaNome").html(dadosGlobais.dadosTemp[0].nomeEmpresa_configs_template);
    $("#endereco").html(dadosGlobais.dadosTemp[0].endereco_configs_template);

    $('#linkDominioRed').attr('href', dadosGlobais.dadosTemp[0].linkDominio_configs_template);
    $("#dominio").html(dadosGlobais.dadosTemp[0].dominio_configs_template);







}

function AbrirModalEditarModeloTemplateEmail(EditarModelo) {

    showModalEditarModeloTemplateEmail(EditarModelo);

}

function showModalEditarModeloTemplateEmail(EditarModelo) {

    $('#alterarInformacoesModeloTemplateEmail').modal('show');

    $("#idModeloTemplateEmailAlterar").val(dadosGlobais.temp[EditarModelo].id_templateEmail);
    $("#alterarNomeModeloTemplateEmail").html(dadosGlobais.temp[EditarModelo].nome_templateEmail);
    $("#alterarNomeModeloTemplate").val(dadosGlobais.temp[EditarModelo].nome_templateEmail);
    $("#htmlEditarTemplateModelo").val(dadosGlobais.temp[EditarModelo].html_templateEmail);

}

$('#formAlterarModeloTemplateEmail').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = alterarInformacoesModeloTemplateEmail(self);

});

function alterarInformacoesModeloTemplateEmail(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarInfosModeloTemplate",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroAlterarModelotemplate').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                $('#formAlterarModeloTemplateEmail').each(function() {
                    this.reset();
                });

                $('#alterarInformacoesModeloTemplateEmail').modal('hide');

                listarModelosTemplateEmails();

            }
        }
    });
}

function AbrirModalExcluirModeloTemplateEmail(ExcluirModelo) {

    showModalExcluirModeloTemplateEmail(ExcluirModelo);

}

function showModalExcluirModeloTemplateEmail(ExcluirModelo) {

    $('#ModalExcluirModeloTemplate').modal('show');

    $("#tituloModeloExcluir").html(dadosGlobais.temp[ExcluirModelo].nome_templateEmail);
    $("#idModeloExcluir").val(dadosGlobais.temp[ExcluirModelo].id_templateEmail);
    $("#excluirNomeModelo").html(dadosGlobais.temp[ExcluirModelo].nome_templateEmail);

}

$('#formExcluirModelo').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = ExcluirModeloTemplateEmail(self);

});

function ExcluirModeloTemplateEmail(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/exclusoes/excluirModeloTemplateEmail",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemERROExcluirModelo').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                $('#formExcluirModelo').each(function() {
                    this.reset();
                });

                $('#ModalExcluirModeloTemplate').modal('hide');

                listarModelosTemplateEmails();

            }
        }
    });
}

function listarConfigsTemplateEmails() {

    $.ajax({

        url: "/listagens/listarConfigsTemplateEmails",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#linkImagemLogo').val(dados.linkLogotipo_configs_template);
            $('#linkLogotipo').val(dados.linkImagem_configs_template);
            $('#nomeEmpresa').val(dados.nomeEmpresa_configs_template);
            $('#enderecoEmpresa').val(dados.endereco_configs_template);
            $('#dominioSite').val(dados.dominio_configs_template);
            $('#linkDominio').val(dados.linkDominio_configs_template);
            $('#linkFacebook').val(dados.linkFacebook_configs_template);
            $('#linkInstagram').val(dados.linkInstagram_configs_template);
            $('#linkWhatsApp').val(dados.linkWhatsApp_configs_template);

        }
    });
}

$('#formAtualizarConfigsTempEmail').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarDadosConfigsTempEmail(self);

});

function atualizarDadosConfigsTempEmail(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarDadosConfigsTempEmail",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarModelosTemplateEmails();
                listarConfigsTemplateEmails();

            }
        }
    });
}