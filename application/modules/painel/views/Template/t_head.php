<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>

<html lang="pt-br">

    <head>
        <meta charset="utf-8" />

        <title><?=$titulo?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Newsletter, Disparo de Newsletter" name="description" />
        <meta content="SolidSystem," name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <link rel="shortcut icon" href="<?=base_url('assets/painel/images/favicon.png')?>">
        
        <?=$scriptsCSSacima?>

        <link href="<?=base_url('assets/painel/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/painel/css/icons.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?=base_url('assets/painel/css/app.min.css')?>" rel="stylesheet" type="text/css" />

        <?=$scriptsCSS?>

    </head>

    <body>
        
        <div id="wrapper">