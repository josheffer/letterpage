-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 192.168.10.10    Database: letterpage
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configs_landing_pages`
--

DROP TABLE IF EXISTS `configs_landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `configs_landing_pages` (
  `id_configs` int(11) NOT NULL,
  `fone1_landingPage` varchar(45) DEFAULT NULL,
  `fone2_landingPage` varchar(45) DEFAULT NULL,
  `fone1TextoWhats_landingPage` varchar(256) DEFAULT NULL,
  `fone2TextoWhats_landingPage` varchar(256) DEFAULT NULL,
  `verificaWhatsapp1_landingPage` enum('0','1') DEFAULT '1',
  `verificaWhatsapp2_landingPage` enum('0','1') DEFAULT '1',
  `linkFacebook_landingPage` varchar(60) DEFAULT NULL,
  `linkInstagram_landingPage` varchar(60) DEFAULT NULL,
  `linkSite_landingPage` varchar(60) DEFAULT NULL,
  `linkAlternativo1_landingPage` varchar(60) DEFAULT NULL,
  `linkAlternativo2_landingPage` varchar(60) DEFAULT NULL,
  `email_landingPage` varchar(45) DEFAULT NULL,
  `endereco_landingPage` varchar(145) DEFAULT NULL,
  `logo_landingPage` varchar(45) DEFAULT NULL,
  `favicon_landingPage` varchar(45) DEFAULT NULL,
  `googleMaps_landingPage` text,
  PRIMARY KEY (`id_configs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configs_landing_pages`
--

LOCK TABLES `configs_landing_pages` WRITE;
/*!40000 ALTER TABLE `configs_landing_pages` DISABLE KEYS */;
INSERT INTO `configs_landing_pages` VALUES (1,'62993998725','62993998725','Olá, você está falando com o Vendedor 1','Olá, você está falando com o Vendedor 2','1','1','teste','teste','teste.com','','','teste@teste.com','endereco','img_logo_1824337042.jpg','img_favicon_33273439.jpg','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61152.03025844469!2d-49.34097843224888!3d-16.676788807968148!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x16d0acbabdf231e5!2sSupermercado%20Hora%20Extra!5e0!3m2!1spt-BR!2sbr!4v1586979740914!5m2!1spt-BR!2sbr\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>');
/*!40000 ALTER TABLE `configs_landing_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landing_pages`
--

DROP TABLE IF EXISTS `landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `landing_pages` (
  `id_landingPage` int(11) NOT NULL AUTO_INCREMENT,
  `nome_landingPage` varchar(145) NOT NULL COMMENT 'Nome para identificação da landing page',
  `titulo_landingPage` varchar(512) NOT NULL COMMENT 'Texto que aparecerá na aba do browser ao abrir a página',
  `status_landingPage` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'Status da Landing Page, 0 = desativada, 1 ativada',
  `template_landingPage` int(11) NOT NULL DEFAULT '1' COMMENT 'Template da landing page, por padrão iniciará com o template 1 (template padrão)',
  `link_landingPage` varchar(512) NOT NULL,
  `grupoLista_landingPage` varchar(45) NOT NULL,
  PRIMARY KEY (`id_landingPage`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landing_pages`
--

LOCK TABLES `landing_pages` WRITE;
/*!40000 ALTER TABLE `landing_pages` DISABLE KEYS */;
INSERT INTO `landing_pages` VALUES (1,'Campanha 1','campanha numero 1','1',1,'http://letterpage.net.br/landingpages/1/campanha-numero-1','1'),(2,'asasd','asdsad','1',1,'http://letterpage.net.br/landingpages/2/asdsad','5'),(3,'asdasdas','dasdasd','1',1,'http://letterpage.net.br/landingpages/3/dasdasd','1');
/*!40000 ALTER TABLE `landing_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_cadastros`
--

DROP TABLE IF EXISTS `leads_cadastros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `leads_cadastros` (
  `id_cadastros` int(11) NOT NULL,
  `nome_cadastros` varchar(145) DEFAULT NULL,
  `telefone_cadastros` varchar(45) DEFAULT NULL,
  `email_cadastros` varchar(90) DEFAULT NULL,
  `ondeNosConheceu_cadastros` varchar(45) DEFAULT NULL,
  `landingPage_cadastros` int(11) DEFAULT NULL,
  `grupoLista_cadastros` int(11) DEFAULT NULL,
  `cidade_cadastros` varchar(45) DEFAULT NULL,
  `estado_cadastros` varchar(45) DEFAULT NULL,
  `data_cadastros` varchar(9) DEFAULT NULL,
  `hora_cadastros` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_cadastros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_cadastros`
--

LOCK TABLES `leads_cadastros` WRITE;
/*!40000 ALTER TABLE `leads_cadastros` DISABLE KEYS */;
INSERT INTO `leads_cadastros` VALUES (1,'dsdfsd','+5542342342342','dfsdfsds@gmail.com','Facebook',1,1,'Aparecida de Goiania','Goias','20200415','1722'),(2,'Josheffer Robert','+5562993998725','josheffer@gmail.com','Facebook',1,1,'Aparecida de Goiania','Goias','20200415','1732'),(3,'asdas','+5512331231231','asdasd@gmail.com','Instagram',1,1,'Aparecida de Goiania','Goias','20200429','1718'),(4,'Kreme','+5562993998725','kremegay@gmail.com','Outros',1,1,'Aparecida de Goiania','Goias','20200514','1122'),(5,'dasfs','+5512341231231','2asdas@gmail.com','Amigos',1,1,'Aparecida de Goiania','Goias','20200514','1659'),(6,'uoeiuweiou','+5512312312312','qwaskdljaslkj@gmail.com','Instagram',1,1,'Aparecida de Goiania','Goias','20200528','1448'),(7,'rtterter','+5523423423423','rdsfsdf@gmail.com','WhatsApp',1,1,'Aparecida de Goiania','Goias','20200528','1449'),(8,'aaaaaaaaa','+5522222222222','aaaaaa@gmail.com','Instagram',1,1,'Aparecida de Goiania','Goias','20200528','1459'),(9,'wwww','+5512123123123','3123123@gmail.com','WhatsApp',1,1,'Goiania','Goias','20200528','1513'),(10,'sssssss','+552222','ssss@gmail.com','Google',1,1,'Aparecida de Goiania','Goias','20200528','1514');
/*!40000 ALTER TABLE `leads_cadastros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_landing_pages_clicks`
--

DROP TABLE IF EXISTS `lp_landing_pages_clicks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_landing_pages_clicks` (
  `id_clicks` int(11) NOT NULL,
  `idLandingPage_clicks` int(11) DEFAULT NULL,
  `token_clicks` varchar(90) DEFAULT NULL,
  `nomeBotao_clicks` varchar(45) DEFAULT NULL,
  `linkBotao_clicks` varchar(145) DEFAULT NULL,
  `dataClick__clicks` varchar(45) DEFAULT NULL,
  `horaClick__clicks` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_clicks`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_landing_pages_clicks`
--

LOCK TABLES `lp_landing_pages_clicks` WRITE;
/*!40000 ALTER TABLE `lp_landing_pages_clicks` DISABLE KEYS */;
INSERT INTO `lp_landing_pages_clicks` VALUES (1,1,'cggjl03gb5t6ng9npj15bjqp99q8pqv4','botaoLateralWhatsApp1','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%202','20200415','1735'),(2,1,'cggjl03gb5t6ng9npj15bjqp99q8pqv4','botaoLateralWhatsApp2','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%201','20200415','1736'),(3,1,'cggjl03gb5t6ng9npj15bjqp99q8pqv4','botaoRodapeWhatsApp1','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%201','20200415','1737'),(4,1,'0i71ph9vkgm4d0vfsibj9g0qmrrl4for','botaoRodapeWhatsApp1','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%201','20200514','1145'),(5,1,'0i71ph9vkgm4d0vfsibj9g0qmrrl4for','botaoRodapeWhatsApp2','https://api.whatsapp.com/send?phone=5562993998725&text=Ol%C3%A1,%20voc%C3%AA%20est%C3%A1%20falando%20com%20o%20Vendedor%202','20200514','1146'),(6,1,'5padk5kmbaeor7pn51dm6k7nkrvcgrkt','botaoRodapeFacebook','https://www.facebook.com/teste','20200528','1548');
/*!40000 ALTER TABLE `lp_landing_pages_clicks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_landing_pages_contador`
--

DROP TABLE IF EXISTS `lp_landing_pages_contador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_landing_pages_contador` (
  `id_contador` int(11) NOT NULL AUTO_INCREMENT,
  `ip_contador` varchar(45) DEFAULT NULL,
  `cidade_contador` varchar(65) DEFAULT NULL,
  `estado_contador` varchar(45) DEFAULT NULL,
  `pais_contador` varchar(45) DEFAULT NULL,
  `dispositivo_contador` varchar(45) DEFAULT NULL,
  `navegador_contador` varchar(45) DEFAULT NULL,
  `SO_contador` varchar(45) DEFAULT NULL,
  `landingPage_contador` int(11) DEFAULT NULL,
  `data_contador` varchar(45) DEFAULT NULL,
  `hora_contador` varchar(45) DEFAULT NULL,
  `tempo_contador` varchar(45) DEFAULT NULL,
  `token_contador` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id_contador`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_landing_pages_contador`
--

LOCK TABLES `lp_landing_pages_contador` WRITE;
/*!40000 ALTER TABLE `lp_landing_pages_contador` DISABLE KEYS */;
INSERT INTO `lp_landing_pages_contador` VALUES (47,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1647','590','2rbq28cj769js7ikkkl0s5na3oud02e1'),(48,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1657','170','6qk9rq4kvfsths254gaqt7p20nl34otu'),(49,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1700','302','pu0tv75j4k0uqc4e8ongtnpk09l0a9cs'),(50,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1705','406','62lr0soe23vo0fusj7obuugo8kq11f2r'),(51,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1712','329','pdgp1c2hrvfmpchf6o0tv5kecfn2mc6g'),(52,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1718','257','10u7g0tc6r2m5qdsjme4don3o63lvcqo'),(53,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1727',NULL,'8io4noajk9hcf91o03aoc7dd9ltiuaoi'),(54,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1731','373','cggjl03gb5t6ng9npj15bjqp99q8pqv4'),(55,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1738','239','r7uvn760rhvb89mo5st5n6e6cla04hqu'),(56,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1742','208','iv40onrq9f5tuq079u762eam6kjl70eb'),(57,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200415','1746',NULL,'utflb06iulk2upd0ai4l9p2p1bfkivl4'),(58,'177.30.111.114','Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200429','1707','638','5iuih44ddrs7ptr4qij172v0oq4vd587'),(59,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200429','1718','7','0r787dbrs9pca0u02tbk26c2740vr9j1'),(60,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200429','1724',NULL,'gaqejsu3u5alaak8lhc2ikmp4g8q2lo3'),(61,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200514','1119','115','gn2boakv9hac5vd7bjj3b3mj84gemumb'),(62,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200514','1145','178','0i71ph9vkgm4d0vfsibj9g0qmrrl4for'),(63,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200514','1148','42','dh3ov9e0e00ie2gft6nemn677k43a1ia'),(64,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200514','1659','9','bgsci59jivv2hml4lp8h9e5v9r13t73i'),(65,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200515','1505',NULL,'bvjq71sej6sq7o8mvads4jsipqg0gpsb'),(66,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200522','1618','2','mk684ssql3bqtjcl3odll3b85puhmvda'),(67,'177.30.111.114','Goiania','Goias','Brazil','0','Opera','Windows',2,'20200526','1334','6','u25f4ck084bdo6f8cu0l6pli675mao9u'),(68,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200527','1100','4','h6f3gbg3pjfrbn88v6ta7o8e4sekb90u'),(69,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',3,'20200527','1105','3','3ceu504nfkml4m19hm0h5crsgu0cp5ja'),(70,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200527','1115',NULL,'rk1p4tq48vfof6avm890cmpb87s3vqg9'),(71,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',3,'20200527','1115','6','rk1p4tq48vfof6avm890cmpb87s3vqg9'),(72,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',2,'20200527','1115','3','rk1p4tq48vfof6avm890cmpb87s3vqg9'),(73,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200528','1447','668','9s3a4lmkqqud0t6gvnalqgtg6djiuk1m'),(74,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200528','1513','48','mjmps2l2aie6nqs37ta10e2lqvlmudds'),(75,'177.30.111.114','Aparecida de Goiania','Goias','Brazil','0','Chrome','Windows',1,'20200528','1548','6321','5padk5kmbaeor7pn51dm6k7nkrvcgrkt');
/*!40000 ALTER TABLE `lp_landing_pages_contador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modeloconfig_head`
--

DROP TABLE IF EXISTS `lp_modeloconfig_head`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modeloconfig_head` (
  `id_modelo` int(11) DEFAULT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `topo_head_css` text,
  `topo_head_js` text,
  `topo_head_SEO_keywords` text,
  `topo_head_SEO_description` text,
  `topo_head_SEO_description2` text,
  `topo_head_SEO_description3` text,
  `topo_head_SEO_imagem` varchar(145) DEFAULT NULL,
  `topo_head_coordenadasMaps` varchar(80) DEFAULT NULL,
  `topo_head_cidade` varchar(80) DEFAULT NULL,
  `topo_head_estado` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modeloconfig_head`
--

LOCK TABLES `lp_modeloconfig_head` WRITE;
/*!40000 ALTER TABLE `lp_modeloconfig_head` DISABLE KEYS */;
INSERT INTO `lp_modeloconfig_head` VALUES (1,1,'','','teste','teste de landingpage','teste de landingpage','teste de landingpage','img_SEO_1190214192.jpg','-49.34097843224888!3d-16.676788807968148','Goiânia, Goiás','GO-BR'),(1,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(1,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `lp_modeloconfig_head` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modeloconfig_logomenuslide`
--

DROP TABLE IF EXISTS `lp_modeloconfig_logomenuslide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modeloconfig_logomenuslide` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_logo1` enum('0','1') DEFAULT '1',
  `body_logo2` enum('0','1') DEFAULT '1',
  `body_imagemPrincipalSlide` enum('0','1') DEFAULT '1',
  `body_imagemFundoSlide` enum('0','1') DEFAULT '1',
  `body_menu_topo` enum('0','1') DEFAULT '1',
  `body_slide1` enum('0','1') DEFAULT '1',
  `body_slide` enum('0','1') DEFAULT '1',
  `body_slide_titulo1` varchar(145) DEFAULT 'Titulo',
  `body_slide_titulo2` varchar(145) DEFAULT 'Titulo',
  `body_slide_titulo3` varchar(145) DEFAULT 'Titulo',
  `body_slide_descricao1` varchar(256) DEFAULT 'Descrição',
  `body_slide_descricao2` varchar(256) DEFAULT 'Descrição',
  `body_slide_descricao3` varchar(256) DEFAULT 'Descrição',
  `body_slide_imagemFundoSlide` varchar(256) DEFAULT 'banner-bg.png',
  `body_slide_imagem1` varchar(256) DEFAULT 'hero-banner.png',
  `body_slide_imagem2` varchar(256) DEFAULT 'hero-banner.png',
  `body_slide_imagem3` varchar(256) DEFAULT 'hero-banner.png',
  `body_slide_imagem1_SEO` varchar(256) DEFAULT NULL,
  `body_slide_imagem2_SEO` varchar(256) DEFAULT NULL,
  `body_slide_imagem3_SEO` varchar(256) DEFAULT NULL,
  `body_slide_botao1` enum('0','1') DEFAULT '1',
  `body_slide_botao2` enum('0','1') DEFAULT '1',
  `body_slide_botao3` enum('0','1') DEFAULT '1',
  `body_slide_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_slide_botaoLink1` varchar(145) DEFAULT 'www.google.com.br',
  `body_slide_botaoLink2` varchar(145) DEFAULT 'www.google.com.br',
  `body_slide_botaoLink3` varchar(145) DEFAULT 'www.google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modeloconfig_logomenuslide`
--

LOCK TABLES `lp_modeloconfig_logomenuslide` WRITE;
/*!40000 ALTER TABLE `lp_modeloconfig_logomenuslide` DISABLE KEYS */;
INSERT INTO `lp_modeloconfig_logomenuslide` VALUES (1,1,'1','1','1','1','1','1','1','Teste banner 2','Titulo','Titulo','teste banner 2','Descrição','Descrição','banner-bg.png','hero-banner.png','hero-banner.png','hero-banner.png',NULL,'',NULL,'','1','1','Ver +','Ver +','Ver +','www.google.com.br','www.google.com.br','www.google.com.br'),(1,2,'1','1','1','1','1','1','1','Titulo','Titulo','Titulo','Descrição','Descrição','Descrição','banner-bg.png','hero-banner.png','hero-banner.png','hero-banner.png',NULL,NULL,NULL,'1','1','1','Ver +','Ver +','Ver +','www.google.com.br','www.google.com.br','www.google.com.br'),(1,3,'1','1','1','1','1','1','1','Titulo','Titulo','Titulo','Descrição','Descrição','Descrição','banner-bg.png','hero-banner.png','hero-banner.png','hero-banner.png',NULL,NULL,NULL,'1','1','1','Ver +','Ver +','Ver +','www.google.com.br','www.google.com.br','www.google.com.br');
/*!40000 ALTER TABLE `lp_modeloconfig_logomenuslide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_forms`
--

DROP TABLE IF EXISTS `lp_modelosconfig_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_forms` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico1_form1` enum('0','1') DEFAULT '1',
  `body_servico1_form1_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico1_form1_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico1_form1_corFundoForm` varchar(10) DEFAULT '#000000',
  `body_servico1_form1_corTextBotao` varchar(10) DEFAULT '#000000',
  `body_servico1_form2` enum('0','1') DEFAULT '1',
  `body_servico1_form2_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico1_form2_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico1_form2_corFundoForm` varchar(10) DEFAULT '#000000',
  `body_servico1_form2_corTextBotao` varchar(10) DEFAULT '#000000',
  `body_servico2_form3` enum('0','1') DEFAULT '1',
  `body_servico2_form3_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico2_form3_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico2_form3_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico2_form3_corTextBotao` varchar(10) DEFAULT '#000000',
  `body_servico3_form4` enum('0','1') DEFAULT '1',
  `body_servico3_form4_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico3_form4_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico3_form4_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico3_form4_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico4_form5` enum('0','1') DEFAULT '1',
  `body_servico4_form5_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico4_form5_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico4_form5_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico4_form5_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico5_form6` enum('0','1') DEFAULT '1',
  `body_servico5_form6_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico5_form6_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico5_form6_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico5_form6_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico6_form7` enum('0','1') DEFAULT '1',
  `body_servico6_form7_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico6_form7_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico6_form7_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico6_form7_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico7_form8` enum('0','1') DEFAULT '1',
  `body_servico7_form8_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico7_form8_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico7_form8_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico7_form8_corTextBotao` varchar(45) DEFAULT '#000000',
  `body_servico_RedesSociais_form` enum('0','1') DEFAULT '1',
  `body_servico_RedesSociais_form_corFundoBotao` varchar(10) DEFAULT '#ffffff',
  `body_servico_RedesSociais_form_textBotao` varchar(45) DEFAULT 'Cadastrar',
  `body_servico_RedesSociais_form_corFundoForm` varchar(45) DEFAULT '#ffffff',
  `body_servico_RedesSociais_form_corTextBotao` varchar(45) DEFAULT '#000000',
  `nosConheceuOpcao1` varchar(45) DEFAULT 'Facebook',
  `nosConheceuOpcao2` varchar(45) DEFAULT 'Instagram',
  `nosConheceuOpcao3` varchar(45) DEFAULT 'WhatsApp',
  `nosConheceuOpcao4` varchar(45) DEFAULT 'Google',
  `nosConheceuOpcao5` varchar(45) DEFAULT 'YouTube',
  `nosConheceuOpcao6` varchar(45) DEFAULT 'Messenger',
  `nosConheceuOpcao7` varchar(45) DEFAULT 'Amigos',
  `nosConheceuOpcao8` varchar(45) DEFAULT 'Outros',
  `nosConheceuOpcao1_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao2_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao3_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao4_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao5_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao6_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao7_visivel` enum('0','1') DEFAULT '1',
  `nosConheceuOpcao8_visivel` enum('0','1') DEFAULT '1',
  `body_servicoForm_tituloForm1` varchar(128) DEFAULT 'Titulo 1',
  `body_servicoForm_descricaoForm1` varchar(512) DEFAULT 'Descrição 1',
  `body_servicoForm_tituloForm2` varchar(128) DEFAULT 'Titulo 2',
  `body_servicoForm_descricaoForm2` varchar(512) DEFAULT 'Descrição 2',
  `body_servicoForm_tituloForm3` varchar(128) DEFAULT 'Titulo 3',
  `body_servicoForm_descricaoForm3` varchar(512) DEFAULT 'Descrição 3',
  `body_servicoForm_tituloForm4` varchar(128) DEFAULT 'Titulo 4',
  `body_servicoForm_descricaoForm4` varchar(512) DEFAULT 'Descrição 4',
  `body_servicoForm_tituloForm5` varchar(128) DEFAULT 'Titulo 5',
  `body_servicoForm_descricaoForm5` varchar(512) DEFAULT 'Descrição 5',
  `body_servicoForm_tituloForm6` varchar(128) DEFAULT 'Titulo 6',
  `body_servicoForm_descricaoForm6` varchar(512) DEFAULT 'Descrição 6',
  `body_servicoForm_tituloForm7` varchar(128) DEFAULT 'Titulo 7',
  `body_servicoForm_descricaoForm7` varchar(512) DEFAULT 'Descrição 7',
  `body_servicoForm_tituloForm8` varchar(128) DEFAULT 'Titulo 8',
  `body_servicoForm_descricaoForm8` varchar(512) DEFAULT 'Descrição 8',
  `body_servicoForm_tituloFormRS` varchar(128) DEFAULT 'Titulo RS',
  `body_servicoForm_descricaoFormRS` varchar(512) DEFAULT 'Descrição RS',
  `body_servico_form1_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form1_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form2_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form2_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form3_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form3_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form4_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form4_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form5_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form5_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form6_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form6_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form7_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form7_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_form8_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_form8_corDescricao` varchar(10) DEFAULT '#ffffff',
  `body_servico_formRS_corTitulo` varchar(10) DEFAULT '#ffffff',
  `body_servico_formRS_corDescricao` varchar(10) DEFAULT '#ffffff'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_forms`
--

LOCK TABLES `lp_modelosconfig_forms` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_forms` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_forms` VALUES (1,1,'1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','1','#000000','Cadastrar','#ffffff','#ffffff','Facebook','Instagram','WhatsApp','Google','YouTube','Messenger','Amigos','Outros','1','1','1','1','1','1','1','1','Titulo 1','Descrição 1','Titulo 2','Descrição 2','Titulo 3','Descrição 3','Titulo 4','Descrição 4','Titulo 5','Descrição 5','Titulo 6','Descrição 6','Titulo 7','Descrição 7','Titulo 8','Descrição 8','Titulo RS','Descrição RS','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000'),(1,2,'1','#ffffff','Cadastrar','#000000','#000000','1','#ffffff','Cadastrar','#000000','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','Facebook','Instagram','WhatsApp','Google','YouTube','Messenger','Amigos','Outros','1','1','1','1','1','1','1','1','Titulo 1','Descrição 1','Titulo 2','Descrição 2','Titulo 3','Descrição 3','Titulo 4','Descrição 4','Titulo 5','Descrição 5','Titulo 6','Descrição 6','Titulo 7','Descrição 7','Titulo 8','Descrição 8','Titulo RS','Descrição RS','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'),(1,3,'1','#ffffff','Cadastrar','#000000','#000000','1','#ffffff','Cadastrar','#000000','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','1','#ffffff','Cadastrar','#ffffff','#000000','Facebook','Instagram','WhatsApp','Google','YouTube','Messenger','Amigos','Outros','1','1','1','1','1','1','1','1','Titulo 1','Descrição 1','Titulo 2','Descrição 2','Titulo 3','Descrição 3','Titulo 4','Descrição 4','Titulo 5','Descrição 5','Titulo 6','Descrição 6','Titulo 7','Descrição 7','Titulo 8','Descrição 8','Titulo RS','Descrição RS','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff');
/*!40000 ALTER TABLE `lp_modelosconfig_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_personalizacao`
--

DROP TABLE IF EXISTS `lp_modelosconfig_personalizacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_personalizacao` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `corFundoMenu` varchar(10) DEFAULT '#000000',
  `corFundoSlide1` varchar(10) DEFAULT '#000000',
  `corFundoSlide` varchar(10) DEFAULT '#000000',
  `corTextoTituloSlide` varchar(10) DEFAULT '#ffffff',
  `corTextoDescricaoSlide` varchar(10) DEFAULT '#ffffff',
  `corTextoBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `corFundoBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `corFundoHoverBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `corTextoHoverBotaoSlide` varchar(10) DEFAULT '#ffffff',
  `servico1_corTituloGenerico1` varchar(10) DEFAULT '#000000',
  `servico1_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico1_corBg` varchar(10) DEFAULT '#ffffff',
  `servico1_corBoxImage` varchar(10) DEFAULT '#ffffff',
  `servico1_corTexto` varchar(10) DEFAULT '#000000',
  `servico2_corTituloGenerico1` varchar(10) DEFAULT '#000000',
  `servico2_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico2_corBg` varchar(10) DEFAULT '#000000',
  `servico2_corTextoBotao` varchar(10) DEFAULT '#ffffff',
  `servico2_corTextoHoverBotao` varchar(10) DEFAULT '#000000',
  `servico2_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico2_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico3_corTituloGenerico1` varchar(10) DEFAULT '#000000',
  `servico3_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico3_corBg` varchar(10) DEFAULT '#000000',
  `servico3_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico3_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico4_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico4_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico4_corBg` varchar(10) DEFAULT '#000000',
  `servico4_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico4_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico4_corTextoBotao` varchar(10) DEFAULT '#ffffff',
  `servico4_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico4_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico4_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico5_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico5_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico5_corBg` varchar(10) DEFAULT '#000000',
  `servico5_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico5_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico5_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico5_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico5_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico5_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico6_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico6_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico6_corBg` varchar(10) DEFAULT '#000000',
  `servico6_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico6_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico6_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico6_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico6_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico6_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico7_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico7_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico7_corBg` varchar(10) DEFAULT '#000000',
  `servico7_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico7_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico7_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico7_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico7_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico7_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico_redesSociais_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico_redesSociais_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corBg` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corBoxImage` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corTexto` varchar(10) DEFAULT '#ffffff',
  `servico_redesSociais_corTextoBotao` varchar(45) DEFAULT '#ffffff',
  `servico_redesSociais_corTextoHoverBotao` varchar(45) DEFAULT '#000000',
  `servico_redesSociais_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico_redesSociais_corFundoHoverBotao` varchar(10) DEFAULT '#ffffff',
  `servico_maps_corTituloGenerico1` varchar(45) DEFAULT '#000000',
  `servico_maps_corDescricaoGenerico1` varchar(10) DEFAULT '#000000',
  `servico_maps_corBg` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoHoverBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoTitulo` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corTextoDescricao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corFundoBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corFundoHoverBotao` varchar(10) DEFAULT '#000000',
  `servico_paginaSucesso_corBG` varchar(10) DEFAULT '#000000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_personalizacao`
--

LOCK TABLES `lp_modelosconfig_personalizacao` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_personalizacao` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_personalizacao` VALUES (1,1,'#68ec06','#f8b981','#000000','#ffff00','#ffff00','#000000','#ffffff','#3c3c3c','#a6a6a6','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#aeaeae','#ffffff','#3a3a3a','#ffffff','#ffffff','#000000','#202020','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#999999','#ffffff','#2c2c2c','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#b5b5b5','#ffffff','#2d2d2d','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#ffffff','#ffffff','#585858','#d7151b'),(1,2,'#000000','#000000','#000000','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000'),(1,3,'#000000','#000000','#000000','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#ffffff','#ffffff','#000000','#000000','#ffffff','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000','#000000');
/*!40000 ALTER TABLE `lp_modelosconfig_personalizacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_redessociais`
--

DROP TABLE IF EXISTS `lp_modelosconfig_redessociais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_redessociais` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_redesSociais` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico_redesSociais` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo_redesSociais` varchar(45) DEFAULT 'Titulo serviço Redes Sociais',
  `body_textoGenericoDescricao_redesSociais` varchar(512) DEFAULT 'Descrição serviço Redes Sociais',
  `body_redesSociais_opcao1` enum('0','1') DEFAULT '1',
  `body_redesSociais_opcao2` enum('0','1') DEFAULT '1',
  `body_redesSociais_opcao3` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico_redesSociais_Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico_redesSociais_Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico_redesSociais_Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico_redesSociais_Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico_redesSociais_Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico_redesSociais_Descricao1` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico_redesSociais_Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico_redesSociais_Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico_redesSociais_ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico_redesSociais_ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico_redesSociais_ImagemSEO3` varchar(90) DEFAULT 'info SEO3',
  `body_servico_redesSociais_botao1` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botao2` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botao3` enum('0','1') DEFAULT '1',
  `body_servico_redesSociais_botaoTexto1` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoTexto2` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoTexto3` varchar(45) DEFAULT 'ver +',
  `body_servico_redesSociais_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico_redesSociais_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico_redesSociais_botaoLink3` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_redessociais`
--

LOCK TABLES `lp_modelosconfig_redessociais` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_redessociais` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_redessociais` VALUES (1,1,'1','1','Titulo serviço Redes Sociais','Descrição serviço Redes Sociais','1','1','1','img_redeSocial_182195700.jpg','img_redeSocial_364774887.jpg','img_redeSocial_686029054.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','descrição serviço 2','descrição serviço 2','descrição serviço 3','info SEO 1','info SEO 2','info SEO3','1','1','1','ver +','ver +','ver +','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço Redes Sociais','Descrição serviço Redes Sociais','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','descrição serviço 2','descrição serviço 2','descrição serviço 3','info SEO 1','info SEO 2','info SEO3','1','1','1','ver +','ver +','ver +','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço Redes Sociais','Descrição serviço Redes Sociais','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','descrição serviço 2','descrição serviço 2','descrição serviço 3','info SEO 1','info SEO 2','info SEO3','1','1','1','ver +','ver +','ver +','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_redessociais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico1`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico1` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico1` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico1` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo1` varchar(45) DEFAULT 'Titulo serviço 1',
  `body_textoGenericoDescricao1` varchar(512) DEFAULT 'Descrição serviço 1',
  `body_servico1_opcao1` enum('0','1') DEFAULT '1',
  `body_servico1_opcao2` enum('0','1') DEFAULT '1',
  `body_servico1_opcao3` enum('0','1') DEFAULT '1',
  `body_servico1_opcao4` enum('0','1') DEFAULT '1',
  `body_servico1_opcao5` enum('0','1') DEFAULT '1',
  `body_servico1_opcao6` enum('0','1') DEFAULT '1',
  `body_servico1_opcao7` enum('0','1') DEFAULT '1',
  `body_servico1_opcao8` enum('0','1') DEFAULT '1',
  `body_servico1Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico1Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico1Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico1Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico1Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico1Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico1Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico1Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico1Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico1Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico1Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico1Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico1Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico1Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico1Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico1Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico1Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico1ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico1ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico1ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico1ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico1ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico1ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico1ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico1ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico1_botao1` enum('0','1') DEFAULT '1',
  `body_servico1_botao2` enum('0','1') DEFAULT '1',
  `body_servico1_botao3` enum('0','1') DEFAULT '1',
  `body_servico1_botao4` enum('0','1') DEFAULT '1',
  `body_servico1_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico1_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico1_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela onde será armazenado os dados da seção de serviços 1';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico1`
--

LOCK TABLES `lp_modelosconfig_servico1` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico1` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico1` VALUES (1,1,'1','1','Titulo serviço 1','Descrição serviço 1','1','1','1','1','1','1','1','1','img_servicos_712412573.jpg','img_servicos_376450579.jpg','img_servicos_1180822558.jpg','img_servicos_1446300572.jpg','img_servicos_977435545.jpg','img_servicos_1704069091.jpg','img_servicos_949986457.jpg','img_servicos_1526723470.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 1','Descrição serviço 1','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 1','Descrição serviço 1','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico2`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico2` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico2` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico2` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo2` varchar(45) DEFAULT 'Titulo serviço 2',
  `body_textoGenericoDescricao2` varchar(512) DEFAULT 'Descrição serviço 2',
  `body_servico2Imagem1` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem2` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem3` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem4` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem5` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem6` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem7` varchar(145) DEFAULT 'about.png',
  `body_servico2Imagem8` varchar(145) DEFAULT 'about.png',
  `body_servico2Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico2Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico2Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico2Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico2Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico2Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico2Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico2Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico2Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico2Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico2Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico2Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico2Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico2Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico2Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico2Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico2ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico2ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico2ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico2ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico2ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico2ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico2ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico2ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico2_botao1` enum('0','1') DEFAULT '1',
  `body_servico2_botao2` enum('0','1') DEFAULT '1',
  `body_servico2_botao3` enum('0','1') DEFAULT '1',
  `body_servico2_botao4` enum('0','1') DEFAULT '1',
  `body_servico2_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico2_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico2_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico2`
--

LOCK TABLES `lp_modelosconfig_servico2` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico2` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico2` VALUES (1,1,'1','1','Titulo serviço 2','Descrição serviço 2','img_servicos_1910013780.jpg','about.png','about.png','about.png','about.png','about.png','about.png','about.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 2','Descrição serviço 2','about.png','about.png','about.png','about.png','about.png','about.png','about.png','about.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 2','Descrição serviço 2','about.png','about.png','about.png','about.png','about.png','about.png','about.png','about.png','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico3`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico3` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico3` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico3` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo3` varchar(45) DEFAULT 'Titulo serviço 3',
  `body_textoGenericoDescricao3` varchar(512) DEFAULT 'Descrição serviço 3',
  `body_servico3_TodasOpcoes` enum('0','1') DEFAULT '1',
  `body_servico3_opcao1` enum('0','1') DEFAULT '1',
  `body_servico3_opcao2` enum('0','1') DEFAULT '1',
  `body_servico3_opcao3` enum('0','1') DEFAULT '1',
  `body_servico3_opcao4` enum('0','1') DEFAULT '1',
  `body_servico3_opcao5` enum('0','1') DEFAULT '1',
  `body_servico3_opcao6` enum('0','1') DEFAULT '1',
  `body_servico3_opcao7` enum('0','1') DEFAULT '1',
  `body_servico3_opcao8` enum('0','1') DEFAULT '1',
  `body_servico3Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico3Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico3Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico3Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico3Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico3Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico3Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico3Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico3Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico3Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico3Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico3Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico3Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico3Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico3Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico3Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico3Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico3ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico3ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico3ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico3ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico3ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico3ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico3ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico3ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico3_botao1` enum('0','1') DEFAULT '1',
  `body_servico3_botao2` enum('0','1') DEFAULT '1',
  `body_servico3_botao3` enum('0','1') DEFAULT '1',
  `body_servico3_botao4` enum('0','1') DEFAULT '1',
  `body_servico3_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico3_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico3_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico3`
--

LOCK TABLES `lp_modelosconfig_servico3` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico3` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico3` VALUES (1,1,'1','1','Titulo serviço 3','Descrição serviço 3','1','1','1','1','1','1','1','1','1','img_servicos_1941908849.jpg','img_servicos_1846368818.jpg','img_servicos_1073224146.jpg','img_servicos_2143148693.jpg','img_servicos_1144940828.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 3','Descrição serviço 3','1','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 3','Descrição serviço 3','1','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico4`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico4` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico4` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico4` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo4` varchar(45) DEFAULT 'Titulo serviço 4',
  `body_textoGenericoDescricao4` varchar(512) DEFAULT 'Descrição serviço 4',
  `body_servico4Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico4Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico4Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico4Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico4Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico4Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico4Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico4Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico4Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico4Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico4Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico4Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico4Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico4Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico4Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico4Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico4Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico4ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico4ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico4ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico4ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico4ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico4ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico4ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico4ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico4_botao1` enum('0','1') DEFAULT '1',
  `body_servico4_botao2` enum('0','1') DEFAULT '1',
  `body_servico4_botao3` enum('0','1') DEFAULT '1',
  `body_servico4_botao4` enum('0','1') DEFAULT '1',
  `body_servico4_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico4_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico4_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico4`
--

LOCK TABLES `lp_modelosconfig_servico4` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico4` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico4` VALUES (1,1,'1','1','Titulo serviço 4','Descrição serviço 4','img_servicos_24925061.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 4','Descrição serviço 4','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 4','Descrição serviço 4','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico5`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico5`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico5` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico5` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico5` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo5` varchar(45) DEFAULT 'Titulo serviço 5',
  `body_textoGenericoDescricao5` varchar(512) DEFAULT 'Descrição serviço 5',
  `body_servico5_opcao1` enum('0','1') DEFAULT '1',
  `body_servico5_opcao2` enum('0','1') DEFAULT '1',
  `body_servico5_opcao3` enum('0','1') DEFAULT '1',
  `body_servico5_opcao4` enum('0','1') DEFAULT '1',
  `body_servico5_opcao5` enum('0','1') DEFAULT '1',
  `body_servico5_opcao6` enum('0','1') DEFAULT '1',
  `body_servico5_opcao7` enum('0','1') DEFAULT '1',
  `body_servico5_opcao8` enum('0','1') DEFAULT '1',
  `body_servico5Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico5Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico5Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico5Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico5Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico5Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico5Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico5Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico5Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico5Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico5Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico5Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico5Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico5Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico5Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico5Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico5Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico5ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico5ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico5ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico5ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico5ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico5ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico5ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico5ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico5_botao1` enum('0','1') DEFAULT '1',
  `body_servico5_botao2` enum('0','1') DEFAULT '1',
  `body_servico5_botao3` enum('0','1') DEFAULT '1',
  `body_servico5_botao4` enum('0','1') DEFAULT '1',
  `body_servico5_botao5` enum('0','1') DEFAULT '1',
  `body_servico5_botao6` enum('0','1') DEFAULT '1',
  `body_servico5_botao7` enum('0','1') DEFAULT '1',
  `body_servico5_botao8` enum('0','1') DEFAULT '1',
  `body_servico5_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto5` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto6` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto7` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoTexto8` varchar(45) DEFAULT 'Ver +',
  `body_servico5_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink4` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink5` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink6` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink7` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_botaoLink8` varchar(145) DEFAULT '//google.com.br',
  `body_servico5_preco1_opcao1` enum('0','1') DEFAULT '1',
  `body_servico5_preco2_opcao2` enum('0','1') DEFAULT '1',
  `body_servico5_preco3_opcao3` enum('0','1') DEFAULT '1',
  `body_servico5_preco4_opcao4` enum('0','1') DEFAULT '1',
  `body_servico5_preco5_opcao5` enum('0','1') DEFAULT '1',
  `body_servico5_preco6_opcao6` enum('0','1') DEFAULT '1',
  `body_servico5_preco7_opcao7` enum('0','1') DEFAULT '1',
  `body_servico5_preco8_opcao8` enum('0','1') DEFAULT '1',
  `body_servico5_preco1` varchar(45) DEFAULT '0.00',
  `body_servico5_preco2` varchar(45) DEFAULT '0.00',
  `body_servico5_preco3` varchar(45) DEFAULT '0.00',
  `body_servico5_preco4` varchar(45) DEFAULT '0.00',
  `body_servico5_preco5` varchar(45) DEFAULT '0.00',
  `body_servico5_preco6` varchar(45) DEFAULT '0.00',
  `body_servico5_preco7` varchar(45) DEFAULT '0.00',
  `body_servico5_preco8` varchar(45) DEFAULT '0.00',
  `body_servico5_opcao1Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao1Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao1Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao1Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao1Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao1Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao1Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao1Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao1Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao1Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao2Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao2Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao2Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao2Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao2Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao2Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao2Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao2Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao2Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao2Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao3Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao3Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao3Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao3Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao3Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao3Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao3Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao3Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao3Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao3Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao4Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao4Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao4Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao4Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao4Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao4Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao4Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao4Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao4Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao4Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao5Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao5Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao5Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao5Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao5Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao5Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao5Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao5Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao5Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao5Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao6Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao6Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao6Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao6Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao6Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao6Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao6Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao6Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao6Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao6Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao7Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao7Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao7Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao7Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao7Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao7Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao7Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao7Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao7Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao7Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao8Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao8Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao8Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao8Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao8Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao8Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao8Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao8Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao8Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao8Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao9Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao9Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao9Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao9Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao9Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao9Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao9Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao9Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao9Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao9Descricao10` varchar(45) DEFAULT 'Descrição 10',
  `body_servico5_opcao10Descricao1` varchar(45) DEFAULT 'Descrição 1',
  `body_servico5_opcao10Descricao2` varchar(45) DEFAULT 'Descrição 2',
  `body_servico5_opcao10Descricao3` varchar(45) DEFAULT 'Descrição 3',
  `body_servico5_opcao10Descricao4` varchar(45) DEFAULT 'Descrição 4',
  `body_servico5_opcao10Descricao5` varchar(45) DEFAULT 'Descrição 5',
  `body_servico5_opcao10Descricao6` varchar(45) DEFAULT 'Descrição 6',
  `body_servico5_opcao10Descricao7` varchar(45) DEFAULT 'Descrição 7',
  `body_servico5_opcao10Descricao8` varchar(45) DEFAULT 'Descrição 8',
  `body_servico5_opcao10Descricao9` varchar(45) DEFAULT 'Descrição 9',
  `body_servico5_opcao10Descricao10` varchar(45) DEFAULT 'Descrição 10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico5`
--

LOCK TABLES `lp_modelosconfig_servico5` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico5` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico5` VALUES (1,1,'1','1','Titulo serviço 5','Descrição serviço 5','1','1','1','1','1','1','1','1','img_servicos_868824182.jpg','img_servicos_1614353600.jpg','img_servicos_214148070.jpg','img_servicos_1264671382.jpg','img_servicos_70487885.jpg','img_servicos_156864761.jpg','img_servicos_165478150.jpg','img_servicos_201846313.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','1','1','1','1','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','1','1','1','1','1','1','1','1','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10'),(1,2,'1','1','Titulo serviço 5','Descrição serviço 5','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','1','1','1','1','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','1','1','1','1','1','1','1','1','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10'),(1,3,'1','1','Titulo serviço 5','Descrição serviço 5','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','1','1','1','1','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','//google.com.br','1','1','1','1','1','1','1','1','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10','Descrição 1','Descrição 2','Descrição 3','Descrição 4','Descrição 5','Descrição 6','Descrição 7','Descrição 8','Descrição 9','Descrição 10');
/*!40000 ALTER TABLE `lp_modelosconfig_servico5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico6`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico6`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico6` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico6` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico6` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo6` varchar(45) DEFAULT 'Titulo serviço 6',
  `body_textoGenericoDescricao6` varchar(512) DEFAULT 'Descrição serviço 6',
  `body_servico6_opcao1` enum('0','1') DEFAULT '1',
  `body_servico6_opcao2` enum('0','1') DEFAULT '1',
  `body_servico6_opcao3` enum('0','1') DEFAULT '1',
  `body_servico6_opcao4` enum('0','1') DEFAULT '1',
  `body_servico6_opcao5` enum('0','1') DEFAULT '1',
  `body_servico6_opcao6` enum('0','1') DEFAULT '1',
  `body_servico6_opcao7` enum('0','1') DEFAULT '1',
  `body_servico6_opcao8` enum('0','1') DEFAULT '1',
  `body_servico6Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico6Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico6Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico6Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico6Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico6Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico6Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico6Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico6Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico6Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico6Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico6Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico6Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico6Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico6Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico6Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico6Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico6ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico6ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico6ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico6ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico6ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico6ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico6ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico6ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico6_botao1` enum('0','1') DEFAULT '1',
  `body_servico6_botao2` enum('0','1') DEFAULT '1',
  `body_servico6_botao3` enum('0','1') DEFAULT '1',
  `body_servico6_botao4` enum('0','1') DEFAULT '1',
  `body_servico6_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico6_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico6_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico6`
--

LOCK TABLES `lp_modelosconfig_servico6` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico6` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico6` VALUES (1,1,'1','1','Titulo serviço 6','Descrição serviço 6','1','1','1','1','1','1','1','1','img_servicos_1911543749.jpg','img_servicos_1764444234.jpg','img_servicos_864422360.jpg','img_servicos_335848619.jpg','img_servicos_2114683196.jpg','img_servicos_1877245552.jpg','img_servicos_566385506.jpg','img_servicos_1850458837.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 6','Descrição serviço 6','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 6','Descrição serviço 6','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico6` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico7`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico7`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico7` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico7` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico7` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo7` varchar(45) DEFAULT 'Titulo serviço 7',
  `body_textoGenericoDescricao7` varchar(512) DEFAULT 'Descrição serviço 7',
  `body_servico7_opcao1` enum('0','1') DEFAULT '1',
  `body_servico7_opcao2` enum('0','1') DEFAULT '1',
  `body_servico7_opcao3` enum('0','1') DEFAULT '1',
  `body_servico7_opcao4` enum('0','1') DEFAULT '1',
  `body_servico7_opcao5` enum('0','1') DEFAULT '1',
  `body_servico7_opcao6` enum('0','1') DEFAULT '1',
  `body_servico7_opcao7` enum('0','1') DEFAULT '1',
  `body_servico7_opcao8` enum('0','1') DEFAULT '1',
  `body_servico7Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico7Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico7Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico7Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico7Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico7Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico7Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico7Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico7Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico7Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico7Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico7Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico7Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico7Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico7Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico7Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico7Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico7ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico7ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico7ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico7ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico7ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico7ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico7ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico7ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico7_botao1` enum('0','1') DEFAULT '1',
  `body_servico7_botao2` enum('0','1') DEFAULT '1',
  `body_servico7_botao3` enum('0','1') DEFAULT '1',
  `body_servico7_botao4` enum('0','1') DEFAULT '1',
  `body_servico7_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico7_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico7_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico7`
--

LOCK TABLES `lp_modelosconfig_servico7` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico7` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico7` VALUES (1,1,'1','1','Titulo serviço 7','Descrição serviço 7','1','1','1','1','1','1','1','1','img_servicos_793904790.jpg','img_servicos_889542648.jpg','img_servicos_1795373101.jpg','img_servicos_302488441.jpg','img_servicos_999830559.jpg','img_servicos_1559030168.jpg','img_servicos_65712972.jpg','img_servicos_723085572.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 7','Descrição serviço 7','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 7','Descrição serviço 7','1','1','1','1','1','1','1','1','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico8`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico8`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico8` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_servico8` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico8` enum('0','1') DEFAULT '1',
  `body_textoGenericoTitulo8` varchar(45) DEFAULT 'Titulo serviço 8',
  `body_textoGenericoDescricao8` varchar(512) DEFAULT 'Descrição serviço 8',
  `body_servico8Imagem1` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem2` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem3` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem4` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem5` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem6` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem7` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Imagem8` varchar(145) DEFAULT 'padrao.jpg',
  `body_servico8Titulo1` varchar(45) DEFAULT 'teste texto titulo 1',
  `body_servico8Titulo2` varchar(45) DEFAULT 'teste texto titulo 2',
  `body_servico8Titulo3` varchar(45) DEFAULT 'teste texto titulo 3',
  `body_servico8Titulo4` varchar(45) DEFAULT 'teste texto titulo 4',
  `body_servico8Titulo5` varchar(45) DEFAULT 'teste texto titulo 5',
  `body_servico8Titulo6` varchar(45) DEFAULT 'teste texto titulo 6',
  `body_servico8Titulo7` varchar(45) DEFAULT 'teste texto titulo 7',
  `body_servico8Titulo8` varchar(45) DEFAULT 'teste texto titulo 8',
  `body_servico8Descricao1` varchar(256) DEFAULT 'descrição serviço 1',
  `body_servico8Descricao2` varchar(256) DEFAULT 'descrição serviço 2',
  `body_servico8Descricao3` varchar(256) DEFAULT 'descrição serviço 3',
  `body_servico8Descricao4` varchar(256) DEFAULT 'descrição serviço 4',
  `body_servico8Descricao5` varchar(256) DEFAULT 'descrição serviço 5',
  `body_servico8Descricao6` varchar(256) DEFAULT 'descrição serviço 6',
  `body_servico8Descricao7` varchar(256) DEFAULT 'descrição serviço 7',
  `body_servico8Descricao8` varchar(256) DEFAULT 'descrição serviço 8',
  `body_servico8ImagemSEO1` varchar(90) DEFAULT 'info SEO 1',
  `body_servico8ImagemSEO2` varchar(90) DEFAULT 'info SEO 2',
  `body_servico8ImagemSEO3` varchar(90) DEFAULT 'info SEO 3',
  `body_servico8ImagemSEO4` varchar(90) DEFAULT 'info SEO 4',
  `body_servico8ImagemSEO5` varchar(90) DEFAULT 'info SEO 5',
  `body_servico8ImagemSEO6` varchar(90) DEFAULT 'info SEO 6',
  `body_servico8ImagemSEO7` varchar(90) DEFAULT 'info SEO 7',
  `body_servico8ImagemSEO8` varchar(90) DEFAULT 'info SEO 8',
  `body_servico8_botao1` enum('0','1') DEFAULT '1',
  `body_servico8_botao2` enum('0','1') DEFAULT '1',
  `body_servico8_botao3` enum('0','1') DEFAULT '1',
  `body_servico8_botao4` enum('0','1') DEFAULT '1',
  `body_servico8_botaoTexto1` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto2` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto3` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoTexto4` varchar(45) DEFAULT 'Ver +',
  `body_servico8_botaoLink1` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink2` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink3` varchar(145) DEFAULT '//google.com.br',
  `body_servico8_botaoLink4` varchar(145) DEFAULT '//google.com.br'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico8`
--

LOCK TABLES `lp_modelosconfig_servico8` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico8` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico8` VALUES (1,1,'1','1','Titulo serviço 8','Descrição serviço 8','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,2,'1','1','Titulo serviço 8','Descrição serviço 8','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br'),(1,3,'1','1','Titulo serviço 8','Descrição serviço 8','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','padrao.jpg','teste texto titulo 1','teste texto titulo 2','teste texto titulo 3','teste texto titulo 4','teste texto titulo 5','teste texto titulo 6','teste texto titulo 7','teste texto titulo 8','descrição serviço 1','descrição serviço 2','descrição serviço 3','descrição serviço 4','descrição serviço 5','descrição serviço 6','descrição serviço 7','descrição serviço 8','info SEO 1','info SEO 2','info SEO 3','info SEO 4','info SEO 5','info SEO 6','info SEO 7','info SEO 8','1','1','1','1','Ver +','Ver +','Ver +','Ver +','//google.com.br','//google.com.br','//google.com.br','//google.com.br');
/*!40000 ALTER TABLE `lp_modelosconfig_servico8` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico_maps`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico_maps` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_maps` enum('0','1') DEFAULT '1',
  `body_conteudoGenerico_maps` enum('0','1') DEFAULT '1',
  `body_maps_textoGenericoTitulo1` varchar(45) DEFAULT 'Titulo serviço MAPS',
  `body_maps_textoGenericoDescricao1` varchar(512) DEFAULT 'Descrição serviço MAPS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico_maps`
--

LOCK TABLES `lp_modelosconfig_servico_maps` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico_maps` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico_maps` VALUES (1,1,'1','1','Titulo serviço MAPS','Descrição serviço MAPS'),(1,2,'1','1','Titulo serviço MAPS','Descrição serviço MAPS'),(1,3,'1','1','Titulo serviço MAPS','Descrição serviço MAPS');
/*!40000 ALTER TABLE `lp_modelosconfig_servico_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_modelosconfig_servico_paginasucesso`
--

DROP TABLE IF EXISTS `lp_modelosconfig_servico_paginasucesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_modelosconfig_servico_paginasucesso` (
  `id_modelo` int(11) NOT NULL,
  `id_landingPage` int(11) DEFAULT NULL,
  `body_textoGenerico_paginaSucesso` varchar(512) DEFAULT NULL,
  `body_textoGenericoDescricao_paginaSucesso` longtext,
  `body_icone_paginaSucesso` varchar(100) DEFAULT NULL,
  `body_iconeBotao_paginaSucesso` varchar(100) DEFAULT NULL,
  `body_textoBotao_paginaSucesso` varchar(45) DEFAULT NULL,
  `body_linkBotao_paginaSucesso` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_modelosconfig_servico_paginasucesso`
--

LOCK TABLES `lp_modelosconfig_servico_paginasucesso` WRITE;
/*!40000 ALTER TABLE `lp_modelosconfig_servico_paginasucesso` DISABLE KEYS */;
INSERT INTO `lp_modelosconfig_servico_paginasucesso` VALUES (1,1,'Obrigado por se cadastrar!!','<p>Fique atento ao seu e-mail, em breve enviaremos novidade.</p>\r\n',' mdi mdi-near-me',' mdi mdi-screw-flat-top','Voltar!','//decoradoces.com.br'),(1,2,NULL,NULL,NULL,NULL,NULL,NULL),(1,3,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `lp_modelosconfig_servico_paginasucesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lp_planos`
--

DROP TABLE IF EXISTS `lp_planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lp_planos` (
  `id_planos` int(11) NOT NULL,
  `nome_planos` varchar(145) DEFAULT NULL,
  `valor_planos` varchar(12) DEFAULT NULL,
  `meses_planos` int(11) DEFAULT NULL,
  `qtdPages_planos` int(11) DEFAULT NULL,
  `destaque_planos` enum('0','1') DEFAULT NULL,
  `descricao_planos` longtext,
  `status_planos` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_planos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lp_planos`
--

LOCK TABLES `lp_planos` WRITE;
/*!40000 ALTER TABLE `lp_planos` DISABLE KEYS */;
INSERT INTO `lp_planos` VALUES (1,'LP 1','1.500,00',2,10,'0','<p>asdasd</p>\r\n\r\n<p>asd</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>asda</p>\r\n\r\n<p>sdas</p>\r\n\r\n<p>dasda</p>\r\n','1'),(2,'LP 2','29,90',1,10,'1','<p>asdasd</p>\r\n\r\n<p>asd</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>asda</p>\r\n\r\n<p>sdas</p>\r\n\r\n<p>dasda</p>\r\n','1'),(3,'LP 3','850,00',4,5,'0','<p>asdasd</p>\r\n\r\n<p>asd</p>\r\n\r\n<p>sd</p>\r\n\r\n<p>asda</p>\r\n\r\n<p>sdas</p>\r\n\r\n<p>dasda</p>\r\n','1');
/*!40000 ALTER TABLE `lp_planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meuplano`
--

DROP TABLE IF EXISTS `meuplano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meuplano` (
  `id_meuPlano` int(11) NOT NULL,
  `dataInicio_meuPlano` varchar(9) DEFAULT NULL,
  `dataFim_meuPlano` varchar(9) DEFAULT NULL,
  `dataAviso1_meuPlano` varchar(9) DEFAULT NULL,
  `dataAviso2_meuPlano` varchar(9) DEFAULT NULL,
  `dataAviso3_meuPlano` varchar(9) DEFAULT NULL,
  `dataFimServico_meuPlano` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`id_meuPlano`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meuplano`
--

LOCK TABLES `meuplano` WRITE;
/*!40000 ALTER TABLE `meuplano` DISABLE KEYS */;
INSERT INTO `meuplano` VALUES (1,'20200127','19691231','19700103','19700106','19700109','19700110');
/*!40000 ALTER TABLE `meuplano` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_aberturas_email`
--

DROP TABLE IF EXISTS `newsletter_aberturas_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletter_aberturas_email` (
  `id_aberturas` bigint(14) unsigned NOT NULL,
  `nome_aberturas` varchar(145) DEFAULT NULL,
  `email_aberturas` varchar(256) DEFAULT NULL,
  `aberto_aberturas` int(11) DEFAULT '0',
  `campanha_aberturas` varchar(512) DEFAULT NULL,
  `data_aberturas` varchar(15) DEFAULT NULL,
  `hora_aberturas` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_aberturas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_aberturas_email`
--

LOCK TABLES `newsletter_aberturas_email` WRITE;
/*!40000 ALTER TABLE `newsletter_aberturas_email` DISABLE KEYS */;
INSERT INTO `newsletter_aberturas_email` VALUES (1,'asdasdasd','asdas@gmail.com',1,'asdasdasd','20200527','10:25:40');
/*!40000 ALTER TABLE `newsletter_aberturas_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_campanhas`
--

DROP TABLE IF EXISTS `newsletter_campanhas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletter_campanhas` (
  `id_campanha` bigint(15) NOT NULL,
  `nome_campanha` varchar(90) NOT NULL,
  `titulo_campanha` varchar(90) NOT NULL,
  `assunto_campanha` varchar(150) NOT NULL,
  `destino_campanha` text NOT NULL,
  `mensagem_campanha` longblob NOT NULL,
  `statusBotao_campanha` enum('0','1','2') NOT NULL COMMENT '0 = pendente\n1 = em andamento\n2 = enviada',
  `id_templateEmail` int(11) NOT NULL,
  PRIMARY KEY (`id_campanha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_campanhas`
--

LOCK TABLES `newsletter_campanhas` WRITE;
/*!40000 ALTER TABLE `newsletter_campanhas` DISABLE KEYS */;
INSERT INTO `newsletter_campanhas` VALUES (1,'teste','teste','teste','5',_binary '<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n','2',1),(2,'asdasd','asdasdasd','asdasd','5',_binary '<p>asdasdasd</p>\r\n','2',1),(3,'teste 3','teste de envio 3','Esse é um e-mail de teste nº 3','5',_binary '<table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' style=\'width:100%\'>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n','2',1);
/*!40000 ALTER TABLE `newsletter_campanhas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_configs_template_email`
--

DROP TABLE IF EXISTS `newsletter_configs_template_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletter_configs_template_email` (
  `id_configs_template` int(11) NOT NULL AUTO_INCREMENT,
  `linkImagem_configs_template` varchar(512) DEFAULT NULL,
  `linkLogotipo_configs_template` varchar(512) DEFAULT NULL,
  `nomeEmpresa_configs_template` varchar(145) DEFAULT NULL,
  `endereco_configs_template` varchar(145) DEFAULT NULL,
  `dominio_configs_template` varchar(145) DEFAULT NULL,
  `linkDominio_configs_template` varchar(512) DEFAULT NULL,
  `linkFacebook_configs_template` varchar(512) DEFAULT NULL,
  `linkInstagram_configs_template` varchar(512) DEFAULT NULL,
  `linkWhatsApp_configs_template` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_configs_template`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_configs_template_email`
--

LOCK TABLES `newsletter_configs_template_email` WRITE;
/*!40000 ALTER TABLE `newsletter_configs_template_email` DISABLE KEYS */;
INSERT INTO `newsletter_configs_template_email` VALUES (1,'http://www.crfgo.org.br','https://i.imgur.com/SNLpEMJ.png','CRF-GO | Conselho Regional de Farmácia do Estado de Goiás','Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110','www.crfgo.org.br','http://www.crfgo.org.br','www','www','www');
/*!40000 ALTER TABLE `newsletter_configs_template_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_emails`
--

DROP TABLE IF EXISTS `newsletter_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletter_emails` (
  `id_emails` int(11) NOT NULL,
  `news_nome` varchar(65) DEFAULT NULL,
  `news_emails` varchar(120) DEFAULT NULL,
  `news_status` enum('0','1') DEFAULT NULL,
  `news_grupoLista` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_emails`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_emails`
--

LOCK TABLES `newsletter_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_emails` DISABLE KEYS */;
INSERT INTO `newsletter_emails` VALUES (1,'Josheffer Robert','josheffer@gmail.com','1',5),(2,'uoeiuweiou','qwaskdljaslkj@gmail.com','1',1),(3,'rtterter','rdsfsdf@gmail.com','1',1),(4,'aaaaaaaaa','aaaaaa@gmail.com','1',1),(5,'wwww','3123123@gmail.com','1',1),(6,'sssssss','ssss@gmail.com','1',1);
/*!40000 ALTER TABLE `newsletter_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_envios`
--

DROP TABLE IF EXISTS `newsletter_envios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletter_envios` (
  `id_envios` bigint(14) unsigned NOT NULL,
  `id_campanha` int(11) DEFAULT NULL,
  `nome_campanha_envios` varchar(120) DEFAULT NULL,
  `nomePessoa_envios` varchar(90) DEFAULT NULL,
  `mensagem_envios` longblob,
  `assunto_envios` varchar(120) DEFAULT NULL,
  `destino_envios` text,
  `emails_envios` varchar(120) DEFAULT NULL,
  `statusEmails_envios` enum('0','1','2') DEFAULT NULL,
  PRIMARY KEY (`id_envios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_envios`
--

LOCK TABLES `newsletter_envios` WRITE;
/*!40000 ALTER TABLE `newsletter_envios` DISABLE KEYS */;
INSERT INTO `newsletter_envios` VALUES (20200527000001,2,'asdasdasd','asdasdasd',_binary '<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">asdasdasd</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdasd</p>\r\n</p>\r\n\r\n<img src=\'http://letterpage.net.br/newsletter/confirmarEmail?nome=asdasdasd&email=asdas@gmail.com&campanha=asdasdasd\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=asdas@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','asdasd','5','asdas@gmail.com','1'),(20200527000002,2,'asdasdasd','asdasdasdasdasd',_binary '<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">asdasdasdasdasd</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdasdasd</p>\r\n</p>\r\n\r\n<img src=\'http://letterpage.net.br/newsletter/confirmarEmail?nome=asdasdasdasdasd&email=asdasdasdasd12321@gmail.com&campanha=asdasdasd\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=asdasdasdasd12321@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','asdasd','5','asdasdasdasd12321@gmail.com','1'),(20200527000003,1,'teste','Josheffer Robert',_binary '<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        Entice the open with some amazing preheader text. Use a little mystery and get those subscribers to read through...\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">Josheffer Robert</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n</p>\r\n\r\n<img src=\'http://letterpage.net.br/newsletter/confirmarEmail?nome=Josheffer Robert&email=josheffer@gmail.com&campanha=teste\' width=\'1px\' height=\'1px\'>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=josheffer@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','teste','5','josheffer@gmail.com','1'),(20200527000004,3,'teste de envio 3','Josheffer Robert',_binary '<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        <span id=\"titulo2\">teste de envio 3</span>\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"http://www.crfgo.org.br\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"https://i.imgur.com/SNLpEMJ.png\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">Josheffer Robert</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\"><table border=\'0\' cellpadding=\'0\' cellspacing=\'0\' style=\'width:100%\'>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n\r\n			<p>asdkljasdjasldkjasdkljaskldjsakldjklasj</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</p>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">Rua 1122, Nº 198 - St. Marista, Goiânia - GO - 74.175-110</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"http://letterpage.net.br/newsletter/cancelar_inscricao?email=josheffer@gmail.com&campanha=5\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"http://www.crfgo.org.br\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">www.crfgo.org.br</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','Esse é um e-mail de teste nº 3','5','josheffer@gmail.com','1');
/*!40000 ALTER TABLE `newsletter_envios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_grupos`
--

DROP TABLE IF EXISTS `newsletter_grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletter_grupos` (
  `id_grupos` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `nome_grupos` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_grupos`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_grupos`
--

LOCK TABLES `newsletter_grupos` WRITE;
/*!40000 ALTER TABLE `newsletter_grupos` DISABLE KEYS */;
INSERT INTO `newsletter_grupos` VALUES (1,'teste 1'),(2,'teste 2'),(3,'teste 3'),(4,'teste 4'),(5,'jozin');
/*!40000 ALTER TABLE `newsletter_grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_grupos_x_emails`
--

DROP TABLE IF EXISTS `newsletter_grupos_x_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletter_grupos_x_emails` (
  `id_grupos` tinyint(2) unsigned NOT NULL,
  `id_emails` bigint(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_grupos_x_emails`
--

LOCK TABLES `newsletter_grupos_x_emails` WRITE;
/*!40000 ALTER TABLE `newsletter_grupos_x_emails` DISABLE KEYS */;
INSERT INTO `newsletter_grupos_x_emails` VALUES (5,1),(1,2),(1,3),(1,4),(1,5),(1,6);
/*!40000 ALTER TABLE `newsletter_grupos_x_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_template_email`
--

DROP TABLE IF EXISTS `newsletter_template_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletter_template_email` (
  `id_templateEmail` int(11) NOT NULL AUTO_INCREMENT,
  `nome_templateEmail` varchar(145) DEFAULT NULL,
  `html_templateEmail` longblob,
  `status_templateEmail` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_templateEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_template_email`
--

LOCK TABLES `newsletter_template_email` WRITE;
/*!40000 ALTER TABLE `newsletter_template_email` DISABLE KEYS */;
INSERT INTO `newsletter_template_email` VALUES (1,'Padrão',_binary '<!DOCTYPE html>\r\n<html>\r\n\r\n<head>\r\n    <title>Titulo</title>\r\n\r\n    <meta charset=\"utf-8\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n    <style type=\"text/css\">\r\n        body,\r\n        table,\r\n        td,\r\n        a {\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n        \r\n        table,\r\n        td {\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n        \r\n        img {\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n        \r\n        table {\r\n            border-collapse: collapse !important;\r\n        }\r\n        \r\n        body {\r\n            height: 100% !important;\r\n            margin: 0 !important;\r\n            padding: 0 !important;\r\n            width: 100% !important;\r\n        }\r\n        \r\n        a[x-apple-data-detectors] {\r\n            color: inherit !important;\r\n            text-decoration: none !important;\r\n            font-size: inherit !important;\r\n            font-family: inherit !important;\r\n            font-weight: inherit !important;\r\n            line-height: inherit !important;\r\n        }\r\n        \r\n        @media screen and (max-width: 525px) {\r\n            /* ALLOWS FOR FLUID TABLES */\r\n            .wrapper {\r\n                width: 100% !important;\r\n                max-width: 100% !important;\r\n            }\r\n            /* ADJUSTS LAYOUT OF LOGO IMAGE */\r\n            .logo img {\r\n                margin: 0 auto !important;\r\n            }\r\n            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */\r\n            .mobile-hide {\r\n                display: none !important;\r\n            }\r\n            .img-max {\r\n                max-width: 100% !important;\r\n                width: 100% !important;\r\n                height: auto !important;\r\n            }\r\n            /* FULL-WIDTH TABLES */\r\n            .responsive-table {\r\n                width: 100% !important;\r\n            }\r\n            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */\r\n            .padding {\r\n                padding: 10px 5% 15px 5% !important;\r\n            }\r\n            .padding-meta {\r\n                padding: 30px 5% 0px 5% !important;\r\n                text-align: center;\r\n            }\r\n            .no-padding {\r\n                padding: 0 !important;\r\n            }\r\n            .section-padding {\r\n                padding: 50px 15px 50px 15px !important;\r\n            }\r\n            /* ADJUST BUTTONS ON MOBILE */\r\n            .mobile-button-container {\r\n                margin: 0 auto;\r\n                width: 100% !important;\r\n            }\r\n            .mobile-button {\r\n                padding: 15px !important;\r\n                border: 0 !important;\r\n                font-size: 16px !important;\r\n                display: block !important;\r\n            }\r\n        }\r\n        /* ANDROID CENTER FIX */\r\n        \r\n        div[style*=\"margin: 16px 0;\"] {\r\n            margin: 0 !important;\r\n        }\r\n    </style>\r\n</head>\r\n\r\n<body style=\"margin: 0 !important; padding: 0 !important;\">\r\n\r\n    <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\">\r\n        <span id=\"titulo2\">{{titulo2}}</span>\r\n    </div>\r\n\r\n    <!-- HEADER -->\r\n    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n        <tr>\r\n            <td bgcolor=\"#37BBCB\" align=\"center\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\">\r\n                    <tr>\r\n                        <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\">\r\n                            <a href=\"{{linkImagem}}\" id=\"linkImagem\" target=\"_blank\">\r\n                                <img alt=\"Logo\" id=\"linkImagemLogotipo\" src=\"{{linkImagemLogotipo}}\" width=\"320\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\">\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#F5F7FA\" align=\"center\" style=\"padding: 70px 15px 70px 15px;\" class=\"section-padding\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td>\r\n                            <!-- TITLE SECTION AND COPY -->\r\n                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;\" class=\"padding\">Olá,\r\n                                        <span id=\"nomePessoa\">{{nomePessoa}}</span>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align=\"center\" style=\"padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">\r\n\r\n                                        <p id=\"mensagem\">{{mensagem}}</p>\r\n\r\n\r\n\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </td>\r\n                    </tr>\r\n\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <td bgcolor=\"#E6E9ED\" align=\"center\" style=\"padding: 20px 0px;\">\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\">\r\n            <tr>\r\n            <td align=\"center\" valign=\"top\" width=\"500\">\r\n            <![endif]-->\r\n                <!-- UNSUBSCRIBE COPY -->\r\n                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\">\r\n                    <tr>\r\n                        <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\">\r\n                            <span id=\"endereco\">{{endereco}}</span>\r\n                            <br>\r\n                            <a id=\"cancelarInscricao\" href=\"{{cancelarInscricao}}\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\">Cancelar Inscrição</a>\r\n                            <span style=\"font-family: Arial, sans-serif; font-size: 12px; color: #444444;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span>\r\n                            <a id=\"linkDominio\" href=\"{{linkDominio}}\" target=\"_blank\" style=\"color: #666666; text-decoration: none;\"> <span id=\"dominio\">{{dominio}}</span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </table>\r\n                <!--[if (gte mso 9)|(IE)]>\r\n            </td>\r\n            </tr>\r\n            </table>\r\n            <![endif]-->\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</body>\r\n\r\n</html>','1');
/*!40000 ALTER TABLE `newsletter_template_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pu_niveis_paginas`
--

DROP TABLE IF EXISTS `pu_niveis_paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pu_niveis_paginas` (
  `id_niveisPaginas` int(11) NOT NULL AUTO_INCREMENT,
  `id_nivelAcesso` varchar(45) DEFAULT NULL,
  `id_pagina` varchar(45) DEFAULT NULL,
  `permissao_niveisPaginas` enum('0','1') DEFAULT NULL,
  `menu_niveisPaginas` enum('0','1') DEFAULT NULL,
  `add_niveisPaginas` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_niveisPaginas`)
) ENGINE=InnoDB AUTO_INCREMENT=296 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pu_niveis_paginas`
--

LOCK TABLES `pu_niveis_paginas` WRITE;
/*!40000 ALTER TABLE `pu_niveis_paginas` DISABLE KEYS */;
INSERT INTO `pu_niveis_paginas` VALUES (6,'5','1','1','1','1'),(7,'4','1','1','0','1'),(8,'3','1','0','0','0'),(9,'2','1','1','1','1'),(10,'1','1','1','1','1'),(11,'5','2','1','1','1'),(12,'4','2','1','0','1'),(13,'3','2','0','0','0'),(14,'2','2','0','1','1'),(15,'1','2','1','1','1'),(16,'5','3','0','0','1'),(17,'4','3','1','1','1'),(18,'3','3','0','0','0'),(19,'2','3','0','0','1'),(20,'1','3','1','1','1'),(21,'5','4','0','0','1'),(22,'4','4','1','1','1'),(23,'3','4','0','0','0'),(24,'2','4','0','0','1'),(25,'1','4','1','1','1'),(26,'5','5','0','0','1'),(27,'4','5','1','1','1'),(28,'3','5','0','0','0'),(29,'2','5','0','0','1'),(30,'1','5','1','1','1'),(31,'5','6','0','0','1'),(32,'4','6','1','1','1'),(33,'3','6','0','0','0'),(34,'2','6','0','0','1'),(35,'1','6','1','1','1'),(36,'5','7','1','0','1'),(37,'4','7','1','1','1'),(38,'3','7','0','0','0'),(39,'2','7','0','0','1'),(40,'1','7','1','0','1'),(41,'5','8','1','0','1'),(42,'4','8','1','1','1'),(43,'3','8','0','0','0'),(44,'2','8','0','0','1'),(45,'1','8','1','0','1'),(46,'5','9','1','0','1'),(47,'4','9','1','1','1'),(48,'3','9','0','0','0'),(49,'2','9','0','0','1'),(50,'1','9','1','0','1'),(51,'5','10','1','0','1'),(52,'4','10','1','1','1'),(53,'3','10','0','0','0'),(54,'2','10','0','0','1'),(55,'1','10','1','0','1'),(56,'5','11','0','0','1'),(57,'4','11','1','1','1'),(58,'3','11','0','0','0'),(59,'2','11','0','0','1'),(60,'1','11','1','0','1'),(61,'5','12','1','0','1'),(62,'4','12','1','1','1'),(63,'3','12','0','0','0'),(64,'2','12','0','0','1'),(65,'1','12','1','0','1'),(66,'5','13','1','0','1'),(67,'4','13','1','1','1'),(68,'3','13','0','0','0'),(69,'2','13','0','0','1'),(70,'1','13','1','0','1'),(71,'5','14','1','0','1'),(72,'4','14','1','1','1'),(73,'3','14','0','0','0'),(74,'2','14','0','0','1'),(75,'1','14','1','0','1'),(76,'5','15','0','0','1'),(77,'4','15','1','1','1'),(78,'3','15','0','0','0'),(79,'2','15','0','0','1'),(80,'1','15','1','0','1'),(81,'5','16','0','0','1'),(82,'4','16','1','1','1'),(83,'3','16','0','0','0'),(84,'2','16','0','0','1'),(85,'1','16','1','0','1'),(86,'5','17','0','0','1'),(87,'4','17','1','1','1'),(88,'3','17','0','0','0'),(89,'2','17','0','0','1'),(90,'1','17','1','0','1'),(91,'5','18','1','0','1'),(92,'4','18','1','1','1'),(93,'3','18','0','0','0'),(94,'2','18','1','0','1'),(95,'1','18','1','0','1'),(96,'5','19','1','0','1'),(97,'4','19','0','0','0'),(98,'3','19','0','0','0'),(99,'2','19','0','0','1'),(100,'1','19','1','0','1'),(101,'5','20','1','0','1'),(102,'4','20','0','0','0'),(103,'3','20','0','0','0'),(104,'2','20','0','0','1'),(105,'1','20','1','0','1'),(106,'5','21','1','0','1'),(107,'4','21','0','0','0'),(108,'3','21','0','0','0'),(109,'2','21','0','0','1'),(110,'1','21','1','0','1'),(111,'5','22','1','0','1'),(112,'4','22','0','0','0'),(113,'3','22','0','0','0'),(114,'2','22','0','0','1'),(115,'1','22','1','0','1'),(116,'5','23','1','0','1'),(117,'4','23','0','0','0'),(118,'3','23','0','0','0'),(119,'2','23','0','0','1'),(120,'1','23','1','0','1'),(121,'5','24','0','0','1'),(122,'4','24','0','0','0'),(123,'3','24','0','0','0'),(124,'2','24','0','0','1'),(125,'1','24','1','0','1'),(126,'5','25','0','0','1'),(127,'4','25','0','0','0'),(128,'3','25','0','0','0'),(129,'2','25','0','0','1'),(130,'1','25','1','0','1'),(131,'5','26','1','0','1'),(132,'4','26','0','0','0'),(133,'3','26','0','0','0'),(134,'2','26','0','0','1'),(135,'1','26','1','0','1'),(136,'5','27','1','0','1'),(137,'4','27','0','0','0'),(138,'3','27','0','0','0'),(139,'2','27','0','0','1'),(140,'1','27','1','0','1'),(141,'5','28','1','0','1'),(142,'4','28','0','0','0'),(143,'3','28','0','0','0'),(144,'2','28','0','0','1'),(145,'1','28','1','0','1'),(146,'5','29','1','0','1'),(147,'4','29','0','0','0'),(148,'3','29','0','0','0'),(149,'2','29','0','0','1'),(150,'1','29','1','0','1'),(151,'5','30','0','0','1'),(152,'4','30','0','0','0'),(153,'3','30','0','0','0'),(154,'2','30','0','0','1'),(155,'1','30','1','0','1'),(156,'5','31','0','0','1'),(157,'4','31','0','0','0'),(158,'3','31','0','0','0'),(159,'2','31','0','0','1'),(160,'1','31','1','0','1'),(161,'5','32','0','0','1'),(162,'4','32','0','0','0'),(163,'3','32','0','0','0'),(164,'2','32','0','0','1'),(165,'1','32','1','0','1'),(166,'5','33','0','0','1'),(167,'4','33','1','0','0'),(168,'3','33','1','0','0'),(169,'2','33','0','0','1'),(170,'1','33','1','0','1'),(171,'5','34','0','0','1'),(172,'4','34','0','0','0'),(173,'3','34','0','0','0'),(174,'2','34','0','0','1'),(175,'1','34','1','0','1'),(176,'5','35','0','0','1'),(177,'4','35','0','0','0'),(178,'3','35','0','0','0'),(179,'2','35','0','0','1'),(180,'1','35','1','0','1'),(181,'5','36','0','0','1'),(182,'4','36','0','0','0'),(183,'3','36','0','0','0'),(184,'2','36','0','0','1'),(185,'1','36','1','1','1'),(186,'5','37','0','0','1'),(187,'4','37','0','0','0'),(188,'3','37','0','0','0'),(189,'2','37','0','0','1'),(190,'1','37','1','0','1'),(191,'5','38','0','0','1'),(192,'4','38','0','0','0'),(193,'3','38','0','0','0'),(194,'2','38','0','0','1'),(195,'1','38','1','0','1'),(196,'5','39','0','0','1'),(197,'4','39','0','0','0'),(198,'3','39','0','0','0'),(199,'2','39','0','0','1'),(200,'1','39','1','0','1'),(201,'5','40','0','0','1'),(202,'4','40','0','0','0'),(203,'3','40','0','0','0'),(204,'2','40','0','0','1'),(205,'1','40','1','0','1'),(206,'5','41','0','0','1'),(207,'4','41','0','0','0'),(208,'3','41','0','0','0'),(209,'2','41','0','0','1'),(210,'1','41','1','0','1'),(211,'5','42','0','0','1'),(212,'4','42','0','0','0'),(213,'3','42','0','0','0'),(214,'2','42','0','0','1'),(215,'1','42','1','0','1'),(216,'5','43','0','0','1'),(217,'4','43','0','0','0'),(218,'3','43','0','0','0'),(219,'2','43','0','0','1'),(220,'1','43','1','0','1'),(221,'5','44','0','0','1'),(222,'4','44','0','0','0'),(223,'3','44','0','0','0'),(224,'2','44','0','0','1'),(225,'1','44','1','0','1'),(226,'5','45','0','0','1'),(227,'4','45','0','0','0'),(228,'3','45','0','0','0'),(229,'2','45','0','0','1'),(230,'1','45','1','0','1'),(231,'5','46','0','0','1'),(232,'4','46','0','0','0'),(233,'3','46','0','0','0'),(234,'2','46','0','0','1'),(235,'1','46','1','1','1'),(236,'5','47','0','0','1'),(237,'4','47','0','0','0'),(238,'3','47','0','0','0'),(239,'2','47','0','0','1'),(240,'1','47','1','0','1'),(241,'5','48','0','0','1'),(242,'4','48','0','0','0'),(243,'3','48','0','0','0'),(244,'2','48','0','0','1'),(245,'1','48','1','0','1'),(246,'5','49','0','0','1'),(247,'4','49','0','0','0'),(248,'3','49','0','0','0'),(249,'2','49','0','0','1'),(250,'1','49','1','0','1'),(251,'5','50','0','0','1'),(252,'4','50','0','0','0'),(253,'3','50','0','0','0'),(254,'2','50','0','0','1'),(255,'1','50','1','0','1'),(256,'5','51','0','0','1'),(257,'4','51','0','0','0'),(258,'3','51','0','0','0'),(259,'2','51','0','0','1'),(260,'1','51','1','0','1'),(261,'5','52','0','0','0'),(262,'4','52','0','0','0'),(263,'3','52','0','0','0'),(264,'2','52','0','0','0'),(265,'1','52','1','0','1'),(266,'5','53','0','0','0'),(267,'4','53','0','0','0'),(268,'3','53','0','0','0'),(269,'2','53','0','0','0'),(270,'1','53','1','0','1'),(271,'5','54','0','0','0'),(272,'4','54','0','0','0'),(273,'3','54','0','0','0'),(274,'2','54','0','0','0'),(275,'1','54','1','0','1'),(276,'5','55','0','0','0'),(277,'4','55','0','0','0'),(278,'3','55','0','0','0'),(279,'2','55','0','0','0'),(280,'1','55','1','0','1'),(281,'5','56','0','0','0'),(282,'4','56','0','0','0'),(283,'3','56','0','0','0'),(284,'2','56','0','0','0'),(285,'1','56','1','0','1'),(286,'5','57','0','0','0'),(287,'4','57','0','0','0'),(288,'3','57','0','0','0'),(289,'2','57','0','0','0'),(290,'1','57','1','0','1'),(291,'5','58','0','0','0'),(292,'4','58','0','0','0'),(293,'3','58','0','0','0'),(294,'2','58','0','0','0'),(295,'1','58','1','0','1');
/*!40000 ALTER TABLE `pu_niveis_paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pu_nivelacesso`
--

DROP TABLE IF EXISTS `pu_nivelacesso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pu_nivelacesso` (
  `id_nivelAcesso` int(11) NOT NULL,
  `nome_nivelAcesso` varchar(45) DEFAULT NULL,
  `cor_nivelAcesso` varchar(10) DEFAULT NULL,
  `corTexto_nivelAcesso` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pu_nivelacesso`
--

LOCK TABLES `pu_nivelacesso` WRITE;
/*!40000 ALTER TABLE `pu_nivelacesso` DISABLE KEYS */;
INSERT INTO `pu_nivelacesso` VALUES (1,'Administração','#000000','#ffffff'),(2,'Marketing','#45309c','#ffffff'),(3,'Juridico','#c02929','#ffffff'),(4,'Contabilidade','#82c655','#000000'),(5,'Cliente','#652dc4','#ffffff');
/*!40000 ALTER TABLE `pu_nivelacesso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pu_paginas`
--

DROP TABLE IF EXISTS `pu_paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pu_paginas` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(45) DEFAULT NULL,
  `endereco_pagina` varchar(256) DEFAULT NULL,
  `observacao_pagina` varchar(128) DEFAULT NULL,
  `icone_pagina` varchar(128) DEFAULT NULL,
  `controller_pagina` varchar(145) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela que armazena todas as páginas que o usuário pode acessar.\n\nusar as rotas para armazenar novas páginas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pu_paginas`
--

LOCK TABLES `pu_paginas` WRITE;
/*!40000 ALTER TABLE `pu_paginas` DISABLE KEYS */;
INSERT INTO `pu_paginas` VALUES (1,'Home','/dashboard','Página inicial',' mdi mdi-home',NULL),(2,'Landing Pages','/dashboard/landingPages','Página de gerenciamento das landing pages','fe-file-text',NULL),(3,'Usuários','/dashboard/usuarios/usuarios','Página de gerenciamento de usuários','mdi mdi-account-multiple-outline',NULL),(4,'Planos','/dashboard/planos/planos','Página de atualização de planos','la la-diamond',NULL),(5,'Permissões','/dashboard/permissoes/niveisAcesso','Página de permissões','fas fa-gavel',NULL),(6,'Configurações','/dashboard/planos','Configurações dos planos','la la-cog',NULL),(7,'Grupos','/dashboard/grupos','Página de Grupos','',NULL),(8,'Cadastrar nova landingPage','/dashboard/cadastrarNovaLandingPage','','',NULL),(9,'Configs LandingPages','/dashboard/configuracoes','Página de configurações das landingpages','',NULL),(10,'Templates LandingPages','/dashboard/templates','Página de configurações dos templates das landingPages','',NULL),(11,'Páginas','/dashboard/permissoes/paginas','Página de gerenciamento das páginas do sistema','',NULL),(12,'Leads','/dashboard/landingPages/leads/','Página de leads das landings pages','',NULL),(13,'Selecionar Template','/dashboard/templates/selecionarTemplate/','Página para selecionar o template da landing page','',NULL),(14,'Configurar template','/dashboard/templates/configurarTemplate/','Página de configuração do template da landing page','',NULL),(15,'Metodo cadastrar usuário','/dashboard/usuarios/cadastrarUsuario','Metodo cadastro de usuario','',NULL),(16,'Metodo listar usuários','/dashboard/usuarios/listarUsuarios','Metodo para listar todos os usuários na página de usuários','',NULL),(17,'Permissão do nível de acesso','/dashboard/permissoes/permissaoNiveisAcesso/','página de permissão do nível de acesso','',NULL),(18,'Minha conta','/dashboard/usuarios/minhaconta','Página para gerenciar a conta do usuário logado','',NULL),(19,'Metodo atualizar status Landing Page','/landingpages/atualizar/atualizarStatusLandingPage','','',NULL),(20,'Metodo Editar Landing Page','/landingpages/atualizar/editarLandingPage','Metodo usado para subir o modal e editar os dados da landing page','',NULL),(21,'metodo excluir Landing Page','/landingpages/excluir/excluirLandingPages','Metodo usado para subir o modal e excluir a landing page','',NULL),(22,'Metodo atualizar status do Usuário','/dashboard/usuarios/atualizarStatusUsuario','Metodo usado para subir o modal e atualizar o status do usuário','',NULL),(23,'Metodo atualizar dados do Usuário','/dashboard/usuarios/atualizarDadosUsuario','Metodo usado para subir o modal e atualizar os dados do usuário','',NULL),(24,'Metodo excluir  Usuário','/dashboard/usuarios/deletarDadosUsuario','Metodo usado para subir o modal e excluir usuário	','',NULL),(25,'Metodo atualizar dados de configurações da la','/landingpages/atualizar/configuracoes','','',NULL),(26,'Metodo cadastrar template (Agência)','/dashboard/cadastro/cadastrarNovoTemplateLandingPage','','',NULL),(27,'Metodo atualizar status do template (Agência)','/dashboard/atualizar/atualizarStatusTemplates','','',NULL),(28,'Metodo que atualiza os dados da template (Age','/dashboard/atualizar/atualizarDadosTemplate','','',NULL),(29,'Metodo excluir template (agencia)','/dashboard/excluir/excluirTemplate','','',NULL),(30,'Metodo cadastrar novo nível de acesso','/dashboard/permissoes/criarNivelAcesso','','',NULL),(31,'Metodo editar nivel de acesso','/dashboard/permissoes/atualizarNivelAcesso','Botão que sobe o modal para editar o nivel','',NULL),(32,'Metodo excluir nivel de acesso','/dashboard/permissoes/deletarNivelAcesso','Botão que sobe o modal para excluir o nivel','',NULL),(33,'Botão associar página ao nivel de acesso','/dashboard/permissoes/associarRemoverPagina','','',NULL),(34,'Permissão MENU','/dashboard/permissoes/liberarBloquearMenu','','',NULL),(35,'Botão permissão','/dashboard/permissoes/liberarBloquearPermissao','','',NULL),(36,'Site','/dashboard/site','',' mdi mdi-web',NULL),(37,'slide/parceiros','/dashboard/site/slideParceiros','','',NULL),(38,'redesSociais/Colecao','/dashboard/site/redesSociaisColecao','','',NULL),(39,'Produtos admin','/dashboard/site/produtos','','',NULL),(40,'Newsletter admin','/dashboard/site/newsletter','','',NULL),(41,'SEO Site','/dashboard/site/seoSite','','',NULL),(42,'Catálogos','/dashboard/site/catalogos','','','site/site_admin/v_catalogos'),(43,'Fotos','/dashboard/site/fotos','','',' site/site_admin/v_fotos'),(44,'Vídeos','/dashboard/site/videos','','',' site/site_admin/v_videos'),(45,'Configs Tema','/dashboard/site/configuracoesTema','','','site/site_admin/v_configsTema'),(46,'Newsletter','/dashboard/newsletter','',' mdi mdi-email-mark-as-unread','site/site_admin/v_newsletter'),(47,'Grupos Newsletter','/dashboard/newsletter/grupos','','',''),(48,'emails newsletter','/dashboard/newsletter/emails','','',''),(49,'Templates newsletter','/dashboard/newsletter/templateEmail','','',''),(50,'Gerenciar template newsletter','/dashboard/newsletter/gerenciar_template','','',''),(51,'Disparos','/dashboard/newsletter/disparos','','',''),(52,'Grupos landingpage','/dashboard/gruposLandingPage','','',''),(53,'Dashboard','/','','',''),(54,'Painel','/painel','','',''),(55,'Newsletter (modulo)','/newsletter','','',''),(56,'Usuários (Modulo)','/usuarios','','',''),(57,'Planos (Modulo)','/planos','','',''),(58,'Contato site configs','/dashboard/site/contato','','','');
/*!40000 ALTER TABLE `pu_paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relatorios`
--

DROP TABLE IF EXISTS `relatorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `relatorios` (
  `id` int(11) NOT NULL,
  `nomePessoa` varchar(75) NOT NULL,
  `emailPessoa` varchar(45) NOT NULL,
  `nomeNewsletter` varchar(145) NOT NULL,
  `ipPessoa` varchar(45) DEFAULT NULL,
  `dispositivoPessoa` varchar(145) DEFAULT NULL,
  `navegadorPessoa` varchar(145) DEFAULT NULL,
  `SOPessoa` varchar(145) DEFAULT NULL,
  `modeloDispositivo` varchar(145) DEFAULT NULL,
  `dataHora` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relatorios`
--

LOCK TABLES `relatorios` WRITE;
/*!40000 ALTER TABLE `relatorios` DISABLE KEYS */;
/*!40000 ALTER TABLE `relatorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site` (
  `id_site` int(11) NOT NULL,
  PRIMARY KEY (`id_site`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site`
--

LOCK TABLES `site` WRITE;
/*!40000 ALTER TABLE `site` DISABLE KEYS */;
/*!40000 ALTER TABLE `site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_catalogos`
--

DROP TABLE IF EXISTS `site_catalogos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_catalogos` (
  `id_catalogos` int(11) NOT NULL AUTO_INCREMENT,
  `ativo_catalogos` enum('0','1') DEFAULT NULL,
  `nome_catalogos` varchar(256) DEFAULT NULL,
  `descricao_catalogos` longtext,
  `imagem_catalogos` varchar(145) DEFAULT NULL,
  `imagemMaior_catalogos` varchar(145) DEFAULT NULL,
  `seoImagem_catalogos` longtext,
  `link_catalogos` varchar(245) DEFAULT NULL,
  `pdf_catalogos` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_catalogos`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_catalogos`
--

LOCK TABLES `site_catalogos` WRITE;
/*!40000 ALTER TABLE `site_catalogos` DISABLE KEYS */;
INSERT INTO `site_catalogos` VALUES (1,'1','Catálogo 1','Catálogo 1','img_site_catalogo_menor_1402785710.jpg','img_site_catalogo_maior_1954472948.jpg','seo catalogo 111111111','catalogo_1','pdf_site_catalogo_1583966212.pdf'),(2,'1','Catálogo 2','catalogo 2','img_site_catalogo_menor_1211695106.jpg','img_site_catalogo_maior_1453171363.jpg','seo catalogo 2','catalogo_2','img_site_catalogo_pdf_149806445.pdf'),(3,'1','Catálogo 3','Catálogo 3\r\n','img_site_catalogo_menor_895618.jpg','img_site_catalogo_maior_2024175717.jpg','seo catalogo 3','catalogo_3','img_site_catalogo_pdf_281290781.pdf'),(4,'1','Catálogo 4','Catálogo 4','img_site_catalogo_menor_830356741.jpg','img_site_catalogo_maior_229619615.jpg','seo catalogo 4','catalogo_4','img_site_catalogo_pdf_1520832416.pdf'),(5,'1','Catálogo 5','Catálogo 5','img_site_catalogo_menor_345228610.jpg','img_site_catalogo_maior_1212786124.jpg','seo catalogo 5','catalogo_5','img_site_catalogo_pdf_355033068.pdf'),(6,'1','Catálogo 6','Catálogo 6','img_site_catalogo_menor_1020245577.jpg','img_site_catalogo_maior_1472688442.jpg','seo catalogo 6','catalogo_6','img_site_catalogo_pdf_1594743474.pdf');
/*!40000 ALTER TABLE `site_catalogos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_colecao`
--

DROP TABLE IF EXISTS `site_colecao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_colecao` (
  `id_colecao` int(11) NOT NULL AUTO_INCREMENT,
  `ativo_colecao` enum('0','1') DEFAULT NULL,
  `imagem_colecao` varchar(145) DEFAULT NULL,
  `link_colecao` varchar(256) DEFAULT NULL,
  `seo_colecao` text,
  `texto_colecao` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_colecao`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_colecao`
--

LOCK TABLES `site_colecao` WRITE;
/*!40000 ALTER TABLE `site_colecao` DISABLE KEYS */;
INSERT INTO `site_colecao` VALUES (1,'1','summer_img.jpg','http://seriesparaassistironline.org/','seo colecao','Coleção 2019');
/*!40000 ALTER TABLE `site_colecao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_configstema`
--

DROP TABLE IF EXISTS `site_configstema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_configstema` (
  `id_configsTema` int(11) NOT NULL AUTO_INCREMENT,
  `corLinksMenu_configsTema` varchar(10) DEFAULT NULL,
  `corLinksHoverMenu_configsTema` varchar(10) DEFAULT NULL,
  `corIconeRSMenu_configsTema` varchar(10) DEFAULT NULL,
  `corIconeRSHoverMenu_configsTema` varchar(10) DEFAULT NULL,
  `corBotaoSubirTopo_configsTema` varchar(10) DEFAULT NULL,
  `corBotaoSubirTopoHover_configsTema` varchar(10) DEFAULT NULL,
  `corIconeSubir_configsTema` varchar(10) DEFAULT NULL,
  `corIconeSubirHover_configsTema` varchar(10) DEFAULT NULL,
  `CorFundoFooter_configsTema` varchar(10) DEFAULT NULL,
  `CorTextoFooter_configsTema` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_configsTema`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_configstema`
--

LOCK TABLES `site_configstema` WRITE;
/*!40000 ALTER TABLE `site_configstema` DISABLE KEYS */;
INSERT INTO `site_configstema` VALUES (1,'#5a4736','#c9781f','#47cebe','#794512','#9b883c','#666326','#000000','#11ff2f','#ca0005','#b3f279');
/*!40000 ALTER TABLE `site_configstema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_contador`
--

DROP TABLE IF EXISTS `site_contador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_contador` (
  `id_contador` int(11) NOT NULL AUTO_INCREMENT,
  `ip_contador` varchar(45) DEFAULT NULL,
  `cidade_contador` varchar(65) DEFAULT NULL,
  `estado_contador` varchar(45) DEFAULT NULL,
  `dispositivo_contador` varchar(45) DEFAULT NULL,
  `navegador_contador` varchar(45) DEFAULT NULL,
  `SO_contador` varchar(45) DEFAULT NULL,
  `data_contador` varchar(45) DEFAULT NULL,
  `hora_contador` varchar(45) DEFAULT NULL,
  `tempo_contador` varchar(45) DEFAULT NULL,
  `token_contador` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id_contador`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_contador`
--

LOCK TABLES `site_contador` WRITE;
/*!40000 ALTER TABLE `site_contador` DISABLE KEYS */;
INSERT INTO `site_contador` VALUES (1,'127.0.0.1','','','0','Chrome','Windows','20200312','1109',NULL,'lhavlaf400p740prf0tqrmlhdbtuhs5h'),(2,'127.0.0.1','','','0','Chrome','Windows','20200312','1118',NULL,'elhgu26o84ieacbpofckffk8aepskjrv'),(3,'127.0.0.1','','','0','Chrome','Windows','20200312','1123',NULL,'0lpoqjojtdmb1sh87ur4dajumf7d87dl'),(4,'127.0.0.1','','','0','Chrome','Windows','20200312','1313',NULL,'h54u6tog0b36rj65fesvetdpr7klcuq5'),(5,'127.0.0.1','','','0','Chrome','Windows','20200312','1344',NULL,'5fn8q72i1kn33am16aa2d1bk0eh6l9ug'),(6,'127.0.0.1','','','0','Chrome','Windows','20200312','1357',NULL,'0dqpscks0ust1evdlghhi40q3vhf7i7r'),(7,'127.0.0.1','','','0','Chrome','Windows','20200313','1130',NULL,'esr8jum4nak550jdj302kb7lmsdjjvd1'),(8,'127.0.0.1','','','0','Chrome','Windows','20200313','1239',NULL,'rind7uas4q6hq32eilm390935kqbjhp2'),(9,'127.0.0.1','','','0','Chrome','Windows','20200313','1615',NULL,'o2p769q03smqtqt9lrbeedherei22h7h'),(10,'127.0.0.1','','','0','Chrome','Windows','20200316','1112',NULL,'02d6psni1nmg0ji73llecdr9b4eaf880'),(11,'127.0.0.1','','','0','Chrome','Windows','20200316','1122',NULL,'c31sm2q1odvo1pku2v250k8dvhqg6g48'),(12,'127.0.0.1','','','0','Chrome','Windows','20200316','1133',NULL,'lm1o72jimqhrk2u9alsseu2adpu6ocbb'),(13,'127.0.0.1','','','0','Chrome','Windows','20200317','1716',NULL,'7jk2udabvsul6k8eloieuqkfpj6jqm61'),(14,'127.0.0.1','','','0','Chrome','Windows','20200317','1746',NULL,'eug9m9c9158luigg59ae6eealvm1ec2v'),(15,'127.0.0.1','','','0','Chrome','Windows','20200317','1845',NULL,'623hbp9bh3pvm3ut05eudh82g2sb2bj5'),(16,'127.0.0.1','','','0','Opera','Windows','20200318','1220',NULL,'acugenlofpai4op50j1m1i2q9kfrnl1l'),(17,'127.0.0.1','','','0','Chrome','Windows','20200318','1220',NULL,'nras496k2sqhbtmujqtm0219plu72mtb'),(18,'127.0.0.1','Goiania','Goias','0','Chrome','Windows','20200318','1538',NULL,'4puv8jjht4lth20clt95s25v4ufv5fru'),(19,'127.0.0.1','','','0','Chrome','Windows','20200318','1602',NULL,'3kkbeaepia33lqennts92qra6d3dkvek'),(20,'127.0.0.1','','','0','Chrome','Windows','20200318','1647',NULL,'odlv6ipqmm1c9oedtkh2j5jj733rsbsn'),(21,'127.0.0.1','','','0','Chrome','Windows','20200321','0023',NULL,'r9cvo9pcfsorvh8e3a7e0plqfk89aoc8'),(22,'127.0.0.1','','','0','Chrome','Windows','20200321','0308',NULL,'e6usru2uc9dm4ers4d8jnig99ftimfna'),(23,'127.0.0.1','','','0','Chrome','Windows','20200321','0313',NULL,'gilohs9bejsa2bir0kascrq5hmgi1k45'),(24,'127.0.0.1','','','0','Chrome','Windows','20200321','0340',NULL,'ubc7id61prnsuuac7qb3uq2roko8jnkd'),(25,'127.0.0.1','','','0','Chrome','Windows','20200322','0051',NULL,'bfb6e0df0ui0l0jnavjfics74f56rstr'),(26,'127.0.0.1','','','0','Chrome','Windows','20200322','2238',NULL,'72q2vhbo6f5rvekp30grll0jpva0as61'),(27,'127.0.0.1','','','0','Chrome','Windows','20200323','0049',NULL,'3i28f9p0sbvu76csv9cogjr55kmlsjh8'),(28,'127.0.0.1','','','0','Chrome','Windows','20200324','1407',NULL,'san41r1l9nrnj5jmilm0fl5geg42q1j8'),(29,'127.0.0.1','','','0','Chrome','Windows','20200325','0102',NULL,'ja2p8o4foajuke14ealppllth1c8lbdf'),(30,'127.0.0.1','','','0','Chrome','Windows','20200326','0133',NULL,'2cnj8kdbrrem8t1vnr43m1aavqj3hihf'),(31,'127.0.0.1','','','0','Chrome','Windows','20200326','0935',NULL,'34j73sc8eqdp6sq9fgpa1v0fpe93s3ll'),(32,'127.0.0.1','','','0','Chrome','Windows','20200326','0959',NULL,'bl546rdqotnvpvjmk728p5qmuem7qb52'),(33,'127.0.0.1','','','0','Chrome','Windows','20200326','2034',NULL,'kknjd07n61o3bhbpcfflutmd5cu91rbs'),(34,'127.0.0.1','','','0','Chrome','Windows','20200326','2041',NULL,'gm7ggm6m9bbq0cclofkafc9194b8ra69'),(35,'127.0.0.1','','','0','Chrome','Windows','20200326','2045',NULL,'ujv0ckpdvf98ak67kbu3do50ukta3vc7'),(36,'127.0.0.1','','','0','Chrome','Windows','20200326','2052',NULL,'3mn6d9j5cvv5khodgn3lj2i3fpkt91n5'),(37,'127.0.0.1','','','0','Chrome','Windows','20200326','2058',NULL,'trm5qm17v8t08epcrqs4tavvr0it120i'),(38,'127.0.0.1','','','0','Chrome','Windows','20200326','2125',NULL,'p0d0ffbu65lamgfltvuh5f3e0qrrjt5b'),(39,'127.0.0.1','','','0','Chrome','Windows','20200326','2130',NULL,'0rakqmdvah0nm4jbkigidqu4t4cdriui'),(40,'127.0.0.1','','','0','Chrome','Windows','20200326','2135',NULL,'kufjaplj0iqsj66tf09mkdcpmcg0qjbc'),(41,'127.0.0.1','','','0','Chrome','Windows','20200326','2143',NULL,'mk7dpsih188bsef2p4kjgos22l26vvd9'),(42,'127.0.0.1','','','0','Chrome','Windows','20200326','2148',NULL,'32nrv9lftfkrc6f7mes1oh0uuog3ng70'),(43,'127.0.0.1','','','0','Chrome','Windows','20200326','2239',NULL,'n23t7ou1fqapianf2bus2b7qki17lsmd'),(44,'127.0.0.1','','','0','Chrome','Windows','20200326','2245',NULL,'hbds1rj7s223b84pv2n9n01lbp35stom'),(45,'127.0.0.1','','','0','Chrome','Windows','20200326','2250',NULL,'r266bvelij0qktj9qp2c996j70s9hlkt'),(46,'127.0.0.1','','','0','Chrome','Windows','20200327','0312',NULL,'100pss31558iov0okkt4le3nad56vedu'),(47,'127.0.0.1','','','0','Chrome','Windows','20200327','1139',NULL,'mbmcu7l8aslmpn6llfanrjnt1ds6ur0b'),(48,'127.0.0.1','','','0','Chrome','Windows','20200327','2140',NULL,'t1v2j2dgdabme4mtg8nn1lk3cqelnbbc'),(49,'127.0.0.1','','','0','Chrome','Windows','20200328','0047',NULL,'qtgat37m7ii2dsbu7g2533boiiui7stq'),(50,'127.0.0.1','','','0','Chrome','Windows','20200329','0022',NULL,'d8v2p3tdv01k18rvuasrlkp73ggn08ic'),(51,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200331','1120',NULL,'c33vvu43tj0c4p21tfdqdt25534st534'),(52,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200402','1410',NULL,'383b5se36gbiq0dllhtgmb7cc5hke2n0'),(53,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200403','1551',NULL,'huhtrlkr3ub852h5fhknjbakqn4qbbe5'),(54,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200403','2005',NULL,'ne3s24ppb7vrjq6hag15aanf9b22q1t6'),(55,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200406','1124',NULL,'5hem35n1ah5g4p6rtg58udpcma3f1ktk'),(56,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200407','1505',NULL,'361a7smblee0se8ojjvobi07cf6e6oec'),(57,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200409','1405',NULL,'ifmtb1f4gr5adcal1eoibo1opn4rv3vj'),(58,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200409','1439',NULL,'vha7supvfusloa48cfi6o79qr86rhd3e'),(59,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200413','1246',NULL,'tukilesmc009dnmh8r0chnpp4r1mglpg'),(60,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200415','1901',NULL,'9pm4kskd0tgh54s5iee10p7s5a3sgfch'),(61,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200415','1948',NULL,'vo605lp432lbfautaa764hfrk2d940vs'),(62,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200416','1322',NULL,'h2p8mi1oq25uc3lggenvq0078ecnqnh4'),(63,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200416','1753',NULL,'iphau5ufl88sv5u9ufh28v9q34r6u41l'),(64,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200423','1649',NULL,'9h5j7d19fhp6nnaurg0789o9je3sinqs'),(65,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200423','1702',NULL,'gj85i2u84jk5adifhqdeeu732aa2avf7'),(66,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200424','1128',NULL,'nanh2h9dlaq0u2s6ruog94ci88k0i3ba'),(67,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200428','1930',NULL,'i5a4ggu43mjalicbmnqnoikm5bml7jl4'),(68,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200428','1942',NULL,'36sesh65cvjgum28gu1u63lhkc9q1ueh'),(69,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200429','1751',NULL,'vbtu163ajuk3k276qbl7eh2kcn1j02bb'),(70,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200505','1117',NULL,'00ac6g6jsfc40rvt72mmgu5g0gdk9g3k'),(71,'177.30.111.114','Aparecida de Goiania','Goias','0','Opera','Windows','20200506','1102',NULL,'h8gd9voonrehsqsffv1q6ggafvgjus8v'),(72,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200506','1239',NULL,'q6j703aheumup60j2ktd6rql1ttl7dev'),(73,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200506','1923',NULL,'c7jkcmlptbgj544tj7uelf5h34fipt6c'),(74,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200507','1155',NULL,'e9bs58dmf97q2kvrhufha0ch3an3fb5e'),(75,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200508','1159',NULL,'5tqbs7u66g4gbc137b4lvmom2p91g8ii'),(76,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200508','1159',NULL,'u7resgr445bvmv11pi7hu052o6van3mb'),(77,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200512','1852',NULL,'u8bgftb6m46h92mitendntvk655kibco'),(78,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200512','1852',NULL,'m5oflolkm2h553skspqm520iiulq4duc'),(79,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200513','1109',NULL,'b3chvg8vuk1e7qu7tn9s8tvgirl7ihrv'),(80,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200513','1109',NULL,'1tbfmk72vcodme5j4dsnf8rjf9013k0e'),(81,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200513','1620',NULL,'94gtuin1tj586cuv2ags51q7l61nj2lj'),(82,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200514','1119',NULL,'75hhfvpmb1bfc0ttp09ppcfjc6j5lclq'),(83,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200515','1130',NULL,'r52n6hu5fdt15lapnmdh9ve22dam03gb'),(84,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200515','1130',NULL,'1mlugfdl0sud61a4ocmo2ea1porfo0tk'),(85,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200515','1805',NULL,'bvjq71sej6sq7o8mvads4jsipqg0gpsb'),(86,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200519','1112',NULL,'2gp7nv4ddnaua4afo7l7i15nu4ffu2h7'),(87,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200522','1809',NULL,'k501c3mqlocgosq7k9qp46j71i96h0vk'),(88,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200522','1812',NULL,'4rnec2tsqfshgq93tm69cj9io8m5e913'),(89,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200522','1821',NULL,'2p7b2kltarvei5fej9c46e8kvh4f4hsg'),(90,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200522','1844',NULL,'ulqir0u7kkuc364tojt3qqd6ievrr09q'),(91,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200525','2006',NULL,'bd5heqq2l7h0plqr8dm7973rsgn8e2ft'),(92,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200525','2007',NULL,'22v8gq7v9fo02jcflhi90shosqe087bi'),(93,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200526','1135',NULL,'spi4e7g9n8u63b3msvmmqr4l3167ii7h'),(94,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200526','1144',NULL,'oggj6s5rtgem8oj3lbc5d4n1h82ejfut'),(95,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200526','1516',NULL,'uqu8m83pp5646of9a7nv413mlgkolttp'),(96,'177.30.111.114','Goiania','Goias','0','Opera','Windows','20200527','1536',NULL,'dmmai2jft0cup0vd6ndn0ka8ivma9roa'),(97,'177.30.111.114','Aparecida de Goiania','Goias','0','Opera','Windows','20200527','1554',NULL,'bddfejllbkeh58m06ksjrua5a4aps414'),(98,'177.30.111.114','Goiania','Goias','0','Opera','Windows','20200527','1603',NULL,'dgiv5slkfoovqdcbr35vort34527rm69'),(99,'177.30.111.114','Aparecida de Goiania','Goias','0','Opera','Windows','20200527','1612',NULL,'ofr0btvqd743b08hge749e2q6pk75563'),(100,'177.30.111.114','Aparecida de Goiania','Goias','0','Opera','Windows','20200527','1629',NULL,'q544a5cbt67rlqh6huk0ggaud4cfrq6t'),(101,'177.30.111.114','Aparecida de Goiania','Goias','0','Opera','Windows','20200527','1852',NULL,'iglbf74rcui95fjh6u38ls6a8ea6uhp9'),(102,'177.30.111.114','Aparecida de Goiania','Goias','0','Opera','Windows','20200527','1925',NULL,'362f5s63jf3l4tka05r0i8dulnq5bg03'),(103,'177.30.111.114','Goiania','Goias','0','Chrome','Windows','20200528','1744',NULL,'9s3a4lmkqqud0t6gvnalqgtg6djiuk1m'),(104,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200529','1153',NULL,'na9r3dcvocjftsdbrdr4qgt8q2gme2es'),(105,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200529','0945',NULL,'evsta8b6ojlei79of55asj9rc7d9tdgd'),(106,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200529','0949',NULL,'eg777i0qridednuclnmg5c759n2d85i6'),(107,'177.30.111.114','Aparecida de Goiania','Goias','0','Chrome','Windows','20200529','0954',NULL,'2djhnqobeadhn65u2vk5j8ou5ho715tr');
/*!40000 ALTER TABLE `site_contador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_contato_configs`
--

DROP TABLE IF EXISTS `site_contato_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_contato_configs` (
  `id_contato` int(11) NOT NULL AUTO_INCREMENT,
  `host_contato` varchar(145) DEFAULT NULL,
  `port_contato` varchar(5) DEFAULT NULL,
  `username_contato` varchar(145) DEFAULT NULL,
  `password_contato` varchar(145) DEFAULT NULL,
  `msgRemetente_contato` varchar(512) DEFAULT NULL,
  `debug_contato` varchar(3) DEFAULT NULL,
  `SMTPSecure_contato` varchar(5) DEFAULT NULL,
  `SMTPAuth_contato` varchar(15) DEFAULT NULL,
  `addAddress_contato` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_contato`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_contato_configs`
--

LOCK TABLES `site_contato_configs` WRITE;
/*!40000 ALTER TABLE `site_contato_configs` DISABLE KEYS */;
INSERT INTO `site_contato_configs` VALUES (1,'mail.lekalekids.com','465','naoresponda@lekalekids.com','123@mudar!@!@','Mensagem automática contato LêkalêKids','0','ssl','true','josheffer@gmail.com');
/*!40000 ALTER TABLE `site_contato_configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_fotos`
--

DROP TABLE IF EXISTS `site_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_fotos` (
  `id_fotos` int(11) NOT NULL AUTO_INCREMENT,
  `ativo_fotos` int(11) DEFAULT NULL,
  `nome_fotos` varchar(145) DEFAULT NULL,
  `imagem_fotos` varchar(145) DEFAULT NULL,
  `seo_fotos` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id_fotos`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_fotos`
--

LOCK TABLES `site_fotos` WRITE;
/*!40000 ALTER TABLE `site_fotos` DISABLE KEYS */;
INSERT INTO `site_fotos` VALUES (1,1,'Teste 1','img_site_fotos_1304697593.jpg','oi oi oi'),(2,1,'teste 2','pexels-photo-305070.jpeg','tim tim tim'),(3,1,'teste 3','pexels-photo-853168.jpeg','vivo vivo');
/*!40000 ALTER TABLE `site_fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_leads`
--

DROP TABLE IF EXISTS `site_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_leads` (
  `id_leads` int(11) NOT NULL AUTO_INCREMENT,
  `nome_leads` varchar(145) DEFAULT NULL,
  `telefone_leads` varchar(45) DEFAULT NULL,
  `email_leads` varchar(90) DEFAULT NULL,
  `id_lista` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_leads`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_leads`
--

LOCK TABLES `site_leads` WRITE;
/*!40000 ALTER TABLE `site_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_menu_links`
--

DROP TABLE IF EXISTS `site_menu_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_menu_links` (
  `id_links` int(11) NOT NULL AUTO_INCREMENT,
  `nome_link_site` varchar(45) DEFAULT NULL,
  `ativo_link_site` enum('0','1') DEFAULT NULL,
  `rota_link_site` varchar(145) DEFAULT NULL,
  `controller_link_site` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_links`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_menu_links`
--

LOCK TABLES `site_menu_links` WRITE;
/*!40000 ALTER TABLE `site_menu_links` DISABLE KEYS */;
INSERT INTO `site_menu_links` VALUES (1,'Catálogos','1','catalogos','v_link1'),(2,'Fotos','1','fotos','v_link2'),(3,'Vídeos','1','videos','v_link3'),(4,'Facebook','0','link 4','v_link4'),(5,'Instagram','0','link5','v_link5'),(6,'Contato','1','contato','v_contato');
/*!40000 ALTER TABLE `site_menu_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_menu_redessociais`
--

DROP TABLE IF EXISTS `site_menu_redessociais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_menu_redessociais` (
  `id_linksMenu` int(11) NOT NULL AUTO_INCREMENT,
  `facebook_link` varchar(256) DEFAULT NULL,
  `facebook_ativo` enum('0','1') DEFAULT NULL,
  `instagram_link` varchar(256) DEFAULT NULL,
  `instagram_ativo` enum('0','1') DEFAULT NULL,
  `whatsapp1_numero` bigint(11) DEFAULT NULL,
  `whatsapp1_frase` varchar(512) DEFAULT NULL,
  `whatsapp1_ativo` enum('0','1') DEFAULT NULL,
  `whatsapp2_numero` bigint(11) DEFAULT NULL,
  `whatsapp2_frase` varchar(512) DEFAULT NULL,
  `whatsapp2_ativo` enum('0','1') DEFAULT NULL,
  `whatsapp3_numero` bigint(11) DEFAULT NULL,
  `whatsapp3_frase` varchar(512) DEFAULT NULL,
  `whatsapp3_ativo` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_linksMenu`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_menu_redessociais`
--

LOCK TABLES `site_menu_redessociais` WRITE;
/*!40000 ALTER TABLE `site_menu_redessociais` DISABLE KEYS */;
INSERT INTO `site_menu_redessociais` VALUES (1,'https://facebook.com','1','https://instagram.com','1',62993998725,'E ai, blz','1',62993998725,'E aiiiiii','1',62993998725,'e aiii tiooo','1');
/*!40000 ALTER TABLE `site_menu_redessociais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_newsletter`
--

DROP TABLE IF EXISTS `site_newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_newsletter` (
  `id_news` int(11) NOT NULL AUTO_INCREMENT,
  `ativo_news` enum('0','1') DEFAULT NULL,
  `email_news` varchar(145) DEFAULT NULL,
  `senha_news` varchar(145) DEFAULT NULL,
  `lista_news` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_news`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_newsletter`
--

LOCK TABLES `site_newsletter` WRITE;
/*!40000 ALTER TABLE `site_newsletter` DISABLE KEYS */;
INSERT INTO `site_newsletter` VALUES (1,'1','josheffer@gmail.com','$2y$10$wMWc2qxyBtlf1J2TuJeUDOtuyxZcikIbUHEvz0v3TDXj5Ym9Ob/yC',2);
/*!40000 ALTER TABLE `site_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_parceiros`
--

DROP TABLE IF EXISTS `site_parceiros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_parceiros` (
  `id_parceiros` int(11) NOT NULL,
  `ativoGeral_parceiros` enum('0','1') DEFAULT NULL,
  `imagem1_parceiros` varchar(145) DEFAULT NULL,
  `imagem2_parceiros` varchar(145) DEFAULT NULL,
  `imagem3_parceiros` varchar(145) DEFAULT NULL,
  `imagem4_parceiros` varchar(145) DEFAULT NULL,
  `imagem5_parceiros` varchar(145) DEFAULT NULL,
  `imagem6_parceiros` varchar(145) DEFAULT NULL,
  `imagem7_parceiros` varchar(145) DEFAULT NULL,
  `imagem8_parceiros` varchar(145) DEFAULT NULL,
  `imagem9_parceiros` varchar(145) DEFAULT NULL,
  `imagem10_parceiros` varchar(145) DEFAULT NULL,
  `imagem11_parceiros` varchar(145) DEFAULT NULL,
  `imagem12_parceiros` varchar(145) DEFAULT NULL,
  `ativo1_parceiros` enum('0','1') DEFAULT '1',
  `ativo2_parceiros` enum('0','1') DEFAULT '1',
  `ativo3_parceiros` enum('0','1') DEFAULT '1',
  `ativo4_parceiros` enum('0','1') DEFAULT '1',
  `ativo5_parceiros` enum('0','1') DEFAULT '1',
  `ativo6_parceiros` enum('0','1') DEFAULT '1',
  `ativo7_parceiros` enum('0','1') DEFAULT '1',
  `ativo8_parceiros` enum('0','1') DEFAULT '1',
  `ativo9_parceiros` enum('0','1') DEFAULT '1',
  `ativo10_parceiros` enum('0','1') DEFAULT '1',
  `ativo11_parceiros` enum('0','1') DEFAULT '1',
  `ativo12_parceiros` enum('0','1') DEFAULT '1',
  `seo_parceiros1` text,
  `seo_parceiros2` text,
  `seo_parceiros3` text,
  `seo_parceiros4` text,
  `seo_parceiros5` text,
  `seo_parceiros6` text,
  `seo_parceiros7` text,
  `seo_parceiros8` text,
  `seo_parceiros9` text,
  `seo_parceiros10` text,
  `seo_parceiros11` text,
  `seo_parceiros12` text,
  PRIMARY KEY (`id_parceiros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_parceiros`
--

LOCK TABLES `site_parceiros` WRITE;
/*!40000 ALTER TABLE `site_parceiros` DISABLE KEYS */;
INSERT INTO `site_parceiros` VALUES (1,'1','img_site_parceiros_615207631.jpg','img_site_parceiros_1223204660.jpg','img_site_parceiros_62890924.jpg','img_site_parceiros_1420942677.jpg','img_site_parceiros_99931609.jpg','img_site_parceiros_791311473.jpg','img_site_parceiros_305184426.jpg','img_site_parceiros_955390279.jpg','img_site_parceiros_51593481.jpg','img_site_parceiros_97963233.jpg','img_site_parceiros_298014847.jpg','img_site_parceiros_140738410.jpg','1','1','1','1','1','1','1','1','1','1','1','1','momi','PETIT CHERIE','ANIMÊ','seo parceiro 444','asdasdasd','asdasdasd','asdasdasd','asdasdasd','asdasd','asdasd','asdasd','asdasd');
/*!40000 ALTER TABLE `site_parceiros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_produtos`
--

DROP TABLE IF EXISTS `site_produtos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_produtos` (
  `id_produtos` int(11) NOT NULL,
  `ativo_produtos` enum('0','1') DEFAULT NULL,
  `ativo_produto1` enum('0','1') DEFAULT NULL,
  `imagem_produto1` varchar(145) DEFAULT NULL,
  `nome_produto1` varchar(256) DEFAULT NULL,
  `link_produto1` varchar(256) DEFAULT NULL,
  `imagem_produto2` varchar(145) DEFAULT NULL,
  `nome_produto2` varchar(256) DEFAULT NULL,
  `ativo_produto2` enum('0','1') DEFAULT NULL,
  `link_produto2` varchar(256) DEFAULT NULL,
  `ativo_produto3` enum('0','1') DEFAULT NULL,
  `imagem_produto3` varchar(145) DEFAULT NULL,
  `nome_produto3` varchar(256) DEFAULT NULL,
  `link_produto3` varchar(256) DEFAULT NULL,
  `ativo_produto4` enum('0','1') DEFAULT NULL,
  `imagem_produto4` varchar(145) DEFAULT NULL,
  `nome_produto4` varchar(256) DEFAULT NULL,
  `link_produto4` varchar(256) DEFAULT NULL,
  `seo_produto1` text,
  `seo_produto2` text,
  `seo_produto3` text,
  `seo_produto4` text,
  PRIMARY KEY (`id_produtos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_produtos`
--

LOCK TABLES `site_produtos` WRITE;
/*!40000 ALTER TABLE `site_produtos` DISABLE KEYS */;
INSERT INTO `site_produtos` VALUES (1,'1','1','img_site_produtos_1236140191.jpg','Produto 1','//google.com','2.png','teste 2','1','//google.com','1','3.png','teste 3','//google.com','1','4.png','teste 4','//google.com','Seo 111111','Seo 2222222','Seo 3333333','Seo 4444444');
/*!40000 ALTER TABLE `site_produtos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_redesociais`
--

DROP TABLE IF EXISTS `site_redesociais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_redesociais` (
  `id_redesSociais` int(11) NOT NULL,
  `ativo_redesSociais` enum('0','1') DEFAULT NULL,
  `ativo_redeSocial1` enum('0','1') DEFAULT NULL,
  `frase_redeSocial1` varchar(145) DEFAULT NULL,
  `link_redeSocial1` varchar(256) DEFAULT NULL,
  `imagem_redeSocial1` varchar(145) DEFAULT NULL,
  `ativo_redeSocial2` enum('0','1') DEFAULT NULL,
  `frase_redeSocial2` varchar(145) DEFAULT NULL,
  `link_redeSocial2` varchar(256) DEFAULT NULL,
  `imagem_redeSocial2` varchar(145) DEFAULT NULL,
  `ativo_redeSocial3` enum('0','1') DEFAULT NULL,
  `frase_redeSocial3` varchar(145) DEFAULT NULL,
  `link_redeSocial3` varchar(256) DEFAULT NULL,
  `imagem_redeSocial3` varchar(145) DEFAULT NULL,
  `ativo_redeSocial4` enum('0','1') DEFAULT NULL,
  `frase_redeSocial4` varchar(145) DEFAULT NULL,
  `link_redeSocial4` varchar(256) DEFAULT NULL,
  `imagem_redeSocial4` varchar(145) DEFAULT NULL,
  `seo_redeSocial1` text,
  `seo_redeSocial2` text,
  `seo_redeSocial3` text,
  `seo_redeSocial4` text,
  PRIMARY KEY (`id_redesSociais`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_redesociais`
--

LOCK TABLES `site_redesociais` WRITE;
/*!40000 ALTER TABLE `site_redesociais` DISABLE KEYS */;
INSERT INTO `site_redesociais` VALUES (1,'1','1','Fale com uma vendedora da loja!','https://api.whatsapp.com/send?phone=5562992857966&text=e%20aiii%20tiooo','img_site_social_704127766.jpg','1','Siga nosso Instagram cheio de novidades!','https://api.whatsapp.com/send?phone=5562992857966&text=e%20aiii%20tiooo','img_site_social_878079438.jpg','1','Curta nossa página no Facebook','https://api.whatsapp.com/send?phone=5562992857966&text=e%20aiii%20tiooo','img_site_social_752149822.jpg','1','Conheça nosso canal','https://api.whatsapp.com/send?phone=5562992857966&text=e%20aiii%20tiooo','img_site_social_523939980.jpg','SEO rede social 11111','SEO rede social 22222','SEO rede social 333','SEO rede social 4444');
/*!40000 ALTER TABLE `site_redesociais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_seo`
--

DROP TABLE IF EXISTS `site_seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_seo` (
  `id_site_seo` int(11) NOT NULL AUTO_INCREMENT,
  `site_nomeSite` text,
  `site_corTema` varchar(10) DEFAULT NULL,
  `site_css` longtext,
  `site_js` longtext,
  `site_js_body` longtext,
  `site_keywords` text,
  `site_description` text,
  `site_description2` text,
  `site_description3` text,
  `site_SEO_imagem` varchar(145) DEFAULT NULL,
  `site_coordenadasMaps` varchar(80) DEFAULT NULL,
  `site_cidade` varchar(80) DEFAULT NULL,
  `site_estado` varchar(80) DEFAULT NULL,
  `site_googleVerificacao` text,
  PRIMARY KEY (`id_site_seo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_seo`
--

LOCK TABLES `site_seo` WRITE;
/*!40000 ALTER TABLE `site_seo` DISABLE KEYS */;
INSERT INTO `site_seo` VALUES (1,'LêkalêKids para meninas e meninos | Loja infantil em Goiânia, (62) 98139-0006 - (62) 98103-9172 - (62) 98218-1617, Vestidos, Calças, Óculos, Camisetas,  Bermudas, Cintos e muito mais..','#a47b4c','','','','LêkalêKids, LekaleKids, LekaleKids, Moda infantil em Goiânia, Roupas infantil, Roupas infantil em Goiânia, Roupas de crianças em goiania, Roupas de crianças, Moda jovem Goiânia, Roupas para meninas em Goiânia, Roupas para meninas, Roupas para meninos em Goiânia, Roupas para meninos, Infantil menino, Infantil menina, Óculos para meninos em Goiânia, Óculos para meninas em Goiânia, Sapatos infantil em Goiânia, Sapatos para meninas, Sapatos para meninos, Sapatos para meninas em Goiânia, Sapatos para meninos em Goiânia, Menor preço do Brasil, Loja em Goiânia, Loja LêkalêKids Setor Sul em Goiânia','LêkalêKids moda infantil em Goiânia,  Produtos exclusivos em Goiânia, Oferece as melhores tendências de marcas de moda infantil para meninos e meninas de 2 a 18 anos! Loja de varejo em Goiânia, Envio para o Exterior, Surpreenda seus filhos  com um presente especial, Camisetas, Pijamas, Conjuntos para crianças e moda Teen, Vestidos infantil, conjunto infantil, calças para meninos e meninas, para meninos e meninas, Coleção Verão, Coleção Outono, Coleção Inverno, Coleção Primavera, estilo e conforto, Roupas que inspiram beleza e autenticidade.','LêkalêKids moda infantil em Goiânia,  Produtos exclusivos em Goiânia, Oferece as melhores tendências de marcas de moda infantil para meninos e meninas de 1 a 17 anos! Surpreenda seus filhos  com um presente especial, Camisetas, Pijamas, Conjuntos para crianças e moda Teen, Vestidos infantil, conjunto infantil, calças para meninos e meninas, para meninos e meninas, Coleção Verão, Coleção Outono, Coleção Inverno, Coleção Primavera, estilo e conforto, Roupas que inspiram beleza e autenticidade.','LêkalêKids moda infantil em Goiânia,  Produtos exclusivos em Goiânia, Oferece as melhores tendências de marcas de moda infantil para meninos e meninas de 1 a 17 anos! Surpreenda seus filhos  com um presente especial, Camisetas, Pijamas, Conjuntos para crianças e moda Teen, Vestidos infantil, conjunto infantil, calças para meninos e meninas, para meninos e meninas, Coleção Verão, Coleção Outono, Coleção Inverno, Coleção Primavera, estilo e conforto, Roupas que inspiram beleza e autenticidade.','img_site_SEO_1883883592.jpg','-16.684301,-49.249649','Goiânia, Goiás','GO-BR',NULL);
/*!40000 ALTER TABLE `site_seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_slide`
--

DROP TABLE IF EXISTS `site_slide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_slide` (
  `id_slide` int(1) NOT NULL AUTO_INCREMENT,
  `ativo_slideGeral` enum('0','1') DEFAULT NULL,
  `ativo_slide1` enum('0','1') DEFAULT NULL,
  `imagem_slide1` varchar(145) DEFAULT NULL,
  `titulo_slide1` varchar(256) DEFAULT NULL,
  `descricao_slide1` varchar(512) DEFAULT NULL,
  `texto_botaoSlide1` varchar(45) DEFAULT NULL,
  `corTituloDescricao_slide1` varchar(10) DEFAULT NULL,
  `corTexto_botaoSlide1` varchar(10) DEFAULT NULL,
  `corFundo_botaoSlide1` varchar(10) DEFAULT NULL,
  `corTextoHover_botaoSlide1` varchar(10) DEFAULT NULL,
  `corFundoHover_botaoSlide1` varchar(10) DEFAULT NULL,
  `link_botaoSlide1` varchar(256) DEFAULT NULL,
  `ativo_botaoSlide1` enum('0','1') DEFAULT NULL,
  `ativo_slide2` enum('0','1') DEFAULT NULL,
  `imagem_slide2` varchar(145) DEFAULT NULL,
  `titulo_slide2` varchar(256) DEFAULT NULL,
  `descricao_slide2` varchar(512) DEFAULT NULL,
  `corTituloDescricao_slide2` varchar(10) DEFAULT NULL,
  `texto_botaoSlide2` varchar(45) DEFAULT NULL,
  `corTexto_botaoSlide2` varchar(10) DEFAULT NULL,
  `corFundo_botaoSlide2` varchar(10) DEFAULT NULL,
  `corTextoHover_botaoSlide2` varchar(10) DEFAULT NULL,
  `corFundoHover_botaoSlide2` varchar(10) DEFAULT NULL,
  `ativo_botaoSlide2` enum('0','1') DEFAULT NULL,
  `link_botaoSlide2` varchar(256) DEFAULT NULL,
  `ativo_slide3` enum('0','1') DEFAULT NULL,
  `imagem_slide3` varchar(145) DEFAULT NULL,
  `titulo_slide3` varchar(256) DEFAULT NULL,
  `descricao_slide3` varchar(512) DEFAULT NULL,
  `corTituloDescricao_slide3` varchar(10) DEFAULT NULL,
  `texto_botaoSlide3` varchar(45) DEFAULT NULL,
  `corTexto_botaoSlide3` varchar(10) DEFAULT NULL,
  `corFundo_botaoSlide3` varchar(10) DEFAULT NULL,
  `corTextoHover_botaoSlide3` varchar(10) DEFAULT NULL,
  `corFundoHover_botaoSlide3` varchar(10) DEFAULT NULL,
  `ativo_botaoSlide3` enum('0','1') DEFAULT NULL,
  `link_botaoSlide3` varchar(256) DEFAULT NULL,
  `ativo_slide4` enum('0','1') DEFAULT NULL,
  `imagem_slide4` varchar(145) DEFAULT NULL,
  `titulo_slide4` varchar(256) DEFAULT NULL,
  `descricao_slide4` varchar(512) DEFAULT NULL,
  `texto_botaoSlide4` varchar(45) DEFAULT NULL,
  `corTituloDescricao_slide4` varchar(10) DEFAULT NULL,
  `corTexto_botaoSlide4` varchar(10) DEFAULT NULL,
  `corFundo_botaoSlide4` varchar(10) DEFAULT NULL,
  `corTextoHover_botaoSlide4` varchar(10) DEFAULT NULL,
  `corFundoHover_botaoSlide4` varchar(10) DEFAULT NULL,
  `ativo_botaoSlide4` enum('0','1') DEFAULT NULL,
  `link_botaoSlide4` varchar(256) DEFAULT NULL,
  `seo_slide1` text,
  `seo_slide2` text,
  `seo_slide3` text,
  `seo_slide4` text,
  PRIMARY KEY (`id_slide`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_slide`
--

LOCK TABLES `site_slide` WRITE;
/*!40000 ALTER TABLE `site_slide` DISABLE KEYS */;
INSERT INTO `site_slide` VALUES (1,'1','1','img_site_banners_713511726.jpg','','','Ver','#f8030a','#b1e418','#fb1318','#000000','#58d638','//google.comtt','0','1','img_site_banners_734296195.jpg','','','#9b0b5e','','#1aeeff','#000000','#000000','#000000','0','','1','img_site_banners_1562517108.jpg','','','#7dd436','asd','#112233','#000000','#000000','#112233','0','//google.comee','1','img_site_banners_1395101024.jpg','','','sextou','#b446c4','#ffffff','#112233','#890d97','#ffffff','0','//google.comss','seo slide 1','seo slide 2','seo slide 3','seo slide 4');
/*!40000 ALTER TABLE `site_slide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_slideparceiros`
--

DROP TABLE IF EXISTS `site_slideparceiros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_slideparceiros` (
  `id_slideParceiros` int(1) NOT NULL AUTO_INCREMENT,
  `ativo_slide1` enum('0','1') DEFAULT NULL,
  `imagem_slide1` varchar(145) DEFAULT NULL,
  `titulo_slide1` varchar(256) DEFAULT NULL,
  `descricao_slide1` varchar(512) DEFAULT NULL,
  `texto_botaoSlide1` varchar(45) DEFAULT NULL,
  `corTituloDescricao_slide1` varchar(10) DEFAULT NULL,
  `corTexto_botaoSlide1` varchar(10) DEFAULT NULL,
  `corFundo_botaoSlide1` varchar(10) DEFAULT NULL,
  `corTextoHover_botaoSlide1` varchar(10) DEFAULT NULL,
  `corFundoHover_botaoSlide1` varchar(10) DEFAULT NULL,
  `link_botaoSlide1` varchar(256) DEFAULT NULL,
  `ativo_botaoSlide1` enum('0','1') DEFAULT NULL,
  `ativo_slide2` enum('0','1') DEFAULT NULL,
  `imagem_slide2` varchar(145) DEFAULT NULL,
  `titulo_slide2` varchar(256) DEFAULT NULL,
  `descricao_slide2` varchar(512) DEFAULT NULL,
  `corTituloDescricao_slide2` varchar(10) DEFAULT NULL,
  `texto_botaoSlide2` varchar(45) DEFAULT NULL,
  `corTexto_botaoSlide2` varchar(10) DEFAULT NULL,
  `corFundo_botaoSlide2` varchar(10) DEFAULT NULL,
  `corTextoHover_botaoSlide2` varchar(10) DEFAULT NULL,
  `corFundoHover_botaoSlide2` varchar(10) DEFAULT NULL,
  `ativo_botaoSlide2` enum('0','1') DEFAULT NULL,
  `ativo_slide3` enum('0','1') DEFAULT NULL,
  `imagem_slide3` varchar(145) DEFAULT NULL,
  `titulo_slide3` varchar(256) DEFAULT NULL,
  `descricao_slide3` varchar(512) DEFAULT NULL,
  `corTituloDescricao_slide3` varchar(10) DEFAULT NULL,
  `texto_botaoSlide3` varchar(45) DEFAULT NULL,
  `corTexto_botaoSlide3` varchar(10) DEFAULT NULL,
  `corFundo_botaoSlide3` varchar(10) DEFAULT NULL,
  `corTextoHover_botaoSlide3` varchar(10) DEFAULT NULL,
  `corFundoHover_botaoSlide3` varchar(10) DEFAULT NULL,
  `ativo_botaoSlide3` enum('0','1') DEFAULT NULL,
  `ativo_slide4` enum('0','1') DEFAULT NULL,
  `imagem_slide4` varchar(145) DEFAULT NULL,
  `titulo_slide4` varchar(256) DEFAULT NULL,
  `descricao_slide4` varchar(512) DEFAULT NULL,
  `texto_botaoSlide4` varchar(45) DEFAULT NULL,
  `corTituloDescricao_slide4` varchar(10) DEFAULT NULL,
  `corTexto_botaoSlide4` varchar(10) DEFAULT NULL,
  `corFundo_botaoSlide4` varchar(10) DEFAULT NULL,
  `corTextoHover_botaoSlide4` varchar(10) DEFAULT NULL,
  `corFundoHover_botaoSlide4` varchar(10) DEFAULT NULL,
  `ativo_botaoSlide4` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id_slideParceiros`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_slideparceiros`
--

LOCK TABLES `site_slideparceiros` WRITE;
/*!40000 ALTER TABLE `site_slideparceiros` DISABLE KEYS */;
INSERT INTO `site_slideparceiros` VALUES (1,'1','img_site_banners_112241957.jpg','teste titulo1','teste descricao1111','asdasd','#7dd436','#890c0f','#e6f4ff','#c5e1fe','#000000','//google.com','1','1','img_site_banners_1211081859.jpg','titulo 2','descricao 2','#9b0b5e','Visitar página','#1aeeff','#000000','#000000','#61f222','1','1','img_site_banners_1865862729.jpg','titulo 3','descricao 3','#7dd436','asd','#112233','#000000','#000000','#112233','1','1','img_site_banners_1367165321.jpg','titulo 4','descricao 4','sextou','#b446c4','#ffffff','#112233','#890d97','#ffffff','1'),(2,'1','img_site_parceiros_594698992.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','img_site_parceiros_1309507667.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','img_site_parceiros_1652435343.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','img_site_parceiros_1789320901.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `site_slideparceiros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_videos`
--

DROP TABLE IF EXISTS `site_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_videos` (
  `id_videos` int(11) NOT NULL AUTO_INCREMENT,
  `ativo_videos` int(11) DEFAULT '1',
  `nome_videos` varchar(145) DEFAULT NULL,
  `link_videos` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_videos`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_videos`
--

LOCK TABLES `site_videos` WRITE;
/*!40000 ALTER TABLE `site_videos` DISABLE KEYS */;
INSERT INTO `site_videos` VALUES (1,1,'Video em PLAYLIST','okRMwja6oTU'),(2,1,'Video  da aba compartilhamento','okRMwja6oTU'),(3,1,'Video URL comum','qM-dOYiyvCA'),(4,1,'Video pos aba compartilhamento','qM-dOYiyvCA'),(5,1,'sdasd','okRMwja6oTU');
/*!40000 ALTER TABLE `site_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates_landing_pages`
--

DROP TABLE IF EXISTS `templates_landing_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `templates_landing_pages` (
  `id_templates` int(11) NOT NULL AUTO_INCREMENT,
  `nomeTemplate_templates` varchar(145) NOT NULL,
  `imagemTemplate_templates` varchar(145) NOT NULL,
  `statusTemplate_templates` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_templates`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates_landing_pages`
--

LOCK TABLES `templates_landing_pages` WRITE;
/*!40000 ALTER TABLE `templates_landing_pages` DISABLE KEYS */;
INSERT INTO `templates_landing_pages` VALUES (1,'Padrão','img_templates_1117283970.jpg','1');
/*!40000 ALTER TABLE `templates_landing_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `login_usuario` varchar(45) NOT NULL,
  `senha_usuario` varchar(145) NOT NULL,
  `nome_usuario` varchar(45) NOT NULL,
  `email_usuario` varchar(90) NOT NULL,
  `status_usuario` enum('0','1') NOT NULL,
  `nivelAcesso_usuario` int(11) DEFAULT NULL,
  `imagem_usuario` varchar(145) NOT NULL DEFAULT 'default.png',
  `token_usuario` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'josheffer','$2y$10$4g/W.I2AnCwxV3aS5W8xbOyx.qpH/t8yB4iYjLyoDo.2uqXCaNKva','Josheffer Robert','josheffer@gmail.com','1',1,'img_usuario_2076543518.jpg','na9r3dcvocjftsdbrdr4qgt8q2gme2es'),(2,'jojojo','$2y$10$/5oOP.crYPDT5Uqjrg5FCet1Antz39JDBIsUCJ7tDPYx.6huZ4GI.','jojojo','jojo@gmail.com','1',5,'img_usuario_417050521.jpg','4s60k5o13rjhh76eou80si7oe1u8vfq0'),(3,'zz','$2y$10$BO0rpDqvjvyX2OMAh2PreuSfJBv9Uroh3IChDygDhThqAvXtQvNw6','zz','zz@gmail.com','1',5,'img_usuario_394630158.jpg','u25f4ck084bdo6f8cu0l6pli675mao9u');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-29 10:04:40
