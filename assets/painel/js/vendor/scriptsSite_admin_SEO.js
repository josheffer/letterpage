primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarSEO();

function listarSEO(){
    
    $.ajax(
        {
            
        url: "/dashboard/listagens/listarSEO",
        
        ajax: 'dados.json',
        
        success: function(dados){
            
            var dados = JSON.parse(dados);

            dadosGlobais = dados;
            
            $('#cssHeadTOPO').html(dados.site_css); 
            $('#jsHead').html(dados.site_js); 
            $('#jsBody').html(dados.site_js_body); 
            $('#SEOKeywordsHead').html(dados.site_keywords); 
            $('#SEODescriptionsHead').html(dados.site_description); 
            $('#SEODescriptionsHead2').html(dados.site_description2); 
            $('#SEODescriptionsHead3').html(dados.site_description3); 
            $('#SEOCoordGMaps').val(dados.site_coordenadasMaps); 
            $('#SEOCidade').val(dados.site_cidade); 
            $('#SEOEstado').val(dados.site_estado); 
            $('#SEONomeSite').val(dados.site_nomeSite); 
            $('#corTema').val(dados.site_corTema); 
            $("#verImagemDoSEO").attr("src", "/assets/site/images/SEO/"+dados.site_SEO_imagem);
            
        }
        }
    );
}

$('#formSEO').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarSeo(self);
});

function atualizarSeo(dados){

    var form = $('#formSEO')[0];

    var data = new FormData(form);
        
    $.ajax(
        {
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/siteSEO",
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 400000,
        
        success: function(retorno){
            
            if(retorno.ret == false) {

                $('#erroMsg').html(
                    
                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                        '<h5 class="text-white">Erro!</h5>' +
                        retorno.msg +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                    '</div>'
                );
                
                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);
            
            } else {
            
                $('#erroMsg').html(
                    
                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                        retorno.msg +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">&times;</span>' +
                        '</button>' +
                    '</div>'
                );
                
                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarSEO();

                $('#formSEO').each (function(){
                    this.reset();
                  });
            
            }
        }
        }
    );
}