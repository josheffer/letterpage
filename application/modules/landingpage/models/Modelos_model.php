<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Modelos_model extends CI_Model {

    public function buscarDados_headSEO($id)
    {
        
        $resultado = $this->db->query("SELECT * FROM lp_modeloconfig_head WHERE id_landingPage =". $id)->result();

        foreach ($resultado as $result) {
            
            return $result;

        }
        
    }
    
    public function m_atualizarHeadSEO($dados)
    {
        $resultado = $this->db->query("UPDATE lp_modeloconfig_head  
                                        SET topo_head_css               = '".$dados['topo_head_css']."', 
                                            topo_head_js                = '".$dados['topo_head_js']."', 
                                            topo_head_SEO_keywords      = '".$dados['topo_head_SEO_keywords']."', 
                                            topo_head_SEO_description   = '".$dados['topo_head_SEO_description']."',
                                            topo_head_SEO_description2  = '".$dados['topo_head_SEO_description2']."',
                                            topo_head_SEO_description3  = '".$dados['topo_head_SEO_description3']."',
                                            topo_head_coordenadasMaps   = '".$dados['topo_head_coordenadasMaps']."',
                                            topo_head_cidade            = '".$dados['topo_head_cidade']."',
                                            topo_head_estado            = '".$dados['topo_head_estado']."',
                                            topo_head_SEO_imagem        = '".$dados['topo_head_SEO_imagem']."'
                                        WHERE (id_landingPage           = '".$dados['id_landingPage']."');");
        
        return $resultado;
    }

    public function m_listarLogoMenuSlide($id)
    {
        $resultado['logoMenuSlide'] = $this->db->query("SELECT * FROM lp_modeloconfig_logomenuslide WHERE id_landingPage =". $id)->result();
        $resultado['configs_personalizadas'] = $this->db->query("SELECT * FROM lp_modelosconfig_personalizacao WHERE id_landingPage =". $id)->result();
        
        return $resultado;
    }

    public function m_buscarLogoMenuSlide($id)
    {
        $resultado = $this->db->query("SELECT * FROM lp_modeloconfig_logomenuslide WHERE id_landingPage =". $id)->result();

        
        foreach ($resultado as $result) {
            
            return $result;
            
        }
        
    }

    public function m_atualizarformLogoMenuSlide($dados)
    {
        $this->db->query("UPDATE lp_modeloconfig_logomenuslide 
                                                    SET body_logo1                      = '".$dados['body_logo1']."', 
                                                        body_slide                      = '".$dados['body_slide']."', 
                                                        body_imagemPrincipalSlide       = '".$dados['body_imagemPrincipalSlide']."', 
                                                        body_slide_imagem2_SEO          = '".$dados['body_slide_imagem2_SEO']."', 
                                                        body_slide1                     = '".$dados['body_slide1']."', 
                                                        body_slide_imagem2              = '".$dados['body_slide_imagem2']."', 
                                                        body_imagemFundoSlide           = '".$dados['body_imagemFundoSlide']."', 
                                                        body_slide_titulo1              = '".$dados['body_slide_titulo1']."', 
                                                        body_slide_descricao1           = '".$dados['body_slide_descricao1']."', 
                                                        body_slide_imagemFundoSlide     = '".$dados['body_slide_imagemFundoSlide']."', 
                                                        body_slide_imagem1              = '".$dados['body_slide_imagem1']."', 
                                                        body_slide_botao1               = '".$dados['body_slide_botao1']."', 
                                                        body_slide_botaoTexto1          = '".$dados['body_slide_botaoTexto1']."', 
                                                        body_slide_botaoLink1           = '".$dados['body_slide_botaoLink1']."' 
                                                    WHERE (id_landingPage                    = '".$dados['id_landingPage']."');");

        $resultado = $this->db->query("UPDATE lp_modelosconfig_personalizacao 
                                        SET corFundoMenu                    = '".$dados['corFundoMenu']."', 
                                            corFundoSlide                   = '".$dados['corFundoSlide']."', 
                                            corTextoTituloSlide             = '".$dados['corTextoTituloSlide']."', 
                                            corTextoDescricaoSlide          = '".$dados['corTextoDescricaoSlide']."', 
                                            corTextoBotaoSlide              = '".$dados['corTextoBotaoSlide']."', 
                                            corFundoBotaoSlide              = '".$dados['corFundoBotaoSlide']."', 
                                            corFundoHoverBotaoSlide         = '".$dados['corFundoHoverBotaoSlide']."', 
                                            corFundoSlide1                  = '".$dados['corFundoSlide1']."', 
                                            corTextoHoverBotaoSlide         = '".$dados['corTextoHoverBotaoSlide']."' 
                                        WHERE (id_landingPage               = '".$dados['id_landingPage']."');    
        ");
        
        return $resultado;
        
    }

    public function m_listarForms($id)
    {

        $resultado = $this->db->query("SELECT * FROM lp_modelosconfig_forms WHERE id_landingPage =". $id)->result();

        foreach ($resultado as $result) {
            
            return $result;

        }
      
    }

    public function m_atualizarFormularios($dados)
    {
        $this->db->where('id_landingPage', $dados['id_landingPage']);

        return $this->db->update('lp_modelosconfig_forms', $dados);
    }

    public function buscarDados_redesSociais($id)
    {

        $resultado['redesSociais'] = $this->db->query("SELECT * FROM lp_modelosconfig_redessociais WHERE id_landingPage =". $id)->result();
        $resultado['configs_personalizadas'] = $this->db->query("SELECT * FROM lp_modelosconfig_personalizacao WHERE id_landingPage =". $id)->result();
        
        return $resultado;

    }

    public function m_atualizarRedesSociais($dados)
    {
        
        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

                          SET   servico_redesSociais_corBg                      = '".$dados['servico_redesSociais_corBg']."',
                                servico_redesSociais_corTituloGenerico1         = '".$dados['servico_redesSociais_corTituloGenerico1']."',
                                servico_redesSociais_corDescricaoGenerico1      = '".$dados['servico_redesSociais_corDescricaoGenerico1']."',
                                servico_redesSociais_corTexto                   = '".$dados['servico_redesSociais_corTexto']."',
                                servico_redesSociais_corBoxImage                = '".$dados['servico_redesSociais_corBoxImage']."',
                                servico_redesSociais_corTextoBotao              = '".$dados['servico_redesSociais_corTextoBotao']."',
                                servico_redesSociais_corTextoHoverBotao         = '".$dados['servico_redesSociais_corTextoHoverBotao']."',
                                servico_redesSociais_corFundoBotao              = '".$dados['servico_redesSociais_corFundoBotao']."',
                                servico_redesSociais_corFundoHoverBotao         = '".$dados['servico_redesSociais_corFundoHoverBotao']."'

                          WHERE (id_landingPage                                      = '".$dados['id_landingPage']."');");

        $resultado = $this->db->query("UPDATE lp_modelosconfig_redessociais 

                                        SET body_conteudoGenerico_redesSociais      = '".$dados['body_conteudoGenerico_redesSociais']."',
                                        body_redesSociais                           = '".$dados['body_redesSociais']."',
                                        body_redesSociais_opcao1                    = '".$dados['body_redesSociais_opcao1']."',
                                        body_redesSociais_opcao2                    = '".$dados['body_redesSociais_opcao2']."',
                                        body_redesSociais_opcao3                    = '".$dados['body_redesSociais_opcao3']."',
                                        body_servico_redesSociais_botao1            = '".$dados['body_servico_redesSociais_botao1']."',
                                        body_servico_redesSociais_botao2            = '".$dados['body_servico_redesSociais_botao2']."',
                                        body_servico_redesSociais_botao3            = '".$dados['body_servico_redesSociais_botao3']."',
                                        body_textoGenericoTitulo_redesSociais       = '".$dados['body_textoGenericoTitulo_redesSociais']."',
                                        body_textoGenericoDescricao_redesSociais    = '".$dados['body_textoGenericoDescricao_redesSociais']."',
                                        body_servico_redesSociais_Titulo1           = '".$dados['body_servico_redesSociais_Titulo1']."',
                                        body_servico_redesSociais_Titulo2           = '".$dados['body_servico_redesSociais_Titulo2']."',
                                        body_servico_redesSociais_Titulo3           = '".$dados['body_servico_redesSociais_Titulo3']."',
                                        body_servico_redesSociais_Descricao1        = '".$dados['body_servico_redesSociais_Descricao1']."',
                                        body_servico_redesSociais_Descricao2        = '".$dados['body_servico_redesSociais_Descricao2']."',
                                        body_servico_redesSociais_Descricao3        = '".$dados['body_servico_redesSociais_Descricao3']."',
                                        body_servico_redesSociais_botaoTexto1       = '".$dados['body_servico_redesSociais_botaoTexto1']."',
                                        body_servico_redesSociais_botaoTexto2       = '".$dados['body_servico_redesSociais_botaoTexto2']."',
                                        body_servico_redesSociais_botaoTexto3       = '".$dados['body_servico_redesSociais_botaoTexto3']."',
                                        body_servico_redesSociais_botaoLink1        = '".$dados['body_servico_redesSociais_botaoLink1']."',
                                        body_servico_redesSociais_botaoLink2        = '".$dados['body_servico_redesSociais_botaoLink2']."',
                                        body_servico_redesSociais_botaoLink3        = '".$dados['body_servico_redesSociais_botaoLink3']."',
                                        body_servico_redesSociais_ImagemSEO1        = '".$dados['body_servico_redesSociais_ImagemSEO1']."',
                                        body_servico_redesSociais_ImagemSEO2        = '".$dados['body_servico_redesSociais_ImagemSEO2']."',
                                        body_servico_redesSociais_ImagemSEO3        = '".$dados['body_servico_redesSociais_ImagemSEO3']."',
                                        body_servico_redesSociais_Imagem1           = '".$dados['body_servico_redesSociais_Imagem1']."',
                                        body_servico_redesSociais_Imagem2           = '".$dados['body_servico_redesSociais_Imagem2']."',
                                        body_servico_redesSociais_Imagem3           = '".$dados['body_servico_redesSociais_Imagem3']."'
                                            
                                        WHERE (id_landingPage                            = '".$dados['id_landingPage']."');");
                                        
        return $resultado;

    }

    public function buscarServicos($id)
    {
        $resultado['servicos_personalizado'] =$this->db->query("SELECT * FROM lp_modelosconfig_personalizacao WHERE id_landingPage =". $id)->result();
        $resultado['servico1'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico1 WHERE id_landingPage =". $id)->result();
        $resultado['servico2'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico2 WHERE id_landingPage =". $id)->result();
        $resultado['servico3'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico3 WHERE id_landingPage =". $id)->result();
        $resultado['servico4'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico4 WHERE id_landingPage =". $id)->result();
        $resultado['servico5'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico5 WHERE id_landingPage =". $id)->result();
        $resultado['servico6'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico6 WHERE id_landingPage =". $id)->result();
        $resultado['servico7'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico7 WHERE id_landingPage =". $id)->result();
        $resultado['servico8'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico_maps WHERE id_landingPage =". $id)->result();
        $resultado['paginaSucesso'] = $this->db->query("SELECT * FROM lp_modelosconfig_servico_paginasucesso WHERE id_landingPage =". $id)->result();
        
        return $resultado;
    }

    public function m_atualizarServico1($dados)
    {
        
        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

                            SET servico1_corTituloGenerico1 = '".$dados['servico1_corTituloGenerico1']."',
                                servico1_corDescricaoGenerico1 = '".$dados['servico1_corDescricaoGenerico1']."',
                                servico1_corBg = '".$dados['servico1_corFundoBG']."',
                                servico1_corTexto = '".$dados['servico1_corTexto']."',
                                servico1_corBoxImage = '".$dados['servico1_corBoxImage']."'
                                            
                            WHERE (id_landingPage = '".$dados['id_landingPage']."');");
        
        $resultado = $this->db->query("UPDATE lp_modelosconfig_servico1 

                                        SET body_servico1 = '".$dados['body_servico1']."',
                                        body_conteudoGenerico1 = '".$dados['body_conteudoGenerico1']."',
                                        body_textoGenericoTitulo1 = '".$dados['body_textoGenericoTitulo1']."',
                                        body_textoGenericoDescricao1 = '".$dados['body_textoGenericoDescricao1']."',
                                        body_servico1_opcao1 = '".$dados['body_servico1_opcao1']."',
                                        body_servico1Titulo1 = '".$dados['body_servico1Titulo1']."',
                                        body_servico1Descricao1 = '".$dados['body_servico1Descricao1']."',
                                        body_servico1ImagemSEO1 = '".$dados['body_servico1ImagemSEO1']."',
                                        body_servico1_opcao2 = '".$dados['body_servico1_opcao2']."',
                                        body_servico1Titulo2 = '".$dados['body_servico1Titulo2']."',
                                        body_servico1Descricao2 = '".$dados['body_servico1Descricao2']."',
                                        body_servico1ImagemSEO2 = '".$dados['body_servico1ImagemSEO2']."',
                                        body_servico1_opcao3 = '".$dados['body_servico1_opcao3']."',
                                        body_servico1Titulo3 = '".$dados['body_servico1Titulo3']."',
                                        body_servico1Descricao3 = '".$dados['body_servico1Descricao3']."',
                                        body_servico1ImagemSEO3 = '".$dados['body_servico1ImagemSEO3']."',
                                        body_servico1_opcao4 = '".$dados['body_servico1_opcao4']."',
                                        body_servico1Titulo4 = '".$dados['body_servico1Titulo4']."',
                                        body_servico1Descricao4 = '".$dados['body_servico1Descricao4']."',
                                        body_servico1ImagemSEO4 = '".$dados['body_servico1ImagemSEO4']."',
                                        body_servico1_opcao5 = '".$dados['body_servico1_opcao5']."',
                                        body_servico1Titulo5 = '".$dados['body_servico1Titulo5']."',
                                        body_servico1Descricao5 = '".$dados['body_servico1Descricao5']."',
                                        body_servico1ImagemSEO5 = '".$dados['body_servico1ImagemSEO5']."',
                                        body_servico1_opcao6 = '".$dados['body_servico1_opcao6']."',
                                        body_servico1Titulo6 = '".$dados['body_servico1Titulo6']."',
                                        body_servico1Descricao6 = '".$dados['body_servico1Descricao6']."',
                                        body_servico1ImagemSEO6 = '".$dados['body_servico1ImagemSEO6']."',
                                        body_servico1_opcao7 = '".$dados['body_servico1_opcao7']."',
                                        body_servico1Titulo7 = '".$dados['body_servico1Titulo7']."',
                                        body_servico1Descricao7 = '".$dados['body_servico1Descricao7']."',
                                        body_servico1ImagemSEO7 = '".$dados['body_servico1ImagemSEO7']."',
                                        body_servico1_opcao8 = '".$dados['body_servico1_opcao8']."',
                                        body_servico1Titulo8 = '".$dados['body_servico1Titulo8']."',
                                        body_servico1Descricao8 = '".$dados['body_servico1Descricao8']."',
                                        body_servico1ImagemSEO8 = '".$dados['body_servico1ImagemSEO8']."',
                                        body_servico1Imagem1 = '".$dados['body_servico1Imagem1']."',
                                        body_servico1Imagem2 = '".$dados['body_servico1Imagem2']."',
                                        body_servico1Imagem3 = '".$dados['body_servico1Imagem3']."',
                                        body_servico1Imagem4 = '".$dados['body_servico1Imagem4']."',
                                        body_servico1Imagem5 = '".$dados['body_servico1Imagem5']."',
                                        body_servico1Imagem6 = '".$dados['body_servico1Imagem6']."',
                                        body_servico1Imagem7 = '".$dados['body_servico1Imagem7']."',
                                        body_servico1Imagem8 = '".$dados['body_servico1Imagem8']."'
                                            
                                        WHERE (id_landingPage                            = '".$dados['id_landingPage']."');");
                                        
        return $resultado;
    }

    public function m_atualizarServico2($dados)
    {
        
        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

                            SET servico2_corTituloGenerico1 = '".$dados['servico2_corTitulo']."',
                                servico2_corDescricaoGenerico1 = '".$dados['servico2_corDescricao']."',
                                servico2_corBg = '".$dados['servico2_corBg']."',
                                servico2_corTextoBotao = '".$dados['servico2_corTextoBotao']."',
                                servico2_corTextoHoverBotao = '".$dados['servico2_corTextoHoverBotao']."',
                                servico2_corFundoBotao = '".$dados['servico2_corFundoBotao']."',
                                servico2_corFundoHoverBotao = '".$dados['servico2_corFundoHoverBotao']."'
                                            
                            WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        $resultado = $this->db->query("UPDATE lp_modelosconfig_servico2 

                                        SET body_servico2 = '".$dados['body_servico2']."',
                                            body_conteudoGenerico2 = '".$dados['body_conteudoGenerico2']."',
                                            body_servico2_botao1 = '".$dados['body_servico2_botao1']."',
                                            body_textoGenericoTitulo2 = '".$dados['body_textoGenericoTitulo2']."',
                                            body_textoGenericoDescricao2 = '".$dados['body_textoGenericoDescricao2']."',
                                            body_servico2ImagemSEO1 = '".$dados['body_servico2ImagemSEO1']."',
                                            body_servico2_botaoTexto1 = '".$dados['body_servico2_botaoTexto1']."',
                                            body_servico2_botaoLink1 = '".$dados['body_servico2_botaoLink1']."',
                                            body_servico2Imagem1 = '".$dados['body_servico2Imagem1']."'
                                                        
                                        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        return $resultado;
    }

    public function m_atualizarServico3($dados_servico, $dados_personalizacao)
    {
        $this->db->query("UPDATE lp_modelosconfig_servico3 

                        SET body_servico3 = '".$dados_servico['body_servico3']."',
                            body_conteudoGenerico3 = '".$dados_servico['body_conteudoGenerico3']."',
                            body_servico3_TodasOpcoes = '".$dados_servico['body_servico3_TodasOpcoes']."',
                            body_servico3_opcao1 = '".$dados_servico['body_servico3_opcao1']."',
                            body_servico3_opcao2 = '".$dados_servico['body_servico3_opcao2']."',
                            body_servico3_opcao3 = '".$dados_servico['body_servico3_opcao3']."',
                            body_servico3_opcao4 = '".$dados_servico['body_servico3_opcao4']."',
                            body_textoGenericoTitulo3 = '".$dados_servico['body_textoGenericoTitulo3']."',
                            body_textoGenericoDescricao3 = '".$dados_servico['body_textoGenericoDescricao3']."',
                            body_servico3ImagemSEO5 = '".$dados_servico['body_servico3ImagemSEO5']."',
                            body_servico3ImagemSEO1 = '".$dados_servico['body_servico3ImagemSEO1']."',
                            body_servico3Titulo1 = '".$dados_servico['body_servico3Titulo1']."',
                            body_servico3Descricao1 = '".$dados_servico['body_servico3Descricao1']."',
                            body_servico3ImagemSEO2 = '".$dados_servico['body_servico3ImagemSEO2']."',
                            body_servico3Titulo2 = '".$dados_servico['body_servico3Titulo2']."',
                            body_servico3Descricao2 = '".$dados_servico['body_servico3Descricao2']."',
                            body_servico3ImagemSEO3 = '".$dados_servico['body_servico3ImagemSEO3']."',
                            body_servico3Titulo3 = '".$dados_servico['body_servico3Titulo3']."',
                            body_servico3Descricao3 = '".$dados_servico['body_servico3Descricao3']."',
                            body_servico3Titulo4 = '".$dados_servico['body_servico3Titulo4']."',
                            body_servico3ImagemSEO4 = '".$dados_servico['body_servico3ImagemSEO4']."',
                            body_servico3Descricao4 = '".$dados_servico['body_servico3Descricao4']."',
                            body_servico3Imagem1 = '".$dados_servico['body_servico3Imagem1']."',
                            body_servico3Imagem2 = '".$dados_servico['body_servico3Imagem2']."',
                            body_servico3Imagem3 = '".$dados_servico['body_servico3Imagem3']."',
                            body_servico3Imagem4 = '".$dados_servico['body_servico3Imagem4']."',
                            body_servico3Imagem5 = '".$dados_servico['body_servico3Imagem5']."'

                        WHERE (id_landingPage = '".$dados_servico['id_landingPage']."');");
                                        
        $resultado = $this->db->query("UPDATE lp_modelosconfig_personalizacao 

                        SET servico3_corTituloGenerico1 = '".$dados_personalizacao['servico3_corTituloGenerico1']."',
                            servico3_corDescricaoGenerico1 = '".$dados_personalizacao['servico3_corDescricaoGenerico1']."',
                            servico3_corBg = '".$dados_personalizacao['servico3_corBg']."',
                            servico3_corBoxImage = '".$dados_personalizacao['servico3_corBoxImage']."',
                            servico3_corTexto = '".$dados_personalizacao['servico3_corTexto']."'
                                        
                        WHERE (id_landingPage = '".$dados_servico['id_landingPage']."');");

        return $resultado;
    }

    public function m_atualizarServico4($dados)
    {
        
        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

                        SET servico4_corTituloGenerico1 = '".$dados['servico4_corTituloGenerico1']."',
                            servico4_corDescricaoGenerico1 = '".$dados['servico4_corDescricaoGenerico1']."',
                            servico4_corBg = '".$dados['servico4_corBg']."',
                            servico4_corTextoBotao = '".$dados['servico4_corTextoBotao']."',
                            servico4_corTextoHoverBotao = '".$dados['servico4_corTextoHoverBotao']."',
                            servico4_corFundoBotao = '".$dados['servico4_corFundoBotao']."',
                            servico4_corFundoHoverBotao = '".$dados['servico4_corFundoHoverBotao']."'

                        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        $resultado = $this->db->query("UPDATE lp_modelosconfig_servico4 

                        SET body_servico4 = '".$dados['body_servico4']."',
                            body_conteudoGenerico4 = '".$dados['body_conteudoGenerico4']."',
                            body_servico4_botao1 = '".$dados['body_servico4_botao1']."',
                            body_textoGenericoTitulo4 = '".$dados['body_textoGenericoTitulo4']."',
                            body_textoGenericoDescricao4 = '".$dados['body_textoGenericoDescricao4']."',
                            body_servico4ImagemSEO1 = '".$dados['body_servico4ImagemSEO1']."',
                            body_servico4_botaoTexto1 = '".$dados['body_servico4_botaoTexto1']."',
                            body_servico4_botaoLink1 = '".$dados['body_servico4_botaoLink1']."',
                            body_servico4Imagem1 = '".$dados['body_servico4Imagem1']."'

                        WHERE (id_landingPage = '".$dados['id_landingPage']."');");
        
        return $resultado;
    }
    
    public function m_atualizarServico5($dados)
    {
        
        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

                        SET servico5_corTituloGenerico1 = '".$dados['servico5_corTituloGenerico1']."',
                            servico5_corDescricaoGenerico1 = '".$dados['servico5_corDescricaoGenerico1']."',
                            servico5_corBg = '".$dados['servico5_corBg']."',
                            servico5_corTexto = '".$dados['servico5_corTexto']."',
                            servico5_corBoxImage = '".$dados['servico5_corBoxImage']."',
                            servico5_corTextoBotao = '".$dados['servico5_corTextoBotao']."',
                            servico5_corTextoHoverBotao = '".$dados['servico5_corTextoHoverBotao']."',
                            servico5_corFundoBotao = '".$dados['servico5_corFundoBotao']."',
                            servico5_corFundoHoverBotao = '".$dados['servico5_corFundoHoverBotao']."'

                        WHERE (id_landingPage = '".$dados['id_landingPage']."');");
                        
        $resultado = $this->db->query("UPDATE lp_modelosconfig_servico5 

        SET body_servico5 = '".$dados['body_servico5']."',
            body_conteudoGenerico5 = '".$dados['body_conteudoGenerico5']."',
            body_textoGenericoTitulo5 = '".$dados['body_textoGenericoTitulo5']."',
            body_textoGenericoDescricao5 = '".$dados['body_textoGenericoDescricao5']."',
            body_servico5_opcao1 = '".$dados['body_servico5_opcao1']."',
            body_servico5_opcao2 = '".$dados['body_servico5_opcao2']."',
            body_servico5_opcao3 = '".$dados['body_servico5_opcao3']."',
            body_servico5_opcao4 = '".$dados['body_servico5_opcao4']."',
            body_servico5_opcao5 = '".$dados['body_servico5_opcao5']."',
            body_servico5_opcao6 = '".$dados['body_servico5_opcao6']."',
            body_servico5_opcao7 = '".$dados['body_servico5_opcao7']."',
            body_servico5_opcao8 = '".$dados['body_servico5_opcao8']."',
            body_servico5Titulo1 = '".$dados['body_servico5Titulo1']."',
            body_servico5Titulo2 = '".$dados['body_servico5Titulo2']."',
            body_servico5Titulo3 = '".$dados['body_servico5Titulo3']."',
            body_servico5Titulo4 = '".$dados['body_servico5Titulo4']."',
            body_servico5Titulo5 = '".$dados['body_servico5Titulo5']."',
            body_servico5Titulo6 = '".$dados['body_servico5Titulo6']."',
            body_servico5Titulo7 = '".$dados['body_servico5Titulo7']."',
            body_servico5Titulo8 = '".$dados['body_servico5Titulo8']."',
            body_servico5Titulo8 = '".$dados['body_servico5Titulo8']."',
            body_servico5Titulo8 = '".$dados['body_servico5Titulo8']."',
            body_servico5Descricao1 = '".$dados['body_servico5Descricao1']."',
            body_servico5Descricao2 = '".$dados['body_servico5Descricao2']."',
            body_servico5Descricao3 = '".$dados['body_servico5Descricao3']."',
            body_servico5Descricao4 = '".$dados['body_servico5Descricao4']."',
            body_servico5Descricao5 = '".$dados['body_servico5Descricao5']."',
            body_servico5Descricao6 = '".$dados['body_servico5Descricao6']."',
            body_servico5Descricao7 = '".$dados['body_servico5Descricao7']."',
            body_servico5Descricao8 = '".$dados['body_servico5Descricao8']."',
            body_servico5ImagemSEO1 = '".$dados['body_servico5ImagemSEO1']."',
            body_servico5ImagemSEO2 = '".$dados['body_servico5ImagemSEO2']."',
            body_servico5ImagemSEO3 = '".$dados['body_servico5ImagemSEO3']."',
            body_servico5ImagemSEO4 = '".$dados['body_servico5ImagemSEO4']."',
            body_servico5ImagemSEO5 = '".$dados['body_servico5ImagemSEO5']."',
            body_servico5ImagemSEO6 = '".$dados['body_servico5ImagemSEO6']."',
            body_servico5ImagemSEO7 = '".$dados['body_servico5ImagemSEO7']."',
            body_servico5ImagemSEO8 = '".$dados['body_servico5ImagemSEO8']."',
            body_servico5_botao1 = '".$dados['body_servico5_botao1']."',
            body_servico5_botao2 = '".$dados['body_servico5_botao2']."',
            body_servico5_botao3 = '".$dados['body_servico5_botao3']."',
            body_servico5_botao4 = '".$dados['body_servico5_botao4']."',
            body_servico5_botao5 = '".$dados['body_servico5_botao5']."',
            body_servico5_botao6 = '".$dados['body_servico5_botao6']."',
            body_servico5_botao7 = '".$dados['body_servico5_botao7']."',
            body_servico5_botao8 = '".$dados['body_servico5_botao8']."',
            body_servico5_botaoTexto1 = '".$dados['body_servico5_botaoTexto1']."',
            body_servico5_botaoTexto2 = '".$dados['body_servico5_botaoTexto2']."',
            body_servico5_botaoTexto3 = '".$dados['body_servico5_botaoTexto3']."',
            body_servico5_botaoTexto4 = '".$dados['body_servico5_botaoTexto4']."',
            body_servico5_botaoTexto5 = '".$dados['body_servico5_botaoTexto5']."',
            body_servico5_botaoTexto6 = '".$dados['body_servico5_botaoTexto6']."',
            body_servico5_botaoTexto7 = '".$dados['body_servico5_botaoTexto7']."',
            body_servico5_botaoTexto8 = '".$dados['body_servico5_botaoTexto8']."',
            body_servico5_botaoLink1 = '".$dados['body_servico5_botaoLink1']."',
            body_servico5_botaoLink2 = '".$dados['body_servico5_botaoLink2']."',
            body_servico5_botaoLink3 = '".$dados['body_servico5_botaoLink3']."',
            body_servico5_botaoLink4 = '".$dados['body_servico5_botaoLink4']."',
            body_servico5_botaoLink5 = '".$dados['body_servico5_botaoLink5']."',
            body_servico5_botaoLink6 = '".$dados['body_servico5_botaoLink6']."',
            body_servico5_botaoLink7 = '".$dados['body_servico5_botaoLink7']."',
            body_servico5_botaoLink8 = '".$dados['body_servico5_botaoLink8']."',
            body_servico5_preco1 = '".$dados['body_servico5_preco1']."',
            body_servico5_preco2 = '".$dados['body_servico5_preco2']."',
            body_servico5_preco3 = '".$dados['body_servico5_preco3']."',
            body_servico5_preco4 = '".$dados['body_servico5_preco4']."',
            body_servico5_preco5 = '".$dados['body_servico5_preco5']."',
            body_servico5_preco6 = '".$dados['body_servico5_preco6']."',
            body_servico5_preco7 = '".$dados['body_servico5_preco7']."',
            body_servico5_preco8 = '".$dados['body_servico5_preco8']."',

            body_servico5_preco1_opcao1 = '".$dados['body_servico5_preco1_opcao1']."',
            body_servico5_preco2_opcao2 = '".$dados['body_servico5_preco2_opcao2']."',
            body_servico5_preco3_opcao3 = '".$dados['body_servico5_preco3_opcao3']."',
            body_servico5_preco4_opcao4 = '".$dados['body_servico5_preco4_opcao4']."',
            body_servico5_preco5_opcao5 = '".$dados['body_servico5_preco5_opcao5']."',
            body_servico5_preco6_opcao6 = '".$dados['body_servico5_preco6_opcao6']."',
            body_servico5_preco7_opcao7 = '".$dados['body_servico5_preco7_opcao7']."',
            body_servico5_preco8_opcao8 = '".$dados['body_servico5_preco8_opcao8']."',

            body_servico5Imagem1 = '".$dados['body_servico5Imagem1']."',
            body_servico5Imagem2 = '".$dados['body_servico5Imagem2']."',
            body_servico5Imagem3 = '".$dados['body_servico5Imagem3']."',
            body_servico5Imagem4 = '".$dados['body_servico5Imagem4']."',
            body_servico5Imagem5 = '".$dados['body_servico5Imagem5']."',
            body_servico5Imagem6 = '".$dados['body_servico5Imagem6']."',
            body_servico5Imagem7 = '".$dados['body_servico5Imagem7']."',
            body_servico5Imagem8 = '".$dados['body_servico5Imagem8']."',

            body_servico5_opcao1Descricao1 = '".$dados['body_servico5_opcao1Descricao1']."',
            body_servico5_opcao1Descricao2 = '".$dados['body_servico5_opcao1Descricao2']."',
            body_servico5_opcao1Descricao3 = '".$dados['body_servico5_opcao1Descricao3']."',
            body_servico5_opcao1Descricao4 = '".$dados['body_servico5_opcao1Descricao4']."',
            body_servico5_opcao1Descricao5 = '".$dados['body_servico5_opcao1Descricao5']."',
            body_servico5_opcao1Descricao6 = '".$dados['body_servico5_opcao1Descricao6']."',
            body_servico5_opcao1Descricao7 = '".$dados['body_servico5_opcao1Descricao7']."',
            body_servico5_opcao1Descricao8 = '".$dados['body_servico5_opcao1Descricao8']."',
            body_servico5_opcao1Descricao9 = '".$dados['body_servico5_opcao1Descricao9']."',
            body_servico5_opcao1Descricao10 = '".$dados['body_servico5_opcao1Descricao10']."',

            body_servico5_opcao2Descricao1 = '".$dados['body_servico5_opcao2Descricao1']."',
            body_servico5_opcao2Descricao2 = '".$dados['body_servico5_opcao2Descricao2']."',
            body_servico5_opcao2Descricao3 = '".$dados['body_servico5_opcao2Descricao3']."',
            body_servico5_opcao2Descricao4 = '".$dados['body_servico5_opcao2Descricao4']."',
            body_servico5_opcao2Descricao5 = '".$dados['body_servico5_opcao2Descricao5']."',
            body_servico5_opcao2Descricao6 = '".$dados['body_servico5_opcao2Descricao6']."',
            body_servico5_opcao2Descricao7 = '".$dados['body_servico5_opcao2Descricao7']."',
            body_servico5_opcao2Descricao8 = '".$dados['body_servico5_opcao2Descricao8']."',
            body_servico5_opcao2Descricao9 = '".$dados['body_servico5_opcao2Descricao9']."',
            body_servico5_opcao2Descricao10 = '".$dados['body_servico5_opcao2Descricao10']."',

            body_servico5_opcao3Descricao1 = '".$dados['body_servico5_opcao3Descricao1']."',
            body_servico5_opcao3Descricao2 = '".$dados['body_servico5_opcao3Descricao2']."',
            body_servico5_opcao3Descricao3 = '".$dados['body_servico5_opcao3Descricao3']."',
            body_servico5_opcao3Descricao4 = '".$dados['body_servico5_opcao3Descricao4']."',
            body_servico5_opcao3Descricao5 = '".$dados['body_servico5_opcao3Descricao5']."',
            body_servico5_opcao3Descricao6 = '".$dados['body_servico5_opcao3Descricao6']."',
            body_servico5_opcao3Descricao7 = '".$dados['body_servico5_opcao3Descricao7']."',
            body_servico5_opcao3Descricao8 = '".$dados['body_servico5_opcao3Descricao8']."',
            body_servico5_opcao3Descricao9 = '".$dados['body_servico5_opcao3Descricao9']."',
            body_servico5_opcao3Descricao10 = '".$dados['body_servico5_opcao3Descricao10']."',

            body_servico5_opcao4Descricao1 = '".$dados['body_servico5_opcao4Descricao1']."',
            body_servico5_opcao4Descricao2 = '".$dados['body_servico5_opcao4Descricao2']."',
            body_servico5_opcao4Descricao3 = '".$dados['body_servico5_opcao4Descricao3']."',
            body_servico5_opcao4Descricao4 = '".$dados['body_servico5_opcao4Descricao4']."',
            body_servico5_opcao4Descricao5 = '".$dados['body_servico5_opcao4Descricao5']."',
            body_servico5_opcao4Descricao6 = '".$dados['body_servico5_opcao4Descricao6']."',
            body_servico5_opcao4Descricao7 = '".$dados['body_servico5_opcao4Descricao7']."',
            body_servico5_opcao4Descricao8 = '".$dados['body_servico5_opcao4Descricao8']."',
            body_servico5_opcao4Descricao9 = '".$dados['body_servico5_opcao4Descricao9']."',
            body_servico5_opcao4Descricao10 = '".$dados['body_servico5_opcao4Descricao10']."',

            body_servico5_opcao5Descricao1 = '".$dados['body_servico5_opcao5Descricao1']."',
            body_servico5_opcao5Descricao2 = '".$dados['body_servico5_opcao5Descricao2']."',
            body_servico5_opcao5Descricao3 = '".$dados['body_servico5_opcao5Descricao3']."',
            body_servico5_opcao5Descricao4 = '".$dados['body_servico5_opcao5Descricao4']."',
            body_servico5_opcao5Descricao5 = '".$dados['body_servico5_opcao5Descricao5']."',
            body_servico5_opcao5Descricao6 = '".$dados['body_servico5_opcao5Descricao6']."',
            body_servico5_opcao5Descricao7 = '".$dados['body_servico5_opcao5Descricao7']."',
            body_servico5_opcao5Descricao8 = '".$dados['body_servico5_opcao5Descricao8']."',
            body_servico5_opcao5Descricao9 = '".$dados['body_servico5_opcao5Descricao9']."',
            body_servico5_opcao5Descricao10 = '".$dados['body_servico5_opcao5Descricao10']."',

            body_servico5_opcao6Descricao1 = '".$dados['body_servico5_opcao6Descricao1']."',
            body_servico5_opcao6Descricao2 = '".$dados['body_servico5_opcao6Descricao2']."',
            body_servico5_opcao6Descricao3 = '".$dados['body_servico5_opcao6Descricao3']."',
            body_servico5_opcao6Descricao4 = '".$dados['body_servico5_opcao6Descricao4']."',
            body_servico5_opcao6Descricao5 = '".$dados['body_servico5_opcao6Descricao5']."',
            body_servico5_opcao6Descricao6 = '".$dados['body_servico5_opcao6Descricao6']."',
            body_servico5_opcao6Descricao7 = '".$dados['body_servico5_opcao6Descricao7']."',
            body_servico5_opcao6Descricao8 = '".$dados['body_servico5_opcao6Descricao8']."',
            body_servico5_opcao6Descricao9 = '".$dados['body_servico5_opcao6Descricao9']."',
            body_servico5_opcao6Descricao10 = '".$dados['body_servico5_opcao6Descricao10']."',

            body_servico5_opcao7Descricao1 = '".$dados['body_servico5_opcao7Descricao1']."',
            body_servico5_opcao7Descricao2 = '".$dados['body_servico5_opcao7Descricao2']."',
            body_servico5_opcao7Descricao3 = '".$dados['body_servico5_opcao7Descricao3']."',
            body_servico5_opcao7Descricao4 = '".$dados['body_servico5_opcao7Descricao4']."',
            body_servico5_opcao7Descricao5 = '".$dados['body_servico5_opcao7Descricao5']."',
            body_servico5_opcao7Descricao6 = '".$dados['body_servico5_opcao7Descricao6']."',
            body_servico5_opcao7Descricao7 = '".$dados['body_servico5_opcao7Descricao7']."',
            body_servico5_opcao7Descricao8 = '".$dados['body_servico5_opcao7Descricao8']."',
            body_servico5_opcao7Descricao9 = '".$dados['body_servico5_opcao7Descricao9']."',
            body_servico5_opcao7Descricao10 = '".$dados['body_servico5_opcao7Descricao10']."',

            body_servico5_opcao8Descricao1 = '".$dados['body_servico5_opcao8Descricao1']."',
            body_servico5_opcao8Descricao2 = '".$dados['body_servico5_opcao8Descricao2']."',
            body_servico5_opcao8Descricao3 = '".$dados['body_servico5_opcao8Descricao3']."',
            body_servico5_opcao8Descricao4 = '".$dados['body_servico5_opcao8Descricao4']."',
            body_servico5_opcao8Descricao5 = '".$dados['body_servico5_opcao8Descricao5']."',
            body_servico5_opcao8Descricao6 = '".$dados['body_servico5_opcao8Descricao6']."',
            body_servico5_opcao8Descricao7 = '".$dados['body_servico5_opcao8Descricao7']."',
            body_servico5_opcao8Descricao8 = '".$dados['body_servico5_opcao8Descricao8']."',
            body_servico5_opcao8Descricao9 = '".$dados['body_servico5_opcao8Descricao9']."',
            body_servico5_opcao8Descricao10 = '".$dados['body_servico5_opcao8Descricao10']."'

        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        return $resultado;
    }
    
    public function m_atualizarServico6($dados)
    {
        
        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

                        SET servico6_corTituloGenerico1 = '".$dados['servico6_corTituloGenerico1']."',
                            servico6_corDescricaoGenerico1 = '".$dados['servico6_corDescricaoGenerico1']."',
                            servico6_corBg = '".$dados['servico6_corBg']."',
                            servico6_corTexto = '".$dados['servico6_corTexto']."'
                        WHERE (id_landingPage = '".$dados['id_landingPage']."');");
        
        $resultado = $this->db->query("UPDATE lp_modelosconfig_servico6 

            SET body_servico6 = '".$dados['body_servico6']."',
                body_conteudoGenerico6 = '".$dados['body_conteudoGenerico6']."',
                body_textoGenericoTitulo6 = '".$dados['body_textoGenericoTitulo6']."',
                body_textoGenericoDescricao6 = '".$dados['body_textoGenericoDescricao6']."',
                body_servico6_opcao1 = '".$dados['body_servico6_opcao1']."',
                body_servico6ImagemSEO1 = '".$dados['body_servico6ImagemSEO1']."',
                body_servico6Titulo1 = '".$dados['body_servico6Titulo1']."',
                body_servico6Descricao1 = '".$dados['body_servico6Descricao1']."',
                body_servico6Imagem1 = '".$dados['body_servico6Imagem1']."',
                body_servico6_opcao2 = '".$dados['body_servico6_opcao2']."',
                body_servico6ImagemSEO2 = '".$dados['body_servico6ImagemSEO2']."',
                body_servico6Titulo2 = '".$dados['body_servico6Titulo2']."',
                body_servico6Descricao2 = '".$dados['body_servico6Descricao2']."',
                body_servico6Imagem2 = '".$dados['body_servico6Imagem2']."',
                body_servico6_opcao3 = '".$dados['body_servico6_opcao3']."',
                body_servico6ImagemSEO3 = '".$dados['body_servico6ImagemSEO3']."',
                body_servico6Titulo3 = '".$dados['body_servico6Titulo3']."',
                body_servico6Descricao3 = '".$dados['body_servico6Descricao3']."',
                body_servico6Imagem3 = '".$dados['body_servico6Imagem3']."',
                body_servico6_opcao4 = '".$dados['body_servico6_opcao4']."',
                body_servico6ImagemSEO4 = '".$dados['body_servico6ImagemSEO4']."',
                body_servico6Titulo4 = '".$dados['body_servico6Titulo4']."',
                body_servico6Descricao4 = '".$dados['body_servico6Descricao4']."',
                body_servico6Imagem4 = '".$dados['body_servico6Imagem4']."',
                body_servico6_opcao5 = '".$dados['body_servico6_opcao5']."',
                body_servico6ImagemSEO5 = '".$dados['body_servico6ImagemSEO5']."',
                body_servico6Titulo5 = '".$dados['body_servico6Titulo5']."',
                body_servico6Descricao5 = '".$dados['body_servico6Descricao5']."',
                body_servico6Imagem5 = '".$dados['body_servico6Imagem5']."',
                body_servico6_opcao6 = '".$dados['body_servico6_opcao6']."',
                body_servico6ImagemSEO6 = '".$dados['body_servico6ImagemSEO6']."',
                body_servico6Titulo6 = '".$dados['body_servico6Titulo6']."',
                body_servico6Descricao6 = '".$dados['body_servico6Descricao6']."',
                body_servico6Imagem6 = '".$dados['body_servico6Imagem6']."',
                body_servico6_opcao7 = '".$dados['body_servico6_opcao7']."',
                body_servico6ImagemSEO7 = '".$dados['body_servico6ImagemSEO7']."',
                body_servico6Titulo7 = '".$dados['body_servico6Titulo7']."',
                body_servico6Descricao7 = '".$dados['body_servico6Descricao7']."',
                body_servico6Imagem7 = '".$dados['body_servico6Imagem7']."',
                body_servico6_opcao8 = '".$dados['body_servico6_opcao8']."',
                body_servico6ImagemSEO8 = '".$dados['body_servico6ImagemSEO8']."',
                body_servico6Titulo8 = '".$dados['body_servico6Titulo8']."',
                body_servico6Descricao8 = '".$dados['body_servico6Descricao8']."',
                body_servico6Imagem8 = '".$dados['body_servico6Imagem8']."'
            
        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        return $resultado;
    }

    public function m_atualizarServico7($dados)
    {

        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

                        SET servico7_corTituloGenerico1 = '".$dados['servico7_corTituloGenerico1']."',
                            servico7_corDescricaoGenerico1 = '".$dados['servico7_corDescricaoGenerico1']."',
                            servico7_corBg = '".$dados['servico7_corBg']."'
                        WHERE (id_landingPage = '".$dados['id_landingPage']."');");
    
        $resultado = $this->db->query("UPDATE lp_modelosconfig_servico7 

            SET body_servico7 = '".$dados['body_servico7']."',
                body_conteudoGenerico7 = '".$dados['body_conteudoGenerico7']."',
                body_textoGenericoTitulo7 = '".$dados['body_textoGenericoTitulo7']."',
                body_textoGenericoDescricao7 = '".$dados['body_textoGenericoDescricao7']."',
                body_servico7_opcao1 = '".$dados['body_servico7_opcao1']."',
                body_servico7_opcao2 = '".$dados['body_servico7_opcao2']."',
                body_servico7_opcao3 = '".$dados['body_servico7_opcao3']."',
                body_servico7_opcao4 = '".$dados['body_servico7_opcao4']."',
                body_servico7_opcao5 = '".$dados['body_servico7_opcao5']."',
                body_servico7_opcao6 = '".$dados['body_servico7_opcao6']."',
                body_servico7_opcao7 = '".$dados['body_servico7_opcao7']."',
                body_servico7_opcao8 = '".$dados['body_servico7_opcao8']."',
                body_servico7Imagem1 = '".$dados['body_servico7Imagem1']."',
                body_servico7Imagem2 = '".$dados['body_servico7Imagem2']."',
                body_servico7Imagem3 = '".$dados['body_servico7Imagem3']."',
                body_servico7Imagem4 = '".$dados['body_servico7Imagem4']."',
                body_servico7Imagem5 = '".$dados['body_servico7Imagem5']."',
                body_servico7Imagem6 = '".$dados['body_servico7Imagem6']."',
                body_servico7Imagem7 = '".$dados['body_servico7Imagem7']."',
                body_servico7Imagem8 = '".$dados['body_servico7Imagem8']."',
                body_servico7ImagemSEO1 = '".$dados['body_servico7ImagemSEO1']."',
                body_servico7ImagemSEO2 = '".$dados['body_servico7ImagemSEO2']."',
                body_servico7ImagemSEO3 = '".$dados['body_servico7ImagemSEO3']."',
                body_servico7ImagemSEO4 = '".$dados['body_servico7ImagemSEO4']."',
                body_servico7ImagemSEO5 = '".$dados['body_servico7ImagemSEO5']."',
                body_servico7ImagemSEO6 = '".$dados['body_servico7ImagemSEO6']."',
                body_servico7ImagemSEO7 = '".$dados['body_servico7ImagemSEO7']."',
                body_servico7ImagemSEO8 = '".$dados['body_servico7ImagemSEO8']."'
            
        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        return $resultado;
    
    }

    public function m_atualizarServico8($dados)
    {
        
        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

            SET servico_maps_corTituloGenerico1 = '".$dados['servico_maps_corTituloGenerico1']."',
            servico_maps_corDescricaoGenerico1 = '".$dados['servico_maps_corDescricaoGenerico1']."',
            servico_maps_corBg = '".$dados['servico_maps_corBg']."'
            
        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        $resultado = $this->db->query("UPDATE lp_modelosconfig_servico_maps 

            SET body_maps = '".$dados['body_maps']."',
            body_conteudoGenerico_maps = '".$dados['body_conteudoGenerico_maps']."',
            body_maps_textoGenericoTitulo1 = '".$dados['body_maps_textoGenericoTitulo1']."',
            body_maps_textoGenericoDescricao1 = '".$dados['body_maps_textoGenericoDescricao1']."'
            
        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        return $resultado;
    }

    public function m_atualizarPaginaSucesso($dados)
    {
        
        $this->db->query("UPDATE lp_modelosconfig_personalizacao 

            SET servico_paginaSucesso_corTextoBotao     = '".$dados['servico_paginaSucesso_corTextoBotao']."',
            servico_paginaSucesso_corTextoHoverBotao    = '".$dados['servico_paginaSucesso_corTextoHoverBotao']."',
            servico_paginaSucesso_corTextoTitulo        = '".$dados['servico_paginaSucesso_corTextoTitulo']."',
            servico_paginaSucesso_corTextoDescricao     = '".$dados['servico_paginaSucesso_corTextoDescricao']."',
            servico_paginaSucesso_corFundoBotao         = '".$dados['servico_paginaSucesso_corFundoBotao']."',
            servico_paginaSucesso_corFundoHoverBotao    = '".$dados['servico_paginaSucesso_corFundoHoverBotao']."',
            servico_paginaSucesso_corBG                 = '".$dados['servico_paginaSucesso_corBG']."'
            
        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        $resultado = $this->db->query("UPDATE lp_modelosconfig_servico_paginasucesso 

        SET
        body_textoGenerico_paginaSucesso            = '".$dados['body_textoGenerico_paginaSucesso']."',
        body_textoGenericoDescricao_paginaSucesso   = '".$dados['body_textoGenericoDescricao_paginaSucesso']."',
        body_icone_paginaSucesso                    = '".$dados['body_icone_paginaSucesso']."',
        body_iconeBotao_paginaSucesso               = '".$dados['body_iconeBotao_paginaSucesso']."',
        body_linkBotao_paginaSucesso                = '".$dados['body_linkBotao_paginaSucesso']."',
        body_textoBotao_paginaSucesso               = '".$dados['body_textoBotao_paginaSucesso']."'

        WHERE (id_landingPage = '".$dados['id_landingPage']."');");

        return $resultado;
    
    }
}