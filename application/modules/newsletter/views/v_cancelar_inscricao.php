<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <title><?=$titulo?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Coderthemes" name="Josheffer Robert" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="/assets/images/favicon.png">
        <link href="/assets/assets_temp/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/assets_temp/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/assets_temp/css/app.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body class="authentication-bg authentication-bg-pattern">

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">

                            <div class="card-body p-4">
                                
                                <div class="text-center w-75 m-auto">
                                        <span><img src="https://i.imgur.com/SNLpEMJ.png" alt="" width="220"></span>
                                    <hr class="mt-3 mb-3">
                                    <h5 class="text-muted mb-4 mt-3" id="msgGenerica"><?=$msgGenerica?></h5>
                                </div>
                                
                                <form id="formCancelarInscricao">
                                    
                                    <input class="form-control" type="hidden" value="<?=$emailUsuario?>" name="emailUsuario" id="emailUsuario" required="required" >
                                    <input class="form-control" type="hidden" value="<?=$listaUsuario?>" name="listaUsuario" id="listaUsuario" required="required" >

                                    <div class="form-group mb-0 text-center" id="botaoCancelarNews">
                                        <?=$botaoCancelar?>
                                    </div>

                                </form>    

                            </div> 
                        </div>
                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <div id="msgErro"></div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer footer-alt">
            <?=date('Y')?> &copy; 
            
            <?php
                $dominio = explode("/", base_url());
                echo "www.".$dominio[2];
            ?>
        </footer>
        
        <?=$scriptsJS?>
        
    </body>
    
</html>