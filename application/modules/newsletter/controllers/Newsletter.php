<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->model("newsletter_model", "newsletter");
        $this->load->model("landingpage/landingpage_model", "landingpage");
        $this->load->model("landingpage/templatelandingpage_model", "template");
        $this->load->model("landingpage/Modelos_model", "modelos");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->library('permitiracesso');
        $this->load->library('emails');
        $this->load->library('relatorioleads');

        $this->load->model("empresa/Empresa_model", "empresa");
    }
    
	public function index()
	{
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        
        $view = "v_newsletter";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);

        $scripts = array(

            "titulo" => "Criar Campanhas | Solid System",

            "scriptsCSS" => "<link href='/assets/libs/bootstrap-select/bootstrap-select.min.css' rel='stylesheet' type='text/css'/>
                            <link href='/assets/painel/css/vendor/emailPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsJS" => "<script src='//cdn.ckeditor.com/4.12.1/full/ckeditor.js'></script>
                            <script src='/assets/painel/js/vendor/scriptsDisparos.js'></script>
                            <script src='/assets/libs/bootstrap-select/bootstrap-select.min.js'></script>",
        

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }
    
    public function v_grupos()
	{
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_grupos";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $scripts = array(

            "titulo" => "Grupos de e-mails para as campanhas | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsGrupo.js'></script>",
            "scriptsCSS" => "",
            "scriptsCSSacima" => ""
            
        );

		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
        $this->load->view('painel/Template/t_menuLateral');
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function c_cadastrarGrupo()
    {

        $retorno['msg'] = "";

        $nomeGrupo = $this->input->post('nomeGrupoCadastro');

        if(empty($nomeGrupo))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Digite um <strong>NOME</strong> <br>';
			echo json_encode($retorno);
            exit;
        }

        $grupoExiste = $this->newsletter->m_verificaGrupoExistente($nomeGrupo);

        if($grupoExiste)
        {
            $retorno['ret'] = false;
			$retorno['msg'].= ' Grupo: <strong>'.$nomeGrupo.'</strong> já existe, tente outro!';
			echo json_encode($retorno);
            exit;
        }
        
        $id = $this->newsletter->m_gerarCodigoGrupo();

        $dados['id_grupos'] = $id;
        $dados['nome_grupos'] = $nomeGrupo;

        $resultado = $this->newsletter->m_cadastrarGrupo($dados);

        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Grupo <strong>'.$nomeGrupo.'</strong> criado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o grupo, tente novamente mais tarde!';
			echo json_encode($retorno);
        }

    }

    public function c_listarGrupos()
    {

        $resultado = $this->newsletter->m_listarGrupos();

        echo json_encode($resultado);

    }

    public function c_alterarGrupos()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dados['id_grupos'] = $this->input->post('idGrupo');
        $dados['nome_grupos'] = $this->input->post('alterarNomeGrupo');

        if(empty($dados['id_grupos']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Grupo não encontrado!<br>';
			$sinal = true;
        }

        if(empty($dados['nome_grupos']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'Digite um <strong>NOME</strong> <br>';
			$sinal = true;
        }

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }

        if($this->newsletter->m_alterarGrupos($dados))
        {
            $retorno['ret'] = true;
            $retorno['msg'] = 'As informações foram atualizadas com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] .= 'Algo deu <strong>ERRADO</strong>, tente novamente em alguns minutos!!';
            echo json_encode($retorno);
        }

    }

    public function c_excluirGrupo()
    {

        $retorno['msg'] = "";

        $id = $this->input->post('idGrupoExcluir');

        if(empty($id))
		{
			$retorno['ret'] = false;
            $retorno['msg'] = 'Grupo não encontrado';
            echo json_encode($retorno);
            exit;
        }
        
        $resultado = $this->newsletter->m_verificaEmailEmGrupoExclusao($id);
        
        if($resultado)
        {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não é possível apagar grupos que tenham e-mails associados!';
            echo json_encode($retorno);
            exit;

        } 
        
        $result = $this->newsletter->m_excluirGrupo($id);

        if($result)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Grupo excluído com sucesso!';
            echo json_encode($retorno);

        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar excluir grupo, tente novamente mais tarde!';
            echo json_encode($retorno);
        }
            
        
    }

    public function v_emails()
    {
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_emails";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $scripts = array(

            "titulo" => "E-mails das campanhas |  Solid System",
            "scriptsCSS" => "<link href='/assets/libs/bootstrap-select/bootstrap-select.min.css' rel='stylesheet' type='text/css'/>
                            <link href='/assets/painel/css/vendor/emailPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsEmails.js'></script>
                            <script src='/assets/libs/bootstrap-select/bootstrap-select.min.js'></script>",
            
            "scriptsCSSacima" => ""
                                
        );

		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
        $this->load->view('painel/Template/t_menuLateral');
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function c_cadastrarEmailIndividual()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_emails'] = $this->newsletter->m_gerarCodigoEmail();
        $dados['news_nome'] = $this->input->post('nomeDestinatarioEmail');
        $dados['news_emails'] = $this->input->post('emailDestinatario');
        $dados['news_status'] = "1";
        $dados['news_grupoLista'] = $this->input->post('codigoGrupo');
        
        if(empty($dados['news_nome']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Digite um <strong>NOME</strong><br>';
			$sinal = true;
        }

        if(empty($dados['news_nome']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Digite um <strong>E-MAIL</strong>!<br>';
			$sinal = true;
        }

        if(empty($dados['news_grupoLista']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escolha um <strong>GRUPO</strong>!<br>';
			$sinal = true;
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $result = $this->newsletter->m_verificaEmailExiste($dados);
        
        if(!is_null($result))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' O E-mail já existe, tente outro!<br>';
			$sinal = true;
        }
        
        $resultado = $this->newsletter->m_cadastrarEmailIndividual($dados);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' E-mail: '.$dados['news_nome'].' cadastrado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não é possível criar cadastrar o e-mail, tente novamente em alguns minutos!';
            echo json_encode($retorno);
            
        }

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }
        
    }

    public function c_listarListasEmails()
    {
       $resultado = $this->newsletter->m_listarListasEmails();

       echo json_encode($resultado);
    } 
    
    public function c_cadastrarListaEmailCSV()
    {
        $arquivo = $_FILES["listaCSV"]['tmp_name'];
        
        $retorno['msg'] = "";
        $sinal = false;

        $arquivoVazio = $_FILES["listaCSV"]['error'];

        if($arquivoVazio === 4)
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Selecione um <strong>ARQUIVO</strong> para importar os dados!<br>';
            $sinal = true;
            
        }

        $grupo = $_POST["codigoGrupoImportar"];

        if(empty($grupo))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Selecione o <strong>GRUPO</strong> antes de importar os dados!<br>';
            $sinal = true;
            
        }

        $tipo = $_FILES["listaCSV"]['type'];

        if($tipo != 'application/vnd.ms-excel')
        {

            $retorno['ret'] = false;
			$retorno['msg'].= ' <strong>TIPO</strong> de <strong>ARQUIVO INVÁLIDO</strong> tente um arquivo com extensão .csv! <br>';
            $sinal = true;
            
        }

        $nomeArquivo = $_FILES["listaCSV"]['name'];

        $extensao = pathinfo($nomeArquivo, PATHINFO_EXTENSION);

        if($extensao != 'csv')
        {

            $retorno['ret'] = false;
			$retorno['msg'].= ' <strong>Arquivo inválido</strong>, somente arquivo do tipo .csv! <br>';
            $sinal = true;
        }

        if($extensao === '')
        {
            $retorno['ret'] = false;
			$retorno['msg'].= ' <strong>ARQUIVO SEM EXTENSÃO</strong> não é aceito, escolha um do tipo .csv! <br>';
            $sinal = true;
        }

        $arquivoTamanho = $_FILES["listaCSV"]['size'];

        if($arquivoTamanho > 50000000000)
        {

            $retorno['ret'] = false;
			$retorno['msg'].= ' <strong>ARQUIVO MUITO GRANDE</strong>, enviar arquivo com no <strong>MÁXIMO 3MB</strong>! <br>';
            $sinal = true;
        }

        $arquivoError = $_FILES["listaCSV"]['error'];

        if($arquivoError != 0)
        {
            
            $retorno['ret'] = false;
			$retorno['msg'].= ' <strong>ARQUIVO CORROMPIDO</strong>, verifique se foi <strong>gerado/exportado/salvo</strong> de forma correta e tente novamente!! <br>';
            $sinal = true;
        }

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }
        
        $objeto = fopen($arquivo, 'r');
        
        while (($dados = fgetcsv($objeto, 1000, ",")) !== FALSE)
        {  
            $id = $this->newsletter->m_gerarCodigoEmail();

            $nomeOK = ucwords(strtolower($dados[0])); 
            $email = trim(strtolower($dados[1]));
            
            $data['id_emails'] = $id;
            $data['news_nome'] = $nomeOK;
            $data['news_emails'] = $email;
            $data['news_status'] = '1';
            $data['news_grupoLista'] = $this->input->post('codigoGrupoImportar');
            
            $resultado = $this->newsletter->m_cadastrarListaEmailCSV($data);
            
        }

        if($resultado)
        {

            $retorno['ret'] = true;
            $retorno['msg']= ' Lista de e-mails importada com sucesso! <br>';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
            echo json_encode($retorno);

        }
        
    }

    public function c_excluirEmailsLista()
    {
        
        $id = $this->input->post('idGrupoExcluirLista');
        $nomeLista = $this->input->post('nomeGrupoExcluirLista');

        $retorno['msg'] = "";
        
        if(empty($id))
		{
			$retorno['ret'] = false;
			$retorno['msg'] = ' Lista não encontrada!<br>';
            echo json_encode($retorno);
            exit;
            
        }

        $resultado = $this->newsletter->m_excluirEmailsLista($id);

        if($resultado)
        {

            $retorno['ret'] = true;
            $retorno['msg']= ' Todos os e-mails da lista: <strong>'.$nomeLista.'</strong> foram apagados!! <br>';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
            echo json_encode($retorno);

        }

    }

    public function c_cadastrarNovaCampanha()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dados['nome_campanha']         = $this->input->post('nomeEmailEnviar');
        $dados['titulo_campanha']       = $this->input->post('tituloEmailEnviar');
        $dados['assunto_campanha']      = $this->input->post('assuntoEmailEnviar');
        $dados['destino_campanha']      = $this->input->post('codigoGrupoEnviar');
        $dados['mensagem_campanha']     = str_replace('"', '\'', $this->input->post('MensagemEnvio'));

        $dados['statusBotao_campanha']  = "0";
        $dados['id_templateEmail']      = "1";

        
        if(empty($dados['nome_campanha']))
		{

			$retorno['ret'] = false;
			$retorno['msg'].= ' Digite um <strong>NOME</strong> para a campanha<br>';
            $sinal = true;
            
        }

        if(empty($dados['titulo_campanha']))
		{

			$retorno['ret'] = false;
			$retorno['msg'].= ' Digite um <strong>TÍTULO</strong> para a campanha/Titulo do e-mail<br>';
            $sinal = true;
            
        }

        if(empty($dados['assunto_campanha']))
		{

			$retorno['ret'] = false;
			$retorno['msg'].= ' Digite um <strong>ASSUNTO</strong> para a campanha<br>';
            $sinal = true;
            
        }

        if(empty($dados['destino_campanha']))
		{
            
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escolha a <strong>LISTA</strong> para o disparo!<br>';
            $sinal = true;
            
        }

        if(empty($dados['mensagem_campanha']))
		{
            
			$retorno['ret'] = false;
			$retorno['msg'].= ' Digite a <strong>MENSAGEM</strong> para o corpo do E-mail!<br>';
            $sinal = true;
            
        }
        
        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }

        $dados['id_campanha'] = $this->newsletter->m_gerarCodigoNovaCampanha();

        $resultado = $this->newsletter->m_cadastrarNovaCampanha($dados);

        if($resultado)
        {

            $retorno['ret'] = true;
            $retorno['msg']= ' Campanha '.$dados['nome_campanha'].' criada com sucesso!! <br>';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
            echo json_encode($retorno);

        }
        
    }

    public function c_listarCampanhas()
    {

        $resultado = $this->newsletter->m_listarCampanhas();

        echo json_encode($resultado);

    } 
    
    public function c_listarCampanhasEnviadas()
    {

        $resultado = $this->newsletter->m_listarCampanhasEnviadas();
        
        echo json_encode($resultado);

    }


    public function c_alterarCampanha()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $id       = $this->input->post('idCampanhaAlterar');
        $campanha = $this->newsletter->m_buscarCampanha($id);
        
       if(!empty($campanha))
       {
           
          if($this->input->post('codigoGrupoEditar') === "")
          {
            $dados['destino_campanha'] = $campanha->destino_campanha;

          } else {

            $dados['destino_campanha']  = $this->input->post('codigoGrupoEditar');
            
          }
          
            $dados['id_campanha']           = $this->input->post('idCampanhaAlterar');
            $dados['nome_campanha']         = $this->input->post('alterarNomeCampanha');
            $dados['titulo_campanha']       = $this->input->post('alterarTituloCampanha');
            $dados['assunto_campanha']      = $this->input->post('alterarAssuntoCampanha');
            $dados['mensagem_campanha']     = str_replace('"', '\'', $this->input->post('MensagemEditar'));
            $dados['statusBotao_campanha']  = $campanha->statusBotao_campanha;


            if(empty($dados['nome_campanha']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' O <strong class="text-uppercase">nome</strong> da <strong class="text-uppercase">campanha</strong> não pode ser vazio<br>';
                $sinal = true;
                
            }

            if(empty($dados['titulo_campanha']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' O <strong class="text-uppercase">título</strong> do <strong class="text-uppercase">e-mail</strong> não pode ser vazio<br>';
                $sinal = true;
                
            }

            if(empty($dados['assunto_campanha']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' O <strong class="text-uppercase">assunto</strong> do <strong class="text-uppercase">e-mail</strong> não pode ser vazio<br>';
                $sinal = true;
                
            }

            if(empty($dados['mensagem_campanha']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' A <strong class="text-uppercase">menssagem</strong> do <strong class="text-uppercase">e-mail</strong> não pode ser vazio<br>';
                $sinal = true;
                
            }
            
            $resultado = $this->newsletter->m_atualizarCampanha($dados);

            if($resultado)
            {

                $retorno['ret'] = true;
                $retorno['msg']= ' Campanha <strong class="text-uppercase">'.$dados['nome_campanha'].'</strong> atualizada com sucesso!! <br>';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
                echo json_encode($retorno);

            }

       } else {

            $retorno['ret'] = false;
			$retorno['msg'].= ' Não foi possível encontrar a campanha';
            $sinal = true;
       }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

    }

    public function c_excluirCampanha()
    {
        
        $retorno['msg'] = "";
        $sinal = false;

        $id       = $this->input->post('idCampanhaExcluir');
        $campanha = $this->newsletter->m_buscarCampanha($id);

        if(!empty($campanha))
       {
        
            $resultado = $this->newsletter->m_deletarCampanha($id);

            if($resultado)
            {

                $retorno['ret'] = true;
                $retorno['msg']= ' Campanha excluída com sucesso!! <br>';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
                echo json_encode($retorno);

            }

        } else {

            $retorno['ret'] = false;
			$retorno['msg'].= ' Não foi possível encontrar a campanha';
            $sinal = true;
       }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function v_templateEmail()
    {
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        
        $view = "v_templatesEmail";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);

        $scripts = array(

            "titulo" => "Templates de e-mail para as campanhas | Solid System",

            "scriptsCSS" => "<link href='/assets/painel/css/vendor/emailPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsNewsletterTemplateEmail.js'></script>",
        

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function v_gerenciar_template()
    {
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        
        $view = "v_gerenciar_template";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Gerenciar templates de e-mail das campanhas | Solid System",

            "scriptsCSS" => "<link href='/assets/libs/bootstrap-select/bootstrap-select.min.css' rel='stylesheet' type='text/css'/>
                            <link href='/assets/painel/css/vendor/emailPersonalizado.css' rel='stylesheet' type='text/css'/>
                            <link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsJS" => "<script src='//cdn.ckeditor.com/4.12.1/full/ckeditor.js'></script>
                            <script src='/assets/painel/js/vendor/scriptsNewsletterModeloTemplateEmail.js'></script>
                            <script src='/assets/libs/bootstrap-select/bootstrap-select.min.js'></script>",
        

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function c_cadastrarNovoModeloTemplateEmail()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['nome_templateEmail']    = $this->input->post('nomeModeloTemplateEmail');
        $dados['html_templateEmail']    = str_replace('"', '\'', $this->input->post('codigoHtmlTempEmail'));
        $dados['status_templateEmail']  = '1';

        if(empty($dados['nome_templateEmail']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>nome</i></strong> do modelo! <br>';
			$sinal = true;
        }

        if(empty($dados['html_templateEmail']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>CÓDIGO HTML</i></strong> do modelo! <br>';
			$sinal = true;
        }
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $resultado = $this->newsletter->m_cadastrarNovoModeloTemplateEmail($dados);

        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Modelo <strong>'.$dados['nome_templateEmail'].'</strong> criado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar o modelo, tente novamente mais tarde!';
			echo json_encode($retorno);
        }
    }
    
    public function c_listarModelosTemplateEmails()
    {
        $resultado = $this->newsletter->m_listarModelosTemplateEmails();
        
        echo json_encode($resultado);

    }

    public function c_atualizarStatusModeloTemplate()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_templateEmail'] = $this->input->post('idAtualizar');
        
        $resultado = $this->newsletter->m_buscarModeloTemplateEmail($dados['id_templateEmail']);
        
        $statusAtual = $resultado->status_templateEmail;

        if($statusAtual === '1'){$dados['status_templateEmail'] = '0';} else {$dados['status_templateEmail'] = '1';}

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $result = $this->newsletter->m_atualizarStatusModeloTemplate($dados);

        if ($result)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] .= 'Status alterado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] .= 'Desculpa, não foi possível alterar o status!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarInfosModeloTemplate()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_templateEmail']      = $this->input->post('idModeloTemplateEmailAlterar');
        $dados['nome_templateEmail']    = $this->input->post('alterarNomeModeloTemplate');
        $dados['html_templateEmail']    = $this->input->post('htmlEditarTemplateModelo');
        
        $resultado = $this->newsletter->m_buscarModeloTemplateEmail($dados['id_templateEmail']);

        if(!$resultado)
        {
            $retorno['ret'] = false;
			$retorno['msg'].= ' Não foi possível alterar o modelo, modelo não encontrado!! <br>';
            $sinal = true;
            
        } else {

            if(empty($dados['nome_templateEmail']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>nome</i></strong> do modelo! <br>';
                $sinal = true;
            }

            if(empty($dados['html_templateEmail']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>CÓDIGO HTML</i></strong> do modelo! <br>';
                $sinal = true;
            }

            $result = $this->newsletter->m_atualizarInfosModeloTemplate($dados);

            if ($result)
            {
                
                $retorno['ret'] = true;
                $retorno['msg'] .= 'Informações do modelo de template alterado com sucesso!';
                echo json_encode($retorno);
                
            } else {
    
                $retorno['ret'] = false;
                $retorno['msg'] .= 'Desculpa, não foi possível alterar o modelo de template!';
                echo json_encode($retorno);
            }
        }
        
    }

    public function c_excluirModeloTemplateEmail()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id       = $this->input->post('idModeloExcluir');
        
        $modelo = $this->newsletter->m_buscarModeloTemplateEmail($id);

        if(!empty($modelo))
       {
        
            $resultado = $this->newsletter->m_excluirModeloTemplateEmail($id);

            if($resultado)
            {

                $retorno['ret'] = true;
                $retorno['msg']= ' Modelo de template excluído com sucesso!! <br>';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
                echo json_encode($retorno);

            }

        } else {

            $retorno['ret'] = false;
			$retorno['msg'].= ' Não foi possível encontrar o modelo de template';
            $sinal = true;
       }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_listarConfigsTemplateEmails()
    {
        $resultado = $this->newsletter->m_listarConfigsTemplateEmails();

        echo json_encode($resultado);
    }

    public function c_atualizarDadosConfigsTempEmail()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $id = '1';
        
        $dados['linkLogotipo_configs_template']     = $this->input->post('linkImagemLogo');
        $dados['linkImagem_configs_template']       = $this->input->post('linkLogotipo');
        $dados['nomeEmpresa_configs_template']      = $this->input->post('nomeEmpresa');
        $dados['endereco_configs_template']         = $this->input->post('enderecoEmpresa');
        $dados['dominio_configs_template']          = $this->input->post('dominioSite');
        $dados['linkDominio_configs_template']      = $this->input->post('linkDominio');
        $dados['linkFacebook_configs_template']     = $this->input->post('linkFacebook');
        $dados['linkInstagram_configs_template']    = $this->input->post('linkInstagram');
        $dados['linkWhatsApp_configs_template']     = $this->input->post('linkWhatsApp');
        
        $modelo = $this->newsletter->m_buscarConfigsTemplateEmail($id);
        
        if(!empty($modelo))
       {

            if(empty($dados['linkImagem_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>LINK</i></strong> da imagem! <br>';
                $sinal = true;
            }

            if(empty($dados['linkLogotipo_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>LINK</i></strong> para <strong class="text-uppercase"><i>redirecionar</i></strong> o usuário! <br>';
                $sinal = true;
            }

            if(empty($dados['nomeEmpresa_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>NOME DA EMPRESA</i></strong>! <br>';
                $sinal = true;
            }

            if(empty($dados['endereco_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>ENDEREÇO</i></strong> da empresa! <br>';
                $sinal = true;
            }
            
            if(empty($dados['dominio_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>domínio</i></strong> do site! <br>';
                $sinal = true;
            }

            if(empty($dados['linkDominio_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o <strong class="text-uppercase"><i>link</i></strong> do domínio! <br>';
                $sinal = true;
            }

            if(empty($dados['linkFacebook_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o link do <strong class="text-uppercase"><i>Facebook</i></strong>! <br>';
                $sinal = true;
            }

            if(empty($dados['linkInstagram_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o link do <strong class="text-uppercase"><i>Instagram</i></strong>! <br>';
                $sinal = true;
            }

            if(empty($dados['linkWhatsApp_configs_template']))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= ' Informe o link do <strong class="text-uppercase"><i>Whatsapp</i></strong>! <br>';
                $sinal = true;
            }

            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }

           $resultado = $this->newsletter->m_atualizarDadosConfigsTempEmail($dados);

            if($resultado)
            {

                $retorno['ret'] = true;
                $retorno['msg']= ' Configurações de template alteradas com sucesso!! <br>';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
                echo json_encode($retorno);

            }

        } else {

            $retorno['ret'] = false;
			$retorno['msg'].= ' Não foi possível encontrar os dados, tente novamente!';
            $sinal = true;
       }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_listarCampanhasTemplates()
    {
        $resultado = $this->newsletter->m_listarCampanhasTemplates();

        echo json_encode($resultado);
    }
    
    public function c_listarModelosTemplateEmailsTabela()
    {
        $resultado = $this->newsletter->m_listarModelosTemplateEmailsTabela();

        echo json_encode($resultado);
    }

    public function c_atualizarTemplateEmailCampanha()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dados['id_campanha'] = $this->input->post('selecionarCampanha');
        $dados['id_templateEmail'] = $this->input->post('selecionarTemplate');
        
        if(empty($dados['id_templateEmail']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Selecione o <strong class="text-uppercase"><i>template</i></strong>! <br>';
            $sinal = true;

        } 

        if(empty($dados['id_campanha']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Selecione a <strong class="text-uppercase"><i>campanha</i></strong>! <br>';
            $sinal = true;

        } 
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

        $campanha = $this->newsletter->m_buscarCampanha($dados['id_campanha']);
        
        $resultado = $this->newsletter->m_atualizarTemplateEmailCampanha($dados);

        if($resultado)
        {

            $retorno['ret'] = true;
            $retorno['msg']= ' Template da campanha <strong><i>'.$campanha->nome_campanha.'</i></strong> alterado com sucesso!! <br>';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
            echo json_encode($retorno);

        }
            
    }

    public function c_prepararCampanhaEnvio()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $destinoNews    = $this->input->post('grupoNewsletterPreparar');
        $id_campanha    = $this->input->post('prepararCodigoCampanha');

        $dadosCampanha = $this->newsletter->m_buscarCampanha($id_campanha);
        $arrayDeEmails = $this->newsletter->m_buscarEmails($destinoNews);
        
        if(empty($arrayDeEmails))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' O grupo/Lista de e-mails não contém e-mails, importe uma lista de e-mails e tente refazer o envio!<br>';
            $sinal = true;
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $templateEmail = $this->newsletter->m_buscarModeloTemplateEmail($dadosCampanha->id_templateEmail);
        $configsTemplateEmail = $this->newsletter->m_listarConfigsTemplateEmails();
        
        foreach ($arrayDeEmails as $row) 
        {
            $id = $this->newsletter->m_gerarCodigoEnvioNewsletter();
            
            $campanhaNome = $this->input->post('nomeNewsletterPreparar');
            
            $cancelarInscricao = base_url('/newsletter/cancelar_inscricao?email='.$row->news_emails.'&campanha='.$row->id_grupos);

            $imgLink = base_url('/newsletter/confirmarEmail?nome='.$row->news_nome.'&email='.$row->news_emails.'&campanha='.$campanhaNome);
           
            $mensagem1  = str_replace('{{nomePessoa}}',         $row->news_nome,                                        $templateEmail->html_templateEmail);
            $mensagem2  = str_replace('{{mensagem}}',           $this->input->post('mensagemNewsletterPreparar'),       $mensagem1);
            $mensagem3  = str_replace('{{nomeCampanha}}',       $this->input->post('nomeNewsletterPreparar'),           $mensagem2);
            $mensagem4  = str_replace('{{cancelarInscricao}}',  $cancelarInscricao,                                     $mensagem3);
            $mensagem5  = str_replace('{{linkWhatsApp}}',       $configsTemplateEmail->linkWhatsApp_configs_template,   $mensagem4);
            $mensagem6  = str_replace('{{linkInstagram}}',      $configsTemplateEmail->linkInstagram_configs_template,  $mensagem5);
            $mensagem7  = str_replace('{{linkFacebook}}',       $configsTemplateEmail->linkFacebook_configs_template,   $mensagem6);
            $mensagem8  = str_replace('{{linkImagemLogotipo}}', $configsTemplateEmail->linkLogotipo_configs_template,   $mensagem7);
            $mensagem9  = str_replace('{{linkImagem}}',         $configsTemplateEmail->linkImagem_configs_template,     $mensagem8);
            $mensagem10 = str_replace('{{dominio}}',            $configsTemplateEmail->dominio_configs_template,        $mensagem9);
            $mensagem11 = str_replace('{{linkDominio}}',        $configsTemplateEmail->linkDominio_configs_template,    $mensagem10);
            $mensagem12 = str_replace('{{nomeEmpresa}}',        $configsTemplateEmail->nomeEmpresa_configs_template,    $mensagem11);
            $mensagem13 = str_replace('{{endereco}}',           $configsTemplateEmail->endereco_configs_template,       $mensagem12);
            $mensagem14 = str_replace('{{contadorAbertura}}',   $imgLink,                                               $mensagem13);
            $mensagem15 = str_replace('{{titulo2}}',            $dadosCampanha->titulo_campanha,                        $mensagem14);
            
            $dados['id_envios']             = $id;
            $dados['id_campanha']           = $this->input->post('prepararCodigoCampanha');
            $dados['nome_campanha_envios']  = $this->input->post('nomeNewsletterPreparar');
            $dados['nomePessoa_envios']     = $row->news_nome;
            $dados['mensagem_envios']       = $mensagem15;
            $dados['assunto_envios']        = $this->input->post('assuntoNewsletterPreparar');
            $dados['destino_envios']        = $this->input->post('grupoNewsletterPreparar');
            $dados['emails_envios']         = strtolower($row->news_emails);
            $dados['statusEmails_envios']   = '0';
            
            $resultado = $this->newsletter->m_prepararNovaCampanha($dados);
            
        }
        
        if($resultado)
        {

            $retorno['ret'] = true;
            $retorno['msg']= ' Iniciou os disparos da campanha: <strong><i>'.$dados['nome_campanha_envios'].'</i></strong>! <br>';
            echo json_encode($retorno);
            
            $at['id_campanha']          = $this->input->post('prepararCodigoCampanha');
            $at['statusBotao_campanha'] = $this->input->post('botaoPrepararCampanha'); 

            $this->newsletter->m_atualizarBotao($at);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
            echo json_encode($retorno);

        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
    }


    public function v_cancelar_inscricao()
    {
        $dados['email']     = $this->input->get('email');
        $dados['campanha'] = $this->input->get('campanha');

        $resultado = $this->newsletter->m_buscarEmail($dados);
                
        if(!empty($resultado))
        {   
            if($resultado->news_status === '0')
            {
                $msgGenerica = "Olá <br><br><strong class='text-warning'><i>$resultado->news_nome</i></strong>,
                                <br><br>  Você já cancelou sua inscrição.";
                                
                $botaoCancelar = "";
                $email = "";
                $grupoLista = "";

            } else {

                $msgGenerica = "Olá <br><br><strong class='text-warning'><i>$resultado->news_nome</i></strong>, 
                                <br><br>é uma pena você não querer mais receber nossos informativos, 
                                    confirme usando o botão abaixo e você não receberá mais e-mails do CRF-GO";

                $botaoCancelar = "<button class='btn btn-danger btn-block' id='botaoCancelar' type='submit'> Sim, quero cancelar minha inscrição </button>";
                $email = $resultado->news_emails;
                $grupoLista = $resultado->news_grupoLista;
            }
            

        } else {

            $msgGenerica = "Olá, não encontramos seu e-mail em nosso sistema, verifique se seu e-mail é válido!";
            $botaoCancelar = "";
            $email = "";
            $grupoLista = "";
        }
        
        $infos = array(

            "titulo" => "Cancelar inscrição de newsletter | Solid System",
            
            "scriptsJS" => "<script src='/assets/js/jquery.min.js'></script>
                            <script src='/assets/js/vendor/scriptsCancelarInscricaoNewsletter.js'></script>
                            <script src='/assets/js/vendor.min.js'></script>
                            <script src='/assets/js/app.min.js'></script>",
            
            "msgGenerica" => $msgGenerica,

            "botaoCancelar" => $botaoCancelar,
            
            "emailUsuario" => $email,
            "listaUsuario" => $grupoLista

            
        );
        
        $this->load->view('v_cancelar_inscricao', $infos);
        
    }

    public function c_cancelar_inscricaoNews()
    {
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['email']    = $this->input->post('emailUsuario');
        $dados['lista']    = $this->input->post('listaUsuario');
        
        $resultado = $this->newsletter->m_cancelar_inscricaoNews($dados);
        
        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg']= ' Sua inscrição foi cancelada, você não receberá mais e-mails sobre a campanha em que você estava inscrito (a).';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg']= ' No momento não é possível remover seu e-mail da inscrição, tente novamente em alguns minutos! <br>';
            echo json_encode($retorno);

        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function v_disparos()
    {


        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        
        $view = "v_disparo";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);

        $scripts = array(

            "titulo" => "Acompanhar disparos | Solid System",

            "scriptsCSS" => "",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsAcompanharDisparos.js'></script>",
        
            "scriptsCSSacima" => ""
                                
        );
        
        $this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }

    public function c_listarEnviosAndamento()
    {
        $resultado = $this->newsletter->m_listarEnviosAndamento();
        
        if($resultado)
        { 
            $resp['x'] = true;
            $resp['r'] = $resultado;

        } else { 
            $resp['x'] = false;
            $resp['r'] = $resultado;
        }
        
        echo json_encode($resp);
    }

    public function c_listarEnviosEnviadosOK()
    {
        $resultado = $this->newsletter->m_listarEnviosEnviadosOK();

        echo json_encode($resultado);
    }

    public function c_disparo_newsletter()
    {
        $this->emails->dispara_emails();
    }

    public function c_confirmarEmail()
    {
        header("Content-Type: image/jpeg");
        readfile(base_url('assets/images/abrirEmail.png'));
        
        $dados['id_aberturas'] = $this->newsletter->m_gerarCodigoConfirmarEmail();
        
        $dados['nome_aberturas'] = $_GET['nome'];
        $dados['email_aberturas'] = $_GET['email'];
        $dados['aberto_aberturas'] = '1';
        $dados['campanha_aberturas'] = $_GET['campanha'];
        $dados['data_aberturas'] = date('Ymd');
        $dados['hora_aberturas'] = date('H:i:s');

        $this->newsletter->m_contadorAberturaEmail($dados);

    }

    public function c_relatorioEnvios()
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
        
        $this->relatorioleads->relatorioEnvios($id);
    }

    public function v_configuracoes_disparo()
	{
		$this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        
        $view = "v_configuracoes_disparo";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);

        $scripts = array(

            "titulo" => "Configurações da conta de disparo das campanhas | Solid System",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsConfigsDisparo.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
		
    }

    public function c_listarConfigs_disparo()
    {
        $resultado = $this->newsletter->m_dadosConfigEnvios();

        echo json_encode($resultado);
    }

    public function c_atualizarConfigsDisparo()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dados['id_configsEnvios']          = 1;
        $dados['host_configsEnvios']        = $this->input->post('host_configDisparo');
        $dados['usuario_configsEnvios']     = $this->input->post('usuario_configDisparo');
        $dados['senha_configsEnvios']       = $this->input->post('senha_configDisparo');
        $dados['debug_configsEnvios']       = $this->input->post('debug_configDisparo');
        $dados['SMTPSecure_configsEnvios']  = $this->input->post('seguranca_configDisparo');
        $dados['SMTPAuth_configsEnvios']    = $this->input->post('auth_configDisparo');
        $dados['porta_configsEnvios']       = $this->input->post('porta_configDisparo');
        $dados['Encoding_configsEnvios']    = $this->input->post('enconding_configDisparo');
        $dados['CharSet_configsEnvios']     = $this->input->post('charSet_configDisparo');
        
        if(empty($dados['host_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' O <strong class="text-uppercase"><i>host</i></strong> não pode ser vazio! <br>';
            $sinal = true;

        } 

        if(empty($dados['usuario_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' O <strong class="text-uppercase"><i>usuário</i></strong> não pode ser vazio! <br>';
            $sinal = true;

        } 

        if(empty($dados['senha_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' A <strong class="text-uppercase"><i>senha</i></strong> não pode ser vazia! <br>';
            $sinal = true;

        } 

        if(empty($dados['debug_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Selecione o tipo de <strong class="text-uppercase"><i>debug</i></strong>! <br>';
            $sinal = true;

        }
        
        if(empty($dados['SMTPSecure_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Selecione o tipo de <strong class="text-uppercase"><i>segurança</i></strong>! <br>';
            $sinal = true;

        } 

        if(empty($dados['SMTPAuth_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Selecione o tipo de <strong class="text-uppercase"><i>autenticação</i></strong>! <br>';
            $sinal = true;

        } 

        if(empty($dados['porta_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe a <strong class="text-uppercase"><i>porta</i></strong>! <br>';
            $sinal = true;

        } 

        if(empty($dados['Encoding_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o tipo de <strong class="text-uppercase"><i>encoding</i></strong>! <br>';
            $sinal = true;

        } 

        if(empty($dados['CharSet_configsEnvios']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= 'Informe o tipo de <strong class="text-uppercase"><i>charset</i></strong>! <br>';
            $sinal = true;

        } 
        
        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
        
        $resultado = $this->newsletter->m_atualizarConfigsDisparo($dados);

        if($resultado)
        {

            $retorno['ret'] = true;
            $retorno['msg']= ' Configurações de envio atualizadas com sucesso!! <br>';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg']= ' Algo deu errado, tente novamente em alguns minutos! <br>';
            echo json_encode($retorno);

        }
    }

}
