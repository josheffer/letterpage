$('#formCancelarInscricao').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cancelarInscricao(self);

});

function cancelarInscricao(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/newsletter/cancelar_inscricaoNews",
        dataType: 'json',

        beforeSend: function() {

            $("#botaoCancelar").prop("disabled", true).html('Aguarde...');

        },

        success: function(retorno) {

            if (retorno.ret === false) {

                $("#botaoCancelar").prop("disabled", false).html('Tentar novamente..');

                $('#msgErro').html(
                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<center>Erro! ' + retorno.msg + '</center>' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button> ' +
                    '</div>'
                );

            } else {

                $('#msgErro').html(
                    '<div class="alert alert-danger alert-dismissible bg-success text-white border-0 fade show" role="alert">' +
                    '<center>Tudo certo! ' + retorno.msg + '</center>' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button> ' +
                    '</div>'
                );

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                $("#botaoCancelarNews").html("");
                $("#msgGenerica").text("Agradecemos o momento em que estivemos juntos, sinta-se a vontade para voltar a receber nossas novidades quando quiser!!");

            }
        }
    });
}