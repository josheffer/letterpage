            <div class="left-side-menu">

                <div class="slimscroll-menu">
                    
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title">Menu</li>
                            
                                <?php
                                
                                    if(!empty($paginas))
                                    {
                                        for ($i=0; $i < count($paginas); $i++) : ?>

                                            <li>
                                                <a href="<?=$paginas[$i]->endereco_pagina?>">
                                                    <i class="<?=$paginas[$i]->icone_pagina?>"></i>
                                                    <span> <?=$paginas[$i]->nome_pagina?> </span>
                                                </a>
                                            </li>
                                        
                                        <?php endfor; 

                                    }
                                    
                                ?>
                                
                        </ul>

                    </div>
                    
                    <div class="clearfix"></div>

                </div>
                
            </div>