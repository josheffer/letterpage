<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Emails {

    public function __construct()
    {
        $this->CI =& get_instance();
        
        $this->CI->load->model("newsletter/newsletter_model", "newsletter", TRUE);

        $this->CI->load->library('conversores');
    }

    public function dispara_emails()
    {
        require 'vendor/autoload.php';

        $mail = new PHPMailer(true);

        $EnviarSituacaoEmails = '0';
			
		$dadosConfigEnvios = $this->CI->newsletter->m_dadosConfigEnvios();
		
        $mail->isSMTP();
		$mail->isHTML(true);

        $mail->SMTPDebug 	= $dadosConfigEnvios->debug_configsEnvios;
        $mail->Host       	= $dadosConfigEnvios->host_configsEnvios;
        $mail->SMTPAuth   	= $dadosConfigEnvios->SMTPAuth_configsEnvios;
        $mail->Username   	= $dadosConfigEnvios->usuario_configsEnvios;
		$mail->Password   	= $dadosConfigEnvios->senha_configsEnvios; 
        $mail->SMTPSecure 	= $dadosConfigEnvios->SMTPSecure_configsEnvios;
        $mail->Port       	= $dadosConfigEnvios->porta_configsEnvios;
        $mail->CharSet    	= $dadosConfigEnvios->CharSet_configsEnvios;
        $mail->Encoding   	= $dadosConfigEnvios->Encoding_configsEnvios;
		
        $arrayDeEmails = $this->CI->newsletter->m_buscarEmailsDisponiveis($EnviarSituacaoEmails);
        
        $codigo = 0;

        $retorno = array();
        
        if (count($arrayDeEmails) > 0) {

            foreach ($arrayDeEmails as $row) {
                
                $codigo = $row->id_campanha;
                
                try {

                    $mail->setFrom('newsletter@solidsystem.net.br', $row->nome_campanha_envios);
                    $mail->Subject = "Olá ".$row->nomePessoa_envios ."! ". $row->nome_campanha_envios;
                    $body    = $row->mensagem_envios;
                    $mail->msgHTML($body);
                    $mail->addAddress($row->emails_envios, $row->nomePessoa_envios);
                    
                    if($mail->send())
                    {
                        $dados['id_envios'] = $row->id_envios;
                        $dados['statusEmails_envios'] = '1';

                        $this->CI->newsletter->m_atualizarStatusEmailEnviado($dados);
                        
                        $ret = true;
                        $msg = 'Campanha <b><i>'.$row->nome_campanha_envios.'</i></b> enviada com sucesso para o e-mail: '.$row->emails_envios;
                        
                    }
                    
                } catch (Exception $e) {
                    
                    $dados['id_envios'] = $row->id_envios;
                    $dados['statusEmails_envios'] = '2';
                    
                    $this->CI->newsletter->m_atualizarStatusEmailEnviado($dados);
                     
                    echo $e->getMessage(); 

                    $mail->getSMTPInstance()->reset();
                    
                }
                
                $mail->clearAddresses();
                
            }
            
            $at['id_campanha'] = $codigo;
            $at['statusBotao_campanha'] = '2';
            
            $this->CI->newsletter->m_atualizarBotao($at);

            // echo json_encode($retorno);

        }
        
    }

    public function enviarEmail($dados)
    {
		
		$whatsapp = $this->CI->conversores->soNumeros($dados['telefone']);
		
		require_once( BASEPATH .'database/DB'. EXT );
		$db =& DB();
		$query = $db->get('site_contato_configs');
		$result = $query->result();

        require_once 'vendor/phpmailer/phpmailer/src/Exception.php';
        require_once 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
        require_once 'vendor/phpmailer/phpmailer/src/SMTP.php';

        $mail = new PHPMailer(true);
		$mail->CharSet = "utf-8";

        $enviada = false;

        $mensagem = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
		<html>
		
		<head>
			<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
		
			<meta property='og:title' content='Cadastro Talisman Dark Age' />
            <title>Cadastro Talisman Dark Age</title>
            <link href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' integrity='sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN' crossorigin='anonymous'>
			<style type='text/css'>
				/* Client-specific Styles */
				#outlook a {
					padding: 0;
				}
		
				/* Force Outlook to provide a 'view in browser' button. */
				body {
					width: 100% !important;
				}
		
				.ReadMsgBody {
					width: 100%;
				}
		
				.ExternalClass {
					width: 100%;
				}
		
				/* Force Hotmail to display emails at full width */
				body {
					-webkit-text-size-adjust: none;
				}
		
				/* Prevent Webkit platforms from changing default text sizes. */
		
				/* Reset Styles */
				body {
					margin: 0;
					padding: 0;
				}
		
				img {
					border: 0;
					height: auto;
					line-height: 100%;
					outline: none;
					text-decoration: none;
				}
		
				table td {
					border-collapse: collapse;
				}
		
				#backgroundTable {
					height: 100% !important;
					margin: 0;
					padding: 0;
					width: 100% !important;
				}
		
				/* Template Styles */
		
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */
		
				/**
					* @tab Page
					* @section background color
					* @tip Set the background color for your email. You may want to choose one that matches your company's branding.
					* @theme page
					*/
				body,
				#backgroundTable {
					/*@editable*/
					background-color: #EEEEEE;
				}
		
				/**
					* @tab Page
					* @section email border
					* @tip Set the border for your email.
					*/
				#templateContainer {
					/*@editable*/
					border: 1px solid #BBBBBB;
				}
		
				/**
					* @tab Page
					* @section heading 1
					* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
					* @style heading 1
					*/
				h1,
				.h1 {
					/*@editable*/
					color: #202020;
					display: block;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 34px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					margin-top: 0;
					margin-right: 0;
					margin-bottom: 10px;
					margin-left: 0;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Page
					* @section heading 2
					* @tip Set the styling for all second-level headings in your emails.
					* @style heading 2
					*/
				h2,
				.h2 {
					/*@editable*/
					color: #202020;
					display: block;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 30px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					margin-top: 0;
					margin-right: 0;
					margin-bottom: 10px;
					margin-left: 0;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Page
					* @section heading 3
					* @tip Set the styling for all third-level headings in your emails.
					* @style heading 3
					*/
				h3,
				.h3 {
					/*@editable*/
					color: #202020;
					display: block;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 26px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					margin-top: 0;
					margin-right: 0;
					margin-bottom: 10px;
					margin-left: 0;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Page
					* @section heading 4
					* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
					* @style heading 4
					*/
				h4,
				.h4 {
					/*@editable*/
					color: #202020;
					display: block;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 22px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					margin-top: 0;
					margin-right: 0;
					margin-bottom: 10px;
					margin-left: 0;
					/*@editable*/
					text-align: left;
				}
		
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */
		
				/**
					* @tab Header
					* @section header style
					* @tip Set the background color and border for your email's header area.
					* @theme header
					*/
				#templateHeader {
					/*@editable*/
					background-color: #FFFFFF;
					/*@editable*/
					border-bottom: 0;
				}
		
				/**
					* @tab Header
					* @section header text
					* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
					*/
				.headerContent {
					/*@editable*/
					color: #202020;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 34px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 100%;
					/*@editable*/
					padding: 0;
					/*@editable*/
					text-align: center;
					/*@editable*/
					vertical-align: middle;
				}
		
				/**
					* @tab Header
					* @section header link
					* @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
					*/
				.headerContent a:link,
				.headerContent a:visited,
				/* Yahoo! Mail Override */
				.headerContent a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #3498db;
					/*@editable*/
					font-weight: normal;
					/*@editable*/
					text-decoration: underline;
				}
		
				#headerImage {
					height: auto;
					max-width: 600px !important;
				}
		
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */
		
				/**
					* @tab Body
					* @section body style
					* @tip Set the background color for your email's body area.
					*/
				#templateContainer,
				.bodyContent {
					/*@editable*/
					background-color: #FFFFFF;
				}
		
				/**
					* @tab Body
					* @section body text
					* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
					* @theme main
					*/
				.bodyContent div {
					/*@editable*/
					color: #505050;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 14px;
					/*@editable*/
					line-height: 150%;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Body
					* @section body link
					* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
					*/
				.bodyContent div a:link,
				.bodyContent div a:visited,
				/* Yahoo! Mail Override */
				.bodyContent div a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #3498db;
					/*@editable*/
					font-weight: normal;
					/*@editable*/
					text-decoration: underline;
				}
		
				/**
					* @tab Body
					* @section data table style
					* @tip Set the background color and border for your email's data table.
					*/
				.templateDataTable {
					/*@editable*/
					background-color: #FFFFFF;
					/*@editable*/
					border: 1px solid #DDDDDD;
				}
		
				/**
					* @tab Body
					* @section data table heading text
					* @tip Set the styling for your email's data table text. Choose a size and color that is easy to read.
					*/
				.dataTableHeading {
					/*@editable*/
					background-color: #dceffc;
					/*@editable*/
					color: #3498db;
					/*@editable*/
					font-family: Helvetica;
					/*@editable*/
					font-size: 14px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 150%;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Body
					* @section data table heading link
					* @tip Set the styling for your email's data table links. Choose a color that helps them stand out from your text.
					*/
				.dataTableHeading a:link,
				.dataTableHeading a:visited,
				/* Yahoo! Mail Override */
				.dataTableHeading a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #FFFFFF;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					text-decoration: underline;
				}
		
				/**
					* @tab Body
					* @section data table text
					* @tip Set the styling for your email's data table text. Choose a size and color that is easy to read.
					*/
				.dataTableContent {
					/*@editable*/
					border-top: 1px solid #DDDDDD;
					/*@editable*/
					border-bottom: 0;
					/*@editable*/
					color: #202020;
					/*@editable*/
					font-family: Helvetica;
					/*@editable*/
					font-size: 12px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					line-height: 150%;
					/*@editable*/
					text-align: left;
				}
		
				/**
					* @tab Body
					* @section data table link
					* @tip Set the styling for your email's data table links. Choose a color that helps them stand out from your text.
					*/
				.dataTableContent a:link,
				.dataTableContent a:visited,
				/* Yahoo! Mail Override */
				.dataTableContent a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #202020;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					text-decoration: underline;
				}
		
				/**
					* @tab Body
					* @section button style
					* @tip Set the styling for your email's button. Choose a style that draws attention.
					*/
				.templateButton {
					/*@editable*/
					background-color: #3498db;
					/*@editable*/
					border: 0;
					border-collapse: separate !important;
					border-bottom: 4px solid #2980b9;
				}
		
				/**
					* @tab Body
					* @section button style
					* @tip Set the styling for your email's button. Choose a style that draws attention.
					*/
				.templateButton,
				.templateButton a:link,
				.templateButton a:visited,
				/* Yahoo! Mail Override */
				.templateButton a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #FFFFFF;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 15px;
					/*@editable*/
					font-weight: bold;
					/*@editable*/
					letter-spacing: -.5px;
					/*@editable*/
					line-height: 100%;
					text-align: center;
					text-decoration: none;
				}
		
				.bodyContent img {
					display: inline;
					height: auto;
				}
		
				/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */
		
				/**
					* @tab Footer
					* @section footer style
					* @tip Set the background color and top border for your email's footer area.
					* @theme footer
					*/
				#templateFooter {
					/*@editable*/
					background-color: #FFFFFF;
					/*@editable*/
					border-top: 0;
				}
		
				/**
					* @tab Footer
					* @section footer text
					* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
					* @theme footer
					*/
				.footerContent div {
					/*@editable*/
					color: #707070;
					/*@editable*/
					font-family: Arial;
					/*@editable*/
					font-size: 12px;
					/*@editable*/
					line-height: 125%;
					/*@editable*/
					text-align: center;
				}
		
				/**
					* @tab Footer
					* @section footer link
					* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
					*/
				.footerContent div a:link,
				.footerContent div a:visited,
				/* Yahoo! Mail Override */
				.footerContent div a .yshortcuts
		
				/* Yahoo! Mail Override */
					{
					/*@editable*/
					color: #3498db;
					/*@editable*/
					font-weight: normal;
					/*@editable*/
					text-decoration: underline;
				}
		
				.footerContent img {
					display: inline;
				}
		
				/**
					* @tab Footer
					* @section utility bar style
					* @tip Set the background color and border for your email's footer utility bar.
					* @theme footer
					*/
				#utility {
					/*@editable*/
					background-color: #FFFFFF;
					/*@editable*/
					border: 0;
				}
		
				/**
					* @tab Footer
					* @section utility bar style
					* @tip Set the background color and border for your email's footer utility bar.
					*/
				#utility div {
					/*@editable*/
					text-align: center;
				}
		
				#monkeyRewards img {
					max-width: 190px;
				}
			</style>
		</head>
		
		<body leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'>
			<center>
				<table border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='backgroundTable'>
					<tr>
						<td align='center' valign='top' style='padding-top:20px;'>
							<table border='0' cellpadding='0' cellspacing='0' width='600' id='templateContainer'>
								<tr>
									<td align='center' valign='top'>
		
										<table border='0' cellpadding='0' cellspacing='0' width='600' id='templateHeader' style='margin-top: 30px'>
											<tr>
												<td class='headerContent'>
		
													<img src='https://i.imgur.com/FwuYFu9.png' style='max-width:300px;'
														id='headerImage campaign-icon' mc:label='header_image'
														mc:edit='header_image' mc:allowdesigner mc:allowtext />
		
												</td>
											</tr>
										</table>
		
									</td>
								</tr>
								<tr>
									<td align='center' valign='top'>
		
										<table border='0' cellpadding='0' cellspacing='0' width='600' id='templateBody'>
											<tr>
												<td valign='top'>
		
													<table border='0' cellpadding='20' cellspacing='0' width='100%'>
														<tr>
															
															<td valign='top' class='bodyContent'>
																
																<hr><br>
		
																<div mc:edit='std_content00'>
																	<center style='color: #a47b4c;'><strong>".$dados['mensagem']."</strong></center>
																</div>
															</td>
														</tr>
														<tr>
															<td valign='top' style='padding-top:0; padding-bottom:0;'>
		
																<table border='0' cellpadding='10' cellspacing='0' width='100%' class='templateDataTable'>
                                                                    
                                                                    <tr mc:repeatable>
																		
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content00'>
																			Assunto:
																		</td>
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content01'>
																			".$dados['assunto']."
																		</td>
																	
                                                                    </tr>
                                                                    
                                                                    <tr mc:repeatable>
																		
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content00'>
																			Nome:
																		</td>
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content01'>
																			".$dados['nome']."
																		</td>
																	
                                                                    </tr>
                                                                    
																	<tr mc:repeatable>
																		
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content00'>
																			E-mail:
																		</td>
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content01'>
																			".$dados['email']."
																		</td>
																	
                                                                    </tr>
		
																	<tr mc:repeatable>
																		
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content00'>
																			Telefone:
																		</td>
																		<td valign='top' class='dataTableContent'
																			mc:edit='data_table_content01'>
																			<strong>".$dados['telefone']." &nbsp; &nbsp;<a href='https://api.whatsapp.com/send?phone=55".$whatsapp."' target='_blank' rel='noopener noreferrer'>[>>WhatsApp<<]</a></strong> 
																		</td>
																	
																	</tr>
																	
		
																</table>
		
															</td>
														</tr>
														
													</table>
		
												</td>
											</tr>
										</table>
		
									</td>
								</tr>
								<tr>
									<td align='center' valign='top'>
		
										<table border='0' cellpadding='10' cellspacing='0' width='600' id='templateFooter'>
											<tr>
												<td valign='top' class='footerContent'>
		
													<table border='0' cellpadding='10' cellspacing='0' width='100%'>
														
														<tr>
															<td valign='middle' id='utility'>
																<div mc:edit='std_utility'>
                                                                    <strong>
                                                                        <a href='https://goo.gl/maps/SVKqCGpt4oH4kodA6' target='_blank' style='text-decoration: none; color: #a47b4c;'>
                                                                            R. 83, 582 - St. Sul, Goiânia - GO, 74083-195
                                                                        </a>
                                                                    </strong>
																</div>
															</td>
														</tr>
		
													</table>
		
													<table border='0' cellpadding='10' cellspacing='0' width='100%'>
														
															<tr>
																<td valign='middle' id='utility'>
																	<div mc:edit='std_utility'>
																		&nbsp;
																		<a href='http://lekalekids.com/' target='_blank'>
																		   <span> www.lekalekids.com</span>
																		</a> &nbsp;
																	</div>
																</td>
															</tr>
														</table>
												</td>
											</tr>
										</table>
		
									</td>
								</tr>
							</table>
							<br />
						</td>
					</tr>
				</table>
			</center>
		</body>
		
		</html>";

        try {
			
			$mail->SMTPDebug    = $result[0]->debug_contato;                                       
            $mail->isSMTP();                                           
            $mail->Host         = $result[0]->host_contato; 
            $mail->SMTPAuth     = $result[0]->SMTPAuth_contato;                              
            $mail->Username     = $result[0]->username_contato;                     
            $mail->Password     = $result[0]->password_contato;                             
            $mail->SMTPSecure   = $result[0]->SMTPSecure_contato;                        
			$mail->Port         = $result[0]->port_contato;
			$mail->Encoding = 'base64';
			
            $mail->setFrom($dados['email'], "Mensagem de: ".$dados['nome']);

            $mail->addAddress($result[0]->addAddress_contato);

            // Content
            $mail->isHTML(true);
            $mail->Subject = html_entity_decode($dados['assunto']);
            $mail->Body    = $mensagem;

            $enviada = $mail->send();

        } catch (Exception $e) {
            
            $enviada = false;

        }

        return $enviada;

    }

}