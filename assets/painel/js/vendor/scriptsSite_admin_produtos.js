primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarProdutos();

function listarProdutos(){
    
    $.ajax(
      {
          
        url: "/dashboard/listagens/listarProdutosSite",
        
        ajax: 'dados.json',
        
        success: function(dados){
            
          var dados = JSON.parse(dados);
          
          if(dados.ativo_produto1 === '1'){$('#ativo_produto1').val(1).attr("checked", "checked");} else {$('#ativo_produto1').val(1);}
          $('#nome_produto1').val(dados.nome_produto1);
          $('#link_produto1').val(dados.link_produto1);
          $('#imagemproduto1').append('<img src="/assets/site/produtos/'+dados.imagem_produto1+'" class="img-fluid" data-product-featured-image>');

          if(dados.ativo_produto2 === '1'){$('#ativo_produto2').val(1).attr("checked", "checked");} else {$('#ativo_produto2').val(1);}
          $('#nome_produto2').val(dados.nome_produto2);
          $('#link_produto2').val(dados.link_produto2);
          $('#imagemproduto2').append('<img src="/assets/site/produtos/'+dados.imagem_produto2+'" class="img-fluid" data-product-featured-image>');

          if(dados.ativo_produto3 === '1'){$('#ativo_produto3').val(1).attr("checked", "checked");} else {$('#ativo_produto3').val(1);}
          $('#nome_produto3').val(dados.nome_produto3);
          $('#link_produto3').val(dados.link_produto3);
          $('#imagemproduto3').append('<img src="/assets/site/produtos/'+dados.imagem_produto3+'"class="img-fluid" data-product-featured-image>');

          if(dados.ativo_produto4 === '1'){$('#ativo_produto4').val(1).attr("checked", "checked");} else {$('#ativo_produto4').val(1);}
          $('#nome_produto4').val(dados.nome_produto4);
          $('#link_produto4').val(dados.link_produto4);
          $('#imagemproduto4').append('<img src="/assets/site/produtos/'+dados.imagem_produto4+'" class="img-fluid" data-product-featured-image>');
          
          $('#seo_produto1').val(dados.seo_produto1);
          $('#seo_produto2').val(dados.seo_produto2);
          $('#seo_produto3').val(dados.seo_produto3);
          $('#seo_produto4').val(dados.seo_produto4);

        }
      }
    );
};
  
$('#formAtualizarProdutos').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarProdutos(self);
});

function atualizarProdutos(dados){

  var form = $('#formAtualizarProdutos')[0];

  var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarProdutos",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarProdutos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarProdutos();
          
        }
      }
    }
  );
}
