<div class="row no-gutters">

    <div class="col-md-6 col-xl-4 mt-2">
        
        <a href="/dashboard/newsletter">
            <div class="bg-soft-primary mb-0">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark"><span>Newsletter</span></h3>
                            <p class="text-warning mb-1 text-truncate">Configure e faça os disparos!</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>

    <div class="col-md-6 col-xl-4 mt-2">
        
        <a href="/dashboard/newsletter/grupos">
            <div class="bg-soft-info mb-0">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                        <h3 class="text-dark"><span>Grupos</span></h3>
                            <p class="text-warning mb-1 text-truncate">Configure os grupos para coletar os leads</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        
    </div>
    
    <div class="col-md-6 col-xl-4 mt-2">
        
        <a href="/dashboard/newsletter/emails">
            <div class="bg-soft-success mb-0">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark"><span>E-mails</span></h3>
                            <p class="text-warning mb-1 text-truncate">Faça a importação ou exportação de listas</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        
    </div>
    
    <div class="col-md-4 col-xl-4 mt-2">
        
        <a href="/dashboard/newsletter/disparos">
            <div class="bg-soft-pink mb-0">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark"><span>Disparos</span></h3>
                            <p class="text-warning mb-1 text-truncate">Acompanhe em tempo real os disparos</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        
    </div>

    <div class="col-md-4 col-xl-4 mt-2">
        
        <a href="/dashboard/newsletter/templateEmail">
            <div class="bg-soft-red mb-0">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark"><span>Templates de e-mail</span></h3>
                            <p class="text-warning mb-1 text-truncate">Escolha o template para sua campanha</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        
    </div>

    <div class="col-md-4 col-xl-4 mt-2">
        
        <a href="/dashboard/newsletter/gerenciar_template">
            <div class="bg-pink mb-0">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark"><span>Gerenciar Templates</span></h3>
                            <p class="text-warning mb-1 text-truncate">Crie templates personalizados</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        
    </div>

    <div class="col-md-12 col-xl-12 mt-2">
        
        <a href="/dashboard/newsletter/configuracoes_disparo">
            <div class="bg-danger">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <h3 class="text-dark"><span>Configurações</span></h3>
                            <p class="text-warning mb-1 text-truncate">E-mail, senha, porta, servidor..</p>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        
    </div>
    

</div>

<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>