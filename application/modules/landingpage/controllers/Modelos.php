<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelos extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->model("LandingPage_model", "landingpage");
        $this->load->model("TemplateLandingPage_model", "template");
        $this->load->model("Modelos_model", "modelos");
        
    }

    public function c_listarHeadSEO()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $dados_headSEO = $this->modelos->buscarDados_headSEO($id);
        
        echo json_encode($dados_headSEO);
        
    }

    public function c_atualizarHeadSEO()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dados['id_modelo'] = $this->input->post('idModelo_headSEO');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_headSEO');
        $dados['topo_head_css'] = $this->input->post('cssHeadTOPO');
        $dados['topo_head_js'] = filter_var($this->input->post('jsHead'), FILTER_SANITIZE_MAGIC_QUOTES);
        $dados['topo_head_SEO_keywords'] = $this->input->post('SEOKeywordsHead');
        $dados['topo_head_SEO_description'] = $this->input->post('SEODescriptionsHead');
        $dados['topo_head_SEO_description2'] = $this->input->post('SEODescriptionsHead2');
        $dados['topo_head_SEO_description3'] = $this->input->post('SEODescriptionsHead3');
        $dados['topo_head_coordenadasMaps'] = $this->input->post('SEOCoordGMaps');
        $dados['topo_head_cidade'] = $this->input->post('SEOCidade');
        $dados['topo_head_estado'] = $this->input->post('SEOEstado');
        
        if(empty($dados['id_modelo']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'Não foi possível encontrar o modelo, tente novamente em alguns minutos<br>';
            $sinal = true;
            
        }

        if(empty($dados['topo_head_SEO_keywords']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' As <strong>Keywords</strong> não pode ser vazias<br>';
            $sinal = true;
            
        }

        if(empty($dados['topo_head_SEO_description']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' A <strong>Descriptions</strong> não pode ser vazia<br>';
            $sinal = true;
            
        }

        if(empty($dados['topo_head_SEO_description2']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' A <strong>Descriptions 2</strong> não pode ser vazia<br>';
            $sinal = true;
            
        }

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }

        if($_FILES['SEOImagem']['size'] === 0 )
        {
            $resultado = $this->modelos->buscarDados_headSEO($dados['id_landingPage']);
            
            $dados['topo_head_SEO_imagem'] = $resultado->topo_head_SEO_imagem;
        }

        if(!empty($_FILES['SEOImagem']['size']))
        {
            $result = $this->modelos->buscarDados_headSEO($dados['id_landingPage']);
            
            $imagemSEO = 'assets/images/SEO/'.$result->topo_head_SEO_imagem;

            if (file_exists($imagemSEO)) {

                unlink('assets/images/SEO/'.$result->topo_head_SEO_imagem);

                $config['upload_path'] = './assets/images/SEO';
                $config['file_name'] = "img_SEO_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['topo_head_SEO_imagem'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('SEOImagem');

            } else {

                $config['upload_path'] = './assets/images/SEO';
                $config['file_name'] = "img_SEO_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['topo_head_SEO_imagem'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('SEOImagem');

            }
            
        }

        $resultado = $this->modelos->m_atualizarHeadSEO($dados);

        if ($resultado)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados de <strong>Head e SEO</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados de <strong>Head e SEO</strong>!';
            echo json_encode($retorno);
        }
        

    }

    public function c_listarLogoMenuSlide()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
        
        $dados_logoMenuSlide = $this->modelos->m_listarLogoMenuSlide($id);

        echo json_encode($dados_logoMenuSlide);
    }

    public function c_atualizarformLogoMenuSlide()
    {
        $dados['id_modelo'] = $this->input->post('idModeloLogoMenuSlide');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_LogoMenuSlide');
        $dados['body_logo1'] = $this->input->post('logotipoMenu');
        $dados['body_slide'] = $this->input->post('ativaDesativaSlideLP');
        $dados['body_slide_titulo1'] = $this->input->post('tituloBanner');
        $dados['body_slide_botao1'] = $this->input->post('ativaDesativaSlideLP');
        $dados['body_slide_descricao1'] = $this->input->post('descricaoBanner');
        $dados['body_slide_botao1'] = $this->input->post('botaoSlideAtivaDesativa');
        $dados['body_slide_botaoTexto1'] = $this->input->post('textoBotaoSlideBanner');
        $dados['body_slide_botaoLink1'] = $this->input->post('linkBotaoSlideBanner');
        $dados['corFundoMenu'] = $this->input->post('corFundoMenu');
        $dados['corFundoSlide'] = $this->input->post('corFundoBannerSlide');
        $dados['corTextoBotaoSlide'] = $this->input->post('corTextoBotaoSlideBanner');
        $dados['corTextoHoverBotaoSlide'] = $this->input->post('corTextoHoverBotaoSlideBanner');
        $dados['corFundoBotaoSlide'] = $this->input->post('corfundoBotaoSlideBanner');
        $dados['corFundoHoverBotaoSlide'] = $this->input->post('corFundoHoverBotaoSlideBanner');
        $dados['corTextoTituloSlide'] = $this->input->post('corTextoTituloBanner');
        $dados['corTextoDescricaoSlide'] = $this->input->post('corTextoDescricaoBanner');
        $dados['corFundoSlide1'] = $this->input->post('atualizarCorFundoSlide1');
        $dados['body_slide_imagem2_SEO'] = $this->input->post('seoBannerSlide2');
        
        if(empty($this->input->post('ativoBannerSlide1'))){$dados['body_slide1']="0";} else {$dados['body_slide1']="1";}
        if(empty($this->input->post('imagemFundoSlideAtivadoDesativado'))){$dados['body_imagemFundoSlide']="0";} else {$dados['body_imagemFundoSlide']="1";}
        if(empty($this->input->post('imagemPrincipalAtivadoDesativado'))){$dados['body_imagemPrincipalSlide']="0";} else {$dados['body_imagemPrincipalSlide']="1";}
        
        if($_FILES['imagemFundoSlide']['size'] === 0 )
        {
            $resultadoImagemFundo = $this->modelos->m_buscarLogoMenuSlide($dados['id_landingPage']);
            
            $dados['body_slide_imagemFundoSlide'] = $resultadoImagemFundo->body_slide_imagemFundoSlide;

        } elseif (!empty($_FILES['imagemFundoSlide']['size']))
        {
            $result = $this->modelos->m_buscarLogoMenuSlide($dados['id_landingPage']);
            
            $imagemFundoBanner = 'assets/images/slide/'.$result->body_slide_imagemFundoSlide;

            if (file_exists($imagemFundoBanner)) {

                unlink('assets/images/slide/'.$result->body_slide_imagemFundoSlide);

                $config['upload_path'] = './assets/images/slide';
                $config['file_name'] = "img_slideFundo_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_slide_imagemFundoSlide'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemFundoSlide');

            } else {

                $config['upload_path'] = './assets/images/slide';
                $config['file_name'] = "img_slideFundo_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_slide_imagemFundoSlide'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemFundoSlide');

            }
            
        }

        if($_FILES['imagemPrincipal']['size'] === 0 )
        {
            $resultadoImagemPrincipal = $this->modelos->m_buscarLogoMenuSlide($dados['id_landingPage']);
            
            $dados['body_slide_imagem1'] = $resultadoImagemPrincipal->body_slide_imagem1;
            
        } elseif (!empty($_FILES['imagemPrincipal']['size']))
        {
            $result = $this->modelos->m_buscarLogoMenuSlide($dados['id_landingPage']);
            
            $imagemPrincipalBanner = 'assets/images/slide/'.$result->body_slide_imagem1;

            if (file_exists($imagemPrincipalBanner)) {

                unlink('assets/images/slide/'.$result->body_slide_imagem1);

                $config['upload_path'] = './assets/images/slide';
                $config['file_name'] = "img_slidePrincipal_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_slide_imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemPrincipal');

            } else {

                $config['upload_path'] = './assets/images/slide';
                $config['file_name'] = "img_slidePrincipal_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_slide_imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemPrincipal');

            }
            
        }
        
        if($_FILES['atualizarImagemSlide1']['size'] === 0 )
        {
            $resultadoImagemSlide1 = $this->modelos->m_buscarLogoMenuSlide($dados['id_landingPage']);
            
            $dados['body_slide_imagem2'] = $resultadoImagemSlide1->body_slide_imagem2;
            
        } elseif (!empty($_FILES['atualizarImagemSlide1']['size']))
        {
            $result = $this->modelos->m_buscarLogoMenuSlide($dados['id_landingPage']);
            
            $imagemBannerSlide1 = 'assets/images/slide/'.$result->body_slide_imagem2;

            if (file_exists($imagemBannerSlide1)) {

                unlink('assets/images/slide/'.$result->body_slide_imagem2);

                $config['upload_path'] = './assets/images/slide';
                $config['file_name'] = "img_slide1_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_slide_imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('atualizarImagemSlide1');

            } else {

                $config['upload_path'] = './assets/images/slide';
                $config['file_name'] = "img_slide1_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_slide_imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('atualizarImagemSlide1');

            }
            
        }
        
        $resultado = $this->modelos->m_atualizarformLogoMenuSlide($dados);

        if ($resultado)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = 'Dados da <strong>Logo, Menu e Slide</strong> atualizados com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Falha ao tentar alterar os dados, tente novamente em alguns minutos!';
            echo json_encode($retorno);
        }
    }

    public function c_listarForms()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
        
        $dados_logoMenuSlide = $this->modelos->m_listarForms($id);

        echo json_encode($dados_logoMenuSlide);
    }

    public function c_atualizarFormularios()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dados['id_modelo']                                     = $this->input->post('idModeloFormularios');
        $dados['id_landingPage']                                     = $this->input->post('idLandingPageFormularios');

        $dados['body_servico1_form1']                           = $this->input->post('formFormulario1');
        $dados['body_servico1_form1_corFundoBotao']             = $this->input->post('corFundoBotaoForm1');
        $dados['body_servico1_form1_textBotao']                 = $this->input->post('textoBotaoForm1');
        $dados['body_servico1_form1_corFundoForm']              = $this->input->post('corFundoFormularioForm1');
        $dados['body_servico1_form1_corTextBotao']              = $this->input->post('corTextoBotaoForm1');
        
        $dados['body_servico1_form2']                           = $this->input->post('formFormulario2');
        $dados['body_servico1_form2_corFundoBotao']             = $this->input->post('corFundoBotaoForm2');
        $dados['body_servico1_form2_textBotao']                 = $this->input->post('textoBotaoForm2');
        $dados['body_servico1_form2_corFundoForm']              = $this->input->post('corFundoFormularioForm2');
        $dados['body_servico1_form2_corTextBotao']              = $this->input->post('corTextoBotaoForm2');
        
        $dados['body_servico2_form3']                           = $this->input->post('formFormulario3');
        $dados['body_servico2_form3_corFundoBotao']             = $this->input->post('corFundoBotaoForm3');
        $dados['body_servico2_form3_textBotao']                 = $this->input->post('textoBotaoForm3');
        $dados['body_servico2_form3_corFundoForm']              = $this->input->post('corFundoFormularioForm3');
        $dados['body_servico2_form3_corTextBotao']              = $this->input->post('corTextoBotaoForm3');
        
        $dados['body_servico3_form4']                           = $this->input->post('formFormulario4');
        $dados['body_servico3_form4_corFundoBotao']             = $this->input->post('corFundoBotaoForm4');
        $dados['body_servico3_form4_textBotao']                 = $this->input->post('textoBotaoForm4');
        $dados['body_servico3_form4_corFundoForm']              = $this->input->post('corFundoFormularioForm4');
        $dados['body_servico3_form4_corTextBotao']              = $this->input->post('corTextoBotaoForm4');

        $dados['body_servico4_form5']                           = $this->input->post('formFormulario5');
        $dados['body_servico4_form5_corFundoBotao']             = $this->input->post('corFundoBotaoForm5');
        $dados['body_servico4_form5_textBotao']                 = $this->input->post('textoBotaoForm5');
        $dados['body_servico4_form5_corFundoForm']              = $this->input->post('corFundoFormularioForm5');
        $dados['body_servico4_form5_corTextBotao']              = $this->input->post('corTextoBotaoForm5');
        
        $dados['body_servico5_form6']                           = $this->input->post('formFormulario6');
        $dados['body_servico5_form6_corFundoBotao']             = $this->input->post('corFundoBotaoForm6');
        $dados['body_servico5_form6_textBotao']                 = $this->input->post('textoBotaoForm6');
        $dados['body_servico5_form6_corFundoForm']              = $this->input->post('corFundoFormularioForm6');
        $dados['body_servico5_form6_corTextBotao']              = $this->input->post('corTextoBotaoForm6');

        $dados['body_servico6_form7']                           = $this->input->post('formFormulario7');
        $dados['body_servico6_form7_corFundoBotao']             = $this->input->post('corFundoBotaoForm7');
        $dados['body_servico6_form7_textBotao']                 = $this->input->post('textoBotaoForm7');
        $dados['body_servico6_form7_corFundoForm']              = $this->input->post('corFundoFormularioForm7');
        $dados['body_servico6_form7_corTextBotao']              = $this->input->post('corTextoBotaoForm7'); 
        
        $dados['body_servico7_form8']                           = $this->input->post('formFormulario8');
        $dados['body_servico7_form8_corFundoBotao']             = $this->input->post('corFundoBotaoForm8');
        $dados['body_servico7_form8_textBotao']                 = $this->input->post('textoBotaoForm8');
        $dados['body_servico7_form8_corFundoForm']              = $this->input->post('corFundoFormularioForm8');
        $dados['body_servico7_form8_corTextBotao']              = $this->input->post('corTextoBotaoForm8');

        $dados['body_servico_RedesSociais_form']                = $this->input->post('formFormularioRedesSociais');
        $dados['body_servico_RedesSociais_form_corFundoBotao']  = $this->input->post('corFundoBotaoFormRedesSociais');
        $dados['body_servico_RedesSociais_form_textBotao']      = $this->input->post('textoBotaoFormRedesSociais');
        $dados['body_servico_RedesSociais_form_corFundoForm']   = $this->input->post('corFundoFormularioFormRedesSociais');
        $dados['body_servico_RedesSociais_form_corTextBotao']   = $this->input->post('corTextoBotaoFormRedesSociais');
        
        $dados['nosConheceuOpcao1']   = $this->input->post('nosConheceuOpcao1');
        $dados['nosConheceuOpcao2']   = $this->input->post('nosConheceuOpcao2');
        $dados['nosConheceuOpcao3']   = $this->input->post('nosConheceuOpcao3');
        $dados['nosConheceuOpcao4']   = $this->input->post('nosConheceuOpcao4');
        $dados['nosConheceuOpcao5']   = $this->input->post('nosConheceuOpcao5');
        $dados['nosConheceuOpcao6']   = $this->input->post('nosConheceuOpcao6');
        $dados['nosConheceuOpcao7']   = $this->input->post('nosConheceuOpcao7');
        $dados['nosConheceuOpcao8']   = $this->input->post('nosConheceuOpcao8');
        
        if(empty($this->input->post('nosConheceuOpcao1_visivel'))){$dados['nosConheceuOpcao1_visivel']="0";} else {$dados['nosConheceuOpcao1_visivel']="1";}
        if(empty($this->input->post('nosConheceuOpcao2_visivel'))){$dados['nosConheceuOpcao2_visivel']="0";} else {$dados['nosConheceuOpcao2_visivel']="1";}
        if(empty($this->input->post('nosConheceuOpcao3_visivel'))){$dados['nosConheceuOpcao3_visivel']="0";} else {$dados['nosConheceuOpcao3_visivel']="1";}
        if(empty($this->input->post('nosConheceuOpcao4_visivel'))){$dados['nosConheceuOpcao4_visivel']="0";} else {$dados['nosConheceuOpcao4_visivel']="1";}
        if(empty($this->input->post('nosConheceuOpcao5_visivel'))){$dados['nosConheceuOpcao5_visivel']="0";} else {$dados['nosConheceuOpcao5_visivel']="1";}
        if(empty($this->input->post('nosConheceuOpcao6_visivel'))){$dados['nosConheceuOpcao6_visivel']="0";} else {$dados['nosConheceuOpcao6_visivel']="1";}
        if(empty($this->input->post('nosConheceuOpcao7_visivel'))){$dados['nosConheceuOpcao7_visivel']="0";} else {$dados['nosConheceuOpcao7_visivel']="1";}
        if(empty($this->input->post('nosConheceuOpcao8_visivel'))){$dados['nosConheceuOpcao8_visivel']="0";} else {$dados['nosConheceuOpcao8_visivel']="1";}

        $dados['body_servicoForm_tituloForm1']      = $this->input->post('tituloForm1');
        $dados['body_servicoForm_descricaoForm1']   = $this->input->post('descricaoForm1');
        $dados['body_servicoForm_tituloForm2']      = $this->input->post('tituloForm2');
        $dados['body_servicoForm_descricaoForm2']   = $this->input->post('descricaoForm2');
        $dados['body_servicoForm_tituloForm3']      = $this->input->post('tituloForm3');
        $dados['body_servicoForm_descricaoForm3']   = $this->input->post('descricaoForm3');
        $dados['body_servicoForm_tituloForm4']      = $this->input->post('tituloForm4');
        $dados['body_servicoForm_descricaoForm4']   = $this->input->post('descricaoForm4');
        $dados['body_servicoForm_tituloForm5']      = $this->input->post('tituloForm5');
        $dados['body_servicoForm_descricaoForm5']   = $this->input->post('descricaoForm5');
        $dados['body_servicoForm_tituloForm6']      = $this->input->post('tituloForm6');
        $dados['body_servicoForm_descricaoForm6']   = $this->input->post('descricaoForm6');
        $dados['body_servicoForm_tituloForm7']      = $this->input->post('tituloForm7');
        $dados['body_servicoForm_descricaoForm7']   = $this->input->post('descricaoForm7');
        $dados['body_servicoForm_tituloForm8']      = $this->input->post('tituloForm8');
        $dados['body_servicoForm_descricaoForm8']   = $this->input->post('descricaoForm8');
        $dados['body_servicoForm_tituloFormRS']     = $this->input->post('tituloFormRS');
        $dados['body_servicoForm_descricaoFormRS']  = $this->input->post('descricaoFormRS');

        $dados['body_servico_form1_corTitulo']      = $this->input->post('corTituloForm1');
        $dados['body_servico_form1_corDescricao']   = $this->input->post('corDescricaoForm1');
        $dados['body_servico_form2_corTitulo']      = $this->input->post('corTituloForm2');
        $dados['body_servico_form2_corDescricao']   = $this->input->post('corDescricaoForm2');
        $dados['body_servico_form3_corTitulo']      = $this->input->post('corTituloForm3');
        $dados['body_servico_form3_corDescricao']   = $this->input->post('corDescricaoForm3');
        $dados['body_servico_form4_corTitulo']      = $this->input->post('corTituloForm4');
        $dados['body_servico_form4_corDescricao']   = $this->input->post('corDescricaoForm4');
        $dados['body_servico_form5_corTitulo']      = $this->input->post('corTituloForm5');
        $dados['body_servico_form5_corDescricao']   = $this->input->post('corDescricaoForm5');
        $dados['body_servico_form6_corTitulo']      = $this->input->post('corTituloForm6');
        $dados['body_servico_form6_corDescricao']   = $this->input->post('corDescricaoForm6');
        $dados['body_servico_form7_corTitulo']      = $this->input->post('corTituloForm7');
        $dados['body_servico_form7_corDescricao']   = $this->input->post('corDescricaoForm7');
        $dados['body_servico_form8_corTitulo']      = $this->input->post('corTituloForm8');
        $dados['body_servico_form8_corDescricao']   = $this->input->post('corDescricaoForm8');
        $dados['body_servico_formRS_corTitulo']     = $this->input->post('corTituloFormRS');
        $dados['body_servico_formRS_corDescricao']  = $this->input->post('corDescricaoFormRS');
        
        if(empty($dados['id_modelo']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'Não foi possível encontrar o modelo, tente novamente em alguns minutos<br>';
            $sinal = true;
            
        }

        if(empty($this->input->post('textoBotaoForm1')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }
        
        if(empty($this->input->post('textoBotaoForm2')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }

        if(empty($this->input->post('textoBotaoForm2')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }

        if(empty($this->input->post('textoBotaoForm3')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }

        if(empty($this->input->post('textoBotaoForm4')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }

        if(empty($this->input->post('textoBotaoForm5')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }

        if(empty($this->input->post('textoBotaoForm6')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }

        if(empty($this->input->post('textoBotaoForm7')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }

        if(empty($this->input->post('textoBotaoForm8')))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> para o botão<br>';
            $sinal = true;
            
        }

        if($textoBotaoForm1 = strlen($this->input->post('textoBotaoForm1')) > 15)
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> menor, máximo 15 caracteres<br>';
            $sinal = true;
            
        }

        if($textoBotaoForm2 = strlen($this->input->post('textoBotaoForm2')) > 15)
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> menor, máximo 15 caracteres<br>';
            $sinal = true;
            
        }

        if($textoBotaoForm3 = strlen($this->input->post('textoBotaoForm3')) > 15)
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> menor, máximo 15 caracteres<br>';
            $sinal = true;
            
        }

        if($textoBotaoForm4 = strlen($this->input->post('textoBotaoForm4')) > 15)
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> menor, máximo 15 caracteres<br>';
            $sinal = true;
            
        }

        if($textoBotaoForm5 = strlen($this->input->post('textoBotaoForm5')) > 15)
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> menor, máximo 15 caracteres<br>';
            $sinal = true;
            
        }

        if($textoBotaoForm6 = strlen($this->input->post('textoBotaoForm6')) > 15)
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> menor, máximo 15 caracteres<br>';
            $sinal = true;
            
        }

        if($textoBotaoForm8 = strlen($this->input->post('textoBotaoForm8')) > 15)
		{
			$retorno['ret'] = false;
			$retorno['msg'].= ' Escreva um <strong>NOME</strong> menor, máximo 15 caracteres<br>';
            $sinal = true;
            
        }

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }

        $resultado = $this->modelos->m_atualizarFormularios($dados);

        if ($resultado)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados dos <strong>Formulários</strong> atualizado com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados dos <strong>Formulários</strong>!';
            echo json_encode($retorno);
        }
    }


    public function c_listarRedesSociais()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
        
        $dados_redesSociais = $this->modelos->buscarDados_redesSociais($id);

        echo json_encode($dados_redesSociais);
    }

    public function c_atualizarRedesSociais()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dados['id_modelo'] = $this->input->post('idConfigsRedesSociais');
        $dados['id_landingPage'] = $this->input->post('idLandingPageRedesSociais');

        $verificaDadosExiste = $this->modelos->buscarDados_redesSociais($dados['id_modelo']);
        
        if($verificaDadosExiste['redesSociais'][0] === $dados['id_modelo'])
		{
            $retorno['ret'] = false;
			$retorno['msg'].= ' Modelo não encontrado, tente novamente em alguns minutos<br>';
            $sinal = true;
            
        }
        
        if($verificaDadosExiste['configs_personalizadas'][0] === $dados['id_modelo'])
		{
            $retorno['ret'] = false;
			$retorno['msg'].= ' Modelo não encontrado, tente novamente em alguns minutos<br>';
            $sinal = true;
            
        }

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }
        
        if(empty($this->input->post('servicoRedesSociais'))){$dados['body_redesSociais']="0";}else{$dados['body_redesSociais']="1";}
        if(empty($this->input->post('servicoRedesSociaisConteudoGenerico'))){$dados['body_conteudoGenerico_redesSociais']="0";}else{$dados['body_conteudoGenerico_redesSociais']="1";}
        if(empty($this->input->post('opcao1RedeSocial'))){$dados['body_redesSociais_opcao1']="0";}else{$dados['body_redesSociais_opcao1']="1";}
        if(empty($this->input->post('opcao2RedeSocial'))){$dados['body_redesSociais_opcao2']="0";}else{$dados['body_redesSociais_opcao2']="1";}
        if(empty($this->input->post('opcao3RedeSocial'))){$dados['body_redesSociais_opcao3']="0";}else{$dados['body_redesSociais_opcao3']="1";}
        if(empty($this->input->post('botaoRedeSocial1'))){$dados['body_servico_redesSociais_botao1']="0";}else{$dados['body_servico_redesSociais_botao1']="1";}
        if(empty($this->input->post('botaoRedeSocial2'))){$dados['body_servico_redesSociais_botao2']="0";}else{$dados['body_servico_redesSociais_botao2']="1";}
        if(empty($this->input->post('botaoRedeSocial3'))){$dados['body_servico_redesSociais_botao3']="0";}else{$dados['body_servico_redesSociais_botao3']="1";}
        
        $dados['servico_redesSociais_corBg']                    = $this->input->post('corFundoRedesSociais');
        $dados['body_textoGenericoTitulo_redesSociais']         = $this->input->post('tituloRedesSociais');
        $dados['body_textoGenericoDescricao_redesSociais']      = $this->input->post('descricaoRedesSociais');
        $dados['servico_redesSociais_corTituloGenerico1']       = $this->input->post('corTituloRedesSociais');
        $dados['servico_redesSociais_corDescricaoGenerico1']    = $this->input->post('corTextoDescricaoRedesSociais');
        $dados['servico_redesSociais_corTexto']                 = $this->input->post('corTituloeDescricaoRedeSocial');
        $dados['servico_redesSociais_corBoxImage']              = $this->input->post('corFundoRedeSocial');
        $dados['body_servico_redesSociais_Titulo1']             = $this->input->post('tituloRedeSocial1');
        $dados['body_servico_redesSociais_Titulo2']             = $this->input->post('tituloRedeSocial2');
        $dados['body_servico_redesSociais_Titulo3']             = $this->input->post('tituloRedeSocial3');
        $dados['body_servico_redesSociais_Descricao1']          = $this->input->post('descricaoRedeSocial1');
        $dados['body_servico_redesSociais_Descricao2']          = $this->input->post('descricaoRedeSocial2');
        $dados['body_servico_redesSociais_Descricao3']          = $this->input->post('descricaoRedeSocial3');
        $dados['body_servico_redesSociais_botaoTexto1']         = $this->input->post('textoBotao_RedeSocial_Opcao1');
        $dados['body_servico_redesSociais_botaoTexto2']         = $this->input->post('textoBotao_RedeSocial_Opcao2');
        $dados['body_servico_redesSociais_botaoTexto3']         = $this->input->post('textoBotao_RedeSocial_Opcao3');
        $dados['body_servico_redesSociais_botaoLink1']          = $this->input->post('linkBotao_RedeSocial_Opcao1');
        $dados['body_servico_redesSociais_botaoLink2']          = $this->input->post('linkBotao_RedeSocial_Opcao2');
        $dados['body_servico_redesSociais_botaoLink3']          = $this->input->post('linkBotao_RedeSocial_Opcao3');
        $dados['body_servico_redesSociais_ImagemSEO1']          = $this->input->post('imagemSEO_RedeSocial_Opcao1');
        $dados['body_servico_redesSociais_ImagemSEO2']          = $this->input->post('imagemSEO_RedeSocial_Opcao2');
        $dados['body_servico_redesSociais_ImagemSEO3']          = $this->input->post('imagemSEO_RedeSocial_Opcao3');
        $dados['servico_redesSociais_corTextoBotao']            = $this->input->post('corTextoRedeSocial');
        $dados['servico_redesSociais_corTextoHoverBotao']       = $this->input->post('corTextoHoverBotaoRedeSocial');
        $dados['servico_redesSociais_corFundoBotao']            = $this->input->post('corfundoBotaoRedeSocial');
        $dados['servico_redesSociais_corFundoHoverBotao']       = $this->input->post('corFundoHoverBotaoRedeSocial');
        
        if($_FILES['imagemBotao_RedeSocial_Opcao1']['size'] === 0 )
        {
            $consultaRedesSociais1 = $this->modelos->buscarDados_redesSociais($dados['id_landingPage']);
            
            $dados['body_servico_redesSociais_Imagem1'] = $consultaRedesSociais1['redesSociais'][0]->body_servico_redesSociais_Imagem1;
        }

        if($_FILES['imagemBotao_RedeSocial_Opcao2']['size'] === 0 )
        {
            $consultaRedesSociais2 = $this->modelos->buscarDados_redesSociais($dados['id_landingPage']);
            
            $dados['body_servico_redesSociais_Imagem2'] = $consultaRedesSociais2['redesSociais'][0]->body_servico_redesSociais_Imagem2;
        }

        if($_FILES['imagemBotao_RedeSocial_Opcao3']['size'] === 0 )
        {
            $consultaRedesSociais3 = $this->modelos->buscarDados_redesSociais($dados['id_landingPage']);
            
            $dados['body_servico_redesSociais_Imagem3'] = $consultaRedesSociais3['redesSociais'][0]->body_servico_redesSociais_Imagem3;
        }       
        
        if(!empty($_FILES['imagemBotao_RedeSocial_Opcao1']['size']))
        {
            $result = $this->modelos->buscarDados_redesSociais($dados['id_landingPage']);
            
            $imagemRedeSocial1 = 'assets/images/social/'.$result['redesSociais'][0]->body_servico_redesSociais_Imagem1;

            if (file_exists($imagemRedeSocial1)) {

                unlink('assets/images/social/'.$result['redesSociais'][0]->body_servico_redesSociais_Imagem1);

                $config['upload_path'] = './assets/images/social';
                $config['file_name'] = "img_redeSocial_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico_redesSociais_Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemBotao_RedeSocial_Opcao1');

            } else {

                $config['upload_path'] = './assets/images/social';
                $config['file_name'] = "img_redeSocial_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico_redesSociais_Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemBotao_RedeSocial_Opcao1');

            }
            
        }

        if(!empty($_FILES['imagemBotao_RedeSocial_Opcao2']['size']))
        {
            $result = $this->modelos->buscarDados_redesSociais($dados['id_landingPage']);
            
            $imagemRedeSocial2 = 'assets/images/social/'.$result['redesSociais'][0]->body_servico_redesSociais_Imagem2;

            if (file_exists($imagemRedeSocial2)) {

                unlink('assets/images/social/'.$result['redesSociais'][0]->body_servico_redesSociais_Imagem2);

                $config['upload_path'] = './assets/images/social';
                $config['file_name'] = "img_redeSocial_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico_redesSociais_Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemBotao_RedeSocial_Opcao2');

            } else {

                $config['upload_path'] = './assets/images/social';
                $config['file_name'] = "img_redeSocial_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico_redesSociais_Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemBotao_RedeSocial_Opcao2');

            }
            
        }

        if(!empty($_FILES['imagemBotao_RedeSocial_Opcao3']['size']))
        {
            $result = $this->modelos->buscarDados_redesSociais($dados['id_landingPage']);
            
            $imagemRedeSocial3 = 'assets/images/social/'.$result['redesSociais'][0]->body_servico_redesSociais_Imagem3;

            if (file_exists($imagemRedeSocial3)) {

                unlink('assets/images/social/'.$result['redesSociais'][0]->body_servico_redesSociais_Imagem3);

                $config['upload_path'] = './assets/images/social';
                $config['file_name'] = "img_redeSocial_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico_redesSociais_Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemBotao_RedeSocial_Opcao3');

            } else {

                $config['upload_path'] = './assets/images/social';
                $config['file_name'] = "img_redeSocial_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico_redesSociais_Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('imagemBotao_RedeSocial_Opcao3');

            }
            
        }
        
        $resultado = $this->modelos->m_atualizarRedesSociais($dados);
        
        if ($resultado)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados das <strong>REDES SOCIAIS</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados dos <strong>REDES SOCIAIS</strong>!';
            echo json_encode($retorno);
        }
        
    }

    public function c_listarServicos()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
        
        $dados_servicos = $this->modelos->buscarServicos($id);

        echo json_encode($dados_servicos);
    }

    public function c_atualizarServico1()
    {
        $dados['id_modelo'] = $this->input->post('idModelo_servico1');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_servico1');
        if(empty($this->input->post('servico1_ativaDesativa'))){$dados['body_servico1']="0";}else{$dados['body_servico1']="1";}
        if(empty($this->input->post('servico1_ConteudoGenerico'))){$dados['body_conteudoGenerico1']="0";}else{$dados['body_conteudoGenerico1']="1";}
        $dados['body_textoGenericoTitulo1'] = $this->input->post('servico1_titulo');
        $dados['body_textoGenericoDescricao1'] = $this->input->post('servico1_descricao');
        $dados['servico1_corTituloGenerico1'] = $this->input->post('servico1_corTitulo');
        $dados['servico1_corDescricaoGenerico1'] = $this->input->post('servico1_corDescricao');
        $dados['servico1_corFundoBG'] = $this->input->post('servico1_corFundoBG');
        $dados['servico1_corTexto'] = $this->input->post('servico1_corTextosProdsServs');
        $dados['servico1_corTexto'] = $this->input->post('servico1_corTextosProdsServs');
        $dados['servico1_corBoxImage'] = $this->input->post('servico1_corFundoProdutosServicos');
        
        if(empty($this->input->post('servico1_opcao1'))){$dados['body_servico1_opcao1']="0";}else{$dados['body_servico1_opcao1']="1";}
        $dados['body_servico1Titulo1'] = $this->input->post('servico1_titulo_opcao1');
        $dados['body_servico1Descricao1'] = $this->input->post('servico1_descricao_opcao1');
        $dados['body_servico1ImagemSEO1'] = $this->input->post('servico1_SEO_opcao1');
        
        if(empty($this->input->post('servico1_opcao2'))){$dados['body_servico1_opcao2']="0";}else{$dados['body_servico1_opcao2']="1";}
        $dados['body_servico1Titulo2'] = $this->input->post('servico1_titulo_opcao2');
        $dados['body_servico1Descricao2'] = $this->input->post('servico1_descricao_opcao2');
        $dados['body_servico1ImagemSEO2'] = $this->input->post('servico1_SEO_opcao2');

        if(empty($this->input->post('servico1_opcao3'))){$dados['body_servico1_opcao3']="0";}else{$dados['body_servico1_opcao3']="1";}
        $dados['body_servico1Titulo3'] = $this->input->post('servico1_titulo_opcao3');
        $dados['body_servico1Descricao3'] = $this->input->post('servico1_descricao_opcao3');
        $dados['body_servico1ImagemSEO3'] = $this->input->post('servico1_SEO_opcao3');

        if(empty($this->input->post('servico1_opcao4'))){$dados['body_servico1_opcao4']="0";}else{$dados['body_servico1_opcao4']="1";}
        $dados['body_servico1Titulo4'] = $this->input->post('servico1_titulo_opcao4');
        $dados['body_servico1Descricao4'] = $this->input->post('servico1_descricao_opcao4');
        $dados['body_servico1ImagemSEO4'] = $this->input->post('servico1_SEO_opcao4');

        if(empty($this->input->post('servico1_opcao5'))){$dados['body_servico1_opcao5']="0";}else{$dados['body_servico1_opcao5']="1";}
        $dados['body_servico1Titulo5'] = $this->input->post('servico1_titulo_opcao5');
        $dados['body_servico1Descricao5'] = $this->input->post('servico1_descricao_opcao5');
        $dados['body_servico1ImagemSEO5'] = $this->input->post('servico1_SEO_opcao5');

        if(empty($this->input->post('servico1_opcao6'))){$dados['body_servico1_opcao6']="0";}else{$dados['body_servico1_opcao6']="1";}
        $dados['body_servico1Titulo6'] = $this->input->post('servico1_titulo_opcao6');
        $dados['body_servico1Descricao6'] = $this->input->post('servico1_descricao_opcao6');
        $dados['body_servico1ImagemSEO6'] = $this->input->post('servico1_SEO_opcao6');
        
        if(empty($this->input->post('servico1_opcao7'))){$dados['body_servico1_opcao7']="0";}else{$dados['body_servico1_opcao7']="1";}
        $dados['body_servico1Titulo7'] = $this->input->post('servico1_titulo_opcao7');
        $dados['body_servico1Descricao7'] = $this->input->post('servico1_descricao_opcao7');
        $dados['body_servico1ImagemSEO7'] = $this->input->post('servico1_SEO_opcao7');
        
        if(empty($this->input->post('servico1_opcao8'))){$dados['body_servico1_opcao8']="0";}else{$dados['body_servico1_opcao8']="1";}
        $dados['body_servico1Titulo8'] = $this->input->post('servico1_titulo_opcao8');
        $dados['body_servico1Descricao8'] = $this->input->post('servico1_descricao_opcao8');
        $dados['body_servico1ImagemSEO8'] = $this->input->post('servico1_SEO_opcao8');
        
        $imagem = $this->modelos->buscarServicos($dados['id_landingPage']);

        if($_FILES['servico1_imagem_opcao1']['size'] === 0 )
        {
            $dados['body_servico1Imagem1'] = $imagem['servico1'][0]->body_servico1Imagem1;
        }

        if($_FILES['servico1_imagem_opcao2']['size'] === 0 )
        {
            $dados['body_servico1Imagem2'] = $imagem['servico1'][0]->body_servico1Imagem2;
        }

        if($_FILES['servico1_imagem_opcao3']['size'] === 0 )
        {
            $dados['body_servico1Imagem3'] = $imagem['servico1'][0]->body_servico1Imagem3;
        }

        if($_FILES['servico1_imagem_opcao4']['size'] === 0 )
        {
            $dados['body_servico1Imagem4'] = $imagem['servico1'][0]->body_servico1Imagem4;
        }

        if($_FILES['servico1_imagem_opcao5']['size'] === 0 )
        {
            $dados['body_servico1Imagem5'] = $imagem['servico1'][0]->body_servico1Imagem5;
        }

        if($_FILES['servico1_imagem_opcao6']['size'] === 0 )
        {
            $dados['body_servico1Imagem6'] = $imagem['servico1'][0]->body_servico1Imagem6;
        }

        if($_FILES['servico1_imagem_opcao7']['size'] === 0 )
        {
            $dados['body_servico1Imagem7'] = $imagem['servico1'][0]->body_servico1Imagem7;
        }

        if($_FILES['servico1_imagem_opcao8']['size'] === 0 )
        {
            $dados['body_servico1Imagem8'] = $imagem['servico1'][0]->body_servico1Imagem8;
        }

        if(!empty($_FILES['servico1_imagem_opcao1']['size']))
        {
            
            $imagem1 = 'assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem1;

            if (file_exists($imagem1)) {

                unlink('assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem1);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao1');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao1');

            }
            
        }

        if(!empty($_FILES['servico1_imagem_opcao2']['size']))
        {
            
            $imagem2 = 'assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem2;

            if (file_exists($imagem2)) {

                unlink('assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem2);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao2');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao2');

            }
            
        }

        if(!empty($_FILES['servico1_imagem_opcao3']['size']))
        {
            
            $imagem3 = 'assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem3;

            if (file_exists($imagem3)) {

                unlink('assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem3);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao3');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao3');

            }
            
        }

        if(!empty($_FILES['servico1_imagem_opcao4']['size']))
        {
            
            $imagem4 = 'assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem4;

            if (file_exists($imagem4)) {

                unlink('assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem4);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao4');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao4');

            }
            
        }

        if(!empty($_FILES['servico1_imagem_opcao5']['size']))
        {
            
            $imagem5 = 'assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem5;

            if (file_exists($imagem5)) {

                unlink('assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem5);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao5');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao5');

            }
            
        }

        if(!empty($_FILES['servico1_imagem_opcao6']['size']))
        {
            
            $imagem6 = 'assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem6;

            if (file_exists($imagem6)) {

                unlink('assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem6);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem6'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao6');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem6'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao6');

            }
            
        }

        if(!empty($_FILES['servico1_imagem_opcao7']['size']))
        {
            
            $imagem7 = 'assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem7;

            if (file_exists($imagem7)) {

                unlink('assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem7);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem7'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao7');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem7'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao7');

            }
            
        }

        if(!empty($_FILES['servico1_imagem_opcao8']['size']))
        {
            
            $imagem8 = 'assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem8;

            if (file_exists($imagem8)) {

                unlink('assets/images/servicos/'.$imagem['servico1'][0]->body_servico1Imagem8);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem8'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao8');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico1Imagem8'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico1_imagem_opcao8');

            }
            
        }
        
        $updateServico1 = $this->modelos->m_atualizarServico1($dados);
        
        if ($updateServico1)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>SERVIÇO 1</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados do <strong>SERVIÇO 1</strong>!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarServico2()
    {

        $dados['id_modelo'] = $this->input->post('idModelo_servico2');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_servico2');
        if(empty($this->input->post('servico2_ativaDesativa'))){$dados['body_servico2']="0";}else{$dados['body_servico2']="1";}
        if(empty($this->input->post('servico2_ConteudoGenerico'))){$dados['body_conteudoGenerico2']="0";}else{$dados['body_conteudoGenerico2']="1";}
        if(empty($this->input->post('servico2_botao'))){$dados['body_servico2_botao1']="0";}else{$dados['body_servico2_botao1']="1";}
        $dados['body_textoGenericoTitulo2'] = $this->input->post('servico2_titulo');
        $dados['body_textoGenericoDescricao2'] = $this->input->post('servico2_descricao');
        $dados['body_servico2ImagemSEO1'] = $this->input->post('servico2_imagemSEO');
        $dados['body_servico2_botaoTexto1'] = $this->input->post('servico2_textoBotao');
        $dados['body_servico2_botaoLink1'] = $this->input->post('servico2_linkBotao');
        $dados['servico2_corTitulo'] = $this->input->post('servico2_corTitulo');
        $dados['servico2_corDescricao'] = $this->input->post('servico2_corDescricao');
        $dados['servico2_corBg'] = $this->input->post('servico2_corFundoBG');
        $dados['servico2_corTextoBotao'] = $this->input->post('servico2_corTextoBotao');
        $dados['servico2_corTextoHoverBotao'] = $this->input->post('servico2_corTextoHoverBotao');
        $dados['servico2_corFundoBotao'] = $this->input->post('servico2_corFundoBotao');
        $dados['servico2_corFundoHoverBotao'] = $this->input->post('servico2_corFundoHoverBotao');

        $imagem = $this->modelos->buscarServicos($dados['id_landingPage']);
        
        if($_FILES['servico2_imagemFundo']['size'] === 0 )
        {
            $dados['body_servico2Imagem1'] = $imagem['servico2'][0]->body_servico2Imagem1;
        }

        if(!empty($_FILES['servico2_imagemFundo']['size']))
        {
            
            $imagem1 = 'assets/images/servicos/'.$imagem['servico2'][0]->body_servico2Imagem1;

            if (file_exists($imagem1)) {

                unlink('assets/images/servicos/'.$imagem['servico2'][0]->body_servico2Imagem1);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico2Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico2_imagemFundo');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico2Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico2_imagemFundo');

            }
            
        }

        $updateServico2 = $this->modelos->m_atualizarServico2($dados);
        
        if ($updateServico2)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>SERVIÇO 2</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados do <strong>SERVIÇO 2</strong>!';
            echo json_encode($retorno);
        }
        
    }
    

    public function c_atualizarServico3()
    {
        $dados_servico['id_modelo'] = $this->input->post('idModelo_servico3');
        $dados_servico['id_landingPage'] = $this->input->post('idLandingPage_servico3');
        if(empty($this->input->post('servico3_ativaDesativa'))){$dados_servico['body_servico3']="0";}else{$dados_servico['body_servico3']="1";}
        if(empty($this->input->post('servico3_ConteudoGenerico'))){$dados_servico['body_conteudoGenerico3']="0";}else{$dados_servico['body_conteudoGenerico3']="1";}
        if(empty($this->input->post('servico3_opcao1'))){$dados_servico['body_servico3_opcao1']="0";}else{$dados_servico['body_servico3_opcao1']="1";}
        if(empty($this->input->post('servico3_opcao2'))){$dados_servico['body_servico3_opcao2']="0";}else{$dados_servico['body_servico3_opcao2']="1";}
        if(empty($this->input->post('servico3_opcao3'))){$dados_servico['body_servico3_opcao3']="0";}else{$dados_servico['body_servico3_opcao3']="1";}
        if(empty($this->input->post('servico3_opcao4'))){$dados_servico['body_servico3_opcao4']="0";}else{$dados_servico['body_servico3_opcao4']="1";}
        if(empty($this->input->post('servico3_todasOpcoes'))){$dados_servico['body_servico3_TodasOpcoes']="0";}else{$dados_servico['body_servico3_TodasOpcoes']="1";}
        
        $dados_servico['body_textoGenericoTitulo3'] = $this->input->post('servico3_titulo');
        $dados_servico['body_textoGenericoDescricao3'] = $this->input->post('servico3_descricao');
        $dados_personalizacao['servico3_corTituloGenerico1'] = $this->input->post('servico3_corTitulo');
        $dados_personalizacao['servico3_corDescricaoGenerico1'] = $this->input->post('servico3_corDescricao');
        $dados_personalizacao['servico3_corTexto'] = $this->input->post('servico3_corTextos');
        $dados_personalizacao['servico3_corBoxImage'] = $this->input->post('servico3_corFundoImagens');
        $dados_personalizacao['servico3_corBg'] = $this->input->post('servico3_corFundoBG'); 
        $dados_servico['body_servico3ImagemSEO5'] = $this->input->post('servico3_imagemSEO5');
        $dados_servico['body_servico3ImagemSEO1'] = $this->input->post('servico3_SEO_opcao1');
        $dados_servico['body_servico3Titulo1'] = $this->input->post('servico3_titulo_opcao1');
        $dados_servico['body_servico3Descricao1'] = $this->input->post('servico3_descricao_opcao1');
        $dados_servico['body_servico3ImagemSEO2'] = $this->input->post('servico3_SEO_opcao2');
        $dados_servico['body_servico3Titulo2'] = $this->input->post('servico3_titulo_opcao2');
        $dados_servico['body_servico3Descricao2'] = $this->input->post('servico3_descricao_opcao2');
        $dados_servico['body_servico3ImagemSEO3'] = $this->input->post('servico3_SEO_opcao3');
        $dados_servico['body_servico3Titulo3'] = $this->input->post('servico3_titulo_opcao3');
        $dados_servico['body_servico3Descricao3'] = $this->input->post('servico3_descricao_opcao3');
        $dados_servico['body_servico3Titulo4'] = $this->input->post('servico3_titulo_opcao4');
        $dados_servico['body_servico3ImagemSEO4'] = $this->input->post('servico3_SEO_opcao4');
        $dados_servico['body_servico3Descricao4'] = $this->input->post('servico3_descricao_opcao4');
        
        $imagem = $this->modelos->buscarServicos($dados_servico['id_landingPage']);
        
        if($_FILES['servico3_imagem_opcao1']['size'] === 0 )
        {
            $dados_servico['body_servico3Imagem1'] = $imagem['servico3'][0]->body_servico3Imagem1;
        }

        if($_FILES['servico3_imagem_opcao2']['size'] === 0 )
        {
            $dados_servico['body_servico3Imagem2'] = $imagem['servico3'][0]->body_servico3Imagem2;
        }

        if($_FILES['servico3_imagem_opcao3']['size'] === 0 )
        {
            $dados_servico['body_servico3Imagem3'] = $imagem['servico3'][0]->body_servico3Imagem3;
        }

        if($_FILES['servico3_imagem_opcao4']['size'] === 0 )
        {
            $dados_servico['body_servico3Imagem4'] = $imagem['servico3'][0]->body_servico3Imagem4;
        }

        if($_FILES['servico3_imagemFundo']['size'] === 0 )
        {
            $dados_servico['body_servico3Imagem5'] = $imagem['servico3'][0]->body_servico3Imagem5;
        }

        if(!empty($_FILES['servico3_imagem_opcao1']['size']))
        {
            
            $imagem1 = 'assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem1;

            if (file_exists($imagem1)) {

                unlink('assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem1);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagem_opcao1');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagem_opcao1');

            }
        }
        
        if(!empty($_FILES['servico3_imagem_opcao2']['size']))
        {
            
            $imagem2 = 'assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem2;

            if (file_exists($imagem2)) {

                unlink('assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem2);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagem_opcao2');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagem_opcao2');

            } 
        }
        
        if(!empty($_FILES['servico3_imagem_opcao3']['size']))
        {
            
            $imagem3 = 'assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem3;

            if (file_exists($imagem3)) {

                unlink('assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem3);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagem_opcao3');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagem_opcao3');

            } 
        }
        
        if(!empty($_FILES['servico3_imagem_opcao4']['size']))
        {
            
            $imagem4 = 'assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem4;

            if (file_exists($imagem4)) {

                unlink('assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem4);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagem_opcao4');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagem_opcao4');

            } 
        }
        
        if(!empty($_FILES['servico3_imagemFundo']['size']))
        {
            
            $imagem5 = 'assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem5;

            if (file_exists($imagem5)) {

                unlink('assets/images/servicos/'.$imagem['servico3'][0]->body_servico3Imagem5);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagemFundo');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados_servico['body_servico3Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico3_imagemFundo');

            } 
        }

        $updateServico2 = $this->modelos->m_atualizarServico3($dados_servico, $dados_personalizacao);

        if ($updateServico2)
        {

            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>SERVIÇO 3</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados do <strong>SERVIÇO 3</strong>!';
            echo json_encode($retorno);
        }
    }

    // c_atualizarServico4

    public function c_atualizarServico4()
    {
        $dados['id_modelo'] = $this->input->post('idModelo_servico4');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_servico4');
        if(empty($this->input->post('servico4_ativaDesativa'))){$dados['body_servico4']="0";}else{$dados['body_servico4']="1";}
        if(empty($this->input->post('servico4_ConteudoGenerico'))){$dados['body_conteudoGenerico4']="0";}else{$dados['body_conteudoGenerico4']="1";}
        if(empty($this->input->post('servico4_botao'))){$dados['body_servico4_botao1']="0";}else{$dados['body_servico4_botao1']="1";}
        $dados['body_textoGenericoTitulo4'] = $this->input->post('servico4_titulo');
        $dados['body_textoGenericoDescricao4'] = $this->input->post('servico4_descricao');
        $dados['servico4_corTituloGenerico1'] = $this->input->post('servico4_corTitulo');
        $dados['servico4_corDescricaoGenerico1'] = $this->input->post('servico4_corDescricao');
        $dados['servico4_corBg'] = $this->input->post('servico4_corFundoBG');
        $dados['body_servico4ImagemSEO1'] = $this->input->post('servico4_imagemSEO');
        $dados['body_servico4_botaoTexto1'] = $this->input->post('servico4_textoBotao');
        $dados['body_servico4_botaoLink1'] = $this->input->post('servico4_linkBotao');
        $dados['servico4_corTextoBotao'] = $this->input->post('servico4_corTextoBotao');
        $dados['servico4_corTextoHoverBotao'] = $this->input->post('servico4_corTextoHoverBotao');
        $dados['servico4_corFundoBotao'] = $this->input->post('servico4_corFundoBotao');
        $dados['servico4_corFundoHoverBotao'] = $this->input->post('servico4_corFundoHoverBotao');
        
        $imagem = $this->modelos->buscarServicos($dados['id_landingPage']);

        if($_FILES['servico4_imagemFundo']['size'] === 0 )
        {
            $dados['body_servico4Imagem1'] = $imagem['servico4'][0]->body_servico4Imagem1;
        }

        if(!empty($_FILES['servico4_imagemFundo']['size']))
        {
            
            $imagem1 = 'assets/images/servicos/'.$imagem['servico4'][0]->body_servico4Imagem1;

            if (file_exists($imagem1)) {

                unlink('assets/images/servicos/'.$imagem['servico4'][0]->body_servico4Imagem1);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico4Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico4_imagemFundo');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico4Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico4_imagemFundo');

            }
        }
        
        $updateServico4 = $this->modelos->m_atualizarServico4($dados);

        if ($updateServico4)
        {

            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>SERVIÇO 4</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados do <strong>SERVIÇO 4</strong>!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarServico5()
    {
        
        $dados['id_modelo'] = $this->input->post('idModelo_servico5');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_servico5');
        $imagem = $this->modelos->buscarServicos($dados['id_landingPage']);

        if(empty($this->input->post('servico5_ativaDesativa'))){$dados['body_servico5']="0";}else{$dados['body_servico5']="1";}
        if(empty($this->input->post('servico5_ConteudoGenerico'))){$dados['body_conteudoGenerico5']="0";}else{$dados['body_conteudoGenerico5']="1";}
        
        $dados['body_textoGenericoTitulo5'] = $this->input->post('servico5_titulo');
        $dados['body_textoGenericoDescricao5'] = $this->input->post('servico5_descricao');
        $dados['servico5_corTituloGenerico1'] = $this->input->post('servico5_corTitulo');
        $dados['servico5_corDescricaoGenerico1'] = $this->input->post('servico5_corDescricao');
        $dados['servico5_corBg'] = $this->input->post('servico5_corFundoBG');
        $dados['servico5_corTexto'] = $this->input->post('servico5_corTextos');
        $dados['servico5_corBoxImage'] = $this->input->post('servico5_corFundoBOXImagem');
        $dados['servico5_corTextoBotao'] = $this->input->post('corTextoBotao_servico5');
        $dados['servico5_corTextoHoverBotao'] = $this->input->post('corTextoHoverBotao_servico5');
        $dados['servico5_corFundoBotao'] = $this->input->post('corfundoBotao_servico5');
        $dados['servico5_corFundoHoverBotao'] = $this->input->post('corFundoHoverBotao_servico5');

        if(empty($this->input->post('servico5_preco1_botao1'))){$dados['body_servico5_preco1_opcao1']="0";}else{$dados['body_servico5_preco1_opcao1']="1";}
        if(empty($this->input->post('servico5_preco2_botao2'))){$dados['body_servico5_preco2_opcao2']="0";}else{$dados['body_servico5_preco2_opcao2']="1";}
        if(empty($this->input->post('servico5_preco3_botao3'))){$dados['body_servico5_preco3_opcao3']="0";}else{$dados['body_servico5_preco3_opcao3']="1";}
        if(empty($this->input->post('servico5_preco4_botao4'))){$dados['body_servico5_preco4_opcao4']="0";}else{$dados['body_servico5_preco4_opcao4']="1";}
        if(empty($this->input->post('servico5_preco5_botao5'))){$dados['body_servico5_preco5_opcao5']="0";}else{$dados['body_servico5_preco5_opcao5']="1";}
        if(empty($this->input->post('servico5_preco6_botao6'))){$dados['body_servico5_preco6_opcao6']="0";}else{$dados['body_servico5_preco6_opcao6']="1";}
        if(empty($this->input->post('servico5_preco7_botao7'))){$dados['body_servico5_preco7_opcao7']="0";}else{$dados['body_servico5_preco7_opcao7']="1";}
        if(empty($this->input->post('servico5_preco8_botao8'))){$dados['body_servico5_preco8_opcao8']="0";}else{$dados['body_servico5_preco8_opcao8']="1";}

        if(empty($this->input->post('servico5_opcao1'))){$dados['body_servico5_opcao1']="0";}else{$dados['body_servico5_opcao1']="1";}
        $dados['body_servico5_botaoTexto1'] = $this->input->post('servico5_texoBotao_opcao1');
        $dados['body_servico5_botaoLink1'] = $this->input->post('servico5_linkBotao_opcao1');
        if(empty($this->input->post('servico5_botao1'))){$dados['body_servico5_botao1']="0";}else{$dados['body_servico5_botao1']="1";}
        $dados['body_servico5Titulo1'] = $this->input->post('servico5_titulo_opcao1');
        $dados['body_servico5Descricao1'] = $this->input->post('servico5_descricao_opcao1');
        $dados['body_servico5ImagemSEO1'] = $this->input->post('servico5_SEO_opcao1');
        $dados['body_servico5_preco1'] = $this->input->post('servico5_preco_opcao1');
        $dados['body_servico5_opcao1Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao1');
        $dados['body_servico5_opcao1Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao2');
        $dados['body_servico5_opcao1Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao3');
        $dados['body_servico5_opcao1Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao4');
        $dados['body_servico5_opcao1Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao5');
        $dados['body_servico5_opcao1Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao6');
        $dados['body_servico5_opcao1Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao7');
        $dados['body_servico5_opcao1Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao8');
        $dados['body_servico5_opcao1Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao9');
        $dados['body_servico5_opcao1Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao1_opcao10');

        if(empty($this->input->post('servico5_opcao2'))){$dados['body_servico5_opcao2']="0";}else{$dados['body_servico5_opcao2']="1";}
        $dados['body_servico5_botaoTexto2'] = $this->input->post('servico5_texoBotao_opcao2');
        $dados['body_servico5_botaoLink2'] = $this->input->post('servico5_linkBotao_opcao2');
        if(empty($this->input->post('servico5_botao2'))){$dados['body_servico5_botao2']="0";}else{$dados['body_servico5_botao2']="1";}
        $dados['body_servico5Titulo2'] = $this->input->post('servico5_titulo_opcao2');
        $dados['body_servico5Descricao2'] = $this->input->post('servico5_descricao_opcao2');
        $dados['body_servico5ImagemSEO2'] = $this->input->post('servico5_SEO_opcao2');
        $dados['body_servico5_preco2'] = $this->input->post('servico5_preco_opcao2');
        $dados['body_servico5_opcao2Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao1');
        $dados['body_servico5_opcao2Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao2');
        $dados['body_servico5_opcao2Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao3');
        $dados['body_servico5_opcao2Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao4');
        $dados['body_servico5_opcao2Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao5');
        $dados['body_servico5_opcao2Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao6');
        $dados['body_servico5_opcao2Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao7');
        $dados['body_servico5_opcao2Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao8');
        $dados['body_servico5_opcao2Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao9');
        $dados['body_servico5_opcao2Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao2_opcao10');
        
        if(empty($this->input->post('servico5_opcao3'))){$dados['body_servico5_opcao3']="0";}else{$dados['body_servico5_opcao3']="1";}
        $dados['body_servico5_botaoTexto3'] = $this->input->post('servico5_texoBotao_opcao3');
        $dados['body_servico5_botaoLink3'] = $this->input->post('servico5_linkBotao_opcao3');
        if(empty($this->input->post('servico5_botao3'))){$dados['body_servico5_botao3']="0";}else{$dados['body_servico5_botao3']="1";}
        $dados['body_servico5Titulo3'] = $this->input->post('servico5_titulo_opcao3');
        $dados['body_servico5Descricao3'] = $this->input->post('servico5_descricao_opcao3');
        $dados['body_servico5ImagemSEO3'] = $this->input->post('servico5_SEO_opcao3');
        $dados['body_servico5_preco3'] = $this->input->post('servico5_preco_opcao3');
        $dados['body_servico5_opcao3Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao1');
        $dados['body_servico5_opcao3Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao2');
        $dados['body_servico5_opcao3Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao3');
        $dados['body_servico5_opcao3Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao4');
        $dados['body_servico5_opcao3Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao5');
        $dados['body_servico5_opcao3Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao6');
        $dados['body_servico5_opcao3Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao7');
        $dados['body_servico5_opcao3Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao8');
        $dados['body_servico5_opcao3Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao9');
        $dados['body_servico5_opcao3Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao3_opcao10');

        if(empty($this->input->post('servico5_opcao4'))){$dados['body_servico5_opcao4']="0";}else{$dados['body_servico5_opcao4']="1";}
        $dados['body_servico5_botaoTexto4'] = $this->input->post('servico5_texoBotao_opcao4');
        $dados['body_servico5_botaoLink4'] = $this->input->post('servico5_linkBotao_opcao4');
        if(empty($this->input->post('servico5_botao4'))){$dados['body_servico5_botao4']="0";}else{$dados['body_servico5_botao4']="1";}
        $dados['body_servico5Titulo4'] = $this->input->post('servico5_titulo_opcao4');
        $dados['body_servico5Descricao4'] = $this->input->post('servico5_descricao_opcao4');
        $dados['body_servico5ImagemSEO4'] = $this->input->post('servico5_SEO_opcao4');
        $dados['body_servico5_preco4'] = $this->input->post('servico5_preco_opcao4');
        $dados['body_servico5_opcao4Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao1');
        $dados['body_servico5_opcao4Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao2');
        $dados['body_servico5_opcao4Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao3');
        $dados['body_servico5_opcao4Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao4');
        $dados['body_servico5_opcao4Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao5');
        $dados['body_servico5_opcao4Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao6');
        $dados['body_servico5_opcao4Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao7');
        $dados['body_servico5_opcao4Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao8');
        $dados['body_servico5_opcao4Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao9');
        $dados['body_servico5_opcao4Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao4_opcao10');

        if(empty($this->input->post('servico5_opcao5'))){$dados['body_servico5_opcao5']="0";}else{$dados['body_servico5_opcao5']="1";}
        $dados['body_servico5_botaoTexto5'] = $this->input->post('servico5_texoBotao_opcao5');
        $dados['body_servico5_botaoLink5'] = $this->input->post('servico5_linkBotao_opcao5');
        if(empty($this->input->post('servico5_botao5'))){$dados['body_servico5_botao5']="0";}else{$dados['body_servico5_botao5']="1";}
        $dados['body_servico5Titulo5'] = $this->input->post('servico5_titulo_opcao5');
        $dados['body_servico5Descricao5'] = $this->input->post('servico5_descricao_opcao5');
        $dados['body_servico5ImagemSEO5'] = $this->input->post('servico5_SEO_opcao5');
        $dados['body_servico5_preco5'] = $this->input->post('servico5_preco_opcao5');
        $dados['body_servico5_opcao5Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao1');
        $dados['body_servico5_opcao5Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao2');
        $dados['body_servico5_opcao5Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao3');
        $dados['body_servico5_opcao5Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao4');
        $dados['body_servico5_opcao5Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao5');
        $dados['body_servico5_opcao5Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao6');
        $dados['body_servico5_opcao5Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao7');
        $dados['body_servico5_opcao5Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao8');
        $dados['body_servico5_opcao5Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao9');
        $dados['body_servico5_opcao5Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao5_opcao10');

        if(empty($this->input->post('servico5_opcao6'))){$dados['body_servico5_opcao6']="0";}else{$dados['body_servico5_opcao6']="1";}
        $dados['body_servico5_botaoTexto6'] = $this->input->post('servico5_texoBotao_opcao6');
        $dados['body_servico5_botaoLink6'] = $this->input->post('servico5_linkBotao_opcao6');
        if(empty($this->input->post('servico5_botao6'))){$dados['body_servico5_botao6']="0";}else{$dados['body_servico5_botao6']="1";}
        $dados['body_servico5Titulo6'] = $this->input->post('servico5_titulo_opcao6');
        $dados['body_servico5Descricao6'] = $this->input->post('servico5_descricao_opcao6');
        $dados['body_servico5ImagemSEO6'] = $this->input->post('servico5_SEO_opcao6');
        $dados['body_servico5_preco6'] = $this->input->post('servico5_preco_opcao6');
        $dados['body_servico5_opcao6Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao1');
        $dados['body_servico5_opcao6Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao2');
        $dados['body_servico5_opcao6Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao3');
        $dados['body_servico5_opcao6Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao4');
        $dados['body_servico5_opcao6Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao5');
        $dados['body_servico5_opcao6Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao6');
        $dados['body_servico5_opcao6Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao7');
        $dados['body_servico5_opcao6Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao8');
        $dados['body_servico5_opcao6Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao9');
        $dados['body_servico5_opcao6Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao10');

        if(empty($this->input->post('servico5_opcao6'))){$dados['body_servico5_opcao6']="0";}else{$dados['body_servico5_opcao6']="1";}
        $dados['body_servico5_botaoTexto6'] = $this->input->post('servico5_texoBotao_opcao6');
        $dados['body_servico5_botaoLink6'] = $this->input->post('servico5_linkBotao_opcao6');
        if(empty($this->input->post('servico5_botao6'))){$dados['body_servico5_botao6']="0";}else{$dados['body_servico5_botao6']="1";}
        $dados['body_servico5Titulo6'] = $this->input->post('servico5_titulo_opcao6');
        $dados['body_servico5Descricao6'] = $this->input->post('servico5_descricao_opcao6');
        $dados['body_servico5ImagemSEO6'] = $this->input->post('servico5_SEO_opcao6');
        $dados['body_servico5_preco6'] = $this->input->post('servico5_preco_opcao6');
        $dados['body_servico5_opcao6Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao1');
        $dados['body_servico5_opcao6Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao2');
        $dados['body_servico5_opcao6Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao3');
        $dados['body_servico5_opcao6Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao4');
        $dados['body_servico5_opcao6Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao5');
        $dados['body_servico5_opcao6Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao6');
        $dados['body_servico5_opcao6Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao7');
        $dados['body_servico5_opcao6Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao8');
        $dados['body_servico5_opcao6Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao9');
        $dados['body_servico5_opcao6Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao6_opcao10');

        if(empty($this->input->post('servico5_opcao7'))){$dados['body_servico5_opcao7']="0";}else{$dados['body_servico5_opcao7']="1";}
        $dados['body_servico5_botaoTexto7'] = $this->input->post('servico5_texoBotao_opcao7');
        $dados['body_servico5_botaoLink7'] = $this->input->post('servico5_linkBotao_opcao7');
        if(empty($this->input->post('servico5_botao7'))){$dados['body_servico5_botao7']="0";}else{$dados['body_servico5_botao7']="1";}
        $dados['body_servico5Titulo7'] = $this->input->post('servico5_titulo_opcao7');
        $dados['body_servico5Descricao7'] = $this->input->post('servico5_descricao_opcao7');
        $dados['body_servico5ImagemSEO7'] = $this->input->post('servico5_SEO_opcao7');
        $dados['body_servico5_preco7'] = $this->input->post('servico5_preco_opcao7');
        $dados['body_servico5_opcao7Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao1');
        $dados['body_servico5_opcao7Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao2');
        $dados['body_servico5_opcao7Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao3');
        $dados['body_servico5_opcao7Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao4');
        $dados['body_servico5_opcao7Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao5');
        $dados['body_servico5_opcao7Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao6');
        $dados['body_servico5_opcao7Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao7');
        $dados['body_servico5_opcao7Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao8');
        $dados['body_servico5_opcao7Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao9');
        $dados['body_servico5_opcao7Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao7_opcao10');

        if(empty($this->input->post('servico5_opcao8'))){$dados['body_servico5_opcao8']="0";}else{$dados['body_servico5_opcao8']="1";}
        $dados['body_servico5_botaoTexto8'] = $this->input->post('servico5_texoBotao_opcao8');
        $dados['body_servico5_botaoLink8'] = $this->input->post('servico5_linkBotao_opcao8');
        if(empty($this->input->post('servico5_botao8'))){$dados['body_servico5_botao8']="0";}else{$dados['body_servico5_botao8']="1";}
        $dados['body_servico5Titulo8'] = $this->input->post('servico5_titulo_opcao8');
        $dados['body_servico5Descricao8'] = $this->input->post('servico5_descricao_opcao8');
        $dados['body_servico5ImagemSEO8'] = $this->input->post('servico5_SEO_opcao8');
        $dados['body_servico5_preco8'] = $this->input->post('servico5_preco_opcao8');
       
        $dados['body_servico5_opcao8Descricao1'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao1');
        $dados['body_servico5_opcao8Descricao2'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao2');
        $dados['body_servico5_opcao8Descricao3'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao3');
        $dados['body_servico5_opcao8Descricao4'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao4');
        $dados['body_servico5_opcao8Descricao5'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao5');
        $dados['body_servico5_opcao8Descricao6'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao6');
        $dados['body_servico5_opcao8Descricao7'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao7');
        $dados['body_servico5_opcao8Descricao8'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao8');
        $dados['body_servico5_opcao8Descricao9'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao9');
        $dados['body_servico5_opcao8Descricao10'] = $this->input->post('servico5_descricaoRapida_opcao8_opcao10');


        if($_FILES['servico5_imagem_opcao1']['size'] === 0 )
        {
            $dados['body_servico5Imagem1'] = $imagem['servico5'][0]->body_servico5Imagem1;
        }

        if($_FILES['servico5_imagem_opcao2']['size'] === 0 )
        {
            $dados['body_servico5Imagem2'] = $imagem['servico5'][0]->body_servico5Imagem2;
        }
        
        if($_FILES['servico5_imagem_opcao3']['size'] === 0 )
        {
            $dados['body_servico5Imagem3'] = $imagem['servico5'][0]->body_servico5Imagem3;
        }

        if($_FILES['servico5_imagem_opcao4']['size'] === 0 )
        {
            $dados['body_servico5Imagem4'] = $imagem['servico5'][0]->body_servico5Imagem4;
        }

        if($_FILES['servico5_imagem_opcao5']['size'] === 0 )
        {
            $dados['body_servico5Imagem5'] = $imagem['servico5'][0]->body_servico5Imagem5;
        }

        if($_FILES['servico5_imagem_opcao6']['size'] === 0 )
        {
            $dados['body_servico5Imagem6'] = $imagem['servico5'][0]->body_servico5Imagem6;
        }

        if($_FILES['servico5_imagem_opcao7']['size'] === 0 )
        {
            $dados['body_servico5Imagem7'] = $imagem['servico5'][0]->body_servico5Imagem7;
        }

        if($_FILES['servico5_imagem_opcao8']['size'] === 0 )
        {
            $dados['body_servico5Imagem8'] = $imagem['servico5'][0]->body_servico5Imagem8;
        }
        
        if(!empty($_FILES['servico5_imagem_opcao1']['size']))
        {
            
            $imagem1 = 'assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem1;

            if (file_exists($imagem1)) {

                unlink('assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem1);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao1');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao1');

            }
        }

        if(!empty($_FILES['servico5_imagem_opcao2']['size']))
        {
            
            $imagem2 = 'assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem2;

            if (file_exists($imagem2)) {

                unlink('assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem2);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao2');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao2');

            }
        }

        if(!empty($_FILES['servico5_imagem_opcao3']['size']))
        {
            
            $imagem3 = 'assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem3;

            if (file_exists($imagem3)) {

                unlink('assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem3);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao3');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao3');

            }
        }

        if(!empty($_FILES['servico5_imagem_opcao4']['size']))
        {
            
            $imagem4 = 'assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem4;

            if (file_exists($imagem4)) {

                unlink('assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem4);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao4');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao4');

            }
        }

        if(!empty($_FILES['servico5_imagem_opcao5']['size']))
        {
            
            $imagem5 = 'assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem5;

            if (file_exists($imagem5)) {

                unlink('assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem5);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao5');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao5');

            }
        }

        if(!empty($_FILES['servico5_imagem_opcao6']['size']))
        {
            
            $imagem6 = 'assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem6;

            if (file_exists($imagem6)) {

                unlink('assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem6);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem6'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao6');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem6'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao6');

            }
        }

        if(!empty($_FILES['servico5_imagem_opcao7']['size']))
        {
            
            $imagem7 = 'assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem7;

            if (file_exists($imagem7)) {

                unlink('assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem7);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem7'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao7');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem7'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao7');

            }
        }

        if(!empty($_FILES['servico5_imagem_opcao8']['size']))
        {
            
            $imagem8 = 'assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem8;

            if (file_exists($imagem8)) {

                unlink('assets/images/servicos/'.$imagem['servico5'][0]->body_servico5Imagem8);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem8'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao8');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico5Imagem8'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico5_imagem_opcao8');

            }
        }

        $updateServico5= $this->modelos->m_atualizarServico5($dados);;

        if ($updateServico5)
        {

            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>SERVIÇO 5</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados do <strong>SERVIÇO 5</strong>!';
            echo json_encode($retorno);
        }
    
    }

    public function c_atualizarServico6()
    {
        
        $dados['id_modelo'] = $this->input->post('idModelo_servico6');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_servico6');
        $imagem = $this->modelos->buscarServicos($dados['id_landingPage']);
        
        if(empty($this->input->post('servico6_ativaDesativa'))){$dados['body_servico6']="0";}else{$dados['body_servico6']="1";}
        if(empty($this->input->post('servico6_ConteudoGenerico'))){$dados['body_conteudoGenerico6']="0";}else{$dados['body_conteudoGenerico6']="1";}
        
        $dados['body_textoGenericoTitulo6'] = $this->input->post('servico6_titulo');
        $dados['body_textoGenericoDescricao6'] = $this->input->post('servico6_descricao');
        $dados['servico6_corTituloGenerico1'] = $this->input->post('servico6_corTitulo');
        $dados['servico6_corDescricaoGenerico1'] = $this->input->post('servico6_corDescricao');
        $dados['servico6_corBg'] = $this->input->post('servico6_corFundoBG');
        $dados['servico6_corTexto'] = $this->input->post('servico6_corTextos');
        
        if(empty($this->input->post('servico6_opcao1'))){$dados['body_servico6_opcao1']="0";}else{$dados['body_servico6_opcao1']="1";}
        $dados['body_servico6ImagemSEO1'] = $this->input->post('servico6_SEO_opcao1');
        $dados['body_servico6Titulo1'] = $this->input->post('servico6_titulo_opcao1');
        $dados['body_servico6Descricao1'] = $this->input->post('servico6_descricao_opcao1');
        if($_FILES['servico6_imagem_opcao1']['size'] === 0 ){$dados['body_servico6Imagem1'] = $imagem['servico6'][0]->body_servico6Imagem1;}
        if(!empty($_FILES['servico6_imagem_opcao1']['size']))
        {
            
            $imagem1 = 'assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem1;

            if (file_exists($imagem1)) {

                unlink('assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem1);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao1');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao1');

            } 
        }

        if(empty($this->input->post('servico6_opcao2'))){$dados['body_servico6_opcao2']="0";}else{$dados['body_servico6_opcao2']="1";}
        $dados['body_servico6ImagemSEO2'] = $this->input->post('servico6_SEO_opcao2');
        $dados['body_servico6Titulo2'] = $this->input->post('servico6_titulo_opcao2');
        $dados['body_servico6Descricao2'] = $this->input->post('servico6_descricao_opcao2');        
        if($_FILES['servico6_imagem_opcao2']['size'] === 0 ){$dados['body_servico6Imagem2'] = $imagem['servico6'][0]->body_servico6Imagem2;}
        if(!empty($_FILES['servico6_imagem_opcao2']['size']))
        {
            
            $imagem2 = 'assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem2;

            if (file_exists($imagem2)) {

                unlink('assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem2);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao2');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao2');

            } 
        }

        if(empty($this->input->post('servico6_opcao3'))){$dados['body_servico6_opcao3']="0";}else{$dados['body_servico6_opcao3']="1";}
        $dados['body_servico6ImagemSEO3'] = $this->input->post('servico6_SEO_opcao3');
        $dados['body_servico6Titulo3'] = $this->input->post('servico6_titulo_opcao3');
        $dados['body_servico6Descricao3'] = $this->input->post('servico6_descricao_opcao3');
        if($_FILES['servico6_imagem_opcao3']['size'] === 0 ){$dados['body_servico6Imagem3'] = $imagem['servico6'][0]->body_servico6Imagem3;}
        if(!empty($_FILES['servico6_imagem_opcao3']['size']))
        {
            
            $imagem3 = 'assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem3;

            if (file_exists($imagem3)) {

                unlink('assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem3);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao3');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao3');

            } 
        }

        if(empty($this->input->post('servico6_opcao4'))){$dados['body_servico6_opcao4']="0";}else{$dados['body_servico6_opcao4']="1";}
        $dados['body_servico6ImagemSEO4'] = $this->input->post('servico6_SEO_opcao4');
        $dados['body_servico6Titulo4'] = $this->input->post('servico6_titulo_opcao4');
        $dados['body_servico6Descricao4'] = $this->input->post('servico6_descricao_opcao4');
        if($_FILES['servico6_imagem_opcao4']['size'] === 0 ){$dados['body_servico6Imagem4'] = $imagem['servico6'][0]->body_servico6Imagem4;}
        if(!empty($_FILES['servico6_imagem_opcao4']['size']))
        {
            
            $imagem4 = 'assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem4;

            if (file_exists($imagem4)) {

                unlink('assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem4);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao4');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao4');

            } 
        }

        if(empty($this->input->post('servico6_opcao5'))){$dados['body_servico6_opcao5']="0";}else{$dados['body_servico6_opcao5']="1";}
        $dados['body_servico6ImagemSEO5'] = $this->input->post('servico6_SEO_opcao5');
        $dados['body_servico6Titulo5'] = $this->input->post('servico6_titulo_opcao5');
        $dados['body_servico6Descricao5'] = $this->input->post('servico6_descricao_opcao5');
        if($_FILES['servico6_imagem_opcao5']['size'] === 0 ){$dados['body_servico6Imagem5'] = $imagem['servico6'][0]->body_servico6Imagem5;}
        if(!empty($_FILES['servico6_imagem_opcao5']['size']))
        {
            
            $imagem5 = 'assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem5;

            if (file_exists($imagem5)) {

                unlink('assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem5);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao5');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao5');

            } 
        }

        if(empty($this->input->post('servico6_opcao6'))){$dados['body_servico6_opcao6']="0";}else{$dados['body_servico6_opcao6']="1";}
        $dados['body_servico6ImagemSEO6'] = $this->input->post('servico6_SEO_opcao6');
        $dados['body_servico6Titulo6'] = $this->input->post('servico6_titulo_opcao6');
        $dados['body_servico6Descricao6'] = $this->input->post('servico6_descricao_opcao6');
        if($_FILES['servico6_imagem_opcao6']['size'] === 0 ){$dados['body_servico6Imagem6'] = $imagem['servico6'][0]->body_servico6Imagem6;}
        if(!empty($_FILES['servico6_imagem_opcao6']['size']))
        {
            
            $imagem6 = 'assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem6;

            if (file_exists($imagem6)) {

                unlink('assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem6);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem6'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao6');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem6'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao6');

            } 
        }

        if(empty($this->input->post('servico6_opcao7'))){$dados['body_servico6_opcao7']="0";}else{$dados['body_servico6_opcao7']="1";}
        $dados['body_servico6ImagemSEO7'] = $this->input->post('servico6_SEO_opcao7');
        $dados['body_servico6Titulo7'] = $this->input->post('servico6_titulo_opcao7');
        $dados['body_servico6Descricao7'] = $this->input->post('servico6_descricao_opcao7');
        if($_FILES['servico6_imagem_opcao7']['size'] === 0 ){$dados['body_servico6Imagem7'] = $imagem['servico6'][0]->body_servico6Imagem7;}
        if(!empty($_FILES['servico6_imagem_opcao7']['size']))
        {
            
            $imagem7 = 'assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem7;

            if (file_exists($imagem7)) {

                unlink('assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem7);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem7'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao7');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem7'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao7');

            } 
        }

        if(empty($this->input->post('servico6_opcao8'))){$dados['body_servico6_opcao8']="0";}else{$dados['body_servico6_opcao8']="1";}
        $dados['body_servico6ImagemSEO8'] = $this->input->post('servico6_SEO_opcao8');
        $dados['body_servico6Titulo8'] = $this->input->post('servico6_titulo_opcao8');
        $dados['body_servico6Descricao8'] = $this->input->post('servico6_descricao_opcao8');
        if($_FILES['servico6_imagem_opcao8']['size'] === 0 ){$dados['body_servico6Imagem8'] = $imagem['servico6'][0]->body_servico6Imagem8;}
        if(!empty($_FILES['servico6_imagem_opcao8']['size']))
        {
            
            $imagem8 = 'assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem8;

            if (file_exists($imagem8)) {

                unlink('assets/images/servicos/'.$imagem['servico6'][0]->body_servico6Imagem8);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem8'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao8');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico6Imagem8'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico6_imagem_opcao8');

            } 
        }
        
        $updateServico6 = $this->modelos->m_atualizarServico6($dados);;

        if ($updateServico6)
        {

            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>SERVIÇO 6</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados do <strong>SERVIÇO 6</strong>!';
            echo json_encode($retorno);
        }
        
    }

    public function c_atualizarServico7()
    {
        
        $dados['id_modelo'] = $this->input->post('idModelo_servico7');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_servico7');
        $imagem = $this->modelos->buscarServicos($dados['id_landingPage']);
        
        if(empty($this->input->post('servico7_ativaDesativa'))){$dados['body_servico7']="0";}else{$dados['body_servico7']="1";}
        if(empty($this->input->post('servico7_ConteudoGenerico'))){$dados['body_conteudoGenerico7']="0";}else{$dados['body_conteudoGenerico7']="1";}
        
        $dados['body_textoGenericoTitulo7'] = $this->input->post('servico7_titulo');
        $dados['body_textoGenericoDescricao7'] = $this->input->post('servico7_descricao');

        $dados['servico7_corTituloGenerico1'] = $this->input->post('servico7_corTitulo');
        $dados['servico7_corDescricaoGenerico1'] = $this->input->post('servico7_corDescricao');
        $dados['servico7_corBg'] = $this->input->post('servico7_corFundoBG');

        if(empty($this->input->post('servico7_opcao1'))){$dados['body_servico7_opcao1']="0";}else{$dados['body_servico7_opcao1']="1";}
        if(empty($this->input->post('servico7_opcao2'))){$dados['body_servico7_opcao2']="0";}else{$dados['body_servico7_opcao2']="1";}
        if(empty($this->input->post('servico7_opcao3'))){$dados['body_servico7_opcao3']="0";}else{$dados['body_servico7_opcao3']="1";}
        if(empty($this->input->post('servico7_opcao4'))){$dados['body_servico7_opcao4']="0";}else{$dados['body_servico7_opcao4']="1";}
        if(empty($this->input->post('servico7_opcao5'))){$dados['body_servico7_opcao5']="0";}else{$dados['body_servico7_opcao5']="1";}
        if(empty($this->input->post('servico7_opcao6'))){$dados['body_servico7_opcao6']="0";}else{$dados['body_servico7_opcao6']="1";}
        if(empty($this->input->post('servico7_opcao7'))){$dados['body_servico7_opcao7']="0";}else{$dados['body_servico7_opcao7']="1";}
        if(empty($this->input->post('servico7_opcao8'))){$dados['body_servico7_opcao8']="0";}else{$dados['body_servico7_opcao8']="1";}

        $dados['body_servico7ImagemSEO1'] = $this->input->post('servico7_SEO_opcao1');
        $dados['body_servico7ImagemSEO2'] = $this->input->post('servico7_SEO_opcao2');
        $dados['body_servico7ImagemSEO3'] = $this->input->post('servico7_SEO_opcao3');
        $dados['body_servico7ImagemSEO4'] = $this->input->post('servico7_SEO_opcao4');
        $dados['body_servico7ImagemSEO5'] = $this->input->post('servico7_SEO_opcao5');
        $dados['body_servico7ImagemSEO6'] = $this->input->post('servico7_SEO_opcao6');
        $dados['body_servico7ImagemSEO7'] = $this->input->post('servico7_SEO_opcao7');
        $dados['body_servico7ImagemSEO8'] = $this->input->post('servico7_SEO_opcao8');

        if($_FILES['servico7_imagem_opcao1']['size'] === 0 ){$dados['body_servico7Imagem1'] = $imagem['servico7'][0]->body_servico7Imagem1;}
        if($_FILES['servico7_imagem_opcao2']['size'] === 0 ){$dados['body_servico7Imagem2'] = $imagem['servico7'][0]->body_servico7Imagem2;}
        if($_FILES['servico7_imagem_opcao3']['size'] === 0 ){$dados['body_servico7Imagem3'] = $imagem['servico7'][0]->body_servico7Imagem3;}
        if($_FILES['servico7_imagem_opcao4']['size'] === 0 ){$dados['body_servico7Imagem4'] = $imagem['servico7'][0]->body_servico7Imagem4;}
        if($_FILES['servico7_imagem_opcao5']['size'] === 0 ){$dados['body_servico7Imagem5'] = $imagem['servico7'][0]->body_servico7Imagem5;}
        if($_FILES['servico7_imagem_opcao6']['size'] === 0 ){$dados['body_servico7Imagem6'] = $imagem['servico7'][0]->body_servico7Imagem6;}
        if($_FILES['servico7_imagem_opcao7']['size'] === 0 ){$dados['body_servico7Imagem7'] = $imagem['servico7'][0]->body_servico7Imagem7;}
        if($_FILES['servico7_imagem_opcao8']['size'] === 0 ){$dados['body_servico7Imagem8'] = $imagem['servico7'][0]->body_servico7Imagem8;}

        if(!empty($_FILES['servico7_imagem_opcao1']['size']))
        {
            
            $imagem1 = 'assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem1;

            if (file_exists($imagem1)) {

                unlink('assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem1);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao1');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem1'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao1');

            } 
        }

        if(!empty($_FILES['servico7_imagem_opcao2']['size']))
        {
            
            $imagem2 = 'assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem2;

            if (file_exists($imagem2)) {

                unlink('assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem2);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao2');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem2'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao2');

            } 
        }

        if(!empty($_FILES['servico7_imagem_opcao3']['size']))
        {
            
            $imagem3 = 'assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem3;

            if (file_exists($imagem3)) {

                unlink('assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem3);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao3');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem3'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao3');

            } 
        }

        if(!empty($_FILES['servico7_imagem_opcao4']['size']))
        {
            
            $imagem4 = 'assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem4;

            if (file_exists($imagem4)) {

                unlink('assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem4);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao4');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem4'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao4');

            } 
        }

        if(!empty($_FILES['servico7_imagem_opcao5']['size']))
        {
            
            $imagem5 = 'assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem5;

            if (file_exists($imagem5)) {

                unlink('assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem5);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao5');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem5'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao5');

            } 
        }

        if(!empty($_FILES['servico7_imagem_opcao6']['size']))
        {
            
            $imagem6 = 'assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem6;

            if (file_exists($imagem6)) {

                unlink('assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem6);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem6'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao6');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem6'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao6');

            } 
        }

        if(!empty($_FILES['servico7_imagem_opcao7']['size']))
        {
            
            $imagem7 = 'assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem7;

            if (file_exists($imagem7)) {

                unlink('assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem7);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem7'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao7');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem7'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao7');

            } 
        }

        if(!empty($_FILES['servico7_imagem_opcao8']['size']))
        {
            
            $imagem8 = 'assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem8;

            if (file_exists($imagem8)) {

                unlink('assets/images/servicos/'.$imagem['servico7'][0]->body_servico7Imagem8);

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem8'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao8');

            } else {

                $config['upload_path'] = './assets/images/servicos';
                $config['file_name'] = "img_servicos_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['body_servico7Imagem8'] = $config['file_name'];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload('servico7_imagem_opcao8');

            } 
        }
        

        $updateServico7 = $this->modelos->m_atualizarServico7($dados);

        if ($updateServico7)
        {

            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>SERVIÇO 7</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados do <strong>SERVIÇO 7</strong>!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarServico8()
    {
        $dados['id_modelo'] = $this->input->post('idModelo_servico8');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_servico8');
        if(empty($this->input->post('servico8_ativaDesativa'))){$dados['body_maps']="0";}else{$dados['body_maps']="1";}
        if(empty($this->input->post('servico8_ConteudoGenerico'))){$dados['body_conteudoGenerico_maps']="0";}else{$dados['body_conteudoGenerico_maps']="1";}
        
        $dados['body_maps_textoGenericoTitulo1'] = $this->input->post('servico8_titulo');
        $dados['body_maps_textoGenericoDescricao1'] = $this->input->post('servico8_descricao');

        $dados['servico_maps_corTituloGenerico1'] = $this->input->post('servico8_corTitulo');
        $dados['servico_maps_corDescricaoGenerico1'] = $this->input->post('servico8_corDescricao');
        $dados['servico_maps_corBg'] = $this->input->post('servico8_corFundoBG');
        

        $updateServico8 = $this->modelos->m_atualizarServico8($dados);;

        if ($updateServico8)
        {

            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>SERVIÇO 8</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados do <strong>SERVIÇO 8</strong>!';
            echo json_encode($retorno);
        }
    }

    public function c_atualizarServico_paginaSucesso()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dados['id_modelo'] = $this->input->post('idModelo_PaginaSucesso');
        $dados['id_landingPage'] = $this->input->post('idLandingPage_PaginaSucesso');
        $dados['body_textoGenerico_paginaSucesso'] = $this->input->post('tituloPaginaSucesso');
        $dados['body_textoGenericoDescricao_paginaSucesso'] = $this->input->post('descricaoPaginaSucesso');
        $dados['body_icone_paginaSucesso'] = $this->input->post('iconePrincipalPaginaSucesso');
        $dados['body_iconeBotao_paginaSucesso'] = $this->input->post('iconeBotaoPaginaSucesso');
        $dados['body_textoBotao_paginaSucesso'] = $this->input->post('tituloBotaoPaginaSucesso');
        $dados['body_linkBotao_paginaSucesso'] = $this->input->post('linkBotaoPaginaSucesso');
        
        $dados['servico_paginaSucesso_corTextoBotao'] = $this->input->post('corTextoBotaoPaginaSucesso');
        $dados['servico_paginaSucesso_corTextoHoverBotao'] = $this->input->post('corTextoBotaoHoverPaginaSucesso');
        $dados['servico_paginaSucesso_corTextoTitulo'] = $this->input->post('corTextoTituloPaginaSucesso');
        $dados['servico_paginaSucesso_corTextoDescricao'] = $this->input->post('corTextoDescricaoPaginaSucesso');
        $dados['servico_paginaSucesso_corFundoBotao'] = $this->input->post('corFundoBotaoPaginaSucesso');
        $dados['servico_paginaSucesso_corFundoHoverBotao'] = $this->input->post('corFundoBotaoHoverPaginaSucesso');
        $dados['servico_paginaSucesso_corBG'] = $this->input->post('corFundoPaginaSucesso');
        
        
        if(empty($dados['body_textoGenerico_paginaSucesso']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' O <strong>TÍTULO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['body_textoGenericoDescricao_paginaSucesso']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' A <strong>DESCRIÇÃO</strong> não pode ser vazia!<br>';
            $sinal = true;
            
        }

        if(empty($dados['body_icone_paginaSucesso']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Escolha um <strong>ícone principal</strong>!<br>';
            $sinal = true;
            
        }

        if(empty($dados['body_iconeBotao_paginaSucesso']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' Escolha um <strong>ícone/strong> para usar dentro do <strong>botão/strong>!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['body_textoBotao_paginaSucesso']))
        {
            $retorno['ret'] = false;
            $retorno['msg'].= ' O <strong>NOME/strong> do botão não pode ser vazio!<br>';
            $sinal = true;
            
        }
        
        $updatePaginaSucesso = $this->modelos->m_atualizarPaginaSucesso($dados);

        if ($updatePaginaSucesso)
        {

            $retorno['ret'] = true;
            $retorno['msg'] = ' Dados do <strong>PÁGINA DE SUCESSO</strong> atualizados com sucesso';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Falha ao tentar atualizar os dados da <strong>PÁGINA DE SUCESSO</strong>!';
            echo json_encode($retorno);
        }

    }
    
}