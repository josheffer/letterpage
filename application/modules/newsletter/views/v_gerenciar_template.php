
<div class="content-page">

    <div class="content">

        <div class="container-fluid mt-2">

            <div class="row">
                
                <div class="col-12">

                    <?php
                        $this->load->view('botoesAll');
                    ?>
                    
                    <div id="msgErro" class="mt-2"></div>

                    <div class="page-title-box">
                        
                        <div class="row mt-3">
                            <div class="col-xl-12">
                                <div id="accordion" class="mb-3">
                                    
                                    <div class="card mb-1">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="m-0">
                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                                    Criar modelo de template
                                                </a>
                                            </h5>
                                        </div>
                            
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                            
                                            <div class="card-body">

                                            <?php
                                                $contadorAbertura = htmlentities("<img src='{{contadorAbertura}}' width='1px' height='1px'>");
                                            ?>
                                                
                                                <center>
                                                    <h3 class="text-warning">Informações importantes</h3>
                                                    <hr>
                                                        <h5>Variável para o usuário cancelar a inscrição. (Obrigatório ter): <span class="text-warning">{{cancelarInscricao}}</span></h5>
                                                        <h5>Titulo de e-mail. (Obrigatório ter): <span class="text-warning">{{titulo2}}</span></h5>
                                                        <h5>Contar abertura de e-mail. (Obrigatório ter): <span class="text-warning"><?=$contadorAbertura?></span></h5>
                                                        <h5>Para colocar titulo no title, utilizar a variável: <span class="text-warning">{{nomeCampanha}}</span></h5>
                                                        <h5>Para colocar o nome do cliente na mensagem, utilizar a variável: <span class="text-warning">{{nomePessoa}}</span></h5>
                                                        <h5>Para posicionar a mensagem utilizada ao criar a campanha, utilizar a variável: <span class="text-warning">{{mensagem}}</span></h5>
                                                        <h5>Variável link WhatsApp: <span class="text-warning">{{linkWhatsApp}}</span> | Variável link Instagram: <span class="text-warning">{{linkInstagram}}</span> | Variável link Facebook: <span class="text-warning">{{linkFacebook}}</span></h5>
                                                        <h5>Variável link imagem logotipo: <span class="text-warning">{{linkImagemLogotipo}}</span> | Variável link da imagem: <span class="text-warning">{{linkImagem}}</span> | Variável Domínio do site: <span class="text-warning">{{dominio}}</span></h5>
                                                        <h5>Variável link Domínio: <span class="text-warning">{{linkDominio}}</span> | Variável Nome Empresa: <span class="text-warning">{{nomeEmpresa}}</span> | Variável Endereço: <span class="text-warning">{{endereco}}</span></h5>
                                                        <br>
                                                        <br>
                                                        <h5 class="text-warning">ATENÇÃO!! <br> >>>> AO USAR A TAG, UTILIZE TAMBÉM O ID COM O MESMO NOME!! <<<< </h5>
                                                        <h6>Ex: id="nomeCampanha" </h6>
                                                        <br>
                                                        <h5 class="text-warning">ATENÇÃO!! <br> >>>> NÃO USAR COR NA TAG BODY!! <<<< </h5>
                                                    <hr>
                                                </center>

                                                
                                                
                                                <form id="cadastrarNovoModeloTemplateEmail">

                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label class="col-form-label">Nome do modelo de template</label>
                                                            <input type="text" class="form-control" name="nomeModeloTemplateEmail" id="nomeModeloTemplateEmail" autocomplete="off">
                                                        </div>

                                                        <div class="form-group col-md-12">
                                                            <label class="col-form-label">Código HTML do modelo</label>
                                                            <textarea class="form-control" name="codigoHtmlTempEmail" id="codigoHtmlTempEmail" rows="5"></textarea>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    <button type="submit" class="btn btn-block btn-primary waves-effect waves-light">Cadastrar modelo</button>

                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-12">
                                <div id="accordion" class="mb-3">
                                    
                                    <div class="card mb-1">
                                        <div class="card-header" id="modelosTemplatesEmail">
                                            <h5 class="m-0">
                                                <a class="text-dark collapsed" data-toggle="collapse" href="#modelosTemplatesEmailListar" aria-expanded="false">
                                                    Modelos de templates
                                                </a>
                                            </h5>
                                        </div>
                            
                                        <div id="modelosTemplatesEmailListar" class="collapse show" aria-labelledby="modelosTemplatesEmail" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                
                                                <div class="table-responsive">
                                                    <table class="table table-bordered mb-0">

                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Nome</th>
                                                                <th>Status</th>
                                                                <th>Preview</th>
                                                                <th>Ação</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody id="tabelaListarModelosTemplateEmails"></tbody>

                                                    </table>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xl-12">
                                <div id="accordion" class="mb-3">
                                    
                                    <div class="card mb-1">
                                        <div class="card-header" id="informacoes">
                                            <h5 class="m-0">
                                                <a class="text-dark collapsed" data-toggle="collapse" href="#informacoesAlterar" aria-expanded="false">
                                                    Alterar informações que serão enviadas
                                                </a>
                                            </h5>
                                        </div>
                            
                                        <div id="informacoesAlterar" class="collapse" aria-labelledby="informacoes" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                <form id="formAtualizarConfigsTempEmail">
                                                    <div class="form-row">
                                                        
                                                        <div class="form-group col-md-12">
                                                            <label class="col-form-label">link da imagem do logotipo <small  class="text-danger">(140px de larg.)</small> | <small class="text-warning">(Fazer o upload em um site de imagens, sugestão: <a href="https://imgur.com/upload" target="_blank">Clique aqui</a>)</small></label>
                                                            <input type="text" class="form-control" autocomplete="off" name="linkImagemLogo" id="linkImagemLogo">
                                                        </div>

                                                        <div class="form-group col-md-12">
                                                            <label class="col-form-label">link do logotipo <small  class="text-warning">(Ao clicar o usuário será redirecionado para o site ou página específica)</small> </label>
                                                            <input type="text" class="form-control" autocomplete="off" name="linkLogotipo" id="linkLogotipo">
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label class="col-form-label">Nome personalizado da empresa <small  class="text-warning">(Rodapé do e-mail)</small></label>
                                                            <input type="text" class="form-control" autocomplete="off" name="nomeEmpresa" id="nomeEmpresa">
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label class="col-form-label">Endereço <small class="text-warning">(Rua, Qd, Lt, Nº, Cidade, Estado, CEP)</small></label>
                                                            <input type="text" class="form-control" autocomplete="off" name="enderecoEmpresa" id="enderecoEmpresa">
                                                        </div>
                                                        
                                                        <div class="form-group col-md-6">
                                                            <label class="col-form-label">Dominio do site <small class="text-warning">(Aparecerá escrito no rodapé, use: www.meusite.com.br)</small></label>
                                                            <input type="text" class="form-control" autocomplete="off" name="dominioSite" id="dominioSite">
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label class="col-form-label">Link para redirecionar o usuário <small class="text-warning">(URL para enviar o usuário ao clicar no nome do domínio)</small></label>
                                                            <input type="text" class="form-control" autocomplete="off" name="linkDominio" id="linkDominio">
                                                        </div>
                                                        
                                                        <div class="form-group col-md-4 mt-2">
                                                            <label class="col-form-label">Link Facebook</label>
                                                            <input type="text" class="form-control" autocomplete="off" name="linkFacebook" id="linkFacebook">
                                                        </div>

                                                        <div class="form-group col-md-4 mt-2">
                                                            <label class="col-form-label">Link Instagram</label>
                                                            <input type="text" class="form-control" autocomplete="off" name="linkInstagram" id="linkInstagram">
                                                        </div>

                                                        <div class="form-group col-md-4 mt-2">
                                                            <label class="col-form-label">Link WhatsApp </label>
                                                            <input type="text" class="form-control" autocomplete="off" name="linkWhatsApp" id="linkWhatsApp">
                                                        </div>

                                                    </div>

                                                    <button type="submit" class="btn btn-block btn-primary waves-effect waves-light">Atualizar dados</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<div id="modalVerModeloTemplate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Visualizando modelo de template: <i><span class="text-warning" id="nomeModeloTemplateVisualizar"></span></i></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">
            
                <div class="row">

                    <div class="col-md-12">
                        <div id="htmlTemplate"></div>
                    </div>

                </div>
                
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="alterarInformacoesModeloTemplateEmail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alterar informações do modelo de template: <i><span class="text-warning" id="alterarNomeModeloTemplateEmail"></span></i></h4>
                
                <div id="botaoModalFechar">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
            </div>
            
            <div class="modal-body">

            <div id="msgErroAlterarModelotemplate" class="col-md-12"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            
                        <form id="formAlterarModeloTemplateEmail">

                            <input type="hidden" name="idModeloTemplateEmailAlterar" id="idModeloTemplateEmailAlterar">

                            <div class="form-row align-items-center">
                                
                                <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                    <h6 class="font-13">Nome do modelo de template de e-mail</h6>
                                    <input type="text" class="form-control mb-2" id="alterarNomeModeloTemplate" name="alterarNomeModeloTemplate" autocomplete="off" autofocus>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xl-12 col-lg-12">
                                    <h6 class="font-13">Mensagem</h6>
                                    <textarea class="form-control" name="htmlEditarTemplateModelo" id="htmlEditarTemplateModelo" rows="10" cols="80"></textarea>
                                </div>

                                <div class="col-xl-4"></div>
                                
                                <div class="col-xl-12 mt-3">
                                    <button type="submit" class="btn btn-success btn-block waves-effect waves-light">Editar</button>
                                </div>

                                <div class="col-xl-4"></div> 

                            </div>

                        </form>
                            
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>

        </div>
    </div>
</div>

<div id="ModalExcluirModeloTemplate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Excluir modelo de template: <i><span class="text-warning" id="tituloModeloExcluir"></span></i></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div id="mensagemERROExcluirModelo" class="col-md-12 mt-2"></div>

            <form id="formExcluirModelo">

                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="form-group">

                                <input type="hidden" class="form-control" id="idModeloExcluir" name="idModeloExcluir">
                                
                                <p>Deseja realmente excluir o modelo de template:
                                    <strong><span class="text-warning" id="excluirNomeModelo"></span></strong>?
                                    Após a exclusão não será possível reverter essa ação! Tem certeza?
                                </p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Sim, quero excluir</button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Não, cancelar</button>
                </div>
            </form>

        </div>
    </div>
</div>