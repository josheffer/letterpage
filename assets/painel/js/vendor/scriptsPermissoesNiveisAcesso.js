primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listaNiveisPaginasDisponiveis();
listaNiveisPaginasAssociadas();

function listaNiveisPaginasDisponiveis(){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
    
  $.ajax(
    {
        
      url: "/dashboard/permissoes/listaNiveisPaginasDisponiveis/"+numModelo[6],
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#tabelaPaginasDisponiveis').html('');
        
        if(dados.listar.length>0){
          
          addPaginasAoNivel = dados.listar;
          
          $('#nomeNivelAcesso').html(dados.listar[0].nome_nivelAcesso);
          
          for(var i = 0; i < dados.listar.length; i++) {

            if(dados.links[32].permissao_niveisPaginas === '1')
            {
              var botaoAssociar = '<button type="submit" onclick="javascript:associarPagina(' + i + ');" class="btn btn-primary btn-xs waves-effect waves-light"><i class=" mdi mdi-arrow-right-thick"></i></button>';
             
            } else {
              
              var botaoAssociar = '';
              
            }

            $('#tabelaPaginasDisponiveis').append(
              '<tr>' +

                '<td>'+dados.listar[i].nome_pagina+'</td>' +

                '<td>' +
                  botaoAssociar +
                '</td>' +

              '</tr>'
            );

          }
          
        } else {
            
            $('#tabelaPaginasDisponiveis').append(
              '<td colspan="2">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhuma página disponível para ser associada ao nível!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );

        }
      }
    }
  );
}

function listaNiveisPaginasAssociadas(){

  var url_atual = window.location.href;
  var numModelo = url_atual.split("/");
    
  $.ajax(
    {
        
      url: "/dashboard/permissoes/listaNiveisPaginasADD/"+numModelo[6],
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);

        $('#tabelaADD').html('');
        
        if(dados.listarAdd.length>0){

          $('#nomeNivelAcesso').html(dados.listarAdd[0].nome_nivelAcesso);

          dadosGlobaisPaginasADD = dados.listarAdd;
          
          for(var i = 0; i < dados.listarAdd.length; i++) {
            
            
            if(dados.listarAdd[i].permissao_niveisPaginas === '1')
            {
              
              if(dados.linksAdd[34].permissao_niveisPaginas === '1')
              {
                var permissao = '<button type="submit" onclick="javascript:liberarBloquearPermissao(' + i + ');" class="btn btn-primary btn-xs waves-effect waves-light">' +
                                  '<i class="fas fa-lock-open"></i>' +
                                '</button>';
              } else {

                var permissao = '';

              }

            } else {

              if(dados.linksAdd[34].permissao_niveisPaginas === '1')
              {
                var permissao = '<button type="submit" onclick="javascript:liberarBloquearPermissao(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light">' +
                                  '<i class="fas fa-lock"></i>' +
                                '</button>';
              } else {

                var permissao = '';

              }                 
            }


            if(dados.linksAdd[32].permissao_niveisPaginas === '1')
            {
              var botaoRemoverPagina        = '<button type="submit" onclick="javascript:removerPagina(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light"><i class=" mdi mdi-arrow-left-thick"></i></button>';
              
            } else {
              var botaoRemoverPagina        = '';
            }


            
              if(dados.listarAdd[i].menu_niveisPaginas === '1')
              {
                
                if(dados.linksAdd[33].permissao_niveisPaginas === '1')
                {
                  var menu = '<button type="submit" onclick="javascript:liberarBloquearMenu(' + i + ');" class="btn btn-primary btn-xs waves-effect waves-light">' +
                                    '<i class="fas fa-lock-open"></i> ' +
                                  '</button>';
                } else {

                  var menu = '';

                }

              } else {

                if(dados.linksAdd[33].permissao_niveisPaginas === '1')
                {
                  var menu = '<button type="submit" onclick="javascript:liberarBloquearMenu(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light">' +
                                    '<i class="fas fa-lock"></i> ' +
                                  '</button>';
                } else {

                  var menu = '';
                }
            }
            
            $('#tabelaADD').append(
              '<tr>' +
                '<td>'+dados.listarAdd[i].nome_pagina+'</td>' +
                '<td>' +  permissao + '</td>' +
                
                '<td>' + 
                  menu + 
                '</td>' +

                '<td>' +
                  botaoRemoverPagina +
                '</td>' +

              '</tr>'
            );
          }

        } else {
            
            $('#tabelaADD').append(
              '<td colspan="4">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhuma <strong>página associada</strong> disponível para ser gerenciada!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );
        }
      }
    }
  );
}

function associarPagina(addPagina) {

  $.post(
    {
      type: "POST",
      
      data: { 
        id_niveisPaginas: addPaginasAoNivel[addPagina].id_niveisPaginas, 
        id_pagina: addPaginasAoNivel[addPagina].id_pagina, 
        add_niveisPaginas: addPaginasAoNivel[addPagina].add_niveisPaginas
      },

      url: "/dashboard/permissoes/associarRemoverPagina",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroADD').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
          
          listaNiveisPaginasDisponiveis();
          listaNiveisPaginasAssociadas();
          
        } else {
          
          $('#msgErroADD').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
          
          listaNiveisPaginasDisponiveis();
          listaNiveisPaginasAssociadas();
        }
      }
    }
  );
}

function removerPagina(removPagina) {

  $.post(
    {
      type: "POST",
      
      data: { 
        id_niveisPaginas: dadosGlobaisPaginasADD[removPagina].id_niveisPaginas, 
        id_pagina: dadosGlobaisPaginasADD[removPagina].id_pagina, 
        add_niveisPaginas: dadosGlobaisPaginasADD[removPagina].add_niveisPaginas
      },

      url: "/dashboard/permissoes/associarRemoverPagina",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroremoverPagina').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
          
          listaNiveisPaginasDisponiveis();
          listaNiveisPaginasAssociadas();
          
        } else {
          
          $('#msgErroremoverPagina').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
          
          listaNiveisPaginasDisponiveis();
          listaNiveisPaginasAssociadas();
        }
      }
    }
  );
}

function liberarBloquearPermissao(permissao) {

  $.post(
    {
      type: "POST",
      
      data: { 
        id_niveisPaginas: dadosGlobaisPaginasADD[permissao].id_niveisPaginas, 
        id_pagina: dadosGlobaisPaginasADD[permissao].id_pagina, 
        permissao_niveisPaginas: dadosGlobaisPaginasADD[permissao].permissao_niveisPaginas
      },

      url: "/dashboard/permissoes/liberarBloquearPermissao",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroremoverPagina').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
          
          listaNiveisPaginasDisponiveis();
          listaNiveisPaginasAssociadas();
          
        } else {
          
          $('#msgErroremoverPagina').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          listaNiveisPaginasDisponiveis();
          listaNiveisPaginasAssociadas();

        }
      }
    }
  );

}

function liberarBloquearMenu(menu) {

  $.post(
    {
      type: "POST",
      
      data: { 
        id_niveisPaginas: dadosGlobaisPaginasADD[menu].id_niveisPaginas, 
        id_pagina: dadosGlobaisPaginasADD[menu].id_pagina, 
        menu_niveisPaginas: dadosGlobaisPaginasADD[menu].menu_niveisPaginas
      },

      url: "/dashboard/permissoes/liberarBloquearMenu",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroremoverPagina').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          listaNiveisPaginasDisponiveis();
          listaNiveisPaginasAssociadas();
          
        } else {
          
          $('#msgErroremoverPagina').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
          
          listaNiveisPaginasDisponiveis();
          listaNiveisPaginasAssociadas();

        }
      }
    }
  );

}
