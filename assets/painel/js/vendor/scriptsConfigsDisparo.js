$(document).ready(function() {
  $('#botaoMobileAbrirEFechar').trigger('click');
});

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarConfigsDisparo();

function listarConfigsDisparo(){
  
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarConfigs_disparo",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        if (dados.debug_configsEnvios === 0) 
        {
          $('#debug_configDisparo').append('<option value="0" selected>0</option>');
          $('#debug_configDisparo').append('<option value="1">1</option>');
          $('#debug_configDisparo').append('<option value="2">2</option>');
        } else if (dados.debug_contato === 1) 
        {
          $('#debug_configDisparo').append('<option value="0" >0</option>');
          $('#debug_configDisparo').append('<option value="1" selected>1</option>');
          $('#debug_configDisparo').append('<option value="2">2</option>');
        } else 
        {
          $('#debug_configDisparo').append('<option value="0">0</option>');
          $('#debug_configDisparo').append('<option value="1">1</option>');
          $('#debug_configDisparo').append('<option value="2" selected>2</option>');
        }

        if (dados.SMTPSecure_configsEnvios === 'ssl') {
          $('#seguranca_configDisparo').append('<option value="ssl" selected>SSL</option>');
          $('#seguranca_configDisparo').append('<option value="tls">TLS</option>');
        } else {
          $('#seguranca_configDisparo').append('<option value="ssl">SSL</option>');
          $('#seguranca_configDisparo').append('<option value="tls" selected>TLS</option>');
        }

        if (dados.SMTPAuth_configsEnvios === 'true') {
          $('#auth_configDisparo').append('<option value="true" selected>True</option>');
          $('#auth_configDisparo').append('<option value="false">False</option>');
        } else {
          $('#auth_configDisparo').append('<option value="true">True</option>');
          $('#auth_configDisparo').append('<option value="false" selected>False</option>');
        }
        
        $('#host_configDisparo').val(dados.host_configsEnvios);
        $('#usuario_configDisparo').val(dados.usuario_configsEnvios);
        $('#senha_configDisparo').val(dados.senha_configsEnvios);
        $('#debug_configDisparo').val(dados.debug_configsEnvios);
        $('#seguranca_configDisparo').val(dados.SMTPSecure_configsEnvios);
        $('#auth_configDisparo').val(dados.SMTPAuth_configsEnvios);
        $('#porta_configDisparo').val(dados.porta_configsEnvios);
        $('#enconding_configDisparo').val(dados.Encoding_configsEnvios);
        $('#charSet_configDisparo').val(dados.CharSet_configsEnvios);

      }
    }
  );
}

$('#formAtualizarConfigsDisparo').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize()); 
  var retorno = atualizarConfigsDisparo(self);
});

function atualizarConfigsDisparo(dados){
  
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarConfigsDisparo",
      dataType: 'json',

      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarConfigsDisparo();
        }
      }
    }
  );
}
