$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

CKEDITOR.replace('MensagemEnvio');
CKEDITOR.replace('MensagemEditar');

listaGrupos();
listarCampanhas();
listarCampanhasEnviadas();

function listarCampanhas() {

    $.ajax({

        url: "/listagens/listarCampanhas",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#tabelaListarCampanhas').html('');

            dadosGlobais = dados;

            if (dados.CampPendente.length > 0) {


                for (var i = 0; i < dados.CampPendente.length; i++) {

                    if (dados.CampPendente[i].statusBotao_campanha == 0) {

                        var botao = '<button type="button" class="btn btn-dark btn-xs waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Disparar campanha" data-original-title="Disparar campanha" onclick="javascript:AbrirModalPrepararNewsletter(' + i + ');"><span class="btn-label"><i class="fe-send"></i></span>Enviar</button>';

                    }

                    if (dados.CampPendente[i].statusBotao_campanha == 2) {

                        var botao = '<button type="button" class="btn btn-success btn-xs waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Campanha enviada" data-original-title="Campanha enviada"><span class="btn-label"><i class="mdi mdi-check-all"></i></span>Enviada</button>';

                    }

                    $('#tabelaListarCampanhas').append(
                        '<tr>' +

                        '<td>' +
                        '<h5 class="m-0 font-weight-normal">' + dados.CampPendente[i].id_campanha + '</h5>' +
                        '</td>' +

                        '<td>' + dados.CampPendente[i].nome_campanha + '</td>' +

                        '<td>' + dados.CampPendente[i].titulo_campanha + '</td>' +

                        '<td>' + dados.CampPendente[i].assunto_campanha + '</td>' +

                        '<td>' +
                        '<span class="badge badge-light-warning">' + dados.CampPendente[i].nome_grupos + '</span>' +
                        '</td>' +

                        '<td>' + botao + '</td>' +

                        '<td>' +
                        '<a href="javascript: void(0);" onclick="javascript:AbrirModalVerTemplate(' + i + ');" class="btn btn-xs btn-info mr-1" data-toggle="tooltip" data-placement="top" title="Visualizar template" data-original-title="Visualizar template"><i class="mdi mdi-eye"></i></a>' +
                        '<a href="javascript: void(0);" onclick="javascript:AbrirModalEditarCampanha(' + i + ');" class="btn btn-xs btn-primary mr-1" data-toggle="tooltip" data-placement="top" title="Editar Campanha" data-original-title="Editar Campanha"><i class="mdi mdi-pencil-outline"></i></a>' +
                        '<a href="javascript: void(0);" onclick="javascript:showModalDeletarCampanha(' + i + ');" class="btn btn-xs btn-danger"  data-toggle="tooltip" data-placement="top" title="Excluir campanha" data-original-title="Excluir campanha"><i class="fe-trash-2"></i></a>' +
                        '</td>' +

                        '</tr>'
                    );

                }

            } else {

                $('#tabelaListarCampanhas').append(
                    '<tr>' +
                    '<td scope="row" colspan="7" class="text-center col-md-1">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma campanha pendente!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>' +
                    '</tr>'
                );

            }
        }
    });
}

function listarCampanhasEnviadas() {

    $.ajax({

        url: "/listagens/listarCampanhasEnviadas",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#tabelaCampanhasEnviadas').html('');

            dadosGlobaisCampEnviadas = dados;

            if (dados.CampEnviada.length > 0) {

                console.log(dados.CampEnviada);

                for (var i = 0; i < dados.CampEnviada.length; i++) {

                    if (dados.CampEnviada[i].statusBotao_campanha === '1') {
                        var botaoCampEnviadaPendente =
                            '<button type="button" class="btn btn-primary bg-primary btn-xs waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Campanha enviada!" disabled data-original-title="Campanha enviada!">' +
                            '<span class="btn-label"><i class="fas fa-clock"></i></span>Enviando..' +
                            '</button>';
                    }

                    if (dados.CampEnviada[i].statusBotao_campanha === '2') {
                        var botaoCampEnviadaPendente =
                            '<button type="button" class="btn btn-success btn-xs waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Campanha enviada!" disabled data-original-title="Campanha enviada!">' +
                            '<span class="btn-label"><i class="mdi mdi-check-all"></i></span>Enviada' +
                            '</button>';
                    }


                    $('#tabelaCampanhasEnviadas').append(
                        '<tr>' +
                        '<td> <h5 class="m-0 font-weight-normal">' + dados.CampEnviada[i].id_campanha + '</h5> </td>' +

                        '<td>' + dados.CampEnviada[i].nome_campanha + '</td>' +

                        '<td>' + dados.CampEnviada[i].titulo_campanha + '</td>' +

                        '<td>' + dados.CampEnviada[i].assunto_campanha + '</td>' +

                        '<td> <span class="badge badge-light-warning">' + dados.CampEnviada[i].nome_grupos + '</span> </td>' +

                        '<td>' + botaoCampEnviadaPendente + '</td>' +

                        '<td> <a href="javascript: void(0);" onclick="javascript:AbrirModalVerTemplateEnviados(' + i + ');" class="btn btn-xs btn-info mr-1" data-toggle="tooltip" data-placement="top" title="Visualizar template" data-original-title="Visualizar template"><i class="mdi mdi-eye"></i></a></td>' +

                        '</tr>'
                    );

                }

            } else {

                $('#tabelaCampanhasEnviadas').append(
                    '<tr>' +
                    '<td scope="row" colspan="7" class="text-center col-md-1">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma campanha enviada!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>' +
                    '</tr>'
                );

            }
        }
    });
}

$('#formCadastrarNovaCampanha').submit(function(e) {

    e.preventDefault();

    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();

    var self = $(this);

    // alert(self.serialize());

    var retorno = cadastrarNovaCampanha(self);

});

function cadastrarNovaCampanha(dados) {

    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/cadastros/cadastrarNovaCampanha",

        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#mensagemErroNovaCampanha').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" id="fecharMsgExcluir">×</span>' +
                    '</button>' +
                    '<strong>Erro! </strong><br>' + retorno.msg +
                    '</div>'
                );

                $('html,body').scrollTop(0);

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                retorno.ret === true;

                $('#mensagemErroNovaCampanha').html(

                    '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Tudo certo! </strong>' + retorno.msg +
                    '</div>'
                );

                $('#formCadastrarNovaCampanha').each(function() {
                    this.reset();
                });

                $("#codigoGrupoEnviar").val('').selectpicker("refresh");

                CKEDITOR.instances['MensagemEnvio'].setData('');

                $('html,body').scrollTop(0);

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                listarCampanhas();
                listarCampanhasEnviadas();

            }
        }
    });
}

function listaGrupos() {

    $.ajax({

        url: "/listagens/listarGrupo",

        success: function(data) {

            var dados = JSON.parse(data);

            $('#codigoGrupoEnviar').html('');

            if (dados[0].id_grupos !== '0') {

                $('#codigoGrupoEnviar').append('<option value="">Selecione...</option>');
                $('#codigoGrupoEditar').append('<option value="">Selecione...</option>');
                $('.selectpicker').selectpicker('refresh');

                for (var i = 0; i < dados.length; i++) {

                    $('#codigoGrupoEnviar').append('<option value="' + dados[i].id_grupos + '">' + dados[i].nome_grupos + '</option>');

                    $('#codigoGrupoEditar').append('<option value="' + dados[i].id_grupos + '">' + dados[i].nome_grupos + '</option>');

                    $('.selectpicker').selectpicker('refresh');

                }

            } else {

                $('#codigoGrupoEnviar').append('<option value="">Nenhum grupo cadastrado</option>');
                $('#codigoGrupoEditar').append('<option value="">Nenhum grupo cadastrado</option>');
                $('.selectpicker').selectpicker('refresh');

            }

        }
    });
}

function AbrirModalVerTemplate(verTemplateCampPendente) {

    showModalVerTemplate(verTemplateCampPendente);

}

function showModalVerTemplate(verTemplateCampPendente) {

    $('#verTemplateCampanha').modal('show');

    $("#nomeCampanhaVisualizarTemplate").html(dadosGlobais.CampPendente[verTemplateCampPendente].nome_campanha);
    $("#htmlMensagem").html(dadosGlobais.CampPendente[verTemplateCampPendente].html_templateEmail);

    $("#mensagem").html(dadosGlobais.CampPendente[verTemplateCampPendente].mensagem_campanha);
    $('#linkImagemLogotipo').attr('src', dadosGlobais.dadosTempCampPendente[0].linkLogotipo_configs_template);
    $('#linkImagemLogotipo2').attr('src', dadosGlobais.dadosTempCampPendente[0].linkLogotipo_configs_template);
    $('#linkImagem').attr('href', dadosGlobais.dadosTempCampPendente[0].linkImagem_configs_template);
    $('#linkImagem2').attr('href', dadosGlobais.dadosTempCampPendente[0].linkImagem_configs_template);
    $('#linkWhatsApp').attr('href', dadosGlobais.dadosTempCampPendente[0].linkWhatsApp_configs_template);
    $('#linkInstagram').attr('href', dadosGlobais.dadosTempCampPendente[0].linkInstagram_configs_template);
    $('#linkFacebook').attr('href', dadosGlobais.dadosTempCampPendente[0].linkFacebook_configs_template);

    $('#linkWhatsApp2').attr('href', dadosGlobais.dadosTempCampPendente[0].linkWhatsApp_configs_template);
    $('#linkInstagram2').attr('href', dadosGlobais.dadosTempCampPendente[0].linkInstagram_configs_template);
    $('#linkFacebook2').attr('href', dadosGlobais.dadosTempCampPendente[0].linkFacebook_configs_template);

    $("#empresaNome").html(dadosGlobais.dadosTempCampPendente[0].nomeEmpresa_configs_template);
    $("#endereco").html(dadosGlobais.dadosTempCampPendente[0].endereco_configs_template);

    $('#linkDominioRed').attr('href', dadosGlobais.dadosTempCampPendente[0].linkDominio_configs_template);
    $("#dominio").html(dadosGlobais.dadosTempCampPendente[0].dominio_configs_template);


}

function AbrirModalEditarCampanha(editarCampanha) {

    showModalEditarCampanha(editarCampanha);

}

function showModalEditarCampanha(editarCampanha) {

    $('#alterarInformacoesCampanha').modal('show');

    $("#alterarNomeCamp").html(dadosGlobais.CampPendente[editarCampanha].nome_campanha);
    $("#alterarNomeCampanha").val(dadosGlobais.CampPendente[editarCampanha].nome_campanha);
    $("#alterarTituloCampanha").val(dadosGlobais.CampPendente[editarCampanha].titulo_campanha);
    $("#alterarAssuntoCampanha").val(dadosGlobais.CampPendente[editarCampanha].assunto_campanha);
    $("#idCampanhaAlterar").val(dadosGlobais.CampPendente[editarCampanha].id_campanha);
    $("#nomeListaAtual").val(dadosGlobais.CampPendente[editarCampanha].nome_grupos);

    var mensagemEditarcampanha = dadosGlobais.CampPendente[editarCampanha].mensagem_campanha;
    CKEDITOR.instances.MensagemEditar.setData(mensagemEditarcampanha);

}

$('#formAlterarCampanha').submit(function(e) {

    e.preventDefault();

    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();

    var self = $(this);

    // alert(self.serialize());

    var retorno = alterarCampanha(self);

});


function alterarCampanha(dados) {

    $('#fecharMsg').trigger('click');

    $.ajax({

        type: "POST",
        data: dados.serialize(),
        url: "/alteracoes/alterarCampanha",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#msgErroEditarCampanha').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" id="fecharMsgExcluir">×</span>' +
                    '</button>' +
                    '<strong>Erro! </strong><br>' + retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                retorno.ret === true;

                $('#mensagemSUCESSOPrepararLista').html(
                    '<input type="hidden" id="focusPosPrepararLista">' +
                    '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Tudo certo! </strong>' + retorno.msg +
                    '</div>'
                );

                limparFormEditar()

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                $('#alterarInformacoesCampanha').modal('hide');

                listarCampanhas();
                listarCampanhasEnviadas();

            }
        }
    });
}

function limparFormEditar() {
    $(':input', '#formEditarNewsletter')
        .not(':button, :submit, :reset, :hidden')
        .removeAttr('checked')
    $('#editarGrupoDeEnvio').prop('selectedIndex', 1);
}


function showModalDeletarCampanha(deletarCamp) {

    showModalDeletarNewsletter(deletarCamp);

}

function showModalDeletarNewsletter(deletarCamp) {

    $('#deletarNewsletter').modal('show');

    $("#idCampanhaExcluir").val(dadosGlobais.CampPendente[deletarCamp].id_campanha);
    $("#nomeCampanhaExcluir").html(dadosGlobais.CampPendente[deletarCamp].nome_campanha);

}

$('#formExcluirCampanha').submit(function(e) {

    e.preventDefault();

    var self = $(this);

    // alert(self.serialize());

    var retorno = deletarCampanha(self);

});


function deletarCampanha(dados) {

    $('#fecharMsgExcluir').trigger('click');

    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/exclusoes/excluirCampanha",

        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret === 'false') {

                $('#msgExcluirNewsletter').html(
                    '<div class="alert alert-danger">' +
                    '<button type="button" id="fecharMsgExcluir" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                    retorno.msg +
                    '</div>');

            } else {

                retorno.ret === 'true';

                $('#mensagemSUCESSOPrepararLista').html('<div class="alert alert-success">' +
                    '<button type="button" id="fecharMsg" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                    retorno.msg +
                    '</div>');

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                $('#deletarNewsletter').modal('hide');

                listarCampanhas();
                listarCampanhasEnviadas();
            }
        }
    });
}

// aqui

function AbrirModalPrepararNewsletter(prepararCampanha) {

    showModalPrepararNewsletter(prepararCampanha);

}

function showModalPrepararNewsletter(prepararCampanha) {

    $('#prepararCampanha').modal('show');

    $('#fecharMsgPreparar').trigger('click');

    $("#prepararCodigoCampanha").val(dadosGlobais.CampPendente[prepararCampanha].id_campanha);
    $("#nomeNewsletterPreparar").val(dadosGlobais.CampPendente[prepararCampanha].titulo_campanha);
    $("#assuntoNewsletterPreparar").val(dadosGlobais.CampPendente[prepararCampanha].assunto_campanha);
    $("#mensagemNewsletterPreparar").val(dadosGlobais.CampPendente[prepararCampanha].mensagem_campanha);
    $("#grupoNewsletterPreparar").val(dadosGlobais.CampPendente[prepararCampanha].destino_campanha);

    $("#tituloListaPreparar").html("#" + dadosGlobais.CampPendente[prepararCampanha].id_campanha + " - " + dadosGlobais.CampPendente[prepararCampanha].nome_campanha);
    $("#nomeListaPreparar").html("#" + dadosGlobais.CampPendente[prepararCampanha].id_campanha + " - " + dadosGlobais.CampPendente[prepararCampanha].nome_campanha);
    $("#nomeGrupoPreparar").html(dadosGlobais.CampPendente[prepararCampanha].nome_grupos);

    var botaoNews = (dadosGlobais.CampPendente[prepararCampanha].statusBotao_campanha + 1) % 2;
    $("#botaoPrepararCampanha").val(botaoNews);

    $("#botaoPrepararLista").text("Enviar").prop("disabled", false);

    $('#botaoModalFechar').html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');

}

$('#formPrepararListaEnviar').submit(function(e) {

    e.preventDefault();

    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();

    var self = $(this);

    // alert(self.serialize());

    var retorno = prepararCampanha(self);

});

function prepararCampanha(dados) {


    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/cadastros/prepararCampanhaEnvio",

        dataType: 'json',

        beforeSend: function() {

            $('#botaoModalFechar').html('');

            $("#botaoPrepararLista").text("Aguarde, não atualize ou feche a página..").append(
                '<span class="spinner-border spinner-border-sm mr-1 ml-1" role="status" aria-hidden="true"></span>').prop("disabled", true);

        },

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#mensagemERROPrepararLista').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" id="fecharMsgPreparar">×</span>' +
                    '</button>' +
                    '<strong>Erro! </strong><br>' + retorno.msg +
                    '</div>'
                );

                $("#botaoPrepararLista").text("Enviar").prop("disabled", false);

                $('#botaoModalFechar').html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                retorno.ret === true;

                $('#mensagemSUCESSOPrepararLista').html(
                    '<input type="hidden" id="focusPosPrepararLista">' +
                    '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Tudo certo! </strong>' + retorno.msg +
                    '</div>'
                );

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                $('#prepararCampanha').modal('hide');

                listarCampanhas();
                listarCampanhasEnviadas();

            }
        }

    });
}

function AbrirModalVerTemplateEnviados(campEnviadas) {

    showModalVerTemplateEnviados(campEnviadas);

}

function showModalVerTemplateEnviados(campEnviadas) {

    $('#verTemplateCampanhaEnviadas').modal('show');

    $("#nomeCampanhaEnviadaVisualizarTemplate").html(dadosGlobaisCampEnviadas.CampEnviada[campEnviadas].nome_campanha);
    $("#htmlMensagemEnviada").html(dadosGlobaisCampEnviadas.CampEnviada[campEnviadas].html_templateEmail);

    $("#mensagem").html(dadosGlobaisCampEnviadas.CampEnviada[campEnviadas].mensagem_campanha);

    $('#linkImagemLogotipo').attr('src', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkLogotipo_configs_template);
    $('#linkImagemLogotipo2').attr('src', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkLogotipo_configs_template);
    $('#linkImagem').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkImagem_configs_template);
    $('#linkImagem2').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkImagem_configs_template);
    $('#linkWhatsApp').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkWhatsApp_configs_template);
    $('#linkInstagram').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkInstagram_configs_template);
    $('#linkFacebook').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkFacebook_configs_template);

    $('#linkWhatsApp2').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkWhatsApp_configs_template);
    $('#linkInstagram2').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkInstagram_configs_template);
    $('#linkFacebook2').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkFacebook_configs_template);

    $("#empresaNome").html(dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].nomeEmpresa_configs_template);
    $("#endereco").html(dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].endereco_configs_template);

    $('#linkDominioRed').attr('href', dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].linkDominio_configs_template);
    $("#dominio").html(dadosGlobaisCampEnviadas.dadosTempCampEnviada[0].dominio_configs_template);

}