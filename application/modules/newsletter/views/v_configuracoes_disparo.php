<div class="content-page">

    <div class="content">

        <div class="container-fluid">

            <div class="row mt-3">

                <div class="col-lg-12">

                    <?php
                        $this->load->view('botoesAll');
                    ?>

                    <div id="erroMsg" class="mt-2"></div>

                    <div class="card mt-3">

                        <div class="card-body">
                        
                            <div class="row">

                                <div class="col-xl-12">

                                    <div class="card-body">
                                    
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div id="accordion" class="mb-3">
                                                    <div class="card mb-1">
                                                        <div class="card-header" id="attDadosEnvioDisparo">
                                                            <h5 class="m-0">
                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapseCadNovo" aria-expanded="false">
                                                                   Configurações de e-mail
                                                                </a>
                                                            </h5>
                                                        </div>
                                            
                                                        <div id="collapseCadNovo" class="collapse show" aria-labelledby="attDadosEnvioDisparo" data-parent="#accordion">
                                                            <div class="card-body">
                                                                    <div class="row">

                                                                        <div class="col-lg-12">
                                                                            <div class="card">
                                                                                <div class="card-body" style="background-color: #353b44;">
                                                                                
                                                                                    <h6 class="text-center text-warning">OBS: NÃO FAÇA ALTERAÇÕES SE NÃO SOUBER SOBRE CONFIGURAÇÕES DE E-MAILS</h6>
                                                               
                                                                                    <form id="formAtualizarConfigsDisparo">
                                                                                        <div class="form-row">
                                                                                            
                                                                                            <div class="form-group col-md-4">
                                                                                                <label>Host</label>
                                                                                                <input type="text" id="host_configDisparo" name="host_configDisparo" class="form-control">
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group col-md-4">
                                                                                                <label>Usuário</label>
                                                                                                <input type="text" id="usuario_configDisparo" name="usuario_configDisparo" class="form-control">
                                                                                            </div>

                                                                                            <div class="form-group col-md-4">
                                                                                                <label>Senha</label>
                                                                                                <input type="text" id="senha_configDisparo" name="senha_configDisparo" class="form-control">
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group col-md-2">  
                                                                                                <label>DEBUG <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="0 = Não mostra o debug, 1 = mostra algumas informações, 2 = mostra debug completo"></i></label>
                                                                                                <select class="form-control" id="debug_configDisparo" name="debug_configDisparo"></select>
                                                                                            </div>

                                                                                            <div class="form-group col-md-2">
                                                                                                <label>Tipo de segurança <small class="text-warning" style="font-size:8px;">(SSL/TLS)</small></label>
                                                                                                <select class="form-control" id="seguranca_configDisparo" name="seguranca_configDisparo"></select>
                                                                                            </div>

                                                                                            <div class="form-group col-md-2">
                                                                                                <label>Autenticação <small class="text-warning" style="font-size:8px;">(TRUE/FALSE)</small></label>
                                                                                                <select class="form-control" id="auth_configDisparo" name="auth_configDisparo"></select>
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group col-md-2">
                                                                                                <label>Porta</label>
                                                                                                <input type="text" id="porta_configDisparo" name="porta_configDisparo" class="form-control">
                                                                                            </div>

                                                                                            <div class="form-group col-md-2">
                                                                                                <label>Encoding</label>
                                                                                                <input type="text" id="enconding_configDisparo" name="enconding_configDisparo" class="form-control">
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group col-md-2">
                                                                                                <label>CharSet</label>
                                                                                                <input type="text" id="charSet_configDisparo" name="charSet_configDisparo" class="form-control">
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group col-md-12">
                                                                                                <button type="submit" class="btn btn-block btn-success waves-effect">Atualizar configurações</button>
                                                                                            </div>
                                                                                            
                                                                                        </div>
                                                                                    </form>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    
                                                            </div>

                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                                
                                            </div> 
                                            
                                        </div>
                                        
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
