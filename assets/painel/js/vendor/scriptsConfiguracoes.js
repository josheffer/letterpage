listarDadosConfigs();

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

$(document).ready(function() {
  $('#botaoMobileAbrirEFechar').trigger('click');
});

function readURL(input) 
{
    if (input.files && input.files[0]) 
    {
        var reader = new FileReader();

        reader.onload = function (e) 
        {
            $('#previewLogo')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readURLFavicon(input) 
{
    if (input.files && input.files[0]) 
    {
        var reader = new FileReader();

        reader.onload = function (e) 
        {
            $('#previewFavicon')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function listarDadosConfigs(){
  
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarDadosConfigs",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        if(dados.length>0){
          
          //ATRIBUINDO DADOS NA VARIÁVEL GLOBAL
          dadosGlobais = dados;

          if(dados[0].verificaWhatsapp1_landingPage === '1'){$('#confirmaWhatsapp1').val(1).attr("checked", "checked");} else {$('#confirmaWhatsapp1').val(1);}
          if(dados[0].verificaWhatsapp2_landingPage === '1'){$('#confirmaWhatsapp2').val(1).attr("checked", "checked");} else {$('#confirmaWhatsapp2').val(1);}
          
          $('#fone1_configs').val(dados[0].fone1_landingPage);
          $('#fone2_configs').val(dados[0].fone2_landingPage);
          $('#fone1TextoWhats_configs').val(dados[0].fone1TextoWhats_landingPage);
          $('#fone2TextoWhats_configs').val(dados[0].fone2TextoWhats_landingPage);
          $('#facebook_configs').val(dados[0].linkFacebook_landingPage);
          $('#instagram_configs').val(dados[0].linkInstagram_landingPage);
          $('#site_configs').val(dados[0].linkSite_landingPage);
          $('#linkAlternativo1_configs').val(dados[0].linkAlternativo1_landingPage);
          $('#linkAlternativo2_configs').val(dados[0].linkAlternativo2_landingPage);
          $('#email_configs').val(dados[0].email_landingPage);
          $('#endereco_configs').val(dados[0].endereco_landingPage);
          $('#maps_configs').val(dados[0].googleMaps_landingPage);
          
          $('#previewLogo').attr("src", "/assets/images/logofavicon/" + dados[0].logo_landingPage);
          $('#previewFavicon').attr("src", "/assets/images/logofavicon/" + dados[0].favicon_landingPage);
          
        }
      }
    }
  );
}

$('#formConfiguracoes').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarConfiguracoesLandingPages(self);

});
  
function atualizarConfiguracoesLandingPages(dados){
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/landingpages/atualizar/configuracoes",
      dataType: 'json',
      data: new FormData($('#formConfiguracoes')[0]),
      cache: false,
      contentType: false,
      processData: false,

      beforeSend: function(){
        
        $("#botaoAtualizarConfigs").text("Atualizando...").prop("disabled", true);
        $("#fone1_configs").prop("disabled", true);
        $("#fone2_configs").prop("disabled", true);
        $("#email_configs").prop("disabled", true);
        $("#endereco_configs").prop("disabled", true);
        $("#imagemLogotipo_configs").prop("disabled", true);
        $("#imagemFavicon_configs").prop("disabled", true);

      },

      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $("#botaoAtualizarConfigs").text("Tentar novamente...").prop("disabled", false);
          $("#fone1_configs").prop("disabled", false);
          $("#fone2_configs").prop("disabled", false);
          $("#email_configs").prop("disabled", false);
          $("#endereco_configs").prop("disabled", false);
          $("#imagemLogotipo_configs").prop("disabled", false);
          $("#imagemFavicon_configs").prop("disabled", false);

          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarDadosConfigs();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $("#botaoAtualizarConfigs").text("Atualizar").prop("disabled", false);
          $("#fone1_configs").prop("disabled", false);
          $("#fone2_configs").prop("disabled", false);
          $("#email_configs").prop("disabled", false);
          $("#endereco_configs").prop("disabled", false);
          $("#imagemLogotipo_configs").prop("disabled", false);
          $("#imagemFavicon_configs").prop("disabled", false);

          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarDadosConfigs();
          
        }
      }
    }
  );
}
