

<div class="content-page">
    
    <div class="content">
        
        <div class="container-fluid">
        
            <div class="row mt-3">

                <div class="col-12">
                
                    <?php
                        $this->load->view('botoesNiveisAcesso');
                    ?>
                    
                </div>

                <div class="col-12">
                    <div class="text-center">
                        <h3 class="mb-2 text-warning" id="nomeNivelAcesso"></h3>
                        <p class="text-muted mb-4">Associe ou remova as páginas do nível de acesso!</p>
                    </div>
                </div>
                
                <div class="col-12">
                    
                    <div class="row">

                        <div class="col-lg-12">

                            <div class="row">

                                <div class="col-lg-6">

                                    <div class="card-box">
                                        <h4 class="header-title">Páginas disponíveis</h4>
                                        
                                        <p class="sub-header">
                                            Lista de páginas disponíveis para associar ao nível de acesso!
                                        </p>

                                        <div id="msgErroADD"></div>

                                        <div class="table-responsive table-bordered">
                                            <table class="table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Nome da página</th>
                                                        <th>Associar</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaPaginasDisponiveis">
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="col-lg-6">

                                    <div class="card-box">
                                        <h4 class="header-title">Páginas <strong class="text-warning">associadas</strong></h4>
                                        
                                        <p class="sub-header">
                                            Lista de páginas <span class="text-warning">associadas</span> ao nível de acesso!
                                            
                                        <div id="msgErroremoverPagina"></div>
                                        
                                        <div class="table-responsive table-bordered" 
                                            style=" max-width: 100%; height: 500px;">
                                            <table class="table mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Nome da página</th>
                                                        <th>Permissão</th>

                                                        <th>Menu</th>
                                                        <th>Remover</th>

                                                    </tr>
                                                </thead>
                                                <tbody id="tabelaADD">
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
</div>