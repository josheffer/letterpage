listarRelatoriosClicks();
listarRelatorioClicksDiario();
listarRelatorioClicksSemanal();
listarRelatorioClicksMensal();
listarRelatorioClicksAnual();
listarRelatorioClicksBotoes();

$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

var url_atual = window.location.href;
var numModelo = url_atual.split("/");

function listarRelatoriosClicks(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatoriosClicks/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
                
                var dados = JSON.parse(data);
                
                dadosGlobais = dados;
                
                $('#clicksDiario').html(dados.clicks_diario[0].totalClicksDiario);
                $('#clicksSemanais').html(dados.clicks_semanal[0].totalClicksSemanal);
                $('#clicksMensal').html(dados.clicks_mensal[0].totalClicksMensal);
                $('#clicksAnual').html(dados.clicks_anual[0].totalClicksAnual);
                
            }
            
        }
    );
}

function listarRelatorioClicksDiario(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioClicksDiario/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioClicksDiario').append(
        
                        '<tr>' +
                            '<td>'+dados[i].nome_landingPage+'</td>' +
                            '<td>'+dados[i].nomeBotao_clicks+'</td>' +
                            '<td>'+dados[i].linkBotao_clicks+'</td>' +
                            '<td>'+moment(dados[i].dataClick__clicks).format('DD/MM/YYYY')+' às '+moment(dados[i].horaClick__clicks, "H:m").format("H:m")+'</td>' +
                        '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioClicksDiario').append(
                      '<td colspan="4">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum click gerado hoje'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioClicksDiario(relatorioClicksDiario) {
  
    showModalRelatorioClicksDiario(relatorioClicksDiario);
    
}

function showModalRelatorioClicksDiario(relatorioClicksDiario){
  
    $('#leadsRelatorioClicksDiario').modal('show');    
    
}

function listarRelatorioClicksSemanal(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioClicksSemanal/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaLeadsClicksSemanal').append(
        
                        '<tr>' +
                            '<td>'+dados[i].nome_landingPage+'</td>' +
                            '<td>'+dados[i].nomeBotao_clicks+'</td>' +
                            '<td>'+dados[i].linkBotao_clicks+'</td>' +
                            '<td>'+moment(dados[i].dataClick__clicks).format('DD/MM/YYYY')+' às '+moment(dados[i].horaClick__clicks, "H:m").format("H:m")+'</td>' +
                        '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaLeadsClicksSemanal').append(
                      '<td colspan="4">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum click gerado hoje'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioClicksSemanal(relatorioClicksSemanal) {
  
    showModalRelatorioClicksSemanal(relatorioClicksSemanal);
    
}

function showModalRelatorioClicksSemanal(relatorioClicksSemanal){
  
    $('#leadsRelatorioClicksSemanal').modal('show');    
    
}

function listarRelatorioClicksMensal(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioClicksMensal/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaLeadsClicksMensal').append(
        
                        '<tr>' +
                            '<td>'+dados[i].nome_landingPage+'</td>' +
                            '<td>'+dados[i].nomeBotao_clicks+'</td>' +
                            '<td>'+dados[i].linkBotao_clicks+'</td>' +
                            '<td>'+moment(dados[i].dataClick__clicks).format('DD/MM/YYYY')+' às '+moment(dados[i].horaClick__clicks, "H:m").format("H:m")+'</td>' +
                        '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaLeadsClicksMensal').append(
                      '<td colspan="4">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum click gerado nesse mês'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioClicksMensal(relatorioClicksMensal) {
  
    showModalRelatorioClicksMensal(relatorioClicksMensal);
    
}

function showModalRelatorioClicksMensal(relatorioClicksMensal){
  
    $('#leadsRelatorioClicksMensal').modal('show');    
    
}

function listarRelatorioClicksAnual(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioClicksAnual/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaLeadsClicksAnual').append(
        
                        '<tr>' +
                            '<td>'+dados[i].nome_landingPage+'</td>' +
                            '<td>'+dados[i].nomeBotao_clicks+'</td>' +
                            '<td>'+dados[i].linkBotao_clicks+'</td>' +
                            '<td>'+moment(dados[i].dataClick__clicks).format('DD/MM/YYYY')+' às '+moment(dados[i].horaClick__clicks, "H:m").format("H:m")+'</td>' +
                        '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaLeadsClicksAnual').append(
                      '<td colspan="4">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum click gerado nesse ano'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioClicksAnual(relatorioClicksAnual) {
  
    showModalRelatorioClicksAnual(relatorioClicksAnual);
    
}

function showModalRelatorioClicksAnual(relatorioClicksAnual){
  
    $('#leadsRelatorioClicksAnual').modal('show');    
    
}

function listarRelatorioClicksBotoes(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatorioClicksBotoes/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
                
                var dados = JSON.parse(data);
                
                dadosGlobais = dados;
                
                $('#totalClicksWhatsAppLateral1').html(dados.clicks_whatsApp1Lateral[0].totalClicksWhatsApp1Lateral);
                $('#totalClicksWhatsAppLateral2').html(dados.clicks_whatsApp2Lateral[0].totalClicksWhatsApp2Lateral);
                $('#totalClicksWhatsAppRodape1').html(dados.clicks_whatsApp1Rodape[0].totalClicksWhatsApp1Rodape);
                $('#totalClicksWhatsAppRodape2').html(dados.clicks_whatsApp2Rodape[0].totalClicksWhatsApp2Rodape);
                $('#totalClicksFacebookRodape').html(dados.clicks_botaoRodapeFacebook[0].totalClicksBotaoRodapeFacebook);
                $('#totalClicksInstagramRodape').html(dados.clicks_botaoRodapeInstagram[0].totalClicksBotaoRodapeInstagram);
                $('#totalClicksTelefone1Rodape').html(dados.clicks_botaoRodapeTelefone1[0].totalClicksBotaoRodapeTelefone1);
                $('#totalClicksTelefone2Rodape').html(dados.clicks_botaoRodapeTelefone2[0].totalClicksBotaoRodapeTelefone2);
                $('#totalClicksLogoMenu').html(dados.clicks_botaoLogo[0].totalClicksBotaoLogo);
                $('#totalClicksBotaoSlide').html(dados.clicks_botaoSlide[0].totalClicksBotaoSlide);
                $('#totalClicksLinkRodape').html(dados.clicks_linkRodapeSite[0].totalClicksLinkRodapeSite);
                $('#totalClicksBotaoRS1').html(dados.clicks_botaoRedesSociais1[0].totalClicksBotaoRedesSociais1);
                $('#totalClicksBotaoRS2').html(dados.clicks_botaoRedesSociais2[0].totalClicksBotaoRedesSociais2);
                $('#totalClicksBotaoRS3').html(dados.clicks_botaoRedesSociais3[0].totalClicksBotaoRedesSociais3);
                $('#totalClicksBotaoServico2').html(dados.clicks_botaoServico2[0].totalClicksBotaoServico2);
                $('#totalClicksBotaoServico4').html(dados.clicks_botaoServico4[0].totalClicksBotaoServico4);
                $('#totalClicksBotaoServico5Opc1').html(dados.clicks_botaoServico5opc1[0].totalClicksBotaoServico5opc1);
                $('#totalClicksBotaoServico5Opc2').html(dados.clicks_botaoServico5opc2[0].totalClicksBotaoServico5opc2);
                $('#totalClicksBotaoServico5Opc3').html(dados.clicks_botaoServico5opc3[0].totalClicksBotaoServico5opc3);
                $('#totalClicksBotaoServico5Opc4').html(dados.clicks_botaoServico5opc4[0].totalClicksBotaoServico5opc4);
                $('#totalClicksBotaoServico5Opc5').html(dados.clicks_botaoServico5opc5[0].totalClicksBotaoServico5opc5);
                $('#totalClicksBotaoServico5Opc6').html(dados.clicks_botaoServico5opc6[0].totalClicksBotaoServico5opc6);
                $('#totalClicksBotaoServico5Opc7').html(dados.clicks_botaoServico5opc7[0].totalClicksBotaoServico5opc7);
                $('#totalClicksBotaoServico5Opc8').html(dados.clicks_botaoServico5opc8[0].totalClicksBotaoServico5opc8);
                
            }
            
        }
    );
}