primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

linksMenu_admin();
RedesSociaisMenu_admin();

function linksMenu_admin(){
    
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarMenulinks",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#nomeLink1').val(dados[0].nome_link_site);
        $('#rota1').val(dados[0].rota_link_site);
        $('#controller1').val(dados[0].controller_link_site);
        if(dados[0].ativo_link_site === '1'){$('#ativo_link1').val(1).attr("checked", "checked");} else {$('#ativo_link1').val(1);}

        $('#nomeLink2').val(dados[1].nome_link_site);
        $('#rota2').val(dados[1].rota_link_site);
        $('#controller2').val(dados[1].controller_link_site);
        if(dados[1].ativo_link_site === '1'){$('#ativo_link2').val(1).attr("checked", "checked");} else {$('#ativo_link2').val(1);}

        $('#nomeLink3').val(dados[2].nome_link_site);
        $('#rota3').val(dados[2].rota_link_site);
        $('#controller3').val(dados[2].controller_link_site);
        if(dados[2].ativo_link_site === '1'){$('#ativo_link3').val(1).attr("checked", "checked");} else {$('#ativo_link3').val(1);}
        
        $('#nomeLink4').val(dados[3].nome_link_site);
        $('#rota4').val(dados[3].rota_link_site);
        $('#controller4').val(dados[3].controller_link_site);
        if(dados[3].ativo_link_site === '1'){$('#ativo_link4').val(1).attr("checked", "checked");} else {$('#ativo_link4').val(1);}

        $('#nomeLink5').val(dados[4].nome_link_site);
        $('#rota5').val(dados[4].rota_link_site);
        $('#controller5').val(dados[4].controller_link_site);
        if(dados[4].ativo_link_site === '1'){$('#ativo_link5').val(1).attr("checked", "checked");} else {$('#ativo_link5').val(1);}

        $('#nomeLink6').val(dados[5].nome_link_site);
        $('#rota6').val(dados[5].rota_link_site);
        $('#controller6').val(dados[5].controller_link_site);
        if(dados[5].ativo_link_site === '1'){$('#ativo_link6').val(1).attr("checked", "checked");} else {$('#ativo_link6').val(1);}
        
      }
    }
  );
}

function RedesSociaisMenu_admin(){
    
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarMenuRedesSociais",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#link_facebook').val(dados.facebook_link);
        if(dados.facebook_ativo === '1'){$('#ativo_facebook').val(1).attr("checked", "checked");} else {$('#ativo_facebook').val(1);}

        $('#link_instagram').val(dados.instagram_link);
        if(dados.instagram_ativo === '1'){$('#ativo_instagram').val(1).attr("checked", "checked");} else {$('#ativo_instagram').val(1);}

        $('#numero_whatsapp1').val(dados.whatsapp1_numero);
        $('#frase1').val(dados.whatsapp1_frase);
        if(dados.whatsapp1_ativo === '1'){$('#ativo_whatsapp1').val(1).attr("checked", "checked");} else {$('#ativo_whatsapp1').val(1);}

        $('#numero_whatsapp2').val(dados.whatsapp2_numero);
        $('#frase2').val(dados.whatsapp2_frase);
        if(dados.whatsapp2_ativo === '1'){$('#ativo_whatsapp2').val(1).attr("checked", "checked");} else {$('#ativo_whatsapp2').val(1);}

        $('#numero_whatsapp3').val(dados.whatsapp3_numero);
        $('#frase3').val(dados.whatsapp3_frase);
        if(dados.whatsapp3_ativo === '1'){$('#ativo_whatsapp3').val(1).attr("checked", "checked");} else {$('#ativo_whatsapp3').val(1);}
        
      }
    }
  );
}

$('#formLinksMenu').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarLinksMenu(self);
});

function atualizarLinksMenu(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarLinksMenu",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaUsuarios()
          
        }
      }
    }
  );
}

$('#formRedesSociaisMenu').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarRedesSociaisMenu(self);
});

function atualizarRedesSociaisMenu(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarRedesSociaisMenu",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaUsuarios()
          
        }
      }
    }
  );
}