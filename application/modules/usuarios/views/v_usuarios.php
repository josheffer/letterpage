

<div class="content-page">
    
    <div class="content">
        
        <div class="container-fluid">
            
            <div class="row mt-4">
                
                <div class="col-12">
                    
                    <div class="row">

                        <div class="col-lg-12">

                            <div id="msgErro"></div>
                            
                            <div class="card">
                            
                                <?php if($permitirMetodo->permissao_niveisPaginas === '1' && $permitirMetodo->add_niveisPaginas === '1'): ?>
                                    
                                    <div class="card-body">
                                        
                                        <h5 class="card-title mb-3">Criar novo usuário</h5>
                                        
                                        <div class="row">
                                            
                                            <div class="col-lg-12 col-xl-12">
                                                
                                                <div class="card-box">
                                                    
                                                    <form id="formCadastrarNovoUsuario">
                                                        
                                                        <div class="row">
                                                            
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="firstname">Login</label>
                                                                    <input type="text" class="form-control" id="loginUsuario" name="loginUsuario" autocomplete="off">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="lastname">Nome</label>
                                                                    <input type="text" class="form-control" id="nomeUsuario" name="nomeUsuario" autocomplete="off">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="lastname">E-mail</label>
                                                                    <input type="email" class="form-control" id="emailUsuario" name="emailUsuario" autocomplete="off">
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="lastname">Nível de acesso</label>
                                                                    <select class="form-control" id="nivelAcessoUsuario" name="nivelAcessoUsuario"></select>
                                                                </div>
                                                            </div>
                                                            
                                                        </div> 
                                                        
                                                        <div class="text-center">
                                                            <button type="submit" id="botaoCadastrarUsuario" class="btn btn-block btn-success waves-effect waves-light mt-2">
                                                                <i class="mdi mdi-content-save"></i> Cadastrar
                                                            </button>
                                                        </div>

                                                    </form>

                                                </div>

                                            </div> 
                                        </div>
                                        
                                    </div>

                                <?php endif;?>

                            </div>
                            
                            <?php if($permitirMetodoListar->permissao_niveisPaginas === '1' && $permitirMetodoListar->add_niveisPaginas === '1'): ?>
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div id="accordion" class="mb-3">
                                            <div class="card mb-1">
                                                <div class="card-header" id="headingOne">
                                                    <h5 class="m-0">
                                                        <a class="text-dark" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                            Usuários cadastrados
                                                        </a>
                                                    </h5>
                                                </div>
                                    
                                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                                    <div class="card-body">

                                                        <div class="table-responsive">
                                                            <table class="table mb-0">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>Login</th>
                                                                        <th>Nome</th>
                                                                        <th>E-mail</th>

                                                                        <?php if($permissaoLinks[21]->permissao_niveisPaginas === '1'):?><th>Status</th><?php endif;?>
                                                                        
                                                                        <?php if($permissaoLinks[22]->permissao_niveisPaginas === '1' && $permissaoLinks[23]->permissao_niveisPaginas === '1'):?><th>Ações</th><?php endif;?>
                                                                        <?php if($permissaoLinks[22]->permissao_niveisPaginas === '0' && $permissaoLinks[23]->permissao_niveisPaginas ===  '1'):?><th>Ações</th><?php endif;?>
                                                                        <?php if($permissaoLinks[22]->permissao_niveisPaginas === '1' && $permissaoLinks[23]->permissao_niveisPaginas === '0'):?><th>Ações</th><?php endif;?>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="tabelaListarUsuarios">
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        

                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            <?php endif;?>


                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
</div>

<?php if($permissaoLinks[21]->permissao_niveisPaginas === '1'):?>
    <div id="modalAtualizarStatus" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mySmallModalLabel">Atualizar status</h4>
                    <button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <form id="formAtualizaStatus">
                    <div class="modal-body">
                        
                        <div id="msgErroAtualizarStatus"></div>

                        Tem certeza que deseja atualizar o status desse usuário??
                        <input type="hidden" name="idUsuario" id="idUsuario">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">(Sim) Atualizar</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif;?>

<?php if($permissaoLinks[22]->permissao_niveisPaginas === '1'):?>
    <div id="modalEditarUsuario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">

        <div class="modal-dialog modal-full">

            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title">Alterar usuário: <u>#<span id="idUsuarioAtualizar"></span> - <span class="text-warning" id="atualizarNomeUsuarioTitulo"></span></u></h4>
                    <button type="button" id="fecharModalAtualizarUsuario" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                <form id="formAtualziarUsuario">

                    <div class="modal-body p-4">

                        <div id="msgErroAtualizarUsuario"></div>
                        
                        <input type="hidden" id="atualizarIdUsuario" name="atualizarIdUsuario">
                            
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Login</label>
                                    <input type="text" class="form-control" id="atualizarLoginUsuario" name="atualizarLoginUsuario" autocomplete="off">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-2" class="control-label">Nome</label>
                                    <input type="text" class="form-control" id="atualizarNomeUsuario" name="atualizarNomeUsuario" autocomplete="off">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">E-mail</label>
                                    <input type="text" class="form-control" id="atualizarEmailUsuario" name="atualizarEmailUsuario" autocomplete="off">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lastname">Nível de acesso</label>
                                    <select class="form-control" id="atualizarNivelAcessoUsuario" name="atualizarNivelAcessoUsuario"></select>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-success waves-effect waves-light">Atualizar</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
<?php endif;?>

<?php if($permissaoLinks[23]->permissao_niveisPaginas === '1'):?>
    <div id="modalDeletarUsuario" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mySmallModalLabel">Deletar Usuario</h4>
                    <button type="button" id="fecharModalDeletarusuario" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <form id="formDeletarUsuario">
                    <input type="hidden" id="idDeletarusuario" name="idDeletarusuario">
                    <div class="modal-body">
                        
                        <div id="msgErroDeletarUsuario"></div>
                        
                        <div id="msgErroAtualizarStatus"></div>

                        Tem certeza que deseja deletar esse usuário??

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">(Sim) DELETAR</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif;?>

<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>