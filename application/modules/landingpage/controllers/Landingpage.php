<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');
class Landingpage extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->model("LandingPage_model", "landingpage");
        $this->load->model("TemplateLandingPage_model", "template");
        $this->load->model("Modelos_model", "modelos");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->library('permitiracesso');
        $this->load->model("empresa/Empresa_model", "empresa");

        
    }
    
	public function index()
	{
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        
        $view = "v_landingPages";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);

        $scripts = array(

            "titulo" => "Landings Page | Solid System",

            "scriptsJS" => "<script src='/assets/libs/custombox/custombox.min.js'></script>
                            <script src='/assets/painel/js/vendor/scriptsLandingPage.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "<link href='/assets/libs/custombox/custombox.min.css' rel='stylesheet'>",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }
    
    public function v_cadastrarNovaLandingPage()
    {
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_cadastrarNovaLandingPage";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Cadastrar nova Landings Page | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsLandingPage.js'></script>",
            "scriptsCSS" => "",
            "scriptsJSacima" => "",
            "scriptsCSSacima" => ""
        );

        $this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }
    
    public function v_configuracoes()
    {
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;
        
        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_configuracoes";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);

        $resultado = $this->landingpage->buscarConfisgs($id = 1);
        
        $dadosSessao['configs'] = $resultado[0];

        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Configurações Landings Page | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsConfiguracoes.js'></script>",
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",
            "scriptsJSacima" => "",
            "scriptsCSSacima" => ""
            
        );

        $this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
        
    }

    public function c_cadastrarNovaLandingPage()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/cadastrarNovaLandingPage";

        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;

                $id = $this->landingpage->m_gerarCodigoCriarLandingPage();

                $dados['id_landingPage'] = $id;

                if(empty($this->input->post('nomeLandingPage')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                } else {

                    $dados['nome_landingPage'] = $this->input->post('nomeLandingPage');
                }

                $tamanhoNome = strlen($this->input->post('nomeLandingPage'));

                if($tamanhoNome > 80)
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser maior que <strong>80</strong> caracteres!<br>';
                    $sinal = true;
                    
                } else {

                    $dados['nome_landingPage'] = $this->input->post('nomeLandingPage');
                }
                
                if(empty($this->input->post('tituloLandingPage')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>TÍTULO</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                } else {

                    $dados['titulo_landingPage'] = $this->input->post('tituloLandingPage');
                    
                }
                
                if(empty($this->input->post('listaEmailsLandingPage')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'Por favor, escolha um <strong>GRUPO</strong> para receber os <strong>CADASTROS</strong><br>';
                    $sinal = true;
                    
                } else {

                    $dados['grupoLista_landingPage'] = $this->input->post('listaEmailsLandingPage');
                    
                }
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }
                
                $dados['status_landingPage'] = '1';
                $dados['template_landingPage'] = 1;
                $string = $dados['titulo_landingPage'];
                $nomeLink = str_replace(" ","-", preg_replace("/&([a-z])[a-z]+;/i", "$1", strtolower(htmlentities(trim($string)))));
                
                $dados['link_landingPage'] = base_url()."landingpages/".$id."/".$nomeLink;
                
                $resultado = $this->landingpage->m_CriarLandingPage($dados);

                if($resultado)
                {
                    $retorno['ret'] = true;
                    $retorno['msg'] = ' LandingPage <strong>'. $dados['nome_landingPage'].'</strong> criada com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = ' Não foi possível criar a LandingPage '. $dados['nome_landingPage'].', tente novamente mais tarde!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function atualizarConfigsLandingPage()
    {
        
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/landingpages/atualizar/configuracoes";

        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;
        
                if(empty($this->input->post('fone1_configs')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>FONE 1</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($this->input->post('facebook_configs')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O link do <strong>Facebook</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($this->input->post('instagram_configs')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O link do <strong>Instagram</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($this->input->post('site_configs')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O link do <strong>Site</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($this->input->post('email_configs')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>E-MAIL</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }
                
                $dados['fone1_landingPage'] = $this->input->post('fone1_configs');
                $dados['fone2_landingPage'] = $this->input->post('fone2_configs');
                $dados['fone1TextoWhats_landingPage'] = $this->input->post('fone1TextoWhats_configs');
                $dados['fone2TextoWhats_landingPage'] = $this->input->post('fone2TextoWhats_configs');

                if(empty($this->input->post('confirmaWhatsapp1'))){$dados['verificaWhatsapp1_landingPage'] = '0';}
                else {$dados['verificaWhatsapp1_landingPage'] = '1';}

                if(empty($this->input->post('confirmaWhatsapp2'))){$dados['verificaWhatsapp2_landingPage'] = '0';}
                else {$dados['verificaWhatsapp2_landingPage'] = '1';}
                
                $dados['linkFacebook_landingPage'] = $this->input->post('facebook_configs');
                $dados['linkInstagram_landingPage'] = $this->input->post('instagram_configs');
                $dados['linkSite_landingPage'] = $this->input->post('site_configs');
                $dados['linkAlternativo1_landingPage'] = $this->input->post('linkAlternativo1_configs');
                $dados['linkAlternativo2_landingPage'] = $this->input->post('linkAlternativo2_configs');
                $dados['email_landingPage'] = $this->input->post('email_configs');
                $dados['endereco_landingPage'] = $this->input->post('endereco_configs');
                $dados['googleMaps_landingPage'] = $this->input->post('maps_configs');
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                // SEM IMAGEM SELECIONADA
                // VAI MANTER A IMAGEM ATUAL
                if($_FILES['imagemLogotipo_configs']['size'] === 0 )
                {
                    $id = 1;

                    $resultado = $this->landingpage->buscarConfisgs($id);

                    foreach ($resultado as $key){

                        $dados['logo_landingPage'] = $key->logo_landingPage;
                    }
                }

                if($_FILES['imagemFavicon_configs']['size'] === 0 )
                {
                    $id = 1;

                    $resultado = $this->landingpage->buscarConfisgs($id);

                    foreach ($resultado as $key){

                        $dados['favicon_landingPage'] = $key->favicon_landingPage;
                    }
                }
                
                if(!empty($_FILES['imagemLogotipo_configs']['size']))
                {
                    $id = 1;

                    $result = $this->landingpage->buscarConfisgs($id);

                    foreach ($result as $key) {

                        $logoExiste = 'assets/images/logofavicon/'.$key->logo_landingPage;

                        if (file_exists($logoExiste)) {

                            unlink('assets/images/logofavicon/'.$key->logo_landingPage);

                            $config['upload_path'] = './assets/images/logofavicon';
                            $config['file_name'] = "img_logo_".rand().".jpg";
                            $config['allowed_types'] = '*' ;

                            $dados['logo_landingPage'] = $config['file_name'];

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('imagemLogotipo_configs');

                        } else {

                            $config['upload_path'] = './assets/images/logofavicon';
                            $config['file_name'] = "img_logo_".rand().".jpg";
                            $config['allowed_types'] = '*' ;

                            $dados['logo_landingPage'] = $config['file_name'];

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('imagemLogotipo_configs');

                        }
                        
                    }

                }

                if(!empty($_FILES['imagemFavicon_configs']['size']))
                {

                    $id = 1;

                    $result2 = $this->landingpage->buscarConfisgs($id);

                    foreach ($result2 as $key2) {

                        $faviconExiste = 'assets/images/logofavicon/'.$key2->favicon_landingPage;

                        if (file_exists($faviconExiste)) {

                            unlink('assets/images/logofavicon/'.$key2->favicon_landingPage);

                            $config2['upload_path'] = './assets/images/logofavicon';
                            $config2['file_name'] = "img_favicon_".rand().".jpg";
                            $config2['allowed_types'] = '*' ;

                            $dados['favicon_landingPage'] = $config2['file_name'];

                            $this->load->library('upload', $config2);
                            $this->upload->initialize($config2);
                            $this->upload->do_upload('imagemFavicon_configs');
                            
                        } else {

                            $config2['upload_path'] = './assets/images/logofavicon';
                            $config2['file_name'] = "img_favicon_".rand().".jpg";
                            $config2['allowed_types'] = '*' ;

                            $dados['favicon_landingPage'] = $config2['file_name'];

                            $this->load->library('upload', $config2);
                            $this->upload->initialize($config2);
                            $this->upload->do_upload('imagemFavicon_configs');
                            
                        }
                        
                    }

                }
                
                $resultado = $this->landingpage->alterarDadosConfigs($dados, $id);

                if ($resultado)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Configurações alteradas com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Falha ao tentar alterar os dados de configurações!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

    }

    public function c_listarDadosConfigs()
    {

        $id = 1;

        $resultado = $this->landingpage->buscarConfisgs($id);

        echo json_encode($resultado);

    }

    public function c_listarTodasLandingPages()
    {
        $dadosSessao['dados'] = $this->session->userdata;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $resultado['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        $resultado['dados'] = $this->landingpage->m_listarTodasLandingPages();

        echo json_encode($resultado);
    }
    
    public function v_landingPages()
    {
        // CONTADOR DE VIEWS
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[2];
        
        $carregarLandingPage = $this->landingpage->buscarLandingPage($id);
        
        if(!$carregarLandingPage || $carregarLandingPage[0]->status_landingPage === '0')
        {   
            echo $this->load->view('errors/html/404');

        } else {

            $carregarConfig = $this->landingpage->buscarConfisgs($idconf=1);
            $carregarConfigsModeloHead = $this->template->buscarConfigsHead($id);
            $carregarConfigsModeloLogoMenuSlide = $this->template->buscarConfigsLogoMenuSlide($id);
            $carregarConfigsModeloRedesSociais = $this->template->buscarConfigsRedesSociais($id);
            $carregarConfigsModeloServico1 = $this->template->buscarConfigsServico1($id);
            $carregarConfigsModeloServico2 = $this->template->buscarConfigsServico2($id);
            $carregarConfigsModeloServico3 = $this->template->buscarConfigsServico3($id);
            $carregarConfigsModeloServico4 = $this->template->buscarConfigsServico4($id);
            $carregarConfigsModeloServico5 = $this->template->buscarConfigsServico5($id);
            $carregarConfigsModeloServico6 = $this->template->buscarConfigsServico6($id);
            $carregarConfigsModeloServico7 = $this->template->buscarConfigsServico7($id);
            $carregarConfigsModeloServico8 = $this->template->buscarConfigsServico8($id);
            $carregarConfigsModeloServicoPaginaSucesso = $this->template->buscarConfigsPaginaSucesso($id);
            $carregarConfigsModeloMaps = $this->template->buscarConfigsMaps($id);
            $carregarConfigsModeloForms = $this->template->buscarConfigsForms($id);
            $carregarConfigsModeloPersonalizacao = $this->template->buscarConfigsPersonalizacao($id);
            
            $this->load->library('contador');

            $dispositivo = $this->contador->dispositivo();
            
            if($_SERVER['REMOTE_ADDR'] === '192.168.10.1')
            {$ip = "177.30.111.114";}else{$ip = $_SERVER['REMOTE_ADDR'];}
            
            $dados_ip['user_ip'] = $ip;
            
            $geo = json_decode($this->contador->consultaCidadeEstado("http://extreme-ip-lookup.com/json/".$dados_ip['user_ip']));
            
            $contador['ip_contador'] = $dados_ip['user_ip'];
            if(is_null($geo->city)){$contador['cidade_contador']="Goiania";}else{$contador['cidade_contador']=$geo->city;}
            if(is_null( $geo->region)){$contador['estado_contador']="Goias";}else{$contador['estado_contador']=$geo->region;}
            if(is_null( $geo->country)){$contador['pais_contador']="Brazil";}else{$contador['pais_contador']=$geo->country;}
            $contador['dispositivo_contador'] = $dispositivo['dispositivo'];
            $contador['navegador_contador'] = $dispositivo['navegador'];
            $contador['SO_contador'] = $dispositivo['sistemaOperacional'];
            $contador['landingPage_contador'] = $carregarLandingPage[0]->id_landingPage;
            
            $contador['data_contador'] = date('Ymd');
            $contador['hora_contador'] = date('Hi');
            $contador['token_contador'] = session_id();
            
            //VERIFICA SE JÁ EXISTE UM TOKEN PARA A PAGINA ATUAL
            $resultToken = $this->landingpage->m_buscarContadorToken($contador['token_contador'], $contador['landingPage_contador']);
            
            if(!$resultToken)
            {
                $this->landingpage->m_contadorDeVisitas($contador);
            }

            $carregarConfigsModelo = (object)array_merge(
                                        (array)$carregarConfigsModeloHead, 
                                        (array)$carregarConfigsModeloLogoMenuSlide,
                                        (array)$carregarConfigsModeloRedesSociais,
                                        (array)$carregarConfigsModeloServico1,
                                        (array)$carregarConfigsModeloServico2,
                                        (array)$carregarConfigsModeloServico3,
                                        (array)$carregarConfigsModeloServico4,
                                        (array)$carregarConfigsModeloServico5,
                                        (array)$carregarConfigsModeloServico6,
                                        (array)$carregarConfigsModeloServico7,
                                        (array)$carregarConfigsModeloServico8,
                                        (array)$carregarConfigsModeloMaps,
                                        (array)$carregarConfigsModeloForms,
                                        (array)$carregarConfigsModeloPersonalizacao,
                                        (array)$carregarConfigsModeloServicoPaginaSucesso
                                    );
                                                        
            foreach ($carregarLandingPage as $dadosLandingPage) {
                
                $configsModelo = array(

                    "titulo" => $dadosLandingPage->titulo_landingPage,
                    "configs" => $carregarConfig[0],
                    "configsModelo" => $carregarConfigsModelo,
                    "url_atual" => $url[0],
                    "dadosLandingPage" => $dadosLandingPage,
                    "tempoOnline" => "<script src='/assets/painel/js/vendor/scriptTempoOnline.js'></script>",
                    "token" =>  $contador['token_contador'],
                    "scriptCSS" => "",
                    "scriptJS" => "<script src='/assets/painel/js/vendor/scriptsLeadsCadastroForms.js'></script>
                                    <script src='/assets/painel/js/vendor/scriptsColetaClicks.js'></script>
                                    <script src='/assets/libs/jquery-mask-plugin/jquery.mask.min.js'></script>
                                    <script src='/assets/libs/form-masks.init.js'></script>
                                    "
                    
                );
                
                $this->load->view('modelos/modelo'
                                    .$dadosLandingPage->template_landingPage.
                                    '/v_modelo'
                                    .$dadosLandingPage->template_landingPage, $configsModelo);

            }
        }
    }

    public function c_tempoOnline()
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $token = $this->input->post('token');
        
        $resultado = $this->landingpage->buscarLandingPageToken($token, $id);
        
        $tempo1 = $this->input->post('tempoOnline');
        $tempo2 = $resultado[0]->tempo_contador;

        $tempoTotal = $tempo1+$tempo2;

        $arrayTempo = explode('.', $tempoTotal);
        
        $dados['landingPage_contador'] = $id;
        $dados['token_contador'] = $this->input->post('token');
        $dados['tempo_contador'] = $arrayTempo[0];
        
        $this->landingpage->m_tempoOnline($dados);

    }
    
    public function c_atualizarStatusLandingPage()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/landingpages/atualizar/atualizarStatusLandingPage";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;
            
                $id = $this->input->post('idLandingPage');
                
                $dados['id_landingPage'] = $id;
                
                $resultado = $this->landingpage->buscarLandingPage($id);
                
                $statusAtual = $resultado[0]->status_landingPage;

                if($statusAtual === '1')
                {

                    $dados['status_landingPage'] = '0';

                } else {

                    $dados['status_landingPage'] = '1';

                }

                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                $result = $this->landingpage->m_atualizarStatusLaingPage($dados);

                if ($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Status alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar o status!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }
    
    public function c_excluirLandingPages()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/landingpages/excluir/excluirLandingPages";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else { 
        
            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id= $this->input->post('idLandingPageExcluir');
                
                $result = $this->landingpage->m_excluirLadingPage($id);
                
                if($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Landing Page excluída com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível excluir a Landing Page!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_editarLandingPage()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/landingpages/atualizar/editarLandingPage";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '1')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            $retorno['msg'] = "";
            $sinal = false;
            
            if(empty($this->input->post('nomeLandingPageEditar')))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }

            if(empty($this->input->post('tituloLandingPageEditar')))
            {
                $retorno['ret'] = false;
                $retorno['msg'].= 'O <strong>TÍTULO1</strong> não pode ser vazio!<br>';
                $sinal = true;
                
            }
            
            $dados['id_landingPage'] = $this->input->post('idLandingPageEditar');
            $dados['nome_landingPage'] = $this->input->post('nomeLandingPageEditar');
            $dados['titulo_landingPage'] = $this->input->post('tituloLandingPageEditar');
            
            $landingPage = $this->landingpage->buscarLandingPage($dados['id_landingPage']);
            
            if($this->input->post('listaEmailsLandingPage') === ''){

                $dados['grupoLista_landingPage'] = $landingPage[0]->grupoLista_landingPage;

            } else {

                $dados['grupoLista_landingPage'] = $this->input->post('listaEmailsLandingPage');

            }

            $dados['titulo_landingPage'] = $this->input->post('tituloLandingPageEditar');
            
            if($sinal)
            {
                echo json_encode($retorno);
                exit;
            }
            
            $id = $dados['id_landingPage']; 
                
            $string = $dados['titulo_landingPage'];
            $nomeLink = str_replace(" ","-", preg_replace("/&([a-z])[a-z]+;/i", "$1", strtolower(htmlentities(trim($string)))));
            
            $dados['link_landingPage'] = base_url()."landingpages/".$id."/".$nomeLink;

            $result = $this->landingpage->m_atualizarDadosLanding($dados);
            
            if($result)
            {
                
                $retorno['ret'] = true;
                $retorno['msg'] = 'Landing Page alterada com sucesso!';
                echo json_encode($retorno);
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'] = 'Desculpa, não foi possível alterar a Landing Page!';
                echo json_encode($retorno);
            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

    }

    public function v_templateLandingPages()
    {
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_templates";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $resultado = $this->landingpage->buscarConfisgs($id = 1);
        
        $dadosSessao['configs'] = $resultado[0];

        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Templates Landings Page | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsTemplates.js'></script>",
            "scriptsCSS" => "",
            "scriptsJSacima" => "",
            "scriptsCSSacima" => ""
            
        );

        $this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
        $this->load->view('painel/Template/t_menuLateral');
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);

    }
    
    public function c_cadastrarNovoTemplateLandingPage()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/cadastro/cadastrarNovoTemplateLandingPage";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;

                $id = $this->template->m_gerarCodigoCriarTemplate();

                $dados['id_templates'] = $id;
                
                if(empty($this->input->post('nomeTemplate')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                $dados['nomeTemplate_templates'] = $this->input->post('nomeTemplate');

                if($_FILES['imagemTemplate']['size'] === 0 )
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'A <strong>FOTO</strong> não pode ser vazia!<br>';
                    $sinal = true;
                    
                }
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }
                
                $config['upload_path'] = './assets/images/templates';
                $config['file_name'] = "img_templates_".rand().".jpg";
                $config['allowed_types'] = '*' ;

                $dados['imagemTemplate_templates'] = $config['file_name'];

                $result = $this->template->m_criarNovoTemplate($dados);
                
                if($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Modelo de template criado com sucesso!';
                    echo json_encode($retorno);

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $this->upload->do_upload('imagemTemplate');
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível criar o modelo de template';
                    echo json_encode($retorno);
                }
                
            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

    }

    public function c_listarTodosTemplates()
    {
        $dadosSessao['dados'] = $this->session->userdata;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $resultado['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $resultado['dados'] = $this->template->m_listarTodosTemplates();

        echo json_encode($resultado);
    }
    
    public function c_listarTodosTemplatesLandingPages()
    {
        $resultado = $this->template->m_listarTodosTemplatesLandingPages();

        echo json_encode($resultado);
    }

    public function c_atualizarStatusTemplates()
    {
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/atualizar/atualizarStatusTemplates";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id = $this->input->post('idTemplate');

                $dados['id_templates'] = $id;
                
                $resultado = $this->template->buscarTemplate($id);
                
                $statusAtual = $resultado[0]->statusTemplate_templates;

                if($statusAtual === '1')
                {

                    $dados['statusTemplate_templates'] = '0';

                } else {

                    $dados['statusTemplate_templates'] = '1';

                }

                $result = $this->template->m_atualizarStatusTemplate($dados);

                if ($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Status alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar o status!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_atualizarDadosTemplate()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/atualizar/atualizarDadosTemplate";

        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;

                $id = $this->input->post('idAtualizarTemplate');

                $dados['id_templates'] = $id;
                
                $resultado = $this->template->buscarTemplate($id);

                if(empty($this->input->post('nomeAtualizarTemplate')))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                } 
                
                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                $dados['nomeTemplate_templates'] = $this->input->post('nomeAtualizarTemplate');

                // SEM IMAGEM SELECIONADA
                // VAI MANTER A IMAGEM ATUAL
                if($_FILES['imagemAtualizarTemplate']['size'] === 0 )
                {
                    $id = $dados['id_templates'];

                    $resultado = $this->template->buscarTemplate($id);
                    
                    foreach ($resultado as $key){

                        $dados['imagemTemplate_templates'] = $key->imagemTemplate_templates;

                        $dados['statusTemplate_templates'] = $key->statusTemplate_templates;
                    }
                }

                if(!empty($_FILES['imagemAtualizarTemplate']['size']))
                {
                    $id = $dados['id_templates'];

                    $result = $this->template->buscarTemplate($id);

                    foreach ($result as $key) {
                        
                        $imagemTemplateExiste = 'assets/images/templates/'.$key->imagemTemplate_templates;

                        if (file_exists($imagemTemplateExiste)) {

                            unlink('assets/images/templates/'.$key->imagemTemplate_templates);

                            $config['upload_path'] = './assets/images/templates';
                            $config['file_name'] = "img_templates_".rand().".jpg";
                            $config['allowed_types'] = '*' ;

                            $dados['imagemTemplate_templates'] = $config['file_name'];

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('imagemAtualizarTemplate');

                        } else {

                            $config['upload_path'] = './assets/images/templates';
                            $config['file_name'] = "img_templates_".rand().".jpg";
                            $config['allowed_types'] = '*' ;

                            $dados['imagemTemplate_templates'] = $config['file_name'];

                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $this->upload->do_upload('imagemAtualizarTemplate');

                        }

                        $dados['statusTemplate_templates'] = $key->statusTemplate_templates;
                        
                    }

                }
                
                $resultadoAtualizarDados = $this->template->m_atualizarDadosTemplate($dados);
                
                if ($resultadoAtualizarDados)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Dados do <strong>Template: <i>'.$dados["nomeTemplate_templates"].'</i></strong> alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar os dados do Template!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_excluirTemplate()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/excluir/excluirTemplate";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id = $this->input->post('idTemplateExcluir');

                $resultado = $this->template->buscarTemplate($id);
                    
                foreach ($resultado as $key){
                    
                    unlink('assets/images/templates/'.$key->imagemTemplate_templates);
                }
                
                $result = $this->template->m_excluirTemplate($id);
                
                if($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Template <strong>('.$id.')</strong> excluído com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível excluir o template!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_atualizarTemplateSelecionadoLandingPage()
    {

        $retorno['msg'] = "";
        
        $dados['id_landingPage'] = $this->input->post('idLandingPage');
        $dados['template_landingPage'] = $this->input->post('idTemplate');
        

        $resultado = $this->template->m_atualizarTemplateSelecionadoLandingPage($dados);

        if($resultado)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = 'Modelo de template atualizado com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Desculpa, não foi possível atualizar o modelo de template!';
            echo json_encode($retorno);
        }
        
    }


    public function v_selecionarTemplate()
    {
        $this->permitiracesso->verifica_sessao();

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $resultado = $this->landingpage->buscarConfisgs(1);
        
        
        $templates = $this->template->m_listarTodosTemplatesLandingPages();
        
        $landingpages = $this->landingpage->buscarLandingPage($id);
        
        $dadosSessao['configs'] = $resultado[0];
        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $dadosSessao['templates'] = $templates;
        $dadosSessao['landingpage'] = $landingpages;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $infos['id_nivelAcesso'] = $nivelAcesso_usuario;
        $infos['view'] = "v_selecionarTemplates";

        $acessoPermitido = $this->permitiracesso->verificaAcessoPaginaParametro($infos);
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Selecionar Template | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsTemplates.js'></script>",
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",
            "scriptsJSacima" => "",
            "scriptsCSSacima" => ""
            
        );

        $this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
        $this->load->view('painel/Template/t_menuLateral');
        $this->load->view($acessoPermitido->view, $dadosSessao);
        $this->load->view('v_selecionarTemplates', $dadosSessao);
        $this->load->view('painel/Template/t_footer');
        $this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function v_configurarTemplate()
    {
        $this->permitiracesso->verifica_sessao();

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
        
        $carregarLandingPage = $this->landingpage->buscarLandingPage($id);
        $carregarConfig = $this->landingpage->buscarConfisgs($idConf = 1);
        
        $carregarConfigsModeloHead = $this->template->buscarConfigsHead($id);
        $carregarConfigsModeloLogoMenuSlide = $this->template->buscarConfigsLogoMenuSlide($id);
        $carregarConfigsModeloRedesSociais = $this->template->buscarConfigsRedesSociais($id);
        $carregarConfigsModeloServico1 = $this->template->buscarConfigsServico1($id);
        $carregarConfigsModeloServico2 = $this->template->buscarConfigsServico2($id);
        $carregarConfigsModeloServico3 = $this->template->buscarConfigsServico3($id);
        $carregarConfigsModeloServico4 = $this->template->buscarConfigsServico4($id);
        $carregarConfigsModeloServico5 = $this->template->buscarConfigsServico5($id);
        $carregarConfigsModeloServico6 = $this->template->buscarConfigsServico6($id);
        $carregarConfigsModeloServico7 = $this->template->buscarConfigsServico7($id);
        $carregarConfigsModeloServico8 = $this->template->buscarConfigsServico8($id);
        $carregarConfigsModeloServicoPaginaSucesso = $this->template->buscarConfigsPaginaSucesso($id);
        $carregarConfigsModeloMaps = $this->template->buscarConfigsMaps($id);
        $carregarConfigsModeloForms = $this->template->buscarConfigsForms($id);
        $carregarConfigsModeloPersonalizacao = $this->template->buscarConfigsPersonalizacao($id);
        
        $carregarConfigsModelo = (object)array_merge(
            (array)$carregarConfigsModeloHead, 
            (array)$carregarConfigsModeloLogoMenuSlide,
            (array)$carregarConfigsModeloRedesSociais,
            (array)$carregarConfigsModeloServico1,
            (array)$carregarConfigsModeloServico2,
            (array)$carregarConfigsModeloServico3,
            (array)$carregarConfigsModeloServico4,
            (array)$carregarConfigsModeloServico5,
            (array)$carregarConfigsModeloServico6,
            (array)$carregarConfigsModeloServico7,
            (array)$carregarConfigsModeloServico8,
            (array)$carregarConfigsModeloMaps,
            (array)$carregarConfigsModeloForms,
            (array)$carregarConfigsModeloPersonalizacao,
            (array)$carregarConfigsModeloServicoPaginaSucesso
        );
        
        foreach ($carregarLandingPage as $dadosLandingPage) {

            $dadosSessao['dados'] = $this->session->userdata;

            $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
            $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

            $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
            $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
            $dadosSessao['paginas'] = $permissaoAcesso;
            
            $scripts = array(
                "titulo" => "Configurar modelo".$carregarConfigsModelo->id_modelo,

                "scriptsJS" => "<script src='/assets/libs/ckeditor/ckeditor.js'></script>
                                <script src='/assets/painel/js/vendor/scriptsModelo1.js'></script>",

                "scriptsCSS" => "<script src='/modelos/modelo1/js/jquery-3.4.1.min.js'></script>
                <link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

                "scriptsJSacima" => "",
                "scriptsCSSacima" => "",
            );
            
            $configsModelo = array(
                "configs" => $carregarConfig[0],
                "configsModelo" => $carregarConfigsModelo,
                "url_atual" => $url[0],
                "dadosLandingPage" => $dadosLandingPage
            );
            
            $this->load->view('painel/Template/t_head', $scripts);
            $this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
            $this->load->view('painel/Template/t_menuLateral');
            $this->load->view('modelos/modelo'.$carregarLandingPage[0]->template_landingPage.'/v_configurarTemplate', $dadosSessao);
            $this->load->view('painel/Template/t_footer');
            $this->load->view('painel/Template/t_scripts', $scripts);

        }
    }

    public function c_leadsCadastroForm1()
    {

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $dados['id_cadastros'] = $this->landingpage->m_gerarCodigoLeadsCadastros();
        $dados['nome_cadastros'] = $this->input->post('nomeVisitante');
        $dados['telefone_cadastros'] = "+55".preg_replace('/[^\d]/', '',$this->input->post('telefoneVisitante'));
        $dados['email_cadastros'] = $this->input->post('emailVisitante');
        $dados['ondeNosConheceu_cadastros'] = $this->input->post('ondeNosConheceu');
        $dados['landingPage_cadastros'] = $id;
        $dados['grupoLista_cadastros'] = $this->input->post('grupoLista_landingPage');
        
        $sinal = false;
        $retorno['msg'] = "";
        
        if(empty($dados['nome_cadastros']))
		{
            $retorno['ret'] = false;
            $retorno['msg'] = ' Por favor, preencha todos os campos!!';
            $sinal = true;
            
        } 

        if(empty($dados['telefone_cadastros']))
		{
            $retorno['ret'] = false;
            $retorno['msg'] = ' Por favor, preencha todos os campos!!';
            $sinal = true;
            
        }

        if(empty($dados['email_cadastros']))
		{
            $retorno['ret'] = false;
            $retorno['msg'] = ' Por favor, preencha todos os campos!!';
            $sinal = true;
            
        }
        

        if(empty($dados['ondeNosConheceu_cadastros']))
		{
            $retorno['ret'] = false;
            $retorno['msg'] = ' Por favor, preencha todos os campos!!';
            $sinal = true;
            
        }
        
        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }

        $this->load->library('contador');

        if($_SERVER['REMOTE_ADDR'] === '192.168.10.1')
        {$ip = "177.30.111.114";}else{$ip = $_SERVER['REMOTE_ADDR'];}
                    
        $geo = json_decode($this->contador->consultaCidadeEstado("http://extreme-ip-lookup.com/json/".$ip));

        if(is_null($geo->city)){$dados['cidade_cadastros']="Goiania";}else{$dados['cidade_cadastros']=$geo->city;}
        if(is_null( $geo->region)){$dados['estado_cadastros']="Goias";}else{$dados['estado_cadastros']=$geo->region;}

        $dados['data_cadastros'] = date('Ymd');
        $dados['hora_cadastros'] = date('Hi');
        
        $resultado = $this->landingpage->m_leadsCadastroForm($dados);

        if($resultado)
        {
            
            $retorno['ret'] = true;
            echo json_encode($retorno);
            
        } else {
            

            $retorno['ret'] = false;
            $retorno['id'] = $dados['id_cadastros'];
            $retorno['msg'] = 'Desculpa, não foi possível atualizar o modelo de template!';
            echo json_encode($retorno);
        }
    
    }

    public function v_cadastroLeadsSucesso()
    {
        $this->permitiracesso->verifica_sessao();
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[2];
        
        $carregarLandingPage = $this->landingpage->buscarLandingPage($id);
        $carregarConfig = $this->landingpage->buscarConfisgs($idconf=1);
        $carregarConfigsModeloServicoPaginaSucesso = $this->template->buscarConfigsPaginaSucesso($id);
        $carregarConfigsModeloPersonalizacao = $this->template->buscarConfigsPersonalizacao($id);

        $carregarConfigsModelo = (object)array_merge(
            (array)$carregarLandingPage[0], 
            (array)$carregarConfig[0], 
            (array)$carregarConfigsModeloServicoPaginaSucesso,
            (array)$carregarConfigsModeloPersonalizacao

        );
        
        $configsModelo = array(

            "titulo" => "Cadastro realizado com sucesso!",
            "configs" => $carregarConfig[0],
            "dadosLandingPage" => $carregarConfigsModelo,
            "url_atual" => $url[0],
            "scriptCSS" => "",
            "scriptJS" => ""
            
        );
        
        $this->load->view('modelos/modelo'
                                .$carregarConfigsModelo->template_landingPage.
                                '/v_landingPageSucesso', $configsModelo);
        
    }

    public function c_clicks()
    {
    
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $dados['id_clicks'] = $this->landingpage->m_gerarCodigoClicks();
        $dados['idLandingPage_clicks'] = $id;
        $dados['token_clicks'] = $this->input->post('token');
        $dados['nomeBotao_clicks'] = $this->input->post('botao');
        $dados['linkBotao_clicks'] = $this->input->post('link');
        $dados['dataClick__clicks'] = date('Ymd');
        $dados['horaClick__clicks'] = date('Hi');
        
        $resultado = $this->landingpage->buscarLandingPageClicksToken($dados['token_clicks'], $id, $dados['nomeBotao_clicks']);
        
        if(empty($resultado))
        {
            $this->landingpage->m_clicks($dados);
        }
        
    }

    public function c_listarGrupos()
    {
    
        $resultado = $this->landingpage->m_listarGrupos();

        echo json_encode($resultado);

    }

    public function v_gruposLandingPage()
    {
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        
        $view = "v_grupos";
        
        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Criar grupos para landing pages | Solid System",

            "scriptsJS" => "<script src='/assets/libs/custombox/custombox.min.js'></script>
                            <script src='/assets/painel/js/vendor/scriptsLandingPage.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "<link href='/assets/libs/custombox/custombox.min.css' rel='stylesheet'>",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
		$this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }
    
}
