<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class Permissoes extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->model("landingpage/Landingpage_model", "landingpage");
        $this->load->model("landingpage/Templatelandingpage_model", "template");
        $this->load->model("landingpage/Modelos_model", "modelos");
        $this->load->model("usuarios/Usuarios_model", "usuarios");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->library('permitiracesso');
        $this->load->model("empresa/Empresa_model", "empresa");
        
    }
    
	public function v_niveisAcesso()
	{
        $this->permitiracesso->verifica_sessao();
        
        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_niveisAcesso";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);

        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 

        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Níveis de acesso | Solid System",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsNivelAcesso.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }
    
    public function v_paginas()
	{
        $this->permitiracesso->verifica_sessao();
        
        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;

        $this->load->library('permitiracesso');

        $view = "v_paginas";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $dadosSessao['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Páginas do sistema | Solid System",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsPaginas.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
		$this->load->view('painel/Template/t_menuLateral');
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
        $this->load->view('landingpage/modelos/modelo1/escolherIcones');
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function c_criarPagina()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $id = $this->permissoes->m_gerarCodigoCriarPagina();

        $dados['id_pagina'] = $id;

        $dados['nome_pagina']           = $this->input->post('nomePagina');
        $dados['endereco_pagina']       = $this->input->post('enderecoPagina');
        $dados['observacao_pagina']     = $this->input->post('observacaoPagina');
        $dados['icone_pagina']          = $this->input->post('iconePagina');
        $dados['controller_pagina']     = $this->input->post('controllerPagina');

        $enderecoPagina = $dados['endereco_pagina'];

        $endereco = $this->permissoes->verificarPaginaExiste($enderecoPagina);
        
        if(!empty($endereco->endereco_pagina))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'Endereço utilizado pela página: <strong><u>'.$endereco->nome_pagina.'</u></strong>, utilize outro <strong>ENDEREÇO</strong>!<br>';
            $sinal = true;
            
        }
        
        if(empty($dados['nome_pagina']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['endereco_pagina']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>ENDEREÇO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }
        
        $resultado = $this->permissoes->m_criarNovaPagina($dados);

        if($resultado)
        {
            $retorno['ret'] = true;
            $retorno['msg'] = ' Página: <strong>'. $dados['nome_pagina'].'</strong> criada com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = ' Não foi possível criar a página '. $dados['nome_pagina'].', tente novamente mais tarde!';
			echo json_encode($retorno);
        }
        
    }

    public function c_listarPaginas()
    {

        $resultado = $this->permissoes->m_listarPaginas();

        echo json_encode($resultado);
    
    }

    public function c_atualizarDadosPaginas()
    {
    
        $retorno['msg'] = "";
        $sinal = false;
        
        $dados['id_pagina']             = $this->input->post('atualizarIdPagina');
        $dados['nome_pagina']           = $this->input->post('atualizarNomePagina');
        $dados['endereco_pagina']       = $this->input->post('atualizarEnderecoPagina');
        $dados['observacao_pagina']     = $this->input->post('atualizarObservacaoPagina');
        $dados['icone_pagina']          = $this->input->post('atualizarIconePagina');
        
        if(empty($dados['nome_pagina']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if(empty($dados['endereco_pagina']))
		{
			$retorno['ret'] = false;
			$retorno['msg'].= 'O <strong>ENDEREÇO</strong> não pode ser vazio!<br>';
            $sinal = true;
            
        }

        if($sinal)
		{
            echo json_encode($retorno);
            exit;
        }

        $result = $this->permissoes->m_atualizarDadosPagina($dados);

        if ($result)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = 'Página alterada com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Desculpa, não foi possível alterar os dados da página!';
            echo json_encode($retorno);
        }

    }

    public function c_deletarDadosPagina()
    {
        $retorno['msg'] = "";
        
        $id= $this->input->post('idDeletarPagina');
        
        $result = $this->permissoes->m_excluirPagina($id);
        
        if($result)
        {
            
            $retorno['ret'] = true;
            $retorno['msg'] = 'Página excluída com sucesso!';
            echo json_encode($retorno);
            
        } else {

            $retorno['ret'] = false;
            $retorno['msg'] = 'Desculpa, não foi possível excluir a Página!';
            echo json_encode($retorno);
        }

    }

    public function c_criarNivelAcesso()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/permissoes/criarNivelAcesso";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){


                $retorno['msg'] = "";
                $sinal = false;

                $id = $this->permissoes->m_gerarCodigoCriarNivelAcesso();

                $dados['id_nivelAcesso']    = $id;
                $dados['nome_nivelAcesso']  = $this->input->post('nomeNivelAcesso');
                $dados['cor_nivelAcesso']   = $this->input->post('corNivelAcesso');
                $dados['corTexto_nivelAcesso']   = $this->input->post('corTextoNivelAcesso');
                
                $nomeNivel = $this->permissoes->verificarNivelExiste($dados['nome_nivelAcesso']);
                
                if(!empty($nomeNivel->nome_nivelAcesso))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'Nome em uso, utilize outro <strong>NOME</strong>!<br>';
                    $sinal = true;
                    
                }
                
                if(empty($dados['nome_nivelAcesso']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($dados['cor_nivelAcesso']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'OA <strong>COR</strong> não pode ser vazia!<br>';
                    $sinal = true;
                    
                }

                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                $resultado = $this->permissoes->m_criarNivelAcesso($dados);

                if($resultado)
                {
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Nível de acesso: <strong>'. $dados['nome_nivelAcesso'].'</strong> criado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = ' Não foi possível criar o Nível de acesso '. $dados['nome_nivelAcesso'].', tente novamente mais tarde!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }    
    }

    public function c_listarNiveisAcesso()
    {
        $dadosSessao['dados'] = $this->session->userdata;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        
        $resultado['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        $resultado['listar'] = $this->permissoes->m_listarNiveisAcesso();

        echo json_encode($resultado);
    }

    public function c_atualizarNivelAcesso()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/permissoes/atualizarNivelAcesso";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                $sinal = false;
                
                $dados['id_nivelAcesso']        = $this->input->post('atualizarIdNivelAcesso');
                $dados['nome_nivelAcesso']      = $this->input->post('atualizarNomeNivelAcesso');
                $dados['cor_nivelAcesso']       = $this->input->post('atualizarCorNivelAcesso');
                $dados['corTexto_nivelAcesso']  = $this->input->post('atualizarCorTextoNivelAcesso');
                
                if(empty($dados['nome_nivelAcesso']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'O <strong>NOME</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($dados['cor_nivelAcesso']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'A <strong>COR DE FUNDO</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if(empty($dados['corTexto_nivelAcesso']))
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= 'A <strong>COR DO TEXTO</strong> não pode ser vazio!<br>';
                    $sinal = true;
                    
                }

                if($sinal)
                {
                    echo json_encode($retorno);
                    exit;
                }

                $result = $this->permissoes->m_atualizarNivelAcesso($dados);

                if ($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Nível de acesso alterado com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível alterar os dados do Nível de acesso!';
                    echo json_encode($retorno);
                }
    
            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }

    public function c_deletarNivelAcesso()
    {
    
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/permissoes/deletarNivelAcesso";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $id= $this->input->post('idNivelAcessoDeletar');
                
                $result = $this->permissoes->m_deletarNivelAcesso($id);
                
                if($result)
                {
                    
                    $retorno['ret'] = true;
                    $retorno['msg'] = 'Nível de acesso excluído com sucesso!';
                    echo json_encode($retorno);
                    
                } else {

                    $retorno['ret'] = false;
                    $retorno['msg'] = 'Desculpa, não foi possível excluir o Nível de acesso!';
                    echo json_encode($retorno);
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }

    }

    public function v_permissaoNiveisAcesso()
    {
        $this->permitiracesso->verifica_sessao();
        
        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $infos['id_nivelAcesso'] = $nivelAcesso_usuario;
        $infos['view'] = "v_permissaoNiveisAcesso";

        $acessoPermitido = $this->permitiracesso->verificaAcessoPaginaParametro($infos);
        $dadosSessao['permissaoLinks'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario);
        
        $scripts = array(

            "titulo" => "Permissão por nível | Solid System",

            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsPermissoesNiveisAcesso.js'></script>",
                            
            "scriptsCSS" => "<link href='/assets/css/cssPersonalizado.css' rel='stylesheet' type='text/css'/>",

            "scriptsCSSacima" => "",

            "scriptsJSacima" => ""
        );
        
		$this->load->view('painel/Template/t_head', $scripts);
		$this->load->view('painel/Template/t_barraTopo',  $dadosSessao);
        $this->load->view('painel/Template/t_menuLateral');
        $this->load->view($acessoPermitido->view, $dadosSessao);
        $this->load->view('landingpage/modelos/modelo1/escolherIcones');
		$this->load->view('painel/Template/t_footer');
		$this->load->view('painel/Template/t_scripts', $scripts);
    }

    public function v_listaNiveisPaginasDisponiveis()
    {
        $this->permitiracesso->verifica_sessao();

        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        
        $resultado['links'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        $resultado['listar'] = $this->permissoes->m_listaNiveisPaginasDisponiveis($id);

        echo json_encode($resultado);
    }
    
    public function v_listaNiveisPaginasADD()
    {
        $this->permitiracesso->verifica_sessao();
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $id = $url[4];
        
        $dadosSessao['dados'] = $this->session->userdata;

        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        
        $resultado['linksAdd'] = $this->permissoes->buscarPermissaoAcessoPagina($nivelAcesso_usuario); 
        $resultado['listarAdd'] = $this->permissoes->m_listaNiveisPaginasADD($id);

        echo json_encode($resultado);
    }

    public function c_associarRemoverPagina()
    {
        
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/permissoes/associarRemoverPagina";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){
			
                $retorno['msg'] = "";
                
                $dados['id_pagina'] = $this->input->post('id_pagina');
                $dados['id_niveisPaginas'] = $this->input->post('id_niveisPaginas');
                
                if($this->input->post('add_niveisPaginas') === '1')
                {
                    $dados['add_niveisPaginas'] = '0';
                    $dados['permissao_niveisPaginas'] = '0';
                    $dados['menu_niveisPaginas'] = '0';
                    
                    $result = $this->permissoes->m_associarRemoverPagina($dados);
                
                    if($result)
                    {
                        
                        $retorno['ret'] = true;
                        $retorno['msg'] = '<strong>Página desassociada</strong> do nível com sucesso';
                        echo json_encode($retorno);
                        
                    } else {

                        $retorno['ret'] = false;
                        $retorno['msg'] = 'Desculpa, não foi possível <strong>desassociar a página</strong> do nível!';
                        echo json_encode($retorno);
                    }

                } else {

                    $dados['add_niveisPaginas'] = '1';
                    $dados['permissao_niveisPaginas'] = '0';
                    $dados['menu_niveisPaginas'] = '0';

                    $result = $this->permissoes->m_associarRemoverPagina($dados);
                
                    if($result)
                    {
                        
                        $retorno['ret'] = true;
                        $retorno['msg'] = '<strong>Página associada</strong> ao nível de acesso com sucesso!';
                        echo json_encode($retorno);
                        
                    } else {

                        $retorno['ret'] = false;
                        $retorno['msg'] = 'Desculpa, não foi possível <strong>associar a página</strong> ao nível de acesso!';
                        echo json_encode($retorno);
                    }
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    
    }
    
    public function c_liberarBloquearPermissao()
    {
        
        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/permissoes/liberarBloquearPermissao";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){
                $retorno['msg'] = "";
                
                $dados['id_pagina'] = $this->input->post('id_pagina');
                $dados['id_niveisPaginas'] = $this->input->post('id_niveisPaginas');
                
                if($this->input->post('permissao_niveisPaginas') === '1')
                {
                    $dados['permissao_niveisPaginas'] = '0';
                    
                    $result = $this->permissoes->m_liberarBloquearPermissao($dados);
                
                    if($result)
                    {
                        
                        $retorno['ret'] = true;
                        $retorno['msg'] = '<strong>Permissão</strong> de acesso <strong>bloqueada</strong> com sucesso!';
                        echo json_encode($retorno);
                        
                    } else {

                        $retorno['ret'] = false;
                        $retorno['msg'] = 'Desculpa, não foi possível bloquear a <strong>permissão de acesso</strong>!';
                        echo json_encode($retorno);
                    }

                } else {

                    $dados['permissao_niveisPaginas'] = '1';

                    $result = $this->permissoes->m_liberarBloquearPermissao($dados);
                
                    if($result)
                    {
                        
                        $retorno['ret'] = true;
                        $retorno['msg'] = '<strong>Permissão</strong> de acesso <strong>liberada</strong> com sucesso!';
                        echo json_encode($retorno);
                        
                    } else {

                        $retorno['ret'] = false;
                        $retorno['msg'] = 'Desculpa, não foi possível liberar acesso ao <strong>Menu</strong>!';
                        echo json_encode($retorno);
                    }
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    
    }

    public function c_liberarBloquearMenu()
    {

        $retorno['msg'] = "";
        $sinal = false;

        $dadosSessao['dados'] = $this->session->userdata;
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];
        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        $this->load->library('permitiracesso');
        $metodo = "/dashboard/permissoes/liberarBloquearMenu";
        $permitirAcessoPagina['permitirMetodo'] = $this->permitiracesso->verificaAcessoBotao($nivelAcesso_usuario, $metodo);
        
        if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '0' && $permitirAcessoPagina['permitirMetodo']->add_niveisPaginas === '0')
		{
			$retorno['ret'] = false;
			$retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
            $sinal = true;
            
        } else {

            if($permitirAcessoPagina['permitirMetodo']->permissao_niveisPaginas === '1'){

                $retorno['msg'] = "";
                
                $dados['id_pagina'] = $this->input->post('id_pagina');
                $dados['id_niveisPaginas'] = $this->input->post('id_niveisPaginas');

                if($this->input->post('menu_niveisPaginas') === '1')
                {
                    $dados['menu_niveisPaginas'] = '0';

                    $result = $this->permissoes->m_liberarBloquearMenu($dados);
                
                    if($result)
                    {
                        
                        $retorno['ret'] = true;
                        $retorno['msg'] = 'O acesso ao <strong>Menu</strong> foi removido!';
                        echo json_encode($retorno);
                        
                    } else {

                        $retorno['ret'] = false;
                        $retorno['msg'] = 'Desculpa, não foi possível remover acesso ao <strong>Menu</strong>!';
                        echo json_encode($retorno);
                    }

                } else {

                    $dados['menu_niveisPaginas'] = '1';

                    $result = $this->permissoes->m_liberarBloquearMenu($dados);
                
                    if($result)
                    {
                        
                        $retorno['ret'] = true;
                        $retorno['msg'] = 'O acesso ao <strong>Menu</strong> foi liberado!';
                        echo json_encode($retorno);
                        
                    } else {

                        $retorno['ret'] = false;
                        $retorno['msg'] = 'Desculpa, não foi possível liberar acesso ao <strong>Menu</strong>!';
                        echo json_encode($retorno);
                    }
                }

            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= '<span class="text-danger"><strong>VOCÊ NÃO TEM PERMISSÃO!</strong></span><br>';
                $sinal = true;

            }
        }

        if($sinal)
        {
            echo json_encode($retorno);
            exit;
        }
    }
    
}