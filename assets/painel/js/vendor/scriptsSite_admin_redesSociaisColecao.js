primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarRedesSociais();
listarColecao();

function listarRedesSociais(){
    
    $.ajax(
      {
          
        url: "/dashboard/listagens/listarRedesSociais",
        
        ajax: 'dados.json',
        
        success: function(dados){
            
          var dados = JSON.parse(dados);
          
            $('#imagemRedeSocial1').append('<img src="/assets/site/social/'+dados.imagem_redeSocial1+'" class="img-fluid " />');
            $('#frase_redeSocial1').val(dados.frase_redeSocial1);
            $('#link_redeSocial1').val(dados.link_redeSocial1);
            if(dados.ativo_redeSocial1 === '1'){$('#ativo_redeSocial1').val(1).attr("checked", "checked");} else {$('#ativo_redeSocial1').val(1);}

            $('#imagemRedeSocial2').append('<img src="/assets/site/social/'+dados.imagem_redeSocial2+'" class="img-fluid" />');
            $('#frase_redeSocial2').val(dados.frase_redeSocial2);
            $('#link_redeSocial2').val(dados.link_redeSocial2);
            if(dados.ativo_redeSocial2 === '1'){$('#ativo_redeSocial2').val(1).attr("checked", "checked");} else {$('#ativo_redeSocial2').val(1);}

            $('#imagemRedeSocial3').append('<img src="/assets/site/social/'+dados.imagem_redeSocial3+'" class="img-fluid" />');
            $('#frase_redeSocial3').val(dados.frase_redeSocial3);
            $('#link_redeSocial3').val(dados.link_redeSocial3);
            if(dados.ativo_redeSocial3 === '1'){$('#ativo_redeSocial3').val(1).attr("checked", "checked");} else {$('#ativo_redeSocial3').val(1);}

            $('#imagemRedeSocial4').append('<img src="/assets/site/social/'+dados.imagem_redeSocial4+'" class="img-fluid" />');
            $('#frase_redeSocial4').val(dados.frase_redeSocial4);
            $('#link_redeSocial4').val(dados.link_redeSocial4);
            if(dados.ativo_redeSocial4 === '1'){$('#ativo_redeSocial4').val(1).attr("checked", "checked");} else {$('#ativo_redeSocial4').val(1);}
            
            $('#seo_redeSocial1').val(dados.seo_redeSocial1);
            $('#seo_redeSocial2').val(dados.seo_redeSocial2);
            $('#seo_redeSocial3').val(dados.seo_redeSocial3);
            $('#seo_redeSocial4').val(dados.seo_redeSocial4);
          
        }
      }
    );
  }

$('#formAtualizarBotoesRedesSociais').submit(function(e){
e.preventDefault();
var self = $(this);
// alert(self.serialize());
var retorno = atualizarBotoesRedesSociais(self);
});

function atualizarBotoesRedesSociais(dados){

  var form = $('#formAtualizarBotoesRedesSociais')[0];

  var data = new FormData(form);
    
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarBotoesRedesSociais",
      data: data,
      dataType: 'json',
      processData: false,
      contentType: false,
      cache: false,
      timeout: 400000,
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarRedesSociais();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listarRedesSociais();
          
        }
      }
    }
  );
}

function listarColecao(){
    
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarColecao",
      
      ajax: 'dados.json',
      
      success: function(dados){
          
        var dados = JSON.parse(dados);
        
          $('#imagemColecao').append('<img src="/assets/site/colecao/'+dados.imagem_colecao+'" class="img-fluid " />');
          $('#link_colecao').val(dados.link_colecao);
          $('#seo_colecao').val(dados.seo_colecao);
          $('#texto_colecao').val(dados.texto_colecao);
          if(dados.ativo_colecao === '1'){$('#ativo_colecao').val(1).attr("checked", "checked");} else {$('#ativo_colecao').val(1);}
          
      
        
      }
    }
  );
}

$('#formAtualizarColecao').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarColecao(self);
  });
  
  function atualizarColecao(dados){
  
    var form = $('#formAtualizarColecao')[0];
  
    var data = new FormData(form);
      
    $.ajax(
      {
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarColecao",
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 400000,
        
        success: function(retorno){
            
          if(retorno.ret == false) {
  
            $('#erroMsg').html(
              
              '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                  '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );
            
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);
  
            listarRedesSociais();
            
          } else {
            
            $('#erroMsg').html(
                
              '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                '<h5 class="text-white">Tudo certo!</h5>' +
                  retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );
            
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);
  
            listarRedesSociais();
            
          }
        }
      }
    );
  }
  