<div class="content-page">

    <div class="content">

        <div class="container-fluid mt-2">

            <div class="row">
                <div class="col-12">

                    <?php
                        $this->load->view('botoesAll');
                    ?>
                    
                    <form id="formAtualizarTemplateCampanha">
                    
                        <div class="page-title-box">

                            <div class="row mt-3">
                                <div class="col-12">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-xl-12 text-center">
                                                
                                                <h4 class="header-title">Atualizar template atual</h4>
                                                
                                                <p class="sub-header">
                                                Selecione a campanha e o template e clique em <strong class="text-uppercase text-warning"><i>atualizar</i></strong> para <strong class="text-uppercase text-warning"><i>atualizar o template</i></strong> da campanha selecionada.
                                                </p>

                                                <div class="button-list">
                                                    <button type="submit" class="btn btn-block btn-dark waves-effect waves-light">
                                                    <i class="fe-refresh-ccw"></i> Atualizar dados
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="msgErro"></div>
                        
                            <div class="row">
                                
                                <div class="col-md-6">
                                
                                    <div class="card">

                                        <h5 class="card-header">Selecione a campanha</h5>

                                        <div class="card-body">

                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover mb-0">

                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nome</th>
                                                            <th>Template atual</th>
                                                            <th>Ação</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody id="tabelaListarCampanhas_x_templateEmail"></tbody>

                                                </table>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="card">

                                        <h5 class="card-header">Selecione o template do e-mail</h5>

                                        <div class="card-body">

                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover mb-0">

                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nome</th>
                                                            <th>Preview</th>
                                                            <th>Ação</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody id="tabelaListarModelosTemplate"></tbody>

                                                </table>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                
                            </div>
                            
                        
                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

<div id="modalVerTemplateCampanha" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Você está usando o template: <strong class="text-warning" id="nomeModeloTemplateVerCampanha"><i></i></strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div id="mensagemERROExcluirEmailsLista" class="col-md-12 mt-2"></div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">

                             <div id="ModeloTemplate"></div>

                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-secondary waves-effect waves-light">fechar</button>
                </div>
                

        </div>
    </div>
</div>

<div id="modalVerTemplateEmail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Você está VISUALIZANDO o template: <strong class="text-warning" id="nomeModeloTemplateEmail"><i></i></strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div id="mensagemERROExcluirEmailsLista" class="col-md-12 mt-2"></div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">

                             <div id="ModeloTemplateEmail"></div>

                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-secondary waves-effect waves-light">fechar</button>
                </div>
                

        </div>
    </div>
</div>