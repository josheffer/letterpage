primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarVideos();

function listarVideos(){
    
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarVideos",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);

        dadosGlobais = dados;
        
        $('#tabelaFotos').html('');
        
        if(dados.length>0){
            
          for(var i = 0; i < dados.length; i++) {

            if(dados[i].ativo_videos === '1')
            {
              
              var botaoStatus = '<button type="submit" onclick="javascript:atualizarStatusVideos(' + i + ');" class="btn btn-xs btn-block btn-purple waves-effect waves-light">Bloquear</button>';
            
            } else {

              var botaoStatus = '<button type="submit" onclick="javascript:atualizarStatusVideos(' + i + ');" class="btn btn-xs btn-block btn-danger waves-effect waves-light">Desbloquear</button>';

            }
            
            
            $('#tabelaFotos').append(

              '<tr>' +
                '<th>'+dados[i].id_videos+'</th>' +
                '<td>'+dados[i].nome_videos+'</td>' +
                '<td>'+botaoStatus+'</td>' +
                '<td>' +
                    '<button type="button" onclick="javascript:editarVideo(' + i + ');" class="btn btn-xs btn-primary waves-effect waves-light mr-2"><i class="mdi mdi-pencil-outline"></i></button>' +
                    '<button type="button" onclick="javascript:excluirVideo(' + i + ');" class="btn btn-xs bg-soft-red waves-effect waves-light mr-1"><i class="mdi mdi-trash-can"></i></i></button>' +
                '</td>' +
              '</tr>' 
                  
            );
          }

        } else {
            
            $('#tabelaFotos').append(
              '<td colspan="4">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhum vídeo cadastrado!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );

        }
      }
    }
  );
}

function atualizarStatusVideos(dados){
  
  $.ajax(
    {
      type: "POST",
      
      data: { 
        idAtualizar: dadosGlobais[dados].id_videos
      },

      url: "/dashboard/atualizar/atualizarStatusVideos",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
          }, 1000, 'linear');

          listarVideos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

         $('html,body').animate({
            scrollTop: 0
          }, 1000, 'linear');

          listarVideos();
          
        }
      }
    }
  );
}


function editarVideo(attVideo)
{
  
  $('#formEditarVideo').each (function(){
    this.reset();
  });

  showModalEditar(attVideo);
}

function showModalEditar(attVideo){

  $('#modalEditarVideo').modal('show');
  $("#idEditarVideo").val(dadosGlobais[attVideo].id_videos);
  $("#nomeVideoEditar").val(dadosGlobais[attVideo].nome_videos);
  $("#tituloNomeVideo").html(dadosGlobais[attVideo].nome_videos);

  $("#linkVideoEditar").val('https://www.youtube.com/watch?v='+dadosGlobais[attVideo].link_videos);
  
}

$('#formEditarVideo').submit(function(e){
  e.preventDefault();
  var self = $(this);
  
  // alert(self.serialize());
  var retorno = atualizarVideo(self);
});

function atualizarVideo(dados){
  
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarVideo",
      dataType: 'json',
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsgEditarVideo').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
            }, 1000, 'linear');

          listarVideos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
            }, 1000, 'linear');

            $('#formEditarFoto').each (function(){
              this.reset();
            });

          $('#modalEditarVideo').modal('hide');

          listarVideos();
          
        }
      }
    }
  );
}

function excluirVideo(excluirVideo)
{
  showModalExcluir(excluirVideo);
}

function showModalExcluir(excluirVideo){

  $('#modalExcluir').modal('show');
  $("#idExcluir").html(dadosGlobais[excluirVideo].id_videos);
  $("#idVideo").val(dadosGlobais[excluirVideo].id_videos);
}

$('#formExcluirVideo').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = deletarVideo(self);
});

function deletarVideo(dados){
  
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/site/excluir/deletarVideo",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroExcluir').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
            }, 1000, 'linear');

          listarVideos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#modalExcluir').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
            }, 1000, 'linear');

          listarVideos();
          
        }
      }
    }
  );
}

$('#formCadVideo').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = cadastrarNovoVideo(self);
});

function cadastrarNovoVideo(dados){
  
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/cadastrar/cadastrarNovoVideo",
      dataType: 'json',
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
          }, 1000, 'linear');


          listarVideos();
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').animate({
            scrollTop: 0
          }, 1000, 'linear');

          $('#formCadVideo').each (function(){
            this.reset();
          });
            

          listarVideos();
          
        }
      }
    }
  );
}