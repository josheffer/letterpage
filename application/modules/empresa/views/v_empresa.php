<div class="content-page">

    <div class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">

                    <div id="msgErro" class="mt-2"></div>

                    <div class="card mt-3">

                        <div class="card-body">
                            
                            <div class="row">

                                <div class="col-lg-12 col-xl-12">

                                    <div class="card-box">

                                        <form id="formEmpresa">
                                            
                                            <div class="row">

                                                <div class="col-12 mb-2" id="avisoAlteracoes"></div>

                                                <div class="col-12">
                                                    <h5 class="card-title mb-3">Atualizar dados da sua empresa</h5>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="firstname">Razão social</label>
                                                        <input type="text" class="form-control" id="razaoSocialEmpresa" name="razaoSocialEmpresa" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="firstname">Nome Fantasia</label>
                                                        <input type="text" class="form-control" id="nomeFantasiaEmpresa" name="nomeFantasiaEmpresa" autocomplete="off">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lastname">CNPJ</label>
                                                        <input type="text" class="form-control" id="cnpjEmpresa" name="cnpjEmpresa" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="lastname">E-mail</label>
                                                        <input type="email" class="form-control" id="emailEmpresa" name="emailEmpresa" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="firstname">Telefone</label>
                                                        <input type="text" class="form-control" id="telefoneEmpresa" name="telefoneEmpresa" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="firstname">logotipo <small class="text-warning">Tamanho máx. 1080px</small></label>
                                                        <input type="file" class="form-control" id="arquivoLogotipoEmpresa" name="arquivoLogotipoEmpresa" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="firstname">Preview do logotipo</label>
                                                        <button data-toggle="modal" data-target="#verLogotipo" id="botaoVerLogotipo" type="button" class="btn btn-block btn-primary">ver imagem</button>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <hr>
                                                </div>

                                                <div class="col-12 mt-2">
                                                    <h5 class="card-title mb-3">Dados do responsável</h5>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Nome</label>
                                                        <input type="text" class="form-control" id="nomeResponsavelEmpresa" name="nomeResponsavelEmpresa" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">Telefone</label>
                                                        <input type="text" class="form-control" id="telefoneResponsavelEmpresa" name="telefoneResponsavelEmpresa" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">E-mail</label>
                                                        <input type="text" class="form-control" id="emailResponsavelEmpresa" name="emailResponsavelEmpresa" autocomplete="off">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstname">C.P.F</label>
                                                        <input type="text" class="form-control" id="cpfResponsavelEmpresa" name="cpfResponsavelEmpresa" autocomplete="off">
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            <div class="text-center">
                                                <button type="submit" id="botaoAtualizarDadosEmpresa" class="btn btn-block btn-success waves-effect waves-light mt-2">
                                                    <i class="mdi mdi-content-save"></i> Atualizar dados
                                                </button>
                                            </div>

                                        </form>

                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<div id="verLogotipo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">imagem atual do logotipo da empresa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                
                <img id="verLogotipoEmpresa" class="img-fluid">
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light waves-effect" data-dismiss="modal">fechar</button>
            </div>
        </div>
    </div>
</div>