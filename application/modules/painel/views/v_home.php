<style>
    .card {
        background-color: #2e333c !important;
    }
</style>

<div class="content-page">

    <div class="content">
        
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        
                    <center class="mb-5">
                        <h3 class="mt-4">Olá <?=$dados['nome']?>!</h3>
                        <p class="text-muted">Seja bem vindo ao painel.</p>
                    </center>

                    <!-- <div class="card">

                        <div class="card-body">
                            
                            <h5 class="card-title mb-0">Informações rápidas</h5>

                            <div class="row mt-3">

                                <div class="col-md-6 col-xl-3">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="avatar-sm bg-soft-purple rounded">
                                                    <i class="la la-bar-chart-o avatar-title font-22 text-purple"></i>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="text-right">
                                                    <h3 class="text-dark my-1">
                                                        <span data-plugin="counterup" id="acessosDoDia" name="acessosDoDia"></span>
                                                    </h3>
                                                    <p class="text-muted mb-1 ">Visitas Hoje</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-3">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="avatar-sm bg-soft-success rounded">
                                                    <i class="mdi mdi-chart-bar avatar-title font-22 text-success"></i>
                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <div class="text-right">
                                                    <h3 class="text-dark my-1">
                                                        <span data-plugin="counterup" id="totalDeVisitas" name="totalDeVisitas"></span>
                                                    </h3>
                                                    <p class="text-muted mb-1 ">Visitas <code>(Total)</code></p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-3">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-3">
                                                <div class="avatar-sm bg-soft-primary rounded" id="dispositivoMaisUsado">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-9">
                                                <div class="text-right">
                                                    <h3 class="text-dark my-1"></h3>
                                                    <p class="text-muted mb-0">Maior quantidade de acessos feito por <span class="text-warning" id="nomeDispositivo"></span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-3">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-3">
                                                <div class="avatar-sm bg-soft-info rounded">
                                                    <i class="mdi mdi-file-eye avatar-title font-22 text-info"></i>
                                                </div>
                                            </div>
                                            <div class="col-9">
                                                <div class="text-right">
                                                    <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalPaginasAtivas"></span></h3>
                                                    <p class="text-muted mb-1">Pages ativas</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div> -->

                    </div>
                </div>
            </div>     
            
        </div>

    </div> 
    
</div>
            
        
<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>