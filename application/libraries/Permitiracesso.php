<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permitiracesso {

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model("permissoes/Permissoes_model", "permissoes");
        $this->CI->load->model("painel/Painel_model", "painel");
    }

    public function verifica_sessao()
    {
        if (empty($this->CI->session->userdata('logado')))
        {   
            redirect('dashboard/logout');

        } else {

            $id = $_SESSION['id'];
            
            $resultado = $this->CI->painel->m_consultaDadosSessao($id);
            
            if($_SESSION['token'] === $resultado['token_usuario'])
            {
                $dados = array(
                    'id'                    => $resultado['id_usuario'],
                    'login'                 => $resultado['login_usuario'],
                    'senha'                 => $resultado['senha_usuario'],
                    'nome'                  => $resultado['nome_usuario'],
                    'nivelAcesso_usuario'   => $resultado['nivelAcesso_usuario'],
                    'status'                => $resultado['status_usuario'],
                    'logado'                => TRUE,
                    
                );
                
                $this->CI->session->set_userdata($dados);
                
                
            } else {
                
                redirect('dashboard/logout');
            }
        }
    
    }
    
    public function verificaAcessoPagina($nivelAcesso_usuario, $view)
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $url2 = explode($url[0], $url_atual);
        
        $urlVerificada = $url2[1];
        
        if($urlVerificada === "/dashboard/") : $urlVerificada = "/dashboard"; endif; 
        if($urlVerificada === "/") : $urlVerificada = "/dashboard"; endif;
        if($urlVerificada === "") : $urlVerificada = "/dashboard"; endif;
        
        
        $infos['id_nivelAcesso'] = $nivelAcesso_usuario;
        $infos['endereco_pagina'] = $urlVerificada;
        
        $acessoPermitido = $this->CI->permissoes->buscarPrmissaoAcesarPagina($infos);
        
        if($acessoPermitido->permissao_niveisPaginas === '1' && $acessoPermitido->add_niveisPaginas === '1')
        {
            $acessoPermitido->view = $view;

        } else {

            $acessoPermitido->view = "errors/html/semPermissao";
        }
        
        return $acessoPermitido;

    }

    public function verificaAcessoPaginaParametro($infos)
    {

        $url_atual_v = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url_v = explode('/', $url_atual_v);
        $urlVerificada = "/".$url_v[1]."/".$url_v[2]."/".$url_v[3]."/";
        
        $infos['endereco_pagina'] = $urlVerificada;
        
        $acessoPermitido = $this->CI->permissoes->buscarPrmissaoAcesarPagina($infos);
        
        if($acessoPermitido->permissao_niveisPaginas === '1' && $acessoPermitido->add_niveisPaginas === '1')
        {
            $acessoPermitido->view = $infos['view'];

        } else {

            $acessoPermitido->view = "errors/html/semPermissao";
        }
        
        return $acessoPermitido;
        
    }

    public function verificaAcessoBotao($nivelAcesso_usuario, $metodo)
    {
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('/', $url_atual);
        $url2 = explode($url[0], $url_atual);
        
        $urlVerificada = $url2[1];
        
        $infos['id_nivelAcesso'] = $nivelAcesso_usuario;
        $infos['endereco_pagina'] = $metodo;
        
        $acessoPermitido = $this->CI->permissoes->buscarPrmissaoAcesarPagina($infos);
        
        if($acessoPermitido->permissao_niveisPaginas === '1' && $acessoPermitido->add_niveisPaginas === '1')
        {
            $acessoPermitido->view = $metodo;

        } else {

            $acessoPermitido->view = "errors/html/semPermissao";
        }
        
        return $acessoPermitido;

    }
    
}