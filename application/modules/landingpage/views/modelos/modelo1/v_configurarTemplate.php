<style type="text/css">

.servico1_imagem {
    height: 250px !important;
}

.servico3_imagem {
    height: 150px !important;
}

.servico5_imagem {
    height: 200px !important;
}

.servico6_imagem {
    width: 100% !important;
}

.servico7_imagem {
    width: 100% !important;
}

.cor_cardBox_bg
{
    background-color: #31363e;
}

</style>

<div class="content-page">

    <div class="content">
                    
        <div class="container-fluid">
                        
            <div class="row mt-2">
                            
                <div class="col-12">

                    <div class="row no-gutters mb-3">
                                        
                        <div class="col-md-6 col-xl-4">
                            <a href="/dashboard/landingPages">
                                <div class="bg-soft-primary rounded-0 card-box mb-0">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="text-center">
                                                <h3 class="text-dark"><span>Landing Pages</span></h3>
                                                <p class="text-warning mb-1 text-truncate">Todas as landing pages</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-md-6 col-xl-4">
                            <a href="/dashboard/configuracoes">
                                <div class="bg-soft-danger rounded-0 card-box mb-0">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="text-center">
                                                <h3 class="text-dark"><span>Configurações</span></h3>
                                                <p class="text-warning mb-1 text-truncate">Logo, favicon, telefone, mapa..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-md-6 col-xl-4">
                            <a href="/dashboard/templates">
                                <div class="bg-soft-info rounded-0 card-box mb-0">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="text-center">
                                                <h3 class="text-dark"><span>Template</span></h3>
                                                <p class="text-warning mb-1 text-truncate">Configurações de templates</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                    </div>
                         
                    <div class="row">

                        <div class="col-lg-12">

                            <div id="erroMsgConfigsModelo1"></div>
                            
                            <div class="card">

                                <div class="card-body">
                                    
                                    <h5 class="card-title mb-3">Configure o template como desejar.</h5>
                                    
                                        <div class="row">

                                            <div class="col-xl-12">
                                                
                                                <div class="card-body">
                                                    
                                                    <div class="row">

                                                        <div class="col-xl-12">

                                                            <div class="card-box">

                                                                <ul class="nav nav-tabs nav-bordered nav-justified">
                                                                    
                                                                    <li class="nav-item">
                                                                        <a href="#headSEO" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                                            Head e SEO
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a href="#logoMenueSlide" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                            Logo, Menu, Slide
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a href="#formularios" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                            Formulários
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a href="#redesSociais" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                            Redes Sociais
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a href="#servicos" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                            Serviços
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a href="#paginaSucesso" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                            Página de Sucesso
                                                                        </a>
                                                                    </li>
                                                                    
                                                                </ul>

                                                                <div class="tab-content">
                                                                    
                                                                    <div class="tab-pane active" id="headSEO">
                                                                        
                                                                        <form id="formHeadSEO" enctype="multipart/form-data">
                                                                            
                                                                            <input type="hidden" id="idModelo_headSEO" name="idModelo_headSEO">
                                                                            <input type="hidden" id="idLandingPage_headSEO" name="idLandingPage_headSEO">

                                                                            <div class="table-responsive">

                                                                                <table class="table table-bordered table-hover mb-0">
                                                                                    
                                                                                    <thead>

                                                                                        <tr>
                                                                                            <th class="text-center">Opção</th>
                                                                                        </tr>

                                                                                    </thead>

                                                                                    <tbody>

                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>CSS <small>(Campo exclusivo para css de outras aplicações)</small> </label>
                                                                                                <textarea id="cssHeadTOPO" name="cssHeadTOPO" class="form-control" rows="3"></textarea>
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td>
                                                                                                <label data-toggle="tooltip" data-placement="top" title="Cole o código SEM a tag <script></script>" data-original-title="Cole o código SEM a tag <script></script>">JS <small>(Campo exclusivo para JS de outras aplicações, pixel, google..)</small> </label>
                                                                                                <textarea id="jsHead" name="jsHead"  class="form-control" rows="3"></textarea>
                                                                                            </td>
                                                                                        </tr>
                                                                                        
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>SEO KeyWords</label>
                                                                                                <textarea id="SEOKeywordsHead" name="SEOKeywordsHead"  class="form-control" rows="3"></textarea>
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>SEO Description 1</label>
                                                                                                <textarea id="SEODescriptionsHead" name="SEODescriptionsHead" class="form-control" rows="3"></textarea>
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>SEO Description 2</label>
                                                                                                <textarea id="SEODescriptionsHead2" name="SEODescriptionsHead2" class="form-control" rows="3"></textarea>
                                                                                            </td>
                                                                                        </tr>
                                                                                        
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>SEO Description 3</label>
                                                                                                <textarea id="SEODescriptionsHead3" name="SEODescriptionsHead3" class="form-control" rows="3"></textarea>
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>Coordenadas Google Maps <small>ex: (-16.734166,-49.302049)</small></label>
                                                                                                <input name="SEOCoordGMaps" id="SEOCoordGMaps" type="text" class="form-control" autocomplete="off">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>Cidade <small>Cidade e estado, ex: Goiânia, Goiás</small></label>
                                                                                                <input name="SEOCidade" id="SEOCidade" type="text" class="form-control" autocomplete="off">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>Estado <small>Estado e País, ex: GO-BR</small></label>
                                                                                                <input name="SEOEstado" id="SEOEstado" type="text" class="form-control" autocomplete="off">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>
                                                                                            <td>
                                                                                                
                                                                                                <div class="form-row">
                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">SEO imagem </label>
                                                                                                        <input type="file" id="SEOImagem" name="SEOImagem" class="form-control">
                                                                                                    </div>
                                                                                                    
                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">ver imagem </label>
                                                                                                        <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemSEO">Ver</button>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </td>
                                                                                        </tr>
                                                                                        
                                                                                    </tbody>

                                                                                </table>
                                                                            </div>

                                                                            <center class="mt-3">
                                                                                <button type="submit" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                    <span class="btn-label"><i class="fe-save"></i></span>Salvar
                                                                                </button>
                                                                            </center>

                                                                        </form>
                                                                    </div>

                                                                    <div class="tab-pane" id="logoMenueSlide">

                                                                        <form id="formLogoMenuSlide" enctype="multipart/form-data">

                                                                            <input type="hidden" name="idModeloLogoMenuSlide" id="idModeloLogoMenuSlide">
                                                                            <input type="hidden" id="idLandingPage_LogoMenuSlide" name="idLandingPage_LogoMenuSlide">

                                                                            <div class="row">
                                                                                
                                                                                <div class="col-xl-12">
                                                                                    <div class="card">
                                                                                        <div class="card-body" style="background-color: #353b44;">
                                                                                            
                                                                                            <div class="card-widgets">
                                                                                                <a data-toggle="collapse" href="#cardCollpase4" role="button" aria-expanded="false" aria-controls="cardCollpase4"><i class="mdi mdi-minus"></i></a>
                                                                                            </div>

                                                                                            <h4 class="header-title mb-0">Logotipo <small class="text-warning">(Menu superior)</small></h4>

                                                                                            <div id="cardCollpase4" class="collapse pt-3">
                                                                                                <div class="row">

                                                                                                    <table class="table table-bordered mb-0">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>Opção</th>
                                                                                                                <th>AÇÃO</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        
                                                                                                        <tbody>
                                                                                                                                                    
                                                                                                            <tr>
                                                                                                                <td class="col-12">
                                                                                                                    Logo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Se desativado: Não mostra a logo e nem o menu" data-original-title="Se desativado: Não mostra a logo e nem o menu"></i>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="logotipoMenu" name="logotipoMenu">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td colspan="2" class="col-12">
                                                                                                                    <div class="form-row">
                                                                                                                        <div class="form-group col-md-12">
                                                                                                                            <label class="col-form-label">Cor de fundo do Menu <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cord de fundo do menu" data-original-title="Cord de fundo do menu"></i></label>
                                                                                                                            <input type="color" class="form-control" id="corFundoMenu" name="corFundoMenu">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-xl-12">
                                                                                    <div class="card">
                                                                                        <div class="card-body" style="background-color: #353b44;">
                                                                                            
                                                                                            <div class="card-widgets">
                                                                                                <a data-toggle="collapse" href="#cardCollpase2" role="button" aria-expanded="false" aria-controls="cardCollpase2"><i class="mdi mdi-minus"></i></a>
                                                                                            </div>

                                                                                            <h4 class="header-title mb-0">Banner 1 <small class="text-warning">(Responsivo)</small></h4>

                                                                                            <div id="cardCollpase2" class="collapse pt-3">
                                                                                                <div class="row">

                                                                                                    <table class="table table-bordered mb-0">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>Opção</th>
                                                                                                                <th>AÇÃO</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        
                                                                                                        <tbody>
                                                                                                                                                    
                                                                                                            <tr>
                                                                                                                <td class="col-12">
                                                                                                                    Ativo? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Se desativado: Não mostra o banner!" data-original-title="Se desativado: Não mostra o banner!"></i>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="ativoBannerSlide1" name="ativoBannerSlide1">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td colspan="2" class="col-12">
                                                                                                                    <div class="form-row">
                                                                                                                        
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Imagem de fundo do banner <small class="text-warning"> (1920px x 840px)</small></label>
                                                                                                                            <input type="file" class="form-control" id="atualizarImagemSlide1" name="atualizarImagemSlide1">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Ver imagem atual de fundo do banner</label>
                                                                                                                            <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemBannerSlide1">Ver</button>
                                                                                                                        </div>
                                                                                                                        
                                                                                                                        <div class="form-group col-md-12">
                                                                                                                            <label class="col-form-label">SEO <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Descrição da imagem" data-original-title="Descrição da imagem"></i></label>
                                                                                                                            <input type="text" id="seoBannerSlide2" name="seoBannerSlide2" class="form-control" >
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-12">
                                                                                                                            <label class="col-form-label">Cor de fundo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do fundo do slide, utilize para combinar com o fundo da imagem" data-original-title="Cor do fundo do slide, utilize para combinar com o fundo da imagem"></i></label>
                                                                                                                            <input type="color" class="form-control" id="atualizarCorFundoSlide1" name="atualizarCorFundoSlide1">
                                                                                                                        </div>

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-xl-12">
                                                                                    <div class="card">
                                                                                        <div class="card-body" style="background-color: #353b44;">
                                                                                            
                                                                                            <div class="card-widgets">
                                                                                                <a data-toggle="collapse" href="#cardCollpaseBanner" role="button" aria-expanded="false" aria-controls="cardCollpaseBanner"><i class="mdi mdi-minus"></i></a>
                                                                                            </div>

                                                                                            <h4 class="header-title mb-0">Banner 2</h4>

                                                                                            <div id="cardCollpaseBanner" class="collapse pt-3">
                                                                                                <div class="row">

                                                                                                <div class="table-responsive">
                                                                                                        <table class="table table-bordered mb-0">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th>Opção</th>
                                                                                                                    <th>AÇÃO</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            
                                                                                                            <tbody>
                                                                                                                
                                                                                                            

                                                                                                                <tr>

                                                                                                                    <td class="col-12">Ativo? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Se desativado: Não mostrará o banner no topo do site" data-original-title="Se desativado: Não mostrará o banner no topo do site"></i></td>
                                                                                                                    <td>
                                                                                                                        <input type="checkbox" class="switch_1" id="ativaDesativaSlideLP" name="ativaDesativaSlideLP">
                                                                                                                    </td>
                                                                                                                </tr>

                                                                                                                <tr>

                                                                                                                    <td colspan="2" class="col-12">
                                                                                                                        
                                                                                                                        Item 1 (Banner) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Título e descrição do item do banner" data-original-title="Título e descrição do item do banner"></i>
                                                                                                                        
                                                                                                                        <div class="form-row">
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Para não aparecer texto em cima do banner/slide, deixe vazio. Não escreva nada." data-original-title="Para não aparecer texto em cima do banner/slide, deixe vazio. Não escreva nada."></i></label>
                                                                                                                                <input type="text" id="tituloBanner" name="tituloBanner" class="form-control" >
                                                                                                                            </div>
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Para não aparecer texto em cima do banner/slide, deixe vazio. Não escreva nada." data-original-title="Para não aparecer texto em cima do banner/slide, deixe vazio. Não escreva nada."></i></label>
                                                                                                                                <input type="text" id="descricaoBanner" name="descricaoBanner" class="form-control" >
                                                                                                                            </div> 
                                                                                                                        </div>

                                                                                                                        <div class="form-row">
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto do título" data-original-title="Cor do texto do título"></i></label>
                                                                                                                                <input type="color" class="form-control" id="corTextoTituloBanner" name="corTextoTituloBanner">
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                                <input type="color" class="form-control" id="corTextoDescricaoBanner" name="corTextoDescricaoBanner">
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                    
                                                                                                                </tr>

                                                                                                                <tr>

                                                                                                                    <td colspan="2" class="col-12">
                                                                                                                    
                                                                                                                        <div class="form-row">
                                                                                                                            
                                                                                                                            Imagem de fundo e imagem principal 
                                                                                                                            
                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Cor de fundo do banner <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos." data-original-title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos."></i></label>
                                                                                                                                <input type="color" class="form-control" id="corFundoBannerSlide" name="corFundoBannerSlide">
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Imagem de fundo do banner <small class="text-warning"> (1920px x 600px) {NÃO USAR INFORMAÇÕES IMPORTANTES}</small> <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos." data-original-title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos."></i></label>
                                                                                                                                <input type="file" class="form-control" id="imagemFundoSlide" name="imagemFundoSlide">
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-5">
                                                                                                                                <label class="col-form-label">Ver imagem atual de fundo do banner</label>
                                                                                                                                <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemDeFundo">Ver</button>
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-1">
                                                                                                                                <label class="col-form-label">Imagem <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Ative ou desative a imagem de fundo do banner" data-original-title="Ative ou desative a imagem de fundo do banner"></i></label>
                                                                                                                                <input type="checkbox" class="switch_1" id="imagemFundoSlideAtivadoDesativado" name="imagemFundoSlideAtivadoDesativado">
                                                                                                                            </div>
                                                                                                                            

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Imagem principal <small class="text-warning"> (500px x 320px)</small> <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Fica em destaque na frente dos elementos e ao lado dos textos" data-original-title="Fica em destaque na frente dos elementos e ao lado dos textos"></i></label>
                                                                                                                                <input type="file" class="form-control" id="imagemPrincipal" name="imagemPrincipal">
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-5">
                                                                                                                                <label class="col-form-label">Ver imagem atual principal</label>
                                                                                                                                <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemPrincipal">Ver</button>
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-1">
                                                                                                                                <label class="col-form-label">Imagem <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Ative ou desative a imagem PRINCIPAL do banner" data-original-title="Ative ou desative a imagem PRINCIPAL do banner"></i></label>
                                                                                                                                <input type="checkbox" class="switch_1" id="imagemPrincipalAtivadoDesativado" name="imagemPrincipalAtivadoDesativado">
                                                                                                                            </div>
                                                                                                                            
                                                                                                                        </div>
                                                                                                                    </td>
                                                                                                                    
                                                                                                                </tr>

                                                                                                                <tr>

                                                                                                                    <td class="col-12">Botão do banner <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Se desativado: Não mostrará o botão dentro do banner" data-original-title="Se desativado: Não mostrará o botão dentro do banner"></i></td>
                                                                                                                    
                                                                                                                    <td>
                                                                                                                        <input type="checkbox" class="switch_1" id="botaoSlideAtivaDesativa" name="botaoSlideAtivaDesativa">
                                                                                                                    </td>
                                                                                                                </tr>

                                                                                                                <tr>

                                                                                                                    <td colspan="2" class="col-12">
                                                                                                                        
                                                                                                                        Item 1 (Banner) - configurações do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Título e descrição do item do banner" data-original-title="Título e descrição do item do banner"></i>
                                                                                                                        
                                                                                                                        <div class="form-row">
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                                                <input type="text" class="form-control" id="textoBotaoSlideBanner" name="textoBotaoSlideBanner">
                                                                                                                            </div>
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Link <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com" data-original-title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com"></i></label>
                                                                                                                                <input type="text" class="form-control" id="linkBotaoSlideBanner" name="linkBotaoSlideBanner">
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Cor do texto </label>
                                                                                                                                <input type="color" class="form-control"id="corTextoBotaoSlideBanner" name="corTextoBotaoSlideBanner" >
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Cor do texto ao passar o mouse</label>
                                                                                                                                <input type="color" class="form-control" id="corTextoHoverBotaoSlideBanner" name="corTextoHoverBotaoSlideBanner">
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Cor do botão </label>
                                                                                                                                <input type="color" class="form-control" id="corfundoBotaoSlideBanner" name="corfundoBotaoSlideBanner">
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Cor do ao passar o mouse </label>
                                                                                                                                <input type="color" class="form-control" id="corFundoHoverBotaoSlideBanner" name="corFundoHoverBotaoSlideBanner">
                                                                                                                            </div>
                                                                                                                        </div>

                                                                                                                    </td>
                                                                                                                    
                                                                                                                </tr>

                                                                                                                
                                                                                                                
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                    
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <center class="mt-3">
                                                                                <button type="submit" id="botaoAtualizarLogoMenulide" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                    <span class="btn-label"><i class="fe-save"></i></span>Salvar
                                                                                </button>
                                                                            </center>

                                                                        </form>

                                                                    </div>

                                                                    <div class="tab-pane" id="formularios">
                                                                        
                                                                        
                                                                        <form id="formFormularios" enctype="multipart/form-data">

                                                                            <input type="hidden" name="idModeloFormularios" id="idModeloFormularios">
                                                                            <input type="hidden" name="idLandingPageFormularios" id="idLandingPageFormularios">

                                                                            <h4 class="header-title mb-2 mt-2">Opções do formulário</h4>

                                                                            <div class="row">
                                                                                <div class="col-xl-12">
                                                                                    <div id="accordion">
                                                                                        <div class="card mb-1">
                                                                                            <div class="card-header" id="headingOne">
                                                                                                <h5 class="m-0">
                                                                                                    <a class="text-dark" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                                                                        Onde nos conheceu?
                                                                                                    </a>
                                                                                                </h5>
                                                                                            </div>
                                                                                
                                                                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                                                                                <div class="card-body">
                                                                                                    
                                                                                                    <div class="form-row">
                                                                                                    
                                                                                                        <div class="col-3">

                                                                                                            <div id="accordionUm" class="mb-0">

                                                                                                                <div class="card">

                                                                                                                    <div class="card-header" id="ondeNosConheceuUm">
                                                                                                                        <h5 class="m-0">
                                                                                                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseUm" aria-expanded="false">
                                                                                                                                Opção 1
                                                                                                                            </a>
                                                                                                                        </h5>
                                                                                                                    </div>
                                                                                                        
                                                                                                                    <div id="collapseUm" class="collapse show" aria-labelledby="ondeNosConheceuUm" data-parent="#accordionUm">
                                                                                                                        
                                                                                                                        <div class="card-body">
                                                                                                                            
                                                                                                                            <div class="card-box cor_cardBox_bg">
                                                                                                                            
                                                                                                                                <div class="row">

                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <label for="nosConheceuOpcao1" class="col-form-label">Nome</label>
                                                                                                                                        <input type="text" class="form-control" name="nosConheceuOpcao1" id="nosConheceuOpcao1" autocomplete="off">
                                                                                                                                    </div>

                                                                                                                                    <div class="form-group col-md-12 text-center">
                                                                                                                                        <label for="nosConheceuOpcao1" class="col-form-label">Visivel?</label><br>
                                                                                                                                        <input type="checkbox" class="switch_1 mt-1" id="nosConheceuOpcao1_visivel" name="nosConheceuOpcao1_visivel">
                                                                                                                                    </div>
                                                                                                                                
                                                                                                                                </div>
                                                                                                                                
                                                                                                                            </div>

                                                                                                                        </div>

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                            </div>
                                                                                                            
                                                                                                        </div>
                                                                                                        
                                                                                                        <div class="col-3">

                                                                                                            <div id="accordionDois" class="mb-0">

                                                                                                                <div class="card">

                                                                                                                    <div class="card-header" id="ondeNosConheceuDois">
                                                                                                                        <h5 class="m-0">
                                                                                                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseDois" aria-expanded="false">
                                                                                                                                Opção 2
                                                                                                                            </a>
                                                                                                                        </h5>
                                                                                                                    </div>
                                                                                                        
                                                                                                                    <div id="collapseDois" class="collapse show" aria-labelledby="ondeNosConheceuDois" data-parent="#accordionDois">
                                                                                                                        
                                                                                                                        <div class="card-body">
                                                                                                                            
                                                                                                                            <div class="card-box cor_cardBox_bg">
                                                                                                                            
                                                                                                                                <div class="row">

                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <label for="nosConheceuOpcao2" class="col-form-label">Nome</label>
                                                                                                                                        <input type="text" class="form-control" name="nosConheceuOpcao2" id="nosConheceuOpcao2" autocomplete="off">
                                                                                                                                    </div>

                                                                                                                                    <div class="form-group col-md-12 text-center">
                                                                                                                                        <label for="nosConheceuOpcao2" class="col-form-label">Visivel?</label><br>
                                                                                                                                        <input type="checkbox" class="switch_1 mt-1" id="nosConheceuOpcao2_visivel" name="nosConheceuOpcao2_visivel">
                                                                                                                                    </div>
                                                                                                                                
                                                                                                                                </div>
                                                                                                                                
                                                                                                                            </div>

                                                                                                                        </div>

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                            </div>
                                                                                                            
                                                                                                        </div>

                                                                                                        <div class="col-3">

                                                                                                            <div id="accordionTres" class="mb-0">

                                                                                                                <div class="card">

                                                                                                                    <div class="card-header" id="ondeNosConheceuTres">
                                                                                                                        <h5 class="m-0">
                                                                                                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseTres" aria-expanded="false">
                                                                                                                                Opção 3
                                                                                                                            </a>
                                                                                                                        </h5>
                                                                                                                    </div>
                                                                                                        
                                                                                                                    <div id="collapseTres" class="collapse show" aria-labelledby="ondeNosConheceuTres" data-parent="#accordionTres">
                                                                                                                        
                                                                                                                        <div class="card-body">
                                                                                                                            
                                                                                                                            <div class="card-box cor_cardBox_bg">
                                                                                                                            
                                                                                                                                <div class="row">

                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <label for="nosConheceuOpcao3" class="col-form-label">Nome</label>
                                                                                                                                        <input type="text" class="form-control" name="nosConheceuOpcao3" id="nosConheceuOpcao3" autocomplete="off">
                                                                                                                                    </div>

                                                                                                                                    <div class="form-group col-md-12 text-center">
                                                                                                                                        <label for="nosConheceuOpcao3" class="col-form-label">Visivel?</label><br>
                                                                                                                                        <input type="checkbox" class="switch_1 mt-1" id="nosConheceuOpcao3_visivel" name="nosConheceuOpcao3_visivel">
                                                                                                                                    </div>
                                                                                                                                
                                                                                                                                </div>
                                                                                                                                
                                                                                                                            </div>

                                                                                                                        </div>

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                            </div>
                                                                                                            
                                                                                                        </div>

                                                                                                        <div class="col-3">

                                                                                                            <div id="accordionQuatro" class="mb-0">

                                                                                                                <div class="card">

                                                                                                                    <div class="card-header" id="ondeNosConheceuQuatro">
                                                                                                                        <h5 class="m-0">
                                                                                                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseQuatro" aria-expanded="false">
                                                                                                                                Opção 4
                                                                                                                            </a>
                                                                                                                        </h5>
                                                                                                                    </div>
                                                                                                        
                                                                                                                    <div id="collapseQuatro" class="collapse show" aria-labelledby="ondeNosConheceuQuatro" data-parent="#accordionQuatro">
                                                                                                                        
                                                                                                                        <div class="card-body">
                                                                                                                            
                                                                                                                            <div class="card-box cor_cardBox_bg">
                                                                                                                            
                                                                                                                                <div class="row">

                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <label for="nosConheceuOpcao4" class="col-form-label">Nome</label>
                                                                                                                                        <input type="text" class="form-control" name="nosConheceuOpcao4" id="nosConheceuOpcao4" autocomplete="off">
                                                                                                                                    </div>
                                                                                                                                    
                                                                                                                                    <div class="form-group col-md-12 text-center">
                                                                                                                                        <label for="nosConheceuOpcao4" class="col-form-label">Visivel?</label><br>
                                                                                                                                        <input type="checkbox" class="switch_1 mt-1" id="nosConheceuOpcao4_visivel" name="nosConheceuOpcao4_visivel">
                                                                                                                                    </div>
                                                                                                                                
                                                                                                                                </div>
                                                                                                                                
                                                                                                                            </div>

                                                                                                                        </div>

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                            </div>
                                                                                                            
                                                                                                        </div>

                                                                                                        <div class="col-3">

                                                                                                            <div id="accordionCinco" class="mb-0">

                                                                                                                <div class="card">

                                                                                                                    <div class="card-header" id="ondeNosConheceuCinco">
                                                                                                                        <h5 class="m-0">
                                                                                                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseCinco" aria-expanded="false">
                                                                                                                                Opção 5
                                                                                                                            </a>
                                                                                                                        </h5>
                                                                                                                    </div>
                                                                                                        
                                                                                                                    <div id="collapseCinco" class="collapse" aria-labelledby="ondeNosConheceuCinco" data-parent="#accordionCinco">
                                                                                                                        
                                                                                                                        <div class="card-body">
                                                                                                                            
                                                                                                                            <div class="card-box cor_cardBox_bg">
                                                                                                                            
                                                                                                                                <div class="row">

                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <label for="nosConheceuOpcao5" class="col-form-label">Nome</label>
                                                                                                                                        <input type="text" class="form-control" name="nosConheceuOpcao5" id="nosConheceuOpcao5" autocomplete="off">
                                                                                                                                    </div>
                                                                                                                                    
                                                                                                                                    <div class="form-group col-md-12 text-center">
                                                                                                                                        <label for="nosConheceuOpcao5" class="col-form-label">Visivel?</label><br>
                                                                                                                                        <input type="checkbox" class="switch_1 mt-1" id="nosConheceuOpcao5_visivel" name="nosConheceuOpcao5_visivel">
                                                                                                                                    </div>
                                                                                                                                
                                                                                                                                </div>
                                                                                                                                
                                                                                                                            </div>

                                                                                                                        </div>

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                            </div>
                                                                                                            
                                                                                                        </div>

                                                                                                        <div class="col-3">

                                                                                                            <div id="accordionSeis" class="mb-0">

                                                                                                                <div class="card">

                                                                                                                    <div class="card-header" id="ondeNosConheceuSeis">
                                                                                                                        <h5 class="m-0">
                                                                                                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseSeis" aria-expanded="false">
                                                                                                                                Opção 6
                                                                                                                            </a>
                                                                                                                        </h5>
                                                                                                                    </div>
                                                                                                        
                                                                                                                    <div id="collapseSeis" class="collapse" aria-labelledby="ondeNosConheceuSeis" data-parent="#accordionSeis">
                                                                                                                        
                                                                                                                        <div class="card-body">
                                                                                                                            
                                                                                                                            <div class="card-box cor_cardBox_bg">
                                                                                                                            
                                                                                                                                <div class="row">

                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <label for="nosConheceuOpcao6" class="col-form-label">Nome</label>
                                                                                                                                        <input type="text" class="form-control" name="nosConheceuOpcao6" id="nosConheceuOpcao6" autocomplete="off">
                                                                                                                                    </div>
                                                                                                                                    
                                                                                                                                    <div class="form-group col-md-12 text-center">
                                                                                                                                        <label for="nosConheceuOpcao6" class="col-form-label">Visivel?</label><br>
                                                                                                                                        <input type="checkbox" class="switch_1 mt-1" id="nosConheceuOpcao6_visivel" name="nosConheceuOpcao6_visivel">
                                                                                                                                    </div>
                                                                                                                                
                                                                                                                                </div>
                                                                                                                                
                                                                                                                            </div>

                                                                                                                        </div>

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                            </div>
                                                                                                            
                                                                                                        </div>

                                                                                                        <div class="col-3">

                                                                                                            <div id="accordionSete" class="mb-0">

                                                                                                                <div class="card">

                                                                                                                    <div class="card-header" id="ondeNosConheceuSete">
                                                                                                                        <h5 class="m-0">
                                                                                                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseSete" aria-expanded="false">
                                                                                                                                Opção 7
                                                                                                                            </a>
                                                                                                                        </h5>
                                                                                                                    </div>
                                                                                                        
                                                                                                                    <div id="collapseSete" class="collapse" aria-labelledby="ondeNosConheceuSete" data-parent="#accordionSete">
                                                                                                                        
                                                                                                                        <div class="card-body">
                                                                                                                            
                                                                                                                            <div class="card-box cor_cardBox_bg">
                                                                                                                            
                                                                                                                                <div class="row">

                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <label for="nosConheceuOpca7" class="col-form-label">Nome</label>
                                                                                                                                        <input type="text" class="form-control" name="nosConheceuOpcao7" id="nosConheceuOpcao7" autocomplete="off">
                                                                                                                                    </div>
                                                                                                                                    
                                                                                                                                    <div class="form-group col-md-12 text-center">
                                                                                                                                        <label for="nosConheceuOpcao7" class="col-form-label">Visivel?</label><br>
                                                                                                                                        <input type="checkbox" class="switch_1 mt-1" id="nosConheceuOpcao7_visivel" name="nosConheceuOpcao7_visivel">
                                                                                                                                    </div>
                                                                                                                                
                                                                                                                                </div>
                                                                                                                                
                                                                                                                            </div>

                                                                                                                        </div>

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                            </div>
                                                                                                            
                                                                                                        </div>

                                                                                                        <div class="col-3">

                                                                                                            <div id="accordionOito" class="mb-0">

                                                                                                                <div class="card">

                                                                                                                    <div class="card-header" id="ondeNosConheceuOito">
                                                                                                                        <h5 class="m-0">
                                                                                                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOito" aria-expanded="false">
                                                                                                                                Opção 8
                                                                                                                            </a>
                                                                                                                        </h5>
                                                                                                                    </div>
                                                                                                        
                                                                                                                    <div id="collapseOito" class="collapse" aria-labelledby="ondeNosConheceuOito" data-parent="#accordionOito">
                                                                                                                        
                                                                                                                        <div class="card-body">
                                                                                                                            
                                                                                                                            <div class="card-box cor_cardBox_bg">
                                                                                                                            
                                                                                                                                <div class="row">

                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <label for="nosConheceuOpca8" class="col-form-label">Nome</label>
                                                                                                                                        <input type="text" class="form-control" name="nosConheceuOpcao8" id="nosConheceuOpcao8" autocomplete="off">
                                                                                                                                    </div>
                                                                                                                                    
                                                                                                                                    <div class="form-group col-md-12 text-center">
                                                                                                                                        <label for="nosConheceuOpcao8" class="col-form-label">Visivel?</label><br>
                                                                                                                                        <input type="checkbox" class="switch_1 mt-1" id="nosConheceuOpcao8_visivel" name="nosConheceuOpcao8_visivel">
                                                                                                                                    </div>
                                                                                                                                
                                                                                                                                </div>
                                                                                                                                
                                                                                                                            </div>

                                                                                                                        </div>

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                            </div>
                                                                                                            
                                                                                                        </div>

                                                                                                    </div>         

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                            </div>

                                                                            <hr>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário 1</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormulario1" name="formFormulario1">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">
                                                                                                            
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloForm1" name="tituloForm1">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoForm1" name="descricaoForm1">
                                                                                                </div> 

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloForm1" name="corTituloForm1" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoForm1" name="corDescricaoForm1" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoForm1" name="textoBotaoForm1">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoForm1" name="corTextoBotaoForm1" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoForm1" name="corFundoBotaoForm1" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioForm1" name="corFundoFormularioForm1" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário Redes Sociais</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormularioRedesSociais" name="formFormularioRedesSociais">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">
                                                                                                
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloFormRS" name="tituloFormRS">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoFormRS" name="descricaoFormRS">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloFormRS" name="corTituloFormRS" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoFormRS" name="corDescricaoFormRS" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoFormRedesSociais" name="textoBotaoFormRedesSociais">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoFormRedesSociais" name="corTextoBotaoFormRedesSociais" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoFormRedesSociais" name="corFundoBotaoFormRedesSociais" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioFormRedesSociais" name="corFundoFormularioFormRedesSociais" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário 2</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormulario2" name="formFormulario2">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">
                                                                                                
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloForm2" name="tituloForm2">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoForm2" name="descricaoForm2">
                                                                                                </div> 

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloForm2" name="corTituloForm2" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoForm2" name="corDescricaoForm2" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" 
                                                                                                    data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoForm2" name="textoBotaoForm2">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" 
                                                                                                    data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoForm2" name="corTextoBotaoForm2" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoForm2" name="corFundoBotaoForm2" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioForm2" name="corFundoFormularioForm2" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário 3</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormulario3" name="formFormulario3">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloForm3" name="tituloForm3">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoForm3" name="descricaoForm3">
                                                                                                </div> 

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloForm3" name="corTituloForm3" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoForm3" name="corDescricaoForm3" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoForm3" name="textoBotaoForm3">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoForm3" name="corTextoBotaoForm3" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoForm3" name="corFundoBotaoForm3" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioForm3" name="corFundoFormularioForm3" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário 4</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormulario4" name="formFormulario4">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloForm4" name="tituloForm4">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoForm4" name="descricaoForm4">
                                                                                                </div> 

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloForm4" name="corTituloForm4" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoForm4" name="corDescricaoForm4" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoForm4" name="textoBotaoForm4">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoForm4" name="corTextoBotaoForm4" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoForm4" name="corFundoBotaoForm4" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioForm4" name="corFundoFormularioForm4" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário 5</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormulario5" name="formFormulario5">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloForm5" name="tituloForm5">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoForm5" name="descricaoForm5">
                                                                                                </div> 

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloForm5" name="corTituloForm5" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoForm5" name="corDescricaoForm5" >
                                                                                                </div>
                                                                                                
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoForm5" name="textoBotaoForm5">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoForm5" name="corTextoBotaoForm5" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoForm5" name="corFundoBotaoForm5" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioForm5" name="corFundoFormularioForm5" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário 6</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormulario6" name="formFormulario6">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloForm6" name="tituloForm6">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoForm6" name="descricaoForm6">
                                                                                                </div> 

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloForm6" name="corTituloForm6" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoForm6" name="corDescricaoForm6" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoForm6" name="textoBotaoForm6">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoForm6" name="corTextoBotaoForm6" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoForm6" name="corFundoBotaoForm6" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioForm6" name="corFundoFormularioForm6" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário 7</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormulario7" name="formFormulario7">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloForm7" name="tituloForm7">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoForm7" name="descricaoForm7">
                                                                                                </div> 

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloForm7" name="corTituloForm7" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoForm7" name="corDescricaoForm7" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoForm7" name="textoBotaoForm7">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoForm7" name="corTextoBotaoForm7" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoForm7" name="corFundoBotaoForm7" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioForm7" name="corFundoFormularioForm7" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>Formulário 8</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o formulário" data-original-title="Ative ou desative o formulário"></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="formFormulario8" name="formFormulario8">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <td colspan="2">
                                                                                            <div class="form-row">

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Título</label>
                                                                                                    <input type="text" class="form-control" id="tituloForm8" name="tituloForm8">
                                                                                                </div>  

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descricao</label>
                                                                                                    <input type="text" class="form-control" id="descricaoForm8" name="descricaoForm8">
                                                                                                </div> 

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTituloForm8" name="corTituloForm8" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do Descricao <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corDescricaoForm8" name="corDescricaoForm8" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                    <input type="text" class="form-control" id="textoBotaoForm8" name="textoBotaoForm8">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do texto do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto que aparecerá dentro do botão" data-original-title="Cor do texto que aparecerá dentro do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corTextoBotaoForm8" name="corTextoBotaoForm8" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de Fundo do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do botão, não use a mesma cor do texto" data-original-title="Cor de fundo do botão, não use a mesma cor do texto"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoBotaoForm8" name="corFundoBotaoForm8" >
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor de fundo do formulário <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do formulário, não use a mesma cor de fundo do botão" data-original-title="Cor de fundo do formulário, não use a mesma cor de fundo do botão"></i></label>
                                                                                                    <input type="color" class="form-control"id="corFundoFormularioForm8" name="corFundoFormularioForm8" >
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            
                                                                            <center class="mt-3">
                                                                                <button type="submit" id="botaoAtualizarFormularios" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                    <span class="btn-label"><i class="fe-save"></i></span>Salvar
                                                                                </button>
                                                                            </center>

                                                                        </form>
                                                                        
                                                                    </div>
                                                                    
                                                                    <div class="tab-pane" id="redesSociais">

                                                                        <form id="formConfuracoesRedesSociais" enctype="multipart/form-data">
                                                                        
                                                                            <input type="hidden" name="idConfigsRedesSociais" id="idConfigsRedesSociais">
                                                                            <input type="hidden" name="idLandingPageRedesSociais" id="idLandingPageRedesSociais">

                                                                            <div class="table-responsive">
                                                                                <table class="table table-bordered">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>SERVIÇO REDES SOCIAIS</th>
                                                                                            <th>AÇÃO</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>
                                                                                        
                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                data-placement="bottom" title="Ative ou desative o serviço de redes sociais na página, se desativado: não irá aparecer." data-original-title="Ative ou desative o serviço de redes sociais na página, se desativado: não irá aparecer."></i>
                                                                                            </td>
                                                                                            
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="servicoRedesSociais" name="servicoRedesSociais">
                                                                                            </td>
                                                                                        </tr>

                                                                                        <tr>

                                                                                            <td colspan="2" class="col-12">
                                                                                                
                                                                                                <div class="form-row">
                                                                                                    <div class="form-group col-md-12">
                                                                                                        <label class="col-form-label">Cor de fundo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cord de fundo do menu" data-original-title="Cord de fundo do menu"></i></label>
                                                                                                        <input type="color" class="form-control" id="corFundoRedesSociais" name="corFundoRedesSociais">
                                                                                                    </div>
                                                                                                </div>

                                                                                            </td>
                                                                                            
                                                                                        </tr>

                                                                                        <tr>

                                                                                            <td class="col-12">
                                                                                                Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                            </td>
                                                                                                
                                                                                            <td>
                                                                                                <input type="checkbox" class="switch_1" id="servicoRedesSociaisConteudoGenerico" name="servicoRedesSociaisConteudoGenerico">
                                                                                            </td>

                                                                                        </tr>
                                                                                        
                                                                                        <td colspan="2" class="col-12">
                                                                                            
                                                                                            <div class="form-row">
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                    data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                    data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                    <input type="text" id="tituloRedesSociais" name="tituloRedesSociais" class="form-control" >
                                                                                                </div>
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                    data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                    data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                    <input type="text" id="descricaoRedesSociais" name="descricaoRedesSociais" class="form-control" >
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-row">
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto do título" data-original-title="Cor do texto do título"></i></label>
                                                                                                    <input type="color" class="form-control" id="corTituloRedesSociais" name="corTituloRedesSociais">
                                                                                                </div>

                                                                                                <div class="form-group col-md-6">
                                                                                                    <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                    <input type="color" class="form-control" id="corTextoDescricaoRedesSociais" name="corTextoDescricaoRedesSociais">
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                            
                                                                            <div class="table-responsive">
                                                                                
                                                                                <table class="table table-bordered">
                                                                                    
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th colspan="1">Cor do titulo, descrição e fundo para as 3 opções</th>
                                                                                        </tr>
                                                                                    </thead>

                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="form-row">
                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor do título e descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                        title="Cor dos títulos e descrições, obs: aplica-se a todas as opções de redes sociais" data-original-title="Cor dos títulos e descrições, obs: aplica-se a todas as opções de redes sociais"></i></label>
                                                                                                        <input type="color" class="form-control" id="corTituloeDescricaoRedeSocial" name="corTituloeDescricaoRedeSocial">
                                                                                                    </div>

                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor do fundo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                        title="Cor de fundo, obs: aplica-se a todas as opções de redes sociais" data-original-title="Cor de fundo, obs: aplica-se a todas as opções de redes sociais"></i></label>
                                                                                                        <input type="color" class="form-control" id="corFundoRedeSocial" name="corFundoRedeSocial">
                                                                                                    </div>

                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor do texto dentro do botão</label>
                                                                                                        <input type="color" class="form-control"id="corTextoRedeSocial" name="corTextoRedeSocial" >
                                                                                                    </div>

                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor do texto ao passar o mouse</label>
                                                                                                        <input type="color" class="form-control" id="corTextoHoverBotaoRedeSocial" name="corTextoHoverBotaoRedeSocial">
                                                                                                    </div>

                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor de fundo do botão </label>
                                                                                                        <input type="color" class="form-control" id="corfundoBotaoRedeSocial" name="corfundoBotaoRedeSocial">
                                                                                                    </div>

                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor de fundo do botão ao passar o mouse </label>
                                                                                                        <input type="color" class="form-control" id="corFundoHoverBotaoRedeSocial" name="corFundoHoverBotaoRedeSocial">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>

                                                                                </table>

                                                                            </div>
                                                                            
                                                                            <div class="col-xl-12">
                                                                                <div id="accordion">
                                                                                    <div class="card mb-1">
                                                                                        <div class="card-header" id="opcaoRedeSocial1">
                                                                                            <h5 class="m-0">
                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapseRedeSocial1" aria-expanded="true"aria-controls="collapseRedesSociaisOpcao1">
                                                                                                    <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                    Opção 1 de rede social
                                                                                                </a>
                                                                                            </h5>
                                                                                        </div>
                                                                            
                                                                                        <div id="collapseRedeSocial1" class="collapse" aria-labelledby="opcaoRedeSocial1" data-parent="#accordion">
                                                                                            <div class="card-body">

                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-bordered">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>SERVIÇO REDES SOCIAIS</th>
                                                                                                                <th>AÇÃO</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        
                                                                                                        <tbody>
                                                                                                            
                                                                                                            <tr>

                                                                                                                <td class="col-12">
                                                                                                                    Opção 1 <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Primeira opção que irá aparecer" data-original-title="Primeira opção que irá aparecer"></i>
                                                                                                                </td>

                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="opcao1RedeSocial" name="opcao1RedeSocial">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                
                                                                                                                <td colspan="2" class="col-12">
                                                                                                                        
                                                                                                                    Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                                    title="Título e Descrição dentro a opção, ex: Titulo: Facebook.. Descrição: Curta nossa página" 
                                                                                                                    data-original-title="Título e Descrição dentro a opção, ex: Titulo: Facebook.. Descrição: Curta nossa página"></i>

                                                                                                                    <div class="form-row">
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                            data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                            <input type="text" id="tituloRedeSocial1" name="tituloRedeSocial1" class="form-control" >
                                                                                                                        </div>
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                            data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                            <input type="text" id="descricaoRedeSocial1" name="descricaoRedeSocial1" class="form-control" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="col-12 mt-3">
                                                                                                                    Botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Habilitar ou desabilitar o botão" data-original-title="Habilitar ou desabilitar o botão"></i>
                                                                                                                </td>

                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="botaoRedeSocial1" name="botaoRedeSocial1">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td colspan="2">
                                                                                                                    <div class="form-row mt-3">
                                                                                                                        
                                                                                                                        <div class="col-md-12">
                                                                                                                            Informações do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Altere os textos de título e descrição, altere também as cores para cada opção!" data-original-title="Altere os textos de título e descrição, altere também as cores para cada opção!"></i>
                                                                                                                        </div>
                                                                                                                        
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                                            <input type="text" class="form-control" id="textoBotao_RedeSocial_Opcao1" name="textoBotao_RedeSocial_Opcao1">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Link <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com" data-original-title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com"></i></label>
                                                                                                                            <input type="text" class="form-control" id="linkBotao_RedeSocial_Opcao1" name="linkBotao_RedeSocial_Opcao1">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Imagem <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos." data-original-title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos."></i></label>
                                                                                                                            <input type="file" class="form-control" id="imagemBotao_RedeSocial_Opcao1" name="imagemBotao_RedeSocial_Opcao1">
                                                                                                                        </div>
                                                                                                                        
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Ver imagem</label>
                                                                                                                            <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#imagem_RedeSocial_Opcao1">Ver</button>
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-12">
                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i></label>
                                                                                                                            <input type="text" class="form-control" id="imagemSEO_RedeSocial_Opcao1" name="imagemSEO_RedeSocial_Opcao1">
                                                                                                                        </div>
                                                                                                                        
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-xl-12">
                                                                                <div id="accordion">
                                                                                    <div class="card mb-1">
                                                                                        <div class="card-header" id="opcaoRedeSocial2">
                                                                                            <h5 class="m-0">
                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapseRedeSocial2" aria-expanded="true"aria-controls="collapseRedesSociaisOpcao2">
                                                                                                    <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                    Opção 2 de rede social
                                                                                                </a>
                                                                                            </h5>
                                                                                        </div>
                                                                            
                                                                                        <div id="collapseRedeSocial2" class="collapse" aria-labelledby="opcaoRedeSocial2" data-parent="#accordion">
                                                                                            <div class="card-body">

                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-bordered">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>SERVIÇO REDES SOCIAIS</th>
                                                                                                                <th>AÇÃO</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        
                                                                                                        <tbody>
                                                                                                            
                                                                                                            <tr>

                                                                                                                <td class="col-12">
                                                                                                                    Opção 2 <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Primeira opção que irá aparecer" data-original-title="Primeira opção que irá aparecer"></i>
                                                                                                                </td>

                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="opcao2RedeSocial" name="opcao2RedeSocial">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                
                                                                                                                <td colspan="2" class="col-12">
                                                                                                                        
                                                                                                                    Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                                    title="Título e Descrição dentro a opção, ex: Titulo: Facebook.. Descrição: Curta nossa página" 
                                                                                                                    data-original-title="Título e Descrição dentro a opção, ex: Titulo: Facebook.. Descrição: Curta nossa página"></i>

                                                                                                                    <div class="form-row">
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                            data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                            <input type="text" id="tituloRedeSocial2" name="tituloRedeSocial2" class="form-control" >
                                                                                                                        </div>
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                            data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                            <input type="text" id="descricaoRedeSocial2" name="descricaoRedeSocial2" class="form-control" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="col-12 mt-3">
                                                                                                                    Botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Habilitar ou desabilitar o botão" data-original-title="Habilitar ou desabilitar o botão"></i>
                                                                                                                </td>

                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="botaoRedeSocial2" name="botaoRedeSocial2">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td colspan="2">
                                                                                                                    <div class="form-row mt-3">
                                                                                                                        
                                                                                                                        <div class="col-md-12">
                                                                                                                            Informações do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Altere os textos de título e descrição, altere também as cores para cada opção!" data-original-title="Altere os textos de título e descrição, altere também as cores para cada opção!"></i>
                                                                                                                        </div>
                                                                                                                        
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                                            <input type="text" class="form-control" id="textoBotao_RedeSocial_Opcao2" name="textoBotao_RedeSocial_Opcao2">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Link <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com" data-original-title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com"></i></label>
                                                                                                                            <input type="text" class="form-control" id="linkBotao_RedeSocial_Opcao2" name="linkBotao_RedeSocial_Opcao2">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Imagem <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos." data-original-title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos."></i></label>
                                                                                                                            <input type="file" class="form-control" id="imagemBotao_RedeSocial_Opcao2" name="imagemBotao_RedeSocial_Opcao2">
                                                                                                                        </div>
                                                                                                                        
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Ver imagem</label>
                                                                                                                            <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#imagem_RedeSocial_Opcao2">Ver</button>
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-12">
                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i></label>
                                                                                                                            <input type="text" class="form-control" id="imagemSEO_RedeSocial_Opcao2" name="imagemSEO_RedeSocial_Opcao2">
                                                                                                                        </div>
                                                                                                                        
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-xl-12">
                                                                                <div id="accordion">
                                                                                    <div class="card mb-1">
                                                                                        <div class="card-header" id="opcaoRedeSocial3">
                                                                                            <h5 class="m-0">
                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapseRedeSocial3" aria-expanded="false">
                                                                                                    <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                    Opção 3 de rede social
                                                                                                </a>
                                                                                            </h5>
                                                                                        </div>
                                                                            
                                                                                        <div id="collapseRedeSocial3" class="collapse" aria-labelledby="opcaoRedeSocial3" data-parent="#accordion">
                                                                                            <div class="card-body">

                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-bordered">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>SERVIÇO REDES SOCIAIS</th>
                                                                                                                <th>AÇÃO</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        
                                                                                                        <tbody>
                                                                                                            
                                                                                                            <tr>

                                                                                                                <td class="col-12">
                                                                                                                    Opção 3 <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Primeira opção que irá aparecer" data-original-title="Terceira opção que irá aparecer"></i>
                                                                                                                </td>

                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="opcao3RedeSocial" name="opcao3RedeSocial">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                
                                                                                                                <td colspan="2" class="col-12">
                                                                                                                        
                                                                                                                    Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                                    title="Título e Descrição dentro a opção, ex: Titulo: Facebook.. Descrição: Curta nossa página" 
                                                                                                                    data-original-title="Título e Descrição dentro a opção, ex: Titulo: Facebook.. Descrição: Curta nossa página"></i>

                                                                                                                    <div class="form-row">
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                            data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                            <input type="text" id="tituloRedeSocial3" name="tituloRedeSocial3" class="form-control" >
                                                                                                                        </div>
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                            data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                            <input type="text" id="descricaoRedeSocial3" name="descricaoRedeSocial3" class="form-control" >
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="col-12 mt-3">
                                                                                                                    Botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Habilitar ou desabilitar o botão" data-original-title="Habilitar ou desabilitar o botão"></i>
                                                                                                                </td>

                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="botaoRedeSocial3" name="botaoRedeSocial3">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td colspan="2">
                                                                                                                    <div class="form-row mt-3">
                                                                                                                        
                                                                                                                        <div class="col-md-12">
                                                                                                                            Informações do botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Altere os textos de título e descrição, altere também as cores para cada opção!" data-original-title="Altere os textos de título e descrição, altere também as cores para cada opção!"></i>
                                                                                                                        </div>
                                                                                                                        
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                                            <input type="text" class="form-control" id="textoBotao_RedeSocial_Opcao3" name="textoBotao_RedeSocial_Opcao3">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Link <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com" data-original-title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com"></i></label>
                                                                                                                            <input type="text" class="form-control" id="linkBotao_RedeSocial_Opcao3" name="linkBotao_RedeSocial_Opcao3">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Imagem <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos." data-original-title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos."></i></label>
                                                                                                                            <input type="file" class="form-control" id="imagemBotao_RedeSocial_Opcao3" name="imagemBotao_RedeSocial_Opcao3">
                                                                                                                        </div>
                                                                                                                        
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Ver imagem</label>
                                                                                                                            <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#imagem_RedeSocial_Opcao3">Ver</button>
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-12">
                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i></label>
                                                                                                                            <input type="text" class="form-control" id="imagemSEO_RedeSocial_Opcao3" name="imagemSEO_RedeSocial_Opcao3">
                                                                                                                        </div>
                                                                                                                        
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            
                                                                            <center class="mt-3">
                                                                                <button type="submit" id="botaoAtualizarRedesSociais" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                    <span class="btn-label"><i class="fe-save"></i></span>Salvar
                                                                                </button>
                                                                            </center>
                                                                            
                                                                        </form>

                                                                    </div>
                                                                    
                                                                    <div class="tab-pane" id="servicos">
                                                                        
                                                                    <div class="col-xl-12">

                                                                        <div class="card-box">

                                                                            <h4 class="header-title mb-4">Configurações dos serviços</h4>

                                                                            <ul class="nav nav-tabs nav-bordered nav-justified">
                                                                                
                                                                                <li class="nav-item">
                                                                                    <a href="#servico1" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                                                        Serviço 1
                                                                                    </a>
                                                                                </li>

                                                                                <li class="nav-item">
                                                                                    <a href="#servico2" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                                                        Serviço 2
                                                                                    </a>
                                                                                </li>

                                                                                <li class="nav-item">
                                                                                    <a href="#servico3" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                                        Serviço 3
                                                                                    </a>
                                                                                </li>

                                                                                <li class="nav-item">
                                                                                    <a href="#servico4" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                                        Serviço 4
                                                                                    </a>
                                                                                </li>

                                                                                <li class="nav-item">
                                                                                    <a href="#servico5" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                                        Serviço 5
                                                                                    </a>
                                                                                </li>

                                                                                <li class="nav-item">
                                                                                    <a href="#servico6" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                                        Serviço 6
                                                                                    </a>
                                                                                </li>

                                                                                <li class="nav-item">
                                                                                    <a href="#servico7" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                                        Serviço 7
                                                                                    </a>
                                                                                </li>

                                                                                <li class="nav-item">
                                                                                    <a href="#servico8" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                                        Serv. 8 Maps
                                                                                    </a>
                                                                                </li>
                                                                                
                                                                            </ul>

                                                                            <div class="tab-content">
                                                                                
                                                                                <div class="tab-pane active" id="servico1">
                                                                                    
                                                                                    <form id="formServico1">

                                                                                        <input type="hidden" name="idModelo_servico1" id="idModelo_servico1">
                                                                                        <input type="hidden" name="idLandingPage_servico1" id="idLandingPage_servico1">

                                                                                        <div class="table-responsive">
                                                                                                    <table class="table table-bordered">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th>SESSÃO DE SERVIÇOS 1</th>
                                                                                                                <th>AÇÃO</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        
                                                                                                        <tbody>
                                                                                                            
                                                                                                            <tr>

                                                                                                                <td class="col-12">
                                                                                                                    Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece" data-original-title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece"></i>
                                                                                                                </td>

                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="servico1_ativaDesativa" name="servico1_ativaDesativa">
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>

                                                                                                                <td class="col-12">
                                                                                                                    Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                                    title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                                    data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                                                </td>
                                                                                                                    
                                                                                                                <td>
                                                                                                                    <input type="checkbox" class="switch_1" id="servico1_ConteudoGenerico" name="servico1_ConteudoGenerico">
                                                                                                                </td>

                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                
                                                                                                                <td colspan="2" class="col-12">

                                                                                                                    <div class="form-row">
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                            data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                            <input type="text" id="servico1_titulo" name="servico1_titulo" class="form-control" >
                                                                                                                        </div>
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                            data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                            <input type="text" id="servico1_descricao" name="servico1_descricao" class="form-control" >
                                                                                                                        </div>
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                            <input type="color" class="form-control" id="servico1_corTitulo" name="servico1_corTitulo">
                                                                                                                        </div>
                                                                                                                        <div class="form-group col-md-6">
                                                                                                                            <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                            <input type="color" class="form-control" id="servico1_corDescricao" name="servico1_corDescricao">
                                                                                                                        </div>
                                                                                                                        <div class="form-group col-md-4">
                                                                                                                            <label class="col-form-label">Cor de fundo do serviço <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos" data-original-title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos"></i></label>
                                                                                                                            <input type="color" class="form-control" id="servico1_corFundoBG" name="servico1_corFundoBG">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-4">
                                                                                                                            <label class="col-form-label">Cor dos textos (serviços/produtos)
                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                                                title="Cor dos textos referentes aos produtos/serviços" 
                                                                                                                                data-original-title="Cor dos textos referentes aos produtos/serviços"></i></label>
                                                                                                                            <input type="color" class="form-control" id="servico1_corTextosProdsServs" name="servico1_corTextosProdsServs">
                                                                                                                        </div>

                                                                                                                        <div class="form-group col-md-4">
                                                                                                                            <label class="col-form-label">Cor de fundo dos produtos/serviços 
                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                                                title="Cor de fundo dos produtos/serviços" 
                                                                                                                                data-original-title="Cor de fundo dos produtos/serviços"></i></label>
                                                                                                                            <input type="color" class="form-control" id="servico1_corFundoProdutosServicos" name="servico1_corFundoProdutosServicos">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            

                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-xl-6">
                                                                                                        <div id="accordion" class="mb-3">
                                                                                                            <div class="card mb-1">
                                                                                                                <div class="card-header" id="headingOne">
                                                                                                                    <h5 class="m-0">
                                                                                                                        <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                                                                                                            Opção 1
                                                                                                                        </a>

                                                                                                                        <a class="text-dark pull-right">
                                                                                                                            <input type="checkbox" class="switch_1" id="servico1_opcao1" name="servico1_opcao1">
                                                                                                                        </a>
                                                                                                                    </h5>
                                                                                                                </div>
                                                                                                    
                                                                                                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                                                                                                    <div class="card-body">
                                                                                                                        <center>
                                                                                                                            <img id="servico1_imagem1" class="servico1_imagem">
                                                                                                                        </center>

                                                                                                                        <div class="input-group col-md-12 mt-2">
                                                                                                                           
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button id="caminhoFakeServico1_opcao1" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                                    <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                                </button>
                                                                                                                            </span>

                                                                                                                            <input type="file" id="servico1_imagem_opcao1" name="servico1_imagem_opcao1" style="display:none">
                                                                                                                            
                                                                                                                            <input type="text" id="servico1nomeFake_opcao1" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                            
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                                                                </button>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="input-group mt-2">
                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                    data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                </label>
                                                                                                                                <input type="text" id="servico1_SEO_opcao1" name="servico1_SEO_opcao1" class="form-control" >
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo </label>
                                                                                                                                <input type="text" id="servico1_titulo_opcao1" name="servico1_titulo_opcao1" class="form-control" >
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição </label>
                                                                                                                                <input type="text" id="servico1_descricao_opcao1" name="servico1_descricao_opcao1" class="form-control" >
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="card mb-1">
                                                                                                                <div class="card-header" id="headingTwo">
                                                                                                                    <h5 class="m-0">
                                                                                                                        
                                                                                                                        <a class="text-dark collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                                                                                                            Opção 2
                                                                                                                        </a>

                                                                                                                        <a class="text-dark pull-right">
                                                                                                                            <input type="checkbox" class="switch_1" id="servico1_opcao2" name="servico1_opcao2">
                                                                                                                        </a>
                                                                                                                    </h5>
                                                                                                                </div>
                                                                                                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                                                                                    <div class="card-body">
                                                                                                                        <center>
                                                                                                                            <img id="servico1_imagem2" class="servico1_imagem">
                                                                                                                        </center>

                                                                                                                        <div class="input-group col-md-12 mt-2">
                                                                                                                           
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button id="caminhoFakeServico1_opcao2" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                                    <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                                </button>
                                                                                                                            </span>

                                                                                                                            <input type="file" id="servico1_imagem_opcao2" name="servico1_imagem_opcao2" style="display:none">
                                                                                                                            
                                                                                                                            <input type="text" id="servico1nomeFake_opcao2" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                            
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                                                                </button>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="input-group mt-2">

                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                    data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                </label>
                                                                                                                                <input type="text" id="servico1_SEO_opcao2" name="servico1_SEO_opcao2" class="form-control" >
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo </label>
                                                                                                                                <input type="text" id="servico1_titulo_opcao2" name="servico1_titulo_opcao2" class="form-control" >
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição </label>
                                                                                                                                <input type="text" id="servico1_descricao_opcao2" name="servico1_descricao_opcao2" class="form-control" >
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="card mb-1">
                                                                                                                <div class="card-header" id="headingThree">
                                                                                                                    <h5 class="m-0">
                                                                                                                        <a class="text-dark" data-toggle="collapse" href="#collapseThree" aria-expanded="false">
                                                                                                                            opção 3
                                                                                                                        </a>

                                                                                                                        <a class="text-dark pull-right">
                                                                                                                            <input type="checkbox" class="switch_1" id="servico1_opcao3" name="servico1_opcao3">
                                                                                                                        </a>
                                                                                                                    </h5>
                                                                                                                </div>
                                                                                                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                                                                                    <div class="card-body">
                                                                                                                        <center>
                                                                                                                            <img id="servico1_imagem3" class="servico1_imagem">
                                                                                                                        </center>

                                                                                                                        <div class="input-group col-md-12 mt-2">
                                                                                                                           
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button id="caminhoFakeServico1_opcao3" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                                    <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                                </button>
                                                                                                                            </span>

                                                                                                                            <input type="file" id="servico1_imagem_opcao3" name="servico1_imagem_opcao3" style="display:none">
                                                                                                                            
                                                                                                                            <input type="text" id="servico1nomeFake_opcao3" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                            
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                                                                </button>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="input-group mt-2">

                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                    data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                </label>
                                                                                                                                <input type="text" id="servico1_SEO_opcao3" name="servico1_SEO_opcao3" class="form-control" >
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo </label>
                                                                                                                                <input type="text" id="servico1_titulo_opcao3" name="servico1_titulo_opcao3" class="form-control" >
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição </label>
                                                                                                                                <input type="text" id="servico1_descricao_opcao3" name="servico1_descricao_opcao3" class="form-control" >
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                
                                                                                                            <div class="card mb-1">
                                                                                                                <div class="card-header" id="headingFour">
                                                                                                                    <h5 class="m-0">
                                                                                                                        <a class="text-dark" data-toggle="collapse" href="#collapseFour" aria-expanded="false">
                                                                                                                            opção 4
                                                                                                                        </a>

                                                                                                                        <a class="text-dark pull-right">
                                                                                                                            <input type="checkbox" class="switch_1" id="servico1_opcao4" name="servico1_opcao4">
                                                                                                                        </a>
                                                                                                                    </h5>
                                                                                                                </div>
                                                                                                                <div id="collapseFour" class="collapse" aria-labelledby="collapseFour" data-parent="#accordion">
                                                                                                                    <div class="card-body">
                                                                                                                        <center>
                                                                                                                            <img id="servico1_imagem4" class="servico1_imagem">
                                                                                                                        </center>

                                                                                                                        <div class="input-group col-md-12 mt-2">
                                                                                                                           
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button id="caminhoFakeServico1_opcao4" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                                    <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                                </button>
                                                                                                                            </span>

                                                                                                                            <input type="file" id="servico1_imagem_opcao4" name="servico1_imagem_opcao4" style="display:none">
                                                                                                                            
                                                                                                                            <input type="text" id="servico1nomeFake_opcao4" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                            
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                                                                </button>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="input-group mt-2">
                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                    data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                </label>
                                                                                                                                <input type="text" id="servico1_SEO_opcao4" name="servico1_SEO_opcao4" class="form-control" >
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo </label>
                                                                                                                                <input type="text" id="servico1_titulo_opcao4" name="servico1_titulo_opcao4" class="form-control" >
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição </label>
                                                                                                                                <input type="text" id="servico1_descricao_opcao4" name="servico1_descricao_opcao4" class="form-control" >
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-xl-6">
                                                                                                        <div id="accordion" class="mb-3">
                                                                                                            <div class="card mb-1">
                                                                                                                <div class="card-header" id="heading5">
                                                                                                                    <h5 class="m-0">
                                                                                                                        <a class="text-dark collapsed" data-toggle="collapse" href="#collapse5" aria-expanded="false">
                                                                                                                            Opção 5
                                                                                                                        </a>

                                                                                                                        <a class="text-dark pull-right">
                                                                                                                            <input type="checkbox" class="switch_1" id="servico1_opcao5" name="servico1_opcao5">
                                                                                                                        </a>
                                                                                                                    </h5>
                                                                                                                </div>
                                                                                                    
                                                                                                                <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion" style="">
                                                                                                                     <div class="card-body">
                                                                                                                        <center>
                                                                                                                            <img id="servico1_imagem5" class="servico1_imagem">
                                                                                                                        </center>

                                                                                                                        <div class="input-group col-md-12 mt-2">
                                                                                                                           
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button id="caminhoFakeServico1_opcao5" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                                    <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                                </button>
                                                                                                                            </span>

                                                                                                                            <input type="file" id="servico1_imagem_opcao5" name="servico1_imagem_opcao5" style="display:none">
                                                                                                                            
                                                                                                                            <input type="text" id="servico1nomeFake_opcao5" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                            
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                                                                </button>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="input-group mt-2">
                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                    data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                </label>
                                                                                                                                <input type="text" id="servico1_SEO_opcao5" name="servico1_SEO_opcao5" class="form-control" >
                                                                                                                            </div>
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo </label>
                                                                                                                                <input type="text" id="servico1_titulo_opcao5" name="servico1_titulo_opcao5" class="form-control" >
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição </label>
                                                                                                                                <input type="text" id="servico1_descricao_opcao5" name="servico1_descricao_opcao5" class="form-control" >
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="card mb-1">
                                                                                                                <div class="card-header" id="heading6">
                                                                                                                    <h5 class="m-0">
                                                                                                                        <a class="text-dark" data-toggle="collapse" href="#collapse6" aria-expanded="false">
                                                                                                                            opção 6
                                                                                                                        </a>

                                                                                                                        <a class="text-dark pull-right">
                                                                                                                            <input type="checkbox" class="switch_1" id="servico1_opcao6" name="servico1_opcao6">
                                                                                                                        </a>
                                                                                                                    </h5>
                                                                                                                </div>
                                                                                                                <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                                                                                                                    <div class="card-body">
                                                                                                                        <center>
                                                                                                                            <img id="servico1_imagem6" class="servico1_imagem">
                                                                                                                        </center>

                                                                                                                        <div class="input-group col-md-12 mt-2">
                                                                                                                           
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button id="caminhoFakeServico1_opcao6" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                                    <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                                </button>
                                                                                                                            </span>

                                                                                                                            <input type="file" id="servico1_imagem_opcao6" name="servico1_imagem_opcao6" style="display:none">
                                                                                                                            
                                                                                                                            <input type="text" id="servico1nomeFake_opcao6" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                            
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                                                                </button>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="input-group mt-2">
                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                    data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                </label>
                                                                                                                                <input type="text" id="servico1_SEO_opcao6" name="servico1_SEO_opcao6" class="form-control" >
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo </label>
                                                                                                                                <input type="text" id="servico1_titulo_opcao6" name="servico1_titulo_opcao6" class="form-control" >
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição </label>
                                                                                                                                <input type="text" id="servico1_descricao_opcao6" name="servico1_descricao_opcao6" class="form-control" >
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="card mb-1">
                                                                                                                <div class="card-header" id="heading7">
                                                                                                                    <h5 class="m-0">
                                                                                                                        <a class="text-dark" data-toggle="collapse" href="#collapse7" aria-expanded="false">
                                                                                                                            opção 7
                                                                                                                        </a>

                                                                                                                        <a class="text-dark pull-right">
                                                                                                                            <input type="checkbox" class="switch_1" id="servico1_opcao7" name="servico1_opcao7">
                                                                                                                        </a>
                                                                                                                    </h5>
                                                                                                                </div>
                                                                                                                <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
                                                                                                                    <div class="card-body">
                                                                                                                        <center>
                                                                                                                            <img id="servico1_imagem7" class="servico1_imagem">
                                                                                                                        </center>

                                                                                                                        <div class="input-group col-md-12 mt-2">
                                                                                                                           
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button id="caminhoFakeServico1_opcao7" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                                    <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                                </button>
                                                                                                                            </span>

                                                                                                                            <input type="file" id="servico1_imagem_opcao7" name="servico1_imagem_opcao7" style="display:none">
                                                                                                                            
                                                                                                                            <input type="text" id="servico1nomeFake_opcao7" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                            
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                                                                </button>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="input-group mt-2">
                                                                                                                            
                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                    data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                </label>
                                                                                                                                <input type="text" id="servico1_SEO_opcao7" name="servico1_SEO_opcao7" class="form-control" >
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo </label>
                                                                                                                                <input type="text" id="servico1_titulo_opcao7" name="servico1_titulo_opcao7" class="form-control" >
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição </label>
                                                                                                                                <input type="text" id="servico1_descricao_opcao7" name="servico1_descricao_opcao7" class="form-control" >
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                
                                                                                                            <div class="card mb-1">
                                                                                                                <div class="card-header" id="heading8">
                                                                                                                    <h5 class="m-0">
                                                                                                                        <a class="text-dark" data-toggle="collapse" href="#collapse8" aria-expanded="false">
                                                                                                                            opção 8
                                                                                                                        </a>

                                                                                                                        <a class="text-dark pull-right">
                                                                                                                            <input type="checkbox" class="switch_1" id="servico1_opcao8" name="servico1_opcao8">
                                                                                                                        </a>
                                                                                                                    </h5>
                                                                                                                </div>
                                                                                                                <div id="collapse8" class="collapse" aria-labelledby="collapse8" data-parent="#accordion">
                                                                                                                    <div class="card-body">
                                                                                                                        <center>
                                                                                                                            <img id="servico1_imagem8" class="servico1_imagem">
                                                                                                                        </center>

                                                                                                                        <div class="input-group col-md-12 mt-2">
                                                                                                                           
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button id="caminhoFakeServico1_opcao8" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                                    <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                                </button>
                                                                                                                            </span>

                                                                                                                            <input type="file" id="servico1_imagem_opcao8" name="servico1_imagem_opcao8" style="display:none">
                                                                                                                            
                                                                                                                            <input type="text" id="servico1nomeFake_opcao8" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                            
                                                                                                                            <span class="input-group-btn">
                                                                                                                                <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                                                                </button>
                                                                                                                            </span>
                                                                                                                        </div>

                                                                                                                        <div class="input-group mt-2">
                                                                                                                            
                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                    data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                </label>
                                                                                                                                <input type="text" id="servico1_SEO_opcao8" name="servico1_SEO_opcao8" class="form-control" >
                                                                                                                            </div>

                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Titulo </label>
                                                                                                                                <input type="text" id="servico1_titulo_opcao8" name="servico1_titulo_opcao8" class="form-control" >
                                                                                                                            </div>
                                                                                                                            
                                                                                                                            <div class="form-group col-md-6">
                                                                                                                                <label class="col-form-label">Descrição </label>
                                                                                                                                <input type="text" id="servico1_descricao_opcao8" name="servico1_descricao_opcao8" class="form-control" >
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                        <center>
                                                                                            <button type="submit"  class="btn btn-xs btn-success waves-effect waves-light">
                                                                                                <span class="btn-label"><i class="fe-save"></i></span> Salvar
                                                                                            </button>
                                                                                        </center>

                                                                                    </form>
                                                                                </div>

                                                                                <div class="tab-pane" id="servico2">
                                                                                    
                                                                                    <form id="formServico2">
                                                                                        
                                                                                        <input type="hidden" name="idModelo_servico2" id="idModelo_servico2">
                                                                                        <input type="hidden" name="idLandingPage_servico2" id="idLandingPage_servico2">

                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-bordered">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>SESSÃO DE SERVIÇOS 2</th>
                                                                                                        <th>AÇÃO</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                
                                                                                                <tbody>
                                                                                                    
                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                            data-placement="top" title="Ative ou desative o serviço (2) na página, se desativado: o serviço não irá aparece" data-original-title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece"></i>
                                                                                                        </td>

                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico2_ativaDesativa" name="servico2_ativaDesativa">
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                            title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                            data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico2_ConteudoGenerico" name="servico2_ConteudoGenerico">
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        
                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico2_titulo" name="servico2_titulo" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico2_descricao" name="servico2_descricao" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico2_corTitulo" name="servico2_corTitulo">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico2_corDescricao" name="servico2_corDescricao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor de fundo do serviço <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos" data-original-title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico2_corFundoBG" name="servico2_corFundoBG">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Imagem <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos." data-original-title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos."></i></label>
                                                                                                                    <input type="file" class="form-control" id="servico2_imagemFundo" name="servico2_imagemFundo">
                                                                                                                </div>
                                                                                                                
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Ver imagem </label>
                                                                                                                    <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemServico2">Ver</button>
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-12">
                                                                                                                    <label class="col-form-label">Titulo/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i></label>
                                                                                                                    <input type="text" id="servico2_imagemSEO" name="servico2_imagemSEO" class="form-control" >
                                                                                                                </div>
                                                                                                            </div>  
                                                                                                            
                                                                                                        </td>
                                                                                                    </tr>


                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Se desativado: Não mostrará o botão" data-original-title="Se desativado: Não mostrará o botão"></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico2_botao" name="servico2_botao">
                                                                                                        </td>

                                                                                                    </tr>
                                                                                                    

                                                                                                    <tr>

                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                                    <input type="text" class="form-control" id="servico2_textoBotao" name="servico2_textoBotao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Link <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com" data-original-title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com"></i></label>
                                                                                                                    <input type="text" class="form-control" id="servico2_linkBotao" name="servico2_linkBotao">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do texto </label>
                                                                                                                    <input type="color" class="form-control"id="servico2_corTextoBotao" name="servico2_corTextoBotao" >
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do texto ao passar o mouse</label>
                                                                                                                    <input type="color" class="form-control" id="servico2_corTextoHoverBotao" name="servico2_corTextoHoverBotao">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do botão </label>
                                                                                                                    <input type="color" class="form-control" id="servico2_corFundoBotao" name="servico2_corFundoBotao">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do fundo ao passar o mouse </label>
                                                                                                                    <input type="color" class="form-control" id="servico2_corFundoHoverBotao" name="servico2_corFundoHoverBotao">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </td>
                                                                                                    
                                                                                                    </tr>

                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    
                                                                                        <center>
                                                                                            <button type="submit"  class="btn btn-xs btn-success waves-effect waves-light">
                                                                                                <span class="btn-label"><i class="fe-save"></i></span> Salvar
                                                                                            </button>
                                                                                        </center>

                                                                                    </form>

                                                                                </div>

                                                                                <div class="tab-pane" id="servico3">
                                                                                    
                                                                                    <form id="formServico3">
                                                                                        
                                                                                        <input type="hidden" name="idModelo_servico3" id="idModelo_servico3">
                                                                                        <input type="hidden" name="idLandingPage_servico3" id="idLandingPage_servico3">

                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-bordered">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>SESSÃO DE SERVIÇOS 3</th>
                                                                                                        <th>AÇÃO</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                
                                                                                                <tbody>
                                                                                                    
                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                            data-placement="top" title="Ative ou desative o serviço (3) na página, se desativado: o serviço não irá aparece" data-original-title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece"></i>
                                                                                                        </td>

                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico3_ativaDesativa" name="servico3_ativaDesativa">
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                            title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                            data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico3_ConteudoGenerico" name="servico3_ConteudoGenerico">
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        
                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico3_titulo" name="servico3_titulo" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico3_descricao" name="servico3_descricao" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-3">
                                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico3_corTitulo" name="servico3_corTitulo">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-3">
                                                                                                                    <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico3_corDescricao" name="servico3_corDescricao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-3">
                                                                                                                    <label class="col-form-label">Cor de fundo imagens <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do fundo das imagens de produtos/serviços" data-original-title="Cor do fundo das imagens de produtos/serviços"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico3_corFundoImagens" name="servico3_corFundoImagens">
                                                                                                                </div>
                                                                                                                
                                                                                                                <div class="form-group col-md-3">
                                                                                                                    <label class="col-form-label">Cor dos textos <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se a todas as opções" data-original-title="Aplica-se a todas as opções"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico3_corTextos" name="servico3_corTextos">
                                                                                                                </div>


                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor de fundo do serviço <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos" data-original-title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico3_corFundoBG" name="servico3_corFundoBG">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Imagem <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos." data-original-title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos."></i></label>
                                                                                                                    <input type="file" class="form-control" id="servico3_imagemFundo" name="servico3_imagemFundo">
                                                                                                                </div>
                                                                                                                
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Ver imagem </label>
                                                                                                                    <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemServico3">Ver</button>
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-12">
                                                                                                                    <label class="col-form-label">Titulo/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i></label>
                                                                                                                    <input type="text" id="servico3_imagemSEO5" name="servico3_imagemSEO5" class="form-control" >
                                                                                                                </div>
                                                                                                            </div>  
                                                                                                            
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td class="col-12">
                                                                                                            Mostrar todas? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" 
                                                                                                            title="Se desabilitado, não irá mostrar nenhuma das opções, ocultará as 4 opções" 
                                                                                                            data-original-title="Se desabilitado, não irá mostrar nenhuma das opções, ocultará as 4 opções"></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico3_todasOpcoes" name="servico3_todasOpcoes">
                                                                                                        </td>
                                                                                                        
                                                                                                    </tr>
                                                                                                    
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                        
                                                                                        <div class="row">
                                                                                            <div class="col-xl-6">
                                                                                                <div id="accordion" class="mb-3">
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico3">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico3" aria-expanded="false">
                                                                                                                    Opção 1
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico3_opcao1" name="servico3_opcao1">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico3" class="collapse" aria-labelledby="heading_servico3" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico3_imagem1" class="servico3_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico3_opcao1" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico3_imagem_opcao1" name="servico3_imagem_opcao1" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico3nomeFake_opcao1" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico3_SEO_opcao1" name="servico3_SEO_opcao1" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico3_titulo_opcao1" name="servico3_titulo_opcao1" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico3_descricao_opcao1" name="servico3_descricao_opcao1" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="headingTwo">
                                                                                                            <h5 class="m-0">
                                                                                                                
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                                                                                                    Opção 2
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico3_opcao2" name="servico3_opcao2">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico3_imagem2" class="servico3_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico3_opcao2" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico3_imagem_opcao2" name="servico3_imagem_opcao2" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico3nomeFake_opcao2" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">

                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico3_SEO_opcao2" name="servico3_SEO_opcao2" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico3_titulo_opcao2" name="servico3_titulo_opcao2" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico3_descricao_opcao2" name="servico3_descricao_opcao2" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-xl-6">
                                                                                                <div id="accordion" class="mb-3">
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading5">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse5" aria-expanded="false">
                                                                                                                    Opção 3
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico3_opcao3" name="servico3_opcao3">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion" style="">
                                                                                                                <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico3_imagem3" class="servico3_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico3_opcao3" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico3_imagem_opcao3" name="servico3_imagem_opcao3" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico3nomeFake_opcao3" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico3_SEO_opcao3" name="servico3_SEO_opcao3" class="form-control" >
                                                                                                                    </div>
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico3_titulo_opcao3" name="servico3_titulo_opcao3" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico3_descricao_opcao3" name="servico3_descricao_opcao3" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading6">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark" data-toggle="collapse" href="#collapse6" aria-expanded="false">
                                                                                                                    opção 4
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico3_opcao4" name="servico3_opcao4">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                                        <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico3_imagem4" class="servico3_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico3_opcao4" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico3_imagem_opcao4" name="servico3_imagem_opcao4" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico3nomeFake_opcao4" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico3_SEO_opcao4" name="servico3_SEO_opcao4" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico3_titulo_opcao4" name="servico3_titulo_opcao4" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico3_descricao_opcao4" name="servico3_descricao_opcao4" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    
                                                                                        <center>
                                                                                            <button type="submit"  class="btn btn-xs btn-success waves-effect waves-light">
                                                                                                <span class="btn-label"><i class="fe-save"></i></span> Salvar
                                                                                            </button>
                                                                                        </center>

                                                                                    </form>

                                                                                </div>

                                                                                <div class="tab-pane" id="servico4">
                                                                                    
                                                                                <form id="formServico4">
                                                                                        
                                                                                        <input type="hidden" name="idModelo_servico4" id="idModelo_servico4">
                                                                                        <input type="hidden" name="idLandingPage_servico4" id="idLandingPage_servico4">

                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-bordered">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>SESSÃO DE SERVIÇOS 4</th>
                                                                                                        <th>AÇÃO</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                
                                                                                                <tbody>
                                                                                                    
                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                            data-placement="top" title="Ative ou desative o serviço (4) na página, se desativado: o serviço não irá aparece" data-original-title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece"></i>
                                                                                                        </td>

                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico4_ativaDesativa" name="servico4_ativaDesativa">
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                            title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                            data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico4_ConteudoGenerico" name="servico4_ConteudoGenerico">
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        
                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico4_titulo" name="servico4_titulo" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico4_descricao" name="servico4_descricao" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico4_corTitulo" name="servico4_corTitulo">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico4_corDescricao" name="servico4_corDescricao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor de fundo do serviço <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos" data-original-title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico4_corFundoBG" name="servico4_corFundoBG">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Imagem <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos." data-original-title="Aplica-se ao fundo do banner, ficando por trás de todos os elementos."></i></label>
                                                                                                                    <input type="file" class="form-control" id="servico4_imagemFundo" name="servico4_imagemFundo">
                                                                                                                </div>
                                                                                                                
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Ver imagem </label>
                                                                                                                    <button type="button" class="btn btn-block btn--md btn-primary waves-effect waves-light" data-toggle="modal" data-target="#modalVerImagemServico4">Ver</button>
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-12">
                                                                                                                    <label class="col-form-label">Titulo/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i></label>
                                                                                                                    <input type="text" id="servico4_imagemSEO" name="servico4_imagemSEO" class="form-control" >
                                                                                                                </div>
                                                                                                            </div>  
                                                                                                            
                                                                                                        </td>
                                                                                                    </tr>


                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Botão <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Se desativado: Não mostrará o botão" data-original-title="Se desativado: Não mostrará o botão"></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico4_botao" name="servico4_botao">
                                                                                                        </td>

                                                                                                    </tr>
                                                                                                    

                                                                                                    <tr>

                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Texto <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Texto dentro do botão" data-original-title="Texto dentro do botão"></i></label>
                                                                                                                    <input type="text" class="form-control" id="servico4_textoBotao" name="servico4_textoBotao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Link <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com" data-original-title="Irá abrir o link em uma nova janela! utilize os exemplos: https://meusite.com ou //meusite.com"></i></label>
                                                                                                                    <input type="text" class="form-control" id="servico4_linkBotao" name="servico4_linkBotao">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do texto </label>
                                                                                                                    <input type="color" class="form-control"id="servico4_corTextoBotao" name="servico4_corTextoBotao" >
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do texto ao passar o mouse</label>
                                                                                                                    <input type="color" class="form-control" id="servico4_corTextoHoverBotao" name="servico4_corTextoHoverBotao">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do botão </label>
                                                                                                                    <input type="color" class="form-control" id="servico4_corFundoBotao" name="servico4_corFundoBotao">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do fundo ao passar o mouse </label>
                                                                                                                    <input type="color" class="form-control" id="servico4_corFundoHoverBotao" name="servico4_corFundoHoverBotao">
                                                                                                                </div>
                                                                                                            </div>

                                                                                                        </td>
                                                                                                    
                                                                                                    </tr>

                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    
                                                                                        <center>
                                                                                            <button type="submit" id="botaoAtualizarServico4" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                                <span class="btn-label"><i class="fe-save"></i></span> Salvar
                                                                                            </button>
                                                                                        </center>

                                                                                    </form>

                                                                                </div>

                                                                                <div class="tab-pane" id="servico5">
                                                                                    
                                                                                <form id="formServico5" enctype="multipart/form-data">
                                                                                        
                                                                                        <input type="hidden" name="idModelo_servico5" id="idModelo_servico5">
                                                                                        <input type="hidden" name="idLandingPage_servico5" id="idLandingPage_servico5">

                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-bordered">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>SESSÃO DE SERVIÇOS 5</th>
                                                                                                        <th>AÇÃO</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                
                                                                                                <tbody>
                                                                                                    
                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                            data-placement="top" title="Ative ou desative o serviço (5) na página, se desativado: o serviço não irá aparece" data-original-title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece"></i>
                                                                                                        </td>

                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico5_ativaDesativa" name="servico5_ativaDesativa">
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                            title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                            data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico5_ConteudoGenerico" name="servico5_ConteudoGenerico">
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        
                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico5_titulo" name="servico5_titulo" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico5_descricao" name="servico5_descricao" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico5_corTitulo" name="servico5_corTitulo">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico5_corDescricao" name="servico5_corDescricao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor de fundo do serviço <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos" data-original-title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico5_corFundoBG" name="servico5_corFundoBG">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor dos textos <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor dos textos dentro de cada comentário" data-original-title="Cor dos textos dentro de cada comentário"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico5_corTextos" name="servico5_corTextos">
                                                                                                                </div>

                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor de fundo das opções de serviços <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor dos textos dentro de cada comentário" data-original-title="Cor dos textos dentro de cada comentário"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico5_corFundoBOXImagem" name="servico5_corFundoBOXImagem">
                                                                                                                </div>
                                                                                                            </div>  
                                                                                                            
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>

                                                                                            <td colspan="2" class="col-12">
                                                                                                
                                                                                                configurações dos botões <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="bottom" title="Aplica-se a todos os botões" data-original-title="Aplica-se a todos os botões"></i>
                                                                                                
                                                                                                <div class="form-row">
                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor do texto </label>
                                                                                                        <input type="color" class="form-control"id="corTextoBotao_servico5" name="corTextoBotao_servico5" >
                                                                                                    </div>

                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor do texto ao passar o mouse</label>
                                                                                                        <input type="color" class="form-control" id="corTextoHoverBotao_servico5" name="corTextoHoverBotao_servico5">
                                                                                                    </div>

                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor do botão </label>
                                                                                                        <input type="color" class="form-control" id="corfundoBotao_servico5" name="corfundoBotao_servico5">
                                                                                                    </div>

                                                                                                    <div class="form-group col-md-6">
                                                                                                        <label class="col-form-label">Cor do ao passar o mouse </label>
                                                                                                        <input type="color" class="form-control" id="corFundoHoverBotao_servico5" name="corFundoHoverBotao_servico5">
                                                                                                    </div>
                                                                                                </div>

                                                                                            </td>
                                                                                            
                                                                                        </tr>

                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-xl-6">
                                                                                                <div id="accordion" class="mb-3">
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico5_1">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico5_1" aria-expanded="false">
                                                                                                                    Opção 1
                                                                                                                    <span class="badge badge-primary">
                                                                                                                        <i class="fe-edit-2"></i>
                                                                                                                    </span>
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico5_opcao1" name="servico5_opcao1">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico5_1" class="collapse" aria-labelledby="heading_servico5_1" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico5_imagem1" class="servico5_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico5_opcao1" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico5_imagem_opcao1" name="servico5_imagem_opcao1" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico5nomeFake_opcao1" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" id="servico5BotaoFake_opcao1">
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Texto </label>
                                                                                                                        <input type="text" id="servico5_texoBotao_opcao1" name="servico5_texoBotao_opcao1" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Link </label>
                                                                                                                        <input type="text" id="servico5_linkBotao_opcao1" name="servico5_linkBotao_opcao1" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        
                                                                                                                        <label class="col-form-label">Exibir? 
                                                                                                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                data-placement="top" title="Mostrar o botão abaixo das informações" 
                                                                                                                                data-original-title="Mostrar o botão abaixo das informações">
                                                                                                                            </i>
                                                                                                                        </label>
                                                                                                                        
                                                                                                                        <br>

                                                                                                                        <input type="checkbox" class="switch_1" id="servico5_botao1" name="servico5_botao1">

                                                                                                                    </div>

                                                                                                                </div>
                                                                                                                
                                                                                                                <div class="col-xl-12">
                                                                                                                
                                                                                                                    <div id="accordion" class="mb-3">
                                                                                                                        <div class="card mb-1">
                                                                                                                            <div class="card-header" id="serv5opc1">
                                                                                                                                <h5 class="m-0">
                                                                                                                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseServ5opc1" aria-expanded="false">
                                                                                                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                                                        Informações importantes!
                                                                                                                                    </a>
                                                                                                                                </h5>
                                                                                                                            </div>
                                                                                                                
                                                                                                                            <div id="collapseServ5opc1" class="collapse" aria-labelledby="serv5opc1" data-parent="#accordion" style="">
                                                                                                                                <div class="card-body">
                                                                                                                                
                                                                                                                                    <div class="input-group mt-2">
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Titulo </label>
                                                                                                                                            <input type="text" id="servico5_titulo_opcao1" name="servico5_titulo_opcao1" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Descrição </label>
                                                                                                                                            <input type="text" id="servico5_descricao_opcao1" name="servico5_descricao_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                                data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                            </label>
                                                                                                                                            <input type="text" id="servico5_SEO_opcao1" name="servico5_SEO_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Preço </label>
                                                                                                                                            <input type="text" id="servico5_preco_opcao1" name="servico5_preco_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                        
                                                                                                                                            <label class="col-form-label">Exibir? 
                                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                    data-placement="top" title="Exibe ou oculta o valor/preço" 
                                                                                                                                                    data-original-title="Exibe ou oculta o valor/preço">
                                                                                                                                                </i>
                                                                                                                                            </label>
                                                                                                                                            
                                                                                                                                            <br>

                                                                                                                                            <input type="checkbox" class="switch_1" id="servico5_preco1_botao1" name="servico5_preco1_botao1">

                                                                                                                                        </div>


                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 1 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao1" name="servico5_descricaoRapida_opcao1_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 2 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao2" name="servico5_descricaoRapida_opcao1_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 3 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao3" name="servico5_descricaoRapida_opcao1_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 4 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao4" name="servico5_descricaoRapida_opcao1_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 5 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao5" name="servico5_descricaoRapida_opcao1_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 6 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao6" name="servico5_descricaoRapida_opcao1_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 7 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao7" name="servico5_descricaoRapida_opcao1_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 8 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao8" name="servico5_descricaoRapida_opcao1_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 9 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao9" name="servico5_descricaoRapida_opcao1_opcao9" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 10 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao1_opcao10" name="servico5_descricaoRapida_opcao1_opcao10" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div> <!-- end #accordions-->
                                                                                                                </div>
                                                                                                            </div>

                                                                                                                <!--FIM ACORDION INFOS 1 -->
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico5_2">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico5_2" aria-expanded="false">
                                                                                                                    Opção 2
                                                                                                                    <span class="badge badge-primary">
                                                                                                                        <i class="fe-edit-2"></i>
                                                                                                                    </span>
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico5_opcao2" name="servico5_opcao2">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico5_2" class="collapse" aria-labelledby="heading_servico5_2" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico5_imagem2" class="servico5_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico5_opcao2" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico5_imagem_opcao2" name="servico5_imagem_opcao2" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico5nomeFake_opcao2" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>
                                                                                                                
                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Texto </label>
                                                                                                                        <input type="text" id="servico5_texoBotao_opcao2" name="servico5_texoBotao_opcao2" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Link </label>
                                                                                                                        <input type="text" id="servico5_linkBotao_opcao2" name="servico5_linkBotao_opcao2" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        
                                                                                                                        <label class="col-form-label">Exibir? 
                                                                                                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                data-placement="top" title="Mostrar o botão abaixo das informações" 
                                                                                                                                data-original-title="Mostrar o botão abaixo das informações">
                                                                                                                            </i>
                                                                                                                        </label>
                                                                                                                        
                                                                                                                        <br>

                                                                                                                        <input type="checkbox" class="switch_1" id="servico5_botao2" name="servico5_botao2">

                                                                                                                    </div>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="col-xl-12">
                                                                                                                    <div id="accordion" class="mb-3">
                                                                                                                        <div class="card mb-1">
                                                                                                                            <div class="card-header" id="serv5opc2">
                                                                                                                                <h5 class="m-0">
                                                                                                                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseServ5opc2" aria-expanded="false">
                                                                                                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                                                        Informações importantes!
                                                                                                                                    </a>
                                                                                                                                </h5>
                                                                                                                            </div>
                                                                                                                
                                                                                                                            <div id="collapseServ5opc2" class="collapse" aria-labelledby="serv5opc2" data-parent="#accordion" style="">
                                                                                                                                <div class="card-body">
                                                                                                                                
                                                                                                                                    <div class="input-group mt-2">
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Titulo </label>
                                                                                                                                            <input type="text" id="servico5_titulo_opcao2" name="servico5_titulo_opcao2" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Descrição </label>
                                                                                                                                            <input type="text" id="servico5_descricao_opcao2" name="servico5_descricao_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                                data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                            </label>
                                                                                                                                            <input type="text" id="servico5_SEO_opcao2" name="servico5_SEO_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Preço </label>
                                                                                                                                            <input type="text" id="servico5_preco_opcao2" name="servico5_preco_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                        
                                                                                                                                            <label class="col-form-label">Exibir? 
                                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                    data-placement="top" title="Exibe ou oculta o valor/preço" 
                                                                                                                                                    data-original-title="Exibe ou oculta o valor/preço">
                                                                                                                                                </i>
                                                                                                                                            </label>
                                                                                                                                            
                                                                                                                                            <br>

                                                                                                                                            <input type="checkbox" class="switch_1" id="servico5_preco2_botao2" name="servico5_preco2_botao2">

                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 1 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao1" name="servico5_descricaoRapida_opcao2_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 2 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao2" name="servico5_descricaoRapida_opcao2_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 3 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao3" name="servico5_descricaoRapida_opcao2_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 4 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao4" name="servico5_descricaoRapida_opcao2_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 5 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao5" name="servico5_descricaoRapida_opcao2_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 6 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao6" name="servico5_descricaoRapida_opcao2_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 7 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao7" name="servico5_descricaoRapida_opcao2_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 8 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao8" name="servico5_descricaoRapida_opcao2_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 9 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao9" name="servico5_descricaoRapida_opcao2_opcao9" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 10 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao2_opcao10" name="servico5_descricaoRapida_opcao2_opcao10" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div> <!-- end #accordions-->
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico5_3">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico5_3" aria-expanded="false">
                                                                                                                    Opção 3
                                                                                                                    <span class="badge badge-primary">
                                                                                                                        <i class="fe-edit-2"></i>
                                                                                                                    </span>
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico5_opcao3" name="servico5_opcao3">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico5_3" class="collapse" aria-labelledby="heading_servico5_3" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico5_imagem3" class="servico5_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico5_opcao3" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico5_imagem_opcao3" name="servico5_imagem_opcao3" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico5nomeFake_opcao3" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Texto </label>
                                                                                                                        <input type="text" id="servico5_texoBotao_opcao3" name="servico5_texoBotao_opcao3" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Link </label>
                                                                                                                        <input type="text" id="servico5_linkBotao_opcao3" name="servico5_linkBotao_opcao3" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        
                                                                                                                        <label class="col-form-label">Exibir? 
                                                                                                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                data-placement="top" title="Mostrar o botão abaixo das informações" 
                                                                                                                                data-original-title="Mostrar o botão abaixo das informações">
                                                                                                                            </i>
                                                                                                                        </label>
                                                                                                                        
                                                                                                                        <br>

                                                                                                                        <input type="checkbox" class="switch_1" id="servico5_botao3" name="servico5_botao3">

                                                                                                                    </div>

                                                                                                                </div>

                                                                                                                <div class="col-xl-12">
                                                                                                                    <div id="accordion" class="mb-3">
                                                                                                                        <div class="card mb-1">
                                                                                                                            <div class="card-header" id="serv5opc3">
                                                                                                                                <h5 class="m-0">
                                                                                                                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseServ5opc3" aria-expanded="false">
                                                                                                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                                                        Informações importantes!
                                                                                                                                    </a>
                                                                                                                                </h5>
                                                                                                                            </div>
                                                                                                                
                                                                                                                            <div id="collapseServ5opc3" class="collapse" aria-labelledby="serv5opc3" data-parent="#accordion">
                                                                                                                                <div class="card-body">
                                                                                                                                
                                                                                                                                    <div class="input-group mt-2">
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Titulo </label>
                                                                                                                                            <input type="text" id="servico5_titulo_opcao3" name="servico5_titulo_opcao3" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Descrição </label>
                                                                                                                                            <input type="text" id="servico5_descricao_opcao3" name="servico5_descricao_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                                data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                            </label>
                                                                                                                                            <input type="text" id="servico5_SEO_opcao3" name="servico5_SEO_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Preço </label>
                                                                                                                                            <input type="text" id="servico5_preco_opcao3" name="servico5_preco_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                        
                                                                                                                                            <label class="col-form-label">Exibir? 
                                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                    data-placement="top" title="Exibe ou oculta o valor/preço" 
                                                                                                                                                    data-original-title="Exibe ou oculta o valor/preço">
                                                                                                                                                </i>
                                                                                                                                            </label>
                                                                                                                                            
                                                                                                                                            <br>

                                                                                                                                            <input type="checkbox" class="switch_1" id="servico5_preco3_botao3" name="servico5_preco3_botao3">

                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 1 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao1" name="servico5_descricaoRapida_opcao3_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 2 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao2" name="servico5_descricaoRapida_opcao3_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 3 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao3" name="servico5_descricaoRapida_opcao3_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 4 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao4" name="servico5_descricaoRapida_opcao3_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 5 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao5" name="servico5_descricaoRapida_opcao3_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 6 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao6" name="servico5_descricaoRapida_opcao3_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 7 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao7" name="servico5_descricaoRapida_opcao3_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 8 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao8" name="servico5_descricaoRapida_opcao3_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 9 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao9" name="servico5_descricaoRapida_opcao3_opcao9" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 10 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao3_opcao10" name="servico5_descricaoRapida_opcao3_opcao10" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div> <!-- end #accordions-->
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico5_4">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico5_4" aria-expanded="false">
                                                                                                                    Opção 4
                                                                                                                    <span class="badge badge-primary">
                                                                                                                        <i class="fe-edit-2"></i>
                                                                                                                    </span>
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico5_opcao4" name="servico5_opcao4">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico5_4" class="collapse" aria-labelledby="heading_servico5_4" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico5_imagem4" class="servico5_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico5_opcao4" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico5_imagem_opcao4" name="servico5_imagem_opcao4" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico5nomeFake_opcao4" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Texto </label>
                                                                                                                        <input type="text" id="servico5_texoBotao_opcao4" name="servico5_texoBotao_opcao4" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Link </label>
                                                                                                                        <input type="text" id="servico5_linkBotao_opcao4" name="servico5_linkBotao_opcao4" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        
                                                                                                                        <label class="col-form-label">Exibir? 
                                                                                                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                data-placement="top" title="Mostrar o botão abaixo das informações" 
                                                                                                                                data-original-title="Mostrar o botão abaixo das informações">
                                                                                                                            </i>
                                                                                                                        </label>
                                                                                                                        
                                                                                                                        <br>

                                                                                                                        <input type="checkbox" class="switch_1" id="servico5_botao4" name="servico5_botao4">

                                                                                                                    </div>

                                                                                                                </div>

                                                                                                                <div class="col-xl-12">
                                                                                                                
                                                                                                                    <div id="accordion" class="mb-3">
                                                                                                                        <div class="card mb-1">
                                                                                                                            <div class="card-header" id="serv5opc4">
                                                                                                                                <h5 class="m-0">
                                                                                                                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseServ5opc4" aria-expanded="false">
                                                                                                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                                                        Informações importantes!
                                                                                                                                    </a>
                                                                                                                                </h5>
                                                                                                                            </div>
                                                                                                                
                                                                                                                            <div id="collapseServ5opc4" class="collapse" aria-labelledby="serv5opc4" data-parent="#accordion" style="">
                                                                                                                                <div class="card-body">
                                                                                                                                
                                                                                                                                    <div class="input-group mt-2">
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Titulo </label>
                                                                                                                                            <input type="text" id="servico5_titulo_opcao4" name="servico5_titulo_opcao4" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Descrição </label>
                                                                                                                                            <input type="text" id="servico5_descricao_opcao4" name="servico5_descricao_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                                data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                            </label>
                                                                                                                                            <input type="text" id="servico5_SEO_opcao4" name="servico5_SEO_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Preço </label>
                                                                                                                                            <input type="text" id="servico5_preco_opcao4" name="servico5_preco_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                        
                                                                                                                                            <label class="col-form-label">Exibir? 
                                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                    data-placement="top" title="Exibe ou oculta o valor/preço" 
                                                                                                                                                    data-original-title="Exibe ou oculta o valor/preço">
                                                                                                                                                </i>
                                                                                                                                            </label>
                                                                                                                                            
                                                                                                                                            <br>

                                                                                                                                            <input type="checkbox" class="switch_1" id="servico5_preco4_botao4" name="servico5_preco4_botao4">

                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 1 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao1" name="servico5_descricaoRapida_opcao4_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 2 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao2" name="servico5_descricaoRapida_opcao4_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 3 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao3" name="servico5_descricaoRapida_opcao4_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 4 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao4" name="servico5_descricaoRapida_opcao4_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 5 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao5" name="servico5_descricaoRapida_opcao4_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 6 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao6" name="servico5_descricaoRapida_opcao4_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 7 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao7" name="servico5_descricaoRapida_opcao4_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 8 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao8" name="servico5_descricaoRapida_opcao4_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 9 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao9" name="servico5_descricaoRapida_opcao4_opcao9" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 10 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao4_opcao10" name="servico5_descricaoRapida_opcao4_opcao10" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div> <!-- end #accordions-->
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-xl-6">

                                                                                            <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico5_5">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico5_5" aria-expanded="false">
                                                                                                                    Opção 5
                                                                                                                    <span class="badge badge-primary">
                                                                                                                        <i class="fe-edit-2"></i>
                                                                                                                    </span>
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico5_opcao5" name="servico5_opcao5">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico5_5" class="collapse" aria-labelledby="heading_servico5_5" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico5_imagem5" class="servico5_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico5_opcao5" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico5_imagem_opcao5" name="servico5_imagem_opcao5" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico5nomeFake_opcao5" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Texto </label>
                                                                                                                        <input type="text" id="servico5_texoBotao_opcao5" name="servico5_texoBotao_opcao5" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Link </label>
                                                                                                                        <input type="text" id="servico5_linkBotao_opcao5" name="servico5_linkBotao_opcao5" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        
                                                                                                                        <label class="col-form-label">Exibir? 
                                                                                                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                data-placement="top" title="Mostrar o botão abaixo das informações" 
                                                                                                                                data-original-title="Mostrar o botão abaixo das informações">
                                                                                                                            </i>
                                                                                                                        </label>
                                                                                                                        
                                                                                                                        <br>

                                                                                                                        <input type="checkbox" class="switch_1" id="servico5_botao5" name="servico5_botao5">

                                                                                                                    </div>

                                                                                                                </div>

                                                                                                                <div class="col-xl-12">
                                                                                                                
                                                                                                                    <div id="accordion" class="mb-3">
                                                                                                                        <div class="card mb-1">
                                                                                                                            <div class="card-header" id="serv5opc5">
                                                                                                                                <h5 class="m-0">
                                                                                                                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseServ5opc5" aria-expanded="false">
                                                                                                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                                                        Informações importantes!
                                                                                                                                    </a>
                                                                                                                                </h5>
                                                                                                                            </div>
                                                                                                                
                                                                                                                            <div id="collapseServ5opc5" class="collapse" aria-labelledby="serv5opc5" data-parent="#accordion" style="">
                                                                                                                                <div class="card-body">
                                                                                                                                
                                                                                                                                    <div class="input-group mt-2">
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Titulo </label>
                                                                                                                                            <input type="text" id="servico5_titulo_opcao5" name="servico5_titulo_opcao5" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Descrição </label>
                                                                                                                                            <input type="text" id="servico5_descricao_opcao5" name="servico5_descricao_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                                data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                            </label>
                                                                                                                                            <input type="text" id="servico5_SEO_opcao5" name="servico5_SEO_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Preço </label>
                                                                                                                                            <input type="text" id="servico5_preco_opcao5" name="servico5_preco_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                        
                                                                                                                                            <label class="col-form-label">Exibir? 
                                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                    data-placement="top" title="Exibe ou oculta o valor/preço" 
                                                                                                                                                    data-original-title="Exibe ou oculta o valor/preço">
                                                                                                                                                </i>
                                                                                                                                            </label>
                                                                                                                                            
                                                                                                                                            <br>

                                                                                                                                            <input type="checkbox" class="switch_1" id="servico5_preco5_botao5" name="servico5_preco5_botao5">

                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 1 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao1" name="servico5_descricaoRapida_opcao5_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 2 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao2" name="servico5_descricaoRapida_opcao5_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 3 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao3" name="servico5_descricaoRapida_opcao5_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 4 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao4" name="servico5_descricaoRapida_opcao5_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 5 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao5" name="servico5_descricaoRapida_opcao5_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 6 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao6" name="servico5_descricaoRapida_opcao5_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 7 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao7" name="servico5_descricaoRapida_opcao5_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 8 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao8" name="servico5_descricaoRapida_opcao5_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 9 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao9" name="servico5_descricaoRapida_opcao5_opcao9" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 10 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao5_opcao10" name="servico5_descricaoRapida_opcao5_opcao10" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div> <!-- end #accordions-->
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                
                                                                                                <div id="accordion" class="mb-3">
                                                                                                
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico5_6">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico5_6" aria-expanded="false">
                                                                                                                    Opção 6
                                                                                                                    <span class="badge badge-primary">
                                                                                                                        <i class="fe-edit-2"></i>
                                                                                                                    </span>
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico5_opcao6" name="servico5_opcao6">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico5_6" class="collapse" aria-labelledby="heading_servico5_6" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico5_imagem6" class="servico5_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico5_opcao6" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico5_imagem_opcao6" name="servico5_imagem_opcao6" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico5nomeFake_opcao6" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" id="servico5BotaoFake_opcao6">
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Texto </label>
                                                                                                                        <input type="text" id="servico5_texoBotao_opcao6" name="servico5_texoBotao_opcao6" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Link </label>
                                                                                                                        <input type="text" id="servico5_linkBotao_opcao6" name="servico5_linkBotao_opcao6" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        
                                                                                                                        <label class="col-form-label">Exibir? 
                                                                                                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                data-placement="top" title="Mostrar o botão abaixo das informações" 
                                                                                                                                data-original-title="Mostrar o botão abaixo das informações">
                                                                                                                            </i>
                                                                                                                        </label>
                                                                                                                        
                                                                                                                        <br>

                                                                                                                        <input type="checkbox" class="switch_1" id="servico5_botao6" name="servico5_botao6">

                                                                                                                    </div>

                                                                                                                </div>

                                                                                                                <div class="col-xl-12">
                                                                                                                
                                                                                                                    <div id="accordion" class="mb-3">
                                                                                                                        <div class="card mb-1">
                                                                                                                            <div class="card-header" id="serv5opc6">
                                                                                                                                <h5 class="m-0">
                                                                                                                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseServ5opc6" aria-expanded="false">
                                                                                                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                                                        Informações importantes!
                                                                                                                                    </a>
                                                                                                                                </h5>
                                                                                                                            </div>
                                                                                                                
                                                                                                                            <div id="collapseServ5opc6" class="collapse" aria-labelledby="serv5opc6" data-parent="#accordion" style="">
                                                                                                                                <div class="card-body">
                                                                                                                                
                                                                                                                                    <div class="input-group mt-2">
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Titulo </label>
                                                                                                                                            <input type="text" id="servico5_titulo_opcao6" name="servico5_titulo_opcao6" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Descrição </label>
                                                                                                                                            <input type="text" id="servico5_descricao_opcao6" name="servico5_descricao_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                                data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                            </label>
                                                                                                                                            <input type="text" id="servico5_SEO_opcao6" name="servico5_SEO_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Preço </label>
                                                                                                                                            <input type="text" id="servico5_preco_opcao6" name="servico5_preco_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                        
                                                                                                                                            <label class="col-form-label">Exibir? 
                                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                    data-placement="top" title="Exibe ou oculta o valor/preço" 
                                                                                                                                                    data-original-title="Exibe ou oculta o valor/preço">
                                                                                                                                                </i>
                                                                                                                                            </label>
                                                                                                                                            
                                                                                                                                            <br>

                                                                                                                                            <input type="checkbox" class="switch_1" id="servico5_preco6_botao6" name="servico5_preco6_botao6">

                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 1 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao1" name="servico5_descricaoRapida_opcao6_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 2 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao2" name="servico5_descricaoRapida_opcao6_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 3 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao3" name="servico5_descricaoRapida_opcao6_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 4 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao4" name="servico5_descricaoRapida_opcao6_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 5 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao5" name="servico5_descricaoRapida_opcao6_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 6 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao6" name="servico5_descricaoRapida_opcao6_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 7 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao7" name="servico5_descricaoRapida_opcao6_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 8 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao8" name="servico5_descricaoRapida_opcao6_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 9 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao9" name="servico5_descricaoRapida_opcao6_opcao9" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 10 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao6_opcao10" name="servico5_descricaoRapida_opcao6_opcao10" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div> <!-- end #accordions-->
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico5_7">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico5_7" aria-expanded="false">
                                                                                                                    Opção 7
                                                                                                                    <span class="badge badge-primary">
                                                                                                                        <i class="fe-edit-2"></i>
                                                                                                                    </span>
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico5_opcao7" name="servico5_opcao7">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico5_7" class="collapse" aria-labelledby="heading_servico5_7" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico5_imagem7" class="servico5_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico5_opcao7" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico5_imagem_opcao7" name="servico5_imagem_opcao7" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico5nomeFake_opcao7" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Texto </label>
                                                                                                                        <input type="text" id="servico5_texoBotao_opcao7" name="servico5_texoBotao_opcao7" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Link </label>
                                                                                                                        <input type="text" id="servico5_linkBotao_opcao7" name="servico5_linkBotao_opcao7" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        
                                                                                                                        <label class="col-form-label">Exibir? 
                                                                                                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                data-placement="top" title="Mostrar o botão abaixo das informações" 
                                                                                                                                data-original-title="Mostrar o botão abaixo das informações">
                                                                                                                            </i>
                                                                                                                        </label>
                                                                                                                        
                                                                                                                        <br>

                                                                                                                        <input type="checkbox" class="switch_1" id="servico5_botao7" name="servico5_botao7">

                                                                                                                    </div>

                                                                                                                </div>

                                                                                                                <div class="col-xl-12">
                                                                                                                
                                                                                                                    <div id="accordion" class="mb-3">
                                                                                                                        <div class="card mb-1">
                                                                                                                            <div class="card-header" id="serv5opc7">
                                                                                                                                <h5 class="m-0">
                                                                                                                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseServ5opc7" aria-expanded="false">
                                                                                                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                                                        Informações importantes!
                                                                                                                                    </a>
                                                                                                                                </h5>
                                                                                                                            </div>
                                                                                                                
                                                                                                                            <div id="collapseServ5opc7" class="collapse" aria-labelledby="serv5opc7" data-parent="#accordion" style="">
                                                                                                                                <div class="card-body">
                                                                                                                                
                                                                                                                                    <div class="input-group mt-2">
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Titulo </label>
                                                                                                                                            <input type="text" id="servico5_titulo_opcao7" name="servico5_titulo_opcao7" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Descrição </label>
                                                                                                                                            <input type="text" id="servico5_descricao_opcao7" name="servico5_descricao_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                                data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                            </label>
                                                                                                                                            <input type="text" id="servico5_SEO_opcao7" name="servico5_SEO_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Preço </label>
                                                                                                                                            <input type="text" id="servico5_preco_opcao7" name="servico5_preco_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                        
                                                                                                                                            <label class="col-form-label">Exibir? 
                                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                    data-placement="top" title="Exibe ou oculta o valor/preço" 
                                                                                                                                                    data-original-title="Exibe ou oculta o valor/preço">
                                                                                                                                                </i>
                                                                                                                                            </label>
                                                                                                                                            
                                                                                                                                            <br>

                                                                                                                                            <input type="checkbox" class="switch_1" id="servico5_preco7_botao7" name="servico5_preco7_botao7">

                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 1 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao1" name="servico5_descricaoRapida_opcao7_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 2 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao2" name="servico5_descricaoRapida_opcao7_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 3 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao3" name="servico5_descricaoRapida_opcao7_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 4 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao4" name="servico5_descricaoRapida_opcao7_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 5 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao5" name="servico5_descricaoRapida_opcao7_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 6 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao6" name="servico5_descricaoRapida_opcao7_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 7 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao7" name="servico5_descricaoRapida_opcao7_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 8 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao8" name="servico5_descricaoRapida_opcao7_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 9 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao9" name="servico5_descricaoRapida_opcao7_opcao9" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 10 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao7_opcao10" name="servico5_descricaoRapida_opcao7_opcao10" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div> <!-- end #accordions-->
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                    
                                                                                                        <div class="card-header" id="heading_servico5_8">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico5_8" aria-expanded="false">
                                                                                                                    Opção 8 
                                                                                                                    <span class="badge badge-primary">
                                                                                                                        <i class="fe-edit-2"></i>
                                                                                                                    </span>
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico5_opcao8" name="servico5_opcao8">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico5_8" class="collapse" aria-labelledby="heading_servico5_8" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico5_imagem8" class="servico5_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico5_opcao8" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico5_imagem_opcao8" name="servico5_imagem_opcao8" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico5nomeFake_opcao8" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Texto </label>
                                                                                                                        <input type="text" id="servico5_texoBotao_opcao8" name="servico5_texoBotao_opcao8" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        <label class="col-form-label">Link </label>
                                                                                                                        <input type="text" id="servico5_linkBotao_opcao8" name="servico5_linkBotao_opcao8" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-4">
                                                                                                                        
                                                                                                                        <label class="col-form-label">Exibir? 
                                                                                                                            <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                data-placement="top" title="Mostrar o botão abaixo das informações" 
                                                                                                                                data-original-title="Mostrar o botão abaixo das informações">
                                                                                                                            </i>
                                                                                                                        </label>
                                                                                                                        
                                                                                                                        <br>

                                                                                                                        <input type="checkbox" class="switch_1" id="servico5_botao8" name="servico5_botao8">

                                                                                                                    </div>

                                                                                                                </div>

                                                                                                                <div class="col-xl-12">
                                                                                                                
                                                                                                                    <div id="accordion" class="mb-3">
                                                                                                                        <div class="card mb-1">
                                                                                                                            <div class="card-header" id="serv5opc8">
                                                                                                                                <h5 class="m-0">
                                                                                                                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseServ5opc8" aria-expanded="false">
                                                                                                                                        <i class="mdi mdi-help-circle mr-1 text-primary"></i> 
                                                                                                                                        Informações importantes!
                                                                                                                                    </a>
                                                                                                                                </h5>
                                                                                                                            </div>
                                                                                                                
                                                                                                                            <div id="collapseServ5opc8" class="collapse" aria-labelledby="serv5opc8" data-parent="#accordion" style="">
                                                                                                                                <div class="card-body">
                                                                                                                                
                                                                                                                                    <div class="input-group mt-2">
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Titulo </label>
                                                                                                                                            <input type="text" id="servico5_titulo_opcao8" name="servico5_titulo_opcao8" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Descrição </label>
                                                                                                                                            <input type="text" id="servico5_descricao_opcao8" name="servico5_descricao_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                                                data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                                            </label>
                                                                                                                                            <input type="text" id="servico5_SEO_opcao8" name="servico5_SEO_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                                            <label class="col-form-label">Preço </label>
                                                                                                                                            <input type="text" id="servico5_preco_opcao8" name="servico5_preco_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-6">
                                                                                                                        
                                                                                                                                            <label class="col-form-label">Exibir? 
                                                                                                                                                <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                                                    data-placement="top" title="Exibe ou oculta o valor/preço" 
                                                                                                                                                    data-original-title="Exibe ou oculta o valor/preço">
                                                                                                                                                </i>
                                                                                                                                            </label>
                                                                                                                                            
                                                                                                                                            <br>

                                                                                                                                            <input type="checkbox" class="switch_1" id="servico5_preco8_botao8" name="servico5_preco8_botao8">

                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 1 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao1" name="servico5_descricaoRapida_opcao8_opcao1" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 2 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao2" name="servico5_descricaoRapida_opcao8_opcao2" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 3 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao3" name="servico5_descricaoRapida_opcao8_opcao3" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 4 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao4" name="servico5_descricaoRapida_opcao8_opcao4" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 5 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao5" name="servico5_descricaoRapida_opcao8_opcao5" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 6 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao6" name="servico5_descricaoRapida_opcao8_opcao6" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 7 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao7" name="servico5_descricaoRapida_opcao8_opcao7" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 8 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao8" name="servico5_descricaoRapida_opcao8_opcao8" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 9 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao9" name="servico5_descricaoRapida_opcao8_opcao9" class="form-control" >
                                                                                                                                        </div>

                                                                                                                                        <div class="form-group col-md-12">
                                                                                                                                            <label class="col-form-label">Descrição rápida- Opção 10 </label>
                                                                                                                                            <input type="text" id="servico5_descricaoRapida_opcao8_opcao10" name="servico5_descricaoRapida_opcao8_opcao10" class="form-control" >
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div> <!-- end #accordions-->
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    
                                                                                        <center>
                                                                                            <button type="submit" id="botaoAtualizarServico5" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                                <span class="btn-label"><i class="fe-save"></i></span> Salvar
                                                                                            </button>
                                                                                        </center>

                                                                                    </form>

                                                                                </div>

                                                                                <div class="tab-pane" id="servico6">
                                                                                    
                                                                                    <form id="formServico6" enctype="multipart/form-data">
                                                                                        
                                                                                        <input type="hidden" name="idModelo_servico6" id="idModelo_servico6">
                                                                                        <input type="hidden" name="idLandingPage_servico6" id="idLandingPage_servico6">

                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-bordered">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>SESSÃO DE SERVIÇOS 6</th>
                                                                                                        <th>AÇÃO</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                
                                                                                                <tbody>
                                                                                                    
                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                            data-placement="top" title="Ative ou desative o serviço (6) na página, se desativado: o serviço não irá aparece" data-original-title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece"></i>
                                                                                                        </td>

                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico6_ativaDesativa" name="servico6_ativaDesativa">
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                            title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                            data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico6_ConteudoGenerico" name="servico6_ConteudoGenerico">
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        
                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico6_titulo" name="servico6_titulo" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico6_descricao" name="servico6_descricao" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-3">
                                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico6_corTitulo" name="servico6_corTitulo">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-3">
                                                                                                                    <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico6_corDescricao" name="servico6_corDescricao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-3">
                                                                                                                    <label class="col-form-label">Cor de fundo do serviço <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos" data-original-title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico6_corFundoBG" name="servico6_corFundoBG">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-3">
                                                                                                                    <label class="col-form-label">Cor dos textos <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor dos textos dentro de cada comentário" data-original-title="Cor dos textos dentro de cada comentário"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico6_corTextos" name="servico6_corTextos">
                                                                                                                </div>
                                                                                                            </div>  
                                                                                                            
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-xl-6">
                                                                                                <div id="accordion" class="mb-3">
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico6_1">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico6_1" aria-expanded="false">
                                                                                                                    Opção 1
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico6_opcao1" name="servico6_opcao1">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico6_1" class="collapse" aria-labelledby="heading_servico6_1" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico6_imagem1" class="servico6_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico6_opcao1" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico6_imagem_opcao1" name="servico6_imagem_opcao1" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico6nomeFake_opcao1" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" id="servico6BotaoFake_opcao1">
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico6_SEO_opcao1" name="servico6_SEO_opcao1" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico6_titulo_opcao1" name="servico6_titulo_opcao1" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico6_descricao_opcao1" name="servico6_descricao_opcao1" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico6_2">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico6_2" aria-expanded="false">
                                                                                                                    Opção 2
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico6_opcao2" name="servico6_opcao2">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico6_2" class="collapse" aria-labelledby="heading_servico6_2" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico6_imagem2" class="servico6_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico6_opcao2" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico6_imagem_opcao2" name="servico6_imagem_opcao2" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico6nomeFake_opcao2" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico6_SEO_opcao2" name="servico6_SEO_opcao2" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico6_titulo_opcao2" name="servico6_titulo_opcao2" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico6_descricao_opcao2" name="servico6_descricao_opcao2" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico6_3">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico6_3" aria-expanded="false">
                                                                                                                    Opção 3
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico6_opcao3" name="servico6_opcao3">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico6_3" class="collapse" aria-labelledby="heading_servico6_3" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico6_imagem3" class="servico6_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico6_opcao3" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico6_imagem_opcao3" name="servico6_imagem_opcao3" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico6nomeFake_opcao3" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico6_SEO_opcao3" name="servico6_SEO_opcao3" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico6_titulo_opcao3" name="servico6_titulo_opcao3" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico6_descricao_opcao3" name="servico6_descricao_opcao3" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico6_4">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico6_4" aria-expanded="false">
                                                                                                                    Opção 4
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico6_opcao4" name="servico6_opcao4">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico6_4" class="collapse" aria-labelledby="heading_servico6_4" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico6_imagem4" class="servico6_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico6_opcao4" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico6_imagem_opcao4" name="servico6_imagem_opcao4" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico6nomeFake_opcao4" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico6_SEO_opcao4" name="servico6_SEO_opcao4" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico6_titulo_opcao4" name="servico6_titulo_opcao4" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico6_descricao_opcao4" name="servico6_descricao_opcao4" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-xl-6">
                                                                                                
                                                                                                <div id="accordion" class="mb-3">
                                                                                                    
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico6_5">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico6_5" aria-expanded="false">
                                                                                                                    Opção 5
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico6_opcao5" name="servico6_opcao5">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico6_5" class="collapse" aria-labelledby="heading_servico6_5" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico6_imagem5" class="servico6_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico6_opcao5" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico6_imagem_opcao5" name="servico6_imagem_opcao5" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico6nomeFake_opcao5" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico6_SEO_opcao5" name="servico6_SEO_opcao5" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico6_titulo_opcao5" name="servico6_titulo_opcao5" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico6_descricao_opcao5" name="servico6_descricao_opcao5" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico6_6">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico6_6" aria-expanded="false">
                                                                                                                    Opção 6
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico6_opcao6" name="servico6_opcao6">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico6_6" class="collapse" aria-labelledby="heading_servico6_6" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico6_imagem6" class="servico6_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico6_opcao6" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico6_imagem_opcao6" name="servico6_imagem_opcao6" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico6nomeFake_opcao6" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico6_SEO_opcao6" name="servico6_SEO_opcao6" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico6_titulo_opcao6" name="servico6_titulo_opcao6" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico6_descricao_opcao6" name="servico6_descricao_opcao6" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico6_7">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico6_7" aria-expanded="false">
                                                                                                                    Opção 7
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico6_opcao7" name="servico6_opcao7">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico6_7" class="collapse" aria-labelledby="heading_servico6_7" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico6_imagem7" class="servico6_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico6_opcao7" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico6_imagem_opcao7" name="servico6_imagem_opcao7" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico6nomeFake_opcao7" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico6_SEO_opcao7" name="servico6_SEO_opcao7" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico6_titulo_opcao7" name="servico6_titulo_opcao7" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico6_descricao_opcao7" name="servico6_descricao_opcao7" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico6_8">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico6_8" aria-expanded="false">
                                                                                                                    Opção 8
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico6_opcao8" name="servico6_opcao8">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico6_8" class="collapse" aria-labelledby="heading_servico6_8" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico6_imagem8" class="servico6_imagem">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico6_opcao8" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico6_imagem_opcao8" name="servico6_imagem_opcao8" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico6nomeFake_opcao8" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico6_SEO_opcao8" name="servico6_SEO_opcao8" class="form-control" >
                                                                                                                    </div>

                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Titulo </label>
                                                                                                                        <input type="text" id="servico6_titulo_opcao8" name="servico6_titulo_opcao8" class="form-control" >
                                                                                                                    </div>
                                                                                                                    
                                                                                                                    <div class="form-group col-md-6">
                                                                                                                        <label class="col-form-label">Descrição </label>
                                                                                                                        <input type="text" id="servico6_descricao_opcao8" name="servico6_descricao_opcao8" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    
                                                                                        <center>
                                                                                            <button type="submit" id="botaoAtualizarServico6" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                                <span class="btn-label"><i class="fe-save"></i></span> Salvar
                                                                                            </button>
                                                                                        </center>

                                                                                    </form>

                                                                                </div>

                                                                                <div class="tab-pane" id="servico7">
                                                                                    
                                                                                <form id="formServico7" enctype="multipart/form-data">
                                                                                        
                                                                                        <input type="hidden" name="idModelo_servico7" id="idModelo_servico7">
                                                                                        <input type="hidden" name="idLandingPage_servico7" id="idLandingPage_servico7">

                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-bordered">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>SESSÃO DE SERVIÇOS 7</th>
                                                                                                        <th>AÇÃO</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                
                                                                                                <tbody>
                                                                                                    
                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                            data-placement="top" title="Ative ou desative o serviço (7) na página, se desativado: o serviço não irá aparece" data-original-title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece"></i>
                                                                                                        </td>

                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico7_ativaDesativa" name="servico7_ativaDesativa">
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                            title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                            data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico7_ConteudoGenerico" name="servico7_ConteudoGenerico">
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        
                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico7_titulo" name="servico7_titulo" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico7_descricao" name="servico7_descricao" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico7_corTitulo" name="servico7_corTitulo">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico7_corDescricao" name="servico7_corDescricao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor de fundo do serviço <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos" data-original-title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico7_corFundoBG" name="servico7_corFundoBG">
                                                                                                                </div>
                                                                                                            </div>  
                                                                                                            
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>

                                                                                        <div class="row">
                                                                                            <div class="col-xl-6">
                                                                                                <div id="accordion" class="mb-3">
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico7_1">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico7_1" aria-expanded="false">
                                                                                                                    Opção 1
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico7_opcao1" name="servico7_opcao1">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico7_1" class="collapse" aria-labelledby="heading_servico7_1" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico7_imagem1" class="">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico7_opcao1" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico7_imagem_opcao1" name="servico7_imagem_opcao1" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico7nomeFake_opcao1" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" id="servico7BotaoFake_opcao1">
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico7_SEO_opcao1" name="servico7_SEO_opcao1" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico7_2">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico7_2" aria-expanded="false">
                                                                                                                    Opção 2
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico7_opcao2" name="servico7_opcao2">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico7_2" class="collapse" aria-labelledby="heading_servico7_2" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico7_imagem2" class="">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico7_opcao2" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico7_imagem_opcao2" name="servico7_imagem_opcao2" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico7nomeFake_opcao2" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" id="servico7BotaoFake_opcao2">
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico7_SEO_opcao2" name="servico7_SEO_opcao2" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico7_3">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico7_3" aria-expanded="false">
                                                                                                                    Opção 3
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico7_opcao3" name="servico7_opcao3">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico7_3" class="collapse" aria-labelledby="heading_servico7_3" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico7_imagem3" class="">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico7_opcao3" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico7_imagem_opcao3" name="servico7_imagem_opcao3" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico7nomeFake_opcao3" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" id="servico7BotaoFake_opcao3">
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico7_SEO_opcao3" name="servico7_SEO_opcao3" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico7_4">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico7_4" aria-expanded="false">
                                                                                                                    Opção 4
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico7_opcao4" name="servico7_opcao4">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico7_4" class="collapse" aria-labelledby="heading_servico7_4" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico7_imagem4" class="">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico7_opcao4" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico7_imagem_opcao4" name="servico7_imagem_opcao4" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico7nomeFake_opcao4" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico7_SEO_opcao4" name="servico7_SEO_opcao4" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="col-xl-6">
                                                                                                
                                                                                                <div id="accordion" class="mb-3">
                                                                                                    
                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico7_5">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico7_5" aria-expanded="false">
                                                                                                                    Opção 5
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico7_opcao5" name="servico7_opcao5">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico7_5" class="collapse" aria-labelledby="heading_servico7_5" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico7_imagem5" class="">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico7_opcao5" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico7_imagem_opcao5" name="servico7_imagem_opcao5" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico7nomeFake_opcao5" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico7_SEO_opcao5" name="servico7_SEO_opcao5" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico7_6">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico7_6" aria-expanded="false">
                                                                                                                    Opção 6
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico7_opcao6" name="servico7_opcao6">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico7_6" class="collapse" aria-labelledby="heading_servico7_6" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico7_imagem6" class="">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico7_opcao6" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico7_imagem_opcao6" name="servico7_imagem_opcao6" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico7nomeFake_opcao6" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico7_SEO_opcao6" name="servico7_SEO_opcao6" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico7_7">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico7_7" aria-expanded="false">
                                                                                                                    Opção 7
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico7_opcao7" name="servico7_opcao7">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico7_7" class="collapse" aria-labelledby="heading_servico7_7" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico7_imagem7" class="">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico7_opcao7" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico7_imagem_opcao7" name="servico7_imagem_opcao7" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico7nomeFake_opcao7" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico7_SEO_opcao7" name="servico7_SEO_opcao7" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="card mb-1">
                                                                                                        <div class="card-header" id="heading_servico7_8">
                                                                                                            <h5 class="m-0">
                                                                                                                <a class="text-dark collapsed" data-toggle="collapse" href="#collapse_servico7_8" aria-expanded="false">
                                                                                                                    Opção 8
                                                                                                                </a>

                                                                                                                <a class="text-dark pull-right">
                                                                                                                    <input type="checkbox" class="switch_1" id="servico7_opcao8" name="servico7_opcao8">
                                                                                                                </a>
                                                                                                            </h5>
                                                                                                        </div>
                                                                                            
                                                                                                        <div id="collapse_servico7_8" class="collapse" aria-labelledby="heading_servico7_8" data-parent="#accordion">
                                                                                                            <div class="card-body">
                                                                                                                <center>
                                                                                                                    <img id="servico7_imagem8" class="">
                                                                                                                </center>

                                                                                                                <div class="input-group col-md-12 mt-2">
                                                                                                                    
                                                                                                                    <span class="input-group-btn">
                                                                                                                        <button id="caminhoFakeServico7_opcao8" type="button" class="btn btn-success waves-effect waves-light">
                                                                                                                            <i class="fas fa-cloud-upload-alt mr-1"></i> Buscar..
                                                                                                                        </button>
                                                                                                                    </span>

                                                                                                                    <input type="file" id="servico7_imagem_opcao8" name="servico7_imagem_opcao8" style="display:none">
                                                                                                                    
                                                                                                                    <input type="text" id="servico7nomeFake_opcao8" disabled="disabled" placeholder="Procurar arquivo" class="form-control">
                                                                                                                    
                                                                                                                    <span class="input-group-btn col-md-12">
                                                                                                                        <button type="button" class="btn btn-default" disabled="disabled" >
                                                                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                                                                        </button>
                                                                                                                    </span>
                                                                                                                    
                                                                                                                </div>

                                                                                                                <div class="input-group mt-2">
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <label class="col-form-label">Título/Alt (SEO) <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                            data-placement="top" title="Título da imagem e Texto alternativo, para fins de SEO" 
                                                                                                                            data-original-title="Título da imagem e Texto alternativo, para fins de SEO"></i>
                                                                                                                        </label>
                                                                                                                        <input type="text" id="servico7_SEO_opcao8" name="servico7_SEO_opcao8" class="form-control" >
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    
                                                                                        <center>
                                                                                            <button type="submit" id="botaoAtualizarServico7" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                                <span class="btn-label"><i class="fe-save"></i></span> Salvar
                                                                                            </button>
                                                                                        </center>

                                                                                    </form>

                                                                                </div>

                                                                                <div class="tab-pane" id="servico8">
                                                                                    
                                                                                    <form id="formServico8" enctype="multipart/form-data">
                                                                                        
                                                                                        <input type="hidden" name="idModelo_servico8" id="idModelo_servico8">
                                                                                        <input type="hidden" name="idLandingPage_servico8" id="idLandingPage_servico8">

                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-bordered">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th>SESSÃO DE SERVIÇOS 8 (Maps)</th>
                                                                                                        <th>AÇÃO</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                
                                                                                                <tbody>
                                                                                                    
                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Visível? <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                            data-placement="top" title="Ative ou desative o serviço (8) na página, se desativado: o serviço não irá aparece" data-original-title="Ative ou desative o serviço (1) na página, se desativado: o serviço não irá aparece"></i>
                                                                                                        </td>

                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico8_ativaDesativa" name="servico8_ativaDesativa">
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                    <tr>

                                                                                                        <td class="col-12">
                                                                                                            Título e Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" 
                                                                                                            title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos." 
                                                                                                            data-original-title="Habilite ou oculte os textos (Titulo e Descrição), não mostrará nenhum dos dois textos."></i>
                                                                                                        </td>
                                                                                                            
                                                                                                        <td>
                                                                                                            <input type="checkbox" class="switch_1" id="servico8_ConteudoGenerico" name="servico8_ConteudoGenerico">
                                                                                                        </td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        
                                                                                                        <td colspan="2" class="col-12">

                                                                                                            <div class="form-row">
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Titulo <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer texto deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer texto deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico8_titulo" name="servico8_titulo" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-6">
                                                                                                                    <label class="col-form-label">Descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" 
                                                                                                                    data-placement="top" title="Para não aparecer a descrição deixe vazio. Não escreva nada." 
                                                                                                                    data-original-title="Para não aparecer a descrição deixe vazio. Não escreva nada."></i></label>
                                                                                                                    <input type="text" id="servico8_descricao" name="servico8_descricao" class="form-control" >
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor do título <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico8_corTitulo" name="servico8_corTitulo">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor da descrição <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor do texto da descrição" data-original-title="Cor do texto da descrição"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico8_corDescricao" name="servico8_corDescricao">
                                                                                                                </div>
                                                                                                                <div class="form-group col-md-4">
                                                                                                                    <label class="col-form-label">Cor de fundo do serviço <i class="fa fa-info-circle text-muted" data-toggle="tooltip" data-placement="top" title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos" data-original-title="Cor de fundo do serviço, irá aplicar ao fundo por trás de todos os elementos"></i></label>
                                                                                                                    <input type="color" class="form-control" id="servico8_corFundoBG" name="servico8_corFundoBG">
                                                                                                                </div>
                                                                                                            </div>  
                                                                                                            
                                                                                                        </td>
                                                                                                    </tr>

                                                                                                </tbody>

                                                                                            </table>

                                                                                        </div>
                                                                                        
                                                                                        <center>
                                                                                            <button type="submit" id="botaoAtualizarServico8" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                                <span class="btn-label"><i class="fe-save"></i></span> Salvar
                                                                                            </button>
                                                                                        </center>

                                                                                    </form>

                                                                                    
                                                                                </div>

                                                                            </div>

                                                                        </div> 

                                                                        </div>
                                                                        

                                                                    </div>

                                                                    <div class="tab-pane" id="paginaSucesso">
                                                                        
                                                                        <form id="formPaginaSucesso" enctype="multipart/form-data">
                                                                        
                                                                            <input type="hidden" id="idModelo_PaginaSucesso" name="idModelo_PaginaSucesso">
                                                                            <input type="hidden" id="idLandingPage_PaginaSucesso" name="idLandingPage_PaginaSucesso">

                                                                            <div class="form-row">
                                                                                
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="tituloPaginaSucesso" class="col-form-label">Título</label>
                                                                                    <input type="text" class="form-control" id="tituloPaginaSucesso" name="tituloPaginaSucesso" autocomplete="off">
                                                                                </div>
                                                                                
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="descricaoPaginaSucesso" class="col-form-label">Descrição</label>
                                                                                    <textarea name="descricaoPaginaSucesso" id="descricaoPaginaSucesso" rows="5" cols="10"></textarea>
                                                                                </div>
                                                                                
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="corTextoTituloPaginaSucesso" class="col-form-label">Cor texto - Título</label>
                                                                                    <input type="color" class="form-control" id="corTextoTituloPaginaSucesso" name="corTextoTituloPaginaSucesso">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="corTextoDescricaoPaginaSucesso" class="col-form-label">Cor texto - Descrição</label>
                                                                                    <input type="color" class="form-control" id="corTextoDescricaoPaginaSucesso" name="corTextoDescricaoPaginaSucesso">
                                                                                </div>

                                                                                <div class="form-group col-md-3">
                                                                                    <label for="tituloBotaoPaginaSucesso" class="col-form-label">Título do botão</label>
                                                                                    <input type="text" class="form-control" id="tituloBotaoPaginaSucesso" name="tituloBotaoPaginaSucesso" autocomplete="off">
                                                                                </div>

                                                                                <div class="form-group col-md-3">
                                                                                    <label for="linkBotaoPaginaSucesso" class="col-form-label">Link do botão</label>
                                                                                    <input type="link" class="form-control" id="linkBotaoPaginaSucesso" name="linkBotaoPaginaSucesso" autocomplete="off">
                                                                                </div>

                                                                                <div class="form-group col-md-3">
                                                                                    <label for="corTextoBotaoPaginaSucesso" class="col-form-label">Cor texto do botão</label>
                                                                                    <input type="color" class="form-control" id="corTextoBotaoPaginaSucesso" name="corTextoBotaoPaginaSucesso">
                                                                                </div>

                                                                                <div class="form-group col-md-3">
                                                                                    <label for="corTextoBotaoHoverPaginaSucesso" class="col-form-label">Cor texto do botão - Hover</label>
                                                                                    <input type="color" class="form-control" id="corTextoBotaoHoverPaginaSucesso" name="corTextoBotaoHoverPaginaSucesso">
                                                                                </div>

                                                                                <div class="form-group col-md-4">
                                                                                    <label for="corFundoBotaoPaginaSucesso" class="col-form-label">Cor fundo do botão</label>
                                                                                    <input type="color" class="form-control" id="corFundoBotaoPaginaSucesso" name="corFundoBotaoPaginaSucesso">
                                                                                </div>

                                                                                <div class="form-group col-md-4">
                                                                                    <label for="corFundoBotaoHoverPaginaSucesso" class="col-form-label">Cor do botão - Hover</label>
                                                                                    <input type="color" class="form-control" id="corFundoBotaoHoverPaginaSucesso" name="corFundoBotaoHoverPaginaSucesso">
                                                                                </div>

                                                                                <div class="form-group col-md-4">
                                                                                    <label for="corFundoPaginaSucesso" class="col-form-label">Cor de fundo da página</label>
                                                                                    <input type="color" class="form-control" id="corFundoPaginaSucesso" name="corFundoPaginaSucesso">
                                                                                </div>

                                                                                <div class="form-group col-md-4">
                                                                                    <label for="iconePrincipalPaginaSucesso" class="col-form-label">Ícone principal</label>
                                                                                    <input type="text" class="form-control" id="iconePrincipalPaginaSucesso" name="iconePrincipalPaginaSucesso" autocomplete="off">
                                                                                </div>

                                                                                <div class="form-group col-md-4">
                                                                                    <label for="iconeBotaoPaginaSucesso" class="col-form-label">Ícone botão</label>
                                                                                    <input type="text" class="form-control" id="iconeBotaoPaginaSucesso" name="iconeBotaoPaginaSucesso" autocomplete="off">
                                                                                </div>

                                                                                 <div class="form-group col-md-4">
                                                                                    <label class="col-form-label">Pesquisar ícones</label>
                                                                                    <button type="button" class="btn btn-block btn--md btn-info waves-effect waves-light"
                                                                                            data-toggle="modal" data-target="#escolherIcone">
                                                                                        Pesquisar <i class="fe-search"></i> 
                                                                                    </button>
                                                                                    
                                                                                </div>
                                                                                
                                                                            </div>

                                                                            <center class="mt-3">
                                                                                <button type="submit" class="btn btn-xs btn-success waves-effect waves-light">
                                                                                    <span class="btn-label"><i class="fe-save"></i></span>Salvar
                                                                                </button>
                                                                            </center>
                                                                            
                                                                        </form>

                                                                    </div>
                                                                    
                                                                </div>

                                                            </div> 

                                                        </div>
                                                    
                                                    </div>

                                                </div> 

                                            </div>

                                        </div> 

                                    </div>
                                    
                                </div>

                             </div>

                        </div>

                    </div>

                </div>

            </div> 

        </div>

    </div> 
    
</div> 

<div id="modalVerImagemPrincipal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Foto principal do banner</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                    <img id="verImagemPrincipal" width="100%" >
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalVerImagemDeFundo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem de fundo do banner</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                    <img id="verImagemDeFundo" width="100%" >
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalVerImagemSEO" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem de SEO</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                    <img id="verImagemDoSEO" width="100%" >
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="imagem_RedeSocial_Opcao1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem de fundo do banner</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                        <center>
                            <img id="verImagemRedeSocial1" width="50%" >
                        </center>
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="imagem_RedeSocial_Opcao2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem de fundo do banner</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                        <center>
                            <img id="verImagemRedeSocial2" width="50%" >
                        </center>
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="imagem_RedeSocial_Opcao" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem de fundo do banner</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                        <center>
                            <img id="verImagemRedeSocial3" width="50%" >
                        </center>
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalVerImagemServico2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem do Serviço 2</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                        <center>
                            <img id="verImagemServico2" width="100%" >
                        </center>
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalVerImagemServico3" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem do Serviço 2</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                        <center>
                            <img id="verImagemServico3" width="100%" >
                        </center>
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalVerImagemServico4" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem do Serviço 2</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                        <center>
                            <img id="verImagemServico4" width="100%" >
                        </center>
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="modalVerImagemBannerSlide1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Imagem do banner 1</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-4">
                <div class="row">
                    <div class="col-md-12">

                        <center>
                            <img id="verImagemBannerSlide1" width="100%" >
                        </center>
                        
                    </div>
                    
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<?php
    $this->load->view('escolherIcones');
?>

<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>