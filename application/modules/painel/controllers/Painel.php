<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends MX_Controller {

	function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        $this->load->model("Painel_model", "painel");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->library('permitiracesso');
        $this->load->model("empresa/Empresa_model", "empresa");

        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->library('zip');

    }
    
	public function index()
	{
        $this->permitiracesso->verifica_sessao();

        $dadosSessao['dados'] = $this->session->userdata;

        $empresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        
        $dadosSessao['dados']['status_empresa'] = $empresa->status_empresa;
        $dadosSessao['dados']['imagem_empresa'] = $empresa->imagem_empresa;
        
        $nivelAcesso_usuario = $dadosSessao['dados']['nivelAcesso_usuario'];

        $permissaoAcesso = $this->permissoes->buscarPermissaoAcesso($nivelAcesso_usuario);
        $dadosSessao['paginas'] = $permissaoAcesso;
        
        $view = "v_home";

        $permitirAcessoPagina = $this->permitiracesso->verificaAcessoPagina($nivelAcesso_usuario, $view);
        
        $scripts = array(

            "titulo" => "Home | Solid System",
            "scriptsJS" => "<script src='/assets/painel/js/vendor/scriptsHome.js'></script>",
            "scriptsCSS" => "", 
            "scriptsCSSacima" => ""
        );
        
		$this->load->view('Template/t_head', $scripts);
		$this->load->view('Template/t_barraTopo',  $dadosSessao);
        $this->load->view('Template/t_menuLateral', $dadosSessao);
        $this->load->view($permitirAcessoPagina->view, $dadosSessao);
		$this->load->view('Template/t_footer');
		$this->load->view('Template/t_scripts', $scripts);
    }

    public function login()
	{   
        $this->session->sess_destroy();

        $dadosEmpresa = (object)$this->empresa->m_listarInformacoesEmpresa();
        
		$scripts = array(

            "titulo" => "Acessar painel | SolidSystem",
            
            "scriptsJS" => '<script src="/assets/js/vendor.min.js"></script>
                            <script src="/assets/js/app.min.js">
                            </script><script src="/assets/painel/modules/login/autenticacaoLogin.js"></script>',

            "scriptsCSS" => '<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
                            <link href="/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
                            <link href="/assets/css/app.min.css" rel="stylesheet" type="text/css"  id="app-stylesheet" />',

            "scriptsJSacima" => "",
            
            "favicon" => '<link rel="shortcut icon" href="/assets/images/favicon.png">',
            "logotipo" => '<img src="/assets/images/logologin.png" alt="" height="75">',
            "logoEmpresa" => $dadosEmpresa->imagem_empresa
        );


		$this->load->view('v_login', $scripts);
    }
    
    public function c_autenticarLogin()
	{
		
		$retorno['msg'] = "";
		$sinal = false;

		$login = $this->input->post('usuario');
        $senha = $this->input->post('senha');
        
		if(empty($login) || empty($senha))
        {
            
            $retorno['ret'] = false;
            $retorno['msg'].= ' Campo <strong>Usuário</strong> ou <strong>Senha</strong> não podem ser vazios!';
			$sinal = true;
			
		} else {
            
            $resultado = $this->painel->buscarLoginExistente($login);
            
            $senhaOK = (bool)password_verify($senha, $resultado['senha_usuario']);

            $dadosMinhaEmpresa = (object)$this->empresa->m_listarInformacoesEmpresa();
            
            if($resultado['login_usuario'] === $login && $senhaOK)
            {  
                if($resultado['status_usuario'] != 1)
                {
                    $retorno['ret'] = false;
                    $retorno['msg'].= ' Usuário >> <i><strong>'.$resultado['login_usuario'].'</strong></i> << bloqueado!<br>';
					$sinal = true;
					
                } else {
                    
                    $dados['id']                = $resultado['id_usuario'];
                    $dados['login']             = $resultado['login_usuario'];
                    $dados['senha']             = $resultado['senha_usuario'];
                    $dados['nome']              = $resultado['nome_usuario'];
                    $dados['status']            = $resultado['status_usuario'];
                    $dados['imagem_usuario']    = $resultado['imagem_usuario'];
                    $dados['token']             = session_id();
                    $dados['logado']            = TRUE;
                    $dados['nomeEmpresa']       = $dadosMinhaEmpresa->nomeFantasia_empresa;
                    $dados['imagemEmpresa']     = $dadosMinhaEmpresa->imagem_empresa;
                    $dados['statusEmpresa']     = $dadosMinhaEmpresa->status_empresa;
                    
					$token = $this->painel->atualizaToken($dados);
					
                    $this->session->set_userdata($dados);
                    
                    $retorno['ret'] = true;
                    $retorno['msg'].= '';
                    $sinal = true;
				}
				
            } else {

                $retorno['ret'] = false;
                $retorno['msg'].= ' <strong>Usuário</strong> ou <strong>Senha</strong> inválidos!<br>';
				$sinal = true;
				
            }
        }
		
		if($sinal)
		{
            echo json_encode($retorno);
            exit;
		}	

    }
    
    public function logout()
    {   
        $this->session->sess_destroy();

        redirect('/dashboard/login');
    }
    
    public function c_backupDB()
    {
        $this->load->dbutil();
        
        $url_atual = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = explode('.', $url_atual);
        $nomeSite = $url[0];
        
        $dataHoraAtual = date('Y-m-d');

        $db_format = array(
            'format' => 'zip',
            'filename' => $nomeSite.' - backup DB - '.$dataHoraAtual.'.sql');

        $backup = $this->dbutil->backup($db_format);

        $dbname = $nomeSite.' - backup DB - '.$dataHoraAtual.'.zip';
        $save='DB/'.$dbname;

        write_file($save, $backup);
        
        // Download na pasta download
        // force_download($dbname, $backup);
    }
    
}
