var url_atual = window.location.href;
var numModelo = url_atual.split("/");

var aberturaPagina;

$(window).ready(function () {
    aberturaPagina = new Date().getTime();
});

if (navigator.userAgent.match(/Firefox/i) ){
    
    $(window).on('unload', function () {
        
        var fechoPagina = new Date().getTime();
        var tempoAberto = (fechoPagina - aberturaPagina) / 1000;
        
        var TokenId = $("#idToken").val();
    
        $.ajax(
            {
                type: "POST",
                url: "/dashboard/contador/tempoOnline/"+numModelo[4],
                data: {tempoOnline:tempoAberto, token:TokenId},
                dataType: 'json'
            }
        );
    });

} else {

    $(window).on('beforeunload', function () {
        var fechoPagina = new Date().getTime();
        var tempoAberto = (fechoPagina - aberturaPagina) / 1000;
        
        var TokenId = $("#idToken").val();
    
        $.ajax(
            {
                type: "POST",
                url: "/dashboard/contador/tempoOnline/"+numModelo[4],
                data: {tempoOnline:tempoAberto, token:TokenId},
                dataType: 'json'
            }
        );
    });

}

