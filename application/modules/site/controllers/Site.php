<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MX_Controller  {

	function __construct()
    {
        parent::__construct();

		$this->load->model("Site_model", "site");

		$this->load->library('emails');
        
	}
	
	
	public function index()
	{
		$scripts = array(
			
			"titulo" => "Solid System | (62) 98192-7179 Sistemas PHP em Goiânia, Sites PHP em Goiânia, Programador PHP em Goiânia, Disparo de e-mails em massa Goiânia",
			
			"scriptsJS" =>'', 
								
			"scriptsCSS" => '',
			
			"scriptsCSSacima" => "",
			
			"scriptsJSacima" => ''
		);
		
		$this->load->view('v_home', $scripts);
	}
	
}
