listarRelatoriosCadastros();
listarRelatorioCadastrosDiario();
listarRelatorioCadastrosSemanal();
listarRelatorioCadastrosMensal();
listarRelatorioCadastrosAnual();
listarRelatorioCadastrosFacebook();
listarRelatorioCadastrosInstagram();
listarRelatorioCadastrosWhatsApp();
listarRelatorioCadastrosGoogle();
listarRelatorioCadastrosYouTube();
listarRelatorioCadastrosMessenger();
listarRelatorioCadastrosAmigos();
listarRelatorioCadastrosOutros();
listarRelatorioCadastrosTotais();

$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

function listarRelatorioCadastrosTotais(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatorioCadastrosTotais/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
                
                var dados = JSON.parse(data);
                
                dadosGlobais = dados;
                
                $('#totalCadastroFacebook').html(dados.totalCadastrosFacebook[0].totalFacebook);
                $('#totalCadastroInstagram').html(dados.totalCadastrosInstagram[0].totalInstagram);
                $('#totalCadastroWhatsApp').html(dados.totalCadastrosWhatsApp[0].totalWhatsApp);
                $('#totalCadastroGoogle').html(dados.totalCadastrosGoogle[0].totalGoogle);
                $('#totalCadastroYouTube').html(dados.totalCadastrosYouTube[0].totalYouTube);
                $('#totalCadastroMessenger').html(dados.totalCadastrosMessenger[0].totalMessenger);
                $('#totalCadastroAmigos').html(dados.totalCadastrosAmigos[0].totalAmigos);
                $('#totalCadastroOutros').html(dados.totalCadastrosOutros[0].totalOutros);
                
            }
            
        }
    );
}

function listarRelatoriosCadastros(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
          
            url: "/dashboard/listagens/listarRelatoriosCadastros/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
                
                var dados = JSON.parse(data);
                
                dadosGlobais = dados;
                
                $('#CadastrosDiario').html(dados.relatorioCadastroDiario[0].totalCadadastrosDiario);
                $('#CadastrosSemanais').html(dados.relatorioCadastroSemanal[0].totalCadadastrosSemanal);
                $('#CadastrosMensal').html(dados.relatorioCadastroMensal[0].totalCadadastrosMensal);
                $('#CadastrosAnual').html(dados.relatorioCadastroAnual[0].totalCadadastrosAnual);
                
            }
            
        }
    );
}

function listarRelatorioCadastrosDiario(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosDiario/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroDiario').append(
        
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td>'+dados[i].nome_landingPage+'</td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroDiario').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado hoje'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroDiario(relatorioDiario) {
  
    showModalRelatorioCadastroDiario(relatorioDiario);
    
}

function showModalRelatorioCadastroDiario(relatorioDiario){
  
    $('#leadsRelatorioCadastroDiario').modal('show');    
    
}

function listarRelatorioCadastrosSemanal(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosSemanal/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroSemanal').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td>'+dados[i].nome_landingPage+'</td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroSemanal').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroSemanal(relatorioSemanal) {
  
    showModalRelatorioCadastroSemanal(relatorioSemanal);
    
}

function showModalRelatorioCadastroSemanal(relatorioSemanal){
  
    $('#leadsRelatorioCadastroSemanal').modal('show');    
    
}

function listarRelatorioCadastrosMensal(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosMensal/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroMensal').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td>'+dados[i].nome_landingPage+'</td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroMensal').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroMensal(relatorioMensal) {
  
    showModalRelatorioCadastroMensal(relatorioMensal);
    
}

function showModalRelatorioCadastroMensal(relatorioMensal){
  
    $('#leadsRelatorioCadastroMensal').modal('show');    
    
}

function listarRelatorioCadastrosAnual(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosAnual/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroAnual').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td>'+dados[i].nome_landingPage+'</td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroAnual').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroAnual(relatorioAnual) {
  
    showModalRelatorioCadastroAnual(relatorioAnual);
    
}

function showModalRelatorioCadastroAnual(relatorioAnual){
  
    $('#leadsRelatorioCadastroAnual').modal('show');    
    
}

function listarRelatorioCadastrosFacebook(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosFacebook/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroFacebook').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td><strong class="text-warning">'+dados[i].nome_landingPage+'</strong></td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroFacebook').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroFacebook(relatorioFacebook) {
  
    showModalRelatorioCadastroFacebook(relatorioFacebook);
    
}

function showModalRelatorioCadastroFacebook(relatorioFacebook){
  
    $('#leadsRelatorioCadastroFacebook').modal('show');    
    
}

function listarRelatorioCadastrosInstagram(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosInstagram/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroInstagram').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td><strong class="text-warning">'+dados[i].nome_landingPage+'</strong></td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroInstagram').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroInstagram(relatorioInstagram) {
  
    showModalRelatorioCadastroInstagram(relatorioInstagram);
    
}

function showModalRelatorioCadastroInstagram(relatorioInstagram){
  
    $('#leadsRelatorioCadastroInstagram').modal('show');    
    
}

function listarRelatorioCadastrosWhatsApp(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosWhatsApp/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroWhatsApp').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td><strong class="text-warning">'+dados[i].nome_landingPage+'</strong></td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroWhatsApp').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroWhatsApp(relatorioWhatsApp) {
  
    showModalRelatorioCadastroWhatsApp(relatorioWhatsApp);
    
}

function showModalRelatorioCadastroWhatsApp(relatorioWhatsApp){
  
    $('#leadsRelatorioCadastroWhatsApp').modal('show');    
    
}

function listarRelatorioCadastrosGoogle(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosGoogle/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroGoogle').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td><strong class="text-warning">'+dados[i].nome_landingPage+'</strong></td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroGoogle').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroGoogle(relatorioGoogle) {
  
    showModalRelatorioCadastroGoogle(relatorioGoogle);
    
}

function showModalRelatorioCadastroGoogle(relatorioGoogle){
  
    $('#leadsRelatorioCadastroGoogle').modal('show');    
    
}

function listarRelatorioCadastrosYouTube(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosYouTube/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroYouTube').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td><strong class="text-warning">'+dados[i].nome_landingPage+'</strong></td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroYouTube').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroYouTube(relatorioYouTube) {
  
    showModalRelatorioCadastroYouTube(relatorioYouTube);
    
}

function showModalRelatorioCadastroYouTube(relatorioYouTube){
  
    $('#leadsRelatorioCadastroYouTube').modal('show');    
    
}

function listarRelatorioCadastrosMessenger(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosMessenger/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroMessenger').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td><strong class="text-warning">'+dados[i].nome_landingPage+'</strong></td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroMessenger').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroMessenger(relatorioMessenger) {
  
    showModalRelatorioCadastroMessenger(relatorioMessenger);
    
}

function showModalRelatorioCadastroMessenger(relatorioMessenger){
  
    $('#leadsRelatorioCadastroMessenger').modal('show');    
    
}

function listarRelatorioCadastrosAmigos(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosAmigos/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroAmigos').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td><strong class="text-warning">'+dados[i].nome_landingPage+'</strong></td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroAmigos').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroAmigos(relatorioAmigos) {
  
    showModalRelatorioCadastroAmigos(relatorioAmigos);
    
}

function showModalRelatorioCadastroAmigos(relatorioAmigos){
  
    $('#leadsRelatorioCadastroAmigos').modal('show');    
    
}

function listarRelatorioCadastrosOutros(){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
        {
            
            url: "/dashboard/listagens/listarRelatorioCadastrosOutros/"+numModelo[6],
            
            ajax: 'data.json',
            
            success: function(data){
          
                var dados = JSON.parse(data);
                
                if(dados.length>0){
                  
                  dadosGlobais = dados;

                  for(var i = 0; i < dados.length; i++) {

                    moment.locale('pt-br');
                    
                    $('#tabelaRelatorioCadastroOutros').append(
                        '<tr>' +
                        '<td>'+dados[i].nome_cadastros+'</td>' +
                        '<td>'+dados[i].telefone_cadastros+'</td>' +
                        '<td>'+dados[i].email_cadastros+'</td>' +
                        '<td>'+dados[i].ondeNosConheceu_cadastros+'</td>' +
                        '<td><strong class="text-warning">'+dados[i].nome_landingPage+'</strong></td>' +
                        '<td>'+dados[i].nm_grp_emails+'</td>' +
                        '<td>'+dados[i].cidade_cadastros+'/'+dados[i].estado_cadastros+'</td>' +
                        '<td>'+moment(dados[i].data_cadastros).format('DD/MM/YYYY')+' às '+moment(dados[i].hora_cadastros, "H:m").format("H:m")+'</td>' +
                    '</tr>' 
                    
                    );
                  }
        
                } else {
                    
                    $('#tabelaRelatorioCadastroOutros').append(
                      '<td colspan="8">' +
                        '<center class="mt-4 text-center">' +
                            '<div class="col-md-12 text-center">' +
                                '<div class="alert alert-danger text-center">' +
                                    '<i class="fas fa-exclamation-circle"></i> Nenhum cadastro gerado essa semana'+
                                '</div>' +
                            '</div>' +
                        '</center>' +
                      '</td>'
                    );
        
                }
              }
        }
    );
}

function abrirRelatorioCadastroOutros(relatorioOutros) {
  
    showModalRelatorioCadastroOutros(relatorioOutros);
    
}

function showModalRelatorioCadastroOutros(relatorioOutros){
  
    $('#leadsRelatorioCadastroOutros').modal('show');    
    
}