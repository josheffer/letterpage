<?php 
    if($permissaoLinks[4]->permissao_niveisPaginas === '1' && $permissaoLinks[10]->permissao_niveisPaginas === '1')
    {$tamanho1 = '6'; $tamanho2 = '6';} 

    if($permissaoLinks[4]->permissao_niveisPaginas === '1' && $permissaoLinks[10]->permissao_niveisPaginas === '0')
    {$tamanho1 = '12'; $tamanho2 = '6';}

    if($permissaoLinks[4]->permissao_niveisPaginas === '0' && $permissaoLinks[10]->permissao_niveisPaginas === '1')
    {$tamanho1 = '6'; $tamanho2 = '12';}
?>

<div class="row no-gutters mb-3">
                    
    <?php if($permissaoLinks[4]->permissao_niveisPaginas === '1'): ?>
        <div class="col-md-<?=$tamanho1?> col-xl-<?=$tamanho1?>">
            <a href="/dashboard/permissoes/niveisAcesso">
                <div class="bg-soft-primary rounded-0 card-box mb-0">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <h3 class="text-dark"><span>Níveis de acesso</span></h3>
                                <p class="text-warning mb-1 text-truncate">Gerencie os níveis de acesso</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <?php endif;?>

    <?php if($permissaoLinks[10]->permissao_niveisPaginas === '1'): ?>
        <div class="col-md-<?=$tamanho2?> col-xl-<?=$tamanho2?>">
            <a href="/dashboard/permissoes/paginas">
                <div class="bg-soft-danger rounded-0 card-box mb-0">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <h3 class="text-dark"><span>Páginas</span></h3>
                                <p class="text-warning mb-1 text-truncate">Gerencie as páginas do sistema</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <?php endif;?>
    
</div>

<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>