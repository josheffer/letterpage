primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarCatalogos();

function listarCatalogos() {

    $.ajax({

        url: "/dashboard/listagens/listarCatalogos",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            dadosGlobais = dados;

            $('#tabelaCatalogos').html('');

            if (dados.length > 0) {

                for (var i = 0; i < dados.length; i++) {

                    if (dados[i].ativo_catalogos === '1') {

                        var botaoStatus = '<button type="submit" onclick="javascript:atualizarStatusCatalogos(' + i + ');" class="btn btn-xs btn-block btn-purple waves-effect waves-light">Bloquear</button>';

                    } else {

                        var botaoStatus = '<button type="submit" onclick="javascript:atualizarStatusCatalogos(' + i + ');" class="btn btn-xs btn-block btn-danger waves-effect waves-light">Desbloquear</button>';

                    }


                    $('#tabelaCatalogos').append(

                        '<tr>' +
                        '<th>' + dados[i].id_catalogos + '</th>' +
                        '<td>' + dados[i].nome_catalogos + '</td>' +
                        '<td>' + botaoStatus + '</td>' +
                        '<td>' +
                        '<button type="button" onclick="javascript:editarCatalogos(' + i + ');" class="btn btn-xs btn-primary waves-effect waves-light mr-2"><i class="mdi mdi-pencil-outline"></i></button>' +
                        '<button type="button" onclick="javascript:excluirCatalogos(' + i + ');" class="btn btn-xs bg-soft-red waves-effect waves-light mr-1"><i class="mdi mdi-trash-can"></i></i></button>' +
                        '</td>' +
                        '</tr>'

                    );
                }

            } else {

                $('#tabelaCatalogos').append(
                    '<td colspan="4">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhum catálogo!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>'
                );

            }
        }
    });
}

function atualizarStatusCatalogos(dados) {

    $.ajax({
        type: "POST",

        data: {
            idAtualizar: dadosGlobais[dados].id_catalogos
        },

        url: "/dashboard/atualizar/atualizarStatusCatalogos",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroAtualizarStatus').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarCatalogos();

            } else {

                $('#erroMsg').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#modalAtualizarStatus').modal('hide');

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarCatalogos();

            }
        }
    });
}


function editarCatalogos(attCatalog) {

    $('#formEditarCatalogo').each(function() {
        this.reset();
    });

    showModalEditar(attCatalog);
}

function showModalEditar(attCatalog) {

    $('#modalEditarCatalagos').modal('show');
    $("#idCatalogoEditar").val(dadosGlobais[attCatalog].id_catalogos);
    $("#nomeCatalogoEditar").val(dadosGlobais[attCatalog].nome_catalogos);
    $("#tituloNomeCatalogo").html(dadosGlobais[attCatalog].nome_catalogos);

    $("#descricaoCatalogoEditar").val(dadosGlobais[attCatalog].descricao_catalogos);

    $("#linkCatalogoEditar").val(dadosGlobais[attCatalog].link_catalogos);
    $("#SEOCatalogoEditar").val(dadosGlobais[attCatalog].seoImagem_catalogos);
    $("#previewPDF").html('<iframe src="/assets/site/catalogos/pdf/' + dadosGlobais[attCatalog].pdf_catalogos + '"></iframe>');
    $("#previewMaior").html('<img width="100%" src="/assets/site/catalogos/maior/' + dadosGlobais[attCatalog].imagemMaior_catalogos + '">');
    $("#previewMenor").html('<img width="100%" src="/assets/site/catalogos/menor/' + dadosGlobais[attCatalog].imagem_catalogos + '">');

}

$('#formEditarCatalogo').submit(function(e) {
    e.preventDefault();
    var self = $(this);

    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();

    // alert(self.serialize());
    var retorno = atualizarCatalogos(self);
});

function atualizarCatalogos(dados) {

    var form = $('#formEditarCatalogo')[0];

    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarCatalogos",
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 400000,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#erroMsgEditarCatalogo').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarCatalogos();

            } else {

                $('#erroMsg').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').animate({
                    scrollTop: 0
                }, 1000, 'linear');

                $('#formEditarCatalogo').each(function() {
                    this.reset();
                });

                $('#modalEditarCatalagos').modal('hide');

                listarCatalogos();

            }
        }
    });
}

function excluirCatalogos(excluirCatalog) {
    showModalExcluir(excluirCatalog);
}

function showModalExcluir(excluirCatalog) {

    $('#modalExcluir').modal('show');
    $("#idExcluir").html(dadosGlobais[excluirCatalog].id_catalogos);
    $("#idCatalogoExcluir").val(dadosGlobais[excluirCatalog].id_catalogos);
}

$('#formExcluirCatalogo').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = excluirCatalogo(self);
});

function excluirCatalogo(dados) {

    $.ajax({
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/site/excluir/excluirCatalogo",
        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErroExcluirTudo').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').animate({
                    scrollTop: 0
                }, 1000, 'linear');

                listarCatalogos();

            } else {

                $('#erroMsg').html(

                    '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-success">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $('#modalExcluir').modal('hide');

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').animate({
                    scrollTop: 0
                }, 1000, 'linear');

                listarCatalogos();

            }
        }
    });
}

$('#formCadastrarNovoCatalogo').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cadastrarNovoCatalogo(self);
});

function cadastrarNovoCatalogo(dados) {

    var form = $('#formCadastrarNovoCatalogo')[0];

    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/cadastrar/cadastrarCatalogo",
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 400000,

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#erroMsg').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').animate({
                    scrollTop: 0
                }, 1000, 'linear');


                listarCatalogos();

            } else {

                $('#erroMsg').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').animate({
                    scrollTop: 0
                }, 1000, 'linear');

                $('#formCadastrarNovoCatalogo').each(function() {
                    this.reset();
                });


                listarCatalogos();

            }
        }
    });
}