$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listaListasEmails();
listaGrupos();

function initializeFileUploads() {
    $('.file-upload').change(function() {
        var file = $(this).val();
        $(this).closest('.input-group').find('.file-upload-text').val(file);
    });
    $('.file-upload-btn').click(function() {
        $(this).find('.file-upload').trigger('click');
    });
    $('.file-upload').click(function(e) {
        e.stopPropagation();
    });
}

$(function() {
    initializeFileUploads();
});

function listaGrupos() {

    $.ajax({

        url: "/listagens/listarGrupo",

        success: function(data) {

            var dados = JSON.parse(data);

            $('#listargruposEmailIndividual').html('');

            if (dados[0].id_grupos !== '0') {

                $('#listargruposEmailIndividual').append('<option value="">Selecione...</option>');
                $('#listargruposImportar').append('<option value="">Selecione...</option>');
                $('.selectpicker').selectpicker('refresh');

                for (var i = 0; i < dados.length; i++) {

                    $('#listargruposEmailIndividual').append('<option value="' + dados[i].id_grupos + '">' + dados[i].nome_grupos + '</option>');
                    $('#listargruposImportar').append('<option value="' + dados[i].id_grupos + '">' + dados[i].nome_grupos + '</option>');
                    $('.selectpicker').selectpicker('refresh');

                }

            } else {

                $('#listargruposEmailIndividual').append('<option value="">Nenhum grupo cadastrado</option>');
                $('#listargruposImportar').append('<option value="">Nenhum grupo cadastrado</option>');
                $('.selectpicker').selectpicker('refresh');

            }

        }
    });
}

$('#formCadastrarNovoEmailIndividual').submit(function(e) {

    e.preventDefault();

    var self = $(this);

    // alert(self.serialize());

    var retorno = cadastrarEmailIndividual(self);

});

function cadastrarEmailIndividual(dados) {

    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/cadastros/cadastrarEmailIndividual",

        dataType: 'json',

        success: function(retorno) {

            if (retorno.ret == false) {

                $('#mensagemErroEmails').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true" id="fecharMsgExcluir">×</span>' +
                    '</button>' +
                    '<strong>Erro! </strong><br>' + retorno.msg +
                    '</div>'
                );

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

            } else {

                $('#mensagemErroEmails').html(

                    '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Tudo certo! </strong>' + retorno.msg +
                    '</div>'
                );

                $('#formCadastrarNovoEmailIndividual').each(function() {
                    this.reset();
                });

                $("#listargruposEmailIndividual").val('').selectpicker("refresh");

                $(".alert").delay(4000).slideUp(1000, function() { $(this).alert('close'); });

                listaListasEmails();

            }
        }
    });
}

function listaListasEmails() {

    $.ajax({

        url: "/listagens/listarListasEmails",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            // console.log(dados);

            $('#tabelaEmails').html('');

            if (dados.length > 0) {

                dadosGlobais = dados;

                for (var i = 0; i < dados.length; i++) {

                    $('#tabelaEmails').append(

                        '<tr>' +
                        '<td>' + dados[i].nome_grupos + '</td>' +
                        '<td><h4><span class="badge badge-primary">' + dados[i].quantidade + '</span></h4></td>' +
                        '<td>' +
                        '<button type="button" title="Código: ' + dados[i].id_grupos + '" onclick="javascript:AbrirModalExcluirListaEmails(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light botaoExcluirTabelaGrupos">Excluir</button>' +
                        '</td>' +
                        '</tr>'

                    );
                }

            } else {

                $('#tabelaEmails').append(
                    '<tr>' +
                    '<td scope="row" colspan="3" class="text-center col-md-1">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma lista importada!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>' +
                    '</tr>'
                );

            }
        }
    });
}

$(document).ready(function() {

    $("#botaoEnviarArqCSV").click(function(event) {

        event.preventDefault();

        var form = $('#formImportarCSV')[0];

        var data = new FormData(form);

        data.append("CustomField", "This is some extra data, testing");

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/cadastros/cadastrarListaEmailCSV",
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 400000,

            beforeSend: function() {

                $("#listargruposImportar").prop('disabled', true);
                $("#botaoEnviarArqCSV").text("Importando a lista, aguarde... ").append(
                    '<span class="spinner-grow spinner-grow-sm mr-1" role="status" aria-hidden="true"></span>').prop("disabled", true);
                $("#listaCSV").prop("disabled", true);
                $("#botaoSelecionarArquivo").prop("disabled", true);

            },

            success: function(retorno) {

                if (retorno.ret === false) {

                    $("#listargruposImportar").prop("disabled", false);
                    $("#botaoEnviarArqCSV").text("Tentar novamente...").prop("disabled", false);
                    $("#listaCSV").prop("disabled", false);
                    $("#botaoSelecionarArquivo").prop("disabled", false);

                    $('#mensagemErroImportarLista').html(

                        '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                        ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true" id="fecharMsgExcluir">×</span>' +
                        '</button>' +
                        '<strong>Erro! </strong><br>' + retorno.msg +
                        '</div>'
                    );

                    $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                    listaListasEmails();

                } else {

                    retorno.ret === true;

                    $("#listargruposImportar").prop("disabled", false);
                    $("#botaoEnviarArqCSV").text("Importar outra lista").prop("disabled", false);
                    $("#listaCSV").prop("disabled", false);
                    $("#botaoSelecionarArquivo").prop("disabled", false);

                    $('#mensagemErroImportarLista').html(

                        '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                        ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '<span aria-hidden="true">×</span>' +
                        '</button>' +
                        '<strong>Tudo certo! </strong>' + retorno.msg +
                        '</div>'
                    );

                    $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                    $('#formImportarCSV').each(function() {
                        this.reset();
                    });

                    $("#listargruposImportar").val('').selectpicker("refresh");

                    listaListasEmails();

                }
            }
        });
    });
});

function AbrirModalExcluirListaEmails(index) {

    $('#ModalExcluirEmailsLista').modal('show');

    showModal(index);

}

function showModal(index) {

    $("#idGrupoExcluirLista").val(dadosGlobais[index].id_grupos);
    $("#nomeGrupoExcluirLista").val(dadosGlobais[index].nome_grupos);
    $("#tituloListaExcluir").html(dadosGlobais[index].nome_grupos);
    $("#excluirNomeLista").html(dadosGlobais[index].nome_grupos);

    $("#botaoExcluirEmailsLista").text("Excluir").prop("disabled", false);
    $('#fecharMsgErroExcluirListas').trigger('click');


}

$('#formExcluirLista').submit(function(e) {

    e.preventDefault();

    var self = $(this);

    // alert(self.serialize());

    var retorno = excluirEmailsListas(self);

});

function excluirEmailsListas(dados) {

    $.ajax({

        type: "POST",

        data: dados.serialize(),

        url: "/exclusoes/excluirEmailsLista",

        dataType: 'json',

        beforeSend: function() {

            $("#botaoExcluirEmailsLista").text("Excluindo... ").append(
                '<span class="spinner-grow spinner-grow-sm mr-1" role="status" aria-hidden="true"></span>').prop("disabled", true);

            $(".botaoExcluirTabelaGrupos").text("Excluindo..").prop("disabled", true);

        },

        success: function(retorno) {

            if (retorno.ret === false) {

                $('#mensagemERROExcluirEmailsLista').html(

                    '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="fecharMsgErroExcluirListas">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Erro! </strong><br>' + retorno.msg +
                    '</div>'
                );

                $("#botaoExcluirEmailsLista").text("Tentar novamente").prop("disabled", false);
                $(".botaoExcluirTabelaGrupos").text("Excluir").prop("disabled", false);

                listaListasEmails();

            } else {

                retorno.ret === true;

                $('#mensagemSUCESSOExclusaoListaEmails').html(

                    '<div class="alert alert-primary alert-dismissible fade show" role="alert">' +
                    ' <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<strong>Tudo certo! </strong>' + retorno.msg +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $(".botaoExcluirTabelaGrupos").text("Excluir").prop("disabled", false);

                $('#ModalExcluirEmailsLista').modal('hide');

                listaListasEmails();
            }
        }
    });
}