<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    
    public function m_verificaEmailExiste($dados)
    {
        $resultado = $this->db->query("SELECT news_emails FROM newsletter_emails where news_emails = '".$dados['news_emails']."'");

        foreach ($resultado->result() as $dados)
        {
            return $dados;    
        }
    }

    public function m_gerarCodigoEmail() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_emails) as codigo FROM newsletter_emails");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;

        }
        
    }
    
    public function m_cadastrarEmailIndividual($dados)
    {
        
        if($this->db->insert('newsletter_emails', $dados))
        {
            $GrupoEmailxGrupo['id_grupos'] = $dados['news_grupoLista'];
            $GrupoEmailxGrupo['id_emails'] = $dados['id_emails'];

            $retorno = $this->db->insert('newsletter_grupos_x_emails', $GrupoEmailxGrupo);
    
            return $retorno;
        }

    }

    public function m_listarListasEmails()
    {
        $this->db->select('grupos.id_grupos, grupos.nome_grupos, count(*) as quantidade');    
        $this->db->from('newsletter_grupos as grupos');
        $this->db->join('newsletter_grupos_x_emails as emails', 'grupos.id_grupos = emails.id_grupos');
        $this->db->group_by("grupos.id_grupos");
        $this->db->order_by('id_grupos', 'DESC');

        $resultado = $this->db->get()->result_object();
        
        return $resultado;
    }
    
    public function m_cadastrarListaEmailCSV($dados)
    {
        $data['id_emails']      = $dados['id_emails'];
        $data['news_nome']      = $dados['news_nome'];
        $data['news_emails']    = $dados['news_emails'];
        $data['news_status']    = $dados['news_status'];
        $data['news_grupoLista']      = $dados['news_grupoLista'];
        
        if($this->db->insert('newsletter_emails', $data))
        {
            $GrupoEmailxGrupo['id_grupos'] = $dados['news_grupoLista'];
            $GrupoEmailxGrupo['id_emails'] = $dados['id_emails'];
    
            $retorno = $this->db->insert('newsletter_grupos_x_emails', $GrupoEmailxGrupo);
    
            return $retorno;
        }
    }

    public function m_gerarCodigoNovaCampanha()
    {
        $result =  $this->db->query("SELECT MAX(id_campanha) as codigo FROM newsletter_campanhas");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
    }

    public function m_cadastrarNovaCampanha($dados)
    {
        
        return $this->db->insert('newsletter_campanhas', $dados);

    }

    public function m_listarCampanhas()
    {
        
        $resultado['CampPendente'] =  $this->db->query("SELECT * FROM newsletter_campanhas AS a 
                                        LEFT JOIN newsletter_grupos AS b 
                                        ON b.id_grupos = a.destino_campanha
                                        LEFT JOIN newsletter_template_email as c
                                        ON c.id_templateEmail = a.id_templateEmail
                                        WHERE a.statusBotao_campanha IN ('0') 
                                        ORDER BY a.id_campanha DESC;")->result();

        $resultado['dadosTempCampPendente'] =  $this->db->query("SELECT * FROM newsletter_configs_template_email WHERE id_configs_template = '1';")->result();                                        
                                    
        return $resultado;

    }

    public function m_listarCampanhasEnviadas()
    {
        $resultado['CampEnviada'] =  $this->db->query("SELECT * FROM newsletter_campanhas AS a 
                                        LEFT JOIN newsletter_grupos AS b 
                                        ON b.id_grupos = a.destino_campanha
                                        LEFT JOIN newsletter_template_email as c
                                        ON c.id_templateEmail = a.id_templateEmail
                                        WHERE a.statusBotao_campanha IN ('1', '2') 
                                        ORDER BY a.id_campanha DESC;")->result();

        $resultado['dadosTempCampEnviada'] =  $this->db->query("SELECT * FROM newsletter_configs_template_email WHERE id_configs_template = '1';")->result();                                                                                
            
        return $resultado;
    }

    function m_verificaGrupoExistente($nomeGrupo)
    {
        $result =  $this->db->query("SELECT nome_grupos FROM newsletter_grupos WHERE nome_grupos = '$nomeGrupo'");

        foreach ($result->result() as $dados)
        {
            return $dados;    
        }

    }

    public function m_gerarCodigoGrupo() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_grupos) as codigo FROM newsletter_grupos");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    public function m_cadastrarGrupo($dados)
    {
        return $this->db->insert('newsletter_grupos', $dados);
    }

    public function m_listarGrupos()
    {
        $this->db->select("*");
        $this->db->order_by('id_grupos', 'ASC');
        $resultado = $this->db->get("newsletter_grupos")->result();

        return $resultado;
    }

    public function m_alterarGrupos($dados = NULL)
    {  
        if ($dados != NULL && $dados['id_grupos'] != NULL){
            return $this->db->update('newsletter_grupos', $dados, array('id_grupos'=>$dados['id_grupos']));
        }
    }

    public function m_verificaEmailEmGrupoExclusao($id)
    {
        $this->db->where('newsletter_grupos_x_emails', $id);

        $resultado = $this->db->query("SELECT * FROM newsletter_grupos_x_emails where id_grupos = $id");

        foreach ($resultado->result() as $dados)
        {
            return $dados;    
        }
    }

    public function m_excluirGrupo($id)
    {
        $result= $this->db->query("DELETE FROM newsletter_grupos WHERE (`id_grupos` = '$id')");

        return $result;
        
    }

    public function m_excluirEmailsLista($id)
    {
        $resultado = $this->db->query(
            "DELETE FROM newsletter_emails, newsletter_grupos_x_emails USING newsletter_emails
             INNER JOIN newsletter_grupos_x_emails
             WHERE newsletter_emails.id_emails = newsletter_grupos_x_emails.id_emails
             AND newsletter_grupos_x_emails.id_grupos = '".$id."';");

        return $resultado;
        
    }

    public function m_buscarCampanha($id)
    {
        
        $resultado = $this->db->query("SELECT * FROM newsletter_campanhas WHERE id_campanha = '".$id."';");

        foreach ($resultado->result() as $dados)
        {
            return $dados;    
        }
    }

    public function m_atualizarCampanha($dados) 
    {
        $campanha = filter_var ($dados['mensagem_campanha'], FILTER_SANITIZE_MAGIC_QUOTES);

        $resultado = $this->db->query("UPDATE newsletter_campanhas SET 
                                                            destino_campanha        = '".$dados['destino_campanha']."', 
                                                            nome_campanha           = '".$dados['nome_campanha']."', 
                                                            titulo_campanha         = '".$dados['titulo_campanha']."', 
                                                            assunto_campanha        = '".$dados['assunto_campanha']."', 
                                                            mensagem_campanha       = '".$campanha."', 
                                                            statusBotao_campanha    = '".$dados['statusBotao_campanha']."' 
                                                            
                                                            WHERE id_campanha    = '".$dados['id_campanha']."';");
        
        return $resultado;
    }

    public function m_deletarCampanha($id)
    {
        $result = $this->db->query("DELETE FROM newsletter_campanhas WHERE (`id_campanha` = '$id')");

        return $result;
    }

    public function m_cadastrarNovoModeloTemplateEmail($dados)
    {
        return $this->db->insert('newsletter_template_email', $dados);
    }

    public function m_listarModelosTemplateEmails()
    {
        $this->db->select("*");
        $this->db->order_by('id_templateEmail', 'DESC');
        
        $resultado['temp'] = $this->db->get("newsletter_template_email")->result();

        $resultado['dadosTemp'] =  $this->db->query("SELECT * FROM newsletter_configs_template_email WHERE id_configs_template = '1';")->result();

        return $resultado;
    }

    public function m_buscarModeloTemplateEmail($id)
    {
        $resultado = $this->db->query("SELECT * FROM newsletter_template_email WHERE id_templateEmail = '".$id."';");

        foreach ($resultado->result() as $dados)
        {
            return $dados;    
        }
    }

    public function m_atualizarStatusModeloTemplate($dados)
    {
        $resultado = $this->db->query("UPDATE newsletter_template_email SET 
                                                status_templateEmail        = '".$dados['status_templateEmail']."'
        
        WHERE id_templateEmail    = '".$dados['id_templateEmail']."';");

        return $resultado;
    }

    public function m_atualizarInfosModeloTemplate($dados)
    {
        $html = filter_var ($dados['html_templateEmail'], FILTER_SANITIZE_MAGIC_QUOTES);
        $nome = $dados['nome_templateEmail'];
        $id = $dados['id_templateEmail'];

        $resultado = $this->db->query("UPDATE newsletter_template_email SET 
                                                nome_templateEmail  = '".$nome."', 
                                                html_templateEmail  = '".$html."'
        
        WHERE id_templateEmail = '".$id."';");

        return $resultado;
    }

    public function m_excluirModeloTemplateEmail($id)
    {
        $result = $this->db->query("DELETE FROM newsletter_template_email WHERE (`id_templateEmail` = '$id')");

        return $result;
    }

    public function m_listarConfigsTemplateEmails()
    {
        $resultado = $this->db->query("SELECT * FROM newsletter_configs_template_email WHERE id_configs_template = '1';");

        foreach ($resultado->result() as $dados)
        {
            return $dados;    
        }
    }

    public function m_buscarConfigsTemplateEmail($id)
    {
        $resultado = $this->db->query("SELECT * FROM newsletter_configs_template_email WHERE id_configs_template = '".$id."';");

        foreach ($resultado->result() as $dados)
        {
            return $dados;    
        }
    }

    public function m_atualizarDadosConfigsTempEmail($dados)
    {
        $resultado = $this->db->query("UPDATE newsletter_configs_template_email SET 
                                                linkImagem_configs_template         = '".$dados['linkImagem_configs_template']."',
                                                linkLogotipo_configs_template       = '".$dados['linkLogotipo_configs_template']."',
                                                nomeEmpresa_configs_template        = '".$dados['nomeEmpresa_configs_template']."',
                                                endereco_configs_template           = '".$dados['endereco_configs_template']."',
                                                dominio_configs_template            = '".$dados['dominio_configs_template']."',
                                                linkDominio_configs_template        = '".$dados['linkDominio_configs_template']."',
                                                linkFacebook_configs_template       = '".$dados['linkFacebook_configs_template']."',
                                                linkInstagram_configs_template      = '".$dados['linkInstagram_configs_template']."',
                                                linkWhatsApp_configs_template       = '".$dados['linkWhatsApp_configs_template']."'

        WHERE id_configs_template = '1';");

        return $resultado;
    }

    public function m_listarCampanhasTemplates()
    {
        $resultado['campanhas'] =  $this->db->query("SELECT * FROM newsletter_campanhas as a
                                        INNER JOIN newsletter_template_email as b
                                        ON b.id_templateEmail = a.id_templateEmail 
                                        ORDER BY a.id_campanha DESC;")->result();

        $resultado['dadosTemplate'] =  $this->db->query("SELECT * FROM newsletter_configs_template_email WHERE id_configs_template = '1';")->result();
            
        return $resultado;
    }

    public function m_listarModelosTemplateEmailsTabela()
    {
        $resultado['infosTemp'] =  $this->db->query("SELECT * FROM newsletter_template_email WHERE status_templateEmail = '1' ORDER BY id_templateEmail DESC;")->result();

        $resultado['dadosConfigsTemp'] =  $this->db->query("SELECT * FROM newsletter_configs_template_email WHERE id_configs_template = '1';")->result();
            
        return $resultado;
    }
    
    public function m_atualizarTemplateEmailCampanha($dados)
    {
        $resultado = $this->db->query("UPDATE newsletter_campanhas SET id_templateEmail = '".$dados['id_templateEmail']."'

                                        WHERE id_campanha = '".$dados['id_campanha']."';");

        return $resultado;
    }

    public function m_buscarEmails($destinoNews)
    {
        $resultado =  $this->db->query('SELECT * FROM  newsletter_emails as a
                                        INNER JOIN  newsletter_grupos_x_emails as b
                                        ON  b.id_emails =  a.id_emails
                                        WHERE  b.id_grupos = '.$destinoNews)->result();
                                    
        return $resultado;

    }

    public function m_gerarCodigoEnvioNewsletter()
    {
        $dataServidor = date("Ymd");

        $resultado =  $this->db->query("SELECT MAX(id_envios) as codigo FROM newsletter_envios WHERE substr(id_envios, 1, 8) = "."'$dataServidor'");
        
        foreach ($resultado->result_array() as $dados)
        {
            
            if(is_numeric($dados['codigo'])){

                $codigo = $dados['codigo']+1;
                
            } else {
                
                $codigo = $dataServidor.(str_pad(1, 6, '0', STR_PAD_LEFT));
    
            }
        }

        return $codigo;
        
    }

    public function m_prepararNovaCampanha($dados)
    {
        return $this->db->insert('newsletter_envios', $dados);
    }

    public function m_atualizarBotao($at = NULL)
    {  
        if ($at != NULL && $at['id_campanha'] != NULL){

            return $this->db->query("UPDATE newsletter_campanhas SET statusBotao_campanha = '".$at['statusBotao_campanha']."' WHERE id_campanha = '".$at['id_campanha']."'");

        }
    }

    public function m_buscarEmail($dados)
    {
        
        $resultado =  $this->db->query("SELECT * FROM newsletter_emails 
                                        WHERE news_emails = '".$dados['email']."' 
                                        AND news_grupoLista = '".$dados['campanha']."'");
                                        
        foreach ($resultado->result() as $dados)
        {
            return $dados;    
        }
        
    }

    public function m_cancelar_inscricaoNews($dados)
    {
        $resultado = $this->db->query("UPDATE newsletter_emails
                                        SET news_status = '0'
                                        WHERE news_emails = '".$dados['email']."';");

        return $resultado;
    }

    public function m_listarEnviosAndamento()
    {
        $resultado =  $this->db->query("SELECT a.id_campanha, a.nome_campanha_envios 
                                        FROM newsletter_envios as a 
                                        INNER JOIN newsletter_campanhas  as b
                                        WHERE a.statusEmails_envios IN ('0', '1', '2')
                                        GROUP BY a.id_campanha, a.nome_campanha_envios
                                        ORDER BY a.id_campanha DESC;")->result();
                                    
        $dados = array();

        $i = 0;

        if(count($resultado) > 0){

            foreach ($resultado as $key => $value) {
                
                $dados[$i]['nome_campanha_envios'] = $value->nome_campanha_envios;

                $consulta = $this->db->query(
                    "SELECT 
                        status_pendentes.qtde AS status_zero_pendente, 
                        status_enviados.qtde AS status_um_enviado, 
                        status_erros.qtde AS status_dois_erro

                        FROM (SELECT statusEmails_envios, count(*) AS qtde 
                        FROM newsletter_envios WHERE id_campanha = '".$value->id_campanha."' AND  statusEmails_envios = '0') AS status_pendentes,

                        (SELECT statusEmails_envios, count(*) AS qtde
                        FROM newsletter_envios WHERE id_campanha = '".$value->id_campanha."' AND  statusEmails_envios = '1') as status_enviados,

                        (SELECT statusEmails_envios, count(*) AS qtde
                        FROM newsletter_envios WHERE id_campanha = '".$value->id_campanha."' AND  statusEmails_envios = '2') as status_erros")->result();
                
                $dados[$i]['status_zero_pendente'] = $consulta[0]->status_zero_pendente;
                $dados[$i]['status_um_enviado'] = $consulta[0]->status_um_enviado;
                $dados[$i]['status_dois_erro'] = $consulta[0]->status_dois_erro;
                $dados[$i]['id_campanha'] = $value->id_campanha;
                
                $total = $consulta[0]->status_zero_pendente + $consulta[0]->status_um_enviado + $consulta[0]->status_dois_erro;
                
                $somaEnviados_Erros =  $dados[$i]['status_um_enviado'] +  $dados[$i]['status_dois_erro'];
                
                $porcentagem = ($somaEnviados_Erros * 100) / $total;
                
                $dados[$i]['porcentagem'] =   intval($porcentagem);

                $dados[$i]['total'] =   $total;
                
                $i++;
        
            } 
        
            return $dados;

        }
    }

    public function m_listarEnviosEnviadosOK()
    {
        // echo '<pre>';
        // echo 'Cheguei_na_Controller_Model';
        // echo '</pre>';
        exit();
    }

    public function m_buscarEmailsDisponiveis($statusEmail)
    {
        $resultado =  $this->db->query("SELECT * FROM newsletter_envios 
                                        WHERE statusEmails_envios = '".$statusEmail."' 
                                        ORDER BY id_campanha LIMIT 4")->result();
                                        
        return $resultado;
        
    }

    public function m_atualizarStatusEmailEnviado($dados)
    {
        $resultado = $this->db->query("UPDATE newsletter_envios 
                                        SET statusEmails_envios = '".$dados['statusEmails_envios']."' 
                                        WHERE id_envios = '".$dados['id_envios']."';");

        return $resultado;
    }

    public function m_gerarCodigoConfirmarEmail() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_aberturas) as codigo FROM newsletter_aberturas_email");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;

        }
        
    }

    public function m_contadorAberturaEmail($dados)
    {
        return $this->db->insert('newsletter_aberturas_email', $dados);
    }

    public function m_relatorioEnvios($idCampanha)
    {
        $resultado =  $this->db->query("SELECT id_envios, nome_campanha_envios, nomePessoa_envios, emails_envios, statusEmails_envios  
                                        FROM newsletter_envios 
                                        WHERE id_campanha = '".$idCampanha."' 
                                        ORDER BY statusEmails_envios;")->result();          
        return $resultado;
        
    }

    public function m_dadosConfigEnvios()
    {
        $resultado =  $this->db->query("SELECT * FROM newsletter_configs_envio 
                                        WHERE id_configsEnvios = '1';")->result();          
        foreach ($resultado as $dados)
        {
            return $dados;    
        }
    }

    public function m_atualizarConfigsDisparo($dados)
    {
        $resultado = $this->db->query("UPDATE newsletter_configs_envio 
                                        SET host_configsEnvios          = '".$dados['host_configsEnvios']."', 
                                            usuario_configsEnvios       = '".$dados['usuario_configsEnvios']."', 
                                            senha_configsEnvios         = '".$dados['senha_configsEnvios']."', 
                                            porta_configsEnvios         = '".$dados['porta_configsEnvios']."', 
                                            debug_configsEnvios         = '".$dados['debug_configsEnvios']."', 
                                            SMTPSecure_configsEnvios    = '".$dados['SMTPSecure_configsEnvios']."', 
                                            SMTPAuth_configsEnvios      = '".$dados['SMTPAuth_configsEnvios']."', 
                                            CharSet_configsEnvios       = '".$dados['CharSet_configsEnvios']."', 
                                            Encoding_configsEnvios      = '".$dados['Encoding_configsEnvios']."'
                                        WHERE id_configsEnvios = 1;");

        return $resultado;
    }
    
}