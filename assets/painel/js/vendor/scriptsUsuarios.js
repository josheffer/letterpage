listaUsuarios();
listaNivelAcessoUsuarios();

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

function listaUsuarios(){
    
  $.ajax(
    {
        
      url: "/dashboard/usuarios/listarUsuarios",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#tabelaListarUsuarios').html('');
        
        if(dados.dados.length>0){
          
          dadosGlobais = dados.dados;
          
          for(var i = 0; i < dados.dados.length; i++) {

            if(dados.links[22].permissao_niveisPaginas === '1')
            {
              
              var botaoModificar = '<button type="button" onclick="javascript:AbrirModalEditarUsuario(' + i + ');" class="btn btn-primary btn-xs waves-effect waves-light mr-2">Modificar</button>';
              var inicioBotoesAcoes = '<td>';
              var fimBotoesAcoes = '</td>';
            
            } else {

              var botaoModificar = '';
              var inicioBotoesAcoes = '';
              var fimBotoesAcoes = '';

            }

            if(dados.links[23].permissao_niveisPaginas === '1')
            {
              
              var botaoExcluir = '<button type="button" onclick="javascript:AbrirModalDeletarUsuario(' + i + ');" class="btn btn-danger btn-xs waves-effect waves-light">Excluir</button>';
              var inicioBotoesAcoes = '<td>';
              var fimBotoesAcoes = '</td>';

            } else {

              var botaoExcluir = '';
             
              
            }
            
            if(dados.links[21].permissao_niveisPaginas === '1') {
              if (dados.dados[i].status_usuario == 0) 
              {

                var botaoStatus = '<button type="submit" onclick="javascript:AbrirModalAtualizarStatus(' + i + ');" class="btn btn-xs btn-dark waves-effect waves-light"><i class="fas fa-user-times"></i></button>';
                var inicioBotaoStatus = '<td>';
                var fimBotaoStatus = '</td>';
              
              } else {

                var inicioBotaoStatus = '<td>';
                var fimBotaoStatus = '</td>';
                var botaoStatus = '<button type="submit" onclick="javascript:AbrirModalAtualizarStatus(' + i + ');" class="btn btn-xs btn-purple waves-effect waves-light"><i class="fas fa-user-check"></i></button>';
              
              }
            } else {

              var botaoStatus = '';
              var inicioBotaoStatus = '';
              var fimBotaoStatus = '';

            }

            $('#tabelaListarUsuarios').append(
              '<tr>' +
                '<td>'+dados.dados[i].id_usuario+'</td>' +
                '<td>'+dados.dados[i].login_usuario+'</td>' +
                '<td>'+dados.dados[i].nome_usuario+'</td>' +
                '<td>'+dados.dados[i].email_usuario+'</td>' +
                
                inicioBotaoStatus +
                  botaoStatus +
                fimBotaoStatus +
                
                inicioBotoesAcoes +
                  botaoModificar +
                  botaoExcluir +
                fimBotoesAcoes +
              '</tr>'
            );

          }
          
          

        } else {
            
            $('#tabelaListarUsuarios').append(
              '<td colspan="6">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhum usuário cadastrado!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );

        }
      }
    }
  );
}

function listaNivelAcessoUsuarios(){
    
  $.ajax(
    {
        
      url: "/dashboard/usuarios/listaNivelAcessoUsuarios",
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#nivelAcessoUsuario').html('');
        $('#atualizarNivelAcessoUsuario').html('');
        
        if(dados[0].id_nivelAcesso !== '0'){
          

          $('#nivelAcessoUsuario').append('<option value="">Selecione...</option>');
          $('#atualizarNivelAcessoUsuario').append('<option value="">Selecione...</option>');
          
          for(var i = 0; i < dados.length; i++) {

            $('#atualizarNivelAcessoUsuario').append('<option selected value="'+dados[i].id_nivelAcesso+'">'+dados[i].nome_nivelAcesso+'</option>');

            $('#nivelAcessoUsuario').append('<option value="'+dados[i].id_nivelAcesso+'">'+dados[i].nome_nivelAcesso+'</option>');
            
          }

        } else {

          $('#nivelAcessoUsuario').append('<option value="">Nenhum nível de acesso cadastrado!</option>');
          $('#atualizarNivelAcessoUsuario').append('<option value="">Nenhum nível de acesso cadastrado!</option>');
         
        }
        
      }
    }
  );
}

$('#formCadastrarNovoUsuario').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = cadastrarNovoUsuario(self);
  
});

function cadastrarNovoUsuario(dados){

    var url_atual = window.location.href;
    var numModelo = url_atual.split("/");
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/usuarios/cadastrarUsuario",
        dataType: 'json',

        beforeSend: function(){
          
          $("#botaoCadastrarUsuario").val("Cadastrando...").prop("disabled", true);
          $("#loginUsuario").prop("disabled", true);
          $("#nomeUsuario").prop("disabled", true);
          $("#emailUsuario").prop("disabled", true);

        },

        success: function(retorno){
            
          if(retorno.ret === false) {
  
            $('#msgErro').html(
                
              '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                  '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );
            
            $("#botaoCadastrarUsuario").text("Tentar novamente...").prop("disabled", false);
            $("#loginUsuario").prop("disabled", false);
            $("#nomeUsuario").prop("disabled", false);
            $("#emailUsuario").prop("disabled", false);
  
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);

            listaUsuarios();
            
          } else {
              
            $('#msgErro').html(
                
              '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                '<h5 class="text-success">Tudo certo!</h5>' +
                  retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );

            $('#formCadastrarNovoUsuario').each (function(){
              this.reset();
            });
            
            $("#botaoCadastrarUsuario").text("Criar novo usuário...").prop("disabled", false);
            $("#loginUsuario").prop("disabled", false);
            $("#nomeUsuario").prop("disabled", false);
            $("#emailUsuario").prop("disabled", false);
  
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);
            
            listaUsuarios();

          }
        }
      }
    );
  }
  
function AbrirModalAtualizarStatus(attStatus) {

  showModalAtualizarStatus(attStatus);

}

function showModalAtualizarStatus(attStatus){

  $('#modalAtualizarStatus').modal('show');

  $("#idUsuario").val(dadosGlobais[attStatus].id_usuario);

  $('#fecharModalAtualizarStatus').trigger('click');

}

$('#formAtualizaStatus').submit(function(e){
  
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarStatusUsuario(self);

});

function atualizarStatusUsuario(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/usuarios/atualizarStatusUsuario",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroAtualizarStatus').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          // listaUsuarios();
          
        } else {
          
          $('#msgSucesso').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#modalAtualizarStatus').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaUsuarios();
        }
      }
    }
  );
}

function AbrirModalEditarUsuario(attUsuario) {

  showModalEditarUsuario(attUsuario);

}

function showModalEditarUsuario(attUsuario){

  $('#modalEditarUsuario').modal('show');
  
  $("#atualizarIdUsuario").val(dadosGlobais[attUsuario].id_usuario);
  $("#idUsuarioAtualizar").html(dadosGlobais[attUsuario].id_usuario);
  $("#atualizarNomeUsuarioTitulo").html(dadosGlobais[attUsuario].nome_usuario);
  $("#atualizarLoginUsuario").val(dadosGlobais[attUsuario].login_usuario);
  $("#atualizarNomeUsuario").val(dadosGlobais[attUsuario].nome_usuario);
  $("#atualizarEmailUsuario").val(dadosGlobais[attUsuario].email_usuario);
  
  $('#fecharModalAtualizarUsuario').trigger('click');

}

$('#formAtualziarUsuario').submit(function(e){
  
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarDadosUsuario(self);

});

function atualizarDadosUsuario(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/usuarios/atualizarDadosUsuario",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroAtualizarUsuario').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaUsuarios();
          
        } else {
          
          $('#msgErro').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#modalEditarUsuario').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaUsuarios();
        }
      }
    }
  );
}

function AbrirModalDeletarUsuario(deletarUsuario) {

  showModalDeletarUsuario(deletarUsuario);

}

function showModalDeletarUsuario(deletarUsuario){

  $('#modalDeletarUsuario').modal('show');
  
  $("#idDeletarusuario").val(dadosGlobais[deletarUsuario].id_usuario);
  
  $('#fecharModalDeletarusuario').trigger('click');

}

$('#formDeletarUsuario').submit(function(e){
  
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = deletarDadosUsuario(self);

});

function deletarDadosUsuario(dados){
    
  $.ajax(
    {
      type: "POST",
      data: dados.serialize(),
      url: "/dashboard/usuarios/deletarDadosUsuario",
      dataType: 'json',
      
      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#msgErroDeletarUsuario').html(
              
            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                '<h5 class="text-danger">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaUsuarios();
          
        } else {
          
          $('#msgErro').html(
              
            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
              '<h5 class="text-success">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );

          $('#modalDeletarUsuario').modal('hide');
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);

          listaUsuarios();
        }
      }
    }
  );
}