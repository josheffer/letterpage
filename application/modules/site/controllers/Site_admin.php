<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_admin extends MX_Controller  {

	function __construct()
    {
        parent::__construct();

        $this->load->model("landingpage/Landingpage_model", "landingpage");
        $this->load->model("landingpage/Templatelandingpage_model", "template");
        $this->load->model("landingpage/Modelos_model", "modelos");
        $this->load->model("permissoes/Permissoes_model", "permissoes");
        $this->load->model("planos/Planos_model", "planos");
        $this->load->model("Site/Site_model", "site");
        $this->load->library('permitiracesso');
        $this->load->library('conversores');
        $this->load->library('uploadarquivos');

        $this->load->model("empresa/Empresa_model", "empresa");
        
    }
	
	
}
