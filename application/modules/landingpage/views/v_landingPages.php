<div class="content-page">

    <div class="content">
        
        <div class="container-fluid">
            
            <div class="row mt-3">
                
                <div class="col-12">
                
                    <?php
                        $this->load->view('botoesLandingPages');
                    ?>
                    
                    <div class="row mt-3 mb-2">
                    
                        <?php 
                            if($permissaoLinks[6]->permissao_niveisPaginas === '1' && $permissaoLinks[7]->permissao_niveisPaginas === '1')
                            {$tam1 = '6'; $tam2 = '6';} else {$tam1 = '12'; $tam2 = '12';}
                        ?>
                                
                        <?php if($permissaoLinks[7]->permissao_niveisPaginas === '1'):?>
                            <div class="col-xl-<?=$tam1?>">
                                <a href="/dashboard/cadastrarNovaLandingPage">
                                    <button type="button" class="btn btn-block btn-sm btn-info waves-effect waves-light"
                                            data-toggle="tooltip" 
                                            data-placement="bottom" title="Cadastrar nova landing page!" 
                                            data-original-title="Cadastrar nova landing page!">
                                        <span class="btn "><i class="fe-plus"></i></span>Novo
                                    </button>
                                </a> 
                            </div>
                        <?php endif;?>

                        <?php if($permissaoLinks[6]->permissao_niveisPaginas === '1'):?>
                            <div class="col-xl-<?=$tam2?>">
                                <a href="/dashboard/gruposLandingPage">
                                    <button type="button" class="btn btn-block btn-sm btn-light waves-effect waves-light"
                                            data-toggle="tooltip" 
                                            data-placement="bottom" title="Cadastrar novo grupo de captura de contato!" 
                                            data-original-title="Cadastrar novo grupo de captura de contato!">
                                        <span class="btn "><i class="fe-plus"></i></span>Grupo
                                    </button>
                                </a> 
                            </div>
                        <?php endif;?>
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            
                        <div id="msgSucesso"></div>

                            <div class="card">
                            
                                <div class="card-body">
                                    
                                    <h5 class="card-title mb-3">Landings Pages cadastradas</h5>

                                    <div class="table-responsive">
                                        <table class="table table-bordered mb-0">
                                        
                                            <thead>
                                                <tr>
                                                    <th>#</th>

                                                    <th>Nome</th>

                                                    <?php if($links[11]->permissao_niveisPaginas === '1'):?><th>Leads</th><?php endif;?>

                                                    <?php if($links[18]->permissao_niveisPaginas === '1'):?><th>Status</th><?php endif;?>

                                                    <?php if($links[12]->permissao_niveisPaginas === '1' && $links[13]->permissao_niveisPaginas === '1'):?><th>Template</th><?php endif;?>
                                                    <?php if($links[12]->permissao_niveisPaginas === '0' && $links[13]->permissao_niveisPaginas === '1'):?><th>Template</th><?php endif;?>
                                                    <?php if($links[12]->permissao_niveisPaginas === '1' && $links[13]->permissao_niveisPaginas === '0'):?><th>Template</th><?php endif;?>
                                                    
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>

                                            <tbody id="tabelaListarLandingPages"></tbody>
                                                
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>     
            
        </div>

    </div> 
    
</div>

<?php if($links[19]->permissao_niveisPaginas === '1'):?>
    <div class="modal fade bs-example-modal-lg" id="modalEditarLandingPage" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Editar as informações da LandingPage: <span class="text-warning" id="tituloNomeLandingPageEditar"></span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                <form id="formEditarLandingPage" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-12">
                            <div class="col-md-12">
                                <div id="erroMsgEditarLandingPage">
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-body">
                                    
                                    <h4 class="header-title">Nome e Título</h4>

                                    <div class="form-row">
                                        
                                        <div class="form-group col-md-4">
                                            <label class="col-form-label">Nome (<small>Nome na lista</small>)</label>
                                            <input type="text" class="form-control" id="nomeLandingPageEditar" name="nomeLandingPageEditar" autocomplete="off">
                                        </div>

                                        <input type="hidden" name="idLandingPageEditar" id="idLandingPageEditar">

                                        <div class="form-group col-md-4">
                                            <label class="col-form-label">Título (<small>Nome na URL</small>)</label>
                                            <input type="text" class="form-control" id="tituloLandingPageEditar" name="tituloLandingPageEditar" autocomplete="off">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label class="col-form-label">Lista (<small>Grupo para envio dos e-mails</small>)</label>
                                            <select class="form-control" id="listaEmailsLandingPage" name="listaEmailsLandingPage">
                                            </select>
                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">Atualizar</button>
                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancelar</button>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
<?php endif;?>

<?php if($links[18]->permissao_niveisPaginas === '1'):?>
    <div id="modalAtualizarStatus" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mySmallModalLabel">Atualizar status</h4>
                    <button type="button" id="fecharModalAtualizarStatus" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <form id="formAtualizaStatus">
                    <div class="modal-body">
                        
                        <div id="msgErroAtualizarStatus"></div>

                        Tem certeza que deseja atualizar o status dessa landing page?
                        <input type="hidden" name="idLandingPage" id="idLandingPage">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect">(Sim) Atualizar</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif;?>

<?php if($permissaoLinks[20]->permissao_niveisPaginas === '1'):?>
    <div id="modalExcluirLandingPage" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="mySmallModalLabel">Excluir LandingPages (<span id="idLanding"></span>)</h4>
                    <button type="button" id="fecharModalExcluirLandingPage" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <form id="formExcluirLandingPage">
                    <div class="modal-body">
                        
                        <div id="msgErroExcluirLandingPage"></div>

                        Tem certeza que deseja excluir essa landing page?
                        <input type="hidden" name="idLandingPageExcluir" id="idLandingPageExcluir">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">(Sim) Excluir</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Fechar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif;?>