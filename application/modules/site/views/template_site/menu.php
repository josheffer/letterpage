    <a href="javascript:void(0);" id="back-to-top" class="background-btn back_to_top show">
            <i class="flaticon-left-arrow"></i>
        </a>
        
        <header>
            
            <div class="col-2 mt-3 visible-lg d-none">
                <a href="<?=base_url();?>"><img src="/assets/images/logologin.png" class="img-fluid" width="140px" alt="logo" /></a>
            </div>
            
            <div class="col_6 visible-lg d-none">
                <nav class="navbar-dark navbar-expand-lg navbar">
                    
                    <div class="navbar-collapse collapse" id="collapseNavbar">

                        <ul class="navbar-nav" id="linksMenu">
                            <li class="nav-item active">
                                <a class="nav-link text-uppercase" href="">Início</a>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
            
            <div class="col-4 visible-lg d-none">
                
                <ul class="social_icons float-md-right">
                    
                    <li class="wishlist_icon"   id="socialLink_instagram"></li>
                    <li class="login_icon"      id="socialLink_facebook"></li>
                    <li class="wishlist_icon"   id="socialLink_whatsapp1"></li>
                    <li class="wishlist_icon"   id="socialLink_whatsapp2"></li>
                    <li class="wishlist_icon"   id="socialLink_whatsapp3"></li>
                    
                </ul>

            </div>
            
            <div class="header_mobile hidden-lg d-block mb-4">
                
                <div class="">
                    
                    <ul class="social_icons mt-3 text-center">
                        
                        <li class="login_icon" id="socialLink_facebook2"></li>

                        <li class="wishlist_icon" id="socialLink_instagram2"></li>

                        <li class="wishlist_icon" id="socialLink2_whatsapp1"></li>

                        <li class="wishlist_icon" id="socialLink2_whatsapp2"></li>

                        <li class="wishlist_icon" id="socialLink2_whatsapp3"></li>
                        
                    </ul>
                </div>
                
                <div class="header_logo col_4">
                    <a href="<?=base_url();?>"><img src="/assets/images/logologin.png" width="160px" class="img-fluid" alt="logo" /></a>
                </div>
                
                <nav class="navbar-dark navbar-expand-lg navbar">

                    <button type="button" data-toggle="collapse" class="navbar-toggler">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="26" viewBox="0 0 28 26">
                        <path id="menu" class="cls-1" fill="#333" d="M0,0H28V2H0V0ZM0,8H20v2H0V8Zm0,8H28v2H0V16Zm0,8H17v2H0V24Z"></path>
                        </svg>
                    </button>
                    
                    <div class="navbar-collapse collapse ">
                        
                        <div class="close_icon"><a class="menu_colse" href="javascript:void(0);"><i class="flaticon-close"></i></a></div>

                        <ul class="navbar-nav" id="linksMenuMobile">
                            <li class="nav-item active">
                                <a class="nav-link text-uppercase" href="">Início</a>
                            </li>
                        </ul>
                        
                    </div>
                </nav>
                
            </div>
        </header>

        <div class="menu_overlay"></div>