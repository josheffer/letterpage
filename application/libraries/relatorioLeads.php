<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class relatorioLeads {

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model("permissoes/Permissoes_model", "permissoes");
        $this->CI->load->model("painel/Painel_model", "painel");
        $this->CI->load->model("landingage/LandingPage_model", "landingpage");

        $this->CI->load->model("newsletter/Newsletter_model", "newsletter");
    }

    public function relatorioEnvios($idCamp)
    {
        $idCampanha = $idCamp;
        
        $campanhas = $this->CI->newsletter->m_relatorioEnvios($idCampanha);
        
        $infos = $campanhas;

        foreach ($infos as $dados)
        {
            $arquivo = "Relatorio dos envios da campanha - ".$dados->nome_campanha_envios.".xls";
            $nomeCampanha = $dados->nome_campanha_envios;
        }

        $html = '';

        $html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

        $html .= '<table border="1">';
		$html .= '<tr>';
		$html .= '<td style="text-align:center;" colspan="6">Relatorio newsletter -  '.$nomeCampanha.'</tr>';
        $html .= '</tr>';

        $html .= '<tr>';
		$html .= '<td><b>ID</b></td>';
		$html .= '<td><b>CAMPANHA</b></td>';
		$html .= '<td><b>NOME DA PESSOA</b></td>';
		$html .= '<td><b>E-MAIL</b></td>';
        $html .= '<td><b>ENVIO</b></td>';
        
        $html .= '<td><b>SITUAÇÃO</b></td>';
        

        $html .= '</tr>';
        
        foreach ($campanhas as $value) {

            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR'); 
            
            if($value->statusEmails_envios === '1')
            {
                $envio = 'Enviado';
                $situacao = 'E-mail OK';

            } else {

                $envio = 'Não enviado';
                $situacao = 'E-mail incorreto ou não existe';
            }
            
            $id = $value->id_envios;

            $html .= '<tr>';
            $html .= "<td>".$id."</td>";
            $html .= '<td>'.$value->nome_campanha_envios.'</td>';
            $html .= '<td>'.$value->nomePessoa_envios.'</td>';
            $html .= '<td>'.$value->emails_envios.'</td>';
            $html .= '<td>'.$envio.'</td>';
            $html .= '<td>'.$situacao.'</td>';
            $html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;
    }

    public function gerarRelatorioAcessoDia($resultado)
    {
        
        $dataAtual = date('d/m/Y');

        $arquivo = "relatorio de acessos ".$dataAtual.".xls";
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
		$html .= '<tr>';
		$html .= '<td style="text-align:center;" colspan="8">Relatório de acessos do dia '.$dataAtual.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>IP</b></td>';
		$html .= '<td><b>CIDADE</b></td>';
		$html .= '<td><b>ESTADO</b></td>';
		$html .= '<td><b>DISPOSITIVO</b></td>';
		$html .= '<td><b>NAVEGADOR</b></td>';
		$html .= '<td><b>S.O</b></td>';
		$html .= '<td><b>DATA/HORA</b></td>';
		$html .= '<td><b>TEMPO NA PÁGINA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {

            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  

            if($value->dispositivo_contador === '1')
            {$dispositivo = 'Celular';}else{$dispositivo = 'Computador';}

            $data = date('d/m/Y', strtotime($value->data_contador));
            $hora = date('H:i', strtotime($value->hora_contador));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->ip_contador.'</td>';
            $html .= '<td>'.$value->cidade_contador.'</td>';
            $html .= '<td>'.$value->estado_contador.'</td>';
            $html .= '<td>'.$dispositivo.'</td>';
            $html .= '<td>'.$value->navegador_contador.'</td>';
            $html .= '<td>'.$value->SO_contador.'</td>';
            $html .= '<td>'.$data.' - '.$hora.'</td>';
            $html .= '<td>'.gmdate("H:i:s", $value->tempo_contador).'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }

    public function gerarRelatorioAcessoSemana($resultado)
    {
        
        $primeiroDiaDaSemana    = date('d/m/Y', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana      = date('d/m/Y', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $arquivo = "relatorio de acessos (Semanal) entre os dias ".$primeiroDiaDaSemana." e ".$ultimoDiaDaSemana.".xls";
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
		$html .= '<tr>';
		$html .= '<td style="text-align:center;" colspan="8">Relatório de acessos <strong>(Semanal)</strong> entre os dias '.$primeiroDiaDaSemana.' e '.$ultimoDiaDaSemana.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>IP</b></td>';
		$html .= '<td><b>CIDADE</b></td>';
		$html .= '<td><b>ESTADO</b></td>';
		$html .= '<td><b>DISPOSITIVO</b></td>';
		$html .= '<td><b>NAVEGADOR</b></td>';
		$html .= '<td><b>S.O</b></td>';
		$html .= '<td><b>DATA/HORA</b></td>';
		$html .= '<td><b>TEMPO NA PÁGINA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {

            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  

            if($value->dispositivo_contador === '1')
            {$dispositivo = 'Celular';}else{$dispositivo = 'Computador';}

            $data = date('d/m/Y', strtotime($value->data_contador));
            $hora = date('H:i', strtotime($value->hora_contador));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->ip_contador.'</td>';
            $html .= '<td>'.$value->cidade_contador.'</td>';
            $html .= '<td>'.$value->estado_contador.'</td>';
            $html .= '<td>'.$dispositivo.'</td>';
            $html .= '<td>'.$value->navegador_contador.'</td>';
            $html .= '<td>'.$value->SO_contador.'</td>';
            $html .= '<td>'.$data.' - '.$hora.'</td>';
            $html .= '<td>'.gmdate("H:i:s", $value->tempo_contador).'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioAcessoMensal($resultado)
    {
        
        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $primeiroDiaDoMes = date('d/m/Y',$data_incio);
        $ultimoDiaDoMes = date('d/m/Y',$data_fim);
        
        $arquivo = "relatorio de acessos (Mensal) entre os dias ".$primeiroDiaDoMes." e ".$ultimoDiaDoMes.".xls";
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
		$html .= '<tr>';
		$html .= '<td style="text-align:center;" colspan="8">Relatório de acessos <strong>(Mensal)</strong> entre os dias '.$primeiroDiaDoMes.' e '.$ultimoDiaDoMes.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>IP</b></td>';
		$html .= '<td><b>CIDADE</b></td>';
		$html .= '<td><b>ESTADO</b></td>';
		$html .= '<td><b>DISPOSITIVO</b></td>';
		$html .= '<td><b>NAVEGADOR</b></td>';
		$html .= '<td><b>S.O</b></td>';
		$html .= '<td><b>DATA/HORA</b></td>';
		$html .= '<td><b>TEMPO NA PÁGINA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {

            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  

            if($value->dispositivo_contador === '1')
            {$dispositivo = 'Celular';}else{$dispositivo = 'Computador';}

            $data = date('d/m/Y', strtotime($value->data_contador));
            $hora = date('H:i', strtotime($value->hora_contador));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->ip_contador.'</td>';
            $html .= '<td>'.$value->cidade_contador.'</td>';
            $html .= '<td>'.$value->estado_contador.'</td>';
            $html .= '<td>'.$dispositivo.'</td>';
            $html .= '<td>'.$value->navegador_contador.'</td>';
            $html .= '<td>'.$value->SO_contador.'</td>';
            $html .= '<td>'.$data.' - '.$hora.'</td>';
            $html .= '<td>'.gmdate("H:i:s", $value->tempo_contador).'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioAcessoAnual($resultado)
    {
        
        $primeiroDiaDoAno   = '01/01/'.date('Y');
        $ultimoDiaDoAno     = '31/12/'.date('Y');
        
        $arquivo = "relatorio de acessos (Anual) entre os dias ".$primeiroDiaDoAno." e ".$ultimoDiaDoAno.".xls";
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
		$html .= '<tr>';
		$html .= '<td style="text-align:center;" colspan="8">Relatório de cadastros '.$primeiroDiaDoAno.' e '.$ultimoDiaDoAno.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>NOME</b></td>';
		$html .= '<td><b>TELEFONE</b></td>';
		$html .= '<td><b>E-MAIL</b></td>';
		$html .= '<td><b>ONDE NOS CONHECEU</b></td>';
		$html .= '<td><b>NAVEGADOR</b></td>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>LISTA</b></td>';
		$html .= '<td><b>CIDADE/ESTADO</b></td>';
		$html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {

            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  

            if($value->dispositivo_contador === '1')
            {$dispositivo = 'Celular';}else{$dispositivo = 'Computador';}

            $data = date('d/m/Y', strtotime($value->data_contador));
            $hora = date('H:i', strtotime($value->hora_contador));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->ip_contador.'</td>';
            $html .= '<td>'.$value->cidade_contador.'</td>';
            $html .= '<td>'.$value->estado_contador.'</td>';
            $html .= '<td>'.$dispositivo.'</td>';
            $html .= '<td>'.$value->navegador_contador.'</td>';
            $html .= '<td>'.$value->SO_contador.'</td>';
            $html .= '<td>'.$data.' - '.$hora.'</td>';
            $html .= '<td>'.gmdate("H:i:s", $value->tempo_contador).'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioCadastroDia($resultado)
    {
        
        $dataAtual = date('d/m/Y');

        $arquivo = "relatorio de cadastros ".$dataAtual.".xls";
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
		$html .= '<tr>';
		$html .= '<td style="text-align:center;" colspan="8">Relatório de cadastros do dia '.$dataAtual.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>NOME</b></td>';
		$html .= '<td><b>TELEFONE</b></td>';
		$html .= '<td><b>E-MAIL</b></td>';
		$html .= '<td><b>ONDE NOS CONHECEU</b></td>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>LISTA</b></td>';
		$html .= '<td><b>CIDADE/ESTADO</b></td>';
		$html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {
            
            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  
            
            $data = date('d/m/Y', strtotime($value->data_cadastros));
            $hora = date('H:i', strtotime($value->hora_cadastros));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->nome_cadastros.'</td>';
            $html .= '<td>'.$value->telefone_cadastros.'</td>';
            $html .= '<td>'.$value->email_cadastros.'</td>';
            $html .= '<td>'.$value->ondeNosConheceu_cadastros.'</td>';
            $html .= '<td>'.$value->nome_landingPage.'</td>';
            $html .= '<td>'.$value->nm_grp_emails.'</td>';
            $html .= '<td>'.$value->cidade_cadastros.'/'.$value->estado_cadastros.'</td>';
            $html .= '<td>'.$data.' às '.$hora.'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioCadastroSemanal($resultado)
    {
        
        $primeiroDiaDaSemana    = date('d/m/Y', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana      = date('d/m/Y', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $arquivo = "relatorio de cadastros (Semanal) entre os dias ".$primeiroDiaDaSemana." e ".$ultimoDiaDaSemana.".xls";
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
		$html .= '<tr>';
        $html .= '<td style="text-align:center;" colspan="8">Relatório de cadastros <strong>(Semanal)</strong> entre os dias '.$primeiroDiaDaSemana.' e '.$ultimoDiaDaSemana.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>NOME</b></td>';
		$html .= '<td><b>TELEFONE</b></td>';
		$html .= '<td><b>E-MAIL</b></td>';
		$html .= '<td><b>ONDE NOS CONHECEU</b></td>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>LISTA</b></td>';
		$html .= '<td><b>CIDADE/ESTADO</b></td>';
		$html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {
            
            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  
            
            $data = date('d/m/Y', strtotime($value->data_cadastros));
            $hora = date('H:i', strtotime($value->hora_cadastros));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->nome_cadastros.'</td>';
            $html .= '<td>'.$value->telefone_cadastros.'</td>';
            $html .= '<td>'.$value->email_cadastros.'</td>';
            $html .= '<td>'.$value->ondeNosConheceu_cadastros.'</td>';
            $html .= '<td>'.$value->nome_landingPage.'</td>';
            $html .= '<td>'.$value->nm_grp_emails.'</td>';
            $html .= '<td>'.$value->cidade_cadastros.'/'.$value->estado_cadastros.'</td>';
            $html .= '<td>'.$data.' às '.$hora.'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioCadastroMensal($resultado)
    {
        
        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));
        
        $primeiroDiaDoMes = date('d/m/Y',$data_incio);
        $ultimoDiaDoMes = date('d/m/Y',$data_fim);

        $arquivo = "relatorio de acessos (Mensal) entre os dias ".$primeiroDiaDoMes." e ".$ultimoDiaDoMes.".xls";
        
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
        $html .= '<tr>';
        $html .= '<td style="text-align:center;" colspan="8">Relatório de cadastros <strong>(Mensal)</strong> entre os dias '.$primeiroDiaDoMes.' e '.$ultimoDiaDoMes.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>NOME</b></td>';
		$html .= '<td><b>TELEFONE</b></td>';
		$html .= '<td><b>E-MAIL</b></td>';
		$html .= '<td><b>ONDE NOS CONHECEU</b></td>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>LISTA</b></td>';
		$html .= '<td><b>CIDADE/ESTADO</b></td>';
		$html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {
            
            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  
            
            $data = date('d/m/Y', strtotime($value->data_cadastros));
            $hora = date('H:i', strtotime($value->hora_cadastros));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->nome_cadastros.'</td>';
            $html .= '<td>'.$value->telefone_cadastros.'</td>';
            $html .= '<td>'.$value->email_cadastros.'</td>';
            $html .= '<td>'.$value->ondeNosConheceu_cadastros.'</td>';
            $html .= '<td>'.$value->nome_landingPage.'</td>';
            $html .= '<td>'.$value->nm_grp_emails.'</td>';
            $html .= '<td>'.$value->cidade_cadastros.'/'.$value->estado_cadastros.'</td>';
            $html .= '<td>'.$data.' às '.$hora.'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioCadastroAnual($resultado)
    {
        
        $primeiroDiaDoAno   = '01/01/'.date('Y');
        $ultimoDiaDoAno     = '31/12/'.date('Y');
        
        $arquivo = "relatorio de cadastros (Anual) entre os dias ".$primeiroDiaDoAno." e ".$ultimoDiaDoAno.".xls";        
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
        $html .= '<tr>';
        $html .= '<td style="text-align:center;" colspan="8">Relatório de cadastros '.$primeiroDiaDoAno.' e '.$ultimoDiaDoAno.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>NOME</b></td>';
		$html .= '<td><b>TELEFONE</b></td>';
		$html .= '<td><b>E-MAIL</b></td>';
		$html .= '<td><b>ONDE NOS CONHECEU</b></td>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>LISTA</b></td>';
		$html .= '<td><b>CIDADE/ESTADO</b></td>';
		$html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {
            
            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  
            
            $data = date('d/m/Y', strtotime($value->data_cadastros));
            $hora = date('H:i', strtotime($value->hora_cadastros));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->nome_cadastros.'</td>';
            $html .= '<td>'.$value->telefone_cadastros.'</td>';
            $html .= '<td>'.$value->email_cadastros.'</td>';
            $html .= '<td>'.$value->ondeNosConheceu_cadastros.'</td>';
            $html .= '<td>'.$value->nome_landingPage.'</td>';
            $html .= '<td>'.$value->nm_grp_emails.'</td>';
            $html .= '<td>'.$value->cidade_cadastros.'/'.$value->estado_cadastros.'</td>';
            $html .= '<td>'.$data.' às '.$hora.'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    
    public function gerarRelatorioClicksBotoesDia($resultado)
    {
        
        $dataAtual = date('d/m/Y');

        $arquivo = "relatorio de clicks ".$dataAtual.".xls";  
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
        $html .= '<tr>';
        $html .= '<td style="text-align:center;" colspan="4">Relatório de clicks do dia '.$dataAtual.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>NOME BOTÃO</b></td>';
		$html .= '<td><b>LINK</b></td>';
        $html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {
            
            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  
            
            $data = date('d/m/Y', strtotime($value->dataClick__clicks));
            $hora = date('H:i', strtotime($value->horaClick__clicks));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->nome_landingPage.'</td>';
            $html .= '<td>'.$value->nomeBotao_clicks.'</td>';
            $html .= '<td>'.$value->linkBotao_clicks.'</td>';
            $html .= '<td>'.$data.' às '.$hora.'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioClicksBotoesSemanal($resultado)
    {
        
        $primeiroDiaDaSemana    = date('d/m/Y', strtotime('sunday last week', strtotime(date('Ymd'))));
        $ultimoDiaDaSemana      = date('d/m/Y', strtotime('saturday this week', strtotime(date('Ymd'))));
        
        $arquivo = "relatorio de clicks (Semanal) entre os dias ".$primeiroDiaDaSemana." e ".$ultimoDiaDaSemana.".xls";

        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
        $html .= '<tr>';
        $html .= '<td style="text-align:center;" colspan="4">Relatório de acessos <strong>(Semanal)</strong> entre os dias '.$primeiroDiaDaSemana.' e '.$ultimoDiaDaSemana.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>NOME BOTÃO</b></td>';
		$html .= '<td><b>LINK</b></td>';
        $html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {
            
            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  
            
            $data = date('d/m/Y', strtotime($value->dataClick__clicks));
            $hora = date('H:i', strtotime($value->horaClick__clicks));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->nome_landingPage.'</td>';
            $html .= '<td>'.$value->nomeBotao_clicks.'</td>';
            $html .= '<td>'.$value->linkBotao_clicks.'</td>';
            $html .= '<td>'.$data.' às '.$hora.'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioClicksBotoesMensal($resultado)
    {
        $data_incio = mktime(0, 0, 0, date('m') , 1 , date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $primeiroDiaDoMes = date('d/m/Y',$data_incio);
        $ultimoDiaDoMes = date('d/m/Y',$data_fim);
        
        $arquivo = "relatorio de clicks (Mensal) entre os dias ".$primeiroDiaDoMes." e ".$ultimoDiaDoMes.".xls";
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
        $html .= '<tr>';
        $html .= '<td style="text-align:center;" colspan="4">Relatório de clicks <strong>(Mensal)</strong> entre os dias '.$primeiroDiaDoMes.' e '.$ultimoDiaDoMes.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>NOME BOTÃO</b></td>';
		$html .= '<td><b>LINK</b></td>';
        $html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {
            
            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  
            
            $data = date('d/m/Y', strtotime($value->dataClick__clicks));
            $hora = date('H:i', strtotime($value->horaClick__clicks));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->nome_landingPage.'</td>';
            $html .= '<td>'.$value->nomeBotao_clicks.'</td>';
            $html .= '<td>'.$value->linkBotao_clicks.'</td>';
            $html .= '<td>'.$data.' às '.$hora.'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
    public function gerarRelatorioClicksBotoesAnual($resultado)
    {
        
        $primeiroDiaDoAno   = '01/01/'.date('Y');
        $ultimoDiaDoAno     = '31/12/'.date('Y');
        
        $arquivo = "relatorio de clicks (Anual) entre os dias ".$primeiroDiaDoAno." e ".$ultimoDiaDoAno.".xls";
        
        $html = '';
        
		$html = '<!DOCTYPE html><html lang="pt-br"><head>
                    <meta charset="utf-8">
                    <title>Contato</title>
                    <head>
                    <body>';

		$html .= '<table border="1">';
		$html .= '<tr>';
		$html .= '<td style="text-align:center;" colspan="4">Relatório de clicks '.$primeiroDiaDoAno.' e '.$ultimoDiaDoAno.'</tr>';
        $html .= '</tr>';
        
		$html .= '<tr>';
		$html .= '<td><b>LANDINGPAGE</b></td>';
		$html .= '<td><b>NOME BOTÃO</b></td>';
		$html .= '<td><b>LINK</b></td>';
        $html .= '<td><b>DATA/HORA</b></td>';
        $html .= '</tr>';
        
		foreach ($resultado as $value) {
            
            setlocale(LC_ALL, NULL);
            setlocale(LC_ALL, 'pt_BR');  
            
            $data = date('d/m/Y', strtotime($value->dataClick__clicks));
            $hora = date('H:i', strtotime($value->horaClick__clicks));
            
            $html .= '<tr>';
            $html .= '<td>'.$value->nome_landingPage.'</td>';
            $html .= '<td>'.$value->nomeBotao_clicks.'</td>';
            $html .= '<td>'.$value->linkBotao_clicks.'</td>';
            $html .= '<td>'.$data.' às '.$hora.'</td>';
			$html .= '</tr>';
        }
        
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: application/x-msexcel");
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );
        
        $html .= '</body></html>';
        
        echo $html;

    }
    
}