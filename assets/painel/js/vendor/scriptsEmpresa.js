$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

listarInformacoesEmpresa();

function listarInformacoesEmpresa() {

    $.ajax({

        url: "/dashboard/listagens/listarInformacoesEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            if (dados.status_empresa === '1') {

                $('#avisoAlteracoes').html(
                    '<div class="alert alert-danger alert-dismissible bg-danger text-center text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<i class="mdi mdi-alert-circle"></i> Você não pode mais alterar os dados da empresa, para fazer alterações, entre em contato via suporte!' +
                    '</div>');

                $('#razaoSocialEmpresa').val(dados.razaoSocial_empresa).prop("disabled", true);
                $('#nomeFantasiaEmpresa').val(dados.nomeFantasia_empresa).prop("disabled", true);
                $("#nomeEmpresaBarra").html(dados.nomeFantasia_empresa).prop("disabled", true);
                $('#cnpjEmpresa').val(dados.cnpj_empresa).prop("disabled", true);
                $('#emailEmpresa').val(dados.email_empresa).prop("disabled", true);
                $('#telefoneEmpresa').val(dados.telefone_empresa).prop("disabled", true);
                $('#nomeResponsavelEmpresa').val(dados.nomeResponsavel_empresa).prop("disabled", true);
                $('#telefoneResponsavelEmpresa').val(dados.telefoneResponsavel_empresa).prop("disabled", true);
                $('#emailResponsavelEmpresa').val(dados.emailResponsavel_empresa).prop("disabled", true);
                $('#cpfResponsavelEmpresa').val(dados.cpfResponsavel_empresa).prop("disabled", true);
                $('#arquivoLogotipoEmpresa').prop("disabled", true);
                $('#botaoVerLogotipo').prop("disabled", true);
                $('#botaoAtualizarDadosEmpresa').prop("disabled", true);
                $("#verLogotipoEmpresa").attr("src", "/assets/painel/images/empresa/logotipo/" + dados.imagem_empresa);
                $('#imagemEmpresaBarra').attr("src", "/assets/painel/images/empresa/logotipo/" + dados.imagem_empresa);

            } else {

                $('#avisoAlteracoes').html(
                    '<div class="alert alert-primary alert-dismissible bg-primary text-center text-white border-0 fade show" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                    '</button>' +
                    '<i class="mdi mdi-alert-circle-outline"></i> Atenção!! Só será possível alterar os dados da empresa uma vez, tenha bastante atenção ao preencher os dados da empresa.' +
                    '</div>');

                $('#razaoSocialEmpresa').val(dados.razaoSocial_empresa);
                $('#nomeFantasiaEmpresa').val(dados.nomeFantasia_empresa);
                $("#nomeEmpresaBarra").html(dados.nomeFantasia_empresa);
                $('#cnpjEmpresa').val(dados.cnpj_empresa);
                $('#emailEmpresa').val(dados.email_empresa);
                $('#telefoneEmpresa').val(dados.telefone_empresa);
                $('#nomeResponsavelEmpresa').val(dados.nomeResponsavel_empresa);
                $('#telefoneResponsavelEmpresa').val(dados.telefoneResponsavel_empresa);
                $('#emailResponsavelEmpresa').val(dados.emailResponsavel_empresa);
                $('#cpfResponsavelEmpresa').val(dados.cpfResponsavel_empresa);
                $("#verLogotipoEmpresa").attr("src", "/assets/painel/images/empresa/logotipo/" + dados.imagem_empresa);
                $('#imagemEmpresaBarra').attr("src", "/assets/painel/images/empresa/logotipo/" + dados.imagem_empresa);

            }



        }
    });
}

$('#formEmpresa').submit(function(e) {
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarDadosEmpresa(self);
});

function atualizarDadosEmpresa(dados) {

    var form = $('#formEmpresa')[0];

    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarDadosEmpresa",
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        timeout: 400000,


        success: function(retorno) {

            if (retorno.ret == false) {

                $('#msgErro').html(

                    '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                    '<h5 class="text-white">Erro!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);


            } else {

                $('#msgErro').html(

                    '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
                    '<h5 class="text-white">Tudo certo!</h5>' +
                    retorno.msg +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>'
                );

                $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

                $('html,body').scrollTop(0);

                listarInformacoesEmpresa();

            }
        }
    });
}