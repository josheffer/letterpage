<style>

.destaque{
    background-color: #ffffff3b !important;
}

.card-pricing-recommended {
    background-color: #3b434e; 
    color: #56c2d6;
    border-color: #56c2d6;
}

</style>

<div class="content-page">
    
    <div class="content">
        
        <div class="container-fluid">
            
            <div class="row mt-4">
                
                <div class="col-12">
                    
                    <div class="row">

                        <div class="col-lg-12">

                            <div id="erroMsgEscolherPlano"></div>
                            
                            <div class="card">

                                <div class="card-body">
                                    
                                    <div class="row justify-content-center">
                                        <div class="col-xl-10">

                                            <!-- Pricing Title-->
                                            <div class="text-center mb-4">
                                                <h3 class="mb-2">Atualize seu plano!</h3>
                                                <p class="text-muted w-50 m-auto">
                                                    Escolha um plano para melhor atender sua necessidade!
                                                </p>
                                            </div>
                                            
                                            <div class="row my-3">

                                            <?php 

                                                foreach ($planos as $planosRow) : 
                                                    
                                                    if($planosRow->meses_planos > 1)
                                                    {
                                                        $mes = "meses";

                                                    } else {

                                                        $mes = "mês";
                                                    }
                                                    
                                                    if($meuPlano ==  $planosRow->id_planos)
                                                    {
                                                        
                                                        $planoAtual =  '<button type="submit" class="btn btn-xs btn-success btn-block waves-effect waves-light" disabled="disabled">
                                                                                <i class="mdi mdi-check-all"></i> Atual
                                                                            </button>';
                                                    } else {

                                                        $planoAtual = '<button type="submit" id="botaoPlanoAtual" class="btn btn-xs btn-primary btn-block waves-effect waves-light">Atualizar</button>';

                                                    }
                                                
                                                ?>
                                                
                                                
                                                    <div class="col-md-4">
                                                        <div class="card card-pricing card-pricing-recommended">
                                                            <div class="card-body text-center">
                                                                <h3 class="card-pricing-plan-name font-weight-bold text-uppercase"><?=$planosRow->nome_planos?></h3>
                                                                
                                                                <h2 class="card-pricing-price text-info mb-3">R$ <?=$planosRow->valor_planos?> <br><span>(<?=$planosRow->meses_planos?> <?=$mes?>)</span></h2>
                                                                
                                                                <span>
                                                                    <?=$planosRow->descricao_planos?>
                                                                </span>

                                                                <form id="escolherPlano<?=$planosRow->id_planos?>">

                                                                    <input type="hidden" name="idPlanos" id="idPlanos" value="<?=$planosRow->id_planos?>">
                                                                    <input type="hidden" name="idPlanoAtual" id="idPlanoAtual" value="<?=$meuPlano?>">

                                                                    <?=$planoAtual?>

                                                                </form>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                <?php endforeach;
                                                
                                            ?>


                                            
                                            </div>
                                            
                                        </div> 
                                    </div>
                                    
                                </div>

                                
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
</div>

<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>