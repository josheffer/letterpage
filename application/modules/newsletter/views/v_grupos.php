<div class="content-page">

<div class="content">
    
    <div class="container-fluid mt-3">
        
        <div class="row">

            <div class="col-12">

                <?php
                    $this->load->view('botoesAll');
                ?>
                
                <div class="page-title-box">
                    
                    <div class="row">
                        
                        <div class="col-md-12">

                        <div id="mensagemCadastroGrupo" class="mt-2 mb-2"></div>

                            <div class="card">
                                <h6 class="card-header">Cadastrar novo grupo</h6>
                                
                                <div class="card-body">
                                    
                                    <form id="formCadastrarNovoGrupo">
                                        <h6 class="font-13 mt-3">Nome do grupo</h6>
                                        
                                        <div class="form-row align-items-center">
                                            
                                            <div class="col-md-9 col-sm-9 col-xl-9 col-lg-9">
                                                <input type="text" class="form-control mb-2" id="nomeGrupoCadastro" name="nomeGrupoCadastro" autocomplete="off" autofocus>
                                            </div>
                                            
                                            <div class="col-md-3 col-sm-3 col-xl-3 col-lg-3">
                                                <div id="botaoCadastrarGrupo">
                                                    <button type="submit" class="btn btn-success waves-effect waves-light mb-2 btn-block">Cadastrar</button>
                                                </div>
                                            </div>

                                        </div>

                                    </form>

                                </div>

                            </div> 
                        </div>
                        
                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            <div id="mensagemSUCESSOgrupo"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12">
                            <div id="accordion" class="mb-3">
                                <div class="card mb-1">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="m-0">
                                            <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                                Grupos cadastrados
                                            </a>
                                        </h5>
                                    </div>
                        
                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                        <div class="card-body">

                                            <div class="table-responsive">
                                                <table class="table mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nome</th>
                                                        <th>Ações</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tabelaListarGrupos"></tbody>
                                                </table>
                                            </div>
                                            

                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                    
                </div>

            </div>

        </div>     
            
    </div>

</div> 
    
</div>


<!-- MODAL ALTERAR GRUPO -->
<!-- MODAL ALTERAR GRUPO -->
<div id="ModalModificarGrupo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Modificar grupo: <span id="tittuloGrupo"></span></h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div id="mensagemERROAlterargrupo" class="col-md-12 mt-2"></div>
        
        <form id="formEditarGrupo">

            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="idGrupo" name="idGrupo">

                            <label for="alterarNomeGrupo" class="control-label">Nome</label>
                            <input type="text" class="form-control" id="alterarNomeGrupo" name="alterarNomeGrupo" autocomplete="off">
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect waves-light">Modificar</button>
            </div>
        </form>

    </div>
</div>
</div>
<!-- MODAL ALTERAR GRUPO -->
<!-- MODAL ALTERAR GRUPO -->

<!-- MODAL DELETAR GRUPO -->
<!-- MODAL DELETAR GRUPO -->
<div id="ModalExcluirGrupo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Excluir grupo: <span id="tituloGrupoExcluir"></span></h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>

        <div id="mensagemERROAExcluirgrupo" class="col-md-12 mt-2"></div>
        
        <form id="formExcluirGrupo">

            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="idGrupoExcluir" name="idGrupoExcluir">
                            <p>Deseja realmente excluir o grupo: 
                                <strong><span id="excluirNomeGrupo"></span></strong>?
                                Após a exclusão não será possível reverter essa ação! Tem certeza?
                            </p>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-danger waves-effect waves-light">Excluir</button>
            </div>
        </form>

    </div>
</div>
</div>
<!-- MODAL DELETAR GRUPO -->
<!-- MODAL DELETAR GRUPO -->


