<?php

date_default_timezone_set('America/Sao_Paulo');

defined('BASEPATH') OR exit('No direct script access allowed');

class Permissoes_model extends CI_Model {
    
    
    public function m_gerarCodigoCriarPagina() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_pagina) as codigo FROM pu_paginas");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }
    
    public function m_gerarCodigoCriarNivelAcesso() 
    {
        
        $result =  $this->db->query("SELECT MAX(id_nivelAcesso) as codigo FROM pu_nivelacesso");

        foreach ($result->result_array() as $dados)
        {
            
            if(is_null($dados['codigo'])) return 1;

            return intval($dados['codigo'])+1;
        }
        
    }

    public function m_criarNovaPagina($dados)
    {
        
        $this->db->select("id_nivelAcesso");
        $this->db->order_by('id_nivelAcesso', 'DESC');
        $resultado = $this->db->get("pu_nivelacesso")->result();

        foreach ($resultado as $key) {
            
            $this->db->query("INSERT INTO pu_niveis_paginas 
                        VALUES (NULL, '".$key->id_nivelAcesso."', '".$dados['id_pagina']."', '0', '0', '0')");
        }
        
        return $this->db->insert('pu_paginas', $dados);
    }

    public function verificarPaginaExiste($endereco)
    {
        $resultado = $this->db->query("SELECT * FROM pu_paginas WHERE endereco_pagina = '".$endereco."';")->result_object();

        foreach ($resultado as $value) {
            return $value;
        }
    }
    
    public function verificarNivelExiste($nome)
    {
        $resultado = $this->db->query("SELECT * FROM pu_nivelacesso WHERE nome_nivelAcesso = '".$nome."';")->result_object();

        foreach ($resultado as $value) {
            return $value;
        }
    }

    public function m_listarPaginas()
    {

        $this->db->select("*");
        $this->db->order_by('id_pagina', 'DESC');
        $resultado = $this->db->get("pu_paginas")->result();

        return $resultado;
    
    }

    public function m_atualizarDadosPagina($dados)
    {
        $resultado = $this->db->query("UPDATE pu_paginas SET 
                                                            nome_pagina = '".$dados['nome_pagina']."',
                                                            endereco_pagina = '".$dados['endereco_pagina']."',
                                                            observacao_pagina = '".$dados['observacao_pagina']."',
                                                            icone_pagina = '".$dados['icone_pagina']."' 
                                                            WHERE id_pagina = '".$dados['id_pagina']."';");
        
        return $resultado;
    }

    public function m_excluirPagina($id)
    {
        $this->db->select("id_nivelAcesso");
        $this->db->order_by('id_nivelAcesso', 'DESC');
        $resul = $this->db->get("pu_nivelacesso")->result();

        foreach ($resul as $key) {
            
            $this->db->query("DELETE FROM pu_niveis_paginas WHERE (id_pagina = '".$id."');");
        }

        $resultado = $this->db->query("DELETE FROM pu_paginas WHERE id_pagina = '".$id."';");
        
        return $resultado;
    }

    public function m_criarNivelAcesso($dados)
    {
        return $this->db->insert('pu_nivelacesso', $dados);
    }

    public function m_listarNiveisAcesso()
    {

        $this->db->select("*");
        $this->db->order_by('id_nivelAcesso', 'DESC');
        $resultado = $this->db->get("pu_nivelacesso")->result();

        return $resultado;
    
    }

    public function m_atualizarNivelAcesso($dados)
    {
        $resultado = $this->db->query("UPDATE pu_nivelacesso SET 
                                                nome_nivelAcesso        = '".$dados['nome_nivelAcesso']."',
                                                cor_nivelAcesso         = '".$dados['cor_nivelAcesso']."',
                                                corTexto_nivelAcesso    = '".$dados['corTexto_nivelAcesso']."'
                                                WHERE id_nivelAcesso    = '".$dados['id_nivelAcesso']."';");

        return $resultado;
    }

    public function m_deletarNivelAcesso($id)
    {
        $resultado = $this->db->query("DELETE FROM pu_nivelacesso WHERE id_nivelAcesso = '".$id."';");
        
        return $resultado;
    }

    public function m_listaNiveisPaginasDisponiveis($id)
    {
        $resultado = $this->db->query(
                                "SELECT 
                                a.id_pagina, a.nome_pagina, 
                                b.add_niveisPaginas, b.id_niveisPaginas, c.nome_nivelAcesso
                            
                                FROM pu_paginas as a
                                
                                LEFT JOIN pu_niveis_paginas as b
                                ON (a.id_pagina = b.id_pagina)
                                
                                LEFT JOIN pu_nivelacesso AS c
                                ON (b.id_nivelAcesso = c.id_nivelAcesso)
                                
                                WHERE b.id_nivelAcesso = '".$id."'
                                AND  b.add_niveisPaginas = '0'
                                GROUP BY a.id_pagina, a.nome_pagina, 
                                b.add_niveisPaginas, b.id_niveisPaginas, c.nome_nivelAcesso;")->result_object();
                                
        return $resultado;
    }
    
    public function m_listaNiveisPaginasADD($id)
    {
        $resultado = $this->db->query(
                                "SELECT 
                                a.id_pagina, a.nome_pagina, 
                                b.add_niveisPaginas, b.id_niveisPaginas, 
                                b.permissao_niveisPaginas, b.menu_niveisPaginas,
                                c.nome_nivelAcesso
                            
                                FROM pu_paginas as a
                                
                                LEFT JOIN pu_niveis_paginas as b
                                ON (a.id_pagina = b.id_pagina)
                                
                                LEFT JOIN pu_nivelacesso AS c
                                ON (b.id_nivelAcesso = c.id_nivelAcesso)
                                
                                WHERE b.id_nivelAcesso = '".$id."'
                                AND  b.add_niveisPaginas = '1'
                                GROUP BY a.id_pagina, a.nome_pagina, 
                                b.add_niveisPaginas, b.id_niveisPaginas, 
                                b.permissao_niveisPaginas, b.menu_niveisPaginas,
                                c.nome_nivelAcesso
                                ORDER BY a.id_pagina DESC;")->result_object();

        return $resultado;
    }

    public function m_associarRemoverPagina($dados)
    {
        $resultado = $this->db->query("UPDATE pu_niveis_paginas SET 
                                        add_niveisPaginas           = '".$dados['add_niveisPaginas']."',
                                        permissao_niveisPaginas     = '".$dados['permissao_niveisPaginas']."',
                                        menu_niveisPaginas          = '".$dados['menu_niveisPaginas']."'
                                        WHERE id_pagina             = '".$dados['id_pagina']."'
                                        AND id_niveisPaginas        = '".$dados['id_niveisPaginas']."';");

        return $resultado;
    }
    
    public function m_liberarBloquearPermissao($dados)
    {
        $resultado = $this->db->query("UPDATE pu_niveis_paginas SET 
                                        permissao_niveisPaginas     = '".$dados['permissao_niveisPaginas']."'
                                        WHERE id_pagina             = '".$dados['id_pagina']."'
                                        AND id_niveisPaginas        = '".$dados['id_niveisPaginas']."';");

        return $resultado;
    }
    
    public function m_liberarBloquearMenu($dados)
    {
        $resultado = $this->db->query("UPDATE pu_niveis_paginas SET 
                                        menu_niveisPaginas      = '".$dados['menu_niveisPaginas']."'
                                        WHERE id_pagina         = '".$dados['id_pagina']."'
                                        AND id_niveisPaginas    = '".$dados['id_niveisPaginas']."';");

        return $resultado;
    }

    public function buscarPermissaoAcesso($id)
    {   
        

        $resultado = $this->db->query("SELECT * FROM pu_niveis_paginas AS a
                                        INNER JOIN pu_paginas AS b
                                        ON a.id_pagina = b.id_pagina
                                        WHERE a.id_nivelAcesso = '".$id."'
                                        AND add_niveisPaginas = '1'
                                        AND menu_niveisPaginas = '1';"
                                    )->result_object();
                                    
                                        
        return $resultado;

    }

    public function buscarPermissaoAcessoPagina($id)
    {   
        

        $resultado = $this->db->query("SELECT * FROM pu_niveis_paginas AS a
                                        INNER JOIN pu_paginas AS b
                                        ON a.id_pagina = b.id_pagina
                                        WHERE a.id_nivelAcesso = '".$id."';"
                                    )->result_object();
                                    
                                        
        return $resultado;

    }
    
    public function buscarPrmissaoAcesarPagina($infos)
    {
        $resultado = $this->db->query("SELECT a.id_nivelAcesso, a.permissao_niveisPaginas, 
                                        b.id_pagina, b.nome_pagina, b.endereco_pagina, a.add_niveisPaginas
                                        FROM pu_niveis_paginas as a
                                        INNER JOIN pu_paginas as b
                                        ON a.id_pagina = b.id_pagina
                                        where a.id_nivelAcesso = '".$infos['id_nivelAcesso']."'
                                        AND b.endereco_pagina = '".$infos['endereco_pagina']."';")->result_object();
        
        foreach ($resultado as $value) {

            return $value;
        }
    }

    public function buscarPrmissaoAcesarBotoes($infos)
    {
        $resultado = $this->db->query("SELECT a.id_nivelAcesso, a.permissao_niveisPaginas, 
                                        b.id_pagina, b.nome_pagina, b.endereco_pagina, a.add_niveisPaginas
                                        FROM letterpage.pu_niveis_paginas as a
                                        INNER JOIN pu_paginas as b
                                        ON a.id_pagina = b.id_pagina
                                        where a.id_nivelAcesso = '".$infos['id_nivelAcesso']."'
                                        AND b.endereco_pagina = '".$infos['endereco_pagina']."';")->result_object();
        
        foreach ($resultado as $value) {

            return $value;
        }
    }
    
}