$(document).ready(function() {
    $('#botaoMobileAbrirEFechar').trigger('click');
});

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarEnviosAndamento();
listarEnviosEnviadosOK();

function listarEnviosAndamento() {

    $.ajax({

        url: "/listagens/listarEnviosAndamento",

        success: function(data) {

            var dados = JSON.parse(data);

            if (dados.x === false) {

                $('#paineisEnvios').html(
                    '<div class="col-md-12"><center class="text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma campanha sendo disparada no momento!!' +
                    '</div>' +
                    '</center></div>'
                );

            } else {

                $('#paineisEnvios').html('');

                if (dados.r.length > 0) {

                    for (var i = 0; i < dados.r.length; i++) {

                        if (dados.r[i].porcentagem == 100) {

                            var mensagem = '<h5><span class="text-success"> Envio concluído! </span></h5>';

                        } else {

                            var mensagem = '<h5><span class="text-warning"> Em andamento... </span></h5>';

                        }

                        $('#paineisEnvios').append(

                            '<div class="col-md-6">' +
                            '<div class="card">' +
                            '<h6 class="card-header">' + dados.r[i].nome_campanha_envios + '</h6>' +
                            '<div class="card-body bg-dark">' +
                            '<center>' + mensagem + '</center>' +
                            '<div class="progress mb-0">' +
                            '<div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: ' + dados.r[i].porcentagem + '%;" aria-valuenow="' + dados.r[i].porcentagem + '" aria-valuemin="0" aria-valuemax="100">' + dados.r[i].porcentagem + ' % ' + '</div>' +
                            '</div>' +
                            '<div class="row justify-content-center mt-2">' +
                            '<h4><span class="badge bg-primary">Total <span class="badge badge-pill bg-white text-primary ml-1">' + dados.r[i].total + '</span></span></h4>' +
                            '<h4><span class="badge bg-info ml-2">Pendentes <span class="badge badge-pill bg-white text-primary ml-1">' + dados.r[i].status_zero_pendente + '</span></span></h4>' +
                            '<h4><span class="badge bg-success ml-2">Enviadas <span class="badge badge-pill bg-white text-primary ml-1">' + dados.r[i].status_um_enviado + '</span></span></h4>' +
                            '<h4><span class="badge bg-soft-red ml-2">Erro <span class="badge badge-pill bg-white text-primary ml-1">' + dados.r[i].status_dois_erro + '</span></span></h4>' +
                            '<h4><a href="/dashboard/listagens/relatorioEnvios/'+ dados.r[i].id_campanha +'" target="_blank"><button type="button" class="btn btn-success btn-xs waves-effect waves-light ml-1" title="Download do relatório"><i class=" mdi mdi-download"></i></button></a></h4>' +
                            '</div>' +


                            '</div>' +
                            '</div>' +
                            '</div>'
                        );
                    }
                }
            }
        }
    });

    setTimeout(listarEnviosAndamento, 5000);

}

function listarEnviosEnviadosOK() {

    $.ajax({

        url: "/listagens/listarEnviosEnviadosOK",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);

            $('#tabelaCampanhasEnviadas').html('');

            dadosGlobaisCampOk = dados;

            if (dados.CampEnviada.length > 0) {

                for (var i = 0; i < dados.CampEnviada.length; i++) {

                    console.log(dados.CampEnviada[i]);

                    $('#tabelaCampanhasEnviadas').append(
                        '<tr>' +
                        '<td><h5 class="m-0 font-weight-normal">' + dados.CampEnviada[i].id_campanha + '</h5></td>' +

                        '<td>' + dados.CampEnviada[i].nome_campanha + '</td>' +

                        '<td>' + dados.CampEnviada[i].assunto_campanha + '</td>' +

                        '<td>' +
                        '<button type="button" class="btn btn-success waves-effect waves-light">' +
                        '<span class="btn-label"><i class="fas fa-download"></i></span>Download (.xls)' +
                        '</button>' +
                        '</td>' +

                        '<td>' +
                        '<p class="text-center">' +
                        '<button class="btn btn-primary waves-effect waves-light" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample">' +
                        'Ver dados' +
                        '</button>' +
                        '</p>' +

                        '<div class="collapse" id="collapseExample" style="">' +
                        '<div class="row justify-content-center">' +
                        '<h4><span class="badge bg-primary">Total <span class="badge badge-pill bg-white text-primary ml-1">9999</span></span></h4>' +
                        '<h4><span class="badge bg-info ml-2">Pendentes <span class="badge badge-pill bg-white text-primary ml-1">9999</span></span></h4>' +
                        '<h4><span class="badge bg-success ml-2">Enviadas <span class="badge badge-pill bg-white text-primary ml-1">9999</span></span></h4>' +
                        '<h4> <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"><span class="badge bg-soft-red ml-2">Erro <span class="badge badge-pill bg-white text-primary ml-1">9999</span></span></a></h4>' +
                        '</div>' +
                        '</div>' +
                        '</td>' +
                        '</tr>'

                    );

                }

            } else {

                $('#tabelaCampanhasEnviadas').append(
                    '<tr>' +
                    '<td scope="row" colspan="7" class="text-center col-md-1">' +
                    '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                    '<div class="alert alert-danger text-center">' +
                    '<i class="fas fa-exclamation-circle"></i> Nenhuma campanha terminou de ser enviada!!' +
                    '</div>' +
                    '</div>' +
                    '</center>' +
                    '</td>' +
                    '</tr>'
                );

            }
        }
    });
}