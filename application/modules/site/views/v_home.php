<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="twitter:site" content="@solidsystem">
    <meta name="twitter:creator" content="@solidsystem">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Solid System">
    <meta name="twitter:description" content="Solid System Goiânia">

    <meta property="og:url" content="https://solidsystem.net.br/">
    <meta property="og:title" content="Solid System">
    <meta property="og:description" content="Solid System Goiânia">

    <meta property="og:image" content="img/SolidSystem.png">
    <meta property="og:image:secure_url" content="/assets/sites/solidsystem/img/SolidSystem.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1920">
    <meta property="og:image:height" content="930">

    <meta name="description" content="Desenvolvimento de Software em Goiânia">
    <meta name="author" content="SolidSystem">

    <link rel="shortcut icon" type="image/x-icon" href="/assets/sites/solidsystem/img/favicon.png">

    <title><?=$titulo?></title>

    <link rel="stylesheet" href="/assets/sites/solidsystem/css/dashforge.css">
    <link rel="stylesheet" href="/assets/sites/solidsystem/css/dashforge.auth.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>

    <div class="content content-fixed content-auth-alt">
        <div class="container ht-100p tx-center">
            <div class="ht-100p d-flex flex-column align-items-center justify-content-center">

                <div class="wd-70p wd-sm-250 wd-lg-400 mg-b-15">
                    <img src="/assets/sites/solidsystem/img/SolidSystem.png" class="img-fluid" alt="SolidSystem - Softwares inteligentes!">
                </div>

                <br>

                <h1 class="tx-color-01 tx-24 tx-sm-32 tx-lg-36 mg-xl-b-5">Ainda estamos trabalhando em nosso site.</h1>

                <h5 class="tx-16 tx-sm-18 tx-lg-20 tx-normal mg-b-20">Em breve estaremos com nosso site online!</h5>

                <div class="mg-b-40">
                    <a href="https://api.whatsapp.com/send?phone=5562981927179&text=Ol%C3%A1,%20venho%20atrav%C3%A9s%20do%20site,%20quero%20saber%20mais%20sobre%20os%20servi%C3%A7os." target="_blank" rel="noopener noreferrer">
                        <button class="btn btn-white bd-2 pd-x-30"> <i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</button>
                    </a>

                    <button class="btn btn-white bd-2 pd-x-30"> <i class="fa fa-envelope" aria-hidden="true"></i> contato@solidsystem.net.br</button>
                </div>

            </div>
        </div>

    </div>

    <footer class="footer">
        <div>
            <span>&copy; <?=date('Y')?> Solid System. Todos os direitos reservados!</span>
        </div>
        <div>
            <nav class="nav">
                <a href="" class="nav-link"></a>
                <a href="" class="nav-link"></a>
                <a href="" class="nav-link"></a>
            </nav>
        </div>
    </footer>


</body>

</html>