
<div class="content-page">

    <div class="content">
                    
        <div class="container-fluid">

            <div class="row no-gutters mt-3">
                                    
                <div class="col-md-6 col-xl-4">
                    <a href="/dashboard/landingPages">
                        <div class="bg-soft-primary rounded-0 card-box mb-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <h3 class="text-dark"><span>Landing Pages</span></h3>
                                        <p class="text-warning mb-1 text-truncate">Todas as landing pages</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-xl-4">
                    <a href="/dashboard/configuracoes">
                        <div class="bg-soft-danger rounded-0 card-box mb-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <h3 class="text-dark"><span>Configurações</span></h3>
                                        <p class="text-warning mb-1 text-truncate">Logo, favicon, telefone, mapa..</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-xl-4">
                    <a href="/dashboard/templates">
                        <div class="bg-soft-info rounded-0 card-box mb-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <h3 class="text-dark"><span>Template</span></h3>
                                        <p class="text-warning mb-1 text-truncate">Configurações de templates</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
            </div>

            <div class="row mt-3">
                <div class="col-xl-12">
                    <div id="accordion" class="mb-3">
                        <div class="card mb-1">
                            <div class="card-header" id="headingOne">
                                <h5 class="m-0 text-center">
                                    <a class="text-dark collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                        Relatório de acessos <span class="badge badge-dark badge-pill"><i class="fe-trending-up"></i></span>
                                    </a>
                                </h5>
                            </div>
                
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="background-color: #30343c;">
                                <div class="card-body">

                                    <div class="row">

                                        <div class="col-12">
                                            <div class="text-center">
                                                <h4 class="mb-3"></h4>
                                            </div>
                                        </div>
                                                    
                                        <div class="col-12 justify-content-center">
                                            
                                            <div class="row justify-content-center">

                                                <div class="col-md-6 col-xl-3">
                                                    <div class="card-box">
                                                        <div class="row">
                                                            
                                                            <div class="col-3">
                                                                
                                                                <button type="button" onclick="javascript:abrirRelatorioDiario();" class="btn btn-info waves-effect waves-info" data-toggle="tooltip" data-placement="top" title="Clique para obter informações sobre os acessos do dia" data-original-title="Clique para obter informações sobre os acessos do dia">
                                                                    <i class="fe-bar-chart-2 avatar-title font-22"></i>
                                                                </button>
                                                                
                                                            </div>

                                                            <div class="col-9">
                                                                <div class="text-right">
                                                                    <h3 class="text-dark my-1"><span data-plugin="counterup" id="acessosDiario" name="acessosDiario"></span></h3>
                                                                    <p class="text-muted mb-1 text-truncate">Diário</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-xl-3">
                                                    <div class="card-box">
                                                        <div class="row">

                                                            <div class="col-3">
                                                                
                                                                <button type="button" onclick="javascript:abrirRelatorioSemana();" class="btn btn-info waves-effect waves-info" data-toggle="tooltip" data-placement="top" title="Clique para obter informações sobre os acessos da semana" data-original-title="Clique para obter informações sobre os acessos do dia">
                                                                    <i class="fe-bar-chart-2 avatar-title font-22"></i>
                                                                </button>
                                                                
                                                            </div>

                                                            <div class="col-9">
                                                                <div class="text-right">
                                                                    <h3 class="text-dark my-1"><span data-plugin="counterup" id="acessosSemanais" name="acessosSemanais"></span></h3>
                                                                    <p class="text-muted mb-1 text-truncate">Semanal</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-xl-3">
                                                    <div class="card-box">
                                                        <div class="row">
                                                        <div class="col-3">
                                                                
                                                                <button type="button" onclick="javascript:abrirRelatorioMensal();" class="btn btn-info waves-effect waves-info" data-toggle="tooltip" data-placement="top" title="Clique para obter informações sobre os acessos do mês" data-original-title="Clique para obter informações sobre os acessos do mês">
                                                                    <i class="fe-bar-chart-2 avatar-title font-22"></i>
                                                                </button>
                                                                
                                                            </div>
                                                            <div class="col-9">
                                                                <div class="text-right">
                                                                    <h3 class="text-dark my-1"><span data-plugin="counterup" id="acessosMensal" name="acessosMensal"></span></h3>
                                                                    <p class="text-muted mb-1 text-truncate">Mensal</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-xl-3">
                                                    <div class="card-box">
                                                        <div class="row">
                                                        <div class="col-3">
                                                                
                                                                <button type="button" onclick="javascript:abrirRelatorioAnual();" class="btn btn-info waves-effect waves-info" data-toggle="tooltip" data-placement="top" title="Clique para obter informações sobre os acessos do ano" data-original-title="Clique para obter informações sobre os acessos do ano">
                                                                    <i class="fe-bar-chart-2 avatar-title font-22"></i>
                                                                </button>
                                                                
                                                            </div>
                                                            <div class="col-9">
                                                                <div class="text-right">
                                                                    <h3 class="text-dark my-1"><span data-plugin="counterup" id="acessosAnual" name="acessosAnual"></span></h3>
                                                                    <p class="text-muted mb-1 text-truncate">Anual</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-12">
                                            <div class="text-center">
                                                <h4 class="mb-3">Tipos de acessos <span class="badge badge-light badge-pill"><i class="fe-monitor"></i></span></h4>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            
                                            <div class="row">

                                                <div class="col-md-6 col-xl-6">
                                                    <div class="card-box">
                                                        <div class="row  mb-3">
                                                            <div class="col-3">
                                                                <div class="avatar-sm bg-soft-info rounded" data-toggle="tooltip" data-placement="top" title="Acessos realizados via celular" data-original-title="Acessos realizados via celular">
                                                                    <i class="mdi mdi-cellphone-android avatar-title font-22 text-info"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-9">
                                                                <div class="text-right">
                                                                    <h3 class="text-dark my-1"><span data-plugin="counterup" id="acessosMobile" name="acessosMobile"></span></h3>
                                                                    <p class="text-muted mb-1 text-truncate">Mobile</p>
                                                                </div>
                                                            </div>

                                                            <div class="col-12 mt-3">
                                                                
                                                                <hr>
                                                                
                                                                    <div class="row justify-content-center">

                                                                        <div class="row col-8">
                                                                        
                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Google Chrome" data-original-title="Google Chrome">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="fe-chrome avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosMobileChrome" name="acessosMobileChrome"></p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Safari" data-original-title="Safari">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="mdi mdi-apple-safari avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosMobileSafari" name="acessosMobileSafari"></p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Mi Browser - Xiaomi" data-original-title="Mi Browser - Xiaomi">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="mdi mdi-web avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosMobileMiBrowser" name="acessosMobileMiBrowser"></p>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Firefox" data-original-title="Firefox">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="mdi mdi-firefox avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosMobileFirefox" name="acessosMobileFirefox"></p>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Opera" data-original-title="Opera">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="mdi mdi-opera avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosMobileOpera" name="acessosMobileOpera"></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row col-4">

                                                                            <div class="col-6" data-toggle="tooltip" data-placement="top" title="Android" data-original-title="Android">
                                                                                <div class="avatar-sm bg-soft-warning rounded">
                                                                                    <i class="fab fa-android avatar-title font-22 text-warning"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosMobileAndroid" name="acessosMobileAndroid"></p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-6" data-toggle="tooltip" data-placement="top" title="Iphone" data-original-title="Iphone">
                                                                                <div class="avatar-sm bg-soft-warning rounded">
                                                                                    <i class="fab fa-apple avatar-title font-22 text-warning"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosMobileIphone" name="acessosMobileIphone"></p>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>

                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-xl-6">
                                                    <div class="card-box">
                                                        <div class="row mb-3">
                                                            <div class="col-3">
                                                                <div class="avatar-sm bg-soft-secondary rounded" data-toggle="tooltip" data-placement="top" title="Acessos realizados via celular" data-original-title="Acessos realizados via celular">
                                                                <i class="la la-desktop avatar-title font-22 text-secondary"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-9">
                                                                <div class="text-right">
                                                                    <h3 class="text-dark my-1"><span data-plugin="counterup" id="acessosDesktop" name="acessosDesktop"></span></h3>
                                                                    <p class="text-muted mb-1 text-truncate">Desktop</p>
                                                                </div>
                                                            </div>

                                                            

                                                            <div class="col-12 mt-3">
                                                                
                                                                <hr>
                                                                
                                                                    <div class="row justify-content-center">

                                                                        <div class="row col-8">
                                                                        
                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Google Chrome" data-original-title="Google Chrome">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="fe-chrome avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosPCChrome" name="acessosPCChrome"></p>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Firefox" data-original-title="Firefox">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="mdi mdi-firefox avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosPCFirefox" name="acessosPCFirefox"></p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Edge ou Explorer" data-original-title="Edge ou Explorer">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="mdi mdi-internet-explorer avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosPCExplorer" name="acessosPCExplorer"></p>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Opera" data-original-title="Opera">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="mdi mdi-opera avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosPCOpera" name="acessosPCOpera"></p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-2" data-toggle="tooltip" data-placement="top" title="Safari - Apple" data-original-title="Safari - Apple">
                                                                                <div class="avatar-sm bg-soft-info rounded">
                                                                                    <i class="mdi mdi-apple-safari avatar-title font-22 text-info"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosPCSafari" name="acessosPCSafari"></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row col-4">
                                                                            
                                                                            <div class="col-4" data-toggle="tooltip" data-placement="top" title="Windows" data-original-title="Windows">
                                                                                <div class="avatar-sm bg-soft-secondary rounded">
                                                                                    <i class="mdi mdi-windows avatar-title font-22 text-secondary"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosPCWindows" name="acessosPCWindows"></p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-4" data-toggle="tooltip" data-placement="top" title="Linux" data-original-title="Linux">
                                                                                <div class="avatar-sm bg-soft-secondary rounded">
                                                                                    <i class="fab fa-linux avatar-title font-22 text-secondary"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosPCLinux" name="acessosPCLinux"></p>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-4" data-toggle="tooltip" data-placement="top" title="Mac - Apple" data-original-title="Mac - Apple">
                                                                                <div class="avatar-sm bg-soft-secondary rounded">
                                                                                    <i class="fab fa-apple avatar-title font-22 text-secondary"></i>
                                                                                    <p class="text-muted mb-1 text-center" id="acessosPCMac" name="acessosPCMac"></p>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                            </div>

                                        </div>

                                    </div> 

                                </div>
                            </div>
                        </div>

                        <div class="card mb-1">
                            <div class="card-header" id="headingTwo">
                                <h5 class="m-0 text-center">
                                    <a class="text-dark" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                        Relatório de cadastros <span class="badge badge-dark badge-pill"><i class="icon-people"></i></span>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="background-color: #30343c;">
                                <div class="card-body">
                                    
                                    <div class="col-12 justify-content-center">
                                                
                                        <div class="row justify-content-center">

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                        
                                                        <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroDiario();" class="btn btn-primary waves-effect waves-info" data-toggle="tooltip" data-placement="top" title="Clique para obter informações sobre os cadastros do dia" data-original-title="Clique para obter informações sobre os cadastros do dia">
                                                                <i class="mdi mdi-chart-pie avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>

                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="CadastrosDiario" name="CadastrosDiario"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Diário</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">

                                                        <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroSemanal();" class="btn btn-primary waves-effect waves-info" data-toggle="tooltip" data-placement="top" title="Clique para obter informações sobre os cadastros da semana" data-original-title="Clique para obter informações sobre os cadastros do dia">
                                                                <i class="mdi mdi-chart-pie avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>

                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="CadastrosSemanais" name="CadastrosSemanais"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Semanal</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroMensal();" class="btn btn-primary waves-effect waves-info" data-toggle="tooltip" data-placement="top" title="Clique para obter informações sobre os cadastros do mês" data-original-title="Clique para obter informações sobre os cadastros do mês">
                                                                <i class="mdi mdi-chart-pie avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="CadastrosMensal" name="CadastrosMensal"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Mensal</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroAnual();" class="btn btn-primary waves-effect waves-info" data-toggle="tooltip" data-placement="top" title="Clique para obter informações sobre os cadastros do ano" data-original-title="Clique para obter informações sobre os cadastros do ano">
                                                                <i class="mdi mdi-chart-pie avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="CadastrosAnual" name="CadastrosAnual"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Anual</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-12 justify-content-center">
                                                
                                        <div class="row justify-content-center">

                                            <div class="col-12">
                                                <div class="text-center">
                                                    <h4 class="mb-3">Onde nos conheceu?</h4>
                                                </div>
                                            </div>

                                            <div class="col-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                        
                                                        <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroFacebook();" class="btn btn-danger waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os cadastros de pessoas vindo pelo Facebook" 
                                                                    data-original-title="Clique para obter informações sobre os cadastros de pessoas vindo pelo Facebook">
                                                                <i class="mdi mdi-facebook-box avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>

                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalCadastroFacebook" name="totalCadastroFacebook"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Facebook</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-3">
                                                <div class="card-box">
                                                    <div class="row">

                                                        <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroInstagram();" class="btn btn-danger waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os cadastros de pessoas vindo pelo Instagram" 
                                                                    data-original-title="Clique para obter informações sobre os cadastros de pessoas vindo pelo Instagram">
                                                                <i class="mdi mdi-instagram avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>

                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalCadastroInstagram" name="totalCadastroInstagram"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Instagram</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroWhatsApp();" class="btn btn-danger waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os cadastros de pessoas vindo pelo WhatsApp" 
                                                                    data-original-title="Clique para obter informações sobre os cadastros de pessoas vindo pelo WhatsApp">
                                                                <i class="mdi mdi-whatsapp avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalCadastroWhatsApp" name="totalCadastroWhatsApp"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">WhatsApp</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroGoogle();" class="btn btn-danger waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os cadastros de pessoas vindo pelo Google" 
                                                                    data-original-title="Clique para obter informações sobre os cadastros de pessoas vindo pelo Google">
                                                                <i class="mdi mdi-google avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalCadastroGoogle" name="totalCadastroGoogle"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Google</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                            
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroYouTube();" class="btn btn-danger waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os cadastros de pessoas vindo pelo YouTube" 
                                                                    data-original-title="Clique para obter informações sobre os cadastros de pessoas vindo pelo YouTube">
                                                                <i class="mdi mdi-youtube avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalCadastroYouTube" name="totalCadastroYouTube"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">YouTube</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroMessenger();" class="btn btn-danger waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os cadastros de pessoas vindo pelo Messenger" 
                                                                    data-original-title="Clique para obter informações sobre os cadastros de pessoas vindo pelo Messenger">
                                                                <i class="mdi mdi-facebook-messenger avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalCadastroMessenger" name="totalCadastroMessenger"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Messenger</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroAmigos();" class="btn btn-danger waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os cadastros de pessoas vindo por indicações Amigos" 
                                                                    data-original-title="Clique para obter informações sobre os cadastros de pessoas vindo por indicações Amigos">
                                                                <i class="fas fa-user-friends avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalCadastroAmigos" name="totalCadastroAmigos"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Amigos</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioCadastroOutros();" class="btn btn-danger waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os cadastros de pessoas vindo por Outro meio de comunicação" 
                                                                    data-original-title="Clique para obter informações sobre os cadastros de pessoas vindo por Outro meio de comunicação">
                                                                <i class="fa fa-question-circle avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalCadastroOutros" name="totalCadastroOutros"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Outros</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="card mb-1">
                            <div class="card-header" id="headingTree">
                                <h5 class="m-0 text-center">
                                    <a class="text-dark" data-toggle="collapse" href="#collapseTree" aria-expanded="false">
                                        Relatório de clicks nos botões <span class="badge badge-dark badge-pill"><i class="la la-mouse-pointer"></i></span>
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTree" class="collapse" aria-labelledby="headingTree" data-parent="#accordion" style="background-color: #30343c;">
                                <div class="card-body">
                                    
                                    <div class="col-12 justify-content-center">
                                                
                                        <div class="row justify-content-center">

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                        
                                                        <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksDiario();" class="btn btn-primary waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks do dia" 
                                                                    data-original-title="Clique para obter informações sobre os clicks do dia">
                                                                <i class="fas fa-mouse-pointer avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>

                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="clicksDiario" name="clicksDiario"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Diário</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">

                                                        <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksSemanal();" class="btn btn-primary waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks da semana" 
                                                                    data-original-title="Clique para obter informações sobre os clicks do dia">
                                                                    <i class="fas fa-mouse-pointer avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>

                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="clicksSemanais" name="clicksSemanais"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Semanal</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksMensal();" class="btn btn-primary waves-effect waves-info"
                                                                data-toggle="tooltip" data-placement="top" 
                                                                title="Clique para obter informações sobre os clicks do mês" 
                                                                data-original-title="Clique para obter informações sobre os clicks do mês">
                                                                <i class="fas fa-mouse-pointer avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="clicksMensal" name="clicksMensal"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Mensal</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksAnual();" class="btn btn-primary waves-effect waves-info" 
                                                                data-toggle="tooltip" data-placement="top" 
                                                                title="Clique para obter informações sobre os clicks do ano" 
                                                                data-original-title="Clique para obter informações sobre os clicks do ano">
                                                                <i class="fas fa-mouse-pointer avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="clicksAnual" name="clicksAnual"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Anual</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-12 justify-content-center">
                                                
                                        <div class="row justify-content-center">

                                            <div class="col-12">
                                                <div class="text-center">
                                                    <h4 class="mb-3">Relatórios por Botões</h4>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksWhatsAppLateral1();" class="btn btn-warning waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no WhatsApp 1 que se encontra na lateral direita" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no WhatsApp 1 que se encontra na lateral direita">
                                                                <i class="mdi mdi-whatsapp avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksWhatsAppLateral1" name="totalClicksWhatsAppLateral1"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">WhatsApp 1 </p>
                                                                <small class="text-warning">(lateral)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksWhatsAppLateral2();" class="btn btn-warning waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no WhatsApp 2 que se encontra na lateral direita" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no WhatsApp 2 que se encontra na lateral direita">
                                                                <i class="mdi mdi-whatsapp avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksWhatsAppLateral2" name="totalClicksWhatsAppLateral2"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">WhatsApp 2 </p>
                                                                <small class="text-warning">(lateral)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksWhatsAppRodape1();" class="btn btn-info waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no WhatsApp 1 que se encontra no rodapé" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no WhatsApp 1 que se encontra no rodapé">
                                                                <i class="mdi mdi-whatsapp avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksWhatsAppRodape1" name="totalClicksWhatsAppRodape1"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">WhatsApp 1</p>
                                                                <small class="text-warning">(rodapé)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksWhatsAppRodape2();" class="btn btn-info waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no WhatsApp 2 que se encontra no rodapé" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no WhatsApp 2 que se encontra no rodapé">
                                                                <i class="mdi mdi-whatsapp avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksWhatsAppRodape2" name="totalClicksWhatsAppRodape2"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">WhatsApp 2</p>
                                                                <small class="text-warning">(rodapé)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksFacebookRodape();" class="btn btn-dark waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Facebook que se encontra no rodapé" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Facebook que se encontra no rodapé">
                                                                <i class="mdi mdi-facebook avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksFacebookRodape" name="totalClicksFacebookRodape"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Facebook</p>
                                                                <small class="text-warning">(rodapé)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksInstagramRodape();" class="btn btn-dark waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Instagram que se encontra no rodapé" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Instagram que se encontra no rodapé">
                                                                <i class="mdi mdi-instagram avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksInstagramRodape" name="totalClicksInstagramRodape"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Instagram</p>
                                                                <small class="text-warning">(rodapé)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksTelefone1Rodape();" class="btn btn-light waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Telefone 1 que se encontra no rodapé" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Telefone 1 que se encontra no rodapé">
                                                                <i class="mdi mdi-phone avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksTelefone1Rodape" name="totalClicksTelefone1Rodape"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Telefone 1</p>
                                                                <small class="text-warning">(rodapé)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksTelefone2Rodape();" class="btn btn-light waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Telefone 2 que se encontra no rodapé" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Telefone 2 que se encontra no rodapé">
                                                                <i class="mdi mdi-phone avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksTelefone2Rodape" name="totalClicksTelefone2Rodape"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Telefone 2</p>
                                                                <small class="text-warning">(rodapé)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksLogoMenu();" class="btn btn-success waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Logo que se encontra no menu" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Logo que se encontra no menu">
                                                                <i class="mdi mdi-image avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksLogoMenu" name="totalClicksLogoMenu"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Logo</p>
                                                                <small class="text-warning">(menu)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoSlide();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra no Slide" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra no Slide">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoSlide" name="totalClicksBotaoSlide"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão</p>
                                                                <small class="text-warning">(slide)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksLinkRodape();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no link do site que se encontra no Rodapé" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no link do site que se encontra no Rodapé">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksLinkRodape" name="totalClicksLinkRodape"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Link site</p>
                                                                <small class="text-warning">(rodapé)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoRS1();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão 1 que se encontra em Redes Sociais" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no BBotão 1 que se encontra em Redes Sociais">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoRS1" name="totalClicksBotaoRS1"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 1</p>
                                                                <small class="text-warning">(redes sociais)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoRS2();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão 2 que se encontra em Redes Sociais" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no BBotão 2 que se encontra em Redes Sociais">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoRS2" name="totalClicksBotaoRS2"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 2</p>
                                                                <small class="text-warning">(redes sociais)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoRS3();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão 3 que se encontra em Redes Sociais" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão 3 que se encontra em Redes Sociais">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoRS3" name="totalClicksBotaoRS3"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 3</p>
                                                                <small class="text-warning">(redes sociais)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico2();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 2" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 2">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico2" name="totalClicksBotaoServico2"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão</p>
                                                                <small class="text-warning">(serviço 2)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico4();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 4" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 4">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico4" name="totalClicksBotaoServico4"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão</p>
                                                                <small class="text-warning">(serviço 4)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico5Opc1();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 1" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 1">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico5Opc1" name="totalClicksBotaoServico5Opc1"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 1</p>
                                                                <small class="text-warning">(serviço 5)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico5Opc2();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 2" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 2">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico5Opc2" name="totalClicksBotaoServico5Opc2"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 2</p>
                                                                <small class="text-warning">(serviço 5)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico5Opc3();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 3" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 3">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico5Opc3" name="totalClicksBotaoServico5Opc3"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 3</p>
                                                                <small class="text-warning">(serviço 5)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico5Opc4();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 4" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 4">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico5Opc4" name="totalClicksBotaoServico5Opc4"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 4</p>
                                                                <small class="text-warning">(serviço 5)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico5Opc5();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 5" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 5">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico5Opc5" name="totalClicksBotaoServico5Opc5"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 5</p>
                                                                <small class="text-warning">(serviço 5)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico5Opc6();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 6" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 6">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico5Opc6" name="totalClicksBotaoServico5Opc6"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 6</p>
                                                                <small class="text-warning">(serviço 5)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico5Opc7();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 7" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 7">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico5Opc7" name="totalClicksBotaoServico5Opc7"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 7</p>
                                                                <small class="text-warning">(serviço 5)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xl-3">
                                                <div class="card-box">
                                                    <div class="row">
                                                    <div class="col-3">
                                                            
                                                            <button type="button" onclick="javascript:abrirRelatorioClicksBotaoServico5Opc8();" class="btn btn-purple waves-effect waves-info" 
                                                                    data-toggle="tooltip" data-placement="top" 
                                                                    title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 8" 
                                                                    data-original-title="Clique para obter informações sobre os clicks efetuados no Botão que se encontra na sessão de serviço 5 opcão 8">
                                                                <i class="mdi mdi-link avatar-title font-22"></i>
                                                            </button>
                                                            
                                                        </div>
                                                        <div class="col-9">
                                                            <div class="text-right">
                                                                <h3 class="text-dark my-1"><span data-plugin="counterup" id="totalClicksBotaoServico5Opc8" name="totalClicksBotaoServico5Opc8"></span></h3>
                                                                <p class="text-muted mb-1 text-truncate">Botão 8</p>
                                                                <small class="text-warning">(serviço 5)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        
                    </div>

                </div>
                
            </div>
            
        </div>

    </div> 
    
</div> 

<div id="leadsDiario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os acessos gerados hoje 
                    <span class="text-warning">(<?=date('d/m/Y')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>IP</th>
                                <th>Cidade</th>
                                <th>Estado</th>
                                <th>Dispositivo</th>
                                <th>Navegador</th>
                                <th>Sistema Operacional</th>
                                <th>Data/Hora</th>
                                <th>Tempo na página</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaLeadsDiario"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosAcessosDia/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsSemana" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                
                <?php
                    $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
                    $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
                ?>

                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os acessos gerados na Semana, entre os dias:
                    <span class="text-warning">
                        (<?=date('d/m/Y', strtotime($primeiroDiaDaSemana))." à";?>
                        <?=date('d/m/Y', strtotime($ultimoDiaDaSemana));?>)
                    </span>
                    
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>IP</th>
                                <th>Cidade</th>
                                <th>Estado</th>
                                <th>Dispositivo</th>
                                <th>Navegador</th>
                                <th>Sistema Operacional</th>
                                <th>Data/Hora</th>
                                <th>Tempo na página</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaLeadsSemana"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosAcessosSemana/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsMensal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os acessos gerados no mês 
                    <span class="text-warning">(<?=date('M')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>IP</th>
                                <th>Cidade</th>
                                <th>Estado</th>
                                <th>Dispositivo</th>
                                <th>Navegador</th>
                                <th>Sistema Operacional</th>
                                <th>Data/Hora</th>
                                <th>Tempo na página</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaLeadsMensal"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosAcessosMes/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsAnual" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os acessos gerados no ano 
                    <span class="text-warning">(<?=date('Y')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>IP</th>
                                <th>Cidade</th>
                                <th>Estado</th>
                                <th>Dispositivo</th>
                                <th>Navegador</th>
                                <th>Sistema Operacional</th>
                                <th>Data/Hora</th>
                                <th>Tempo na página</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaLeadsAnual"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosAcessosAno/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroDiario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados hoje 
                    <span class="text-warning">(<?=date('d/m/Y')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroDiario"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosCadastroDia/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroSemanal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                
                <?php
                    $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
                    $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
                ?>

                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os acessos gerados na Semana, entre os dias:
                    <span class="text-warning">
                        (<?=date('d/m/Y', strtotime($primeiroDiaDaSemana))." à";?>
                        <?=date('d/m/Y', strtotime($ultimoDiaDaSemana));?>)
                    </span>
                    
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroSemanal"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosCadastroSemana/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroMensal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados no mês 
                    <span class="text-warning">(<?=date('M')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroMensal"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosCadastroMes/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroAnual" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados no ano 
                    <span class="text-warning">(<?=date('Y')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroAnual"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosCadastroAno/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroFacebook" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados vindos pelo Facebook</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroFacebook"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroInstagram" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados vindos pelo Instagram</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroInstagram"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroWhatsApp" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados vindos pelo Instagram</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroWhatsApp"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroGoogle" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados vindos pelo Instagram</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroGoogle"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroYouTube" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados vindos pelo Instagram</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroYouTube"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroMessenger" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados vindos pelo Instagram</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroMessenger"></tbody>
                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroAmigos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados vindos pelo Instagram</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroAmigos"></tbody>
                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioCadastroOutros" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados vindo por Outro meio de comunicação</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table w-100 nowrap">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Onde nos conheceu</th>
                                <th>LandingPage</th>
                                <th>Lista</th>
                                <th>Cidade/Estado</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioCadastroOutros"></tbody>
                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-xs btn-success waves-effect" id="pdf">PDF</button>
                <button type="button" class="btn btn-xs btn-info waves-effect" id="csv">CSV</button>
                <button type="button" class="btn btn-xs btn-light waves-effect" id="json">JSON</button>
                <button type="button" class="btn btn-danger waves-effect ml-3" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioClicksDiario" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os clicks gerados hoje 
                    <span class="text-warning">(<?=date('d/m/Y')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>LandingPage</th>
                                <th>Nome botão</th>
                                <th>link</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaRelatorioClicksDiario"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosClicksBotoesDia/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioClicksSemanal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                
                <?php
                    $primeiroDiaDaSemana = date('Ymd', strtotime('sunday last week', strtotime(date('Ymd'))));
                    $ultimoDiaDaSemana = date('Ymd', strtotime('saturday this week', strtotime(date('Ymd'))));
                ?>

                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os acessos gerados na Semana, entre os dias:
                    <span class="text-warning">
                        (<?=date('d/m/Y', strtotime($primeiroDiaDaSemana))." à";?>
                        <?=date('d/m/Y', strtotime($ultimoDiaDaSemana));?>)
                    </span>
                    
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>LandingPage</th>
                                <th>Nome botão</th>
                                <th>link</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaLeadsClicksSemanal"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosClicksBotoesSemana/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioClicksMensal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os acessos gerados no mês 
                    <span class="text-warning">(<?=date('M')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>LandingPage</th>
                                <th>Nome botão</th>
                                <th>link</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaLeadsClicksMensal"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosClicksBotoesMes/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="leadsRelatorioClicksAnual" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-full modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="full-width-modalLabel">Informações sobre os cadastros gerados no ano 
                    <span class="text-warning">(<?=date('Y')?>)</span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">

                <div class="table-responsive table-bordered">
                    
                    <table class="table mb-0">
                        
                        <thead class="thead-light">
                            <tr>
                                <th>LandingPage</th>
                                <th>Nome botão</th>
                                <th>link</th>
                                <th>Data/Hora</th>
                            </tr>
                        </thead>

                        <tbody id="tabelaLeadsClicksAnual"></tbody>

                    </table>

                </div>

            </div>

            <div class="modal-footer">
                <a href="/dashboard/relatorios/downListarRelatoriosClicksBotoesAno/<?=$idLead?>"><button type="button" class="btn btn-success waves-effect"><i class="mdi mdi-file-excel"></i>Relatório</button></a>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div id="primeiroAcesso" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Atenção!!</h4>
            </div>

            <div class="modal-body">
                
                <h5>Olá <span class="text-warning"><?=$dados['nome']?></span>, 
                precisamos que você insira os dados da sua empresa!
                Não vai demorar mais do que 5 minutos para fazer isso, vamos lá?
                </h5>

                <div class="col-md-12 mt-3">
                    <a href="/dashboard/empresa/minhaEmpresa">
                        <button type="button" class="btn btn-block btn-primary waves-effect waves-light">Vou fazer isso agora!</button>
                    </a>
                </div>
            
            </div>
            
        </div>
    </div>
</div>
