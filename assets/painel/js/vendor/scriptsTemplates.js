listaTemplates();

primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

$(document).ready(function() {
  $('#botaoMobileAbrirEFechar').trigger('click');
});

function listaTemplates(){
    
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarTodosTemplates",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        $('#tabelaListarTemplates').html('');
        
        if(dados.dados.length>0){
          
          dadosGlobais = dados.dados;

          for(var i = 0; i < dados.dados.length; i++) {

            $('#escolherTemplate'+ dados.dados[i].id_templates).submit(function(e){
              e.preventDefault();
              var self = $(this);
              var retorno = atualizarTemplateLandingPage(self);
            
            });
          

            if (dados.dados[i].statusTemplate_templates == 0) 
            {

              if(dados.links[26].permissao_niveisPaginas === '1'){
                var botaoStatus = '<button type="button" onclick="javascript:AbrirModalAtualizarStatus(' + i + ');" class="btn btn-xs btn-dark waves-effect waves-light mr-1"><i class="la la-thumbs-down"></i></button>';
                var botaoStatusInicio = '<td>';
                var botaoStatusFim = '</td>';
              } else {

                var botaoStatus = '';
                var botaoStatusInicio = '';
                var botaoStatusFim = '';

              }
              
              
            } else {

              if(dados.links[26].permissao_niveisPaginas === '1'){
                var botaoStatus = '<button type="button" onclick="javascript:AbrirModalAtualizarStatus(' + i + ');" class="btn btn-xs btn-purple waves-effect waves-light mr-1"><i class="la la-thumbs-up"></i></button>';
                var botaoStatusInicio = '<td>';
                var botaoStatusFim = '</td>';
              } else {

                var botaoStatus = '';
                var botaoStatusInicio = '';
                var botaoStatusFim = '';

              }

              if(dados.links[27].permissao_niveisPaginas === '1'){
                
                botaoEditarTemplate = '<button type="button" onclick="javascript:AbrirModalAlterarTemplate(' + i + ');" class="btn btn-xs btn-primary waves-effect waves-light mr-1"><i class="fe-edit-2"></i></button>';  

              } else {
                
                botaoEditarTemplate = "";
              }

              if(dados.links[28].permissao_niveisPaginas === '1'){
                
                botaoExcluirTemplate = '<button type="button" onclick="javascript:AbrirModalExcluirTemplate(' + i + ');" class="btn btn-xs btn-danger waves-effect waves-light"><i class="fe-trash-2"></i></button>';  

              } else {
                
                botaoExcluirTemplate = "";
              }

            }
              
            $('#tabelaListarTemplates').append(

              '<tr>' +

                '<th scope="row">'+ dados.dados[i].id_templates +'</th>' +

                '<td>'+ dados.dados[i].nomeTemplate_templates +'</td>' +
                
                botaoStatusInicio + 
                  botaoStatus + 
                botaoStatusFim +

                '<td>' +
                  '<button type="button" onclick="javascript:AbrirModalVerTemplate(' + i + ');" class="btn btn-xs btn-info waves-effect waves-light mr-1"><i class="fe-eye"></i></button>' +
                  botaoEditarTemplate +
                  botaoExcluirTemplate +
                '</td>' +

              '</tr>'
                  
            );
          }

        } else {
            
            $('#tabelaListarTemplates').append(
              '<td colspan="4">' +
                '<center class="mt-4 text-center">' +
                    '<div class="col-md-12 text-center">' +
                        '<div class="alert alert-danger text-center">' +
                            '<i class="fas fa-exclamation-circle"></i> Nenhum template page criado!!' +
                        '</div>' +
                    '</div>' +
                '</center>' +
              '</td>'
            );

        }
      }
    }
  );
}


$('#previewTemplate').attr("src", "/assets/images/templates/default-template.jpg");

function readURL(input) 
{
    if (input.files && input.files[0]) 
    {
        var reader = new FileReader();

        reader.onload = function (e) 
        {
            $('#previewTemplate')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$('#formTemplates').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = criarTemplateLandingPages(self);
  
  });
  
  function criarTemplateLandingPages(dados){
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/cadastro/cadastrarNovoTemplateLandingPage",
        dataType: 'json',
        data: new FormData($('#formTemplates')[0]),
        cache: false,
        contentType: false,
        processData: false,

        beforeSend: function(){
          
          $("#botaoCadastrarTemplate").text("Criando...").prop("disabled", true);
          $("#nomeTemplate").prop("disabled", true);
          $("#imagemTemplate").prop("disabled", true);

        },

        success: function(retorno){
            
          if(retorno.ret == false) {
  
            $('#erroMsg').html(
                
              '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                  '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );

            $("#botaoCadastrarTemplate").text("Tentar novamente...").prop("disabled", false);
            $("#nomeTemplate").prop("disabled", false);
            $("#imagemTemplate").prop("disabled", false);
  
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);

            listaTemplates();
            
          } else {
            
            $('#erroMsg').html(
                
              '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                '<h5 class="text-success">Tudo certo!</h5>' +
                  retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );

            $('#formTemplates').each (function(){
              this.reset();
            });
            
            $("#botaoCadastrarTemplate").text("Criar novo template...").prop("disabled", false);
            $("#nomeTemplate").prop("disabled", false);
            $("#imagemTemplate").prop("disabled", false);
  
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);

            $('#previewTemplate').attr("src", "/assets/images/templates/default-image.jpg");

            listaTemplates();
            
          }
        }
      }
    );
  }

function AbrirModalAtualizarStatus(index) {

  showModalAtualizarStatus(index);
  
}

function showModalAtualizarStatus(index){

  $('#modalAtualizarStatus').modal('show');

  $("#nomeTemplateAtualizarStatus").html(dadosGlobais[index].nomeTemplate_templates);
  $("#idTemplate").val(dadosGlobais[index].id_templates);
  
}

$('#formAtualizaStatusTemplates').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarStatusTemplates(self);

});

  function atualizarStatusTemplates(dados){
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarStatusTemplates",
        dataType: 'json',
        
        success: function(retorno){
            
          if(retorno.ret == false) {
  
            $('#msgErroAtualizarStatus').html(
                
              '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                  '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );
            
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);

            listaTemplates();
            
          } else {
            
            $('#erroMsgAtualizarStatus').html(
                
              '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                '<h5 class="text-success">Tudo certo!</h5>' +
                  retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );

            $('#modalAtualizarStatus').modal('hide');
            
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);

            listaTemplates();


          }
        }
      }
    );
  }

  function AbrirModalVerTemplate(index) {
  
    showModalVerTemplate(index);
    
  }

  function showModalVerTemplate(index){
  
    $('#modalVerTemplate').modal('show');

    $("#nomeVerTemplate").html(dadosGlobais[index].nomeTemplate_templates);

    $('#previewTemplateView').attr("src", "/assets/images/templates/"+dadosGlobais[index].imagemTemplate_templates);
 
  }
  
function readURL(input) 
{
    if (input.files && input.files[0]) 
    {
        var reader = new FileReader();

        reader.onload = function (e) 
        {
            $('#previewAtualizarTemplate')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

  function AbrirModalAlterarTemplate(index) {
  
    showModalAlterarTemplate(index);
    
  }

  function showModalAlterarTemplate(index){
  
    $('#modalEditarTemplate').modal('show');

    $('#previewAtualizarTemplate').attr("src", "/assets/images/templates/"+dadosGlobais[index].imagemTemplate_templates);

    $("#nomeAlterarTemplate").html(dadosGlobais[index].nomeTemplate_templates);
    $("#nomeAtualizarTemplate").val(dadosGlobais[index].nomeTemplate_templates);
    $("#idAtualizarTemplate").val(dadosGlobais[index].id_templates);

  }

  $('#formAtualizarTemplates').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = atualizarDadosTemplates(self);
  
  });
  
  function atualizarDadosTemplates(dados){
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarDadosTemplate",
        dataType: 'json',
        data: new FormData($('#formAtualizarTemplates')[0]),
        cache: false,
        contentType: false,
        processData: false,

        beforeSend: function(){
          
          $("#botaoCadastrarTemplate").text("Atualizando...").prop("disabled", true);
          $("#nomeAtualizarTemplate").prop("disabled", true);
          $("#imagemAtualizarTemplate").prop("disabled", true);

        },

        success: function(retorno){
            
          if(retorno.ret == false) {
  
            $('#msgErroAtualizarTemplate').html(
                
              '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                  '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );

            $("#botaoCadastrarTemplate").text("Tentar novamente...").prop("disabled", false);
            $("#nomeAtualizarTemplate").prop("disabled", false);
            $("#imagemAtualizarTemplate").prop("disabled", false);
  
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);

            listaTemplates();
            
          } else {
            
            $('#erroMsg').html(
                
              '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                '<h5 class="text-success">Tudo certo!</h5>' +
                  retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );

            $('#formTemplates').each (function(){
              this.reset();
            });

            $('#modalEditarTemplate').modal('hide');
            
            $("#botaoCadastrarTemplate").text("Atualizar dados").prop("disabled", false);
            $("#nomeAtualizarTemplate").prop("disabled", false);
            $("#imagemAtualizarTemplate").prop("disabled", false);
  
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);
            
            listaTemplates();
            
          }
        }
      }
    );
  }

  function AbrirModalExcluirTemplate(index) {
  
    showModalExcluirTemplate(index);
    
  }

  function showModalExcluirTemplate(index){
  
    $('#modalExcluirTemplate').modal('show');
    
    $("#idTemplateExcluirNum").html(dadosGlobais[index].id_templates);
    $("#idTemplateExcluir").val(dadosGlobais[index].id_templates);

  }

  $('#formExcluirTemplate').submit(function(e){
    e.preventDefault();
    var self = $(this);
    // alert(self.serialize());
    var retorno = excluirTemplate(self);
  
  });

  function excluirTemplate(dados){
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/excluir/excluirTemplate",
        dataType: 'json',
        
        success: function(retorno){
            
          if(retorno.ret == false) {
  
            $('#msgErroExcluirTemplate').html(
                
              '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                  '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );
            
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);

            listaTemplates();
            
          } else {
            
            $('#erroMsg').html(
                
              '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                '<h5 class="text-success">Tudo certo!</h5>' +
                  retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );
            
            $('#modalExcluirTemplate').modal('hide');
            
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);
            
            listaTemplates();
            
          }
        }
      }
    );
  }
  
  function atualizarTemplateLandingPage(dados){
    
    $.ajax(
      {
        type: "POST",
        data: dados.serialize(),
        url: "/dashboard/atualizar/atualizarTemplateSelecionadoLandingPage",
        dataType: 'json',
        
        success: function(retorno){
            
          if(retorno.ret == false) {
  
            $('#erroMsgEscolherTemplate').html(
                
              '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                  '<h5 class="text-danger">Erro!</h5>' +
                    retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );
            
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);
            
          } else {
            
            $('#erroMsgEscolherTemplate').html(
                
              '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                '<h5 class="text-success">Tudo certo!</h5>' +
                  retorno.msg +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                  '</button>' +
              '</div>'
            );
            
            $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });
  
            $('html,body').scrollTop(0);

            $("#botaoPlanoAtual").text("Atual");
            
            
          }
        }
      }
    );
  }
  