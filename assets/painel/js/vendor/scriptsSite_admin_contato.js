primeiroAcessoChecarEmpresa();

function primeiroAcessoChecarEmpresa() {

    $.ajax({

        url: "/listagens/primeiroAcesso/checarEmpresa",

        ajax: 'data.json',

        success: function(data) {

            var dados = JSON.parse(data);
            
            if (dados.status_empresa === '1') {

                $('#primeiroAcesso').modal('hide');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });

            } else {

                $('#primeiroAcesso').modal('show');

                $(document).ready(function() {
                    $('#botaoMobileAbrirEFechar').trigger('click');
                });
                

            }

        }
    });
}

listarConfigsContato();

function listarConfigsContato(){
  
  $.ajax(
    {
        
      url: "/dashboard/listagens/listarConfigsContato",
      
      ajax: 'data.json',
      
      success: function(data){
          
        var dados = JSON.parse(data);
        
        if (dados.debug_contato === 0) {
          $('#debug_contato').append('<option value="0" selected>0</option>');
          $('#debug_contato').append('<option value="1">1</option>');
          $('#debug_contato').append('<option value="2">2</option>');
        } else if (dados.debug_contato === 1) {
          $('#debug_contato').append('<option value="0" >0</option>');
          $('#debug_contato').append('<option value="1" selected>1</option>');
          $('#debug_contato').append('<option value="2">2</option>');
        } else {
          $('#debug_contato').append('<option value="0">0</option>');
          $('#debug_contato').append('<option value="1">1</option>');
          $('#debug_contato').append('<option value="2" selected>2</option>');
        }

        if (dados.SMTPSecure_contato === 'ssl') {
          $('#seguranca_contato').append('<option value="ssl" selected>SSL</option>');
          $('#seguranca_contato').append('<option value="tls">TLS</option>');
        } else {
          $('#seguranca_contato').append('<option value="ssl">SSL</option>');
          $('#seguranca_contato').append('<option value="tls" selected>TLS</option>');
        }

        if (dados.SMTPAuth_contato === 'true') {
          $('#auth_contato').append('<option value="true" selected>True</option>');
          $('#auth_contato').append('<option value="false">False</option>');
        } else {
          $('#auth_contato').append('<option value="true">True</option>');
          $('#auth_contato').append('<option value="false" selected>False</option>');
        }

        if (dados.port_contato === '465') {
          $('#porta_contato').append('<option value="465" selected>465</option>');
          $('#porta_contato').append('<option value="587">587</option>');
        } else {
          $('#porta_contato').append('<option value="465">465</option>');
          $('#porta_contato').append('<option value="587" selected>587</option>');
        }
        
        $('#host_contato').val(dados.host_contato);
        $('#usuario_contato').val(dados.username_contato);
        $('#senha_contato').val(dados.password_contato);
        $('#mensagem_contato').val(dados.msgRemetente_contato);
        $('#debug_contato').val(dados.debug_contato);
        $('#emailPara_contato').val(dados.addAddress_contato);
        $('#seguranca_contato').val(dados.SMTPSecure_contato);
        $('#auth_contato').val(dados.SMTPAuth_contato);
        $('#porta_contato').val(dados.port_contato);

      }
    }
  );
}

$('#formAtualizarConfigsContato').submit(function(e){
  e.preventDefault();
  var self = $(this);
  // alert(self.serialize());
  var retorno = atualizarConfigsContato(self);
});

function atualizarConfigsContato(dados){
  
  $.ajax(
    {
      type: "POST",
      enctype: 'multipart/form-data',
      data: dados.serialize(),
      url: "/dashboard/atualizar/atualizarConfigsContato",
      dataType: 'json',

      success: function(retorno){
          
        if(retorno.ret == false) {

          $('#erroMsg').html(
            
            '<div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">' +
                '<h5 class="text-white">Erro!</h5>' +
                  retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
          
        } else {
          
          $('#erroMsg').html(
              
            '<div class="alert alert-success bg-success text-white border-0 alert-dismissible fade show" role="alert">' +
              '<h5 class="text-white">Tudo certo!</h5>' +
                retorno.msg +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' +
            '</div>'
          );
          
          $(".alert").delay(10000).slideUp(1000, function() { $(this).alert('close'); });

          $('html,body').scrollTop(0);
        }
      }
    }
  );
}
