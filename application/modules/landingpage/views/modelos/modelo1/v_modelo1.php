<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$titulo?></title>
    <link rel="canonical" href="<?=$dadosLandingPage->link_landingPage?>"/>
    <meta name="description" content="<?=$configsModelo->topo_head_SEO_description?>">
    <meta name="keywords" content="<?=$configsModelo->topo_head_SEO_keywords?>">
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="Portuguese">
    <meta name="generator" content="N/A">
    <meta property="og:locale" content="pt_BR"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="<?=$titulo?>"/>
    <meta property="og:description" content="<?=$configsModelo->topo_head_SEO_description?>"/>
    <meta property="og:url" content="<?=$dadosLandingPage->link_landingPage?>"/>
    <meta property="og:site_name" content="<?=$dadosLandingPage->nome_landingPage?>"/>
    <meta property="og:image" content="<?=base_url('/assets/images/SEO/').$configsModelo->topo_head_SEO_imagem?>"/>
    <meta property="og:image:secure_url" content="<?=base_url('/assets/images/SEO/').$configsModelo->topo_head_SEO_imagem?>"/>
    <meta property="og:image:alt" content="<?=$dadosLandingPage->nome_landingPage?>"/>
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="850">
    <meta property="og:image:height" content="400">
    <meta name="author" content="Josheffer Robert">
    <meta name="application-name" content="<?=$dadosLandingPage->nome_landingPage?>">
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content="<?=$configsModelo->topo_head_SEO_description2?>"/>
    <meta name="twitter:title" content="<?=$dadosLandingPage->nome_landingPage?>"/>
    <meta name="twitter:image" content="<?=base_url('/assets/images/SEO/').$configsModelo->topo_head_SEO_imagem?>"/>
    <meta content="#000000" name=theme-color>
    <meta name="geo.position" content="<?=$configsModelo->topo_head_coordenadasMaps?>" />
    <meta name="geo.placename" content="<?=$configsModelo->topo_head_cidade?>" />
    <meta name="geo.region" content="<?=$configsModelo->topo_head_estado?>" />
    <meta name="ICBM" content="<?=$configsModelo->topo_head_coordenadasMaps?>" />
    <meta name="author" content="Josheffer Robert" />
    <meta name="DC.title" content="<?=$titulo?>" />
    <meta name="distribution" content="'Global" />
    <meta name="rating" content="'General" />
    <meta name="doc-class" content="'Completed" />

    <link rel="stylesheet" href="<?=base_url('modelos/modelo1/')?>vendors/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url('modelos/modelo1/')?>vendors/fontawesome/css/all.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=base_url('modelos/modelo1/')?>vendors/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="<?=base_url('modelos/modelo1/')?>vendors/linericon/style.css">
    <link rel="stylesheet" href="<?=base_url('modelos/modelo1/')?>vendors/owl-carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?=base_url('modelos/modelo1/')?>vendors/owl-carousel/owl.carousel.min.css">

    <link rel="stylesheet" href="<?=base_url('modelos/modelo1/')?>css/style.css">

    <link rel="icon" href="<?=base_url('assets/images/logofavicon/').$configs->favicon_landingPage?>" type="image/png">

    <?=$scriptCSS?>

    <style>
        
        .button-header {
            padding: 8px 12px 7px 7px !important;
            font-size: 25px !important;
            border-radius: 10px !important;
        }

        /*INICIO CORES DE TITULO E DESCRIÇÃO DOS FORMS */
        .servico_form1_corTitulo {
            color: <?=$configsModelo->body_servico_form1_corTitulo?>;
        }

        .servico_form1_corDescricao {
            color: <?=$configsModelo->body_servico_form1_corDescricao?>;
        }

        .servico_form2_corTitulo {
            color: <?=$configsModelo->body_servico_form2_corTitulo?>;
        }

        .servico_form2_corDescricao {
            color: <?=$configsModelo->body_servico_form2_corDescricao?>;
        }

        .servico_form3_corTitulo {
            color: <?=$configsModelo->body_servico_form3_corTitulo?>;
        }

        .servico_form3_corDescricao {
            color: <?=$configsModelo->body_servico_form3_corDescricao?>;
        }

        .servico_form4_corTitulo {
            color: <?=$configsModelo->body_servico_form4_corTitulo?>;
        }

        .servico_form4_corDescricao {
            color: <?=$configsModelo->body_servico_form4_corDescricao?>;
        }

        .servico_form5_corTitulo {
            color: <?=$configsModelo->body_servico_form5_corTitulo?>;
        }

        .servico_form5_corDescricao {
            color: <?=$configsModelo->body_servico_form5_corDescricao?>;
        }

        .servico_form6_corTitulo {
            color: <?=$configsModelo->body_servico_form6_corTitulo?>;
        }

        .servico_form6_corDescricao {
            color: <?=$configsModelo->body_servico_form6_corDescricao?>;
        }

        .servico_form7_corTitulo {
            color: <?=$configsModelo->body_servico_form7_corTitulo?>;
        }

        .servico_form7_corDescricao {
            color: <?=$configsModelo->body_servico_form7_corDescricao?>;
        }

        .servico_form8_corTitulo {
            color: <?=$configsModelo->body_servico_form8_corTitulo?>;
        }

        .servico_form8_corDescricao {
            color: <?=$configsModelo->body_servico_form8_corDescricao?>;
        }

        .servico_formRS_corTitulo {
            color: <?=$configsModelo->body_servico_formRS_corTitulo?>;
        }

        .servico_formRS_corDescricao {
            color: <?=$configsModelo->body_servico_formRS_corDescricao?>;
        }
        /*FIM CORES DE TITULO E DESCRIÇÃO DOS FORMS */

        .icone-whatsapp
        {
            padding-top: 8px;
            margin: 0 auto;
            font-size: 40px;
        }

        .icone-whatsapp-topbox
        {
            font-size: 40px;
            margin-top: 10px;
        }

        /* INICIO CONFIGS FORMULÁRIO 1 */
        .sevico1_Form1 {
            padding: 45px 0 20px 0;
            background-color: <?=$configsModelo->body_servico1_form1_corFundoForm?>;

            <?php if($configsModelo->body_slide != '1') : ?>
                margin-top: 90px;
            <?php endif ?>
            
        }

        @media only screen and (max-width: 600px) {
            .sevico1_Form1 {
                <?php if($configsModelo->body_slide != '1') : ?>
                    margin-top: 60px;
                <?php endif ?>
            }
        }
        
        .corFundoBotaoForm1 {
            background-color: <?=$configsModelo->body_servico1_form1_corFundoBotao?>;
            color: <?=$configsModelo->body_servico1_form1_corTextBotao?>;
        }
        
        /* FIM CONFIGS FORMULÁRIO 1 */

        /* INICIO CONFIGS FORMULÁRIO 1 */
        .sevico1_Form2 {
            padding: 20px 0 40px 0;
            background-color: <?=$configsModelo->body_servico1_form2_corFundoForm?>;
        }

        .corFundoBotaoForm2 {
            background-color: <?=$configsModelo->body_servico1_form2_corFundoBotao?>;
            color: <?=$configsModelo->body_servico1_form2_corTextBotao?>;
        }
        /* FIM CONFIGS FORMULÁRIO 1 */
        
        /* INICIO CONFIGS MENU */
        <?php

            if(empty($configsModelo->corFundoMenu))
            {
                $corBG = "#00000000";
                $shadowBox = "0px 0px 0px 0px rgba(0,0,0,0.0)";

            } else {

                $corBG = $configsModelo->corFundoMenu;
                $shadowBox =  "0px 3px 16px 0px rgba(0,0,0,0.1)";

            }
        ?>

        .corFundoMenu {

            background-color: <?=$corBG?> !important;

        }
        
        .header_area.navbar_fixed .main_menu {
            position: fixed;
            width: 100%;
            top: -70px;
            left: 0;
            right: 0;
            background-color: <?=$corBG?> !important;
            z-index: 99;
            transform: translateY(70px);
            transition: transform 500ms ease, background 500ms ease;
            -webkit-transition: transform 500ms ease, background 500ms ease;
            box-shadow: <?=$shadowBox?>;
        }

        .header_area .navbar {
            background: <?=$corBG?> !important;
        }
        /* FIM CONFIGS MENU */
        
        /* INICIO CONFIGS SLIDE */

        .fundoBgSlide{background-color:<?=$configsModelo->corFundoSlide?>!important;}
        .corTextoTituloSlide{color:<?=$configsModelo->corTextoTituloSlide?> !important; word-break: break-all !important;}
        .corTextoDescricaoSlide{color:<?=$configsModelo->corTextoDescricaoSlide?> !important; word-break: break-all !important;}

        .corTextoBotaoSlide {
            display: inline-block;
            border: 1px solid transparent;
            text-transform: capitalize;
            font-size: 15px;
            background-color: <?=$configsModelo->corFundoBotaoSlide?> !important;
            color: <?=$configsModelo->corTextoBotaoSlide?> !important;
            font-weight: 500;
            padding: 10px 24px;
            border-radius: 30px;
            color: #21146a;
            color: #3a414e;
            transition: all .4s ease;
        }

        .corTextoBotaoSlide.bg:hover {
            background-color: <?=$configsModelo->corFundoHoverBotaoSlide?> !important;
            color: <?=$configsModelo->corTextoHoverBotaoSlide?> !important;
        }
        /* FIM CONFIGS SLIDE */

        /* INICIO CONFIGS SERVIÇO 1 */
        .servico1_corTituloGenerico1 {
            color: <?=$configsModelo->servico1_corTituloGenerico1?> !important;
        }
        
        .servico1_corDescricaoGenerico1 {
            color: <?=$configsModelo->servico1_corDescricaoGenerico1?> !important;
        }

        .section-margin-serv1 {
            padding-bottom: 20px;
            padding-top: 70px;
            background-color: <?=$configsModelo->servico1_corBg?> !important;
        }

        .objectIMG {
            object-fit: cover !important;
            height: 250px;
        }

        .boxIMG {
            margin-bottom: 30px !important;
            background: <?=$configsModelo->servico1_corBoxImage?> !important;
        }

        .servico1_corTexto{color: <?=$configsModelo->servico1_corTexto?> !important;}
        /* FIM CONFIGS SERVIÇO 1 */

        /* INICIO CONFIGS SERVIÇO 2 */

        .servico2{background-color: <?=$configsModelo->servico2_corBg?> !important;}
        
        .corTexto_servico2Titulo1{color: <?=$configsModelo->servico2_corTituloGenerico1?> !important;}

        .corDescricao_servico2Descricao1 {
            color: <?=$configsModelo->servico2_corDescricaoGenerico1?> !important;
        }

        .botao_serv2 {
            color: <?=$configsModelo->servico2_corTextoBotao?> !important;
            background-color: <?=$configsModelo->servico2_corFundoBotao?> !important;
            border-color: <?=$configsModelo->servico2_corFundoBotao?> !important;
        }

        .botao_serv2:hover {
            color: <?=$configsModelo->servico2_corTextoHoverBotao?> !important;
            background-color: <?=$configsModelo->servico2_corFundoHoverBotao?> !important;
            border-color: <?=$configsModelo->servico2_corFundoHoverBotao?> !important;
        }

        .sevico2_Form3 {
            padding: 20px 0 40px 0;
            background-color: <?=$configsModelo->body_servico2_form3_corFundoForm?> !important;
        }

        .corFundoBotaoForm3 {
            background-color: <?=$configsModelo->body_servico2_form3_corFundoBotao?> !important;
            color: <?=$configsModelo->body_servico2_form3_corTextBotao?> !important;
        }

        /* FIM CONFIGS SERVIÇO 2 */

        /* INICIO CONFIGS SERVIÇO 3 */

        .servico3_form4 {
            padding: 40px 0 50px 0;
            background-color: <?=$configsModelo->body_servico3_form4_corFundoForm?> !important;
        }

        .corFundoBotaoForm4{
            background-color: <?=$configsModelo->body_servico3_form4_corFundoBotao?> !important;
            color: <?=$configsModelo->body_servico3_form4_corTextBotao?> !important;
        }

        .servico3{background-color: <?=$configsModelo->servico3_corBg?> !important;}

        .servico3_corTituloGenerico1{color: <?=$configsModelo->servico3_corTituloGenerico1?> !important;}

        .servico3_corDescricaoGenerico1{color: <?=$configsModelo->servico3_corDescricaoGenerico1?> !important;}

        .servico3_corBoxImage{background-color: <?=$configsModelo->servico3_corBoxImage?> !important;}

        .servico3_corTexto{color: <?=$configsModelo->servico3_corTexto?> !important;}

        /* FIM CONFIGS SERVIÇO 3 */

        /* INICIO CONFIGS SERVIÇO 4 */

        .servico4_form5 {
            padding: 40px 0 50px 0 !important;
            background-color: <?=$configsModelo->body_servico4_form5_corFundoForm?> !important;
        }
        .corFundoBotaoForm5 {
            background-color: <?=$configsModelo->body_servico4_form5_corFundoBotao?> !important;
            color: <?=$configsModelo->body_servico4_form5_corTextBotao?> !important;
        }
        
        .servico4{background-color: <?=$configsModelo->servico4_corBg?> !important;}

        .body_textoGenericoTitulo4{color: <?=$configsModelo->servico4_corTituloGenerico1?> !important;}
        .body_textoGenericoDescricao4{color: <?=$configsModelo->servico4_corDescricaoGenerico1?> !important;}
        
        .botao_serv4 {
            color: <?=$configsModelo->servico4_corTextoBotao?> !important;
            background-color: <?=$configsModelo->servico4_corFundoBotao?> !important;
            border-color: <?=$configsModelo->servico4_corFundoBotao?> !important;
        }

        .botao_serv4:hover {
            color: <?=$configsModelo->servico4_corTextoHoverBotao?> !important;
            background-color: <?=$configsModelo->servico4_corFundoHoverBotao?> !important;
            border-color: <?=$configsModelo->servico4_corFundoHoverBotao?> !important;
        }

        /* FIM CONFIGS SERVIÇO 4 */

        /* INICIO CONFIGS SERVIÇO 5 */
        
        .servico5 {
            background-color: <?=$configsModelo->servico5_corBg?> !important;
            padding-bottom: 60px;
        }

        .servico5_form6 {
            padding: 40px 0 50px 0;
            background-color: <?=$configsModelo->body_servico5_form6_corFundoForm?> !important;
        }

        .corFundoBotaoForm6 {
            background-color: <?=$configsModelo->body_servico5_form6_corFundoBotao?> !important;
            color: <?=$configsModelo->body_servico5_form6_corTextBotao?> !important;
        }
        
        .servico5_corTituloGenerico1{color: <?=$configsModelo->servico5_corTituloGenerico1?> !important;}

        .servico5_corDescricaoGenerico1{color: <?=$configsModelo->servico5_corDescricaoGenerico1?> !important;}

        .servico5_corBoxImage {
            background-color: <?=$configsModelo->servico5_corBoxImage?> !important;
            margin-top: 30px !important;
        }

        .img-fluid {
            margin-bottom: 20px !important;
        }

        .servico5_corTexto{color: <?=$configsModelo->servico5_corTexto?> !important;}

        .servico5_corDivisoria {
            border-bottom: 1px solid <?=$configsModelo->servico5_corTexto?> !important;
        }

        .botao_serv5 {
            color: <?=$configsModelo->servico5_corTextoBotao?> !important;
            background-color: <?=$configsModelo->servico5_corFundoBotao?> !important;
            border-color: <?=$configsModelo->servico5_corFundoBotao?> !important;
        }

        .botao_serv5:hover {
            color: <?=$configsModelo->servico5_corTextoHoverBotao?> !important;
            background-color: <?=$configsModelo->servico5_corFundoHoverBotao?> !important;
            border-color: <?=$configsModelo->servico5_corFundoHoverBotao?> !important;
        }
        
        .servico5_form6 {
            padding: 40px 0 50px 0 !important;
            background-color: <?=$configsModelo->body_servico5_form6_corFundoForm?>;
        }

        .corFundoBotaoForm6 {
            background-color: <?=$configsModelo->body_servico5_form6_corFundoBotao?> !important;
            color: <?=$configsModelo->body_servico5_form6_corTextBotao?> !important;
        }

        /* FIM CONFIGS SERVIÇO 5 */

        /* INICIO CONFIGS SERVIÇO 6 */
        
        .servico6{background-color: <?=$configsModelo->servico6_corBg?> !important;}

        .servico6_form7 {
            padding: 40px 0 50px 0;
            background-color: <?=$configsModelo->body_servico6_form7_corFundoForm?> !important;
        }

        .corFundoBotaoForm7 {
            background-color: <?=$configsModelo->body_servico6_form7_corFundoBotao?> !important;
            color: <?=$configsModelo->body_servico6_form7_corTextBotao?> !important;
        }
        
        .servico6_corTituloGenerico1{color: <?=$configsModelo->servico6_corTituloGenerico1?> !important;}

        .servico6_corDescricaoGenerico1{color: <?=$configsModelo->servico6_corDescricaoGenerico1?> !important;}

        .servico6_corTexto{color: <?=$configsModelo->servico6_corTexto?> !important;}

        .testimonial .owl-dots .owl-dot.active span {background: <?=$configsModelo->servico6_corTituloGenerico1?> !important;}

        /* FIM CONFIGS SERVIÇO 6 */

        /* INICIO CONFIGS SERVIÇO 7 */

        .servico7 {
            background-color: <?=$configsModelo->servico7_corBg?> !important;
            padding: 70px 0px !important;
        }

        .servico7_form8 {
            padding: 40px 0 50px 0 !important;
            background-color: <?=$configsModelo->body_servico7_form8_corFundoForm?> !important;
        }

        .corFundoBotaoForm8 {
            background-color: <?=$configsModelo->body_servico7_form8_corFundoBotao?> !important;
            color: <?=$configsModelo->body_servico7_form8_corTextBotao?> !important;
        }
        
        .servico7_corTituloGenerico1{color: <?=$configsModelo->servico7_corTituloGenerico1?> !important;}

        .servico7_corDescricaoGenerico1{color: <?=$configsModelo->servico7_corDescricaoGenerico1?> !important;}

        /* FIM CONFIGS SERVIÇO 7*/


        /* INICIO CONFIGS SERVIÇO REDES SOCIAIS*/

        .servico_redesSociais_corTituloGenerico1{color: <?=$configsModelo->servico_redesSociais_corTituloGenerico1?> !important;}
        .servico_redesSociais_corDescricaoGenerico1{color: <?=$configsModelo->servico_redesSociais_corDescricaoGenerico1?> !important;}

        .redesSociais-alinhamento {
            justify-content: center;
        }

        .botao_RedesSociais {
            margin-top: 20px !important;
            color: <?=$configsModelo->servico_redesSociais_corTextoBotao?> !important;
            background-color: <?=$configsModelo->servico_redesSociais_corFundoBotao?> !important;
            border-color: <?=$configsModelo->servico_redesSociais_corTextoBotao?> !important;
        }

        .botao_RedesSociais:hover {
            margin-top: 20px !important;
            color: <?=$configsModelo->servico_redesSociais_corTextoHoverBotao?> !important;
            background-color: <?=$configsModelo->servico_redesSociais_corFundoHoverBotao?> !important;
            border-color: <?=$configsModelo->servico_redesSociais_corTextoHoverBotao?> !important;
        }

        .objectIMG_redesSociais{
            object-fit: contain !important;
            width: 100% !important;
        }

        .serv_redesSociais {
            background-color: <?=$configsModelo->servico_redesSociais_corBg?> !important;
        }

        .boxIMG_redesSociais {
            background-color: <?=$configsModelo->servico_redesSociais_corBoxImage?> !important;
        }

        .boxIMG_redesSociais:hover {
            background-color: <?=$configsModelo->servico_redesSociais_corBoxImage?> !important;
        }

        .servico_redesSociais_corTexto{
            color: <?=$configsModelo->servico_redesSociais_corTexto?> !important;
        }

        .sevico_Form_redesSociais {
            padding: 20px 0 40px 0;
            background-color: <?=$configsModelo->body_servico_RedesSociais_form_corFundoForm?> !important;
        }

        .corFundoBotaoForm_RedesSociais{
            background-color: <?=$configsModelo->body_servico_RedesSociais_form_corFundoBotao?> !important;
            color: <?=$configsModelo->body_servico_RedesSociais_form_corTextBotao?> !important;
        }
        
        /* FIM CONFIGS SERVIÇO REDES SOCIAIS*/

        /* INICIO CONFIGS SERVIÇO MAPS*/

        .servico_maps {
            padding: 40px 0 50px 0 !important;
            background-color: <?=$configsModelo->servico_maps_corBg?> !important;
        }

        .servico_maps_corTituloGenerico1 {
            color: <?=$configsModelo->servico_maps_corTituloGenerico1?> !important;
        }
        .servico_maps_corDescricaoGenerico1 {
            color: <?=$configsModelo->servico_maps_corDescricaoGenerico1?> !important;
        }

        /* FIM CONFIGS SERVIÇO MAPS*/
        
        .hero-banner::after {
            content: "";
            display: block !important;
            position: absolute !important;
            top: 0 !important;
            left: 0 !important;
            width: 100% !important;
            height: 100% !important;
            
            <?php if($configsModelo->body_imagemFundoSlide === "0") : ?>
                background: none !important;
            <?php else: ?>
                background: url(/assets/images/slide/<?=$configsModelo->body_slide_imagemFundoSlide?>)center no-repeat !important;
            <?php endif ?>
            
            object-fit: cover !important;
            z-index: -1 !important; 
        }
        
    </style>

    <style>

        <?=$configsModelo->topo_head_css?>

    </style>
    
</head>

<body>

    <input type="hidden" value="<?=$token?>" name="idToken" id="idToken">
    <input type="hidden" value="<?=$dadosLandingPage->link_landingPage?>" name="linkLogo" id="linkLogo">
    <input type="hidden" value="<?=$configsModelo->body_slide_botaoLink1?>" name="linkBotao" id="linkBotao">
    
    

            <header class="header_area">
                <div class="main_menu corFundoMenu">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="container box_1620">
                            
                            <?php if($configsModelo->body_logo1 === '1') : ?>
                                <a class="navbar-brand logo_h" href="<?=$dadosLandingPage->link_landingPage?>" id="botaoLogo" name="botaoLogo">
                                    <img src="<?=base_url('assets/images/logofavicon/')?><?=$configs->logo_landingPage?>" alt="" height="45px">
                                </a> 
                            <?php endif ?>

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                               
                                <ul class="navbar-right col-12">
                                    <li class="nav-item">
                                        <a id="botaoRodapeFacebook" href="https://www.facebook.com/<?=$configs->linkFacebook_landingPage?>" target="_blank">
                                            <button class="button btn-xs button-header bg"><i class="fab fa-facebook-f"></i></button>
                                        </a>

                                        <a id="botaoRodapeInstagram" href="https://www.instagram.com/<?=$configs->linkInstagram_landingPage?>" target="_blank">
                                            <button class="button btn-xs button-header bg"><i class="fab fa-instagram"></i></button>
                                        </a>
                                        
                                        <?php
                                            if(!empty($configs->fone1_landingPage)) 
                                            {
                                                if($configs->verificaWhatsapp1_landingPage === '1') { ?>
                                                    
                                                    <a id="botaoRodapeWhatsApp1" href="https://api.whatsapp.com/send?phone=55<?=$configs->fone1_landingPage?>&text=<?=$configs->fone1TextoWhats_landingPage?>" target="_blank">
                                                        <button class="button btn-xs button-header bg"><i class="fab fa-whatsapp"></i></button>
                                                    </a>
                                                    
                                                <?php } else {  ?>

                                                    <a data-toggle="modal" data-target="#telefone1">
                                                        <button class="button btn-xs button-header bg"><i class="fa fa-phone"></i></button>
                                                    </a>
                                                    
                                                <?php }
                                            }

                                            if(!empty($configs->fone2_landingPage)) 
                                            {
                                                if($configs->verificaWhatsapp2_landingPage === '1') { ?>

                                                    <a id="botaoRodapeWhatsApp2" href="https://api.whatsapp.com/send?phone=55<?=$configs->fone2_landingPage?>&text=<?=$configs->fone2TextoWhats_landingPage?>" target="_blank">
                                                        <button class="button btn-xs button-header bg"><i class="fab fa-whatsapp"></i></button>
                                                    </a>
                                                    
                                                <?php } else {  ?>

                                                    <a data-toggle="modal" data-target="#telefone2">
                                                        <button class="button btn-xs button-header bg"><i class="fa fa-phone"></i></button>
                                                    </a>
                                                    
                                                <?php }
                                            }
                                        ?>
                                        
                                    </li>

                                </ul>

                            </div> 
                            
                        </div>

                    </nav>

                </div>

            </header>
            
    <main class="side-main">

        <?php if($configsModelo->body_slide1 === '1') : ?>
            <section style="background-color: <?=$configsModelo->corFundoSlide1?>;">
                <center>
                <img class="img-fluid" alt="<?=$configsModelo->body_slide_imagem2_SEO?>" title="<?=$configsModelo->body_slide_imagem2_SEO?>" style="margin-top: 6.4em; margin-bottom: 0px !important;" src="<?=base_url('assets/images/slide/')?><?=$configsModelo->body_slide_imagem2?>" alt="">
                </center>  
            </section>
        <?php endif ?>

        <?php if($configsModelo->body_slide === '1') : ?>

            <section class="hero-banner mb-30px fundoBgSlide" style="padding-bottom: 200px;">
                <div class="container">
                    <div class="row">
                        
                        <?php if($configsModelo->body_imagemPrincipalSlide === "1") : ?>
                            <div class="col-lg-7">
                                <div class="hero-banner__img">
                                    <img class="img-fluid" src="<?=base_url('assets/images/slide/')?><?=$configsModelo->body_slide_imagem1?>" alt="">
                                </div>
                            </div>
                        <?php endif ?>
                        
                        <?php if($configsModelo->body_imagemPrincipalSlide === "0") : ?>
                            <div class="col-lg-12 text-center pt-5">
                        <?php else : ?>
                            <div class="col-lg-5 pt-5">
                        <?php endif ?>
                        
                            <div class="hero-banner__content">
                                <h1 class="corTextoTituloSlide"><?=$configsModelo->body_slide_titulo1?></h1>
                                <p class="corTextoDescricaoSlide"><?=$configsModelo->body_slide_descricao1?></p>

                                <?php
                                
                                    if(empty($configsModelo->body_slide_botaoLink1))
                                    {
                                        $verificaLink = "href='JavaScript:Void(0);'";
                                        
                                    } else {
                                        
                                        $verificaLink = "href='".$configsModelo->body_slide_botaoLink1."' target='_blank'";
                                    }

                                
                                ?>

                                <?php if($configsModelo->body_slide_botao1 === '1') : ?>
                                    <a id="botaoSlide" name="botaoSlide" class="button bg corTextoBotaoSlide" <?=$verificaLink?>>
                                        <?=$configsModelo->body_slide_botaoTexto1?>
                                    </a>
                                <?php endif ?>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico1_form1 === '1') : ?>

            <section class="sevico1_Form1">
                    
                <div class="container">

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsForm1">

                            <div class="form-row">

                                <?php if(!empty($configsModelo->body_servicoForm_tituloForm1) && !empty($configsModelo->body_servicoForm_descricaoForm1)) : ?>
                                    <div class="col-12 mb-4">
                                        <div class="section-intro text-center">
                                            <h2 class="servico_form1_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloForm1?></h2>
                                            <p class="servico_form1_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoForm1?></p>
                                        </div>
                                    </div>
                                <?php endif ?>

                                <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">
                                
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control" placeholder="Seu nome" id="nomeVisitante" name="nomeVisitante" autocomplete="off">
                                </div>
                                
                                <div class="form-group col-md-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" 
                                        id="telefoneVisitante" name="telefoneVisitante" autocomplete="off"
                                        data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div class="form-group col-md-3">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitante" name="emailVisitante" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <select class="form-control" id="ondeNosConheceu" name="ondeNosConheceu">
                                            
                                        <option value="">Onde nos conheceu?</option>
                                        
                                        <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                            <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                        <?php endif ?>

                                        <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                            <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                        <?php endif ?>

                                        <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                            <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                        <?php endif ?>

                                        <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                            <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                        <?php endif ?>

                                        <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                            <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                        <?php endif ?>

                                        <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                            <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                        <?php endif ?>

                                        <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                            <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                        <?php endif ?>

                                        <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                            <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                        <?php endif ?>

                                    </select>
                                </div>
                                
                                <div class="form-group col-md-2">
                                    <input type="submit" class="form-control btn-block btn waves-effect waves-light corFundoBotaoForm1" id="leadsBotaoForm1" value="<?=$configsModelo->body_servico1_form1_textBotao?>">
                                </div>
                            </div>

                            <div id="erroMsg1"></div>
                            
                        </form>

                    </div>

                </div>

            </section>

        <?php endif ?>

        <?php if($configsModelo->body_redesSociais === '1') : ?>

            <section class="section-padding--small serv_redesSociais">
                
                <div class="container">

                    <?php if($configsModelo->body_conteudoGenerico_redesSociais === '1') : ?>
                        <div class="section-intro text-center">
                            <h2 class="servico_redesSociais_corTituloGenerico1 section-intro__title"><?=$configsModelo->body_textoGenericoTitulo_redesSociais?></h2>
                            <p class="servico_redesSociais_corDescricaoGenerico1 section-intro__subtitle"><?=$configsModelo->body_textoGenericoDescricao_redesSociais?></p>
                        </div>
                    <?php endif ?>
                    
                    <div class="container mt-5">
                    
                        <div class="row redesSociais-alinhamento">
                            
                            <?php if($configsModelo->body_redesSociais_opcao1 === '1') : ?>

                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG_redesSociais text-center mb-4 mb-lg-0">
                                        
                                        <center>
                                            <img class="objectIMG_redesSociais" src="<?=base_url('/assets/images/social/').$configsModelo->body_servico_redesSociais_Imagem1?>" alt="<?=$configsModelo->body_servico_redesSociais_ImagemSEO1?>" title="<?=$configsModelo->body_servico_redesSociais_ImagemSEO1?>">
                                        </center>

                                        <h3 class="card-feature__title servico_redesSociais_corTexto mt-3"><?=$configsModelo->body_servico_redesSociais_Titulo1?></h3>                                   
                                        <h6 class="card-feature__subtitle servico_redesSociais_corTexto"><?=$configsModelo->body_servico_redesSociais_Descricao1?></h6>

                                        <?php
                                            
                                            if(empty($configsModelo->body_servico_redesSociais_botaoLink1))
                                            {
                                                $botaoRedesSociais1 = "href='JavaScript:Void(0);'";

                                            } else {
                                                
                                                $botaoRedesSociais1 = "href='".$configsModelo->body_servico_redesSociais_botaoLink1."' target='_blank'";
                                            }
                                                
                                        ?>

                                        <?php if($configsModelo->body_servico_redesSociais_botao1 === '1') : ?>
                                            <a id="botaoRedeSocial1" class="button button-light botao_RedesSociais" <?=$botaoRedesSociais1?>>
                                                <?=$configsModelo->body_servico_redesSociais_botaoTexto1?>
                                            </a>
                                        <?php endif ?>

                                    </div>
                                </div>

                            <?php endif ?>

                            <?php if($configsModelo->body_redesSociais_opcao2 === '1') : ?>

                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG_redesSociais text-center mb-4 mb-lg-0">
                                        
                                        <center>
                                            <img class="objectIMG_redesSociais" src="<?=base_url('/assets/images/social/').$configsModelo->body_servico_redesSociais_Imagem2?>" alt="<?=$configsModelo->body_servico_redesSociais_ImagemSEO2?>" title="<?=$configsModelo->body_servico_redesSociais_ImagemSEO2?>">
                                        </center>
                                        
                                        <h3 class="card-feature__title servico_redesSociais_corTexto mt-3"><?=$configsModelo->body_servico_redesSociais_Titulo2?></h3>                                   
                                        <h6 class="card-feature__subtitle servico_redesSociais_corTexto"><?=$configsModelo->body_servico_redesSociais_Descricao2?></h6>
                                        
                                        <?php
                                            
                                            if(empty($configsModelo->body_servico_redesSociais_botaoLink2))
                                            {
                                                $botaoRedesSociais2 = "href='JavaScript:Void(0);'";

                                            } else {
                                                
                                                $botaoRedesSociais2 = "href='".$configsModelo->body_servico_redesSociais_botaoLink2."' target='_blank'";
                                            }
                                                
                                        ?>

                                        <?php if($configsModelo->body_servico_redesSociais_botao2 === '1') : ?>
                                            <a id="botaoRedeSocial2" class="button button-light botao_RedesSociais" <?=$botaoRedesSociais2?>>
                                                <?=$configsModelo->body_servico_redesSociais_botaoTexto2?>
                                            </a>
                                        <?php endif ?>

                                    </div>
                                </div>

                            <?php endif ?>

                            <?php if($configsModelo->body_redesSociais_opcao3 === '1') : ?>

                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG_redesSociais text-center mb-4 mb-lg-0">
                                        
                                        <center>
                                            <img class="objectIMG_redesSociais" src="<?=base_url('/assets/images/social/').$configsModelo->body_servico_redesSociais_Imagem3?>" alt="<?=$configsModelo->body_servico_redesSociais_ImagemSEO3?>" title="<?=$configsModelo->body_servico_redesSociais_ImagemSEO3?>">
                                        </center>

                                        <h3 class="card-feature__title servico_redesSociais_corTexto mt-3"><?=$configsModelo->body_servico_redesSociais_Titulo3?></h3>                                   
                                        <h6 class="card-feature__subtitle servico_redesSociais_corTexto"><?=$configsModelo->body_servico_redesSociais_Descricao3?></h6>

                                        <?php
                                            
                                            if(empty($configsModelo->body_servico_redesSociais_botaoLink3))
                                            {
                                                $botaoRedesSociais3 = "href='JavaScript:Void(0);'";

                                            } else {
                                                
                                                $botaoRedesSociais3 = "href='".$configsModelo->body_servico_redesSociais_botaoLink3."' target='_blank'";
                                            }
                                                
                                        ?>

                                        <?php if($configsModelo->body_servico_redesSociais_botao3 === '1') : ?>
                                            <a id="botaoRedeSocial3" class="button button-light botao_RedesSociais" <?=$botaoRedesSociais3?>>
                                                <?=$configsModelo->body_servico_redesSociais_botaoTexto3?>
                                            </a>
                                        <?php endif ?>

                                    </div>
                                </div>

                            <?php endif ?>
                            

                        </div>


                    </div>
                    
                </div>
                
            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico_RedesSociais_form === '1') : ?>

            <section class="sevico_Form_redesSociais">
                    
                <div class="container">

                    <?php if(!empty($configsModelo->body_servicoForm_tituloFormRS) && !empty($configsModelo->body_servicoForm_descricaoFormRS)) : ?>
                        <div class="col-12 mb-4">
                            <div class="section-intro text-center">
                                <h2 class="servico_formRS_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloFormRS?></h2>
                                <p class="servico_formRS_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoFormRS?></p>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsFormRS">

                            <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">

                            <div class="form-row align-items-center">
                                
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label for="example-select"></label>
                                    <input type="text" class="form-control mb-2" placeholder="Seu nome" id="nomeVisitanteRS" name="nomeVisitante" autocomplete="off">
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" id="telefoneVisitanteRS" name="telefoneVisitante" autocomplete="off"
                                                data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitanteRS" name="emailVisitante" autocomplete="off">
                                    </div>
                                </div> 
                                
                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label></label>
                                    <div class="input-group mb-2">
                                        <select class="form-control" id="ondeNosConheceuRS" name="ondeNosConheceu">
                                            
                                            <option>Onde nos conheceu?</option>
                                            
                                            <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
                                    <div class="input-group mt-3">
                                        <button type="submit" class="form-control btn btn-block waves-effect waves-light corFundoBotaoForm_RedesSociais" id="leadsBotaoFormRS"><?=$configsModelo->body_servico_RedesSociais_form_textBotao?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="erroMsgRS"></div>

                        </form>

                    </div>

                </div>

            </section>

        <?php endif ?>
        
        <?php if($configsModelo->body_servico1 === '1') : ?>

            <section class="section-margin-serv1">
                
                <div class="container">

                    <?php if($configsModelo->body_conteudoGenerico1 === '1') : ?>
                        <div class="section-intro text-center">
                            <h2 class="servico1_corTituloGenerico1 section-intro__title"><?=$configsModelo->body_textoGenericoTitulo1?></h2>
                            <p class="servico1_corDescricaoGenerico1 section-intro__subtitle"><?=$configsModelo->body_textoGenericoDescricao1?></p>
                        </div>
                    <?php endif ?>

                    <div class="container mt-5 text-center">

                        <div class="row">

                            <?php if($configsModelo->body_servico1_opcao1 === '1') : ?>

                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG text-center mb-4 mb-lg-0">
                                        <img class="objectIMG" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico1Imagem1?>" alt="<?=$configsModelo->body_servico1ImagemSEO1?>" title="<?=$configsModelo->body_servico1ImagemSEO1?>">
                                        <h3 class="card-feature__title servico1_corTexto mt-3"><?=$configsModelo->body_servico1Titulo1?></h3>                                   
                                        <h6 class="card-feature__subtitle servico1_corTexto"><?=$configsModelo->body_servico1Descricao1?></h6>
                                    </div>
                                </div>

                            <?php endif ?>

                            <?php if($configsModelo->body_servico1_opcao2 === '1') : ?>

                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG text-center mb-4 mb-lg-0">
                                        <img class="objectIMG" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico1Imagem2?>" alt="<?=$configsModelo->body_servico1ImagemSEO2?>" title="<?=$configsModelo->body_servico1ImagemSEO2?>">
                                        <h3 class="card-feature__title servico1_corTexto mt-3"><?=$configsModelo->body_servico1Titulo2?></h3>
                                        <h6 class="card-feature__subtitle servico1_corTexto"><?=$configsModelo->body_servico1Descricao2?></h6>
                                    </div>
                                </div>

                            <?php endif ?>

                            <?php if($configsModelo->body_servico1_opcao3 === '1') : ?>

                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG text-center mb-4 mb-lg-0">
                                        <img class="objectIMG" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico1Imagem3?>" alt="<?=$configsModelo->body_servico1ImagemSEO3?>" title="<?=$configsModelo->body_servico1ImagemSEO3?>">
                                        <h3 class="card-feature__title servico1_corTexto mt-3"><?=$configsModelo->body_servico1Titulo3?></h3>
                                        <h6 class="card-feature__subtitle servico1_corTexto"><?=$configsModelo->body_servico1Descricao3?></h6>
                                    </div>
                                </div>

                            <?php endif ?>

                            <?php if($configsModelo->body_servico1_opcao4 === '1') : ?>
                                
                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG text-center mb-4 mb-lg-0">
                                        <img class="objectIMG" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico1Imagem4?>" alt="<?=$configsModelo->body_servico1ImagemSEO4?>" title="<?=$configsModelo->body_servico1ImagemSEO4?>">
                                        <h3 class="card-feature__title servico1_corTexto mt-3"><?=$configsModelo->body_servico1Titulo4?></h3>
                                        <h6 class="card-feature__subtitle servico1_corTexto"><?=$configsModelo->body_servico1Descricao4?></h6>
                                    </div>
                                </div>

                            <?php endif ?>
                            
                            <?php if($configsModelo->body_servico1_opcao5 === '1') : ?>
                                
                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG text-center mb-4 mb-lg-0">
                                        <img class="objectIMG" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico1Imagem5?>" alt="<?=$configsModelo->body_servico1ImagemSEO5?>" title="<?=$configsModelo->body_servico1ImagemSEO5?>">
                                        <h3 class="card-feature__title servico1_corTexto mt-3"><?=$configsModelo->body_servico1Titulo5?></h3>
                                        <h6 class="card-feature__subtitle servico1_corTexto"><?=$configsModelo->body_servico1Descricao5?></h6>
                                    </div>
                                </div>

                            <?php endif ?>

                            <?php if($configsModelo->body_servico1_opcao6 === '1') : ?>

                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG text-center mb-4 mb-lg-0">
                                        <img class="objectIMG" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico1Imagem6?>" alt="<?=$configsModelo->body_servico1ImagemSEO6?>" title="<?=$configsModelo->body_servico1ImagemSEO6?>">
                                        <h3 class="card-feature__title servico1_corTexto mt-3"><?=$configsModelo->body_servico1Titulo6?></h3>
                                        <h6 class="card-feature__subtitle servico1_corTexto"><?=$configsModelo->body_servico1Descricao6?></h6>
                                    </div>
                                </div>

                            <?php endif ?>

                            <?php if($configsModelo->body_servico1_opcao7 === '1') : ?>

                                <div class="col-lg-3">
                                    <div class="card card-feature boxIMG text-center mb-4 mb-lg-0">
                                        <img class="objectIMG" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico1Imagem7?>" alt="<?=$configsModelo->body_servico1ImagemSEO7?>" title="<?=$configsModelo->body_servico1ImagemSEO7?>">
                                        <h3 class="card-feature__title servico1_corTexto mt-3"><?=$configsModelo->body_servico1Titulo7?></h3>
                                        <h6 class="card-feature__subtitle servico1_corTexto"><?=$configsModelo->body_servico1Descricao7?></h6>
                                    </div>
                                </div>

                            <?php endif ?>

                            <?php if($configsModelo->body_servico1_opcao8 === '1') : ?>

                            <div class="col-lg-3">
                                <div class="card card-feature boxIMG text-center mb-4 mb-lg-0">
                                    <img class="objectIMG" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico1Imagem8?>" alt="<?=$configsModelo->body_servico1ImagemSEO8?>" title="<?=$configsModelo->body_servico1ImagemSEO8?>">
                                    <h3 class="card-feature__title servico1_corTexto mt-3"><?=$configsModelo->body_servico1Titulo8?></h3>
                                    <h6 class="card-feature__subtitle servico1_corTexto"><?=$configsModelo->body_servico1Descricao8?></h6>
                                </div>
                            </div>

                            <?php endif ?>

                        </div>

                    </div>

                </div>

            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico1_form2 === '1') : ?>

            <section class="sevico1_Form2">
                    
                <div class="container">

                    <?php if(!empty($configsModelo->body_servicoForm_tituloForm2) && !empty($configsModelo->body_servicoForm_descricaoForm2)) : ?>
                        <div class="col-12 mb-4">
                            <div class="section-intro text-center">
                                <h2 class="servico_form2_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloForm2?></h2>
                                <p class="servico_form2_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoForm2?></p>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsForm2">

                            <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">

                            <div class="form-row align-items-center">
                                
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label for="example-select"></label>
                                    <input type="text" class="form-control mb-2" placeholder="Seu nome" id="nomeVisitante2" name="nomeVisitante" autocomplete="off">
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" id="telefoneVisitante2" name="telefoneVisitante" autocomplete="off"
                                                data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitante2" name="emailVisitante" autocomplete="off" autocomplete="off">
                                    </div>
                                </div> 
                                
                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label></label>
                                    <div class="input-group mb-2">
                                        <select class="form-control" id="ondeNosConheceu2" name="ondeNosConheceu">
                                            
                                            <option>Onde nos conheceu?</option>
                                            
                                            <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
                                    <div class="input-group mt-3">
                                        <button type="submit" class="btn btn-block waves-effect waves-light corFundoBotaoForm2" id="leadsBotaoForm2"><?=$configsModelo->body_servico1_form2_textBotao?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="erroMsg2"></div>


                        </form>

                    </div>

                </div>

            </section>

        <?php endif ?>
        
        <?php if($configsModelo->body_servico2 === '1') : ?>

            <section class="section-padding--small servico2">
                <div class="container">
                    <div class="row no-gutters align-items-center">
                        <div class="col-md-5 mb-5 mb-md-0">
                            <div class="about__content">

                                <?php if($configsModelo->body_conteudoGenerico2 === '1') : ?>

                                    <h2 class="corTexto_servico2Titulo1"><?=$configsModelo->body_textoGenericoTitulo2?></h2>
                                    <p class="corDescricao_servico2Descricao1"><?=$configsModelo->body_textoGenericoDescricao2?></p>
                                
                                <?php endif; ?>

                                <?php
                                
                                    if(empty($configsModelo->body_servico2_botaoLink1))
                                    {
                                        $botaoServ2 = "href='JavaScript:Void(0);'";

                                    } else {
                                        
                                        $botaoServ2 = "href='".$configsModelo->body_servico2_botaoLink1."' target='_blank'";
                                    }
                                    
                                ?>

                                <?php if($configsModelo->body_servico2_botao1 === '1') : ?>
                                    <a id="botaoServico2" class="button button-light botao_serv2" <?=$botaoServ2?>>
                                        <?=$configsModelo->body_servico2_botaoTexto1?>
                                    </a>
                                <?php endif ?>
                                
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="about__img">
                                <img class="img-fluid" src="<?=base_url('/assets/images/servicos/').$configsModelo->body_servico2Imagem1?>" title="<?=$configsModelo->body_servico2ImagemSEO1?>" alt="<?=$configsModelo->body_servico2ImagemSEO1?>">
                            </div>
                        </div>

                    </div>
                </div>
            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico2_form3 === '1') : ?>

            <section class="sevico2_Form3">
                    
                <div class="container">

                    <?php if(!empty($configsModelo->body_servicoForm_tituloForm3) && !empty($configsModelo->body_servicoForm_descricaoForm3)) : ?>
                        <div class="col-12 mb-4">
                            <div class="section-intro text-center">
                                <h2 class="servico_form3_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloForm3?></h2>
                                <p class="servico_form3_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoForm3?></p>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsForm3">

                            <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">

                            <div class="form-row align-items-center">
                                
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label for="example-select"></label>
                                    <input type="text" class="form-control mb-2" placeholder="Seu nome" id="nomeVisitante3" name="nomeVisitante" autocomplete="off">
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" id="telefoneVisitante3" name="telefoneVisitante" autocomplete="off"
                                                data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitante3" name="emailVisitante" autocomplete="off">
                                    </div>
                                </div> 
                                
                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label></label>
                                    <div class="input-group mb-2">
                                    <select class="form-control" id="ondeNosConheceu3" name="ondeNosConheceu">
                                            
                                            <option>Onde nos conheceu?</option>
                                            
                                            <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
                                    <div class="input-group mt-3">
                                        <button type="submit" class="btn btn-block waves-effect waves-light corFundoBotaoForm3" id="leadsBotaoForm3"><?=$configsModelo->body_servico2_form3_textBotao?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="erroMsg3"></div>

                        </form>

                    </div>
                    
                </div>

            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico3 === '1') : ?>

            <section class="servico3">
            
                <div class="container">

                    <?php if($configsModelo->body_conteudoGenerico3 === '1') : ?>

                        <div class="section-intro pb-85px text-center mt-5">
                            <h2 class="servico3_corTituloGenerico1 section-intro__title"><?=$configsModelo->body_textoGenericoTitulo3?></h2>
                            <h6 class="servico3_corDescricaoGenerico1 section-intro__subtitle"><?=$configsModelo->body_textoGenericoDescricao3?></h6>
                        </div>

                    <?php endif ?>
                
                    <div class="row mb-5">
                        
                        <?php if($configsModelo->body_servico3_TodasOpcoes === '1') : ?>

                            <div class="col-lg-6">

                                <div class="row offer-single-wrapper">
                                    
                                    <?php if($configsModelo->body_servico3_opcao1 === '1') : ?>
                                        <div class="col-lg-6 offer-single">
                                            <div class="card offer-single__content text-center servico3_corBoxImage">
                                                
                                                <center>
                                                    <img class="mb-4 tamanhoImagemServ3" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico3Imagem1?>" title="<?=$configsModelo->body_servico3ImagemSEO1?>" alt="<?=$configsModelo->body_servico3ImagemSEO1?>"></a>
                                                </center>

                                                <h4 class="servico3_corTexto"><?=$configsModelo->body_servico3Titulo1?></h4>
                                                <h6 class="servico3_corTexto"><?=$configsModelo->body_servico3Descricao1?></h6>

                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <?php if($configsModelo->body_servico3_opcao2 === '1') : ?>
                                        <div class="col-lg-6 offer-single">
                                            <div class="card offer-single__content text-center servico3_corBoxImage">
                                                
                                                <center>
                                                    <img class="mb-4 tamanhoImagemServ3" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico3Imagem2?>" title="<?=$configsModelo->body_servico3ImagemSEO2?>" alt="<?=$configsModelo->body_servico3ImagemSEO2?>"></a>
                                                </center>

                                                <h4 class="servico3_corTexto"><?=$configsModelo->body_servico3Titulo2?></h4>
                                                <h6 class="servico3_corTexto"><?=$configsModelo->body_servico3Descricao2?></h6>
                                            
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    
                                    <?php if($configsModelo->body_servico3_opcao3 === '1') : ?>
                                        <div class="col-lg-6 offer-single">
                                            <div class="card offer-single__content text-center servico3_corBoxImage">
                                                
                                                <center>
                                                    <img class="mb-4 tamanhoImagemServ3" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico3Imagem3?>" title="<?=$configsModelo->body_servico3ImagemSEO3?>" alt="<?=$configsModelo->body_servico3ImagemSEO3?>"></a>
                                                </center>

                                                <h4 class="servico3_corTexto"><?=$configsModelo->body_servico3Titulo3?></h4>
                                                <h6 class="servico3_corTexto"><?=$configsModelo->body_servico3Descricao3?></h6>
                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <?php if($configsModelo->body_servico3_opcao4 === '1') : ?>
                                        <div class="col-lg-6 offer-single">
                                            <div class="card offer-single__content text-center servico3_corBoxImage">
                                                
                                            <center>
                                                    <img class="mb-4 tamanhoImagemServ3" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico3Imagem4?>" title="<?=$configsModelo->body_servico3ImagemSEO4?>" alt="<?=$configsModelo->body_servico3ImagemSEO4?>"></a>
                                                </center>

                                                <h4 class="servico3_corTexto"><?=$configsModelo->body_servico3Titulo4?></h4>
                                                <h6 class="servico3_corTexto"><?=$configsModelo->body_servico3Descricao4?></h6>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                </div>

                            </div>
                        <?php endif ?>

                        <?php if($configsModelo->body_servico3_TodasOpcoes === '0') : ?>
                            <div class="col-lg-12 text-center">
                        <?php else :  ?>
                            <div class="col-lg-6">
                        <?php endif ?>
                            <div class="offer-single__img">
                                <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico3Imagem5?>" title="<?=$configsModelo->body_servico3ImagemSEO5?>" alt="<?=$configsModelo->body_servico3ImagemSEO5?>">
                            </div>
                        </div>

                    </div>
                </div>
            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico3_form4 === '1') : ?>

            <section class="servico3_form4">
                    
                <div class="container">

                    <?php if(!empty($configsModelo->body_servicoForm_tituloForm4) && !empty($configsModelo->body_servicoForm_descricaoForm4)) : ?>
                        <div class="col-12 mb-4">
                            <div class="section-intro text-center">
                                <h2 class="servico_form4_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloForm4?></h2>
                                <p class="servico_form4_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoForm4?></p>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsForm4">

                            <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">

                            <div class="form-row align-items-center">
                                
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label for="example-select"></label>
                                    <input type="text" class="form-control mb-2" placeholder="Seu nome" id="nomeVisitante4" name="nomeVisitante" autocomplete="off">
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" id="telefoneVisitante4" name="telefoneVisitante" autocomplete="off"
                                                data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitante4" name="emailVisitante" autocomplete="off">
                                    </div>
                                </div> 
                                
                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label></label>
                                    <div class="input-group mb-2">
                                        <select class="form-control" id="ondeNosConheceu4" name="ondeNosConheceu">
                                            
                                            <option>Onde nos conheceu?</option>
                                            
                                            <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
                                    <div class="input-group mt-3">
                                        <button type="submit" class="btn btn-block  waves-effect waves-light corFundoBotaoForm4" id="leadsBotaoForm4"><?=$configsModelo->body_servico3_form4_textBotao?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="erroMsg4"></div>

                        </form>

                    </div>

                </div>

            </section>

        <?php endif ?>
        
        <?php if($configsModelo->body_servico4 === '1') : ?>

            <section class="section-padding--small servico4">
                <div class="container">
                    <div class="row align-items-center pt-xl-3 pb-xl-5">
                        <div class="col-lg-6">
                            <div class="solution__img text-center text-lg-left mb-4 mb-lg-0">
                                <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico4Imagem1?>" title="<?=$configsModelo->body_servico4ImagemSEO1?>" alt="<?=$configsModelo->body_servico4ImagemSEO1?>">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="solution__content">

                                <?php if($configsModelo->body_conteudoGenerico4 === '1') : ?>
                                    
                                    <h2 class="body_textoGenericoTitulo4"><?=$configsModelo->body_textoGenericoTitulo4?></h2>
                                    <p class="body_textoGenericoDescricao4"><?=$configsModelo->body_textoGenericoDescricao4?></p>
                                    
                                <?php endif;
                            
                                if(empty($configsModelo->body_servico4_botaoLink1))
                                {
                                    $botaoServ4 = "href='JavaScript:Void(0);'";

                                } else {
                                    
                                    $botaoServ4 = "href='".$configsModelo->body_servico4_botaoLink1."' target='_blank'";
                                }
                                
                            
                                if($configsModelo->body_servico4_botao1 === '1') : ?>
                                    <a id="botaoServico4" class="button button-light botao_serv4" <?=$botaoServ4?>>
                                        <?=$configsModelo->body_servico4_botaoTexto1?>
                                    </a>
                                <?php endif; ?>



                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico4_form5 === '1') : ?>

            <section class="servico4_form5">
                    
                <div class="container">

                    <?php if(!empty($configsModelo->body_servicoForm_tituloForm5) && !empty($configsModelo->body_servicoForm_descricaoForm5)) : ?>
                        <div class="col-12 mb-4">
                            <div class="section-intro text-center">
                                <h2 class="servico_form5_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloForm5?></h2>
                                <p class="servico_form5_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoForm5?></p>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsForm5">

                            <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">

                            <div class="form-row align-items-center">
                                
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label for="example-select"></label>
                                    <input type="text" class="form-control mb-2" placeholder="Seu nome" id="nomeVisitante5" name="nomeVisitante" autocomplete="off">
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" id="telefoneVisitante5" name="telefoneVisitante" autocomplete="off"
                                                data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitante5" name="emailVisitante" autocomplete="off">
                                    </div>
                                </div> 
                                
                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label></label>
                                    <div class="input-group mb-2">
                                        <select class="form-control" id="ondeNosConheceu5" name="ondeNosConheceu">
                                            
                                            <option>Onde nos conheceu?</option>
                                            
                                            <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
                                    <div class="input-group mt-3">
                                        <button type="submit" class="btn btn-block  waves-effect waves-light corFundoBotaoForm5" id="leadsBotaoForm5"><?=$configsModelo->body_servico4_form5_textBotao?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="erroMsg5"></div>
                        </form>

                    </div>

                </div>

            </section>

        <?php endif ?>
        
        <?php if($configsModelo->body_servico5 === '1') : ?>

            <section class="section-padding--small servico5">
                <div class="container">
                    
                    <div class="section-intro pb-85px text-center">

                        <?php if($configsModelo->body_conteudoGenerico5 === '1') : ?>
                            <h2 class="section-intro__title servico5_corTituloGenerico1"><?=$configsModelo->body_textoGenericoTitulo5?></h2>
                            <p class="section-intro__subtitle servico5_corDescricaoGenerico1"><?=$configsModelo->body_textoGenericoDescricao5?></p>
                        <?php endif; ?>

                    </div>

                    <div class="row">
                        
                        <?php if($configsModelo->body_servico5_opcao1 === '1') : ?>

                            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                                <div class="card text-center card-pricing servico5_corBoxImage">
                                    <div class="card-pricing__header servico5_corDivisoria">
                                        <h4 class="servico5_corTexto"><?=$configsModelo->body_servico5Titulo1?></h4>
                                        <p class="servico5_corTexto"><?=$configsModelo->body_servico5Descricao1?></p>
                                        
                                        <?php if($configsModelo->body_servico5_preco1_opcao1 === '1'): ?>
                                            <h1 class="servico5_corTexto"><span>R$ </span><?=number_format($configsModelo->body_servico5_preco1, 2, ',', '.')?></h1>
                                        <?php endif?>
                                        
                                    </div>
                                    <ul class="card-pricing__list servico5_corDivisoria">
                                        
                                        <?php if(!empty($configsModelo->body_servico5Imagem1)): ?>
                                            <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico5Imagem1?>" title="<?=$configsModelo->body_servico5ImagemSEO1?>" alt="<?=$configsModelo->body_servico5ImagemSEO1?>">
                                        <?php endif?>
                                        
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao1)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao1?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao2)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao2?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao3)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao3?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao4)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao4?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao5)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao5?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao6)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao6?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao7)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao7?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao8)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao8?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao9)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao9?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao1Descricao10)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao1Descricao10?><?php endif?>
                                    </ul>

                                    <?php
                                        
                                        if(empty($configsModelo->body_servico5_botaoLink1))
                                        {
                                            $botaoServ5_opcao1 = "href='JavaScript:Void(0);'";

                                        } else {
                                            
                                            $botaoServ5_opcao1 = "href='".$configsModelo->body_servico5_botaoLink1."' target='_blank'";
                                        }
                                        
                                    
                                        if($configsModelo->body_servico5_botao1 === '1') : ?>

                                            <div class="card-pricing__footer">
                                                
                                                <a id="botaoServico5opc1" <?=$botaoServ5_opcao1?>>
                                                    <button class="button button-light botao_serv5">
                                                        <?=$configsModelo->body_servico5_botaoTexto1?>
                                                    </button>
                                                </a>

                                            </div>
                                        
                                        <?php endif; 
                                            
                                    ?>
                                    
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico5_opcao2 === '1') : ?>

                            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                                <div class="card text-center card-pricing servico5_corBoxImage">
                                    <div class="card-pricing__header servico5_corDivisoria">
                                        <h4 class="servico5_corTexto"><?=$configsModelo->body_servico5Titulo2?></h4>
                                        <p class="servico5_corTexto"><?=$configsModelo->body_servico5Descricao2?></p>

                                        <?php if($configsModelo->body_servico5_preco2_opcao2 === '1'): ?>
                                            <h1 class="servico5_corTexto"><span>R$ </span><?=number_format($configsModelo->body_servico5_preco2, 2, ',', '.')?></h1>
                                        <?php endif?>

                                        
                                    </div>
                                    <ul class="card-pricing__list servico5_corDivisoria">
                                        
                                        <?php if(!empty($configsModelo->body_servico5Imagem2)): ?>
                                            <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico5Imagem2?>" title="<?=$configsModelo->body_servico5ImagemSEO2?>" alt="<?=$configsModelo->body_servico5ImagemSEO2?>">
                                        <?php endif?>
                                        
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao1)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao1?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao2)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao2?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao3)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao3?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao4)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao4?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao5)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao5?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao6)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao6?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao7)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao7?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao8)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao8?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao9)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao9?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao2Descricao10)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao2Descricao10?><?php endif?>
                                    </ul>

                                    <?php
                                        
                                        if(empty($configsModelo->body_servico5_botaoLink2))
                                        {
                                            $botaoServ5_opcao2 = "href='JavaScript:Void(0);'";

                                        } else {
                                            
                                            $botaoServ5_opcao2 = "href='".$configsModelo->body_servico5_botaoLink2."' target='_blank'";
                                        }
                                        
                                    
                                        if($configsModelo->body_servico5_botao2 === '1') : ?>

                                            <div class="card-pricing__footer">
                                                <a id="botaoServico5opc2" <?=$botaoServ5_opcao2?>>
                                                    <button class="button button-light botao_serv5">
                                                        <?=$configsModelo->body_servico5_botaoTexto2?>
                                                    </button>
                                                </a>
                                            </div>
                                            
                                        <?php endif; 
                                            
                                    ?>
                                    
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico5_opcao3 === '1') : ?>

                            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                                <div class="card text-center card-pricing servico5_corBoxImage">
                                    <div class="card-pricing__header servico5_corDivisoria">
                                        <h4 class="servico5_corTexto"><?=$configsModelo->body_servico5Titulo3?></h4>
                                        <p class="servico5_corTexto"><?=$configsModelo->body_servico5Descricao3?></p>

                                        <?php if($configsModelo->body_servico5_preco3_opcao3 === '1'): ?>
                                            <h1 class="servico5_corTexto"><span>R$ </span><?=number_format($configsModelo->body_servico5_preco3, 2, ',', '.')?></h1>
                                        <?php endif?>
                                        
                                    </div>
                                    <ul class="card-pricing__list servico5_corDivisoria">
                                        
                                        <?php if(!empty($configsModelo->body_servico5Imagem3)): ?>
                                            <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico5Imagem3?>" title="<?=$configsModelo->body_servico5ImagemSEO3?>" alt="<?=$configsModelo->body_servico5ImagemSEO3?>">
                                        <?php endif?>
                                        
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao1)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao1?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao2)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao2?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao3)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao3?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao4)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao4?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao5)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao5?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao6)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao6?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao7)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao7?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao8)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao8?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao9)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao9?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao3Descricao10)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao3Descricao10?><?php endif?>
                                    </ul>

                                    <?php
                                        
                                        if(empty($configsModelo->body_servico5_botaoLink3))
                                        {
                                            $botaoServ5_opcao3 = "href='JavaScript:Void(0);'";

                                        } else {
                                            
                                            $botaoServ5_opcao3 = "href='".$configsModelo->body_servico5_botaoLink3."' target='_blank'";
                                        }
                                        
                                    
                                        if($configsModelo->body_servico5_botao3 === '1') : ?>
                                            
                                            <div class="card-pricing__footer">
                                                <a id="botaoServico5opc3" <?=$botaoServ5_opcao3?>>
                                                    <button class="button button-light botao_serv5">
                                                        <?=$configsModelo->body_servico5_botaoTexto3?>
                                                    </button>
                                                </a>
                                            </div>
                                        
                                        <?php endif; 
                                            
                                    ?>
                                    
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico5_opcao4 === '1') : ?>

                            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                                <div class="card text-center card-pricing servico5_corBoxImage">
                                    <div class="card-pricing__header servico5_corDivisoria">
                                        <h4 class="servico5_corTexto"><?=$configsModelo->body_servico5Titulo4?></h4>
                                        <p class="servico5_corTexto"><?=$configsModelo->body_servico5Descricao4?></p>

                                        <?php if($configsModelo->body_servico5_preco4_opcao4 === '1'): ?>
                                            <h1 class="servico5_corTexto"><span>R$ </span><?=number_format($configsModelo->body_servico5_preco4, 2, ',', '.')?></h1>
                                        <?php endif?>
                                        
                                    </div>
                                    <ul class="card-pricing__list servico5_corDivisoria">
                                        
                                        <?php if(!empty($configsModelo->body_servico5Imagem4)): ?>
                                            <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico5Imagem4?>" title="<?=$configsModelo->body_servico5ImagemSEO4?>" alt="<?=$configsModelo->body_servico5ImagemSEO4?>">
                                        <?php endif?>
                                        
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao1)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao1?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao2)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao2?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao3)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao3?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao4)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao4?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao5)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao5?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao6)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao6?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao7)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao7?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao8)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao8?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao9)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao9?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao4Descricao10)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao4Descricao10?><?php endif?>
                                    </ul>

                                    <?php
                                        
                                        if(empty($configsModelo->body_servico5_botaoLink4))
                                        {
                                            $botaoServ5_opcao4 = "href='JavaScript:Void(0);'";

                                        } else {
                                            
                                            $botaoServ5_opcao4 = "href='".$configsModelo->body_servico5_botaoLink4."' target='_blank'";
                                        }
                                        
                                    
                                        if($configsModelo->body_servico5_botao4 === '1') : ?>
                                            
                                            <div class="card-pricing__footer">
                                                <a id="botaoServico5opc4" <?=$botaoServ5_opcao4?>>
                                                    <button class="button button-light botao_serv5">
                                                        <?=$configsModelo->body_servico5_botaoTexto4?>
                                                    </button>
                                                </a>
                                            </div>
                                        
                                        <?php endif; 
                                            
                                    ?>
                                    
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico5_opcao5 === '1') : ?>

                            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                                <div class="card text-center card-pricing servico5_corBoxImage">
                                    <div class="card-pricing__header servico5_corDivisoria">
                                        <h4 class="servico5_corTexto"><?=$configsModelo->body_servico5Titulo5?></h4>
                                        <p class="servico5_corTexto"><?=$configsModelo->body_servico5Descricao5?></p>

                                        <?php if($configsModelo->body_servico5_preco5_opcao5 === '1'): ?>
                                            <h1 class="servico5_corTexto"><span>R$ </span><?=number_format($configsModelo->body_servico5_preco5, 2, ',', '.')?></h1>
                                        <?php endif?>

                                        
                                    </div>
                                    <ul class="card-pricing__list servico5_corDivisoria">
                                        
                                        <?php if(!empty($configsModelo->body_servico5Imagem5)): ?>
                                            <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico5Imagem5?>" title="<?=$configsModelo->body_servico5ImagemSEO5?>" alt="<?=$configsModelo->body_servico5ImagemSEO5?>">
                                        <?php endif?>
                                        
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao1)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao1?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao2)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao2?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao3)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao3?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao4)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao4?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao5)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao5?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao6)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao6?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao7)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao7?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao8)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao8?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao9)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao9?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao5Descricao10)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao5Descricao10?><?php endif?>
                                    </ul>

                                    <?php
                                        
                                        if(empty($configsModelo->body_servico5_botaoLink5))
                                        {
                                            $botaoServ5_opcao5 = "href='JavaScript:Void(0);'";

                                        } else {
                                            
                                            $botaoServ5_opcao5 = "href='".$configsModelo->body_servico5_botaoLink5."' target='_blank'";
                                        }
                                        
                                    
                                        if($configsModelo->body_servico5_botao5 === '1') : ?>
                                        
                                            <div class="card-pricing__footer">
                                                <a id="botaoServico5opc5" <?=$botaoServ5_opcao5?>>
                                                    <button class="button button-light botao_serv5">
                                                        <?=$configsModelo->body_servico5_botaoTexto5?>
                                                    </button>
                                                </a>
                                            </div>

                                        <?php endif; 
                                            
                                    ?>
                                    
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico5_opcao6 === '1') : ?>

                            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                                <div class="card text-center card-pricing servico5_corBoxImage">
                                    <div class="card-pricing__header servico5_corDivisoria">
                                        <h4 class="servico5_corTexto"><?=$configsModelo->body_servico5Titulo6?></h4>
                                        <p class="servico5_corTexto"><?=$configsModelo->body_servico5Descricao6?></p>

                                        <?php if($configsModelo->body_servico5_preco6_opcao6 === '1'): ?>
                                            <h1 class="servico5_corTexto"><span>R$ </span><?=number_format($configsModelo->body_servico5_preco6, 2, ',', '.')?></h1>
                                        <?php endif?>
                                        
                                    </div>
                                    <ul class="card-pricing__list servico5_corDivisoria">
                                        
                                        <?php if(!empty($configsModelo->body_servico5Imagem6)): ?>
                                            <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico5Imagem6?>" title="<?=$configsModelo->body_servico5ImagemSEO6?>" alt="<?=$configsModelo->body_servico5ImagemSEO6?>">
                                        <?php endif?>
                                        
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao1)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao1?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao2)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao2?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao3)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao3?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao4)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao4?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao5)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao5?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao6)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao6?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao7)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao7?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao8)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao8?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao9)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao9?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao6Descricao10)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao6Descricao10?><?php endif?>
                                    </ul>

                                    <?php
                                        
                                        if(empty($configsModelo->body_servico5_botaoLink6))
                                        {
                                            $botaoServ5_opcao6 = "href='JavaScript:Void(0);'";

                                        } else {
                                            
                                            $botaoServ5_opcao6 = "href='".$configsModelo->body_servico5_botaoLink6."' target='_blank'";
                                        }
                                        
                                    
                                        if($configsModelo->body_servico5_botao6 === '1') : ?>
                                        
                                            <div class="card-pricing__footer">
                                                <a id="botaoServico5opc6" <?=$botaoServ5_opcao6?>>
                                                    <button class="button button-light botao_serv5">
                                                        <?=$configsModelo->body_servico5_botaoTexto6?>
                                                    </button>
                                                </a>
                                            </div>

                                        <?php endif; 
                                            
                                    ?>
                                    
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico5_opcao7 === '1') : ?>

                            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                                <div class="card text-center card-pricing servico5_corBoxImage">
                                    <div class="card-pricing__header servico5_corDivisoria">
                                        <h4 class="servico5_corTexto"><?=$configsModelo->body_servico5Titulo7?></h4>
                                        <p class="servico5_corTexto"><?=$configsModelo->body_servico5Descricao7?></p>

                                        <?php if($configsModelo->body_servico5_preco7_opcao7 === '1'): ?>
                                            <h1 class="servico5_corTexto"><span>R$ </span><?=number_format($configsModelo->body_servico5_preco7, 2, ',', '.')?></h1>
                                        <?php endif?>
                                        
                                    </div>
                                    <ul class="card-pricing__list servico5_corDivisoria">
                                        
                                        <?php if(!empty($configsModelo->body_servico5Imagem7)): ?>
                                            <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico5Imagem7?>" title="<?=$configsModelo->body_servico5ImagemSEO7?>" alt="<?=$configsModelo->body_servico5ImagemSEO7?>">
                                        <?php endif?>
                                        
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao1)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao1?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao2)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao2?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao3)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao3?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao4)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao4?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao5)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao5?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao6)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao6?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao7)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao7?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao8)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao8?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao9)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao9?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao7Descricao10)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao7Descricao10?><?php endif?>
                                    </ul>

                                    <?php
                                        
                                        if(empty($configsModelo->body_servico5_botaoLink7))
                                        {
                                            $botaoServ5_opcao7 = "href='JavaScript:Void(0);'";

                                        } else {
                                            
                                            $botaoServ5_opcao7 = "href='".$configsModelo->body_servico5_botaoLink7."' target='_blank'";
                                        }
                                        
                                    
                                        if($configsModelo->body_servico5_botao7 === '1') : ?>
                                        
                                            <div class="card-pricing__footer">
                                                <a id="botaoServico5opc7" <?=$botaoServ5_opcao7?>>
                                                    <button class="button button-light botao_serv5">
                                                        <?=$configsModelo->body_servico5_botaoTexto7?>
                                                    </button>
                                                </a>
                                            </div>

                                        <?php endif; 
                                            
                                    ?>
                                    
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico5_opcao8 === '1') : ?>

                            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                                <div class="card text-center card-pricing servico5_corBoxImage">
                                    <div class="card-pricing__header servico5_corDivisoria">
                                        <h4 class="servico5_corTexto"><?=$configsModelo->body_servico5Titulo8?></h4>
                                        <p class="servico5_corTexto"><?=$configsModelo->body_servico5Descricao8?></p>
                                        
                                        <?php if($configsModelo->body_servico5_preco8_opcao8 === '1'): ?>
                                            <h1 class="servico5_corTexto"><span>R$ </span><?=number_format($configsModelo->body_servico5_preco8, 2, ',', '.')?></h1>
                                        <?php endif?>
                                        
                                    </div>
                                    <ul class="card-pricing__list servico5_corDivisoria">
                                        
                                        <?php if(!empty($configsModelo->body_servico5Imagem8)): ?>
                                            <img class="img-fluid" src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico5Imagem8?>" title="<?=$configsModelo->body_servico5ImagemSEO7?>" alt="<?=$configsModelo->body_servico5ImagemSEO7?>">
                                        <?php endif?>
                                        
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao1)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao1?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao2)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao2?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao3)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao3?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao4)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao4?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao5)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao5?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao6)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao6?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao7)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao7?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao8)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao8?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao9)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao9?><?php endif?>
                                        <?php if(!empty($configsModelo->body_servico5_opcao8Descricao10)): ?><li class="servico5_corTexto"><?=$configsModelo->body_servico5_opcao8Descricao10?><?php endif?>
                                    </ul>

                                    <?php
                                        
                                        if(empty($configsModelo->body_servico5_botaoLink8))
                                        {
                                            $botaoServ5_opcao8 = "href='JavaScript:Void(0);'";

                                        } else {
                                            
                                            $botaoServ5_opcao8 = "href='".$configsModelo->body_servico5_botaoLink8."' target='_blank'";
                                        }
                                        
                                    
                                        if($configsModelo->body_servico5_botao8 === '1') : ?>
                                        
                                            <div class="card-pricing__footer">
                                                <a id="botaoServico5opc8" <?=$botaoServ5_opcao8?>>
                                                    <button class="button button-light botao_serv5">
                                                        <?=$configsModelo->body_servico5_botaoTexto8?>
                                                    </button>
                                                </a>
                                            </div>
                                            
                                        <?php endif; 
                                            
                                    ?>
                                    
                                </div>
                            </div>

                        <?php endif; ?>
                        
                    </div>
                </div>
            </section>
        
        <?php endif ?>

        <?php if($configsModelo->body_servico5_form6 === '1') : ?>

            <section class="servico5_form6">
                    
                <div class="container">
                
                    <?php if(!empty($configsModelo->body_servicoForm_tituloForm6) && !empty($configsModelo->body_servicoForm_descricaoForm6)) : ?>
                        <div class="col-12 mb-4">
                            <div class="section-intro text-center">
                                <h2 class="servico_form6_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloForm6?></h2>
                                <p class="servico_form6_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoForm6?></p>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsForm6">

                            <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">

                            <div class="form-row align-items-center">
                                
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label for="example-select"></label>
                                    <input type="text" class="form-control mb-2" placeholder="Seu nome" id="nomeVisitante6" name="nomeVisitante" autocomplete="off">
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" id="telefoneVisitante6" name="telefoneVisitante" autocomplete="off"
                                                data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitante6" name="emailVisitante" autocomplete="off">
                                    </div>
                                </div> 
                                
                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label></label>
                                    <div class="input-group mb-2">
                                        <select class="form-control" id="ondeNosConheceu6" name="ondeNosConheceu">
                                            
                                            <option>Onde nos conheceu?</option>
                                            
                                            <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
                                    <div class="input-group mt-3">
                                        <button type="submit" class="btn btn-block waves-effect waves-light corFundoBotaoForm6" id="leadsBotaoForm6"><?=$configsModelo->body_servico5_form6_textBotao?></button>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="erroMsg6"></div>

                        </form>

                    </div>

                </div>

            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico6 === '1') : ?>

            <section class="section-padding servico6">
                <div class="container">
                    <div class="section-intro pb-5 text-center">

                        <?php if($configsModelo->body_conteudoGenerico6 === '1') : ?>
                            <h2 class="section-intro__title servico6_corTituloGenerico1"><?=$configsModelo->body_textoGenericoTitulo6?></h2>
                            <p class="section-intro__subtitle servico6_corDescricaoGenerico1"><?=$configsModelo->body_textoGenericoDescricao6?></p>
                        <?php endif; ?>

                    </div>

                    <div class="owl-carousel owl-theme testimonial">
                        
                        <?php if($configsModelo->body_servico6_opcao1 === '1') : ?>

                            <div class="testimonial__item text-center">
                                <div class="testimonial__img">
                                    <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico6Imagem1?>" title="<?=$configsModelo->body_servico6ImagemSEO1?>" alt="<?=$configsModelo->body_servico6ImagemSEO1?>">
                                </div>
                                <div class="testimonial__content servico6">
                                    <h3 class="servico6_corTexto"><?=$configsModelo->body_servico6Titulo1?></h3>
                                    
                                    <p class="testimonial__i mt-3 servico6_corTexto"><?=$configsModelo->body_servico6Descricao1?></p>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico6_opcao2 === '1') : ?>

                            <div class="testimonial__item text-center">
                                <div class="testimonial__img">
                                    <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico6Imagem2?>" title="<?=$configsModelo->body_servico6ImagemSEO2?>" alt="<?=$configsModelo->body_servico6ImagemSEO2?>">
                                </div>
                                <div class="testimonial__content servico6">
                                    <h3 class="servico6_corTexto"><?=$configsModelo->body_servico6Titulo2?></h3>
                                    
                                    <p class="testimonial__i mt-3 servico6_corTexto"><?=$configsModelo->body_servico6Descricao2?></p>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico6_opcao3 === '1') : ?>
                            
                            <div class="testimonial__item text-center">
                                <div class="testimonial__img">
                                    <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico6Imagem3?>" title="<?=$configsModelo->body_servico6ImagemSEO3?>" alt="<?=$configsModelo->body_servico6ImagemSEO3?>">
                                </div>
                                <div class="testimonial__content servico6">
                                    <h3 class="servico6_corTexto"><?=$configsModelo->body_servico6Titulo3?></h3>
                                    
                                    <p class="testimonial__i mt-3 servico6_corTexto"><?=$configsModelo->body_servico6Descricao3?></p>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico6_opcao4 === '1') : ?>
                            
                            <div class="testimonial__item text-center">
                                <div class="testimonial__img">
                                    <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico6Imagem4?>" title="<?=$configsModelo->body_servico6ImagemSEO4?>" alt="<?=$configsModelo->body_servico6ImagemSEO4?>">
                                </div>
                                <div class="testimonial__content servico6">
                                    <h3 class="servico6_corTexto"><?=$configsModelo->body_servico6Titulo4?></h3>
                                    
                                    <p class="testimonial__i mt-3 servico6_corTexto"><?=$configsModelo->body_servico6Descricao4?></p>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico6_opcao5 === '1') : ?>
                            
                            <div class="testimonial__item text-center">
                                <div class="testimonial__img">
                                    <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico6Imagem5?>" title="<?=$configsModelo->body_servico6ImagemSEO5?>" alt="<?=$configsModelo->body_servico6ImagemSEO5?>">
                                </div>
                                <div class="testimonial__content servico6">
                                    <h3 class="servico6_corTexto"><?=$configsModelo->body_servico6Titulo5?></h3>
                                    
                                    <p class="testimonial__i mt-3 servico6_corTexto"><?=$configsModelo->body_servico6Descricao5?></p>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico6_opcao6 === '1') : ?>
                            
                            <div class="testimonial__item text-center">
                                <div class="testimonial__img">
                                    <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico6Imagem6?>" title="<?=$configsModelo->body_servico6ImagemSEO6?>" alt="<?=$configsModelo->body_servico6ImagemSEO6?>">
                                </div>
                                <div class="testimonial__content servico6">
                                    <h3 class="servico6_corTexto"><?=$configsModelo->body_servico6Titulo6?></h3>
                                    
                                    <p class="testimonial__i mt-3 servico6_corTexto"><?=$configsModelo->body_servico6Descricao6?></p>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico6_opcao7 === '1') : ?>
                            
                            <div class="testimonial__item text-center">
                                <div class="testimonial__img">
                                    <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico6Imagem7?>" title="<?=$configsModelo->body_servico6ImagemSEO7?>" alt="<?=$configsModelo->body_servico6ImagemSEO7?>">
                                </div>
                                <div class="testimonial__content servico6">
                                    <h3 class="servico6_corTexto"><?=$configsModelo->body_servico6Titulo7?></h3>
                                    
                                    <p class="testimonial__i mt-3 servico6_corTexto"><?=$configsModelo->body_servico6Descricao7?></p>
                                </div>
                            </div>

                        <?php endif; ?>

                        <?php if($configsModelo->body_servico6_opcao8 === '1') : ?>
                            
                            <div class="testimonial__item text-center">
                                <div class="testimonial__img">
                                    <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico6Imagem8?>" title="<?=$configsModelo->body_servico6ImagemSEO8?>" alt="<?=$configsModelo->body_servico6ImagemSEO8?>">
                                </div>
                                <div class="testimonial__content servico6">
                                    <h3 class="servico6_corTexto"><?=$configsModelo->body_servico6Titulo8?></h3>
                                    
                                    <p class="testimonial__i mt-3 servico6_corTexto"><?=$configsModelo->body_servico6Descricao8?></p>
                                </div>
                            </div>

                        <?php endif; ?>

                    </div>
                </div>
            </section>

        <?php endif ?>
        
        <?php if($configsModelo->body_servico6_form7 === '1') : ?>

            <section class="servico6_form7">
                    
                <div class="container">

                    <?php if(!empty($configsModelo->body_servicoForm_tituloForm7) && !empty($configsModelo->body_servicoForm_descricaoForm7)) : ?>
                        <div class="col-12 mb-4">
                            <div class="section-intro text-center">
                                <h2 class="servico_form7_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloForm7?></h2>
                                <p class="servico_form7_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoForm7?></p>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsForm7">

                            <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">

                            <div class="form-row align-items-center">
                                
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label for="example-select"></label>
                                    <input type="text" class="form-control mb-2" placeholder="Seu nome" id="nomeVisitante7" name="nomeVisitante" autocomplete="off">
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" id="telefoneVisitante7" name="telefoneVisitante" autocomplete="off"
                                                data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitante7" name="emailVisitante" autocomplete="off">
                                    </div>
                                </div> 
                                
                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label></label>
                                    <div class="input-group mb-2">
                                        <select class="form-control" id="ondeNosConheceu7" name="ondeNosConheceu">
                                            
                                            <option>Onde nos conheceu?</option>
                                            
                                            <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
                                    <div class="input-group mt-3">
                                        <button type="submit" class="btn btn-block  waves-effect waves-light corFundoBotaoForm7" id="leadsBotaoForm7"><?=$configsModelo->body_servico6_form7_textBotao?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="erroMsg7"></div>

                        </form>

                    </div>

                </div>

            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico7 === '1') : ?>
            
            <section class="clients_logo_area servico7">
                <div class="container">
                    
                    <div class="section-intro pb-5 text-center">

                        <?php if($configsModelo->body_conteudoGenerico7 === '1') : ?>
                            <h2 class="section-intro__title servico7_corTituloGenerico1"><?=$configsModelo->body_textoGenericoTitulo7?></h2>
                            <p class="section-intro__subtitle servico7_corDescricaoGenerico1"><?=$configsModelo->body_textoGenericoDescricao7?></p>
                        <?php endif; ?>

                    </div>

                    <div class="clients_slider owl-carousel">
                        
                        <?php if($configsModelo->body_servico7_opcao1 === '1') : ?>
                            <div class="item">
                                <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico7Imagem1?>" title="<?=$configsModelo->body_servico7ImagemSEO1?>" alt="<?=$configsModelo->body_servico7ImagemSEO1?>">
                            </div>
                        <?php endif ?>

                        <?php if($configsModelo->body_servico7_opcao2 === '1') : ?>
                            <div class="item">
                                <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico7Imagem2?>" title="<?=$configsModelo->body_servico7ImagemSEO2?>" alt="<?=$configsModelo->body_servico7ImagemSEO2?>">
                            </div>
                        <?php endif ?>

                        <?php if($configsModelo->body_servico7_opcao3 === '1') : ?>
                            <div class="item">
                                <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico7Imagem3?>" title="<?=$configsModelo->body_servico7ImagemSEO3?>" alt="<?=$configsModelo->body_servico7ImagemSEO3?>">
                            </div>
                        <?php endif ?>

                        <?php if($configsModelo->body_servico7_opcao4 === '1') : ?>
                            <div class="item">
                                <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico7Imagem4?>" title="<?=$configsModelo->body_servico7ImagemSEO4?>" alt="<?=$configsModelo->body_servico7ImagemSEO4?>">
                            </div>
                        <?php endif ?>

                        <?php if($configsModelo->body_servico7_opcao5 === '1') : ?>
                            <div class="item">
                                <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico7Imagem5?>" title="<?=$configsModelo->body_servico7ImagemSEO5?>" alt="<?=$configsModelo->body_servico7ImagemSEO5?>">
                            </div>
                        <?php endif ?>

                        <?php if($configsModelo->body_servico7_opcao6 === '1') : ?>
                            <div class="item">
                                <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico7Imagem6?>" title="<?=$configsModelo->body_servico7ImagemSEO6?>" alt="<?=$configsModelo->body_servico7ImagemSEO6?>">
                            </div>
                        <?php endif ?>

                        <?php if($configsModelo->body_servico7_opcao7 === '1') : ?>
                            <div class="item">
                                <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico7Imagem7?>" title="<?=$configsModelo->body_servico7ImagemSEO7?>" alt="<?=$configsModelo->body_servico7ImagemSEO7?>">
                            </div>
                        <?php endif ?>

                        <?php if($configsModelo->body_servico7_opcao8 === '1') : ?>
                            <div class="item">
                                <img src="<?=base_url('assets/images/servicos/').$configsModelo->body_servico7Imagem8?>" title="<?=$configsModelo->body_servico7ImagemSEO8?>" alt="<?=$configsModelo->body_servico7ImagemSEO8?>">
                            </div>
                        <?php endif ?>

                    </div>
                </div>
            </section>

        <?php endif ?>

        <?php if($configsModelo->body_servico7_form8 === '1') : ?>

            <section class="servico7_form8">
                    
                <div class="container">

                    <?php if(!empty($configsModelo->body_servicoForm_tituloForm8) && !empty($configsModelo->body_servicoForm_descricaoForm8)) : ?>
                        <div class="col-12 mb-4">
                            <div class="section-intro text-center">
                                <h2 class="servico_form8_corTitulo section-intro__title"><?=$configsModelo->body_servicoForm_tituloForm8?></h2>
                                <p class="servico_form8_corDescricao section-intro__subtitle"><?=$configsModelo->body_servicoForm_descricaoForm8?></p>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12 col-lg-12">

                        <form id="leadsForm8">

                            <input type="hidden" value="<?=$dadosLandingPage->grupoLista_landingPage?>" name="grupoLista_landingPage">

                            <div class="form-row align-items-center">
                                
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label for="example-select"></label>
                                    <input type="text" class="form-control mb-2" placeholder="Seu nome" id="nomeVisitante8" name="nomeVisitante" autocomplete="off">
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Seu telefone (Whatsapp)" id="telefoneVisitante8" name="telefoneVisitante" autocomplete="off"
                                                data-toggle="input-mask" data-mask-format="(00) 0 0000-0000">
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <label for="example-select"></label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="email" class="form-control" placeholder="Seu e-mail" id="emailVisitante8" name="emailVisitante" autocomplete="off">
                                    </div>
                                </div> 
                                
                                <div  class="col-xs-12 col-sm-6 col-md-3 col-lg-2 col-xl-2">
                                    <label></label>
                                    <div class="input-group mb-2">
                                        <select class="form-control" id="ondeNosConheceu8" name="ondeNosConheceu">
                                            
                                            <option>Onde nos conheceu?</option>
                                            
                                            <?php if($configsModelo->nosConheceuOpcao1_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao1?>"><?=$configsModelo->nosConheceuOpcao1?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao2_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao2?>"><?=$configsModelo->nosConheceuOpcao2?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao3_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao3?>"><?=$configsModelo->nosConheceuOpcao3?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao4_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao4?>"><?=$configsModelo->nosConheceuOpcao4?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao5_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao5?>"><?=$configsModelo->nosConheceuOpcao5?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao6_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao6?>"><?=$configsModelo->nosConheceuOpcao6?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao7_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao7?>"><?=$configsModelo->nosConheceuOpcao7?></option>
                                            <?php endif ?>

                                            <?php if($configsModelo->nosConheceuOpcao8_visivel === '1') : ?>
                                                <option value="<?=$configsModelo->nosConheceuOpcao8?>"><?=$configsModelo->nosConheceuOpcao8?></option>
                                            <?php endif ?>

                                        </select>
                                    </div>
                                </div>

                                <div  class="col-xs-12 col-sm-12 col-md-1 col-lg-1 col-xl-2">
                                    <div class="input-group mt-3">
                                        <button type="submit" class="btn btn-block waves-effect waves-light corFundoBotaoForm8" id="leadsBotaoForm8"><?=$configsModelo->body_servico7_form8_textBotao?></button>
                                    </div>
                                </div>
                            </div>

                            <div id="erroMsg8"></div>

                        </form>

                    </div>

                </div>

            </section>

        <?php endif ?>

        

        <?php if($configsModelo->body_maps === '1') : ?>

            <section class="servico_maps">

                <div class="section-intro pb-5 text-center">

                    <?php if($configsModelo->body_conteudoGenerico_maps === '1') : ?>
                        <h2 class="section-intro__title servico_maps_corTituloGenerico1"><?=$configsModelo->body_maps_textoGenericoTitulo1?></h2>
                        <p class="section-intro__subtitle servico_maps_corDescricaoGenerico1"><?=$configsModelo->body_maps_textoGenericoDescricao1?></p>
                    <?php endif; ?>

                </div>

                <div class=" pb-2 text-center">
                    
                    <p class="section-intro__subtitle servico_maps_corDescricaoGenerico1">
                        
                        <?php if(!empty($configs->endereco_landingPage)) : ?> 
                            <i class="fa fa-map-marker pr-2" aria-hidden="true"></i> <?=$configs->endereco_landingPage?>
                        <?php endif; ?>
                        
                        <?php if(!empty($configs->fone1_landingPage)) : ?> / 
                            <i class="fa fa-phone pl-2" aria-hidden="true"></i> <?=$configs->fone1_landingPage?>
                        <?php endif; ?>

                        <?php if(!empty($configs->fone2_landingPage)) : ?> / 
                            <i class="fa fa-phone pl-2" aria-hidden="true"></i> <?=$configs->fone2_landingPage?>
                        <?php endif; ?>
                    </p>

                </div>

                <?php if(!empty($configs->googleMaps_landingPage)) : ?>
                    <div class="container">
                        <?=$configs->googleMaps_landingPage?>
                    </div>
                <?php endif; ?>
                
            </section>

        <?php endif ?>

    </main>
    
    <footer class="footer-area section-gap">
        <div class="container">
            
            <div class="footer-bottom row align-items-center text-center text-lg-left">
                <p class="footer-text m-0 col-lg-8 col-md-12">
                
                    &copy;<script>
                        document.write(new Date().getFullYear());
                    </script> Todos os direitos reservados | visite o nosso site - <a id="linkRodapeSite" href="<?=$configs->linkSite_landingPage?>" target="_blank">www.<?=$url_atual?></a>
                
                </p>

                <div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
                    
                    <a id="botaoRodapeFacebook" href="https://www.facebook.com/<?=$configs->linkFacebook_landingPage?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a id="botaoRodapeInstagram" href="https://www.instagram.com/<?=$configs->linkInstagram_landingPage?>" target="_blank"><i class="fab fa-instagram"></i></a>
                    
                    <?php

                        if(!empty($configs->fone1_landingPage)) 
                        {
                            if($configs->verificaWhatsapp1_landingPage === '1') { ?>
                                
                                <a id="botaoRodapeWhatsApp1" href="https://api.whatsapp.com/send?phone=55<?=$configs->fone1_landingPage?>&text=<?=$configs->fone1TextoWhats_landingPage?>" target="_blank"><i class="fab fa-whatsapp"></i></a>

                            <?php } else {  ?>

                                <a data-toggle="modal" data-target="#telefone1"><i class="fa fa-phone"></i></a>

                            <?php }
                        }

                        if(!empty($configs->fone2_landingPage)) 
                        {
                            if($configs->verificaWhatsapp2_landingPage === '1') { ?>
                                
                                <a id="botaoRodapeWhatsApp2" href="https://api.whatsapp.com/send?phone=55<?=$configs->fone2_landingPage?>&text=<?=$configs->fone2TextoWhats_landingPage?>" target="_blank"><i class="fab fa-whatsapp"></i></a>

                            <?php } else {  ?>

                                <a data-toggle="modal" data-target="#telefone2"><i class="fa fa-phone"></i></a>

                            <?php }
                        }
                        
                        
                    ?>

                </div>
            </div>
        </div>
    </footer>

    <div class="modal fade" id="telefone1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ver telefone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <center>
                    <h1>
                        <a id="botaoRodapeTelefone1" href="tel:+55<?=$configs->fone1_landingPage?>"><?=$configs->fone1_landingPage?></a>
                    </h1>
                </center>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="telefone2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ver telefone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <center>
                    <h1>
                        <a id="botaoRodapeTelefone2" href="tel:+55<?=$configs->fone2_landingPage?>"><?=$configs->fone2_landingPage?></a>
                    </h1>
                </center>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
            </div>
        </div>
    </div>

    <link rel="stylesheet" type="text/css" href="/assets/libs/whatsapp2/webquantica-grid.css">
	
	<link rel="stylesheet" type="text/css" href="/assets/libs/whatsapp2/style.css">

    <style>
        .fa-whatsapp:hover
        {
            color: #ffffff !important;
        }
    </style>

    
    
    <ul>

        <?php
            if($configs->verificaWhatsapp1_landingPage === '1'){
                $alturaStyle = 'margin-bottom: 110px;';
            } else {
                $alturaStyle = 'margin-bottom: 50px;';
            }
        ?>
        
        <?php if($configs->verificaWhatsapp2_landingPage === '1') : ?>
            <li>
                <div class="wq-whatsapp_btn" style="<?=$alturaStyle?>">
                <a id="botaoLateralWhatsApp1" href="https://api.whatsapp.com/send?phone=55<?=$configs->fone2_landingPage?>&text=<?=$configs->fone2TextoWhats_landingPage?>" target="_blank">
                        <i class="fa fa-whatsapp" aria-hidden="true"></i>
                    </a>
                </div>
            </li>
        <?php endif ?>

        <?php if($configs->verificaWhatsapp1_landingPage === '1') : ?>
            <li>
                <div class="wq-whatsapp_btn" style="margin-bottom: 50px;">
                    <a id="botaoLateralWhatsApp2" href="https://api.whatsapp.com/send?phone=55<?=$configs->fone1_landingPage?>&text=<?=$configs->fone1TextoWhats_landingPage?>" target="_blank">
                        <i class="fa fa-whatsapp" aria-hidden="true"></i>
                    </a>
                </div>
            </li>
        <?php endif ?>

    </ul>

    <script src="<?=base_url('modelos/modelo1/')?>vendors/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?=base_url('modelos/modelo1/')?>vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url('modelos/modelo1/')?>vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?=base_url('modelos/modelo1/')?>js/jquery.ajaxchimp.min.js"></script>
    <script src="<?=base_url('modelos/modelo1/')?>js/mail-script.js"></script>
    <script src="<?=base_url('modelos/modelo1/')?>js/main.js"></script>
    <script src="<?=base_url('modelos/modelo1/')?>js/main.js"></script>
    
    <?=$tempoOnline?>

    <?=$scriptJS?>

    <script>
        <?=$configsModelo->topo_head_js?>
    </script>


</body>

</html>