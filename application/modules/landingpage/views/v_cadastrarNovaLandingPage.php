
<style>
.fundo_card-body
{
    background-color: #373e48;
}

.custom-file-input:disabled~.custom-file-label {
    background-color: #4b5460;
}

</style>

<div class="content-page">

    <div class="content">
        
        <div class="container-fluid">
            
            <div class="row mt-3">
            
                <div class="col-12">

                    <?php
                        $this->load->view('botoesLandingPages');
                    ?>
                    
                    <div class="row mt-3">
                        <div class="col-lg-12">
                            <!-- Portlet card -->
                            <div class="card">
                                <div class="card-body">
                                    
                                    <h5 class="card-title mb-3">Cadastrar nova landing page</h5>

                                    <form id="formCadastrarNovaLandingPage" enctype="multipart/form-data">

                                        <div class="row">
                                            
                                            <div class="col-12">
                                                
                                                <div class="col-md-12">
                                                    <div id="erroMsg">
                                                        <input type="hidden" id="focoImagens">
                                                    </div>
                                                </div>

                                                <div class="card">
                                                    <div class="card-body">
                                                        
                                                        <h4 class="header-title">Nome e Título</h4>

                                                        <div class="form-row">
                                                            
                                                            <div class="form-group col-md-4">
                                                                <label class="col-form-label">Nome (<small>Nome na lista</small>)</label>
                                                                <input type="text" class="form-control" id="nomeLandingPage" name="nomeLandingPage" autocomplete="off">
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label class="col-form-label">Título (<small>Nome na URL</small>)</label>
                                                                <input type="text" class="form-control" id="tituloLandingPage" name="tituloLandingPage" autocomplete="off">
                                                            </div>

                                                            <div class="form-group col-md-4">
                                                                <label class="col-form-label">Lista (<small>Grupo para envio dos e-mails</small>)</label>
                                                                <select class="form-control" id="listaEmailsLandingPage" name="listaEmailsLandingPage">
                                                                    
                                                                </select>
                                                            </div>

                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit" id="botaoCriarNovaLandingPage" class="btn btn-block btn-sm btn-primary waves-effect waves-light">Cadastrar</button>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
</div>
